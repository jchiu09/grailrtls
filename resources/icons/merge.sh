#! /usr/bin/bash

echo "Mergings all pngs into grail_map_icons.png"
#montage -background "transparent" ./*.png -geometry 200x175 -matte -transparent "transparent" grail_map_icons.png
files=`ls *.png | grep -v grail_map_icons | grep -v transparent_overlay.png | awk '{str = str" "$0} END{print str} ORS=""'`
#Resize any images largers than 90x48. Tiles will be 90 by 50 in a single column.
montage -background "transparent" $files -tile 1x -geometry 90x48\>+0+1 -matte -transparent "transparent" grail_map_icons.png
montage -background "transparent" $files -frame 3 -tile 1x -geometry 84x42\>+0+1 -matte -transparent "transparent" grail_map_icons_hover.png

#Or
#convert *.png +append out.png
