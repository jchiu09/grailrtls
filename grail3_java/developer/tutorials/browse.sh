#!/bin/bash

REQUIRED_ARGS=1

E_BADARGS=65

WM_HOST="localhost"
WM_SOLV_PORT=7009
WM_CLIENT_PORT=7010

JAR_FILE=target/developer-tutorials-3.0.3-SNAPSHOT-jar-with-dependencies.jar
LAUNCH_CLASS=org.grailrtls.tutorials.world_model.SimpleCLIBrowser

if [ $# -lt $REQUIRED_ARGS ]
then
  echo "Usage: `basename $0` {World Model Host} [{Solver Port} {Client Port}]" 
  exit $E_BADARGS
else
  WM_HOST=$1
fi

if [ $# -gt 1 ]
then
  WM_SOLV_PORT=$2
fi

if [ $# -gt 2 ]
then
  WM_CLIENT_PORT=$3
fi

if [ ! -f $JAR_FILE ]
then
  echo "Could not fine $JAR_FILE. Try 'mvn clean install'?"
fi

java -cp $JAR_FILE $LAUNCH_CLASS $WM_HOST $WM_SOLV_PORT $WM_CLIENT_PORT
