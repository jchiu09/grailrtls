/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.tutorials.solver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.grailrtls.libsolver.SolverAggregatorInterface;
import org.grailrtls.libsolver.listeners.ConnectionListener;
import org.grailrtls.libsolver.listeners.SampleListener;
import org.grailrtls.libsolver.protocol.messages.SampleMessage;
import org.grailrtls.libsolver.rules.SubscriptionRequestRule;

/**
 * A very simple example of how to use the Aggregator-Solver protocol library for Java (libsolver). A single class that contacts an aggregator and prints samples to standard out.
 * 
 * This example relies on libcommon for printing of device addresses, and libsolver to communicate with the Aggregator.
 * 
 * @author Robert Moore
 * 
 */

/*
 * Implement SampleListener to be able to print samples sent by the aggregator. Implement ConnectionListener to stop running after the connection is closed.
 */
public class SimpleDeviceLister extends Thread implements SampleListener, ConnectionListener
{
    /**
     * Flag to keep running until the connection is closed.
     */
    private boolean keepRunning = true;

    /**
     * Interface to the aggregator, takes care of all the basic protocol stuff.
     */
    private SolverAggregatorInterface aggregatorInterface = new SolverAggregatorInterface();

    /**
     * Used to get user input.
     */
    private BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));

    private List<Integer> deviceIds = new ArrayList<Integer>();

    private List<Integer> oldDeviceIds = new ArrayList<Integer>();

    /**
     * Creates a new {@code SimpleSampleFilter} and starts it.
     * 
     * @param args
     *            Aggregator host and port, in that order.
     */
    public static void main(String[] args)
    {
	if (args.length < 2)
	{
	    printUsageInfo();
	    System.exit(1);
	}

	// Normally, catching the exception would be good, but this is just a
	// tutorial.
	int port = Integer.parseInt(args[1]);

	SimpleDeviceLister solver = new SimpleDeviceLister(args[0], port);

	solver.start();
    }

    /**
     * Prints a simple message about the parameters for this program.
     */
    protected static void printUsageInfo()
    {
	System.out.println("Requires two parameters: <Aggregator host> <Aggregator port> [filter].");
	System.out.println("Once running, type \"exit\" to exit.");
    }

    /**
     * Creates a new {@code SimpleSampleFilter} that connects to an aggregator at the specified host name/IP address and port number.
     * 
     * @param aggHost
     *            the hostname or IP address of the aggregator.
     * @param aggPort
     *            the port number the aggregator is listening for solvers on.
     */
    public SimpleDeviceLister(String aggHost, int aggPort)
    {
	this.aggregatorInterface.setHost(aggHost);
	this.aggregatorInterface.setPort(aggPort);
    }

    /**
     * Connects to the aggregator. Checks for user input every 100ms, and exits if the aggregator connection is ended, or the user types "exit".
     */
    @Override
    public void run()
    {
	// Create a subscription rule to limit samples to 1 per 10 seconds for
	// each transmitter/receiver pair.
	SubscriptionRequestRule[] rules = new SubscriptionRequestRule[1];
	rules[0] = SubscriptionRequestRule.generateGenericRule();
	rules[0].setUpdateInterval(0l);
	this.aggregatorInterface.setRules(rules);

	// Add this class as a sample listener to print them out.
	this.aggregatorInterface.addSampleListener(this);

	// Add this class as a connection listener to know when the connection
	// closes.
	this.aggregatorInterface.addConnectionListener(this);

	// Disconnect if we have any problems
	this.aggregatorInterface.setDisconnectOnException(true);

	// Don't try to reconnect after a disconnect
	this.aggregatorInterface.setStayConnected(true);

	// Wait 30 seconds between attempts to connect to the aggregator.
	this.aggregatorInterface.setConnectionRetryDelay(30000);

	// Connect to the aggregator
	if (!this.aggregatorInterface.doConnectionSetup())
	{
	    System.out.println("Unable to establish a connection to the aggregator.");
	    System.exit(1);
	}
	System.out.println("Connected to aggregator at " + this.aggregatorInterface.getHost() + ":" + this.aggregatorInterface.getPort());

	// Read the user input into this string
	String command = null;

	new Thread()
	{
	    private boolean running = true;

	    public void run()
	    {
		while (running)
		{
		    try
		    {
			System.out.println("Device ID List: ");
			synchronized (deviceIds)
			{
			    for (int id : deviceIds)
			    {
				System.out.println(id);
			    }
			}
			Thread.sleep(5000l);
		    } catch (InterruptedException e)
		    {

		    }
		}
	    }

	};

	// Keep running until either we get disconnected, or the user wants to
	// quit.
	while (this.keepRunning)
	{
	    try
	    {
		// If the user has something to read in the buffer.
		if (this.userInput.ready())
		{
		    command = this.userInput.readLine();

		    // Allow the solver to shut down gracefully if the user
		    // wants to exit.
		    if ("exit".equalsIgnoreCase(command))
		    {
			System.out.println("Exiting solver (user).");
			break;
		    }

		    System.out.println("Unknown command \"" + command + "\". Type \"exit\" to exit.");
		}
	    } catch (IOException e1)
	    {
		System.err.println("Error reading user input.");
		e1.printStackTrace();
	    }
	    // Sleep for 100ms, since there isn't much else to do.
	    try
	    {
		Thread.sleep(100l);
	    } catch (InterruptedException e)
	    {
		// Ignored
	    }
	}
	// Stop running
	this.aggregatorInterface.doConnectionTearDown();
	System.exit(0);
    }

    /**
     * Prints out the received {@link SampleMessage}.
     */
    public void sampleReceived(SolverAggregatorInterface aggregator, SampleMessage sample)
    {
	int deviceId = SimpleDeviceLister.getPipId(sample.getDeviceId());
	int receiverId = SimpleDeviceLister.getPipId(sample.getReceiverId());
	synchronized (deviceIds)
	{
	    if (!deviceIds.contains(deviceId) && receiverId==188 && sample.getRssi()>-50)
	    {
		System.out.println("[SAMPLE] Device ID: " + deviceId +", Hub ID: "+receiverId+",  RSSI: "+sample.getRssi());
		deviceIds.add(deviceId);
		//Collections.sort(deviceIds);
	    }
	}
    }

    /**
     * Returns an integer representation of a 23-bit PIPSqueak device ID.
     * 
     * @param deviceId
     * @return the 23-bit PIPSqueak device ID as a 32-bit integer
     */
    private static int getPipId(byte[] deviceId)
    {
	// PIPSqueak tags only have a 23-bit ID, so we can extract the last 3
	// bytes as an integer
	return ((deviceId[SampleMessage.DEVICE_ID_SIZE - 3] << 16) & 0xFF) + ((deviceId[SampleMessage.DEVICE_ID_SIZE - 2] << 8) & 0xFF) + (deviceId[SampleMessage.DEVICE_ID_SIZE - 1] & 0xFF);
    }

    public void connectionEnded(SolverAggregatorInterface aggregator)
    {
	// Stop running after the connection is finally closed.
	System.out.println("Aggregator connection closed.");
	this.keepRunning = false;
	this.interrupt();
    }

    public void connectionEstablished(SolverAggregatorInterface aggregator)
    {
	// Not used
    }

    public void connectionInterrupted(SolverAggregatorInterface aggregator)
    {
	// Not used
    }

}
