package org.grailrtls.tutorials.world_model;

import java.util.Collection;
import java.util.Date;

import org.grailrtls.libworldmodel.client.ClientWorldConnection;
import org.grailrtls.libworldmodel.client.StepResponse;
import org.grailrtls.libworldmodel.client.WorldState;
import org.grailrtls.libworldmodel.client.protocol.messages.Attribute;

public class SimpleStreamingApp {

	public static void main(String[] args) {
		if (args.length != 3) {
			System.err
					.println("I need at least 3 things: <World Model Host> <World Model Port> <Query String>");
			return;
		}

		ClientWorldConnection wmc = new ClientWorldConnection();
		wmc.setHost(args[0]);
		wmc.setPort(Integer.parseInt(args[1]));
		if (!wmc.connect()) {
			System.err
					.println("Couldn't connect to the world model!  Check your connection parameters.");
			return;
		}

		long now = System.currentTimeMillis();
		long oneSecond = 1000l;
		System.out.println("Requesting from " + new Date(now) + " every " + oneSecond + " ms.");
		StepResponse response = wmc.getStreamRequest(args[2], now, oneSecond, ".*");
		
		WorldState state = null;
		// The next line will block until the response gets at least one state
		// Streaming requests only complete when they are cancelled
		while (!response.isComplete()) {
			try {
			state = response.next();
			}catch(Exception e){
				System.err.println("Error occured during request: " + e);
				e.printStackTrace();
				break;
			}
			Collection<String> uris = state.getURIs();
			if (uris != null) {
				for (String uri : uris) {
					System.out.println("URI: " + uri);
					Collection<Attribute> attribs = state.getState(uri);
					for (Attribute att : attribs) {
						System.out.println("\t" + att);
					}
					System.out.println();
				}
			}
		}

		wmc.disconnect();
	}
}
