package org.grailrtls.tutorials.world_model;

import java.util.Collection;

import org.grailrtls.libworldmodel.client.ClientWorldConnection;
import org.grailrtls.libworldmodel.client.Response;
import org.grailrtls.libworldmodel.client.WorldState;
import org.grailrtls.libworldmodel.client.protocol.messages.Attribute;

public class SimpleSnapshotApp {

	public static void main(String[] args) {
		if (args.length != 3) {
			System.err
					.println("I need at least 3 things: <World Model Host> <World Model Port> <Query String>");
			return;
		}

		ClientWorldConnection wmc = new ClientWorldConnection();
		wmc.setHost(args[0]);
		wmc.setPort(Integer.parseInt(args[1]));
		if (!wmc.connect()) {
			System.err
					.println("Couldn't connect to the world model!  Check your connection parameters.");
			return;
		}

		long now = System.currentTimeMillis();
		Response response = wmc.getSnapshot(args[2], now, now, ".*");

		try {
			// The next line will block until the response completes
			WorldState state = response.get();
			Collection<String> uris = state.getURIs();
			for (String uri : uris) {
				System.out.println("URI: " + uri);
				Collection<Attribute> attribs = state.getState(uri);
				for (Attribute att : attribs) {
					System.out.println("\t" + att);
				}
				System.out.println();
			}
		} catch (Exception e) {
			System.err.println("Exception thrown while getting response: "
					+ e.getMessage());
		}

		wmc.disconnect();
	}
}
