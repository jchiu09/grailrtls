/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2012 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.tutorials.world_model;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.grailrtls.libworldmodel.client.ClientWorldConnection;
import org.grailrtls.libworldmodel.client.ClientWorldModelInterface;
import org.grailrtls.libworldmodel.client.Response;
import org.grailrtls.libworldmodel.client.StepResponse;
import org.grailrtls.libworldmodel.client.WorldState;
import org.grailrtls.libworldmodel.client.protocol.codec.RangeRequestDecoder;
import org.grailrtls.libworldmodel.client.protocol.messages.AbstractRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.Attribute;
import org.grailrtls.libworldmodel.client.protocol.messages.AttributeAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.DataResponseMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginPreferenceMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.RangeRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.SnapshotRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.URISearchResponseMessage;
import org.grailrtls.libworldmodel.solver.SolverWorldConnection;
import org.grailrtls.libworldmodel.solver.SolverWorldModelInterface;
import org.grailrtls.libworldmodel.solver.listeners.ConnectionListener;
import org.grailrtls.libworldmodel.solver.listeners.DataListener;
import org.grailrtls.libworldmodel.solver.protocol.messages.DataTransferMessage.Solution;
import org.grailrtls.libworldmodel.solver.protocol.messages.StartTransientMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.StopTransientMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage.TypeSpecification;
import org.grailrtls.libworldmodel.types.DataConverter;
import org.grailrtls.libworldmodel.types.TypeConverter;
import org.grailrtls.libworldmodel.types.DoubleConverter;
import org.grailrtls.libworldmodel.types.IntegerConverter;
import org.grailrtls.libworldmodel.types.StringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleCLIBrowser extends Thread {

	private static final Logger log = LoggerFactory
			.getLogger(SimpleCLIBrowser.class);

	private static final String TARGET_URI = "test.target";

	private static final String ATTRIBUTE = "test.attribute";

	private static final String ORIGIN = "test.solver";

	private static final String CMD_SEARCH = "search";

	private static final String CMD_SHOW = "show";

	private static final String CMD_HISTORY = "history";

	private static final String CMD_CREATE = "create";

	private static final String CMD_DELETE = "delete";

	private static final String CMD_EXPIRE = "expire";

	private static final String CMD_UPDATE = "update";

	private static final String TYPE_URI = "uri";

	private static final String TYPE_ATTRIBUTE = "attribute";

	private static final String ALL_COMMANDS = "Available commands:\n"
			+ "\tsearch <URI REGEX>\n" + "\tshow <URI REGEX> [ATTRIBUTE]\n"
			+ "\thistory <URI REGEX>\n" + "\tcreate uri <URI>\n"
			+ "\tupdate <URI> <ATTRIBUTE>\n" + "\texpire uri <URI>\n"
			+ "\texpire attribute <URI> <ATTRIBUTE>\n" + "\tdelete uri <URI>\n"
			+ "\tdelete attribute <URI> <ATTRIBUTE>";

	private final SolverWorldConnection solverWorldModel = new SolverWorldConnection();

	private final ClientWorldConnection clientWorldModel = new ClientWorldConnection();

	private boolean keepRunning = true;

	public static void main(String[] args) {
		if (args.length < 3) {
			printUsageInfo();
			return;
		}

		int solverPort = 7012;
		try {
			solverPort = Integer.parseInt(args[1]);
		} catch (NumberFormatException nfe) {
			log.error(
					"Unable to format {} into a valid port number. Defaulting to {}.",
					args[1], solverPort);
		}
		int clientPort = 7012;
		try {
			clientPort = Integer.parseInt(args[2]);
		} catch (NumberFormatException nfe) {
			log.error(
					"Unable to format {} into a valid port number. Defaulting to {}.",
					args[2], clientPort);
		}

		final SimpleCLIBrowser solver = new SimpleCLIBrowser(args[0],
				solverPort, clientPort);
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				solver.keepRunning = false;
				solver.solverWorldModel.disconnect();
				solver.clientWorldModel.disconnect();
			}
		});
		solver.start();

	}

	public static void printUsageInfo() {
		StringBuffer sb = new StringBuffer(
				"Usage: <World Model IP> <World Model Solver Port> <World Model Client Port>\n");
		System.err.println(sb.toString());
	}

	public SimpleCLIBrowser(final String wmHost, final int wmSPort,
			final int wmCPort) {
		this.solverWorldModel.setHost(wmHost);
		this.solverWorldModel.setPort(wmSPort);

		TypeSpecification testType = new TypeSpecification();
		testType.setIsTransient(false);
		testType.setUriName(ATTRIBUTE);
		this.solverWorldModel.addSolutionType(testType);
		this.solverWorldModel.setOriginString(ORIGIN);

		this.clientWorldModel.setHost(wmHost);
		this.clientWorldModel.setPort(wmCPort);
	}

	@Override
	public void run() {
		if (this.solverWorldModel.connect() && this.clientWorldModel.connect()) {
			BufferedReader userIn = new BufferedReader(new InputStreamReader(
					System.in));

			System.out
					.println("Connected to World Model.\nAll data entered will use the origin value \""
							+ SimpleCLIBrowser.ORIGIN
							+ "\".\nEnter command and press <Enter>.");
			System.out.println("Use \"help\" for a list of commands.");

			while (this.keepRunning) {
				try {
					String userString = userIn.readLine();

					if (userString != null) {
						if ("help".equalsIgnoreCase(userString)) {
							System.out.println(ALL_COMMANDS);
							continue;
						}

						this.handleUserInput(userString.trim());
					}
				} catch (IOException e) {
					log.error("IOException occurred while reading from console: "
							+ e);
					e.printStackTrace();
					System.exit(1);
				}
			}
		}
	}

	private void handleUserInput(final String userString) {

		if (userString == null || userString.length() == 0) {
			return;
		}

		String[] words = userString.split("\\s+");

		if (words.length < 2) {
			System.out
					.println("All commands require at least 2 components. Type help<Enter> for a list of commands.");
			return;
		}

		String command = words[0];
		boolean handledCommand = false;

		if (CMD_SEARCH.equalsIgnoreCase(command)) {
			handledCommand = true;
			if (words.length == 2) {
				String[] results = this.clientWorldModel.searchURI(words[1]);
				System.out.println("Results for " + words[1] + ":");
				for (String result : results) {
					System.out.println("\t" + result);
				}
			} else {
				System.out
						.println("Invalid number of arguments to search a URI.");
			}
		} else if (CMD_SHOW.equalsIgnoreCase(command)) {
			handledCommand = true;
			if (words.length == 2) {
				long now = System.currentTimeMillis();
				Response resp = this.clientWorldModel.getSnapshot(words[1],
						now, now, ".*");
				try {
					WorldState state = resp.get();

					for (String uri : state.getURIs()) {
						System.out.println("--(" + uri + ")--");

						for (Attribute attr : state.getState(uri)) {
							System.out.println("+ " + attr + '\n');
						}
						System.out.println("--(" + uri + ")--");
					}
				} catch (Exception e) {
					System.out.println("Unable to show " + words[1]);
				}

			} else if (words.length == 3) {
				long now = System.currentTimeMillis();
				Response resp = this.clientWorldModel.getSnapshot(words[1],
						now, now, words[2]);
				try {
					WorldState state = resp.get();

					for (String uri : state.getURIs()) {
						System.out.println("--(" + uri + ")--");

						for (Attribute attr : state.getState(uri)) {
							System.out.println("+ " + attr + '\n');
						}
						System.out.println("--(" + uri + ")--");
					}
				} catch (Exception e) {
					System.out.println("Unable to show " + words[1] + "/"
							+ words[2]);
				}
			} else {
				System.out
						.println("Invalid number of arguments to show a URI.");
			}
		} else if (CMD_HISTORY.equalsIgnoreCase(command)) {
			handledCommand = true;
			if (words.length == 2) {
				StepResponse resp = this.clientWorldModel.getRangeRequest(
						words[1], 0l, System.currentTimeMillis(), ".*");
				WorldState state = null;
				while (!resp.isComplete()) {
					try {
						Thread.sleep(50);
					} catch (InterruptedException ie) {
						// Ignored
					}
				}
				while (resp.hasNext()) {
					try {
						state = resp.next();
					} catch (Exception e) {
						System.out
								.println("Unable to retrieve range request for "
										+ words[1]);
						break;
					}
					for (String uri : state.getURIs()) {
						System.out.println("--(" + uri + ")--");

						for (Attribute attr : state.getState(uri)) {
							System.out.println("+ " + attr + '\n');
						}
						System.out.println("--(" + uri + ")--");
					}
				}

			} else if (words.length == 3) {
				StepResponse resp = this.clientWorldModel.getRangeRequest(
						words[1], 0l, System.currentTimeMillis(), words[2]);
				WorldState state = null;
				while (!resp.isComplete()) {
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						// Ignored
					}
				}
				while (resp.hasNext()) {
					try {
						state = resp.next();
					} catch (Exception e) {
						System.out
								.println("Unable to retrieve range request for "
										+ words[1]);
						break;
					}
					for (String uri : state.getURIs()) {
						System.out.println("--(" + uri + ")--");

						for (Attribute attr : state.getState(uri)) {
							System.out.println("+ " + attr + '\n');
						}
						System.out.println("--(" + uri + ")--");
					}
				}
			}
		}

		else if (CMD_CREATE.equalsIgnoreCase(command)) {
			if (TYPE_URI.equalsIgnoreCase(words[1])) {
				handledCommand = true;
				this.solverWorldModel.createURI(words[2]);

			}
		} else if (CMD_UPDATE.equalsIgnoreCase(command)) {

			handledCommand = true;
			if (words.length == 3) {
				this.updateAttribute(words[1], words[2]);
			} else {
				System.out
						.println("Invalid number of arguments to create/update an attribute.");
			}

		} else if (CMD_EXPIRE.equalsIgnoreCase(command)) {
			if (TYPE_URI.equalsIgnoreCase(words[1])) {
				handledCommand = true;
				this.solverWorldModel.expire(words[2]);
			} else if (TYPE_ATTRIBUTE.equalsIgnoreCase(words[1])) {
				handledCommand = true;
				if (words.length == 4) {
					this.solverWorldModel.expire(words[2], words[3]);
				} else {
					System.out
							.println("Invalid number of arguments to expire an attribute.");
				}
			}
		} else if (CMD_DELETE.equalsIgnoreCase(command)) {
			if (TYPE_URI.equalsIgnoreCase(words[1])) {
				handledCommand = true;
				this.solverWorldModel.delete(words[2]);
			} else if (TYPE_ATTRIBUTE.equalsIgnoreCase(words[1])) {
				handledCommand = true;
				if (words.length == 4) {
					this.solverWorldModel.delete(words[2], words[3]);
				} else {
					System.out
							.println("Invalid number of arguments to delete an attribute.");
				}
			}
		}
		if (!handledCommand) {
			System.out.println("Unrecognized command: " + userString);
		}
	}

	private boolean updateAttribute(final String uri, final String attribute) {

		BufferedReader userIn = new BufferedReader(new InputStreamReader(
				System.in));

		int attempts = 0;
		String line = null;
		String[] supportedTypes = DataConverter.getSupportedTypes();
		String typeName = null;
		while (!DataConverter.hasConverterForURI(attribute) && attempts < 3) {
			System.out.println("Unknown attribute type \"" + attribute
					+ "\".\nPlease select a data type:");
			for (int i = 0; i < supportedTypes.length; ++i) {
				System.out.println(i + ") " + supportedTypes[i]);
			}
			try {
				line = userIn.readLine();
				int index = Integer.parseInt(line);
				if (index < 0 || index >= supportedTypes.length) {
					throw new NumberFormatException(
							"Selection is out of range.");
				}
				typeName = supportedTypes[index];
				DataConverter.putConverter(attribute, typeName);
			} catch (NumberFormatException nfe) {
				System.out.println("Invalid selection: \"" + line
						+ "\". Please make another selection.");
			} catch (IOException ioe) {
				System.out.println("Unable to read your selection. Aborting.");
				log.error("Unable to read data type selection.", ioe);
				return false;
			}
		}
		
		if(!DataConverter.hasConverterForURI(attribute)){
			return false;
		}

		System.out.println("Please enter a value for " + attribute + " as a String:");
		try {
			line = userIn.readLine();
		} catch (IOException e) {
			System.out.println("Unable to read your data. Aborting.");
			log.error("Unable to read attribute data.", e);
			return false;
		}

		byte[] data = DataConverter.encodeUri(attribute, line);

		if(data == null){
			return false;
		}
		
		boolean success = this.insertAttributeValue(uri, attribute, data);
		if (success) {
			System.out.println("Updated \"" + attribute + "\" with \"" + line
					+ "\".");
		} else {
			System.out.println("Unable to update world model. Reason unknown.");
		}
		return success;
	}

	private boolean insertAttributeValue(final String uri,
			final String attribute, final byte[] data) {
		if (uri == null) {
			System.out
					.println("Unable to create/update attribute for a null uri.");
			return false;
		}

		if (attribute == null) {
			System.out.println("Unable to create/update a null attribute.");
			return false;
		}

		TypeSpecification spec = new TypeSpecification();
		spec.setIsTransient(false);
		spec.setUriName(attribute);
		this.solverWorldModel.addSolutionType(spec);

		Solution soln = new Solution();
		soln.setTime(System.currentTimeMillis());
		soln.setAttributeName(attribute);
		soln.setTargetName(uri);
		soln.setData(data);

		return this.solverWorldModel.sendSolution(soln);
	}
}
