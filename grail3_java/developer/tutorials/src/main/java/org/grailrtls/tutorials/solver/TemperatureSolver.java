/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.tutorials.solver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

import org.grailrtls.libcommon.util.HashableByteArray;
import org.grailrtls.libsolver.SolverAggregatorConnection;
import org.grailrtls.libsolver.SolverAggregatorInterface;
import org.grailrtls.libsolver.listeners.ConnectionListener;
import org.grailrtls.libsolver.listeners.SampleListener;
import org.grailrtls.libsolver.protocol.messages.SampleMessage;
import org.grailrtls.libsolver.rules.SubscriptionRequestRule;
import org.grailrtls.libworldmodel.solver.SolverWorldConnection;
import org.grailrtls.libworldmodel.solver.SolverWorldModelInterface;
import org.grailrtls.libworldmodel.solver.WorldModelIoHandler;
import org.grailrtls.libworldmodel.solver.protocol.messages.DataTransferMessage.Solution;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage.TypeSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An example solver used as a tutorial, demonstrating the use of the
 * Aggregator-Solver protocol library (libsolver) and the Solver-World Model
 * protocol library (libworldmodel). This class is based on the C++ temperature
 * solver tutorial written by Ben Firner. A single class that analyzes packet
 * data from PIPSqueak devices and generates temperature solutions.
 * 
 * This example relies on libcommon for printing of device addresses, libsolver
 * to communicate with the Aggregator, and libworldmodel to communicate with
 * the World Model.
 * 
 * @author Robert Moore
 * 
 */
public class TemperatureSolver extends Thread{
	/**
	 * Logging facility for this class.
	 */
	private static final Logger log = LoggerFactory
			.getLogger(TemperatureSolver.class);

	/**
	 * Keep track of the solution URI name.
	 */
	private static final String SOLUTION_URI_NAME = "temperature";

	/**
	 * Region URI for this example.
	 */
	private static final String SOLUTION_REGION_URI = "Winlab";
	
	/**
	 * Origin string for this solver.
	 */
	private static final String ORIGIN_STRING = "grail.java.tutorial.temperature";

	/**
	 * Flag to keep running until the connection is closed.
	 */
	private boolean keepRunning = true;

	/**
	 * Interface to the aggregator, takes care of all the basic protocol stuff.
	 */
	private SolverAggregatorConnection aggregatorInterface = new SolverAggregatorConnection();

	/**
	 * Interface to the World Model for publishing solutions. Takes care of all
	 * the basic protocol stuff.  We can use this one since this solver won't produce transient data.
	 */
	private SolverWorldConnection worldModelInterface = new SolverWorldConnection();

	/**
	 * Used to get user input.
	 */
	private BufferedReader userInput = new BufferedReader(
			new InputStreamReader(System.in));

	/**
	 * Indicates whether we should use the simplest temperature solving
	 * algorithm, or a more complex one. The simple algorithm just copies the
	 * raw temperature data from the sample into the generated solution.
	 */
	private boolean useSimpleAlgorithm = false;

	/*
	 * Variables for smoother solution
	 */
	/**
	 * Each sensor's votes on the temperature since the last update
	 */
	private ConcurrentHashMap<HashableByteArray, ConcurrentHashMap<Integer, Integer>> temp_votes = new ConcurrentHashMap<HashableByteArray, ConcurrentHashMap<Integer, Integer>>();
	/**
	 * The number of times the transmitter has sent a temperature reading since
	 * the last update
	 */
	private ConcurrentHashMap<HashableByteArray, Integer> times_reported = new ConcurrentHashMap<HashableByteArray, Integer>();
	/**
	 * The last temperature reported by a sensor
	 */
	private ConcurrentHashMap<HashableByteArray, Integer> temp_reported = new ConcurrentHashMap<HashableByteArray, Integer>();
	/**
	 * When the last temperature solution was generated.
	 */
	private volatile long lastUpdate = 0l;
	/**
	 * How often to generate temperature solutions are generated
	 */
	private volatile long updateInterval = 10000l;

	/**
	 * Creates a new {@code TemperatureSolver} and starts it.
	 * 
	 * @param args
	 *            Aggregator host, aggregator port, distributor host, and
	 *            distributor port (solvers), in that order.
	 */
	public static void main(String[] args) {
		if (args.length < 4) {
			printUsageInfo();
			System.exit(1);
		}

		// Normally, catching the exception would be good, but this is just a
		// tutorial.
		int aggPort = Integer.parseInt(args[1]);
		int distPort = Integer.parseInt(args[3]);

		TemperatureSolver solver = new TemperatureSolver(args[0], aggPort,
				args[2], distPort);
		solver.start();
	}

	/**
	 * Prints a simple message about the parameters for this program.
	 */
	protected static void printUsageInfo() {
		System.out
				.println("Requires four parameters: <Aggregator host> <Aggregator port> <World Model host> <World Model port>.");
		System.out.println("Once running, type \"exit\" to exit.");
	}

	/**
	 * Creates a new {@code TemperatureSolver} that connects to an aggregator
	 * and world model at the specified host name/IP addresses and port numbers.
	 * 
	 * @param aggHost
	 *            the hostname or IP address of the aggregator.
	 * @param aggPort
	 *            the port number the aggregator is listening for solvers on.
	 * @param wmHost
	 *            the hostname or IP address of the world model.
	 * @param wmPort
	 *            the port number the world model is listening for solvers on.
	 */
	public TemperatureSolver(String aggHost, int aggPort, String wmHost,
			int wmPort) {
		this.aggregatorInterface.setHost(aggHost);
		this.aggregatorInterface.setPort(aggPort);

		this.worldModelInterface.setHost(wmHost);
		this.worldModelInterface.setPort(wmPort);
	}

	/**
	 * Connects to the aggregator and distributor. Checks for user input every
	 * 100ms, and exits if the aggregator or distributor connections are ended,
	 * or the user types "exit".
	 */
	@Override
	public void run() {

		/*
		 * Aggregator interface set-up.
		 */

		// Create a subscription rule to limit samples to 1 per 4 seconds for
		// each transmitter/receiver pair (simple), and only receive PIPSqueak
		// samples (both).
		SubscriptionRequestRule rule = SubscriptionRequestRule.generateGenericRule();
		if (this.useSimpleAlgorithm) {
			rule.setUpdateInterval(4000l);
		} else {
			rule.setUpdateInterval(0l);
		}
		rule.setPhysicalLayer((byte) 1);
		this.aggregatorInterface.addRule(rule);

		/*
		 * World Model interface set-up.
		 */

		TypeSpecification spec = new TypeSpecification();
		spec.setIsTransient(false);
		spec.setUriName(SOLUTION_URI_NAME);
		// Add the solution type URI name to the world model interface
		this.worldModelInterface.addSolutionType(spec);
		
		this.worldModelInterface.setOriginString(ORIGIN_STRING);

		/*
		 * Connect to the world model. Note that this is being done before the
		 * aggregator to simplify the sample-handling logic in this example
		 * class.
		 */
		if (!this.worldModelInterface.connect()) {
			System.out
					.println("Unable to establish a connection to the world model.");
			System.exit(1);
		}
		System.out.println("Connected to " + this.worldModelInterface);

		// Connect to the aggregator
		if (!this.aggregatorInterface.connect()) {
			System.out
					.println("Unable to establish a connection to the aggregator.");
			System.exit(1);
		}
		System.out.println("Connected to " + this.aggregatorInterface);

		// Read the user input into this string
		String command = null;

		// Keep running until either we get disconnected, or the user wants to
		// quit.
		while (this.keepRunning) {
		
			if(!this.aggregatorInterface.isConnectionLive()){
				this.keepRunning = false;
				break;
			}
			
			try {
				SampleMessage sample = this.aggregatorInterface.getNextSample();
				this.procesSample(sample);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(!this.aggregatorInterface.isConnectionLive() || !this.worldModelInterface.isConnectionLive())
			{
				this.keepRunning = false;
				break;
			}
			// Sleep for 100ms, since there isn't much else to do.
			try {
				Thread.sleep(100l);
			} catch (InterruptedException e) {
				// Ignored
			}
		}
		// Stop running
		this.aggregatorInterface.disconnect();
		this.worldModelInterface.disconnect();
		System.exit(0);
	}

	/**
	 * Sends sensed temperature to the distributor.
	 */
	public void procesSample(SampleMessage sample) {
		/*
		 * Quick and dirty way to check for temperature data is to see if it's
		 * 2-bytes long. Ideally, a temperature solver should contact the
		 * appropriate World Server to determine which device ID values are
		 * temperature sensors.
		 */
		if (sample.getSensedData() == null
				|| sample.getSensedData().length != 2) {
			return;
		}

		// Generate a temperature solution.
		Collection<Solution> solution = this.useSimpleAlgorithm ? this
				.generateSimpleSolution(sample) : this
				.generateBetterSolution(sample);

		// No solution generated so nothing to send.
		if (solution == null) {
			return;
		}

		// Send the solution to the world model
		if (!this.worldModelInterface.sendSolutions(
				solution)) {
			log.warn("Unable to send solution to distributor: {}", solution);
		}
	}

	/**
	 * Generates a very simple temperature solution based on the raw sensed
	 * data.
	 * 
	 * @param sample
	 * @return generated solutions, or {@code null} if none were generated.
	 */
	private Collection<Solution> generateSimpleSolution(SampleMessage sample) {
		Solution solution = new Solution();
		// PIPSqueak tags only have a 23-bit ID, so we can extract the last 3
		// bytes as an integer
		solution.setTargetName(Integer.toString(TemperatureSolver
				.getPipId(sample.getDeviceId())));

		// Set the solution URI name. The world model interface will translate
		// to the correct alias value.
		solution.setAttributeName(SOLUTION_URI_NAME);

		// Copy the temperature data from the PIPSqueak tag to the solution.
		solution.setData(sample.getSensedData());
		
		solution.setTime(System.currentTimeMillis());

		ArrayList<Solution> returnedList = new ArrayList<Solution>();
		returnedList.add(solution);
		log.info("Simple temperature: {}", solution);
		return returnedList;
	}

	/**
	 * Generates a more reasonable temperature solution only every 30 seconds,
	 * if the temperature has changed, and reduces some sampling noise.
	 * 
	 * @param sample
	 * @return generated solutions, or {@code null} if none were generated.
	 */
	private Collection<Solution> generateBetterSolution(SampleMessage sample) {
		long now = System.currentTimeMillis();

		// Generate a HashMap entry for the device ID (transmitter).
		HashableByteArray deviceId = new HashableByteArray(sample.getDeviceId());

		Integer temp = Integer.valueOf((sample.getSensedData()[0] << 8)
				+ (sample.getSensedData()[1] & 0xFF));

		// Update the number of times the sensor has reported temperatures
		Integer times_rptd = this.times_reported.get(deviceId);
		if (times_rptd == null) {
			times_rptd = Integer.valueOf(0);
		}
		this.times_reported.put(deviceId, Integer
				.valueOf(times_rptd.intValue() + 1));

		// Update the temperature votes for the sensor
		ConcurrentHashMap<Integer, Integer> tempsMap = this.temp_votes
				.get(deviceId);
		// Initialize the voting map if this is the first vote for this sensor
		if (tempsMap == null) {
			tempsMap = new ConcurrentHashMap<Integer, Integer>();
			this.temp_votes.put(deviceId, tempsMap);
		}

		// Initialize the votes if this is the first vote for this temperature.
		Integer numVotes = tempsMap.get(temp);
		if (numVotes == null) {
			numVotes = Integer.valueOf(0);
		}
		tempsMap.put(temp, Integer.valueOf(numVotes.intValue() + 1));

		// If it's not yet time to report, then stop here (finished update).
		if (now - this.lastUpdate < this.updateInterval) {
			return null;
		}
		this.lastUpdate = now;

		log.debug("Checking temperatures.");

		Collection<Solution> solutions = new ArrayList<Solution>();

		// Go through each device and check the votes
		for (HashableByteArray someDev : this.times_reported.keySet()) {
			Integer timesReported = this.times_reported.get(someDev);
			ConcurrentHashMap<Integer, Integer> theVotes = this.temp_votes
					.get(someDev);
			// Only calculate a vote if the device was seen at least ten times
			if (10 < timesReported.intValue() && theVotes != null
					&& theVotes.size() > 0) {
				// Grab the first element and assume that's the max-voted value
				Iterator<Integer> tempIter = theVotes.keySet().iterator();
				Integer new_temp = tempIter.next();
				Integer max_votes = theVotes.get(new_temp);
				// Check the remaining votes
				for (; tempIter.hasNext();) {
					Integer nextTemp = tempIter.next();
					Integer nextVotes = theVotes.get(nextTemp);
					// Update the highest-voted value if found
					if (nextVotes.intValue() > max_votes.intValue()) {
						new_temp = nextTemp;
						max_votes = nextVotes;
					}
				}

				Integer reportedTemp = this.temp_reported.get(someDev);
				// Update if this is the first time, or if the value has changed
				if (reportedTemp == null
						|| (reportedTemp.intValue() != new_temp.intValue())) {
					Solution solution = new Solution();
					solution.setTargetName(Integer.toString(TemperatureSolver
							.getPipId(someDev.getData())));
					byte[] solutionData = { (byte) (new_temp.intValue() >> 8),
							(byte) (new_temp.intValue() & 0xFF) };
					
					
					solution.setAttributeName(SOLUTION_URI_NAME);

					// Copy the temperature data from the PIPSqueak tag to the solution.
					solution.setData(solutionData);
					
					solution.setTime(System.currentTimeMillis());
					
					
					log.info("Generated {}", solution);
					solutions.add(solution);
					this.temp_reported.put(someDev, new_temp);
				}
			}
		}
		return solutions.size() > 0 ? solutions : null;
	}

	/**
	 * Returns an integer representation of a 23-bit PIPSqueak device ID.
	 * 
	 * @param deviceId
	 * @return the 23-bit PIPSqueak device ID as a 32-bit integer
	 */
	private static int getPipId(byte[] deviceId) {
		// PIPSqueak tags only have a 23-bit ID, so we can extract the last 3
		// bytes as an integer
		return (deviceId[SampleMessage.DEVICE_ID_SIZE - 3] << 16)
				+ (deviceId[SampleMessage.DEVICE_ID_SIZE - 2] << 8)
				+ deviceId[SampleMessage.DEVICE_ID_SIZE - 1];
	}

}
