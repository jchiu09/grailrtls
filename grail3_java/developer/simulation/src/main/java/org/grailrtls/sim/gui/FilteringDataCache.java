/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package org.grailrtls.sim.gui;

import java.awt.geom.Point2D;
import java.util.LinkedList;
import java.util.List;

import org.apache.mina.util.ConcurrentHashSet;
import org.grailrtls.libcommon.util.HashableByteArray;
import org.grailrtls.libsolver.protocol.messages.SampleMessage;
import org.grailrtls.sim.gui.structs.SignalToDistanceItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FilteringDataCache extends DataCache {
	private static final Logger log = LoggerFactory.getLogger(FilteringDataCache.class);
	
	protected final ConcurrentHashSet<HashableByteArray> allowedDevices = new ConcurrentHashSet<HashableByteArray>();
	
	public FilteringDataCache(){
		super();
	}
	
	public void addAllowedDevice(HashableByteArray deviceId){
		this.allowedDevices.add(deviceId);
	}
	
	public void removeAllowedDevice(HashableByteArray deviceId){
		this.allowedDevices.remove(deviceId);
	}
	
	@Override
	public float getCurrentRssi(final HashableByteArray transmitter, final HashableByteArray receiver){
		if(this.allowedDevices.size() == 0){
			return super.getCurrentRssi(transmitter, receiver);
		}
		if(this.allowedDevices.contains(receiver) && this.allowedDevices.contains(transmitter)){
			return super.getCurrentRssi(transmitter, receiver);
		}
		return Float.NaN;
	}
	
	@Override
	public float getCurrentVariance(final HashableByteArray transmitter, final HashableByteArray receiver){
		if(this.allowedDevices.size() == 0){
			return super.getCurrentVariance(transmitter, receiver);
		}
		if(this.allowedDevices.contains(receiver) && this.allowedDevices.contains(transmitter)){
			return super.getCurrentVariance(transmitter, receiver);
		}
		return Float.NaN;
	}
	
	@Override
	public List<SampleMessage> getSampleList(
			final HashableByteArray receiverId,
			final HashableByteArray transmitterId) {
		if(this.allowedDevices.size() == 0){
			return super.getSampleList(receiverId, transmitterId);
		}
		if(this.allowedDevices.contains(receiverId) && this.allowedDevices.contains(transmitterId)){
			return super.getSampleList(receiverId, transmitterId);
		}
		return null;
	}
	
	@Override
	public List<SampleMessage> getVarianceSampleList(
			final HashableByteArray receiverId,
			final HashableByteArray transmitterId) {
		if(this.allowedDevices.size() == 0){
			return super.getVarianceSampleList(receiverId, transmitterId);
		}
		if(this.allowedDevices.contains(receiverId) && this.allowedDevices.contains(transmitterId)){
			return super.getVarianceSampleList(receiverId, transmitterId);
		}
		return null;
	}
	
	@Override
	public List<SignalToDistanceItem> getSignalToDistance(
			final HashableByteArray receiverId) {
		if(this.allowedDevices.size() == 0){
			return super.getSignalToDistance(receiverId);
		}
		if(this.allowedDevices.contains(receiverId)){
			return super.getSignalToDistance(receiverId);
		}
		return null;
	}
	
	@Override
	public Point2D getDeviceLocation(final HashableByteArray deviceId) {
		if(this.allowedDevices.size() == 0){
			return super.getDeviceLocation(deviceId);
		}
		if(this.allowedDevices.contains(deviceId)){
			return super.getDeviceLocation(deviceId);
		}
		return null;
	}
	
	public List<HashableByteArray> getAllowedDevices(){
		LinkedList<HashableByteArray> returnedList = new LinkedList<HashableByteArray>();
		returnedList.addAll(this.allowedDevices);
		return returnedList;
	}
	
	@Override
	public void clearCachedData(){
		super.clearCachedData();
		
		this.allowedDevices.clear();
	}
	
	public void clearAll(){
		super.clearAll();
		this.clearCachedData();
	}
	
	protected void overlay(FilteringDataCache clone){
		clone.clearAll();
		super.overlay(clone);
		clone.allowedDevices.addAll(this.allowedDevices);
	}
	
	public FilteringDataCache clone(){
		FilteringDataCache returnedCache = new FilteringDataCache();
		this.overlay(returnedCache);
		return returnedCache;
	}
}
