/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.sim.gui.panels;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.swing.JPanel;

import org.grailrtls.libcommon.util.HashableByteArray;
import org.grailrtls.libsolver.protocol.messages.SampleMessage;
import org.grailrtls.sim.gui.structs.RSSILine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SignalLineMap extends JPanel {

	private static final Logger log = LoggerFactory
			.getLogger(SignalLineMap.class);
	
	protected Rectangle2D regionBounds;

		protected ConcurrentHashMap<HashableByteArray, ConcurrentHashMap<HashableByteArray, RSSILine>> linesReceiverTransmitter = new ConcurrentHashMap<HashableByteArray, ConcurrentHashMap<HashableByteArray,RSSILine>>();
		protected ConcurrentLinkedQueue<RSSILine> currentLines = new ConcurrentLinkedQueue<RSSILine>();
		
		protected final ConcurrentHashMap<HashableByteArray, Point2D> receiverLocations = new ConcurrentHashMap<HashableByteArray, Point2D>();
		protected final ConcurrentHashMap<HashableByteArray,Point2D> transmitterLocations = new ConcurrentHashMap<HashableByteArray, Point2D>();
		

	protected BufferedImage backgroundImage = null;
	
	protected boolean enableAlpha = false;
	
	protected long lastRepaint = System.currentTimeMillis();
	
	protected long minFps = 15;

	protected float currFps = 30;

	protected int slowFrames = 0;
	
	protected float minValue = 0f;
	
	protected float maxValue = 10f;
	
	protected long maxAge = 3000l;
	
	public float getMinValue() {
		return minValue;
	}

	public void setMinValue(float minValue) {
		this.minValue = minValue;
	}

	public BufferedImage getBackgroundImage() {
		return backgroundImage;
	}

	public void setBackgroundImage(BufferedImage backgroundImage) {
		this.backgroundImage = backgroundImage;
	}

	public SignalLineMap() {
		super();
	}

	@Override
	public String getToolTipText(MouseEvent me) {

		return "TODO: Tool tips.";

	}
	
//	public String 

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.lastRepaint = System.currentTimeMillis();
		Graphics2D g2 = (Graphics2D) g;
		
		
		g2.setColor(Color.BLACK);
		int screenWidth = this.getWidth();
		int screenHeight = this.getHeight();
		

		if(this.backgroundImage == null)
		{
			g2.fillRect(0, 0, screenWidth, screenHeight);
		}
		else
		{
			g2.drawImage(this.backgroundImage, 0, 0, screenWidth, screenHeight, 0, 0, this.backgroundImage.getWidth(), this.backgroundImage.getHeight(), null);
		}

		if(this.regionBounds == null){
			return;
		}

		float regionWidth = (float)this.regionBounds.getWidth();
		float regionHeight = (float)this.regionBounds.getHeight();
	

		double xScale = screenWidth / regionWidth;
		double yScale = screenHeight / regionHeight;
		
		float valueRange = this.maxValue - this.minValue;

		Color origColor = g2.getColor();
		Composite origComposite = g2.getComposite();		
		

		Color drawColor = Color.GREEN;
		
		long oldestTime = this.lastRepaint - this.maxAge;
		
		for(RSSILine line : this.currentLines){
			if(line.getValue() < this.minValue){
				continue;
			}
			if(line.getUpdateTime() < oldestTime)
			{
				continue;
			}
			Point2D receiverLocation = this.receiverLocations.get(line.getReceiver());
			if(receiverLocation == null)
			{
				continue;
			}
			Point2D transmitterLocation = this.transmitterLocations.get(line.getTransmitter());
			if(transmitterLocation == null)
			{
				continue;
			}
			
			// Random coloring if nothing is set
			float adjusted = 0.9f * (line.getValue() - this.minValue)/valueRange;
			float hue = adjusted;
			float sat = 0.95f;
			float bright = 0.95f;
			drawColor = new Color(Color.HSBtoRGB(hue, sat, bright));
			g2.setColor(drawColor);
			
			adjusted += 0.25f;
			if(adjusted > 1)
			{
				adjusted = 1f;
			}
			if(this.enableAlpha){
				g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, adjusted));
			}
			
			g2.drawLine((int)(receiverLocation.getX()*xScale), screenHeight - (int)(receiverLocation.getY()*yScale), (int)(transmitterLocation.getX()*xScale), screenHeight - (int)(transmitterLocation.getY()*yScale));
		}
	/*
		if(this.lines != null)
		{
			Collection<RSSILine> currLines = this.lines;
			for(RSSILine line : currLines){
				float alpha = line.getValue() / 10f;
				if(alpha > 1)
				{
					alpha = 1f;
				}
				if(this.enableAlpha){
					g2.setComposite(this.makeComposite(alpha));
				}
				g2.drawLine((int)(line.getLine().x1*xScale), (int)(screenHeight - line.getLine().y1*yScale), (int)(line.getLine().x2*xScale), (int)(screenHeight - line.getLine().y2*yScale));
			}
		}
		*/
		g2.setComposite(origComposite);
		
		g2.setColor(origColor);
		
		long renderTime = System.currentTimeMillis() - this.lastRepaint;
		this.currFps = this.currFps * 0.875f + (1000f / renderTime)*0.125f;

		if (this.enableAlpha && (this.currFps < this.minFps * 0.9f)) {
			++this.slowFrames;
			if (this.slowFrames > 3) {
				this.enableAlpha = false;
				log.warn("FPS: {} Disabling Alpha Tranparency.", this.currFps);
			}
		} else if (this.enableAlpha) {
			this.slowFrames = 0;
		}

		log.debug("Rendered in {}ms.", renderTime);

	}

	private AlphaComposite makeComposite(float alpha) {
		int type = AlphaComposite.SRC_OVER;
		return (AlphaComposite.getInstance(type, alpha));
	}

	public Rectangle2D getRegionBounds() {
		return regionBounds;
	}

	public void setRegionBounds(Rectangle2D regionBounds) {
		this.regionBounds = regionBounds;
	}

	public boolean isEnableAlpha() {
		return enableAlpha;
	}

	public void setEnableAlpha(boolean enableAlpha) {
		this.enableAlpha = enableAlpha;
	}

	public long getMinFps() {
		return minFps;
	}

	public void setMinFps(long minFps) {
		this.minFps = minFps;
	}
	
	public void setTransmitterLocation(HashableByteArray deviceId, float x, float y){
		Point2D.Float newPoint = new Point2D.Float(x,y);
		this.transmitterLocations.put(deviceId, newPoint);
	}
	
	public void setReceiverLocation(HashableByteArray receiverId, float x, float y){
		Point2D.Float newPoint = new Point2D.Float(x, y);
		this.receiverLocations.put(receiverId, newPoint);
	}
	
	public void addValue(final byte[] receiverId, final byte[] deviceId, final float value)
	{
		if(receiverId == null)
		{
			return;
		}
		if(deviceId == null)
		{
			return;
		}
		HashableByteArray recHash = new HashableByteArray(receiverId);
		HashableByteArray devHash = new HashableByteArray(deviceId);
		
		ConcurrentHashMap<HashableByteArray, RSSILine> recvLines = this.linesReceiverTransmitter.get(recHash);
		if(recvLines == null)
		{
			recvLines = new ConcurrentHashMap<HashableByteArray, RSSILine>();
			this.linesReceiverTransmitter.put(recHash, recvLines);
		}
		
		RSSILine line = recvLines.get(devHash);
		if(line == null)
		{
			line = new RSSILine();
			recvLines.put(devHash, line);
			line.setReceiver(recHash);
			line.setTransmitter(devHash);
			this.currentLines.add(line);
		}
		line.setValue(value);
	}

	public float getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(float maxValue) {
		this.maxValue = maxValue;
	}
}
