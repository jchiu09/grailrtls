/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.sim;

import java.text.DecimalFormat;

import org.grailrtls.libsolver.SolverAggregatorInterface;
import org.grailrtls.libsolver.listeners.ConnectionListener;
import org.grailrtls.libsolver.listeners.SampleListener;
import org.grailrtls.libsolver.protocol.messages.SampleMessage;
import org.grailrtls.libsolver.rules.SubscriptionRequestRule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A fake solver that simply prints received samples and some statistics about the sample rate from the aggregator.
 * Most useful for testing/debugging purposes, but may be used as a base class for other solvers.
 * 
 * @author Robert Moore
 *
 */
public class SlowSolver implements ConnectionListener, SampleListener {

	/**
	 * Logging facility for this class.
	 */
	public static final Logger log = LoggerFactory.getLogger(SlowSolver.class);

	/**
	 * The host name/IP address of the aggregator.
	 */
	protected String host = null;
	
	/**
	 * The port number of the aggregator.
	 */
	protected int port = -1;
	
	/**
	 * Interface to the aggregator.  Handles all the network protocol stuff for us.
	 */
	protected SolverAggregatorInterface aggregatorInterface = new SolverAggregatorInterface();

	/**
	 * Parses the command-line parameters and creates a new fake solver.
	 * @param args
	 */
	public static void main(String[] args) {
		log.debug("FakeSolver starting up.");

		String aggHost = "localhost";
		int aggPort = 7007;

		if (args.length < 2) {
			printUsageInfo();
			System.exit(1);
		}

		aggHost = args[0];
		aggPort = Integer.parseInt(args[1]);
		
		SlowSolver solver = new SlowSolver();
		solver.setHost(aggHost);
		solver.setPort(aggPort);
		
		solver.start();
		
	}
	
	/**
	 * Prints out information about the command-line parameters and their use.
	 */
	protected static void printUsageInfo() {
		StringBuffer sb = new StringBuffer();

		sb.append("Parameters: <host> <port>");
		
		System.err.println(sb.toString());
	}

	/**
	 * Called when a sample is received from the aggregator.  Computes and logs statistics every 10 seconds.
	 */
	public void sampleReceived(SolverAggregatorInterface aggregator,
			SampleMessage sampleMessage) {
		long now = System.currentTimeMillis();
		log.debug("Received sample {}, {}ms delay.", sampleMessage,
				Long.valueOf((now - sampleMessage.getReceiverTimeStamp())));
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

	/**
	 * Logs a warning message.  The solver will exit once the connection to the aggregator is closed.
	 */
	@Override
	public void connectionEnded(SolverAggregatorInterface aggregator) {
		log.warn("Lost connection to {}.",aggregator);
	}

	/**
	 * Logs a message about the connection opening.
	 */
	@Override
	public void connectionEstablished(SolverAggregatorInterface aggregator) {
		log.info("Connected to {}.",aggregator);
	}

	/**
	 * Logs a message about the connection being interrupted.
	 */
	@Override
	public void connectionInterrupted(SolverAggregatorInterface aggregator) {
		log.info("Temporarily lost connection to {}.",aggregator);
	}

	/**
	 * Starts the aggregator interface.
	 */
	public void start() {
		if(this.host == null || this.port < 0)
		{
			log.warn("Missing one or more required parameters.");
			return;
		}

		this.aggregatorInterface.setHost(this.host);
		this.aggregatorInterface.setPort(this.port);
		this.aggregatorInterface.addConnectionListener(this);
		this.aggregatorInterface.addSampleListener(this);
		
		log.debug("Attempting connection to {}:{}", this.host, Integer.valueOf(this.port));
		
		if(!this.aggregatorInterface.doConnectionSetup())
		{
			log.warn("Aggregator connection failed.");
			return;
		}
	}

	/**
	 * Sets the host/IP address for the aggregator interface.
	 * @param host the host name or IP address of the aggregator.
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * Sets the port number for the aggregator interface.
	 * @param port the port number of the aggregator.
	 */
	public void setPort(int port) {
		this.port = port;
	}

	public SolverAggregatorInterface getAggregatorInterface() {
		return aggregatorInterface;
	}
}
