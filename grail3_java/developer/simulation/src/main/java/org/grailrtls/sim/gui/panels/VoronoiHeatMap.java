/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.sim.gui.panels;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JPanel;

import org.grailrtls.libcommon.util.HashableByteArray;
import org.grailrtls.libcommon.util.NumericUtils;
import org.grailrtls.sim.gui.DataCache;
import org.grailrtls.sim.gui.DisplayPanel;
import org.grailrtls.sim.gui.DataCache.ValueType;
import org.grailrtls.sim.gui.structs.Item2DPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import delaunay.Pnt;
import delaunay.Triangle;
import delaunay.Triangulation;

public class VoronoiHeatMap extends JPanel implements DisplayPanel{

	private static final Logger log = LoggerFactory
			.getLogger(VoronoiHeatMap.class);

	protected long maxAge = 3000;

	private static int initialSize = 10000; // Size of initial triangle

	protected HashableByteArray displayedId = null;

	public HashableByteArray getDisplayedId() {
		return displayedId;
	}

	public void setDisplayedId(HashableByteArray displayedId) {
		this.displayedId = displayedId;
	}

	protected boolean deviceIsTransmitter = false;

	protected ValueType type;

	protected Triangle initialTriangle = null;

	protected float minValue = -100f;

	protected float maxValue = -30f;

	protected BufferedImage deviceImage;

	protected long lastRepaint = System.currentTimeMillis();

	protected boolean enableAlpha = true;

	protected float minFps = 8;

	protected float currFps = 30;

	protected int slowFrames = 0;

	protected final DataCache cache;

	public VoronoiHeatMap(final ValueType type, final DataCache cache) {
		super();
		this.cache = cache;
		this.type = type;
		initialTriangle = new Triangle(new Pnt(-initialSize, -initialSize),
				new Pnt(initialSize, -initialSize), new Pnt(0, initialSize));
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		this.lastRepaint = System.currentTimeMillis();
		Graphics2D g2 = (Graphics2D) g;
		
		if(this.displayedId == null || this.cache.getRegionBounds() == null){
			return;
		}

		int screenWidth = this.getWidth();
		int screenHeight = this.getHeight();

		float xScale = screenWidth / (float) this.cache.getRegionBounds().getWidth();
		float yScale = screenHeight / (float) this.cache.getRegionBounds().getHeight();

		float valueRange = this.maxValue - this.minValue;
		Ellipse2D drawPoint = null;

		Color origColor = g2.getColor();
		Composite origComposite = g2.getComposite();

		g2.setColor(Color.BLACK);
		BufferedImage regionImage = this.cache.getRegionImage();
		if (regionImage == null) {
			g2.fillRect(0, 0, screenWidth, screenHeight);
		} else {
			g2.drawImage(regionImage, 0, 0, screenWidth, screenHeight,
					0, 0, regionImage.getWidth(), regionImage
							.getHeight(), null);
		}

		// Draw first if Alpha is enabled as we should still be able to see it
		// With no alpha, draw it last (see below).
		if (this.enableAlpha) {
			this.drawDeviceIcon(g2, screenWidth, screenHeight);
		}

		Composite fillVoronoiComposite = origComposite;

		// g2.setColor(Color.RED);
		if (this.enableAlpha) {
			fillVoronoiComposite = AlphaComposite.getInstance(
					AlphaComposite.SRC_OVER, 0.7f);
		}

		Triangulation dt = new Triangulation(this.initialTriangle);

		HashSet<Point2D> voronoiPoints = new HashSet<Point2D>();
		HashMap<Pnt, Item2DPoint> pntToItem = new HashMap<Pnt, Item2DPoint>();
		long oldestItem = this.lastRepaint - this.maxAge;

		Map<HashableByteArray, Item2DPoint> heatPoints = this
				.generateDisplayedValues();

		for (HashableByteArray hash : heatPoints.keySet()) {
			Item2DPoint point = heatPoints.get(hash);
			if (point == null) {
				continue;
			}
			if (point.getUpdateTime() < oldestItem) {
				point.setValue(this.minValue-1);
				point.setUpdateTime(this.lastRepaint);
			}

			voronoiPoints.add(point.getPoint());
			Pnt newPnt = new Pnt(point.getPoint().getX() * xScale, screenHeight
					- point.getPoint().getY() * yScale);
			pntToItem.put(newPnt, point);
			dt.delaunayPlace(newPnt);
		}

		for (Point2D point : voronoiPoints) {
			Pnt newPnt = new Pnt(point.getX() * xScale, screenHeight
					- point.getY() * yScale);
			dt.delaunayPlace(newPnt);
		}

		// Keep track of sites done; no drawing for initial triangles sites
		HashSet<Pnt> done = new HashSet<Pnt>(initialTriangle);
		for (Triangle triangle : dt) {
			for (Pnt site : triangle) {
				if (done.contains(site))
					continue;
				done.add(site);
				List<Triangle> list = dt.surroundingTriangles(site, triangle);
				Pnt[] vertices = new Pnt[list.size()];
				int i = 0;
				for (Triangle tri : list)
					vertices[i++] = tri.getCircumcenter();
				Item2DPoint item = pntToItem.get(site);
				// if (item.getValue() < this.minValue) {
				// continue;
				// }

				float normalRssi = (float) ((Math.abs(item.getValue()
						- this.minValue)) / valueRange);
				if (normalRssi < 0) {
					normalRssi = 0;
				} else if (normalRssi > 1.0) {
					normalRssi = 1.0f;
				}
				// if (normalRssi < 0.01f) {
				// continue;
				// }

				if (item.getValue() >= this.minValue || normalRssi < 0.01f) {
					this.draw(g, vertices, Color.getHSBColor(normalRssi * .66f,
							0.9f, 0.9f), fillVoronoiComposite);
				} else {
					this.draw(g, vertices, null, null);
				}
				// Draw the point
				this.draw(g, site);
			}
		}

		// Draw icon last if no alpha transparency is used
		if (!this.enableAlpha) {
			this.drawDeviceIcon(g2, screenWidth, screenHeight);
		}

		g2.setComposite(origComposite);
		g2.setColor(origColor);

		long renderTime = System.currentTimeMillis() - this.lastRepaint;
		this.currFps = this.currFps * 0.875f + (1000f / renderTime) * 0.125f;

		if (this.enableAlpha && (this.currFps < this.minFps * 0.9f)) {
			++this.slowFrames;
			if (this.slowFrames > 3) {
				this.enableAlpha = false;
				log.warn("FPS: {} Disabling Alpha Tranparency.", this.currFps);
			}
		} else if (this.enableAlpha) {
			this.slowFrames = 0;
		}
	}

	protected Map<HashableByteArray, Item2DPoint> generateDisplayedValues() {
		if (this.displayedId == null) {
			return null;
		}
		TreeMap<HashableByteArray, Item2DPoint> returnedMap = new TreeMap<HashableByteArray, Item2DPoint>();
		ValueType currType = this.type;
		List<HashableByteArray> deviceList = this.deviceIsTransmitter ? this.cache
				.getReceiverIds()
				: this.cache.getFiduciaryTransmitterIds();

		for (HashableByteArray device : deviceList) {
			if (currType == ValueType.RSSI) {
				float value = this.deviceIsTransmitter ? this.cache
						.getCurrentRssi(this.displayedId, device)
						: this.cache.getCurrentRssi(device,
								this.displayedId);
				Point2D location = this.cache.getDeviceLocation(device);
				if (location == null) {
					continue;
				}
				Item2DPoint newItem = new Item2DPoint((float) location.getX(),
						(float) location.getY(), value);
				returnedMap.put(device, newItem);
			} else if (currType == ValueType.VARIANCE) {
				float value = this.deviceIsTransmitter ? this.cache
						.getCurrentVariance(this.displayedId, device)
						: this.cache.getCurrentVariance(device,
								this.displayedId);
				Point2D location = this.cache.getDeviceLocation(device);
				if (location == null) {
					continue;
				}
				Item2DPoint newItem = new Item2DPoint((float) location.getX(),
						(float) location.getY(), value);
				returnedMap.put(device, newItem);
			}
		}
		return returnedMap;
	}

	protected void drawDeviceIcon(final Graphics g, final int screenWidth,
			final int screenHeight) {
		if (this.deviceImage == null || this.displayedId == null)
			return;
		
		Point2D deviceLocation = this.cache.getDeviceLocation(this.displayedId);
		if(deviceLocation == null){
			return;
		}

		Graphics2D g2 = (Graphics2D) g;
		int imageWidth = this.deviceImage.getWidth();
		int imageHeight = this.deviceImage.getHeight();

		float xScale = screenWidth / (float) this.cache.getRegionBounds().getWidth();
		float yScale = screenHeight / (float) this.cache.getRegionBounds().getHeight();

		g2
				.drawImage(
						this.deviceImage,
						(int) (deviceLocation.getX() * xScale - (imageWidth / 2f)),
						(int) (screenHeight
								- (deviceLocation.getY() * yScale) - (imageHeight / 2f)),
						(int) (deviceLocation.getX() * xScale + (imageWidth / 2f)),
						(int) (screenHeight
								- (deviceLocation.getY() * yScale) + (imageHeight / 2f)),
						0, 0, imageWidth, imageHeight, null);
	}

	/**
	 * Draw a point.
	 * 
	 * @param point
	 *            the Pnt to draw
	 */
	public void draw(Graphics g, Pnt point) {
		int r = 1;
		int x = (int) point.coord(0);
		int y = (int) point.coord(1);
		g.fillOval(x - r, y - r, r + r, r + r);
	}

	/**
	 * Draw a circle.
	 * 
	 * @param center
	 *            the center of the circle
	 * @param radius
	 *            the circle's radius
	 * @param fillColor
	 *            null implies no fill
	 */
	public void draw(Graphics g, Pnt center, double radius, Color fillColor) {
		int x = (int) center.coord(0);
		int y = (int) center.coord(1);
		int r = (int) radius;
		if (fillColor != null) {
			Color temp = g.getColor();
			g.setColor(fillColor);
			// g.fillOval(x - r, y - r, r + r, r + r);
			g.setColor(temp);
		}
		g.drawOval(x - r, y - r, r + r, r + r);
	}

	/**
	 * Draw a polygon.
	 * 
	 * @param polygon
	 *            an array of polygon vertices
	 * @param fillColor
	 *            null implies no fill
	 */
	public void draw(final Graphics g, Pnt[] polygon, Color fillColor,
			Composite fillComposite) {
		Graphics2D g2 = (Graphics2D) g;
		int[] x = new int[polygon.length];
		int[] y = new int[polygon.length];
		for (int i = 0; i < polygon.length; i++) {
			x[i] = (int) polygon[i].coord(0);
			y[i] = (int) polygon[i].coord(1);
		}
		if (fillColor != null) {
			Color temp = g.getColor();
			g.setColor(fillColor);
			Composite origComposite = g2.getComposite();
			g2.setComposite(fillComposite);

			g.fillPolygon(x, y, polygon.length);
			g.setColor(temp);
			g2.setComposite(origComposite);
		}
		g.drawPolygon(x, y, polygon.length);
	}

	public float getMinValue() {
		return minValue;
	}

	public void setMinValue(float minValue) {
		this.minValue = minValue;
	}

	public float getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(float maxValue) {
		this.maxValue = maxValue;
	}

	public void clear() {
		this.displayedId = null;
	}

	public boolean isEnableAlpha() {
		return enableAlpha;
	}

	public void setEnableAlpha(boolean enableAlpha) {
		this.enableAlpha = enableAlpha;
	}

	public float getMinFps() {
		return minFps;
	}

	public void setMinFps(float minFps) {
		this.minFps = minFps;
	}

	public long getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(long maxAge) {
		this.maxAge = maxAge;
	}

	public boolean isDeviceTransmitter() {
		return deviceIsTransmitter;
	}

	public void setDeviceIsTransmitter(boolean deviceIsTransmitter) {
		this.deviceIsTransmitter = deviceIsTransmitter;
	}

	@Override
	public void setSelfAdjustMax(boolean selfAdjustMax) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setSelfAdjustMin(boolean selfAdjustMin) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setDisplayLegend(boolean displayLegend) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setRenderFill(boolean renderFill) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setDeviceIcon(BufferedImage icon) {
		this.deviceImage = icon;
	}
}
