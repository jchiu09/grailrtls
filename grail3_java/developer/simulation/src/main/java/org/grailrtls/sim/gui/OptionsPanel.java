/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.sim.gui;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.print.attribute.standard.JobName;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.grailrtls.libsolver.SolverAggregatorInterface;
import org.grailrtls.libworldmodel.client.ClientWorldModelInterface;

public class OptionsPanel extends JPanel  implements ActionListener{

	protected JTextField aggregatorHost = new JTextField("localhost");
	protected JTextField aggregatorPort = new JTextField("7007");
	protected JCheckBox aggregtorConnect = new JCheckBox("Connect");

	protected JTextField worldServerHost = new JTextField("localhost");
	protected JTextField worldServerPort = new JTextField("7011");
	protected JCheckBox worldServerConnect = new JCheckBox("Connect");
	
	protected SolverAggregatorInterface aggregatorInterface = null;
	
	protected ClientWorldModelInterface worldServerInterface = null;

	public OptionsPanel() {
		super();
		
		this.aggregtorConnect.addActionListener(this);
		this.worldServerConnect.addActionListener(this);

		this.setLayout(new GridLayout(3, 1));
		
		JPanel flowPanel = new JPanel();
		
		flowPanel.setLayout(new FlowLayout());
		flowPanel.add(new JLabel("Aggregator"));
		flowPanel.add(this.aggregatorHost);
		flowPanel.add(this.aggregatorPort);
		flowPanel.add(this.aggregtorConnect);
		this.add(flowPanel);
		
		flowPanel = new JPanel();
		flowPanel.setLayout(new FlowLayout());
		flowPanel.add(new JLabel("World Server"));
		flowPanel.add(this.worldServerHost);
		flowPanel.add(this.worldServerPort);
		flowPanel.add(this.worldServerConnect);
		
		
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource() == this.worldServerConnect){
			
		}else if(arg0.getSource() == this.aggregtorConnect){
			
		}
	}

	public SolverAggregatorInterface getAggregatorInterface() {
		return aggregatorInterface;
	}

	public void setAggregatorInterface(SolverAggregatorInterface aggregatorInterface) {
		this.aggregatorInterface = aggregatorInterface;
	}

	public ClientWorldModelInterface getWorldServerInterface() {
		return worldServerInterface;
	}

	public void setWorldServerInterface(ClientWorldModelInterface worldServerInterface) {
		this.worldServerInterface = worldServerInterface;
	}

}
