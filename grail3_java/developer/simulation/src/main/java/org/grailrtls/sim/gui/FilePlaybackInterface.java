/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.sim.gui;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.grailrtls.libsensor.SensorAggregatorInterface;
import org.grailrtls.libsolver.listeners.SampleListener;
import org.grailrtls.libsolver.protocol.messages.SampleMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A simple utility that replays sample traces recorded by the
 * {@link RecordingSolver}. Useful for testing new solver algorithms or when
 * live data is unavailable.
 * 
 * @author Robert Moore
 * 
 */
public class FilePlaybackInterface implements Runnable {

	/**
	 * Logging facility for this class.
	 */
	public static final Logger log = LoggerFactory
			.getLogger(FilePlaybackInterface.class);

	/**
	 * The input stream for the replay file.
	 */
	protected DataInputStream inputStream = null;
	
	/**
	 * The file being played.
	 */
	protected RandomAccessFile playbackFile = null; 

	/**
	 * Flag to keep playing back the sample trace.
	 */
	protected boolean keepRunning = true;

	/**
	 * Flag that indicates the aggregator is ready to receive samples from this
	 * sensor.
	 */
	protected boolean canSendSamples = false;

	/**
	 * The name of the file containing the recorded samples.
	 */
	private String replayFileName;

	/**
	 * How quickly or slowly to playback the samples. File timestamps are
	 * interepreted after being multiplied by this factor.
	 */
	private float playbackSpeed = 1.0f;
	
	/**
	 * Whether the Replay Sensor should update sample timestamps to the current time.
	 */
	private boolean updateTimestamps = false;
	
	/**
	 * Queue of interfaces that should be sent the samples.
	 */
	protected ConcurrentLinkedQueue<SampleListener> listeners = new ConcurrentLinkedQueue<SampleListener>();
	
	/**
	 * Sets the name of the replay file.
	 * 
	 * @param replayFileName
	 *            the name of the replay file.
	 */
	public void setReplayFileName(String replayFileName) {
		this.replayFileName = replayFileName;
	}

	/**
	 * Prepares the input stream for reading from the replay file.
	 * 
	 * @param inFileName
	 *            the name of the replay file to open.
	 * @return {@code true} if the stream was created successfully, else {@code
	 *         false}.
	 */
	private boolean prepareInputFile(String inFileName) {
		File inFile = new File(inFileName);

		if (!inFile.exists()) {
			log.error("File does not exist: {}.", inFileName);
			return false;
		}

		if (!inFile.canRead()) {
			log.error("Cannot read from {}.", inFile);
			return false;
		}

		try {
			this.inputStream = new DataInputStream(new FileInputStream(inFile));
		} catch (FileNotFoundException e) {
			log.error("Could not find {} to create input stream.", inFileName);
			return false;
		}

		return true;
	}

	/**
	 * Sends the provided sample message to the aggregator if everything is
	 * prepared correctly.
	 * 
	 * @param sampleMessage
	 *            the sample message to send.
	 */
	public void sendSampleMessage(SampleMessage sampleMessage) {
		
		for(SampleListener listener : this.listeners){
			listener.sampleReceived(null, sampleMessage);
		}
		
		log.debug("Sent {}.", sampleMessage);

	}

	/**
	 * Parses the next sample message from the replay file.
	 * 
	 * @return the next sample message in the replay file, or {@code null} if
	 *         there are no remaining samples.
	 */
	public SampleMessage getNextMessage() {
		SampleMessage message = new SampleMessage();

		if (this.inputStream != null) {
			try {
				// This code is largely copied from
				// org.grailrtls.sensor.prtocol.codecs.SampleDecoder
				int messageLength = this.inputStream.readInt();
				message.setPhysicalLayer(this.inputStream.readByte());
				--messageLength;
				byte[] deviceId = new byte[SampleMessage.DEVICE_ID_SIZE];
				byte[] receiverId = new byte[SampleMessage.DEVICE_ID_SIZE];
				int read = 0;
				while (read < deviceId.length) {
					int tmp = this.inputStream.read(deviceId, read,
							deviceId.length - read);
					if (tmp < 0) {
						log
								.error("Unable to read more bytes from input file (device id).");
						return null;
					}
					read += tmp;
				}
				message.setDeviceId(deviceId);
				messageLength -= deviceId.length;
				read = 0;
				while (read < receiverId.length) {
					int tmp = this.inputStream.read(receiverId, read,
							receiverId.length - read);
					if (tmp < 0) {
						log
								.error("Unable to read more bytes from input file (receiver id).");
						return null;
					}
					read += tmp;
				}
				message.setReceiverId(receiverId);
				messageLength -= receiverId.length;

				message.setReceiverTimeStamp(this.inputStream.readLong());
				messageLength -= 8;

				message.setRssi(this.inputStream.readFloat());
				messageLength -= 4;

				if (messageLength > 0) {
					byte[] data = new byte[messageLength];
					read = 0;
					while (read < data.length) {
						int tmp = this.inputStream.read(data, read, data.length
								- read);
						if (tmp < 0) {
							log
									.error("Unable to read more bytes from input file (data).");
							return null;
						}
						read += tmp;
					}
					message.setSensedData(data);
				}

			} catch (IOException ioe) {
				log
						.error(
								"Caught IO Exception while reading from input file: {}",
								ioe);
				return null;
			}

		}

		return message;
	}

	/**
	 * Timestamp of when replay of the file was started. Used to replay the
	 * samples at approximately the same rate that they were originally sent.
	 */
	private volatile long replayStart = Long.MIN_VALUE;

	/**
	 * Connects to the aggregator and begins sending samples as soon as the
	 * aggregator is ready to receive them.
	 */
	public void run() {

		while (this.keepRunning) {

			if (!this.canSendSamples) {
				try {
					log.debug("Not ready to send samples.  Waiting 100ms.");
					Thread.sleep(100);
				} catch (InterruptedException ie) {
					// Ignored
				}
				continue;
			}

			try {
				if (this.inputStream.available() <= 1) {
					log.info("Input file has no data remaining.");
				}
			} catch (IOException e) {
				log.error("Unable to checking remaining bytes in input file.");
			}

			long now = System.currentTimeMillis();
			long realTimeIndex = now - this.replayStart;
			long nextSampleIndex = 0l;

			try {
				nextSampleIndex = this.inputStream.readLong();
			} catch (IOException ioe) {
				log.error("Unable to retrieve next timestamp.");
			}

			long simTimeIndex = (long) ((float) realTimeIndex * this.playbackSpeed);

			// If we're more than 10ms ahead, then perform a sleep.
			if ((simTimeIndex + 10) < nextSampleIndex) {
				long sleepDelay = nextSampleIndex - simTimeIndex;
				sleepDelay /= this.playbackSpeed;
				try {
					log.debug("Sleeping {}ms.", sleepDelay);
					Thread.sleep(sleepDelay);
				} catch (InterruptedException ie) {
					log.info("Unable to sleep full {}ms.", sleepDelay);
				}
			}

			SampleMessage message = getNextMessage();
			if(message == null)
			{
				log.warn("Reached the end of the replay file.");
			}
			if(this.updateTimestamps)
			{
				message.setReceiverTimeStamp(System.currentTimeMillis());
			}

			this.sendSampleMessage(message);
		}
	}

	/**
	 * Skips ahead by {@code skipSample} number of samples in the file. For instance, if currently
	 * the file is positioned to read sample {@code n}, and skip(5) is called, the file will
	 * be ready to read sample {@code n+6).  {@code skip(0)} skips the current sample.
	 * @param skipMessages the number of messages to skip.
	 * @throws IOException 
	 */
	public void skip(final int skipSamples) throws IOException{
		for(int i = 0; i < skipSamples; ++i){
			this.inputStream.readLong();
			int messageLength = this.inputStream.readInt();
			this.inputStream.skip(messageLength);
		}
	}

	public float getPlaybackSpeed() {
		return playbackSpeed;
	}

	public void setPlaybackSpeed(float playbackSpeed) {
		this.playbackSpeed = playbackSpeed;
	}

	public boolean isUpdateTimestamps() {
		return updateTimestamps;
	}

	public void setUpdateTimestamps(boolean updateTimestamps) {
		this.updateTimestamps = updateTimestamps;
	}
	
	public void addSampleListener(final SampleListener listener){
		this.listeners.add(listener);
	}
	
	public void removeSampleListener(final SampleListener listener){
		this.listeners.remove(listener);
	}

	public RandomAccessFile getPlaybackFile() {
		return this.playbackFile;
	}

	public void setPlaybackFile(RandomAccessFile playbackFile) {
		this.playbackFile = playbackFile;
	}
}
