/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.sim;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.grailrtls.libsolver.SolverAggregatorInterface;
import org.grailrtls.libsolver.listeners.ConnectionListener;
import org.grailrtls.libsolver.listeners.SampleListener;
import org.grailrtls.libsolver.protocol.messages.SampleMessage;
import org.grailrtls.libsolver.protocol.messages.Transmitter;
import org.grailrtls.libsolver.rules.SubscriptionRequestRule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.XStream;

/**
 * A fake solver that simply prints received samples and some statistics about
 * the sample rate from the aggregator. Most useful for testing/debugging
 * purposes, but may be used as a base class for other solvers.
 * 
 * @author Robert Moore
 * 
 */
public class FakeSolver implements ConnectionListener, SampleListener {

	/**
	 * Logging facility for this class.
	 */
	public static final Logger log = LoggerFactory.getLogger(FakeSolver.class);

	/**
	 * Formatter for printing floating point numbers with 4 decimal places of
	 * precision.
	 */
	private static final DecimalFormat fourPlacesFormat = new DecimalFormat(
			"###,##0.0000");

	/**
	 * Number of samples received since the statistics were last generated.
	 */
	private int samplesReceived = 0;

	/**
	 * Mean latency of samples received from the aggregator. This value assumes
	 * that the sensors are sending valid timestamps to the aggregator.
	 */
	private float meanReceiveLatency = 0;

	/**
	 * The last time that statistics were generated.
	 */
	private long lastReportTime = System.currentTimeMillis();

	/**
	 * Number of bytes read from the aggregator since the last time statistics
	 * were generated.
	 */
	private long bytesRead = 0;

	/**
	 * Number of bytes written to the aggregator since the last time statistics
	 * were generated.
	 */
	private long bytesWritten = 0;

	/**
	 * The host name/IP address of the aggregator.
	 */
	protected String host = null;

	/**
	 * The port number of the aggregator.
	 */
	protected int port = -1;

	/**
	 * Interface to the aggregator. Handles all the network protocol stuff for
	 * us.
	 */
	protected SolverAggregatorInterface aggregatorInterface = new SolverAggregatorInterface();
	
	protected final SolverConfiguration config;

	/**
	 * Parses the command-line parameters and creates a new fake solver.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		log.debug("FakeSolver starting up.");

		SolverConfiguration config = new SolverConfiguration();
		List<SubscriptionRequestRule> rules = new ArrayList<SubscriptionRequestRule>();
		SubscriptionRequestRule rule = new SubscriptionRequestRule();
		rule.setPhysicalLayer((byte) 1);
		rule.setUpdateInterval(10);
		Transmitter txer = new Transmitter();
		txer.setBaseId(new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				00, 00 });
		txer.setMask(new byte[] { (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
				(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
				(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
				(byte) 0xFF, (byte) 0xFF, (byte) 0x00, (byte) 0x00, (byte) 0x00 });
		rule.setTransmitters(new Transmitter[] { txer });
		rules.add(rule);
		config.setRules(rules);
		XStream xstream = new XStream();
		xstream.alias("rule", SubscriptionRequestRule.class);
		xstream.alias("transmitter", Transmitter.class);
		xstream.alias("solverConfig", SolverConfiguration.class);
		System.out.println(xstream.toXML(config));

		String aggHost = "localhost";
		int aggPort = 7007;

		if (args.length < 2) {
			printUsageInfo();
			System.exit(1);
		}

		aggHost = args[0];
		aggPort = Integer.parseInt(args[1]);

		FakeSolver solver = new FakeSolver(config);
		solver.setHost(aggHost);
		solver.setPort(aggPort);

		solver.start();

	}
	
	public FakeSolver(final SolverConfiguration config){
		this.config = config;
	}

	/**
	 * Prints out information about the command-line parameters and their use.
	 */
	protected static void printUsageInfo() {
		StringBuffer sb = new StringBuffer();

		sb.append("Parameters: <host> <port>");

		System.err.println(sb.toString());
	}

	/**
	 * Called when a sample is received from the aggregator. Computes and logs
	 * statistics every 10 seconds.
	 */
	public void sampleReceived(SolverAggregatorInterface aggregator,
			SampleMessage sampleMessage) {

		++this.samplesReceived;

		long now = System.currentTimeMillis();

		this.meanReceiveLatency = (this.meanReceiveLatency * ((this.samplesReceived - 1f) / this.samplesReceived))
				+ ((now - sampleMessage.getReceiverTimeStamp()) * (1f / this.samplesReceived));

		if (now - this.lastReportTime > 10000) {
			this.printStatistics();
		}

		log.debug("Received sample {}, {}ms delay.", sampleMessage,
				Long.valueOf((now - sampleMessage.getReceiverTimeStamp())));

	}

	/**
	 * Logs network statistics.
	 */
	protected void printStatistics() {
		long now = System.currentTimeMillis();

		log.info(
				"Received {} S/s over last {}ms.",
				fourPlacesFormat.format(this.samplesReceived
						/ ((now - this.lastReportTime) / 1000f)),
				Long.valueOf(now - this.lastReportTime));
		log.info("Mean receive latency: {}ms.",
				Float.valueOf(this.meanReceiveLatency));
		long newBytesRead = this.aggregatorInterface.getSession()
				.getReadBytes();
		long newBytesWritten = this.aggregatorInterface.getSession()
				.getWrittenBytes();
		log.info("Received {} B/s and wrote {} B/s.", fourPlacesFormat
				.format((double) (newBytesRead - this.bytesRead)
						/ ((now - this.lastReportTime) / 1000f)),
				fourPlacesFormat
						.format((double) (newBytesWritten - this.bytesWritten)
								/ ((now - this.lastReportTime) / 1000f)));
		double avgSampleSize = (double) (newBytesRead - this.bytesRead)
				/ this.samplesReceived;
		double overhead = avgSampleSize == 0 ? 1.0
				: (5.0 * this.samplesReceived)
						/ (newBytesRead - this.bytesRead);
		log.info("Average sample size is {} B ({}% overhead)\n",
				fourPlacesFormat.format(avgSampleSize),
				fourPlacesFormat.format(overhead * 100));
		this.bytesRead = newBytesRead;
		this.bytesWritten = newBytesWritten;
		this.samplesReceived = 0;
		this.meanReceiveLatency = 0;
		this.lastReportTime = now;
	}

	/**
	 * Logs a warning message. The solver will exit once the connection to the
	 * aggregator is closed.
	 */
	@Override
	public void connectionEnded(SolverAggregatorInterface aggregator) {
		log.warn("Lost connection to {}.", aggregator);
	}

	/**
	 * Logs a message about the connection opening.
	 */
	@Override
	public void connectionEstablished(SolverAggregatorInterface aggregator) {
		log.info("Connected to {}.", aggregator);
	}

	/**
	 * Logs a message about the connection being interrupted.
	 */
	@Override
	public void connectionInterrupted(SolverAggregatorInterface aggregator) {
		log.info("Temporarily lost connection to {}.", aggregator);
	}

	/**
	 * Starts the aggregator interface.
	 */
	public void start() {
		if (this.host == null || this.port < 0) {
			log.warn("Missing one or more required parameters.");
			return;
		}

		this.aggregatorInterface.setHost(this.host);
		this.aggregatorInterface.setPort(this.port);
		this.aggregatorInterface.addConnectionListener(this);
		this.aggregatorInterface.addSampleListener(this);
		this.aggregatorInterface.setRules(this.config.getRules().toArray(new SubscriptionRequestRule[]{}));

		log.debug("Attempting connection to {}:{}", this.host,
				Integer.valueOf(this.port));

		if (!this.aggregatorInterface.doConnectionSetup()) {
			log.warn("Aggregator connection failed.");
			return;
		}
	}

	/**
	 * Sets the host/IP address for the aggregator interface.
	 * 
	 * @param host
	 *            the host name or IP address of the aggregator.
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * Sets the port number for the aggregator interface.
	 * 
	 * @param port
	 *            the port number of the aggregator.
	 */
	public void setPort(int port) {
		this.port = port;
	}

	public SolverAggregatorInterface getAggregatorInterface() {
		return aggregatorInterface;
	}
}
