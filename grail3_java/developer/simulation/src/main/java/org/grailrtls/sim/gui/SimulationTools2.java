/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package org.grailrtls.sim.gui;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;

import org.grailrtls.libcommon.util.FieldDecoder;
import org.grailrtls.libcommon.util.HashableByteArray;
import org.grailrtls.libsolver.SolverAggregatorInterface;
import org.grailrtls.libsolver.listeners.SampleListener;
import org.grailrtls.libsolver.protocol.messages.SampleMessage;
import org.grailrtls.libworldmodel.client.ClientWorldModelInterface;
import org.grailrtls.libworldmodel.client.listeners.DataListener;
import org.grailrtls.libworldmodel.client.protocol.messages.AbstractRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.Attribute;
import org.grailrtls.libworldmodel.client.protocol.messages.AttributeAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.DataResponseMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginPreferenceMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.SnapshotRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.URISearchResponseMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimulationTools2 implements DataListener, SampleListener {

	public static void main(String[] args) {
		if (args.length != 5) {
			printUsageInfo();
			System.exit(1);
		}
		SimulationTools2 tools = new SimulationTools2(args[4]);
		if (args.length >= 4) {
			try {
				int aggPort = Integer.parseInt(args[1]);
				int worldPort = Integer.parseInt(args[3]);
				tools.setSolverAggregatorHost(args[0]);
				tools.setSolverAggregatorPort(aggPort);
				tools.setWorldServerHost(args[2]);
				tools.setWorldServerPort(worldPort);
			} catch (NumberFormatException nfe) {
				System.err.println("One or more options were invalid.");
				printUsageInfo();
				System.exit(1);
			}

			if (args.length > 4) {
				for (int i = 4; i < args.length; ++i) {
					String arg = args[i];
					if (args.equals("--fps")) {
						if (args.length < ++i) {
							log.error("No valid fps value specified.");
							return;
						}
						try {
							float fps = Float.parseFloat(args[++i]);
							tools.setDesiredFps((int) fps);
						} catch (NumberFormatException nfe) {
							log.error("Unable to parse fps value {}", args[i],
									nfe);
						}
					}
				}
			}
		}

		log.info("Starting up...");
		tools.initConnections();
		tools.connectToWorldModel();
		// Wait 100 ms
		try {
			Thread.sleep(100);
		} catch (InterruptedException ie) {
			// ignored
		}
		tools.connectToAggregator();
	}

	protected static void printUsageInfo() {
		System.out
				.println("Parameters: <Aggregator Host> <Aggregator Port> <World Model Host> <World Model Client Port> <Region Name>");
	}

	private static final Logger log = LoggerFactory
			.getLogger(SimulationTools2.class);

	protected static final int DEFAULT_FPS = 10;

	protected SolverAggregatorInterface solverAggregatorInterface = new SolverAggregatorInterface();

	protected ClientWorldModelInterface clientWorldModelInterface = new ClientWorldModelInterface();

	protected final FilteringDataCache cache = new FilteringDataCache();

	protected String solverAggregatorHost = "localhost";
	protected int solverAggregatorPort = 7008;

	protected String worldServerHost = "localhost";
	protected int worldServerPort = 7011;

	protected int desiredFps = DEFAULT_FPS;

	public SimulationTools2(final String regionUri) {
		super();

		this.clientWorldModelInterface.addDataListener(this);

		this.solverAggregatorInterface.addSampleListener(this);

		this.cache.setRegionUri(regionUri);
		// 5 minutes
		this.cache.setMaxCacheAge(1000*60*10);
		// Shorter variance seems better for plotting
		this.cache.setMaxVarianceAge(3500);
		
		this.cache.setMaxVarianceHistory(4);
		
		SimpleFrame initialFrame = new SimpleFrame(this.cache);
		initialFrame.configureDisplay();
	}

	protected void initConnections() {
		this.solverAggregatorInterface.setHost(this.solverAggregatorHost);
		this.solverAggregatorInterface.setPort(this.solverAggregatorPort);
		this.solverAggregatorInterface.setStayConnected(true);

		this.clientWorldModelInterface.setHost(this.worldServerHost);
		this.clientWorldModelInterface.setPort(this.worldServerPort);
		this.clientWorldModelInterface.setStayConnected(true);

		this.clientWorldModelInterface.registerSearchRequest("*.receiver.*");
		this.clientWorldModelInterface.registerSearchRequest("*.transmitter.*");
		this.clientWorldModelInterface.registerSearchRequest(this.cache
				.getRegionUri());
	}
	
	public void doShutdown(){
		this.solverAggregatorInterface.doConnectionTearDown();
		this.clientWorldModelInterface.doConnectionTearDown();
		// TODO: Allow graceful exit
		log.warn("Forcing exit.");
		System.exit(1);
	}

	public void connectToAggregator() {
		this.solverAggregatorInterface.doConnectionSetup();
	}

	public void connectToWorldModel() {
		this.clientWorldModelInterface.doConnectionSetup();
	}

	public String getSolverAggregatorHost() {
		return solverAggregatorHost;
	}

	public void setSolverAggregatorHost(String solverAggregatorHost) {
		this.solverAggregatorHost = solverAggregatorHost;
	}

	public int getSolverAggregatorPort() {
		return solverAggregatorPort;
	}

	public void setSolverAggregatorPort(int solverAggregatorPort) {
		this.solverAggregatorPort = solverAggregatorPort;
	}

	public String getWorldServerHost() {
		return worldServerHost;
	}

	public void setWorldServerHost(String worldServerHost) {
		this.worldServerHost = worldServerHost;
	}

	public int getWorldServerPort() {
		return worldServerPort;
	}

	public void setWorldServerPort(int worldServerPort) {
		this.worldServerPort = worldServerPort;
	}

	public int getDesiredFps() {
		return desiredFps;
	}

	public void setDesiredFps(int desiredFps) {
		this.desiredFps = desiredFps;
	}

	@Override
	public void sampleReceived(SolverAggregatorInterface aggregator,
			SampleMessage sample) {
		this.cache.addSample(sample);

	}

	public FilteringDataCache getCache() {
		return cache;
	}

	@Override
	public void requestCompleted(ClientWorldModelInterface worldModel,
			AbstractRequestMessage message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dataResponseReceived(ClientWorldModelInterface worldModel,
			DataResponseMessage message) {
		if (message.getUri().equals(this.cache.getRegionUri())) {
			double width = -1f;
			double height = -1f;
			String mapURL = null;
			for (Attribute field : message.getAttributes()) {
				if ("width".equals(field.getAttributeName())) {
					width = (Double) FieldDecoder.decodeField("double", field.getData());
				} else if ("height".equals(field.getAttributeName())) {
					height = (Double) FieldDecoder.decodeField("double", field.getData());
				} else if ("map url".equals(field.getAttributeName())) {
					mapURL = (String) FieldDecoder.decodeField("string", field.getData());
					continue;
				}
			}
			if (width > 0 && height > 0) {
				Rectangle2D bounds = new Rectangle2D.Float(0, 0, (float) width,
						(float) height);
				this.cache.setRegionBounds(bounds);
			}

			if (mapURL != null) {
				BufferedImage regionImage;
				try {
					
					if(!mapURL.startsWith("http://")){
						mapURL = "http://" + mapURL;
					}
					regionImage = ImageIO.read(new URL(mapURL));
					this.cache.setRegionImage(regionImage);
				} catch (MalformedURLException e) {
					log.warn("Malformed URL: {}", mapURL);
					e.printStackTrace();
				} catch (IOException e) {
				log.warn("Unable to load region image URL {} due to an exception.", mapURL, e);
					e.printStackTrace();
				}
			}

		} else if (message.getUri().contains(
				".receiver.") || message.getUri().contains(".transmitter.")) {
			
			if(message.getAttributes() == null){
				return;
			}
			byte[] deviceId = null;
			double x = -1;
			double y = -1;

			for (Attribute field : message.getAttributes()) {
				if ("location.x".equals(field.getAttributeName())) {
					x = (Double) FieldDecoder.decodeField("double",
							field.getData());
				} else if ("location.y".equals(field.getAttributeName())) {
					y = (Double) FieldDecoder.decodeField("double",
							field.getData());
				} else if ("id".equals(field.getAttributeName())) {
					deviceId = new byte[16];
					if (field.getData() != null && field.getData().length <= 16) {
						System.arraycopy(field.getData(), 0, deviceId, 16 - field
								.getData().length, field.getData().length);
					} else {
						log.warn("Invalid \"id\" attribute for {}.",
								message.getUri());
					}

				}
			}
			if (x > 0 && y > 0) {
				HashableByteArray deviceHash = new HashableByteArray(deviceId);
				Point2D location = new Point2D.Double(x, y);
				if(message.getUri().indexOf(".receiver.") != -1){
					this.cache.addReceiver(deviceHash);
				}else if(message.getUri().indexOf(".transmitter.") != -1){
					this.cache.addFiduciaryTransmitter(deviceHash);
				}
				this.cache.setDeviceLocation(deviceHash, location);
			}

		}
	}

	@Override
	public void uriSearchResponseReceived(ClientWorldModelInterface worldModel,
			URISearchResponseMessage message) {
		if (message.getMatchingUris() == null) {
			return;
		}
		for (String uri : message.getMatchingUris()) {
			SnapshotRequestMessage request = new SnapshotRequestMessage();
			request.setBeginTimestamp(0l);
			request.setEndTimestamp(System.currentTimeMillis());
			request.setQueryURI(uri);
			request.setQueryAttributes(new String[] { "*" });

			this.clientWorldModelInterface.sendMessage(request);
		}
	}

	@Override
	public void attributeAliasesReceived(
			ClientWorldModelInterface clientWorldModelInterface,
			AttributeAliasMessage message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void originAliasesReceived(
			ClientWorldModelInterface clientWorldModelInterface,
			OriginAliasMessage message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void originPreferenceSent(ClientWorldModelInterface worldModel,
			OriginPreferenceMessage message) {
		// TODO Auto-generated method stub
		
	}
	
	
}
