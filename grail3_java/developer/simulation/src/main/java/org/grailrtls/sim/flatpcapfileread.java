/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and John-Austen Francisco
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package org.grailrtls.sim;


import java.lang.Thread;
import java.lang.Long;
import java.lang.String;
import java.lang.Boolean;

import java.util.Date;
import java.util.concurrent.TimeoutException;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.DataInputStream;
import java.io.File;

import java.nio.ByteBuffer;



public class flatpcapfileread
{

private final static String NON_SWAPPED_MAGIC = "a1b2c3d4";
private final static String SWAPPED_MAGIC = "d4c3b2a1";
private static boolean BYTES_SWAPPED = false;

private final long totalfilelength;
private final File filetoread;
private DataInputStream datain;



private long magicNumber;
private int majorVersion;
private int minorVersion;
private long GMTcorrection;
private long timestampAcc;
private long snaplenInOct;
private long linklayerHeaderType;




private long packetTimestampSeconds;
private long packetTimestampMillis;

//private long timestampforrealsies = (timestampseconds*1000)+(timestampmillis/1000);
//System.out.printf("   Translated Timestamp: %s \n", checkfile.timestampToSense(timestampforrealsies));

private long packetBytesCaptured; //in octets... bleargh
private long packetBytesOnWire;




private long differentialTimestampBase = -1;





public flatpcapfileread(final String filename) throws Exception
{
	if( (filename == null) || ("".equals(filename.trim())) )
	{
		throw new IllegalArgumentException("Cannot use a null file or a file with no name.");
	}

	this.filetoread = new File(filename);
	this.totalfilelength = this.filetoread.length();
	this.datain = new DataInputStream( new BufferedInputStream( new FileInputStream(this.filetoread), 65535) );

//long magicnumber = ;

//true is swapped
this.BYTES_SWAPPED = checkordering(readUnsignedInt(0, 10));


}


//public int readSignedInt(int skipbytes, int sleeptimemillis, boolean swapbyteorder) throws Exception
public int readSignedInt(int skipbytes, int sleeptimemillis) throws Exception
{
	//return Long.valueOf(readUnsignedInt(skipbytes, sleeptimemillis, swapbyteorder)).intValue() ;
	return Long.valueOf(readUnsignedInt(skipbytes, sleeptimemillis)).intValue() ;

}


//public long readUnsignedInt(int skipbytes, int sleeptimemillis, boolean swapbyteorder) throws Exception
public long readUnsignedInt(int skipbytes, int sleeptimemillis) throws Exception
{
	//DataInputStream datain = null;
	//datain = new DataInputStream( new BufferedInputStream( new FileInputStream(this.filetoread), 65535) );

	int available = 0;
	int remaining = skipbytes;

	available = this.datain.available();

	while( (available > 0) && (remaining > 0) )
	{
		if( available > remaining )
		{
			available = remaining;
		}
		this.datain.skipBytes(available);
		remaining += -available;
		available = this.datain.available();
	}

	int sleepytime = 0;

	while( (this.datain.available() < 4) && (sleepytime < 4) )
	{
		Thread.sleep(sleeptimemillis);
		sleepytime += 1;
	}

	if(sleepytime >= 4)
	{
		throw new TimeoutException("Data stream did not have enough available bytes ready to convert to an unsigned int");
	}
	else
	{
		int byte0 = (0x000000FF & ((int)this.datain.readByte()));
		int byte1 = (0x000000FF & ((int)this.datain.readByte()));
		int byte2 = (0x000000FF & ((int)this.datain.readByte()));
		int byte3 = (0x000000FF & ((int)this.datain.readByte()));

		//if( !swapbyteorder )
		if( !BYTES_SWAPPED )
		{
			return ((long) (byte0 << 24 | byte1 << 16 | byte2 << 8 | byte3)) & 0xFFFFFFFFL;
		}
		else
		{
			return ((long) (byte3 << 24 | byte2 << 16 | byte1 << 8 | byte0)) & 0xFFFFFFFFL;
		}
	}

	//return 0;
}


//public int readUnsignedShort(int skipbytes, int sleeptimemillis, boolean swapbyteorder) throws Exception
public int readUnsignedShort(int skipbytes, int sleeptimemillis) throws Exception
{
	//DataInputStream datain = null;
	//datain = new DataInputStream( new BufferedInputStream( new FileInputStream(this.filetoread), 65535) );

	int available = 0;
	int remaining = skipbytes;

	available = this.datain.available();

	while( (available > 0) && (remaining > 0) )
	{
		if( available > remaining )
		{
			available = remaining;
		}
		this.datain.skipBytes(available);
		remaining += -available;
		available = this.datain.available();
	}

	int sleepytime = 0;

	while( (this.datain.available() < 4) && (sleepytime < 4) )
	{
		Thread.sleep(sleeptimemillis);
		sleepytime += 1;
	}

	if(sleepytime >= 4)
	{
		throw new TimeoutException("Data stream did not have enough available bytes ready to convert to an unsigned int");
	}
	else
	{
		int byte0 = (0x000000FF & ((int)this.datain.readByte()));
		int byte1 = (0x000000FF & ((int)this.datain.readByte()));


		if( !BYTES_SWAPPED )
		{
			return ((int) (byte0 << 8 | byte1)); //& 0x0000FFFF;
		}
		else
		{
			return ((int) (byte1 << 8 | byte0)); //& 0x0000FFFF;
		}
	}

	//return 0;
}


public byte[] fetchPacketBytes(int skipbytes, int readbytes) throws Exception
{
	int available = 0;
	int remaining = skipbytes;

	available = this.datain.available();

	while( (available > 0) && (remaining > 0) )
	{
		if( available > remaining )
		{
			available = remaining;
		}
		this.datain.skipBytes(available);
		remaining += -available;
		available = this.datain.available();
	}


	remaining = readbytes;
	ByteBuffer catchbytes = ByteBuffer.allocate(remaining);

	while( (available > 0) && (remaining > 0) )
	{
		catchbytes.put(datain.readByte());
		//System.out.printf("%x",datain.readByte());
		remaining += -1;
		available = this.datain.available();
	}

	return catchbytes.array();
}


public byte[] fetchCurrentPacketBytes() throws Exception
{
	int readvalue = Integer.parseInt( (new Long(this.packetBytesCaptured)).toString() );

	return fetchPacketBytes(0, readvalue);
}


public int available() throws Exception
{
	return this.datain.available();
}


public void close() throws Exception
{
	this.datain.close();
}


public void dumpHexBlock(int skipbytes, int readbytes) throws Exception
{
	int available = 0;
	int remaining = skipbytes;

	available = this.datain.available();

	while( (available > 0) && (remaining > 0) )
	{
		if( available > remaining )
		{
			available = remaining;
		}
		this.datain.skipBytes(available);
		remaining += -available;
		available = this.datain.available();
	}


	remaining = readbytes;

	while( (available > 0) && (remaining > 0) )
	{
		System.out.printf("%x",datain.readByte());
		remaining += -1;
		available = this.datain.available();
	}
}


public void dumpCurrentPacketAsHex() throws Exception
{
	int readvalue = Integer.parseInt( (new Long(this.packetBytesCaptured)).toString() );

	dumpHexBlock(0, readvalue);
}


public int getRadioTapRSSI(byte[] sourcebytes)
{
	//byte 22 is the RSSI value... I know this because of magic
	//and with 000000FF to pad out the byte into int size
	//XOR with FF to invert the bits then add one to un-2s-complement it
	return ((((int)sourcebytes[22]) & 0x000000FF ) ^ 0x000000FF)+1;
}


public byte[] get80211FrameRxTx(byte[] sourcebytes) throws Exception
{
	int byte0 = (0x000000FF & ((int)sourcebytes[26]));
	int byte1 = (0x000000FF & ((int)sourcebytes[27]));
	int framecontrol = 0;

	byte[] addresses = new byte[12];


	if( !BYTES_SWAPPED )
	{
		framecontrol = ((int) (byte0 << 8 | byte1)); //& 0x0000FFFF;
	}
	else
	{
		framecontrol = ((int) (byte1 << 8 | byte0)); //& 0x0000FFFF;
	}

//System.out.printf("wtf: %x\n", framecontrol);

System.arraycopy(sourcebytes, 36, addresses, 0, 6);
System.arraycopy(sourcebytes, 30, addresses, 6, 6);

/*
	if( (framecontrol & 0x00000208) == 0x00000208 )
	{
//System.out.println("D B S");
		//System.arraycopy(sourcebytes, 41, addresses, 0, 6);
		//System.arraycopy(sourcebytes, 29, addresses, 6, 6);

		System.arraycopy(sourcebytes, 42, addresses, 0, 6);
		System.arraycopy(sourcebytes, 30, addresses, 6, 6);


int count = 0;

while( count < 12 )
{
	System.out.printf("%x:", addresses[count]);
	count += 1;
}

System.out.println();


		return addresses;
	}
	else if( (framecontrol & 0x00000108) == 0x00000108 )
	{
//System.out.println("B S D");
		System.arraycopy(sourcebytes, 36, addresses, 0, 12);

int count = 0;

while( count < 12 )
{
	System.out.printf("%x:", addresses[count]);
	count += 1;
}

System.out.println();


		return addresses;
	}
	else
	{
System.out.printf("wtf: %x\n", framecontrol);
		throw new IllegalArgumentException("Unexpected frame control value: "+framecontrol);
	}
*/

	return addresses;

}


public long getPcapEpochTimestamp()
{	
	return (this.packetTimestampSeconds*1000)+(this.packetTimestampMillis/1000);
}


public long getPcapAccumulativeTimestamp()
{
	return getPcapEpochTimestamp() - differentialTimestampBase;
}


public boolean checkordering(long magicnumber) throws Exception
{
	if( (Long.toString(magicnumber, 16).toLowerCase()).equals(NON_SWAPPED_MAGIC) )
	{
		return false;
	}
	else if( (Long.toString(magicnumber, 16).toLowerCase()).equals(SWAPPED_MAGIC) )
	{
		return true;
	}
	else
	{
		throw new IllegalArgumentException("Magic number to verify PCAP byte ordering does not match the swapped or unswapped value.");
	}
}


public long getMagicNumber()
{
	if(BYTES_SWAPPED)
	{
		return Long.parseLong(SWAPPED_MAGIC, 16);
	}
	else
	{
		return Long.parseLong(NON_SWAPPED_MAGIC, 16);
	}
}


public String timestampToSense( long timestamp )
{
	return (new Date( timestamp )).toString();
}


public void readGlobalHeader() throws Exception
{
this.magicNumber = this.getMagicNumber();
this.majorVersion = this.readUnsignedShort(0, 10);
this.minorVersion = this.readUnsignedShort(0, 10);
this.GMTcorrection = this.readUnsignedInt(0, 10);
this.timestampAcc = this.readUnsignedInt(0, 10);
this.snaplenInOct = this.readUnsignedInt(0, 10);
this.linklayerHeaderType = this.readUnsignedInt(0, 10);
}


public void printGlobalHeader()
{

System.out.printf( "Magic Number: %X \n", this.magicNumber );
System.out.printf( "Major Version: %d \n", this.majorVersion );
System.out.printf( "Minor Version: %d \n", this.minorVersion );
System.out.printf( "GMT to Local Correction: %d \n", this.GMTcorrection );
System.out.printf( "Timestamp Accuracy : %d \n", this.timestampAcc );
System.out.printf( "Snapshot length (in octets): %o \n", this.snaplenInOct );
System.out.printf( "Link-Layer Header Type: %d \n", this.linklayerHeaderType );
}


public void readNextPacketHeader() throws Exception
{
this.packetTimestampSeconds = this.readUnsignedInt(0,10);
this.packetTimestampMillis = this.readUnsignedInt(0,10);

this.packetBytesCaptured = this.readUnsignedInt(0,10);  //in octets... bleargh
this.packetBytesOnWire = this.readUnsignedInt(0,10);

	if(differentialTimestampBase == -1)
	{
		differentialTimestampBase = getPcapEpochTimestamp();
	}
}


public void printPacketHeader() throws Exception
{
System.out.printf("Timestamp Seconds: %d\n", this.packetTimestampSeconds);
System.out.printf("Timestamp Milliseconds: %d\n", this.packetTimestampMillis);

long timestampforrealsies = (this.packetTimestampSeconds*1000)+(this.packetTimestampMillis/1000);
System.out.printf("   Translated Timestamp: %s \n", this.timestampToSense(timestampforrealsies));

System.out.printf("Bytes Captured (in octets) %d\n", this.packetBytesCaptured);
System.out.printf("Bytes on wire: %d\n", this.packetBytesOnWire);
}


public void skipCurrentPacket() throws Exception
{
	//Integer.parseInt( (new Long(this.packetBytesCaptured)).toString() )
	this.datain.skipBytes( Integer.parseInt( (new Long(this.packetBytesCaptured)).toString() ));
}



public static void main(String[] args)
{
//standin hardcode pthbbt
//"./wiresharktest.pcap";
String filename = args[0];
flatpcapfileread checkfile = null;

try
{
	checkfile = new flatpcapfileread(filename);
}
catch(Exception e)
{
	System.err.println("Fatal error checking file "+filename+" for readability");
	System.exit(1);
}


//checkfile.skipthroughfile();

try{


checkfile.readGlobalHeader();
checkfile.printGlobalHeader();


int count = 10;
byte[] currentdata;

while( count > 0 )
{



checkfile.readNextPacketHeader();

System.out.println();
checkfile.printPacketHeader();

System.out.println();

//checkfile.skipCurrentPacket();
//checkfile.dumpCurrentPacketAsHex();
//checkfile.fetchCurrentPacketBytes();
//System.out.println( checkfile.fetchCurrentPacketBytes().length );

byte[] currentpacket = checkfile.fetchCurrentPacketBytes();

//checkfile.getRadioTapRSSI(currentpacket);
//checkfile.get80211FrameRxTx(currentpacket);
System.out.println(checkfile.getPcapEpochTimestamp());
System.out.println(checkfile.getPcapAccumulativeTimestamp());




System.out.println();

count += -1;

}


/*
if( this.datain.available() > 0 )
{
	System.out.println("A");
}
*/



//checkfile.readUnsigned
//checkfile.dumpHexBlock(0, 40);




}
catch( Exception e )
{
	System.err.println(e);
	System.exit(1);
}


}

}










