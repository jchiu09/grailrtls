/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2012 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package org.grailrtls.sim;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Random;

import org.grailrtls.libworldmodel.client.protocol.messages.OriginPreferenceMessage;
import org.grailrtls.libworldmodel.solver.SolverWorldModelInterface;
import org.grailrtls.libworldmodel.solver.listeners.ConnectionListener;
import org.grailrtls.libworldmodel.solver.listeners.DataListener;
import org.grailrtls.libworldmodel.solver.protocol.messages.DataTransferMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.StartTransientMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.StopTransientMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.DataTransferMessage.Solution;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage.TypeSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FakeFireHoseSolver extends Thread implements ConnectionListener,
		DataListener {

	private static final Logger log = LoggerFactory
			.getLogger(FakeFireHoseSolver.class);

	private static final String TARGET_URI = "test.target";

	private static final String ATTRIBUTE = "test.attribute";

	private static final String ORIGIN = "test.fire_hose";

	private final SolverWorldModelInterface worldModel = new SolverWorldModelInterface();

	private boolean wmReadyForSolutions = false;

	private boolean keepRunning = true;

	private long delay = 100;

	public static void main(String[] args) {
		if (args.length < 2) {
			printUsageInfo();
			return;
		}

		int port = 7013;
		try {
			port = Integer.parseInt(args[1]);
		} catch (NumberFormatException nfe) {
			log.error(
					"Unable to format {} into a valid port number. Defaulting to 7013.",
					args[1]);
		}

		long delay = 100;
		if (args.length >= 3) {
			delay = Long.parseLong(args[2]);
		}

		int numSolvers = 1;
		if (args.length >= 4) {
			numSolvers = Integer.parseInt(args[3]);
		}

		final FakeFireHoseSolver[] solvers = new FakeFireHoseSolver[numSolvers];

		for (int i = 0; i < solvers.length; ++i) {
			solvers[i] = new FakeFireHoseSolver(args[0], port, delay);
		}

		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				for (FakeFireHoseSolver solver : solvers) {
					solver.keepRunning = false;
					solver.worldModel.doConnectionTearDown();
				}
				for (FakeFireHoseSolver solver : solvers) {
					try {
						solver.join();
						System.out.println("Joined.");
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});

		for(FakeFireHoseSolver solver : solvers){
			solver.start();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	public static void printUsageInfo() {
		StringBuffer sb = new StringBuffer(
				"Usage: <World Model IP> <World Model Port> <Delay>\n");
		System.err.println(sb.toString());
	}

	public FakeFireHoseSolver(final String wmHost, final int wmPort,
			final long delay) {
		this.worldModel.setHost(wmHost);
		this.worldModel.setPort(wmPort);

		this.worldModel.addConnectionListener(this);
		this.worldModel.addDataListener(this);
		TypeSpecification testType = new TypeSpecification();
		testType.setIsTransient(false);
		testType.setUriName(ATTRIBUTE);
		this.worldModel.addType(testType);
		this.worldModel.setOriginString(ORIGIN);
		this.worldModel.setCreateUris(true);
		this.worldModel.setDisconnectOnException(true);
		this.worldModel.setStayConnected(true);
		this.delay = delay;
	}

	@Override
	public void run() {
		Random rand = new Random(System.currentTimeMillis());
		long lastReport = System.currentTimeMillis();
		int numSent = 0;
		if (this.worldModel.doConnectionSetup()) {
			while (this.keepRunning) {
				try {
					long diff = System.currentTimeMillis() - lastReport;

					if (diff > 1000) {
						System.out.println((float) numSent / (diff / 1000));
						numSent = 0;
						lastReport = System.currentTimeMillis();
					}
					if(this.worldModel.getCachedWrites() < 100){
						this.sendSolution(0 - rand.nextFloat() * 100f);
						++numSent;
					}

					Thread.sleep(this.delay);
				} catch (InterruptedException ie) {
					// Nope
				} catch (UnsupportedEncodingException e) {
					log.error("What, no encoding?");
					e.printStackTrace();
				}
			}
			this.worldModel.doConnectionTearDown();
		}
	}

	private void sendSolution(final float rssi)
			throws UnsupportedEncodingException {
		Solution sol = new Solution();
		sol.setTargetName(TARGET_URI);
		sol.setAttributeName(ATTRIBUTE);
		sol.setTime(System.currentTimeMillis());
		ByteBuffer buff = ByteBuffer.allocate(4);
		buff.putFloat(rssi);
		sol.setData(buff.array());
		if (this.wmReadyForSolutions) {
			this.worldModel.sendSolution(sol);
		}
	}

	@Override
	public void startTransientReceived(SolverWorldModelInterface worldModel,
			StartTransientMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void stopTransientReceived(SolverWorldModelInterface worldModel,
			StopTransientMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void typeSpecificationsSent(SolverWorldModelInterface worldModel,
			TypeAnnounceMessage message) {
		log.info("Announced type specifications. Ready to send solutions.");
		this.wmReadyForSolutions = true;
	}

	@Override
	public void connectionInterrupted(SolverWorldModelInterface worldModel) {
		log.warn("Temporarily lost connection to {}", worldModel);
		this.wmReadyForSolutions = false;

	}

	@Override
	public void connectionEnded(SolverWorldModelInterface worldModel) {
		log.error("Permanently lost connection to {}", worldModel);
		System.exit(1);

	}

	@Override
	public void connectionEstablished(SolverWorldModelInterface worldModel) {
		log.info("Connected to {}", worldModel);
	}

}
