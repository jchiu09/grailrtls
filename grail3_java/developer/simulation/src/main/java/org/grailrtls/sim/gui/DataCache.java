/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.sim.gui;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.mina.util.ConcurrentHashSet;
import org.grailrtls.libcommon.util.HashableByteArray;
import org.grailrtls.libcommon.util.OnlineVariance;
import org.grailrtls.libsolver.protocol.messages.SampleMessage;
import org.grailrtls.sim.gui.structs.ChartItem;
import org.grailrtls.sim.gui.structs.SignalToDistanceItem;
import org.grailrtls.sim.gui.structs.SimpleChartItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Designed to keep track of various data from GRAIL components for the
 * simulation toolkit.
 * 
 * @author Robert Moore
 * 
 */
public class DataCache implements Cloneable {

	public enum ValueType {
		RSSI, VARIANCE
	};

	/**
	 * Logging facility for this class.
	 */
	private static final Logger log = LoggerFactory.getLogger(DataCache.class);

	protected static final float smoothingWeight = 0.875f;

	protected final Timer sweepTimer = new Timer();

	/**
	 * How long time-sensitive data should be kept in the cache.
	 */
	protected long maxCacheAge = 1000 * 60 * 10;

	/**
	 * Maximum size of the memory cache, in bytes. This is only for Sample
	 * objects, and the actual consumed memory will likely be a multiple of this
	 * value.
	 */
	protected int maxCacheByes = 1024 * 1024 * 2; // 2MiB

	/**
	 * The approximate size of the cache, in bytes.
	 */
	protected int currentCacheSize = 0;

	/**
	 * Maximum number of samples to use while computing RSSI variance.
	 */
	protected int maxVarianceHistory = 5;

	/**
	 * Max age of samples (in milliseconds) to use in calculating RSSI variance.
	 */
	protected long maxVarianceAge = 5000;

	/**
	 * Most recent sample from each receiver/transmitter pair.
	 */
	protected final ConcurrentHashMap<HashableByteArray, ConcurrentHashMap<HashableByteArray, ChartItem<SampleMessage>>> mostRecentSamplesByRByT = new ConcurrentHashMap<HashableByteArray, ConcurrentHashMap<HashableByteArray, ChartItem<SampleMessage>>>();

	/**
	 * Lists of SampleMessage objects mapped by receiver, then transmitter ID
	 * values.
	 */
	protected final ConcurrentHashMap<HashableByteArray, ConcurrentHashMap<HashableByteArray, ConcurrentLinkedQueue<SampleMessage>>> sampleQueuesByRByT = new ConcurrentHashMap<HashableByteArray, ConcurrentHashMap<HashableByteArray, ConcurrentLinkedQueue<SampleMessage>>>();

	/**
	 * Lists of SampleMessage objects mapped by receiver, then transmitter ID.
	 * RSSI value is actually variance
	 */
	protected final ConcurrentHashMap<HashableByteArray, ConcurrentHashMap<HashableByteArray, ConcurrentLinkedQueue<SampleMessage>>> varianceQueuesByRByT = new ConcurrentHashMap<HashableByteArray, ConcurrentHashMap<HashableByteArray, ConcurrentLinkedQueue<SampleMessage>>>();

	/**
	 * RSSI variance by receiver, then transmitter ID values.
	 */
	protected final ConcurrentHashMap<HashableByteArray, ConcurrentHashMap<HashableByteArray, OnlineVariance>> varianceByRByT = new ConcurrentHashMap<HashableByteArray, ConcurrentHashMap<HashableByteArray, OnlineVariance>>();

	/**
	 * Map of queues containing the most recent signal-to-distance data for
	 * receiver-fiduciary transmitter pairs. Mapped by receiver.
	 */
	protected ConcurrentHashMap<HashableByteArray, ConcurrentLinkedQueue<SignalToDistanceItem>> sigToDistHistory = new ConcurrentHashMap<HashableByteArray, ConcurrentLinkedQueue<SignalToDistanceItem>>();

	/**
	 * Mapping from device (transmitter, receiver) ID values to their
	 * 2-dimensional position within a region.
	 */
	protected final ConcurrentHashMap<HashableByteArray, Point2D> deviceLocations = new ConcurrentHashMap<HashableByteArray, Point2D>();

	/**
	 * List of receiver ID values returned by the world model.
	 */
	protected final ConcurrentHashSet<HashableByteArray> receiverIds = new ConcurrentHashSet<HashableByteArray>();

	/**
	 * List of fiduciary transmitter ID values returned by the world model.
	 */
	protected final ConcurrentHashSet<HashableByteArray> fiduciaryTransmitterIds = new ConcurrentHashSet<HashableByteArray>();

	/**
	 * List of devices that have been heard consistently, but have no info about
	 * them.
	 */
	protected final ConcurrentHashSet<HashableByteArray> dynamicTransmitterIds = new ConcurrentHashSet<HashableByteArray>();

	/**
	 * Two-dimensional region bounds for the defined region.
	 */
	protected Rectangle2D regionBounds = null;

	/**
	 * Image for the region background when drawing map-based panels.
	 */
	protected BufferedImage regionImage = null;

	/**
	 * UTF-16 URI of the region.
	 */
	protected String regionUri = null;

	/**
	 * Number of samples received since the last statistics calculation.
	 */
	protected long samplesReceived = 0;

	/**
	 * Weighted average of samples per second rate.
	 */
	protected float smoothedSamplesPerSecond = 0f;

	/**
	 * Timestamp of the last statistics update (samples per second).
	 */
	protected long lastStatsUpdate = 0l;

	/**
	 * The weighted average number of samples per second for some period of the
	 * past.
	 */
	protected final ConcurrentLinkedQueue<SimpleChartItem> samplesPerSecondHistory = new ConcurrentLinkedQueue<SimpleChartItem>();

	protected final ConcurrentHashMap<HashableByteArray, Integer> transmitterSamplesReceived = new ConcurrentHashMap<HashableByteArray, Integer>();

	/**
	 * List of objects that care when receivers or fiduciary transmitters become
	 * known to the data cache.
	 */
	protected final ConcurrentLinkedQueue<DataCacheListener> listeners = new ConcurrentLinkedQueue<DataCacheListener>();

	public DataCache() {
		super();
		this.sweepTimer.schedule(new TimerTask() {

			@Override
			public void run() {
				DataCache.this.performSweep();
			}
		}, 60000, 60000);

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				DataCache.this.shutdown();
			}

		});
	}

	protected void shutdown() {
		this.sweepTimer.cancel();
	}

	protected void performSweep() {
		this.transmitterSamplesReceived.clear();
	}

	/**
	 * Returns the current RSSI value for a specific transmitter from a specific
	 * receiver.
	 * 
	 * @param transmitter
	 *            the transmitter that sent the packet
	 * @param receiver
	 *            the receiver that observed it
	 * @return the most recent RSSI data from the aggregator, or
	 *         {@link Float#NaN} if no data exists.
	 */
	public float getCurrentRssi(final HashableByteArray transmitter,
			final HashableByteArray receiver) {
		ConcurrentHashMap<HashableByteArray, ChartItem<SampleMessage>> receiverSamples = this.mostRecentSamplesByRByT
				.get(receiver);

		if (receiverSamples == null) {
			return Float.NaN;
		}

		ChartItem<SampleMessage> mostRecent = receiverSamples.get(transmitter);

		if (mostRecent == null) {
			return Float.NaN;
		}

		if (mostRecent.getCreationTime() < System.currentTimeMillis()
				- this.maxCacheAge) {
			receiverSamples.remove(transmitter);
			return Float.NaN;
		}

		return mostRecent.getValue().getRssi();
	}

	public float getCurrentVariance(final HashableByteArray transmitter,
			final HashableByteArray receiver) {
		ConcurrentHashMap<HashableByteArray, OnlineVariance> receiverVariances = this.varianceByRByT
				.get(receiver);
		if (receiverVariances == null) {
			return Float.NaN;
		}

		OnlineVariance variance = receiverVariances.get(transmitter);
		if (variance == null) {
			return Float.NaN;
		}

		return variance.getCurrentVariance();
	}

	/**
	 * Returns the full list of samples received by a receiver for a specific
	 * transmitter. The size of the list is governed by {@link #maxCacheByes},
	 * but may vary over time. Also, due to concurrency issues, there is no
	 * guarantee that the list of samples returned is the CURRENT list of
	 * samples, but is some stable view of those samples in the recent past.
	 * 
	 * @param receiverId
	 *            the ID of the receiver.
	 * @param transmitterId
	 *            the ID of the transmitter.
	 * @return a list of {@code SampleMessage} objects that were received by the
	 *         receiver from the transmitter, or {@code null} if none are
	 *         currently cached.
	 */
	public List<SampleMessage> getSampleList(
			final HashableByteArray receiverId,
			final HashableByteArray transmitterId) {
		ConcurrentHashMap<HashableByteArray, ConcurrentLinkedQueue<SampleMessage>> receiverMap = this.sampleQueuesByRByT
				.get(receiverId);
		if (receiverMap == null) {
			return null;
		}

		ConcurrentLinkedQueue<SampleMessage> sampleQueue = receiverMap
				.get(transmitterId);
		if (sampleQueue == null) {
			return null;
		}

		LinkedList<SampleMessage> sampleList = new LinkedList<SampleMessage>();
		sampleList.addAll(sampleQueue);
		return sampleList;
	}

	/**
	 * Returns a list of SampleMessage objects containing the RSSI variance as
	 * the {@code rssi} field. Other values are the same as the original
	 * SampleMessage;
	 * 
	 * @param receiverId
	 *            the ID of the receiver.
	 * @param transmitterId
	 *            the ID of the transmitter.
	 * @return a list of {@code SampleMessage} objects that were received by the
	 *         receiver from the transmitter, or {@code null} if none are
	 *         currently cached.
	 */
	public List<SampleMessage> getVarianceSampleList(
			final HashableByteArray receiverId,
			final HashableByteArray transmitterId) {
		ConcurrentHashMap<HashableByteArray, ConcurrentLinkedQueue<SampleMessage>> receiverMap = this.varianceQueuesByRByT
				.get(receiverId);
		if (receiverMap == null) {
			return null;
		}

		ConcurrentLinkedQueue<SampleMessage> sampleQueue = receiverMap
				.get(transmitterId);
		if (sampleQueue == null) {
			return null;
		}

		LinkedList<SampleMessage> sampleList = new LinkedList<SampleMessage>();
		sampleList.addAll(sampleQueue);

		return sampleList;
	}

	/**
	 * Adds a {@code SampleMessage} to this data cache. Updates relevant data
	 * structures as needed.
	 * 
	 * @param newSample
	 *            the new {@code SampleMessage} to add to this data cache
	 */
	public void addSample(final SampleMessage newSample) {
		// Cache size
		this.currentCacheSize += newSample.getLengthPrefix();
		// End cache size

		HashableByteArray receiverId = new HashableByteArray(
				newSample.getReceiverId());

		HashableByteArray transmitterId = new HashableByteArray(
				newSample.getDeviceId());

		// Count the number of samples from this transmitter
		if (!(this.fiduciaryTransmitterIds.contains(transmitterId) || this.dynamicTransmitterIds
				.contains(transmitterId))) {
			Integer txerCount = this.transmitterSamplesReceived
					.get(transmitterId);
			if (txerCount == null) {
				txerCount = Integer.valueOf(0);
			}
			txerCount = Integer.valueOf(txerCount.intValue() + 1);
			this.transmitterSamplesReceived.put(transmitterId, txerCount);

			if (txerCount.intValue() > this.receiverIds.size() * 5) {
				log.info("Adding dynamic device {}", transmitterId);
				this.dynamicTransmitterIds.add(transmitterId);
				this.transmitterSamplesReceived.remove(transmitterId);
				for (DataCacheListener listener : this.listeners) {
					listener.transmitterAdded(transmitterId, false);
				}
			}
		}

		// Insert the raw sample.
		ConcurrentHashMap<HashableByteArray, ChartItem<SampleMessage>> receiverCurrent = this.mostRecentSamplesByRByT
				.get(receiverId);

		if (receiverCurrent == null) {
			receiverCurrent = new ConcurrentHashMap<HashableByteArray, ChartItem<SampleMessage>>();
			this.mostRecentSamplesByRByT.put(receiverId, receiverCurrent);
		}

		receiverCurrent.put(transmitterId, new SimpleChartItem<SampleMessage>(
				newSample));

		// Queue of recent samples
		ConcurrentHashMap<HashableByteArray, ConcurrentLinkedQueue<SampleMessage>> receiverSamples = this.sampleQueuesByRByT
				.get(receiverId);

		if (receiverSamples == null) {
			receiverSamples = new ConcurrentHashMap<HashableByteArray, ConcurrentLinkedQueue<SampleMessage>>();
			this.sampleQueuesByRByT.put(receiverId, receiverSamples);
		}
		ConcurrentLinkedQueue<SampleMessage> transmitterSamples = receiverSamples
				.get(transmitterId);

		if (transmitterSamples == null) {
			transmitterSamples = new ConcurrentLinkedQueue<SampleMessage>();
			receiverSamples.put(transmitterId, transmitterSamples);
		}

		transmitterSamples.offer(newSample);
		long oldestAge = System.currentTimeMillis() - this.maxCacheAge;
		for (Iterator<SampleMessage> iter = transmitterSamples.iterator(); iter
				.hasNext();) {
			SampleMessage item = iter.next();
			if (item.getCreationTime() >= oldestAge) {
				break;
			}
			iter.remove();
			this.currentCacheSize -= item.getLengthPrefix();
		}
		// End raw samples

		// Update the variance info
		ConcurrentHashMap<HashableByteArray, OnlineVariance> receiverVariances = this.varianceByRByT
				.get(receiverId);

		if (receiverVariances == null) {
			receiverVariances = new ConcurrentHashMap<HashableByteArray, OnlineVariance>();
			this.varianceByRByT.put(receiverId, receiverVariances);
		}

		OnlineVariance variance = receiverVariances.get(transmitterId);

		if (variance == null) {
			variance = new OnlineVariance();
			variance.setMaxAge(this.maxVarianceAge);
			variance.setMaxHistory(this.maxVarianceHistory);
			receiverVariances.put(transmitterId, variance);
		}

		variance.addValue(newSample.getRssi());

		SampleMessage varianceSample = new SampleMessage();
		varianceSample.setCreationTime(newSample.getCreationTime());
		varianceSample.setDeviceId(newSample.getDeviceId());
		varianceSample.setPhysicalLayer(newSample.getPhysicalLayer());
		varianceSample.setReceiverId(newSample.getReceiverId());
		varianceSample.setRssi(variance.getCurrentVariance());
		ConcurrentHashMap<HashableByteArray, ConcurrentLinkedQueue<SampleMessage>> receiverVarianceQueues = this.varianceQueuesByRByT
				.get(receiverId);
		if (receiverVarianceQueues == null) {
			receiverVarianceQueues = new ConcurrentHashMap<HashableByteArray, ConcurrentLinkedQueue<SampleMessage>>();
			this.varianceQueuesByRByT.put(receiverId, receiverVarianceQueues);
		}

		ConcurrentLinkedQueue<SampleMessage> varianceQueue = receiverVarianceQueues
				.get(transmitterId);
		if (varianceQueue == null) {
			varianceQueue = new ConcurrentLinkedQueue<SampleMessage>();
			receiverVarianceQueues.put(transmitterId, varianceQueue);
		}

		varianceQueue.offer(varianceSample);
		for (Iterator<SampleMessage> iter = varianceQueue.iterator(); iter
				.hasNext();) {
			SampleMessage item = iter.next();
			if (item.getCreationTime() >= oldestAge) {
				break;
			}
			iter.remove();
		}
		// End variance update

		// Signal to distance update
		Point2D recPoint = this.getDeviceLocation(receiverId);
		Point2D transPoint = this.getDeviceLocation(transmitterId);
		if (recPoint == null || transPoint == null) {
			return;
		}

		float distance = (float) Math.sqrt(Math.pow(recPoint.getX()
				- transPoint.getX(), 2)
				+ Math.pow(recPoint.getY() - transPoint.getY(), 2));
		SignalToDistanceItem newSigToDist = new SignalToDistanceItem(distance,
				newSample.getRssi());
		ConcurrentLinkedQueue<SignalToDistanceItem> history = this.sigToDistHistory
				.get(receiverId);
		if (history == null) {
			history = new ConcurrentLinkedQueue<SignalToDistanceItem>();
			this.sigToDistHistory.put(receiverId, history);
		}
		history.offer(newSigToDist);
		// Trim the old values
		while (history.peek().getCreationTime() < oldestAge) {
			history.poll();
		}
		// End signal to distance
	}

	protected void generateStatistics() {
		long now = System.currentTimeMillis();
		float sps = this.samplesReceived
				/ ((now - this.lastStatsUpdate) / 1000f);

		this.smoothedSamplesPerSecond = sps * (1 - DataCache.smoothingWeight)
				+ this.smoothedSamplesPerSecond * this.smoothingWeight;
		if (this.smoothedSamplesPerSecond < 0.01f) {
			this.smoothedSamplesPerSecond = 0f;
		}

		this.samplesPerSecondHistory.add(new SimpleChartItem(
				this.smoothedSamplesPerSecond));
		long oldestItemTime = now - this.maxCacheAge;
		SimpleChartItem lastItem = null;
		do {
			lastItem = this.samplesPerSecondHistory.peek();
			if (lastItem == null) {
				break;
			}
			if (lastItem.getCreationTime() >= oldestItemTime) {
				break;
			}
			this.samplesPerSecondHistory.poll();
		} while (true);

		this.samplesReceived = 0;
		this.lastStatsUpdate = now;
	}

	/**
	 * Returns the cached signal-to-distance data for a specific receiver based
	 * on fiduciary transmitter information.
	 * 
	 * @param receiverId
	 * @return a list of {@link SignalToDistanceItem} objects containing the
	 *         cached signal-to-distance cata for the receiver.
	 */
	public List<SignalToDistanceItem> getSignalToDistance(
			final HashableByteArray receiverId) {
		ConcurrentLinkedQueue<SignalToDistanceItem> signalToDistanceItems = this.sigToDistHistory
				.get(receiverId);

		if (signalToDistanceItems == null) {
			return null;
		}

		LinkedList<SignalToDistanceItem> itemList = new LinkedList<SignalToDistanceItem>();
		for (SignalToDistanceItem item : signalToDistanceItems) {
			itemList.add(item);
		}
		return itemList;
	}

	/**
	 * Sets the location of a specified device within the defined region.
	 * 
	 * @param deviceId
	 *            the ID of a device (receiver or transmitter).
	 * @param location
	 *            the 2-dimensional location of the device, specified as
	 *            coordinates within the region.
	 */
	public void setDeviceLocation(final HashableByteArray deviceId,
			final Point2D location) {
		this.deviceLocations.put(deviceId, location);
		if (this.receiverIds.contains(deviceId)) {
			for (DataCacheListener listener : this.listeners) {
				listener.receiverAdded(deviceId);
			}
		} else if (this.fiduciaryTransmitterIds.contains(deviceId)) {
			for (DataCacheListener listener : this.listeners) {
				listener.transmitterAdded(deviceId, true);
			}
		}
	}

	public Point2D getDeviceLocation(final HashableByteArray deviceId) {
		return this.deviceLocations.get(deviceId);
	}

	/**
	 * Clears all information in this data cache, including region definitions,
	 * device locations, receiver ids, fiduciary transmitter ids, and cached
	 * data. This method will essentially "reset" the data cache to the original
	 * state.
	 */
	public void clearAll() {
		this.clearCachedData();
		this.deviceLocations.clear();
		this.regionBounds = null;
		this.regionUri = null;
		this.fiduciaryTransmitterIds.clear();
		this.receiverIds.clear();
		log.info("Region info and device locations cleared from cache.");
	}

	/**
	 * Clears sample-related data from this cache, including raw samples and
	 * variance information. If you need to clear region data and device
	 * locations, you should call {@link #clearAll()} instead.
	 */
	public void clearCachedData() {
		this.sampleQueuesByRByT.clear();
		this.varianceQueuesByRByT.clear();
		this.varianceByRByT.clear();
		this.sigToDistHistory.clear();

		log.info("All sample data cleared from cache.");
	}

	public int getMaxCacheByes() {
		return maxCacheByes;
	}

	public void setMaxCacheByes(int maxCacheByes) {
		this.maxCacheByes = maxCacheByes;
	}

	public int getMaxVarianceHistory() {
		return maxVarianceHistory;
	}

	public void setMaxVarianceHistory(int maxVarianceHistory) {
		this.maxVarianceHistory = maxVarianceHistory;
	}

	public long getMaxVarianceAge() {
		return maxVarianceAge;
	}

	public void setMaxVarianceAge(long maxVarianceAge) {
		this.maxVarianceAge = maxVarianceAge;
	}

	public Rectangle2D getRegionBounds() {
		return regionBounds;
	}

	public void setRegionBounds(Rectangle2D regionBounds) {
		this.regionBounds = regionBounds;
	}

	public String getRegionUri() {
		return regionUri;
	}

	public void setRegionUri(String regionUri) {
		this.regionUri = regionUri;
	}

	public int getCurrentCacheSize() {
		return currentCacheSize;
	}

	public BufferedImage getRegionImage() {
		return regionImage;
	}

	public void setRegionImage(BufferedImage regionImage) {
		this.regionImage = regionImage;
	}

	public void addListener(final DataCacheListener listener) {
		this.listeners.add(listener);
	}

	public void removeListener(final DataCacheListener listener) {
		this.listeners.add(listener);
	}

	public void addReceiver(final HashableByteArray receiverId) {
		this.receiverIds.add(receiverId);
	}

	public void addFiduciaryTransmitter(final HashableByteArray transmitterId) {
		this.fiduciaryTransmitterIds.add(transmitterId);
	}

	public List<HashableByteArray> getReceiverIds() {
		List<HashableByteArray> receivers = new LinkedList<HashableByteArray>();
		receivers.addAll(this.receiverIds);
		return receivers;
	}

	public List<HashableByteArray> getFiduciaryTransmitterIds() {
		List<HashableByteArray> transmitters = new LinkedList<HashableByteArray>();
		transmitters.addAll(this.fiduciaryTransmitterIds);
		return transmitters;
	}

	public List<HashableByteArray> getDynamicTransmitterIds() {
		List<HashableByteArray> transmitters = new LinkedList<HashableByteArray>();
		transmitters.addAll(this.dynamicTransmitterIds);
		return transmitters;
	}

	public long getMaxCacheAge() {
		return maxCacheAge;
	}

	public void setMaxCacheAge(long maxCacheAge) {
		this.maxCacheAge = maxCacheAge;
	}

	public DataCache clone() {
		DataCache returnedCache = new DataCache();
		this.overlay(returnedCache);
		return returnedCache;
	}

	protected void overlay(final DataCache clone) {
		clone.clearAll();

		clone.maxCacheAge = this.maxCacheAge;
		clone.maxCacheByes = this.maxCacheByes;
		clone.currentCacheSize = this.currentCacheSize;
		clone.maxVarianceHistory = this.maxVarianceHistory;
		clone.maxVarianceAge = this.maxVarianceAge;
		clone.regionBounds = this.regionBounds;
		clone.regionImage = this.regionImage;
		clone.regionUri = this.regionUri;
		clone.samplesReceived = this.samplesReceived;
		clone.smoothedSamplesPerSecond = this.smoothedSamplesPerSecond;
		clone.lastStatsUpdate = this.lastStatsUpdate;

		for (HashableByteArray device : this.receiverIds) {
			clone.receiverIds.add(device);
		}

		for (HashableByteArray device : this.fiduciaryTransmitterIds) {
			clone.fiduciaryTransmitterIds.add(device);
		}

		for (HashableByteArray device : this.deviceLocations.keySet()) {
			clone.deviceLocations.put(device, this.deviceLocations.get(device));
		}

		for (HashableByteArray receiver : this.sigToDistHistory.keySet()) {
			ConcurrentLinkedQueue<SignalToDistanceItem> receiverSigToDist = this.sigToDistHistory
					.get(receiver);
			ConcurrentLinkedQueue<SignalToDistanceItem> cloneReceiverSigToDist = new ConcurrentLinkedQueue<SignalToDistanceItem>();
			cloneReceiverSigToDist.addAll(receiverSigToDist);
			clone.sigToDistHistory.put(receiver, cloneReceiverSigToDist);
		}

		for (HashableByteArray receiver : this.mostRecentSamplesByRByT.keySet()) {
			ConcurrentHashMap<HashableByteArray, ChartItem<SampleMessage>> receiverSamples = this.mostRecentSamplesByRByT
					.get(receiver);
			if (receiverSamples == null) {
				continue;
			}
			ConcurrentHashMap<HashableByteArray, ChartItem<SampleMessage>> cloneReceiverSamples = new ConcurrentHashMap<HashableByteArray, ChartItem<SampleMessage>>();
			clone.mostRecentSamplesByRByT.put(receiver, cloneReceiverSamples);

			for (HashableByteArray transmitter : receiverSamples.keySet()) {
				ChartItem<SampleMessage> origSampleItem = receiverSamples
						.get(transmitter);
				ChartItem<SampleMessage> cloneSampleItem = new SimpleChartItem<SampleMessage>(
						origSampleItem.getValue());
				cloneReceiverSamples.put(transmitter, cloneSampleItem);
			}
		}

		for (HashableByteArray receiver : this.varianceByRByT.keySet()) {
			ConcurrentHashMap<HashableByteArray, OnlineVariance> receiverSamples = this.varianceByRByT
					.get(receiver);
			if (receiverSamples == null) {
				continue;
			}
			ConcurrentHashMap<HashableByteArray, OnlineVariance> cloneReceiverSamples = new ConcurrentHashMap<HashableByteArray, OnlineVariance>();
			clone.varianceByRByT.put(receiver, cloneReceiverSamples);

			for (HashableByteArray transmitter : receiverSamples.keySet()) {
				OnlineVariance origVariance = receiverSamples.get(transmitter);
				OnlineVariance cloneVariance = origVariance.clone();
				cloneReceiverSamples.put(transmitter, cloneVariance);
			}
		}

		for (HashableByteArray receiver : this.sampleQueuesByRByT.keySet()) {
			ConcurrentHashMap<HashableByteArray, ConcurrentLinkedQueue<SampleMessage>> receiverSampleQueues = this.sampleQueuesByRByT
					.get(receiver);
			ConcurrentHashMap<HashableByteArray, ConcurrentLinkedQueue<SampleMessage>> cloneReceiverSampleQueues = new ConcurrentHashMap<HashableByteArray, ConcurrentLinkedQueue<SampleMessage>>();
			clone.sampleQueuesByRByT.put(receiver, cloneReceiverSampleQueues);
			for (HashableByteArray transmitter : receiverSampleQueues.keySet()) {
				ConcurrentLinkedQueue<SampleMessage> transmitterSamples = receiverSampleQueues
						.get(transmitter);
				ConcurrentLinkedQueue<SampleMessage> cloneTransmitterSamples = new ConcurrentLinkedQueue<SampleMessage>();
				cloneReceiverSampleQueues.put(transmitter,
						cloneTransmitterSamples);
				cloneTransmitterSamples.addAll(transmitterSamples);
			}
		}

		for (HashableByteArray receiver : this.varianceQueuesByRByT.keySet()) {
			ConcurrentHashMap<HashableByteArray, ConcurrentLinkedQueue<SampleMessage>> receiverSampleQueues = this.varianceQueuesByRByT
					.get(receiver);
			ConcurrentHashMap<HashableByteArray, ConcurrentLinkedQueue<SampleMessage>> cloneReceiverSampleQueues = new ConcurrentHashMap<HashableByteArray, ConcurrentLinkedQueue<SampleMessage>>();
			clone.varianceQueuesByRByT.put(receiver, cloneReceiverSampleQueues);
			for (HashableByteArray transmitter : receiverSampleQueues.keySet()) {
				ConcurrentLinkedQueue<SampleMessage> transmitterSamples = receiverSampleQueues
						.get(transmitter);
				ConcurrentLinkedQueue<SampleMessage> cloneTransmitterSamples = new ConcurrentLinkedQueue<SampleMessage>();
				cloneReceiverSampleQueues.put(transmitter,
						cloneTransmitterSamples);
				cloneTransmitterSamples.addAll(transmitterSamples);
			}
		}
	}
}
