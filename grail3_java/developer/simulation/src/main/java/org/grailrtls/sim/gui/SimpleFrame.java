/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package org.grailrtls.sim.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSeparator;

import org.grailrtls.libcommon.util.HashableByteArray;
import org.grailrtls.sim.gui.DataCache.ValueType;
import org.grailrtls.sim.gui.panels.BarChart;
import org.grailrtls.sim.gui.panels.LineChart;
import org.grailrtls.sim.gui.panels.SignalToDistanceMap;
import org.grailrtls.sim.gui.panels.VoronoiHeatMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleFrame extends JFrame implements ActionListener,
		DataCacheListener, WindowListener {
	private static final Logger log = LoggerFactory
			.getLogger(SimpleFrame.class);

	protected int desiredFps = 10;
	
	protected final FilteringDataCache cache;

	protected BufferedImage receiverIcon = null;

	protected BufferedImage transmitterIcon = null;

	public SimpleFrame(FilteringDataCache cache) {
		super();

		this.cache = cache;
		this.cache.addListener(this);

		try {
			this.receiverIcon = ImageIO.read(getClass().getResourceAsStream(
					"/images/receiver.png"));
		} catch (IOException e) {
			log.warn("Unable to load receiver icon.");
		}
		try {
			this.transmitterIcon = ImageIO.read(getClass().getResourceAsStream(
					"/images/fiduciary_transmitter.png"));
		} catch (IOException e) {
			log.warn("Unable to load transmittericon.");
		}
	}

	public void configureDisplay() {

		this.addWindowListener(this);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setPreferredSize(new Dimension(800, 600));
		this.setLayout(new BorderLayout());

		this.buildMenu();

		this.setJMenuBar(this.menu);

		this.pack();

		this.setVisible(true);

		this.startUpdates();
	}

	protected Timer updateTimer = new Timer();

	protected TimerTask updateTask = null;

	protected HashableByteArray currentDeviceId = null;

	protected boolean isTransmitter = false;

	private String titleChartType = "";
	private String titleDeviceName = "";

	/*
	 * mainPanel and displayPanel should point to the same object, but are
	 * convenience pointers for accessing it as a JPanel class or a DisplayPanel
	 * interface.
	 */
	protected JPanel mainPanel = null;

	protected DisplayPanel displayPanel = null;

	protected JMenuBar menu = new JMenuBar();

	protected JMenu windowMenu = new JMenu("Windows");

	protected JMenuItem windowNew = new JMenuItem("New Window");

	protected JMenu refreshMenu = new JMenu("Refresh Rate");

	protected JMenu visualizationMenu = new JMenu("Visualizations");

	protected JMenu dataMenu = new JMenu("Data");

	protected JMenuItem clearCache = new JMenuItem("Clear Cache");
	
	protected JMenuItem cloneCache = new JMenuItem("Clone Cache");

	protected JRadioButtonMenuItem visualizeRssiBars = new JRadioButtonMenuItem(
			"RSSI Bar");
	protected JRadioButtonMenuItem visualizeVarianceBars = new JRadioButtonMenuItem(
			"Var. Bar");

	protected JRadioButtonMenuItem visualizeRssiVoronoi = new JRadioButtonMenuItem(
			"RSSI. Voronoi");

	protected JRadioButtonMenuItem visualizeVarianceVoronoi = new JRadioButtonMenuItem(
			"Var. Voronoi");

	protected JRadioButtonMenuItem visualizeRssiLines = new JRadioButtonMenuItem(
			"RSSI Lines");

	protected JRadioButtonMenuItem visualizeVarianceLines = new JRadioButtonMenuItem(
			"Var. Lines");
	
	protected JRadioButtonMenuItem visualizeSignalRings = new JRadioButtonMenuItem("RSSI Rings");

	protected JMenuItem refresh1hz = new JRadioButtonMenuItem("1 Hz");
	protected JMenuItem refresh5hz = new JRadioButtonMenuItem("5 Hz");
	protected JMenuItem refresh10hz = new JRadioButtonMenuItem("10 Hz", true);
	protected JMenuItem refresh15hz = new JRadioButtonMenuItem("15 Hz");
	protected JMenuItem refresh20hz = new JRadioButtonMenuItem("20 Hz");
	protected JMenuItem refresh30hz = new JRadioButtonMenuItem("30 Hz");

	protected ButtonGroup refreshGroup = new ButtonGroup();

	protected JMenu receiversMenu = new JMenu("Receivers");

	protected ConcurrentHashMap<JRadioButtonMenuItem, HashableByteArray> receiverMenuItems = new ConcurrentHashMap<JRadioButtonMenuItem, HashableByteArray>();

	protected JMenu transmittersMenu = new JMenu("Transmitters");
	
	protected JMenu transmittersFiduciaryMenu = new JMenu("Fiduciary");
	
	protected JMenu transmittersDynamicMenu = new JMenu("Dynamic");
	
	protected ConcurrentHashMap<JRadioButtonMenuItem, HashableByteArray> transmitterFiduciaryItems = new ConcurrentHashMap<JRadioButtonMenuItem, HashableByteArray>();
	
	protected ConcurrentHashMap<JRadioButtonMenuItem, HashableByteArray> transmitterDynamicItems = new ConcurrentHashMap<JRadioButtonMenuItem, HashableByteArray>();
	
	protected JMenu deviceMenu = new JMenu("Device");

	protected JMenu sourceReceiversMenu = new JMenu("Receivers");

	protected ConcurrentHashMap<JCheckBoxMenuItem, HashableByteArray> sourceReceiverMenuItems = new ConcurrentHashMap<JCheckBoxMenuItem, HashableByteArray>();

	protected JMenu sourceTransmittersMenu = new JMenu("Transmitters");
	
	protected ConcurrentHashMap<JCheckBoxMenuItem, HashableByteArray> sourceTransmitterMenuItems = new ConcurrentHashMap<JCheckBoxMenuItem, HashableByteArray>();
	
	protected JMenuItem sourceReceiverSelectAll = new JMenuItem("Select All");
	
	protected JMenuItem sourceReceiverClearAll = new JMenuItem("Clear All");
	
	protected JMenuItem sourceTransmitterSelectAll = new JMenuItem("Select All");
	
	protected JMenuItem sourceTransmitterClearAll = new JMenuItem("Clear All");
	
	protected JMenu sourcesMenu = new JMenu("Sources");

	protected ButtonGroup selectedDeviceGroup = new ButtonGroup();

	protected ButtonGroup visualizationGroup = new ButtonGroup();

	protected static volatile int numWindows = 0;

	protected void buildMenu() {

		this.windowMenu.add(this.windowNew);

		this.windowNew.addActionListener(this);

		this.menu.add(this.windowMenu);
		
		this.menu.add(this.deviceMenu);
		
		this.deviceMenu.add(this.transmittersMenu);
		this.deviceMenu.add(this.receiversMenu);
		
		this.transmittersMenu.add(this.transmittersFiduciaryMenu);
		this.transmittersMenu.add(this.transmittersDynamicMenu);
		
		this.menu.add(this.sourcesMenu);
		this.sourcesMenu.add(this.sourceTransmittersMenu);
		this.sourcesMenu.add(this.sourceReceiversMenu);

		this.sourceTransmittersMenu.add(this.sourceTransmitterSelectAll);
		this.sourceTransmittersMenu.add(this.sourceTransmitterClearAll);
		this.sourceTransmittersMenu.add(new JSeparator(JSeparator.HORIZONTAL));
		
		this.sourceReceiversMenu.add(this.sourceReceiverSelectAll);
		this.sourceReceiversMenu.add(this.sourceReceiverClearAll);
		this.sourceReceiversMenu.add(new JSeparator(JSeparator.HORIZONTAL));
		
		this.sourceTransmitterSelectAll.addActionListener(this);
		this.sourceTransmitterClearAll.addActionListener(this);
		
		this.sourceReceiverSelectAll.addActionListener(this);
		this.sourceReceiverClearAll.addActionListener(this);

		// Fiduciary transmitters
		List<HashableByteArray> transmitters = this.cache
				.getFiduciaryTransmitterIds();
		for (HashableByteArray transmitter : transmitters) {
			JRadioButtonMenuItem newSelectedItem = new JRadioButtonMenuItem(transmitter
					.toString());
			JCheckBoxMenuItem newCheckedItem = new JCheckBoxMenuItem(transmitter.toString());
			newCheckedItem.setSelected(true);
			if (this.transmitterFiduciaryItems.put(newSelectedItem, transmitter) == null) {
				newSelectedItem.addActionListener(this);
				this.selectedDeviceGroup.add(newSelectedItem);
				this.transmittersFiduciaryMenu.add(newSelectedItem);
			}
			if (this.sourceTransmitterMenuItems.put(newCheckedItem, transmitter) == null) {
				newCheckedItem.addActionListener(this);
				this.sourceTransmittersMenu.add(newCheckedItem);
			}
		}
		
		// Dynamic transmitters
		transmitters = this.cache.getDynamicTransmitterIds();
		for(HashableByteArray transmitter : transmitters){
			JRadioButtonMenuItem newSelectedItem = new JRadioButtonMenuItem(transmitter.toString());
			JCheckBoxMenuItem newCheckedItem = new JCheckBoxMenuItem(transmitter.toString());
			newCheckedItem.setSelected(true);
			if (this.transmitterDynamicItems.put(newSelectedItem, transmitter) == null) {
				newSelectedItem.addActionListener(this);
				this.selectedDeviceGroup.add(newSelectedItem);
				this.transmittersDynamicMenu.add(newSelectedItem);
			}
			if (this.sourceTransmitterMenuItems.put(newCheckedItem, transmitter) == null) {
				newCheckedItem.addActionListener(this);
				this.sourceTransmittersMenu.add(newCheckedItem);
			}
			
		}

		List<HashableByteArray> receivers = this.cache
				.getReceiverIds();
		for (HashableByteArray receiver : receivers) {
			JRadioButtonMenuItem newSelectedItem = new JRadioButtonMenuItem(receiver
					.toString());
			JCheckBoxMenuItem newCheckedItem = new JCheckBoxMenuItem(receiver.toString());
			newCheckedItem.setSelected(true);
			if (this.receiverMenuItems.put(newSelectedItem, receiver) == null) {
				newSelectedItem.addActionListener(this);
				this.selectedDeviceGroup.add(newSelectedItem);
				this.receiversMenu.add(newSelectedItem);
			}
			if (this.sourceReceiverMenuItems.put(newCheckedItem, receiver) == null) {
				newCheckedItem.addActionListener(this);
				this.sourceReceiversMenu.add(newCheckedItem);
			}
		}

		this.refreshGroup.add(this.refresh1hz);
		this.refreshGroup.add(this.refresh5hz);
		this.refreshGroup.add(this.refresh10hz);
		this.refreshGroup.add(this.refresh15hz);
		this.refreshGroup.add(this.refresh20hz);
		this.refreshGroup.add(this.refresh30hz);

		this.refreshMenu.add(this.refresh1hz);
		this.refreshMenu.add(this.refresh5hz);
		this.refreshMenu.add(this.refresh10hz);
		this.refreshMenu.add(this.refresh15hz);
		this.refreshMenu.add(this.refresh20hz);
		this.refreshMenu.add(this.refresh30hz);

		this.refresh1hz.addActionListener(this);
		this.refresh5hz.addActionListener(this);
		this.refresh10hz.addActionListener(this);
		this.refresh15hz.addActionListener(this);
		this.refresh20hz.addActionListener(this);
		this.refresh30hz.addActionListener(this);

		this.menu.add(refreshMenu);

		this.visualizationMenu.add(this.visualizeRssiBars);
		this.visualizationMenu.add(this.visualizeVarianceBars);
		this.visualizationMenu.add(this.visualizeRssiVoronoi);
		this.visualizationMenu.add(this.visualizeVarianceVoronoi);
		this.visualizationMenu.add(this.visualizeRssiLines);
		this.visualizationMenu.add(this.visualizeVarianceLines);
		this.visualizationMenu.add(this.visualizeSignalRings);

		this.visualizeRssiBars.addActionListener(this);
		this.visualizeVarianceBars.addActionListener(this);
		this.visualizeRssiVoronoi.addActionListener(this);
		this.visualizeVarianceVoronoi.addActionListener(this);
		this.visualizeRssiLines.addActionListener(this);
		this.visualizeVarianceLines.addActionListener(this);
		this.visualizeSignalRings.addActionListener(this);

		this.visualizationGroup.add(this.visualizeRssiBars);
		this.visualizationGroup.add(this.visualizeVarianceBars);
		this.visualizationGroup.add(this.visualizeRssiVoronoi);
		this.visualizationGroup.add(this.visualizeVarianceVoronoi);
		this.visualizationGroup.add(this.visualizeRssiLines);
		this.visualizationGroup.add(this.visualizeVarianceLines);
		this.visualizationGroup.add(this.visualizeSignalRings);

		this.menu.add(this.visualizationMenu);

		this.clearCache.addActionListener(this);
		this.cloneCache.addActionListener(this);

		this.dataMenu.add(this.clearCache);
		this.dataMenu.add(this.cloneCache);

		this.menu.add(this.dataMenu);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.refresh1hz) {
			this.setDesiredFps(1);
		} else if (e.getSource() == this.refresh5hz) {
			this.setDesiredFps(5);
		} else if (e.getSource() == this.refresh10hz) {
			this.setDesiredFps(10);
		} else if (e.getSource() == this.refresh15hz) {
			this.setDesiredFps(15);
		} else if (e.getSource() == this.refresh20hz) {
			this.setDesiredFps(20);
		} else if (e.getSource() == this.refresh30hz) {
			this.setDesiredFps(30);
		} else if (e.getSource() == this.windowNew) {
			this.openNewWindow();
		} else if (e.getSource() == this.visualizeRssiBars) {
			if (this.mainPanel != null) {
				this.remove(this.mainPanel);
			}
			BarChart newChart = new BarChart(DataCache.ValueType.RSSI,
					this.cache);
			this.mainPanel = newChart;
			this.displayPanel = newChart;
			this.displayPanel.setMinValue(-100f);
			this.displayPanel.setMaxValue(-20f);
			this.displayPanel.setMinFps((float) Math
					.ceil(this.desiredFps * 0.8f));
			newChart.setDisplayedId(this.currentDeviceId);
			newChart.setDeviceIsTransmitter(this.isTransmitter);
			this.titleChartType = "RSSI Bar Chart";
			this.setTitle((this.isTransmitter ? "Transmitter " : "Receiver ")
					+ this.titleDeviceName + " - " + this.titleChartType);
			this.add(newChart, BorderLayout.CENTER);
			this.validate();
		} else if (e.getSource() == this.visualizeVarianceBars) {
			if (this.mainPanel != null) {
				this.remove(this.mainPanel);
			}
			BarChart newChart = new BarChart(DataCache.ValueType.VARIANCE,
					this.cache);
			this.mainPanel = newChart;
			this.displayPanel = newChart;
			this.displayPanel.setMinValue(0f);
			this.displayPanel.setMaxValue(50f);
			this.displayPanel.setMinFps((float) Math
					.ceil(this.desiredFps * 0.8f));
			newChart.setDisplayedId(this.currentDeviceId);
			newChart.setDeviceIsTransmitter(this.isTransmitter);
			this.titleChartType = "Variance Bar Chart";
			this.setTitle((this.isTransmitter ? "Transmitter " : "Receiver ")
					+ this.titleDeviceName + " - " + this.titleChartType);
			this.add(newChart, BorderLayout.CENTER);
			this.validate();
		} else if (e.getSource() == this.visualizeRssiVoronoi) {
			if (this.mainPanel != null) {
				this.remove(this.mainPanel);
			}
			VoronoiHeatMap heatMap = new VoronoiHeatMap(ValueType.RSSI,
					this.cache);
			this.mainPanel = heatMap;
			this.displayPanel = heatMap;
			this.displayPanel.setMinValue(-100f);
			this.displayPanel.setMaxValue(-20f);
			this.displayPanel.setMinFps((float) Math
					.ceil(this.desiredFps * 0.8f));
			heatMap.setDisplayedId(this.currentDeviceId);
			heatMap.setDeviceIsTransmitter(this.isTransmitter);
			this.displayPanel
					.setDeviceIcon(this.isTransmitter ? this.transmitterIcon
							: this.receiverIcon);
			this.titleChartType = "RSSI Voronoi Map";
			this.setTitle((this.isTransmitter ? "Transmitter " : "Receiver ")
					+ this.titleDeviceName + " - " + this.titleChartType);
			this.add(heatMap, BorderLayout.CENTER);
			this.validate();

		} else if (e.getSource() == this.visualizeVarianceVoronoi) {
			if (this.mainPanel != null) {
				this.remove(this.mainPanel);
			}
			VoronoiHeatMap heatMap = new VoronoiHeatMap(ValueType.VARIANCE,
					this.cache);
			this.mainPanel = heatMap;
			this.displayPanel = heatMap;
			this.displayPanel.setMinValue(0f);
			this.displayPanel.setMaxValue(50f);
			this.displayPanel.setMinFps((float) Math
					.ceil(this.desiredFps * 0.8f));
			heatMap.setDisplayedId(this.currentDeviceId);
			heatMap.setDeviceIsTransmitter(this.isTransmitter);
			this.displayPanel
					.setDeviceIcon(this.isTransmitter ? this.transmitterIcon
							: this.receiverIcon);
			this.titleChartType = "Variance Voronoi Map";
			this.setTitle((this.isTransmitter ? "Transmitter " : "Receiver ")
					+ this.titleDeviceName + " - " + this.titleChartType);
			this.add(heatMap, BorderLayout.CENTER);
			this.validate();
		} else if (e.getSource() == this.visualizeRssiLines) {
			if (this.mainPanel != null) {
				this.remove(this.mainPanel);
			}
			LineChart newChart = new LineChart(ValueType.RSSI, this.cache);
			this.mainPanel = newChart;
			this.displayPanel = newChart;
			this.displayPanel.setMinValue(-100f);
			this.displayPanel.setMaxValue(-20f);
			this.displayPanel.setMinFps((float) Math
					.ceil(this.desiredFps * 0.8f));
			this.displayPanel.setSelfAdjustMax(false);
			this.displayPanel.setSelfAdjustMin(false);
			this.displayPanel.setDisplayedId(this.currentDeviceId);
			this.displayPanel.setDeviceIsTransmitter(this.isTransmitter);
			this.displayPanel.setMaxAge(120000);
			this.displayPanel.setDisplayLegend(true);

			this.titleChartType = "RSSI Lines";
			this.setTitle((this.isTransmitter ? "Transmitter " : "Receiver ")
					+ this.titleDeviceName + " - " + this.titleChartType);
			this.add(newChart, BorderLayout.CENTER);
			this.validate();
		} else if (e.getSource() == this.visualizeVarianceLines) {
			if (this.mainPanel != null) {
				this.remove(this.mainPanel);
			}
			LineChart newChart = new LineChart(ValueType.VARIANCE, this.cache);
			this.mainPanel = newChart;
			this.displayPanel = newChart;
			this.displayPanel.setMinValue(0f);
			this.displayPanel.setMaxValue(50f);
			this.displayPanel.setSelfAdjustMax(true);
			this.displayPanel.setSelfAdjustMin(false);
			this.displayPanel.setMinFps((float) Math
					.ceil(this.desiredFps * 0.8f));
			this.displayPanel.setDisplayedId(this.currentDeviceId);
			this.displayPanel.setDeviceIsTransmitter(this.isTransmitter);
			this.displayPanel.setMaxAge(120000);
			this.displayPanel.setDisplayLegend(true);
			this.displayPanel.setRenderFill(true);
			this.titleChartType = "Variance Lines";
			this.setTitle((this.isTransmitter ? "Transmitter " : "Receiver ")
					+ this.titleDeviceName + " - " + this.titleChartType);
			this.add(newChart, BorderLayout.CENTER);
			this.validate();

		} else if (e.getSource() == this.visualizeSignalRings) {
			if (this.mainPanel != null) {
				this.remove(this.mainPanel);
			}
			SignalToDistanceMap newMap = new SignalToDistanceMap(this.cache);
			this.mainPanel = newMap;
			this.displayPanel = newMap;
			this.displayPanel.setMinValue(-100f);
			this.displayPanel.setMaxValue(-20f);
			this.displayPanel.setSelfAdjustMax(false);
			this.displayPanel.setSelfAdjustMin(false);
			this.displayPanel.setMinFps((float) Math
					.ceil(this.desiredFps * 0.8f));
			this.displayPanel.setDisplayedId(this.currentDeviceId);
			this.displayPanel.setDeviceIsTransmitter(this.isTransmitter);
			this.displayPanel.setMaxAge(120000);
			this.displayPanel.setDisplayLegend(true);
			this.displayPanel.setRenderFill(true);
			this.displayPanel.setDeviceIcon(this.isTransmitter ? this.transmitterIcon : this.receiverIcon);
			this.titleChartType = "RSSI Rings";
			this.setTitle((this.isTransmitter ? "Transmitter " : "Receiver ")
					+ this.titleDeviceName + " - " + this.titleChartType);
			this.add(newMap, BorderLayout.CENTER);
			this.validate();

		}
		
		else if (e.getSource() instanceof JRadioButtonMenuItem) {
			boolean found = false;
			for (JRadioButtonMenuItem menuItem : this.receiverMenuItems
					.keySet()) {
				if (e.getSource() == menuItem) {
					HashableByteArray receiverId = this.receiverMenuItems
							.get(menuItem);
					this.currentDeviceId = receiverId;
					this.titleDeviceName = receiverId.toString();
					this.isTransmitter = false;
					if (this.displayPanel != null) {
						this.displayPanel.setDisplayedId(receiverId);
						this.displayPanel
								.setDeviceIsTransmitter(this.isTransmitter);
						this.displayPanel.setDeviceIcon(this.receiverIcon);
					}
					found = true;
					break;
				}

			}
			if (!found) {
				for (JRadioButtonMenuItem menuItem : this.transmitterFiduciaryItems
						.keySet()) {
					if (e.getSource() == menuItem) {
						HashableByteArray transmitterId = this.transmitterFiduciaryItems
								.get(menuItem);
						this.currentDeviceId = transmitterId;
						this.titleDeviceName = transmitterId.toString();
						this.isTransmitter = true;
						if (this.displayPanel != null) {
							this.displayPanel.setDisplayedId(transmitterId);
							this.displayPanel
									.setDeviceIsTransmitter(this.isTransmitter);
							this.displayPanel.setDeviceIcon(this.transmitterIcon);
						}
						found = true;
						break;
					}

				}
			}
			if(!found){
				for(JRadioButtonMenuItem menuItem : this.transmitterDynamicItems.keySet()) {
					if (e.getSource() == menuItem) {
						HashableByteArray transmitterId = this.transmitterDynamicItems
								.get(menuItem);
						this.currentDeviceId = transmitterId;
						this.titleDeviceName = transmitterId.toString();
						this.isTransmitter = true;
						if (this.displayPanel != null) {
							this.displayPanel.setDisplayedId(transmitterId);
							this.displayPanel
									.setDeviceIsTransmitter(this.isTransmitter);
							this.displayPanel.setDeviceIcon(this.transmitterIcon);
						}
						found=true;
						break;
					}
				}
			}
			this.setTitle((this.isTransmitter ? "Transmitter " : "Receiver ")
					+ this.titleDeviceName + " - " + this.titleChartType);
			this.validate();
		} else if(e.getSource() instanceof JCheckBoxMenuItem){
			boolean found = false;
			for(JCheckBoxMenuItem menuItem : this.sourceReceiverMenuItems.keySet()) {
				if(e.getSource() == menuItem){
					found = true;
					HashableByteArray deviceId = this.sourceReceiverMenuItems.get(menuItem);
					if(menuItem.isSelected()){
						if(deviceId != null){
							this.cache.addAllowedDevice(deviceId);
						}
						else{
							log.warn("No HashableByteArray available for {}", menuItem);
						}
					}
					else{
						if(deviceId != null){
							this.cache.removeAllowedDevice(deviceId);
						}
						else{
							log.warn("No HashableByteArray available for {}", menuItem);
						}
					}
					break;
				}
			}
			if(!found) {
				for(JCheckBoxMenuItem menuItem : this.sourceTransmitterMenuItems.keySet()){
					if(e.getSource() == menuItem){
						found = true;
						HashableByteArray deviceId = this.sourceTransmitterMenuItems.get(menuItem);
						if(menuItem.isSelected()){
							if(deviceId != null){
								this.cache.addAllowedDevice(deviceId);
							}
							else{
								log.warn("No HashableByteArray available for {}", menuItem);
							}
						}
						else{
							if(deviceId != null){
								this.cache.removeAllowedDevice(deviceId);
							}
							else{
								log.warn("No HashableByteArray available for {}", menuItem);
							}
						}
						break;
					}
				}
			}
		}
		
		else if (e.getSource() == this.clearCache) {
			this.cache.clearCachedData();
		}
		else if(e.getSource() == this.cloneCache){
			this.cloneNewWindow();
		}
		else if(e.getSource() == this.sourceReceiverClearAll){
			for(JCheckBoxMenuItem item : this.sourceReceiverMenuItems.keySet()){
				item.setSelected(false);
				this.cache.removeAllowedDevice(this.sourceReceiverMenuItems.get(item));
			}
			
		}else if(e.getSource() == this.sourceReceiverSelectAll){
			for(JCheckBoxMenuItem item : this.sourceReceiverMenuItems.keySet()){
				item.setSelected(true);
				this.cache.addAllowedDevice(this.sourceReceiverMenuItems.get(item));
			}
			
		}else if(e.getSource() == this.sourceTransmitterClearAll){
			for(JCheckBoxMenuItem item : this.sourceTransmitterMenuItems.keySet()){
				item.setSelected(false);
				this.cache.removeAllowedDevice(this.sourceTransmitterMenuItems.get(item));
			}
		}else if(e.getSource() == this.sourceTransmitterSelectAll){
			for(JCheckBoxMenuItem item : this.sourceTransmitterMenuItems.keySet()){
				item.setSelected(true);
				this.cache.addAllowedDevice(this.sourceTransmitterMenuItems.get(item));
			}
		}
	}

	protected void openNewWindow() {
		SimpleFrame newFrame = new SimpleFrame(this.cache);
		newFrame.configureDisplay();
	}
	
	protected void cloneNewWindow() {
		SimpleFrame newFrame = new SimpleFrame(this.cache.clone());
		newFrame.configureDisplay();
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent e) {
		--SimpleFrame.numWindows;

		if (SimpleFrame.numWindows <= 0) {
			log.warn("Forcing exit.");
			System.exit(0);
		}
	}

	@Override
	public void windowClosing(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent e) {
		++SimpleFrame.numWindows;
	}

	public int getDesiredFps() {
		return desiredFps;
	}

	public void setDesiredFps(int desiredFps) {
		this.desiredFps = desiredFps;
		if (this.displayPanel != null) {
			this.displayPanel.setMinFps((float) Math
					.ceil(this.desiredFps * 0.8f));
		}

		if (this.updateTask != null) {
			this.updateTask.cancel();
			this.updateTask = new TimerTask() {

				@Override
				public void run() {
					if (SimpleFrame.this.mainPanel != null) {
						SimpleFrame.this.mainPanel.repaint(20);
					}
				}
			};
			this.updateTimer.schedule(this.updateTask, 1000 / this.desiredFps,
					1000 / this.desiredFps);
		}
	}

	@Override
	public void receiverAdded(HashableByteArray receiverId) {
		JRadioButtonMenuItem newReceiverItem = new JRadioButtonMenuItem(
				receiverId.toString());
		JCheckBoxMenuItem newCheckedItem = new JCheckBoxMenuItem(receiverId.toString());
		newCheckedItem.setSelected(true);
		this.cache.addAllowedDevice(receiverId);
		if (this.receiverMenuItems.put(newReceiverItem, receiverId) == null) {
			newReceiverItem.addActionListener(this);
			this.selectedDeviceGroup.add(newReceiverItem);
			this.receiversMenu.add(newReceiverItem);
		}
		if(this.sourceReceiverMenuItems.put(newCheckedItem,receiverId) == null){
			newCheckedItem.addActionListener(this);
			this.sourceReceiversMenu.add(newCheckedItem);
		}

	}

	@Override
	public void transmitterAdded(HashableByteArray transmitterId, final boolean isFiduciary) {
		JRadioButtonMenuItem newTransmitterItem = new JRadioButtonMenuItem(
				transmitterId.toString());
		JCheckBoxMenuItem newCheckedItem = new JCheckBoxMenuItem(transmitterId.toString());
		this.cache.addAllowedDevice(transmitterId);
		newCheckedItem.setSelected(true);
		if(isFiduciary){
			if (this.transmitterFiduciaryItems.put(newTransmitterItem, transmitterId) == null) {
				this.selectedDeviceGroup.add(newTransmitterItem);
				this.transmittersFiduciaryMenu.add(newTransmitterItem);
				newTransmitterItem.addActionListener(this);
			}
		}else
		{
			if (this.transmitterDynamicItems.put(newTransmitterItem, transmitterId) == null) {	
				this.selectedDeviceGroup.add(newTransmitterItem);
				this.transmittersDynamicMenu.add(newTransmitterItem);
				newTransmitterItem.addActionListener(this);
			}
			
		}
		
		if(this.sourceTransmitterMenuItems.put(newCheckedItem,transmitterId) == null){
			newCheckedItem.addActionListener(this);
			this.sourceTransmittersMenu.add(newCheckedItem);
		}

	}

	public void startUpdates() {
		this.updateTask = new TimerTask() {

			@Override
			public void run() {
				if (SimpleFrame.this.mainPanel != null) {
					SimpleFrame.this.mainPanel.repaint(20);
				}
			}
		};
		this.updateTimer.schedule(this.updateTask, 1000 / this.desiredFps,
				1000 / this.desiredFps);
	}
}
