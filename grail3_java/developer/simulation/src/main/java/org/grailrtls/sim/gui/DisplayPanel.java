/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package org.grailrtls.sim.gui;

import java.awt.image.BufferedImage;

import org.grailrtls.libcommon.util.HashableByteArray;

public interface DisplayPanel {
	
	public void setDisplayedId(final HashableByteArray deviceId);
	
	public HashableByteArray getDisplayedId();
	
	public void setDeviceIsTransmitter(final boolean isTransmitter);
	
	public boolean isDeviceTransmitter();
	
	public void setMinValue(final float minValue);
	
	public void setMaxValue(final float maxValue);
	
	public void setMinFps(final float minFps);
	
	public void setSelfAdjustMin(final boolean selfAdjustMin);
	
	public void setSelfAdjustMax(final boolean selfAdjustMax);
	
	public void setMaxAge(final long maxAge);
	
	public void setDisplayLegend(final boolean displayLegend);
	
	public void setRenderFill(final boolean renderFill);
	
	public void setDeviceIcon(final BufferedImage icon);
}
