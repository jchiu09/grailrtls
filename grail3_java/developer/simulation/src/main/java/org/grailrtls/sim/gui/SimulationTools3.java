package org.grailrtls.sim.gui;

import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.imageio.ImageIO;

import org.grailrtls.libsolver.SolverAggregatorConnection;
import org.grailrtls.libworldmodel.client.ClientWorldConnection;
import org.grailrtls.libworldmodel.client.Response;
import org.grailrtls.libworldmodel.client.WorldState;
import org.grailrtls.libworldmodel.client.protocol.messages.Attribute;
import org.grailrtls.libworldmodel.types.DataConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimulationTools3 {

	public static final Logger log = LoggerFactory
			.getLogger(SimulationTools3.class);

	private final ClientWorldConnection world = new ClientWorldConnection();

	private final SolverAggregatorConnection agg = new SolverAggregatorConnection();

	private final ExecutorService workers = Executors.newCachedThreadPool();

	public static void main(String[] args) {
		if (args.length < 5) {
			printUsageInfo();
			System.exit(1);
		}

		int aggPort = Integer.parseInt(args[1]);

		int worldPort = Integer.parseInt(args[3]);

		SimulationTools3 tools = new SimulationTools3(args[0], aggPort,
				args[2], worldPort, args[4]);

		if (!tools.initConnections()) {
			System.err
					.println("Unable to connect to one or more servers. Please check your arguments and try again.");
			return;
		}

		tools.getStarted();
	}

	private final FilteringDataCache cache = new FilteringDataCache();

	private final String regionName;

	public SimulationTools3(final String aggHost, final int aggPort,
			final String wmHost, final int wmPort, final String regionName) {
		super();

		this.agg.setHost(aggHost);
		this.agg.setPort(aggPort);

		this.world.setHost(wmHost);
		this.world.setPort(wmPort);

		this.regionName = regionName;
	}

	public boolean initConnections() {
		if (!this.agg.connect()) {
			log.error("Unable to connect to aggregator.");
			return false;
		}

		System.out.println("Connected to " + this.agg);

		if (!this.world.connect()) {
			log.error("Unable to connect to world model.");
			return false;
		}

		System.out.println("Connected to " + this.world);

		return true;
	}

	public void getStarted() {
		this.cache.setRegionUri(this.regionName);
		SimpleFrame frame = new SimpleFrame(this.cache);
		frame.configureDisplay();
		String[] matchingUris = this.world.searchURI(this.regionName);
		this.handleRegionUris(matchingUris);
	}
	
	/*
	 *      winlab.anchor.pipsqueak.receiver.667
        winlab.anchor.pipsqueak.repeater.220
        winlab.anchor.pipsqueak.transmitter.112
     
	 */

	protected void handleRegionUris(String[] matchingUris) {
		long now = System.currentTimeMillis();
		for (String uri : matchingUris) {
			Response res = this.world.getSnapshot(uri, now, now,
					"dimension\\..*", "image\\.url");
			double width = 0;
			double height = 0;
			String imageUrl = null;
			try {
				WorldState state = res.get();
				for (String stateUri : state.getURIs()) {
					Collection<Attribute> attributes = state.getState(stateUri);
					for (Attribute attrib : attributes) {
						if ("dimension.width".equals(attrib.getAttributeName())) {
							width = ((Double) DataConverter
									.decodeUri(attrib.getAttributeName(),
											attrib.getData())).doubleValue();
						} else if ("dimension.height".equals(attrib
								.getAttributeName())) {
							height = ((Double) DataConverter
									.decodeUri(attrib.getAttributeName(),
											attrib.getData())).doubleValue();
						} else if ("image.url"
								.equals(attrib.getAttributeName())) {
							imageUrl = (String)DataConverter.decodeUri(attrib.getAttributeName(), attrib.getData());
						}
					}
				}
			} catch (Exception e) {
				System.err.println("Couldn't retrieve dimension data for "
						+ uri);
			}

			if (width != 0 && height != 0) {
				this.cache.setRegionBounds(new Rectangle2D.Double(0, 0, width,
						height));
				System.out.println("Set region bounds.");
			}
			if(imageUrl != null){
				BufferedImage regionImage;
				try {
					
					if(!imageUrl.startsWith("http://")){
						imageUrl = "http://" + imageUrl;
					}
					regionImage = ImageIO.read(new URL(imageUrl));
					this.cache.setRegionImage(regionImage);
					System.out.println("Set image for " + uri + " as \""+imageUrl + "\".");
				} catch (MalformedURLException e) {
					log.warn("Malformed URL: {}", imageUrl);
					e.printStackTrace();
				} catch (IOException e) {
				log.warn("Unable to load region image URL {} due to an exception.", imageUrl, e);
					e.printStackTrace();
				}
			}
		}
	}

	protected static void printUsageInfo() {
		System.out
				.println("Parameters: <Aggregator Host> <Aggregator Port> <World Model Host> <World Model Client Port> <Region Name>");
	}

}
