/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.sim;

import java.util.Random;

import org.grailrtls.libsensor.SensorAggregatorInterface;
import org.grailrtls.libsensor.listeners.ConnectionListener;
import org.grailrtls.libsensor.protocol.messages.SampleMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A sensor simulator that simply generates sample messages at regular intervals.  Can be adapted to provide
 * more complex/specialized streams of generated data. 
 * @author Robert Moore
 *
 */
public class FakeSensor implements ConnectionListener, Runnable {

	/**
	 * Logging facility for this class.
	 */
	public static final Logger log = LoggerFactory.getLogger(FakeSensor.class);
	
	/**
	 * Flag to keep running.
	 */
	protected boolean keepRunning = true;

	/**
	 * Flag to indicate whether the aggregator interface is ready for sample messages.
	 */
	protected boolean canSendSamples = false;
	
	/**
	 * How long to wait between sample messages (in milliseconds).
	 */
	protected long sampleDelay = 1000l;
	
	/**
	 * The hostname of the aggregator.
	 */
	protected String host = null;
	
	/**
	 * The port number of the aggregator
	 */
	protected int port = -1;
	
	/**
	 * Aggregator interface.  Handles all of the heavy network lifting.
	 */
	protected SensorAggregatorInterface aggregatorInterface = new SensorAggregatorInterface();
	
	/**
	 * The unique number of this sensor within the process.
	 */
	protected int sensorNum = 0;

	/**
	 * Parses command-line arguments are starts the fake sensor.
	 * @param args
	 */
	public static void main(String[] args) {
		log.debug("FakeSensor starting up.");

		String aggHost = "localhost";
		int aggPort = 7007;

		// Need at least the host and port number
		if (args.length < 2) {
			printUsageInfo();
			System.exit(1);
		}

		aggHost = args[0];
		aggPort = Integer.parseInt(args[1]);
		
		int sampleDelay = -1;
		int numSensors = 1;
		
		// Parse any optional command-line arguments
		for(int argIndex = 2; argIndex < args.length; ++argIndex)
		{
			// Sample delay
			if ("-D".equals(args[argIndex])) {
				// Make sure there is one more parameter available.
				if (args.length > argIndex++) {
					try {
						sampleDelay =  Integer
								.parseInt(args[argIndex]);
					} catch (Exception e) {
						System.err
								.println("Unable to parse sample delay value.");
						e.printStackTrace(System.err);
						printUsageInfo();
						System.exit(1);
					}
				} else {
					System.err.println("Missing sample delay value.");
					printUsageInfo();
					System.exit(1);
				}
			}
			// Number of sensors to spawn
			else if ("-N".equals(args[argIndex])) {
				// Make sure there is one more parameter available.
				if (args.length > argIndex++) {
					try {
						numSensors=  Integer
								.parseInt(args[argIndex]);
					} catch (Exception e) {
						System.err
								.println("Unable to determine number of sensors.");
						e.printStackTrace(System.err);
						printUsageInfo();
						System.exit(1);
					}
				} else {
					System.err.println("Missing number of sensors.");
					printUsageInfo();
					System.exit(1);
				}
			}
		}

		// Create an array of fake sensors (in case we want to spawn multiple)
		FakeSensor sensors[] = new FakeSensor[numSensors];
		for(int i = 0; i < sensors.length; ++i)
		{
			sensors[i] = new FakeSensor(i);
			sensors[i].setHost(aggHost);
			sensors[i].setPort(aggPort);
			if(sampleDelay > 0)
			{
				sensors[i].setSampleDelay(sampleDelay);
			}
		}
		
		log.info("Created {} fake sensors.",Integer.valueOf(sensors.length));
		Thread threads[] = new Thread[sensors.length];
		
		for(int i = 0; i < sensors.length; ++i)
		{
			threads[i] = new Thread(sensors[i], "Sensor " + i);
			threads[i].start();
			
			// Be sure to put a delay between thread start-up, otherwise we might thrash the CPU a bit too much
			try {
				Thread.sleep(500);
			}
			catch(InterruptedException ie)
			{
				// Ignored
			}
		}
		
		// Wait for all of the threads to terminate
		for(Thread thread : threads)
		{
			try {
				thread.join();
				log.info("{} finished running.", thread);
			} catch (InterruptedException e) {
				// Ignored
			}
		}
		
	}

	/**
	 * Prints out details about what command-line parameters are supported and what they do.
	 */
	protected static void printUsageInfo() {
		StringBuffer sb = new StringBuffer();

		sb.append("Parameters: <host> <port> [-D <INT>] [-N <INT>]");
		sb.append("\n\t");
		sb.append("-D : Followed by an integer, indicates how long to wait between samples, in milliseconds.\n\t");
		sb.append("-N : Followed by an integer, indicates how many sensors to start.");
		
		System.err.println(sb.toString());
	}
	
	public FakeSensor(){
	}
	
	public FakeSensor(final int num){
		this.sensorNum = num;
	}

	/**
	 * Causes this fake solver to exit its run method.
	 */
	@Override
	public void connectionEnded(SensorAggregatorInterface aggregator) {
		// Need to exit now, since the aggregator interface has "given up" on
		// reconnecting
		log.warn("Lost connection to {}.", aggregator);
		this.keepRunning = false;
	}

	/**
	 * Simply prints a little debug message when we've connected to the aggregator.
	 */
	@Override
	public void connectionEstablished(SensorAggregatorInterface aggregator) {
		log.debug("Connected to {}", aggregator);
	}

	/**
	 * Stops sending samples to the aggregator.
	 */
	@Override
	public void connectionInterrupted(SensorAggregatorInterface aggregator) {
		log.info("Connection temporarily interrupted to {}.",aggregator);
		this.canSendSamples = false;
	}

	/**
	 * Enables this fake sensor to send samples to the aggregator.
	 */
	@Override
	public void readyForSamples(SensorAggregatorInterface aggregator) {
		log.info("Can begin sending samples to {}.",aggregator);
		this.canSendSamples = true;
	}

	/**
	 * Establishes a connection to the aggregator and sends samples once the
	 * connection is ready.
	 */
	@Override
	public void run() {
		if(this.host == null || this.port < 0 || this.sampleDelay < 1)
		{
			log.warn("Missing one or more required parameters.");
			return;
		}

		this.aggregatorInterface.setHost(this.host);
		this.aggregatorInterface.setPort(this.port);
		this.aggregatorInterface.addConnectionListener(this);
		
		log.debug("Attempting connection to {}:{}", this.host, Integer.valueOf(this.port));
		
		if(!this.aggregatorInterface.doConnectionSetup())
		{
			log.warn("Aggregator connection failed.");
			return;
		}
		
		Random rand = new Random(System.currentTimeMillis());
		
		while(this.keepRunning)
		{
			try {
				log.debug("Sleeping {}ms",Long.valueOf(this.sampleDelay));
				Thread.sleep(this.sampleDelay);
			}
			catch(InterruptedException ie)
			{
				// Ignored
			}
			
			if(this.canSendSamples)
			{
				log.debug("Sending sample.");
				SampleMessage message = SampleMessage.getTestMessage();
				byte[] data = new byte[2];
				int temp = rand.nextInt(100);
				data[0] = (byte)(temp >> 8);
				data[1] = (byte)(temp);
				message.setSensorData(data);
				byte[] buff = message.getReceiverId();
				buff[buff.length-1] = (byte)this.sensorNum;
				buff = message.getDeviceId();
				buff[0] = (byte)rand.nextInt(this.sensorNum+1);
				this.aggregatorInterface.sendSample(message);
			}
		}
		
	}


	/**
	 * Sets how long to wait between sending each sample.
	 * @param sampleDelay how long to wait between samples, in milliseconds.
	 */
	public void setSampleDelay(long sampleDelay) {
		this.sampleDelay = sampleDelay;
	}

	/**
	 * Sets the host name/IP of the aggregator.
	 * @param host the host name or IP address of the aggregator.
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * Sets the port number of the aggregator.
	 * @param port the port number of the aggregator.
	 */
	public void setPort(int port) {
		this.port = port;
	}
}
