/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and John-Austen Francisco
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */


package org.grailrtls.sim;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.mina.proxy.utils.ByteUtilities;

import org.grailrtls.libsensor.SensorAggregatorInterface;
import org.grailrtls.libsensor.listeners.ConnectionListener;
import org.grailrtls.libsolver.SolverAggregatorInterface;
import org.grailrtls.libsolver.listeners.SampleListener;
import org.grailrtls.libsolver.protocol.messages.SampleMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SysoutSolver implements ConnectionListener,
		org.grailrtls.libsolver.listeners.ConnectionListener, SampleListener {

	private static final Logger log = LoggerFactory
			.getLogger(SysoutSolver.class);

	protected final ConcurrentLinkedQueue<SolverAggregatorInterface> inputAggregators = new ConcurrentLinkedQueue<SolverAggregatorInterface>();

	protected final ConcurrentLinkedQueue<SensorAggregatorInterface> outputAggregators = new ConcurrentLinkedQueue<SensorAggregatorInterface>();

	protected final ExecutorService workers = Executors
			.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

	@Override
	public void connectionEnded(SensorAggregatorInterface aggregator) {
		aggregator.removeConnectionListener(this);
		this.outputAggregators.remove(aggregator);
		log.info("Connection to {} ended. No longer forwarding samples.",
				aggregator);
	}

	@Override
	public void connectionEstablished(SensorAggregatorInterface aggregator) {
		log.info("Connecting to {} to send samples.", aggregator);
	}

	@Override
	public void connectionInterrupted(SensorAggregatorInterface aggregator) {
		this.outputAggregators.remove(aggregator);
		log
				.warn("Connection to {} was interrupted. Samples will not be sent until the connection resumes.");

	}

	@Override
	public void readyForSamples(SensorAggregatorInterface aggregator) {
		this.outputAggregators.add(aggregator);
		log.info("Ready to send samples to {}", aggregator);
	}

	@Override
	public void connectionEnded(SolverAggregatorInterface aggregator) {
		aggregator.removeConnectionListener(this);
		aggregator.removeSampleListener(this);
		this.inputAggregators.remove(aggregator);
		log.info("Connection to {} ended.  No longer receiving samples.",
				aggregator);
	}

	@Override
	public void connectionEstablished(SolverAggregatorInterface aggregator) {
		this.inputAggregators.add(aggregator);
		log.info("Connecting to {} to receive samples.", aggregator);
	}

	@Override
	public void connectionInterrupted(SolverAggregatorInterface aggregator) {
		log
				.info(
						"Connection to {} was interrupted.  Samples will not be sent until the connection resumes.",
						aggregator);
	}

	@Override
	public void sampleReceived(final SolverAggregatorInterface aggregator,
			final SampleMessage sample) {
		this.workers.execute(new Runnable() {

			@Override
			public void run() {
				SysoutSolver.this.processSample(aggregator, sample);

			}
		});
	}

	protected void processSample(final SolverAggregatorInterface aggregator,
			final SampleMessage sample) {

		System.out.println();
		//System.out.println("Dev ID: "+sample.getDeviceId());
		System.out.println("Dev ID: "+ByteUtilities.asHex(sample.getDeviceId()).substring(20,32) );
		System.out.println("Recv ID: "+ByteUtilities.asHex(sample.getReceiverId()).substring(20,32) );
		System.out.println("PHY Type: "+sample.getPhysicalLayer());
		System.out.println("Recv TS: "+sample.getReceiverTimeStamp());
		System.out.println("RSSI: "+sample.getRssi());
		//System.out.println("Sensor Data: "+sample.getSensorData());
		System.out.println("Sensed Data asHex?: "+ByteUtilities.asHex(sample.getSensedData()) );


		// Turn it into a sensing-layer sample
		/*
		org.grailrtls.sensor.protocol.messages.SampleMessage sendSample = new org.grailrtls.sensor.protocol.messages.SampleMessage();
		sendSample.setDeviceId(sample.getDeviceId());
		sendSample.setReceiverId(sample.getReceiverId());
		sendSample.setPhysicalLayer(sample.getPhysicalLayer());
		sendSample.setReceivedTimestamp(sample.getReceiverTimeStamp());
		sendSample.setRssi(sample.getRssi());
		sendSample.setSensorData(sample.getSensedData());

		for (SensorAggregatorInterface outAgg : this.outputAggregators) {
			if (outAgg.isCanSendSamples()) {
				outAgg.sendSample(sendSample);
			}
		
		}
		*/
	}
	
	public void addOutput(final String hostname, final Integer port){
		SensorAggregatorInterface newAgg = new SensorAggregatorInterface();
		newAgg.setHost(hostname);
		newAgg.setPort(port.intValue());
		newAgg.addConnectionListener(this);
		newAgg.setStayConnected(true);
		// TODO: Make sure aggregators get reconnected
		if(newAgg.doConnectionSetup())
		{
			log.info("Connection for outgoing samples succeeded to {}", newAgg);
		}
		else
		{
			log.error("Failed connection for outgoing samples to {}", newAgg);
		}
	}
	
	public void addInput(final String hostname, final Integer port){
		SolverAggregatorInterface newAgg = new SolverAggregatorInterface();
		newAgg.setHost(hostname);
		newAgg.setPort(port.intValue());
		newAgg.addConnectionListener(this);
		newAgg.addSampleListener(this);
		newAgg.setStayConnected(true);
		if(newAgg.doConnectionSetup()){
			log.info("Connection for incoming samples succeeded to {}", newAgg);
		}
		else
		{
			log.warn("Connection for incoming samples failed to {}", newAgg);
		}
	}
	
	public void removeInput(final SolverAggregatorInterface aggregator)
	{
		this.inputAggregators.remove(aggregator);
		aggregator.doConnectionTearDown();
	}
	
	public void removeOutput(final SensorAggregatorInterface aggregator){
		this.outputAggregators.remove(aggregator);
		aggregator.doConnectionTearDown();
	}
	
	public static void main(String[]args)
	{
		// Expects --input host port --output host port ...
		// Alternatives -i host port -o host port
		// Basically flag followed by host, port #.
		// -i or --input for receiving samples
		// -o or --output for sending/forwarding samples
		List<String> inputHosts = new ArrayList<String>();
		List<Integer> inputPorts = new ArrayList<Integer>();
		
		List<String> outputHosts = new ArrayList<String>();
		List<Integer> outputPorts = new ArrayList<Integer>();
		
		for(int i = 0; i < args.length; ++i)
		{
			// Process input host and port
			if("-i".equals(args[i]) || "--input".equals(args[i])){
				String host = args[++i];
				Integer port = Integer.valueOf(args[++i]);
				inputHosts.add(host);
				inputPorts.add(port);
			}
			/*
			else if("-o".equals(args[i]) || "--output".equals(args[i]))
			{
				String host = args[++i];
				Integer port = Integer.valueOf(args[++i]);
				outputHosts.add(host);
				outputPorts.add(port);
			}
			*/
			else
			{
				log.warn("Unknown option {}. Skipping...", args[i]);
			}
		}
		
		SysoutSolver solver = new SysoutSolver();
		for(int i = 0; i < inputHosts.size(); ++i)
		{
			solver.addInput(inputHosts.get(i), inputPorts.get(i));
		}
		/*
		for(int i = 0; i < outputHosts.size(); ++i)
		{
			solver.addOutput(outputHosts.get(i), outputPorts.get(i));
		}
		*/
	}
}
