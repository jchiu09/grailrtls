/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.sim.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.grailrtls.libcommon.util.FieldDecoder;
import org.grailrtls.libcommon.util.HashableByteArray;
import org.grailrtls.libcommon.util.OnlineVariance;
import org.grailrtls.libsolver.SolverAggregatorInterface;
import org.grailrtls.libsolver.listeners.SampleListener;
import org.grailrtls.libsolver.protocol.messages.SampleMessage;
import org.grailrtls.libworldmodel.client.ClientWorldModelInterface;
import org.grailrtls.libworldmodel.client.listeners.ConnectionListener;
import org.grailrtls.libworldmodel.client.listeners.DataListener;
import org.grailrtls.libworldmodel.client.protocol.messages.AbstractRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.Attribute;
import org.grailrtls.libworldmodel.client.protocol.messages.AttributeAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.DataResponseMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginPreferenceMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.SnapshotRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.URISearchResponseMessage;
import org.grailrtls.sim.gui.panels.HeatStripes;
import org.grailrtls.sim.gui.panels.LineChart;
import org.grailrtls.sim.gui.panels.ScatterPlotPanel;
import org.grailrtls.sim.gui.panels.SignalLineMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AggregatorStatisticsPanel extends JPanel implements
		SampleListener, DataListener, ConnectionListener, MouseListener {

	private static final Logger log = LoggerFactory
			.getLogger(AggregatorStatisticsPanel.class);

	protected SolverAggregatorInterface solver;

	protected ClientWorldModelInterface worldServer;

	protected JLabel samplesPerSecond = new JLabel("[UNKNOWN]");

	protected ScatterPlotPanel signalPlot = new ScatterPlotPanel();

	protected ScatterPlotPanel variancePlot = new ScatterPlotPanel();

	protected SignalLineMap signalLineMap = new SignalLineMap();

	protected SignalLineMap varianceLineMap = new SignalLineMap();

	protected HeatStripes varianceStripes = new HeatStripes();

	protected float smoothedSPS = 0;

	protected float smoothingWeight = 0.875f;

	protected final ConcurrentHashMap<HashableByteArray, Point2D> transmitterLocations = new ConcurrentHashMap<HashableByteArray, Point2D>();

	protected final ConcurrentHashMap<HashableByteArray, Point2D> receiverLocations = new ConcurrentHashMap<HashableByteArray, Point2D>();

	protected final ConcurrentHashMap<HashableByteArray, ConcurrentHashMap<HashableByteArray, OnlineVariance>> varianceReceiverTransmitter = new ConcurrentHashMap<HashableByteArray, ConcurrentHashMap<HashableByteArray, OnlineVariance>>();

	protected String regionName = null;

	protected String regionImageUri = null;

	/**
	 * Formatter for printing floating point numbers with 4 decimal places of
	 * precision.
	 */
	private static final DecimalFormat samplesPerSecFmt = new DecimalFormat(
			"###,##0.0");

	/**
	 * Number of samples received since the statistics were last generated.
	 */
	private int samplesReceived = 0;

	/**
	 * Mean latency of samples received from the aggregator. This value assumes
	 * that the sensors are sending valid timestamps to the aggregator.
	 */
	private float meanReceiveLatency = 0;

	/**
	 * The last time that statistics were generated.
	 */
	private long lastReportTime = System.currentTimeMillis();

	/**
	 * Number of bytes read from the aggregator since the last time statistics
	 * were generated.
	 */
	private long bytesRead = 0;

	/**
	 * Number of bytes written to the aggregator since the last time statistics
	 * were generated.
	 */
	private long bytesWritten = 0;

	private final Timer updateTimer = new Timer();

	private TimerTask updateTask = null;

	protected long statsUpdatePeriod = 50;

	protected long statsTimeHistory = 1000 * 60;

	protected long spsRefreshPeriod = 100;

	protected int desiredFps = 5;

	protected int minFps = 3;

	protected final ExecutorService workers = Executors
			.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

	public AggregatorStatisticsPanel(final SolverAggregatorInterface solver,
			final ClientWorldModelInterface worldServer) {
		this.solver = solver;
		this.worldServer = worldServer;

		this.solver.addSampleListener(this);
		this.worldServer.addDataListener(this);
		this.worldServer.addConnectionListener(this);

		this.worldServer.registerSearchRequest("winlab");
		this.worldServer.registerSearchRequest("*.receiver.*");
		this.worldServer.registerSearchRequest("*.transmitter.*");

		this.signalPlot.setMinYValue(-100f);
		this.signalPlot.setMaxYValue(-20f);
		this.signalPlot.setMinXValue(0f);
		this.signalPlot.setMaxXValue(150f);
		this.signalPlot.setMaxElements(1000);
		this.signalPlot.setDisplayedInfo("WINLAB");
		this.signalPlot.addMouseListener(this);

		this.variancePlot.setMinYValue(0f);
		this.variancePlot.setMaxYValue(80f);
		this.variancePlot.setMinXValue(0f);
		this.variancePlot.setMaxXValue(150f);
		this.variancePlot.setMaxElements(500);
		this.variancePlot.setSelfAdjustMax(true);
		this.variancePlot.setDisplayedInfo("WINLAB");
		this.variancePlot.addMouseListener(this);

		this.signalLineMap.setMinValue(-100f);
		this.signalLineMap.setMaxValue(-20f);
		this.signalLineMap.addMouseListener(this);

		this.varianceLineMap.setMinValue(1.1f);
		this.varianceLineMap.setMaxValue(20f);
		this.varianceLineMap.addMouseListener(this);

		this.varianceStripes.setMinValue(0f);
		this.varianceStripes.setMaxValue(50f);
		this.varianceStripes.setThresholdValue(2f);
		this.varianceStripes.setDrawVerticalLines(true);
		this.varianceStripes.setMaxAge(20000l);
		this.varianceStripes.addMouseListener(this);

		// Samples per second graph
		this.setLayout(new BorderLayout());
		JPanel tempPanel = new JPanel(new GridLayout(1, 1));
		tempPanel.setBorder(BorderFactory.createTitledBorder("Sample Rate"));
		this.add(tempPanel, BorderLayout.SOUTH);

		// Signal Maps
		JTabbedPane tabPane = new JTabbedPane();

		// Signal to Distance scatterplot
		// tempPanel = new JPanel();
		// tempPanel.setLayout(new GridLayout(1, 1));
		// tempPanel.setBorder(BorderFactory
		// .createTitledBorder("Signal to Distance"));
		// tempPanel.add(this.signalPlot);

		tabPane.add(this.signalPlot, "Signal to Distance");
		tabPane.add(this.variancePlot, "Variance to Distance");
		tabPane.add(this.signalLineMap, "RSSI Line Map");
		tabPane.add(this.varianceLineMap, "Var. Line Map");
		tabPane.add(this.varianceStripes, "Var. Stripes");

		this.add(tabPane, BorderLayout.CENTER);

		this.setPreferredSize(new Dimension(800, 600));
	}

	protected JPanel createTitledPanel(String title) {
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createTitledBorder(title));
		return panel;
	}

	protected void updateFps() {
		this.signalPlot.setMinFps(this.minFps);
		this.variancePlot.setMinFps(this.minFps);
		this.signalLineMap.setMinFps(this.minFps);
		this.varianceLineMap.setMinFps(this.minFps);
		this.varianceStripes.setMinFps(this.minFps);
	}

	public void startUpdates() {
		this.updateTask = new TimerTask() {

			@Override
			public void run() {
				AggregatorStatisticsPanel.this.repaintMembers();
			}
		};
		this.updateTimer.schedule(this.updateTask, 1000 / this.desiredFps,
				1000 / this.desiredFps);
		this.updateTimer.schedule(new TimerTask() {

			@Override
			public void run() {
				AggregatorStatisticsPanel.this.updateInfo();
			}
		}, 200, 200);

	}

	@Override
	public void sampleReceived(SolverAggregatorInterface aggregator,
			final SampleMessage sample) {
		this.workers.execute(new Runnable() {

			@Override
			public void run() {
				AggregatorStatisticsPanel.this.processSample(sample);
			}
		});
	}

	protected void processSample(final SampleMessage sample) {
		++this.samplesReceived;
		this.updateVariances(sample);
		this.updateSignalScatter(sample);
		this.updateVarianceScatter(sample);

		this.signalLineMap.addValue(sample.getReceiverId(),
				sample.getDeviceId(), sample.getRssi());

	}

	protected void updateVariances(final SampleMessage sample) {
		HashableByteArray devHash = new HashableByteArray(sample.getDeviceId());
		HashableByteArray recHash = new HashableByteArray(
				sample.getReceiverId());
		if (devHash == null || recHash == null)
			return;

		ConcurrentHashMap<HashableByteArray, OnlineVariance> varianceByTransmitter = this.varianceReceiverTransmitter
				.get(recHash);
		if (varianceByTransmitter == null) {
			varianceByTransmitter = new ConcurrentHashMap<HashableByteArray, OnlineVariance>();
			this.varianceReceiverTransmitter
					.put(recHash, varianceByTransmitter);
		}

		OnlineVariance variance = varianceByTransmitter.get(devHash);

		if (variance == null) {
			variance = new OnlineVariance();
			variance.setMaxAge(5000);
			variance.setMaxHistory(10);
			varianceByTransmitter.put(devHash, variance);
		}

		float v = variance.addValue(sample.getRssi());
		this.varianceLineMap.addValue(sample.getReceiverId(),
				sample.getDeviceId(), v);

		this.varianceStripes.addValue(recHash, devHash, v);
	}

	protected void updateSignalScatter(SampleMessage sample) {
		if (sample == null)
			return;
		HashableByteArray receiverHash = new HashableByteArray(
				sample.getReceiverId());
		HashableByteArray transmitterHash = new HashableByteArray(
				sample.getDeviceId());

		Point2D receiverPoint = this.receiverLocations.get(receiverHash);
		if (receiverPoint == null) {
			return;
		}
		Point2D transmitterPoint = this.transmitterLocations
				.get(transmitterHash);
		if (transmitterPoint == null) {
			return;
		}

		double xDiff = receiverPoint.getX() - transmitterPoint.getX();
		double yDiff = receiverPoint.getY() - transmitterPoint.getY();

		double distance = Math.sqrt(Math.pow(xDiff, 2) + Math.pow(yDiff, 2));

		this.signalPlot.addPoint((float) distance, sample.getRssi());

	}

	protected void updateVarianceScatter(SampleMessage sample) {
		if (sample == null)
			return;
		HashableByteArray receiverHash = new HashableByteArray(
				sample.getReceiverId());
		HashableByteArray transmitterHash = new HashableByteArray(
				sample.getDeviceId());

		Point2D receiverPoint = this.receiverLocations.get(receiverHash);
		if (receiverPoint == null) {
			return;
		}
		Point2D transmitterPoint = this.transmitterLocations
				.get(transmitterHash);
		if (transmitterPoint == null) {
			return;
		}

		double xDiff = receiverPoint.getX() - transmitterPoint.getX();
		double yDiff = receiverPoint.getY() - transmitterPoint.getY();

		double distance = Math.sqrt(Math.pow(xDiff, 2) + Math.pow(yDiff, 2));

		ConcurrentHashMap<HashableByteArray, OnlineVariance> varianceByTransmitter = this.varianceReceiverTransmitter
				.get(receiverHash);
		if (varianceByTransmitter == null) {
			varianceByTransmitter = new ConcurrentHashMap<HashableByteArray, OnlineVariance>();
			this.varianceReceiverTransmitter.put(receiverHash,
					varianceByTransmitter);
		}

		OnlineVariance variance = varianceByTransmitter.get(transmitterHash);

		if (variance == null) {
			variance = new OnlineVariance();
			varianceByTransmitter.put(transmitterHash, variance);
		}

		this.variancePlot.addPoint((float) distance,
				variance.getCurrentVariance());

	}

	public void updateInfo() {
		long now = System.currentTimeMillis();
		float sps = this.samplesReceived
				/ ((now - this.lastReportTime) / 1000f);

		this.smoothedSPS = sps * (1 - this.smoothingWeight) + this.smoothedSPS
				* this.smoothingWeight;
		if (this.smoothedSPS < 0.01f) {
			this.smoothedSPS = 0f;
		}

		this.samplesPerSecond.setText(samplesPerSecFmt.format(this.smoothedSPS)
				+ " Samples per second");

		if (this.solver.getSession() != null) {
			long newBytesRead = this.solver.getSession().getReadBytes();
			long newBytesWritten = this.solver.getSession().getWrittenBytes();
			this.bytesRead = newBytesRead;
			this.bytesWritten = newBytesWritten;
		}

		this.samplesReceived = 0;
		this.meanReceiveLatency = 0;
		this.lastReportTime = now;

	}

	protected void repaintMembers() {
		this.signalPlot.repaint();
		this.variancePlot.repaint();
		this.signalLineMap.repaint();
		this.varianceLineMap.repaint();
		this.varianceStripes.repaint();
	}

	public static byte[] makeIdFromPipString(String uri) {
		int lastDot = uri.lastIndexOf(".");
		if (lastDot < 0)
			return null;
		String pipIdString = uri.substring(lastDot + 1, uri.length());
		try {
			Integer fullValue = Integer.valueOf(pipIdString);
			byte[] fullBytes = new byte[SampleMessage.DEVICE_ID_SIZE];
			fullBytes[15] = (byte) (fullValue.intValue() & 0xFF);
			fullBytes[14] = (byte) ((fullValue.intValue() >> 8) & 0xFF);
			return fullBytes;
		} catch (NumberFormatException nfe) {
			log.error("Error while parsing {}", uri, nfe);
			return null;
		}
	}

	@Override
	public void uriSearchResponseReceived(ClientWorldModelInterface worldModel,
			URISearchResponseMessage searchResponseMessage) {
		if (searchResponseMessage.getMatchingUris() == null) {
			return;
		}
		for (String uri : searchResponseMessage.getMatchingUris()) {
			SnapshotRequestMessage message = new SnapshotRequestMessage();
			message.setBeginTimestamp(0l);
			message.setEndTimestamp(System.currentTimeMillis());
			message.setQueryURI(uri);
			message.setQueryAttributes(new String[] { "*" });

			this.worldServer.sendMessage(message);
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON1) {
			return;
		}

		// Toggle Anti-Aliasing for scatterplot and line chart
		if (e.getSource() instanceof LineChart) {
			LineChart chart = (LineChart) e.getSource();
			chart.setEnableAntiAliasing(!chart.isEnableAntiAliasing());
		} else if (e.getSource() instanceof ScatterPlotPanel) {
			ScatterPlotPanel plot = (ScatterPlotPanel) e.getSource();
			plot.setEnableAntiAliasing(!plot.isEnableAntiAliasing());
		} else if (e.getSource() instanceof SignalLineMap) {
			SignalLineMap map = (SignalLineMap) e.getSource();
			map.setEnableAlpha(!map.isEnableAlpha());
		} else if (e.getSource() instanceof HeatStripes) {
			HeatStripes stripes = (HeatStripes) e.getSource();
			stripes.setEnableAntiAliasing(!stripes.isEnableAntiAliasing());
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	public int getDesiredFps() {
		return desiredFps;
	}

	public void setDesiredFps(int desiredFps) {
		this.desiredFps = desiredFps;
		this.updateFps();
		if (this.updateTask != null) {
			this.updateTask.cancel();
			this.updateTask = new TimerTask() {

				@Override
				public void run() {
					// AggregatorStatisticsPanel.this.updateInfo();
					AggregatorStatisticsPanel.this.repaintMembers();
				}
			};
			this.updateTimer.schedule(this.updateTask, 1000l / this.desiredFps,
					1000l / this.desiredFps);
		}
	}

	public int getMinFps() {
		return minFps;
	}

	public void setMinFps(int minFps) {
		this.minFps = minFps;
		this.updateFps();
	}

	protected void setRegionImageUri(final String regionImageUri) {
		this.regionImageUri = regionImageUri;
		if (this.regionImageUri.indexOf("http://") == -1) {
			this.regionImageUri = "http://" + this.regionImageUri;
		}
		try {
			BufferedImage origImage = ImageIO
					.read(new URL(this.regionImageUri));
			// BufferedImage invert = negative(origImage);
			this.signalLineMap.setBackgroundImage(origImage);
			this.varianceLineMap.setBackgroundImage(origImage);

		} catch (MalformedURLException e) {
			log.warn("Invalid region URI: {}", this.regionImageUri);
			e.printStackTrace();
		} catch (IOException e) {
			log.warn("Could not load region URI at {}.", this.regionImageUri);
			e.printStackTrace();
		}

	}

	@Override
	public void connectionInterrupted(ClientWorldModelInterface worldModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connectionEnded(ClientWorldModelInterface worldModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connectionEstablished(ClientWorldModelInterface worldModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void requestCompleted(ClientWorldModelInterface worldModel,
			AbstractRequestMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dataResponseReceived(ClientWorldModelInterface worldModel,
			DataResponseMessage message) {
		if (message.getUri().equals("winlab")) {
			double width = -1f;
			double height = -1f;
			String mapURL = null;
			for (Attribute field : message.getAttributes()) {
				if ("width".equals(field.getAttributeName())) {
					width = (Double) FieldDecoder.decodeField(
							"double", field.getData());
				} else if ("height".equals(field.getAttributeName())) {
					height = (Double) FieldDecoder.decodeField(
							"double", field.getData());
				} else if ("map url".equals(field.getAttributeName())) {
					mapURL = (String) FieldDecoder.decodeField(
							"string", field.getData());
					continue;
				}
			}

			if (width > 0 && height > 0) {
				Rectangle2D regionBounds = new Rectangle2D.Double(0, 0, width,
						height);
				this.signalLineMap.setRegionBounds(regionBounds);
				this.varianceLineMap.setRegionBounds(regionBounds);
			}
			this.setRegionImageUri(mapURL);
			this.regionName = message.getUri();

		} else if (message.getUri().startsWith(
				"pipsqueak.receiver")) {
			byte[] pipId = makeIdFromPipString(message.getUri());
			if (pipId == null) {
				log.debug("Null receiver id for {}",
						message.getUri());
				return;
			}
			double x = -1;
			double y = -1;

			for (Attribute field : message.getAttributes()) {
				if ("location.x".equals(field.getAttributeName())) {
					x = (Double) FieldDecoder.decodeField("double",
							field.getData());
				} else if ("location.y".equals(field.getAttributeName())) {
					y = (Double) FieldDecoder.decodeField("double",
							field.getData());
				}
			}
			if (x > 0 && y > 0) {
				HashableByteArray receiverHash = new HashableByteArray(pipId);
				Point2D point = new Point2D.Float((float) x, (float) y);
				this.receiverLocations.put(receiverHash, point);
				this.signalLineMap.setReceiverLocation(receiverHash, (float) x,
						(float) y);
				this.varianceLineMap.setReceiverLocation(receiverHash,
						(float) x, (float) y);
				this.varianceStripes.addReceiver(receiverHash);
			}
		} else if (message.getUri().startsWith(
				"pipsqueak.transmitter")) {
			byte[] pipId = makeIdFromPipString(message.getUri());
			if (pipId == null) {
				log.debug("Null transmitter id for {}",
						message.getUri());
				return;
			}
			double x = -1;
			double y = -1;

			if (message.getAttributes() == null) {
				return;
			}

			for (Attribute field : message.getAttributes()) {
				if ("location.x".equals(field.getAttributeName())) {
					x = (Double) FieldDecoder.decodeField("double",
							field.getData());
				} else if ("location.y".equals(field.getAttributeName())) {
					y = (Double) FieldDecoder.decodeField("double",
							field.getData());
				}
			}
			if (x > 0 && y > 0) {
				HashableByteArray devHash = new HashableByteArray(pipId);
				this.transmitterLocations.put(devHash, new Point2D.Float(
						(float) x, (float) y));
				this.signalLineMap.setTransmitterLocation(devHash, (float) x,
						(float) y);
				this.varianceLineMap.setTransmitterLocation(devHash, (float) x,
						(float) y);
				this.varianceStripes.addDevice(devHash);
			}
		}
	}

	@Override
	public void attributeAliasesReceived(
			ClientWorldModelInterface clientWorldModelInterface,
			AttributeAliasMessage message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void originAliasesReceived(
			ClientWorldModelInterface clientWorldModelInterface,
			OriginAliasMessage message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void originPreferenceSent(ClientWorldModelInterface worldModel,
			OriginPreferenceMessage message) {
		// TODO Auto-generated method stub
		
	}
}
