/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.sim.gui.panels;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Composite;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import org.apache.mina.util.ConcurrentHashSet;
import org.grailrtls.libcommon.util.HashableByteArray;
import org.grailrtls.sim.gui.structs.ChartItem;
import org.grailrtls.sim.gui.structs.SimpleChartItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HeatStripes extends JPanel {

	private static final Logger log = LoggerFactory
			.getLogger(HeatStripes.class);

	protected static final int DEFAULT_MARGIN = 20;

	protected static final int MARGIN_TOP = 0;
	protected static final int MARGIN_RIGHT = 1;
	protected static final int MARGIN_BOTTOM = 2;
	protected static final int MARGIN_LEFT = 3;

	protected final ConcurrentHashMap<HashableByteArray, ConcurrentHashMap<HashableByteArray, ConcurrentLinkedQueue<ChartItem<Float>>>> dataValues = new ConcurrentHashMap<HashableByteArray, ConcurrentHashMap<HashableByteArray, ConcurrentLinkedQueue<ChartItem<Float>>>>();

	protected final ConcurrentHashSet<HashableByteArray> receivers = new ConcurrentHashSet<HashableByteArray>();

	protected final ConcurrentHashSet<HashableByteArray> devices = new ConcurrentHashSet<HashableByteArray>();

	protected int[] margins = { DEFAULT_MARGIN, DEFAULT_MARGIN,
			DEFAULT_MARGIN * 2, DEFAULT_MARGIN * 2 };

	// Default to 30 seconds so as not to draw too much
	protected long maxAge = 30000;

	protected float thresholdValue = .5f;

	public float getThresholdValue() {
		return thresholdValue;
	}

	public void setThresholdValue(float thresholdValue) {
		this.thresholdValue = thresholdValue;
	}

	public long getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(long maxAge) {
		this.maxAge = maxAge;
	}

	protected float maxValue = 1f;

	protected float currValMax = 1f;

	protected float minValue = 0f;

	protected long lastRepaint = 0l;

	protected boolean drawLegend = false;

	protected float legendHeight = 0;

	protected boolean drawVerticalLines = true;

	protected Color axisTextColor = Color.LIGHT_GRAY;

	protected Color backgroundColor = Color.BLACK;

	protected Color chartGridColor = Color.DARK_GRAY;

	protected Color borderColor = Color.LIGHT_GRAY;

	protected boolean enableAntiAliasing = true;

	protected int minFps = 15;

	protected float currFps = 30f;

	protected int slowFrames = 0;

	public int getMinFps() {
		return minFps;
	}

	public void setMinFps(int minFps) {
		this.minFps = minFps;
	}

	public boolean isEnableAntiAliasing() {
		return enableAntiAliasing;
	}

	public void setEnableAntiAliasing(boolean enableAntiAliasing) {
		this.enableAntiAliasing = enableAntiAliasing;
	}

	public boolean isDrawLegend() {
		return drawLegend;
	}

	public void setDrawLegend(boolean drawLegend) {
		this.drawLegend = drawLegend;
		if (!this.drawLegend) {
			this.legendHeight = 0f;
		}
	}

	public HeatStripes() {
		super();
	}

	public void addValue(final HashableByteArray recHash,
			final HashableByteArray devHash, final float value) {
		ChartItem newItem = new SimpleChartItem(value);
		if (!this.devices.contains(devHash)) {
			return;
		}
		if (!this.receivers.contains(recHash)) {
			return;
		}
		ConcurrentHashMap<HashableByteArray, ConcurrentLinkedQueue<ChartItem<Float>>> recMap = this.dataValues
				.get(recHash);
		if (recMap == null) {
			recMap = new ConcurrentHashMap<HashableByteArray, ConcurrentLinkedQueue<ChartItem<Float>>>();
			this.dataValues.put(recHash, recMap);
		}

		ConcurrentLinkedQueue<ChartItem<Float>> devItems = recMap.get(devHash);
		if (devItems == null) {
			devItems = new ConcurrentLinkedQueue<ChartItem<Float>>();
			recMap.put(devHash, devItems);
		}

		devItems.add(newItem);
	}

	public void addReceiver(HashableByteArray recHash) {
		this.receivers.add(recHash);
	}

	public void addDevice(HashableByteArray devHash) {
		this.devices.add(devHash);
	}

	public void clear() {
		this.dataValues.clear();
	}

	public void paintComponent(Graphics graphics) {
		super.paintComponent(graphics);

		this.lastRepaint = System.currentTimeMillis();

		Graphics2D g2 = (Graphics2D) graphics;
		if (this.enableAntiAliasing) {
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON);
		}

		int screenWidth = this.getWidth();
		int screenHeight = this.getHeight();

		Color origColor = g2.getColor();
		Composite origComposite = g2.getComposite();
		Stroke origStroke = g2.getStroke();

		g2.setColor(Color.BLACK);
		g2.fillRect(0, 0, screenWidth, screenHeight);
		float valueRange = this.maxValue - this.minValue;

		int numStreams = this.receivers.size() * this.devices.size();

		int usableWidth = (screenWidth - this.margins[MARGIN_LEFT] - this.margins[MARGIN_RIGHT]);
		int usableHeight = screenHeight - this.margins[MARGIN_TOP]
				- this.margins[MARGIN_BOTTOM] - (int) this.legendHeight;
		float millisHeight = usableHeight / (float) this.maxAge;
		float itemWidth = (float) (screenWidth - this.margins[MARGIN_LEFT] - this.margins[MARGIN_RIGHT])
				/ (float) numStreams;

		if (this.drawLegend) {
			this.drawLegend(g2, screenWidth, screenHeight);
		}

		g2.setColor(Color.DARK_GRAY);
		this.drawChartGrid(g2, screenWidth, screenHeight,
				(float) (this.maxAge / 10.0),
				(this.maxValue - this.minValue) / 10f);
		this.drawChartBorders(g2, screenWidth, screenHeight);

		if (this.dataValues.keySet().size() == 0) {
			return;
		}

		long oldestItem = this.lastRepaint - this.maxAge;

		BasicStroke stroke = new BasicStroke(itemWidth < .76f ? 1 : itemWidth, BasicStroke.CAP_ROUND,
				BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);
		int itemIndex = -1;
		for (HashableByteArray recHash : this.receivers) {
			ConcurrentHashMap<HashableByteArray, ConcurrentLinkedQueue<ChartItem<Float>>> recMap = this.dataValues
					.get(recHash);
			if (recMap == null) {
				itemIndex += this.devices.size();
				continue;
			}
			for (HashableByteArray devHash : this.devices) {
				++itemIndex;
				ConcurrentLinkedQueue<ChartItem<Float>> devItems = recMap.get(devHash);
				if (devItems == null) {
					continue;
				}

				// GeneralPath path = new GeneralPath();
				int numItems = devItems.size();
				ChartItem[] items = devItems.toArray(new ChartItem[] {});
				boolean skippedPrevious = true;
				float xOnScreen = this.margins[MARGIN_LEFT] + itemIndex
						* itemWidth;
				float prevYOnScreen = 0f;
				float previousValue = 0f;
				for (Iterator<ChartItem<Float>> iter = devItems.iterator(); iter
						.hasNext();) {

					ChartItem<Float> item = iter.next();
					long timeOffset = item.getCreationTime() - oldestItem;
					float yOnScreen = this.margins[MARGIN_TOP]
							+ this.legendHeight + millisHeight * timeOffset;
					if (item.getCreationTime() < oldestItem) {
						iter.remove();
						skippedPrevious = true;
						continue;
					}

					float normalValue = item.getValue() / valueRange;
					if (normalValue > 1.0f) {
						normalValue = 1.0f;
					}

					g2.setColor(Color.getHSBColor(previousValue * .9f, 0.9f,
							0.9f));
					if (!skippedPrevious) {
						g2.drawLine((int) xOnScreen, (int) prevYOnScreen,
								(int) xOnScreen, (int) yOnScreen);
					}
					previousValue = normalValue;
					prevYOnScreen = yOnScreen;

					if (item.getValue() < this.thresholdValue) {
						if (!skippedPrevious) {
							g2.setColor(Color.getHSBColor(previousValue * .9f,
									0.9f, 0.9f));

							g2.drawLine((int) xOnScreen, (int) prevYOnScreen,
									(int) xOnScreen, (int) yOnScreen);

						}
						skippedPrevious = true;
						continue;
					}
					skippedPrevious = false;

				}
				// g2.draw(path);
			}
		}

		g2.setColor(Color.LIGHT_GRAY);

		// this.drawStatsValues(g2, screenWidth, screenHeight);

		g2.setColor(origColor);
		g2.setComposite(origComposite);
		g2.setStroke(origStroke);

		long renderTime = System.currentTimeMillis() - this.lastRepaint;
		this.currFps = this.currFps * 0.875f + (1000f / renderTime) * 0.125f;

		if (this.enableAntiAliasing && (this.currFps < this.minFps * 0.9f)) {
			++this.slowFrames;
			if (this.slowFrames > 3) {
				this.enableAntiAliasing = false;
				log.warn("FPS: {} Disabling Anti-Aliasing.", this.currFps);
			}
		} else if (this.enableAntiAliasing) {
			this.slowFrames = 0;
		}
	}

	protected void drawLegend(final Graphics g, final int screenWidth,
			final int screenHeight) {
		Graphics2D g2 = (Graphics2D) g;
		ArrayList<HashableByteArray> streamIdSet = new ArrayList<HashableByteArray>();
		streamIdSet.addAll(this.dataValues.keySet());
		Collections.sort(streamIdSet);
		if (streamIdSet == null && streamIdSet.size() == 0) {
			return;
		}
		FontMetrics fontMetrics = g2.getFontMetrics();

		float stringHeight = (float) fontMetrics.getStringBounds("TEST", g)
				.getHeight();

		float rowHeight = stringHeight + 10;
		ArrayList<Integer> lineBreakIndexes = new ArrayList<Integer>();

		// Determine the number of rows and columns
		// Assume a square box for color display at string height, 5px spacing,
		// then text
		// 10px spacing between rows and columns

		String[] streamIdArray = streamIdSet.toArray(new String[] {});

		int streamIndex = 0;
		float availableWidth = screenWidth - this.margins[MARGIN_LEFT]
				- this.margins[MARGIN_RIGHT];
		float currentRowWidth = 0f;
		for (String id : streamIdArray) {
			float entryWidth = fontMetrics.stringWidth(id) + 10;
			currentRowWidth += entryWidth;
			if (currentRowWidth > availableWidth) {
				lineBreakIndexes.add(streamIndex);
				currentRowWidth = entryWidth;
			}
			++streamIndex;
		}

		streamIndex = 0;

		float currentYLine = this.margins[MARGIN_TOP] + stringHeight;
		float currentXStart = this.margins[MARGIN_LEFT];

		Color drawColor = Color.BLUE;
		for (String streamId : streamIdArray) {
			if (lineBreakIndexes.contains(streamIndex)) {
				currentYLine += rowHeight;
				currentXStart = this.margins[MARGIN_LEFT];
			}

			// Random coloring if nothing is set
			float adjusted = 0.9f * streamIndex / streamIdArray.length;
			float hue = adjusted;
			float sat = 0.95f;
			float bright = 0.95f;
			drawColor = new Color(Color.HSBtoRGB(hue, sat, bright));
			g2.setColor(drawColor);
			g2.drawString(streamId, currentXStart, currentYLine);

			currentXStart += fontMetrics.stringWidth(streamId) + 10;
			++streamIndex;
		}

		this.legendHeight = currentYLine - this.margins[MARGIN_TOP]
				+ stringHeight;
	}

	protected void drawChartBorders(Graphics g, final int screenWidth,
			final int screenHeight) {
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(this.borderColor);
		// Draw left border
		Line2D.Float borderLine = new Line2D.Float();
		borderLine.setLine(this.margins[MARGIN_LEFT], this.margins[MARGIN_TOP]
				+ this.legendHeight, this.margins[MARGIN_LEFT], screenHeight);
		g2.draw(borderLine);

		// Draw right border
		borderLine.setLine(screenWidth - this.margins[MARGIN_RIGHT],
				this.margins[MARGIN_TOP] + this.legendHeight, screenWidth
						- this.margins[MARGIN_RIGHT], screenHeight);
		g2.draw(borderLine);

		// Draw the bottom border
		borderLine.setLine(this.margins[MARGIN_LEFT], screenHeight
				- this.margins[MARGIN_BOTTOM], screenWidth
				- this.margins[MARGIN_RIGHT], screenHeight
				- this.margins[MARGIN_BOTTOM]);
		g2.draw(borderLine);
	}

	protected void drawChartGrid(Graphics g, final int screenWidth,
			final int screenHeight, float xStep, float yStep) {
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(this.chartGridColor);
		int numItems = this.receivers.size() * this.devices.size();
		int usableWidth = screenWidth - this.margins[MARGIN_LEFT]
				- this.margins[MARGIN_RIGHT];
		int usableHeight = screenHeight - this.margins[MARGIN_TOP]
				- this.margins[MARGIN_BOTTOM] - (int) this.legendHeight;

		float yScale = (this.maxValue - this.minValue) / this.maxAge;
		FontMetrics metrics = g2.getFontMetrics();

		if (this.drawVerticalLines) {
			// Draw lines separating the receiver "lanes"
			HashableByteArray[] recArray = this.receivers
					.toArray(new HashableByteArray[] {});
			float xScale = (float) usableWidth / recArray.length;
			long verticalLine = this.margins[MARGIN_LEFT];
			float screenX;
			int laneIndex = 0;

			int textOffset = 0;
			
			for (; verticalLine <= screenWidth - this.margins[MARGIN_RIGHT]; verticalLine += (long) xScale) {
				g2.setColor(this.chartGridColor);
				g2.drawLine((int) verticalLine, screenHeight
						- this.margins[MARGIN_BOTTOM], (int) verticalLine,
						(int) (this.margins[MARGIN_TOP] + this.legendHeight));
				g2.setColor(this.axisTextColor);
				if (laneIndex < recArray.length) {
					String drawString = recArray[laneIndex].toString();
					float stringWidth = metrics.stringWidth(drawString);
					
					
					g2.drawString(String.format("%s", recArray[laneIndex]
							.toString()), verticalLine +xScale/2 - stringWidth/2, screenHeight
							- this.margins[MARGIN_BOTTOM] / 2f + textOffset);
				}
				if(textOffset != 0){
					textOffset = 0;
				}else{
					textOffset = this.margins[MARGIN_BOTTOM]/2-1;
				}
				++laneIndex;
			}
		}
	}

	protected void drawStatsValues(Graphics g, final int screenWidth,
			final int screenHeight, final ChartItem currItem) {
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.WHITE);

		// Draw the max value
		String valueString = String.format("%03.1f", this.maxValue);
		g2.drawString(valueString, 0, this.margins[MARGIN_TOP]
				+ this.legendHeight);

		// Draw the min value
		valueString = String.format("%03.1f", this.minValue);
		g2.drawString(valueString, 0,
				(float) (screenHeight - this.margins[MARGIN_BOTTOM]));

		valueString = String.format("%03.1f", currItem.getValue());
		FontMetrics metrics = g.getFontMetrics();
		int fontWidth = metrics.stringWidth(valueString);

		// Draw the current value
		g2.drawString(valueString, screenWidth - fontWidth
				- this.margins[MARGIN_RIGHT] / 2, this.margins[MARGIN_TOP]
				+ this.legendHeight);
	}

	public float getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(float maxValue) {
		this.maxValue = maxValue;
	}

	public float getMinValue() {
		return minValue;
	}

	public void setMinValue(float minValue) {
		this.minValue = minValue;
	}

	public boolean isDrawVerticalLines() {
		return drawVerticalLines;
	}

	public void setDrawVerticalLines(boolean drawVerticalLines) {
		this.drawVerticalLines = drawVerticalLines;
	}
}
