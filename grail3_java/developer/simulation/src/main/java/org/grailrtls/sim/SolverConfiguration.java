package org.grailrtls.sim;

import java.util.List;

import org.grailrtls.libsolver.rules.SubscriptionRequestRule;

public class SolverConfiguration {

	private String aggregatorHost = "localhost";
	
	private int aggregatorPort = 7008;
	
	private List<SubscriptionRequestRule> rules = null;
	
	private String worldModelHost = "localhost";
	
	private int worldModelSolverPort = 7012;
	
	private int worldModelClientPort = 7013;

	public String getAggregatorHost() {
		return aggregatorHost;
	}

	public void setAggregatorHost(String aggregatorHost) {
		this.aggregatorHost = aggregatorHost;
	}

	public int getAggregatorPort() {
		return aggregatorPort;
	}

	public void setAggregatorPort(int aggregatorPort) {
		this.aggregatorPort = aggregatorPort;
	}

	public String getWorldModelHost() {
		return worldModelHost;
	}

	public void setWorldModelHost(String worldModelHost) {
		this.worldModelHost = worldModelHost;
	}

	public int getWorldModelSolverPort() {
		return worldModelSolverPort;
	}

	public void setWorldModelSolverPort(int worldModelSolverPort) {
		this.worldModelSolverPort = worldModelSolverPort;
	}

	public int getWorldModelClientPort() {
		return worldModelClientPort;
	}

	public void setWorldModelClientPort(int worldModelClientPort) {
		this.worldModelClientPort = worldModelClientPort;
	}

	public List<SubscriptionRequestRule> getRules() {
		return rules;
	}

	public void setRules(List<SubscriptionRequestRule> rules) {
		this.rules = rules;
	}
}
