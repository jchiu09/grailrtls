package org.grailrtls.sim;

import java.io.FileNotFoundException;
import java.io.PrintStream;

import org.grailrtls.libcommon.util.NumericUtils;
import org.grailrtls.libsolver.SolverAggregatorConnection;
import org.grailrtls.libsolver.protocol.messages.SampleMessage;

/**
 * A simple solver that logs all samples from an aggregator as a CSV file.  All output is written
 * to standard out unless an output file is provided.
 * 
 * @author Robert Moore
 * 
 */
public class CSVSolver extends Thread {

	/**
	 * A format string for floating point values to 3 decimal places.
	 */
	private static final String THREE_DECIMAL_FLOAT = "%1.3f";

	/**
	 * The header string for the CSV file.
	 */
	private static final String HEADER_STRING = "\"Current Timestamp\",\"Reciever Timestamp\",\"Physical Layer\",\"Device ID\",\"Receiver ID\",\"RSSI\",\"Sensed Data\"";

	/**
	 * Creates a new CSVSolver and connects it to the aggregator.
	 * @param args aggregator host, aggregator port, and optionally an output file name.
	 */
	public static void main(String[] args) {
		if (args.length < 2) {
			System.err
					.println("Requires 2 arguments: <Aggregator Host> <Aggregator Solver Port>");
			return;
		}

		int port = Integer.parseInt(args[1]);
		
		PrintStream out = System.out;
		
		if(args.length >= 3){
			try {
				out = new PrintStream(args[2]);
			} catch (FileNotFoundException e) {
				System.err.println("Could not write to file \"" + args[2] + "\". Defaulting to standard out.");
			}
		}
		

		CSVSolver solver = new CSVSolver(args[0], port, out);
		solver.init();
		solver.start();
	}

	/**
	 * The interface to the aggregator.
	 */
	protected final SolverAggregatorConnection agg = new SolverAggregatorConnection();
	
	/**
	 * PrintStream for writing the CSV data.
	 */
	private final PrintStream out;

	/**
	 * Creates a new CSVSolver that connects to an aggregator at the host/port specified.  All CSV output
	 * is written to {@code out}.
	 * @param host the host name/IP address of the aggregator.
	 * @param port the port on which the aggregator is listening for solver connections.
	 * @param out where CSV data is written.
	 */
	public CSVSolver(final String host, final int port, final PrintStream out) {
		super("Fake Solver for (" + host + ":" + port + ")");
		this.agg.setHost(host);
		this.agg.setPort(port);
		this.out = out;

		// Capture shutdown/terminate requests and be sure we will gracefully 
		// record any remaining data
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				System.err.println("Shutting down...");
				CSVSolver.this.agg.disconnect();
			}
		});
	}

	/**
	 * Sets up the CSV solver and gets things ready to run.
	 */
	public void init() {
		this.agg.connect();
	}

	/**
	 * Main loop.  While the solver is connected, keeps reading samples and writing them out to the 
	 * CSV stream.
	 */
	@Override
	public void run() {
		this.out.println(HEADER_STRING);

		SampleMessage sample = null;
		StringBuilder sb = null;
		try {
			// Two conditions here: keep reading samples while there are some buffered,
			// but be sure to exit once we're disconnected and have exhausted the buffer
			while (this.agg.isConnectionLive() || this.agg.hasNext()) {
				sample = this.agg.getNextSample();
				if (sample == null) {
					break;
				}
				sb = new StringBuilder(1024);
				sb.append(Long.toString(System.currentTimeMillis()));
				sb.append(',');
				sb.append(Long.toString(sample.getReceiverTimeStamp()));
				sb.append(',');
				sb.append(Integer.toString(sample.getPhysicalLayer() & 0xFF));
				sb.append(',');
				sb.append(NumericUtils.toHexShortString(sample.getDeviceId()));
				sb.append(',');
				sb.append(NumericUtils.toHexShortString(sample.getReceiverId()));
				sb.append(',');
				sb.append(String.format(THREE_DECIMAL_FLOAT, Float.valueOf(sample.getRssi())));
				sb.append(',');
				if (sample.getSensedData() != null) {
					sb.append(NumericUtils.toHexString(sample.getSensedData()));
				}
				this.out.println(sb.toString());
			}

		} catch (Exception e) {
			System.err
					.println("Exception caught while receiving samples from the aggregator.");
			e.printStackTrace();
		}
		this.out.flush();
	}

}
