/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *  
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.libsolver.protocol.codec;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.apache.mina.filter.codec.demux.MessageEncoder;
import org.apache.mina.filter.util.SessionAttributeInitializingFilter;
import org.grailrtls.libsolver.protocol.messages.SampleMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SampleEncoder implements MessageEncoder<SampleMessage> {

	private static final Logger log = LoggerFactory
			.getLogger(SampleEncoder.class);

	public void dispose(IoSession arg0) throws Exception {
	}

	public void encode(IoSession session, SampleMessage message,
			ProtocolEncoderOutput out) throws Exception {

		if (message.getLengthPrefix() < 0) {
			throw new IOException("Message length is negative.");
		}
		
		IoBuffer buffer = IoBuffer.allocate(message.getLengthPrefix()+4);
		
		buffer.putInt(message.getLengthPrefix());
		buffer.put(message.getMessageType());
		buffer.put(message.getPhysicalLayer());
		buffer.put(message.getDeviceId());
		buffer.put(message.getReceiverId());
		buffer.putLong(message.getReceiverTimeStamp());
		buffer.putFloat(message.getRssi());
		if (message.getSensedData() != null) {
			buffer.put(message.getSensedData());
		}
		buffer.flip();
		out.write(buffer);
		buffer.free();
	}

}
