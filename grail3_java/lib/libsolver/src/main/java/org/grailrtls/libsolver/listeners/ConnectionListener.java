/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *  
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.libsolver.listeners;

import org.apache.mina.core.session.IoSession;
import org.grailrtls.libsolver.SolverAggregatorInterface;

public interface ConnectionListener
{
	/**
	 * Called when a connection to the aggregator is terminated and will not be reestablished.
	 * @param aggregator
	 */
    public void connectionEnded(SolverAggregatorInterface aggregator);
    
    /**
     * Called when a connection to the aggregator is opened.
     * @param aggregator
     */
    public void connectionEstablished(SolverAggregatorInterface aggregator);
    
    /**
     * Called when a connection to the aggregator is terminated, but may be reestablished.
     * @param aggregator
     */
    public void connectionInterrupted(SolverAggregatorInterface aggregator);

}
