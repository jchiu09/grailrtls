/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *  
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.libsolver.protocol.messages;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.grailrtls.libcommon.util.NumericUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Represents a sample sent from the Aggregator to a Solver as defined in the
 * GRAIL RTLS v3 Aggregator-Solver protocol.
 * 
 * @author Robert Moore II
 * 
 */
public class SampleMessage extends AggregatorSolverMessage {

	/**
	 * The size, in octets, of the device identifier.
	 */
	public static final int DEVICE_ID_SIZE = 16;

	/**
	 * The identifier used for this message.
	 */
	public static final byte MESSAGE_TYPE = 6;

	/**
	 * The physical layer type of the transmitter/receiver identified in this
	 * message.
	 */
	private byte physicalLayer;

	/**
	 * Returns the physical layer type for the transmitter/receiver identified
	 * in this message.
	 * 
	 * @return the physical layer type for the transmitter/receiver identified
	 *         in this message.
	 */
	public byte getPhysicalLayer() {
		return this.physicalLayer;
	}

	/**
	 * Sets the physical layer type for the transmitter/receiver identified in
	 * this message.
	 * 
	 * @param physicalLayer
	 *            the physical layer type for the transmitter/receiver
	 *            identified in this message.
	 */
	public void setPhysicalLayer(byte physicalLayer) {
		this.physicalLayer = physicalLayer;
	}

	/**
	 * The device identifier for the transmitter referenced in this message.
	 */
	private byte[] deviceId;

	/**
	 * The device identifier for the receiver referenced in this message.
	 */
	private byte[] receiverId;

	/**
	 * The UNIX timestamp indicating when this sample was observed by the
	 * receiver.
	 */
	private long receiverTimeStamp;

	/**
	 * The Received Signal Strength Indicator (RSSI) for this sample.
	 */
	private float rssi;

	/**
	 * The raw sensed data provided by the transmitter or receiver for this
	 * sample.
	 */
	private byte[] sensedData = null;
	
	public int getLengthPrefix() {
		// messageType, physicalLayer, devId, recvId, timestamp, rssi
		int length = 1 + 1 + DEVICE_ID_SIZE * 2 + 8 + 4;
		if (this.sensedData != null) {
			length += this.sensedData.length;
		}
		return length;
	}

	/**
	 * Returns the device identifier for the transmitter referenced in this
	 * sample.
	 * 
	 * @return the device identifier for the transmitter referenced in this
	 *         sample.
	 */
	public byte[] getDeviceId() {
		return this.deviceId;
	}

	/**
	 * Sets the device identifier for the transmitter referenced in this sample.
	 * 
	 * @param deviceId
	 *            the device identifier for the transmitter referenced in this
	 *            sample.
	 */
	public void setDeviceId(byte[] deviceId) {
		if (deviceId == null) {
			throw new RuntimeException("Device ID cannot be null.");
		}
		if (deviceId.length != DEVICE_ID_SIZE) {
			throw new RuntimeException(String.format(
					"Device ID must be %d bytes long.", Integer
							.valueOf(DEVICE_ID_SIZE)));
		}
		this.deviceId = deviceId;
	}

	/**
	 * Returns the device identifier for the receiver referenced in this sample.
	 * 
	 * @return the device identifier for the receiver referenced in this sample.
	 */
	public byte[] getReceiverId() {
		return this.receiverId;
	}

	/**
	 * Sets the device identifier for the receiver referenced in this sample.
	 * 
	 * @param receiverId
	 *            the device identifier for the receiver referenced in this
	 *            sample.
	 */
	public void setReceiverId(byte[] receiverId) {
		if (receiverId == null) {
			throw new RuntimeException("Receiver ID cannot be null.");
		}
		if (receiverId.length != DEVICE_ID_SIZE) {
			throw new RuntimeException(String.format(
					"Receiver ID must be %d bytes long.", Integer
							.valueOf(DEVICE_ID_SIZE)));
		}
		this.receiverId = receiverId;
	}

	/**
	 * Returns the UNIX timestamp indicating when this sample was received by
	 * the receiver.
	 * 
	 * @return the UNIX timestamp indicating when this sample was received by
	 *         the receiver.
	 */
	public long getReceiverTimeStamp() {
		return this.receiverTimeStamp;
	}

	/**
	 * Sets the UNIX timestamp indicating when this sample was received by the
	 * receiver.
	 * 
	 * @param receiverTimeStamp
	 *            the UNIX timestamp indicating when this sample was received by
	 *            the receiver.
	 */
	public void setReceiverTimeStamp(long receiverTimeStamp) {
		this.receiverTimeStamp = receiverTimeStamp;
	}

	/**
	 * Returns the Received Signal Strength Indicator (RSSI) observed by the
	 * receiver for this sample.
	 * 
	 * @return the Received Signal Strength Indicator (RSSI) observed by the
	 *         receiver for this sample.
	 */
	public float getRssi() {
		return this.rssi;
	}

	/**
	 * Sets the Received Signal Strength Indicator (RSSI) observed by the
	 * receiver for this sample.
	 * 
	 * @param rssi
	 *            the Received Signal Strength Indicator (RSSI) observed by the
	 *            receiver for this sample.
	 */
	public void setRssi(float rssi) {
		this.rssi = rssi;
	}

	/**
	 * Returns the raw sensed data sent by the transmitter and observed by the
	 * receiver for this sample.
	 * 
	 * @return the raw sensed data sent by the transmitter and observed by the
	 *         receiver for this sample.
	 */
	public byte[] getSensedData() {
		return this.sensedData;
	}

	/**
	 * Sets the raw sensed data sent by the transmitter and observed by the
	 * receiver for this sample.
	 * 
	 * @param sensedData
	 *            the raw sensed data sent by the transmitter and observed by
	 *            the receiver for this sample.
	 */
	public void setSensedData(byte[] sensedData) {
		this.sensedData = sensedData;
	}

	@Override
	public byte getMessageType() {
		return MESSAGE_TYPE;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Solver Sample (").append(this.getPhysicalLayer()).append(
				", ").append(NumericUtils.toHexString(this.getDeviceId()))
				.append(", ").append(
						NumericUtils.toHexString(this.getReceiverId())).append(
						"): ").append(this.getRssi()).append(" @ ").append(this.getReceiverTimeStamp());
		if (this.getSensedData() != null) {
			sb.append(" [").append(
					NumericUtils.toHexString(this.getSensedData())).append(']');
		}

		return sb.toString();
	}
}
