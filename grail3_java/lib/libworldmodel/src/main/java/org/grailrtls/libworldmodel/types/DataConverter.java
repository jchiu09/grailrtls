package org.grailrtls.libworldmodel.types;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DataConverter {
	private static final Logger log = LoggerFactory
			.getLogger(DataConverter.class);

	private static final Map<String, TypeConverter> converterNames = new ConcurrentHashMap<String, TypeConverter>();

	static {
		converterNames.put(StringConverter.CONVERTER.getTypeName(),
				StringConverter.CONVERTER);
		converterNames.put(IntegerConverter.CONVERTER.getTypeName(),
				IntegerConverter.CONVERTER);
		converterNames.put(DoubleConverter.CONVERTER.getTypeName(),
				DoubleConverter.CONVERTER);
		converterNames.put(BooleanConverter.CONVERTER.getTypeName(),
				BooleanConverter.CONVERTER);
	}

	private static final Map<String, TypeConverter> attributeConverters = new ConcurrentHashMap<String, TypeConverter>();

	static {
		attributeConverters.put("location.x_offset", DoubleConverter.CONVERTER);
		attributeConverters.put("location.y_offset", DoubleConverter.CONVERTER);
		attributeConverters.put("dimension.width", DoubleConverter.CONVERTER);
		attributeConverters.put("dimension.height", DoubleConverter.CONVERTER);
		attributeConverters.put("dimension.units", StringConverter.CONVERTER);
		attributeConverters.put("closed", BooleanConverter.CONVERTER);
		attributeConverters.put("on", BooleanConverter.CONVERTER);
		attributeConverters.put("image.url", StringConverter.CONVERTER);
	}

	public static byte[] encodeUri(final String attributeUri, final Object obj) {
		TypeConverter converter = DataConverter.attributeConverters
				.get(attributeUri);
		if (converter == null) {
			log.warn(
					"Unable to find a suitable data converter for attribute URI {}.",
					attributeUri);
			throw new IllegalArgumentException(
					"Unable to find a suitable data converter for attribute URI \""
							+ attributeUri + "\".");
		}

		if (obj instanceof String) {
			return converter.encode((String) obj);
		}
		return converter.encode(obj);
	}

	public static Object decodeUri(final String attributeUri,
			final byte[] encodedBytes) {
		TypeConverter converter = DataConverter.attributeConverters
				.get(attributeUri);
		if (converter == null) {
			log.warn(
					"Unable to find a suitable data converter for attribute URI {}.",
					attributeUri);
			throw new IllegalArgumentException(
					"Unable to find a suitable data converter for attribute URI \""
							+ attributeUri + "\".");
		}

		return converter.decode(encodedBytes);
	}

	public static boolean hasConverterForURI(final String attributeUri) {
		return DataConverter.attributeConverters.containsKey(attributeUri);
	}

	public static boolean hasConverterForType(final String type) {
		return DataConverter.converterNames.containsKey(type);
	}

	public static String[] getSupportedTypes() {
		return DataConverter.converterNames.keySet().toArray(new String[] {});
	}

	public static TypeConverter putConverter(final String attributeUri,
			final String type) {
		TypeConverter conv = DataConverter.converterNames.get(type);
		if (conv == null) {
			log.warn("Could not find a converter for data type {}.", type);
			throw new IllegalArgumentException(
					"Could not find a converter for data type \"" + type
							+ "\".");
		}
		return DataConverter.attributeConverters.put(attributeUri, conv);
	}

}
