package org.grailrtls.libworldmodel.types;

import java.nio.ByteBuffer;

public class IntegerConverter implements TypeConverter<Integer>{

	public static final IntegerConverter CONVERTER = new IntegerConverter();
	
	private IntegerConverter(){super();}
	
	@Override
	public Integer decode(byte[] data) {
		ByteBuffer buff = ByteBuffer.wrap(data);
		return Integer.valueOf(buff.getInt());
	}

	@Override
	public byte[] encode(Integer object) {
		ByteBuffer buff = ByteBuffer.allocate(4);
		buff.putInt(object.intValue());
		return buff.array();
	}

	public String getTypeName(){
		return "Integer";
	}

	@Override
	public Integer decode(String asString) {
		return Integer.valueOf(asString);
	}

	@Override
	public byte[] encode(String asString) {
		return this.encode(this.decode(asString));
	}
	
}
