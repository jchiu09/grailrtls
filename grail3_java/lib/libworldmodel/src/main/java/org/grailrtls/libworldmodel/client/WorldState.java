package org.grailrtls.libworldmodel.client;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.grailrtls.libworldmodel.client.protocol.messages.Attribute;


public class WorldState {
	private Map<String, Collection<Attribute>> stateMap = new ConcurrentHashMap<String, Collection<Attribute>>();
	
	public void addState(final String uri, final Collection<Attribute> attributes){
		this.stateMap.put(uri,attributes);
	}
	
	public Collection<Attribute> getState(final String uri){
		return this.stateMap.get(uri);
	}
	
	/**
	 * Returns a collection containing the same URI Strings as this WorldState object.  Modifications to the
	 * returned Collection do not impact this WorldState.
	 * @return a collection containing the same URI Strings as this WorldState.
	 */
	public Collection<String> getURIs(){
		List<String> keys = new LinkedList<String>();
		keys.addAll(this.stateMap.keySet());
		return keys;
	}
}
