/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *  
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.libworldmodel.client.protocol.messages;

import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A World Model-Client message used to search the World Model for matching URI values.
 * @author Robert Moore
 *
 */
public class URISearchMessage {

	/**
	 * Logging facility for this class.
	 */
	private static final Logger log = LoggerFactory.getLogger(URISearchMessage.class);
	
	/**	
	 * Message Type value for the URI Search message.
	 */
	public static final byte MESSAGE_TYPE = 9;
	
	/**
	 * A regular expression pattern represented as a UTF-16BE String.
	 */
	private String uriRegex;
	
	public int getMessageLength(){
		if(this.uriRegex == null){
			return 1;
		}
		try {
			return this.uriRegex.getBytes("UTF-16BE").length + 1;
		} catch (UnsupportedEncodingException e) {
			log.error("Unable to decode UTF-16BE String: {}", e);
			return 1;
		}
	}

	public String getUriRegex() {
		return uriRegex;
	}

	public void setUriRegex(String uriRegex) {
		this.uriRegex = uriRegex;
	}
	
	public String toString(){
		StringBuffer sb = new StringBuffer();
		
		sb.append("URI Search (").append(this.uriRegex).append(")");
		
		return sb.toString();
	}
}
