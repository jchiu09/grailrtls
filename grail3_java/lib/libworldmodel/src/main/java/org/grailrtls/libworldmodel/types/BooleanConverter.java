package org.grailrtls.libworldmodel.types;

public class BooleanConverter implements TypeConverter<Boolean> {

	public static final BooleanConverter CONVERTER = new BooleanConverter();

	private BooleanConverter() {super();}

	@Override
	public Boolean decode(byte[] data) {
		return data[0] == 0 ? Boolean.FALSE : Boolean.TRUE;
	}

	@Override
	public byte[] encode(Boolean object) {
		return new byte[] { object.booleanValue() ? (byte) 1 : 0 };
	}

	@Override
	public Boolean decode(String asString) {
		return Boolean.parseBoolean(asString);
	}

	@Override
	public byte[] encode(String asString) {
		return encode(decode(asString));
	}

	@Override
	public String getTypeName() {
		return "Boolean";
	}

}
