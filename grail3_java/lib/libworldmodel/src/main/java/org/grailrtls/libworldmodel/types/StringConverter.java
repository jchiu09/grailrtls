package org.grailrtls.libworldmodel.types;

import java.io.UnsupportedEncodingException;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringConverter implements TypeConverter<String> {

	private static final Logger log = LoggerFactory.getLogger(StringConverter.class);
	
	public static final StringConverter CONVERTER = new StringConverter();
	
	private StringConverter(){super();}
	
	@Override
	public String decode(byte[] data) {
		try {
			return new String(data,"UTF-16BE");
		} catch (UnsupportedEncodingException e) {
			log.error("Unable to decode UTF-16BE strings.",e);
			return null;
		}
	}

	@Override
	public byte[] encode(String object) {
		try {
			return object.getBytes("UTF-16BE");
		} catch (UnsupportedEncodingException e) {
			log.error("Unable to decode UTF-16BE strings.",e);
			return null;
		}
	}

	public String getTypeName(){
		return "String";
	}

	@Override
	public String decode(String asString) {
		return asString;
	}
	
}
