package org.grailrtls.libworldmodel.solver.listeners;

import org.grailrtls.libworldmodel.solver.SolverWorldModelInterface;
import org.grailrtls.libworldmodel.solver.protocol.messages.StartTransientMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.StopTransientMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage.TypeSpecification;

public interface DataListener {
	public void startTransientReceived(SolverWorldModelInterface worldModel, StartTransientMessage message);
	
	public void stopTransientReceived(SolverWorldModelInterface worldModel, StopTransientMessage message);
	
	public void typeSpecificationsSent(SolverWorldModelInterface worldModel, TypeAnnounceMessage message);
}
