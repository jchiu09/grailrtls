/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *  
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package org.grailrtls.libworldmodel.solver.protocol.codec;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.demux.MessageDecoder;
import org.apache.mina.filter.codec.demux.MessageDecoderResult;
import org.grailrtls.libworldmodel.solver.protocol.messages.StopTransientMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage.TypeSpecification;

public class TypeAnnounceDecoder implements MessageDecoder {

	@Override
	public MessageDecoderResult decodable(IoSession session, IoBuffer buffer) {
		if (buffer.prefixedDataAvailable(4, 65536)) {
			buffer.mark();
			int messageLength = buffer.getInt();
			if (messageLength < 1) {
				buffer.reset();
				return MessageDecoderResult.NOT_OK;
			}

			byte messageType = buffer.get();
			buffer.reset();
			if (messageType == TypeAnnounceMessage.MESSAGE_TYPE) {
				return MessageDecoderResult.OK;
			}
			return MessageDecoderResult.NOT_OK;
		}
		return MessageDecoderResult.NEED_DATA;
	}

	@Override
	public MessageDecoderResult decode(IoSession session, IoBuffer buffer,
			ProtocolDecoderOutput out) throws Exception {
		TypeAnnounceMessage message = new TypeAnnounceMessage();
		
		int messageLength = buffer.getInt();
		byte messageType = buffer.get();
		--messageLength;
		
		int numTypes = buffer.getInt();
		messageLength -= 4;
		
		if(numTypes > 0){
			TypeSpecification[] specs = new TypeSpecification[numTypes];
			
			for(int i = 0; i < numTypes; ++i){
				TypeSpecification spec = new TypeSpecification();
				
				int alias = buffer.getInt();
				messageLength -= 4;
				spec.setTypeAlias(alias);
				
				int uriLength = buffer.getInt();
				messageLength -= 4;
				byte[] uriBytes = new byte[uriLength];
				buffer.get(uriBytes);
				messageLength -= uriLength;
				spec.setUriName(new String(uriBytes,"UTF-16BE"));
				
				spec.setIsTransient(buffer.get() == (byte)0 ?  false : true);
				--messageLength;
				
				specs[i] = spec;
			}
			
			message.setTypeSpecifications(specs);
		}
		
		byte[] originBytes = new byte[messageLength];
		buffer.get(originBytes);
		message.setOrigin(new String(originBytes, "UTF-16BE"));
		
		out.write(message);
		
		return MessageDecoderResult.OK;
	}

	@Override
	public void finishDecode(IoSession arg0, ProtocolDecoderOutput arg1)
			throws Exception {
		// TODO Auto-generated method stub

	}

}
