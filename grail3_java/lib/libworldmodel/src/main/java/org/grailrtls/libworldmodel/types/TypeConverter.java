package org.grailrtls.libworldmodel.types;

public interface TypeConverter <T>{
	
	public T decode(final byte[] data);
	
	public byte[] encode(final T object);
	
	public T decode(final String asString);
	
	public byte[] encode(final String asString);

	public String getTypeName();
}
