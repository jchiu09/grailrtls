/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *  
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package org.grailrtls.libworldmodel.client.protocol.codec;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.apache.mina.filter.codec.demux.MessageEncoder;
import org.grailrtls.libworldmodel.client.protocol.messages.URISearchResponseMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class URISearchResponseEncoder implements
		MessageEncoder<URISearchResponseMessage> {

	/**
	 * Logging facility for this class.
	 */
	private static final Logger log = LoggerFactory.getLogger(URISearchResponseEncoder.class);
	
	@Override
	public void encode(IoSession session, URISearchResponseMessage message,
			ProtocolEncoderOutput out) throws Exception {
		int prefixLength = message.getMessageLength();
		
		IoBuffer buffer = IoBuffer.allocate(prefixLength+4);
		
		buffer.putInt(prefixLength);
		buffer.put(URISearchResponseMessage.MESSAGE_TYPE);
		
		if(message.getMatchingUris() != null){
			for(String uri : message.getMatchingUris()){
				byte[] uriByte = uri.getBytes("UTF-16BE");
				buffer.putInt(uriByte.length);
				buffer.put(uriByte);
			}
		}
		
		buffer.flip();
		
		out.write(buffer);
		
		buffer.free();
		
	}

}
