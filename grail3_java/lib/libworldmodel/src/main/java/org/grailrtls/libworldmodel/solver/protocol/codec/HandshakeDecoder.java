/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *  
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.libworldmodel.solver.protocol.codec;

import java.nio.charset.Charset;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.demux.MessageDecoder;
import org.apache.mina.filter.codec.demux.MessageDecoderResult;
import org.grailrtls.libworldmodel.solver.protocol.messages.HandshakeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HandshakeDecoder implements MessageDecoder {

	private static final Logger log = LoggerFactory
			.getLogger(HandshakeDecoder.class);

	private static final Charset charsetASCII = Charset.forName("US-ASCII");

	public MessageDecoderResult decodable(IoSession arg0, IoBuffer arg1) {

		if (!arg1.prefixedDataAvailable(4,
				HandshakeMessage.PROTOCOL_STRING_LENGTH)) {
			log
					.debug("Not yet decodable with only {} bytes.", arg1
							.remaining());
			return MessageDecoderResult.NEED_DATA;
		}

		// TODO: Need better logic to determine decodability
		return MessageDecoderResult.OK;
	}

	public MessageDecoderResult decode(IoSession arg0, IoBuffer arg1,
			ProtocolDecoderOutput arg2) throws Exception {

		if (arg1.prefixedDataAvailable(4,
				HandshakeMessage.PROTOCOL_STRING_LENGTH)) {
			HandshakeMessage message = new HandshakeMessage();
			message.setStringLength(arg1.getInt());
			if (message.getStringLength() != HandshakeMessage.PROTOCOL_STRING_LENGTH) {
				throw new RuntimeException(String.format(
						"Handshake protocol string length is incorrect: %d",
						message.getStringLength()));
			}

			message.setProtocolString(String.valueOf(arg1.getString(message
					.getStringLength(), HandshakeDecoder.charsetASCII
					.newDecoder())));
			message.setVersionNumber(arg1.get());
			message.setReservedBits(arg1.get());

			arg2.write(message);
			log.debug("Wrote {}.", message);
			return MessageDecoderResult.OK;
		}
		// Entire message is not yet available
		log.warn("Insufficient buffer size: {}.", arg1.remaining());
		return MessageDecoderResult.NEED_DATA;
	}

	public void finishDecode(IoSession arg0, ProtocolDecoderOutput arg1)
			throws Exception {
		// Nothing to do
	}

}
