package org.grailrtls.libworldmodel.types;

import java.nio.ByteBuffer;

public class DoubleConverter implements TypeConverter<Double> {

	public static final DoubleConverter CONVERTER = new DoubleConverter();
	
	private DoubleConverter(){super();}
	
	@Override
	public Double decode(byte[] data) {
		ByteBuffer buff = ByteBuffer.wrap(data);
		return Double.valueOf(buff.getDouble());
	}

	@Override
	public byte[] encode(Double object) {
		ByteBuffer buff = ByteBuffer.allocate(8);
		buff.putDouble(object);
		return buff.array();
	}
	
	public String getTypeName(){
		return "Double";
	}

	@Override
	public Double decode(String asString) {
		return Double.valueOf(asString);
	}

	@Override
	public byte[] encode(String asString) {
		return this.encode(this.decode(asString));
	}

}
