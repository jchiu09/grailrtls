package org.grailrtls.libworldmodel.client.protocol.messages;

public abstract class AbstractRequestMessage {

    /**
     * The client-assigned ticket number for the request.  Used by the 
     * World Model to notify the client when a request is finished (via
     * {@link RequestCompleteMessage}) or by the client to cancel a request
     * (via {@link CancelRequestMessage}).
     */
    protected long ticketNumber = 0;

    public long getTicketNumber()
    {
        return ticketNumber;
    }

    public void setTicketNumber(long ticketNumber)
    {
        this.ticketNumber = ticketNumber&0xFFFFFFFF;
    }
    
}
