/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *  
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package org.grailrtls.libworldmodel.client.protocol.codec;

import org.apache.mina.filter.codec.demux.DemuxingProtocolCodecFactory;
import org.grailrtls.libworldmodel.client.protocol.messages.AttributeAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.CancelRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.DataResponseMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.HandshakeMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.KeepAliveMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginPreferenceMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.RangeRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.RequestCompleteMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.SnapshotRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.StreamRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.URISearchMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.URISearchResponseMessage;

public class WorldModelClientProtocolCodecFactory extends
        DemuxingProtocolCodecFactory
{

    public static final String CODEC_NAME = "Grail Client-World Model codec";

    public WorldModelClientProtocolCodecFactory(final boolean isClient)
    {
        super();

        // Encoders for both sides
        super.addMessageEncoder(HandshakeMessage.class,
                HandshakeEncoder.class);
        super.addMessageEncoder(KeepAliveMessage.class,
                KeepAliveEncoder.class);

        if (isClient)
        {
            // Encoders for client
            super.addMessageEncoder(SnapshotRequestMessage.class,
                    SnapshotRequestEncoder.class);
            super.addMessageEncoder(RangeRequestMessage.class,
                    RangeRequestEncoder.class);
            super.addMessageEncoder(StreamRequestMessage.class,
                    StreamRequestEncoder.class);
            super.addMessageEncoder(CancelRequestMessage.class,
                    CancelRequestEncoder.class);
            super.addMessageEncoder(URISearchMessage.class,
                    URISearchEncoder.class);
            super.addMessageEncoder(OriginPreferenceMessage.class,
                    OriginPreferenceEncoder.class);

            // Decoders for client
            super.addMessageDecoder(URISearchResponseDecoder.class);
            super.addMessageDecoder(OriginAliasDecoder.class);
            super.addMessageDecoder(AttributeAliasDecoder.class);
            super.addMessageDecoder(DataResponseDecoder.class);
            super.addMessageDecoder(RequestCompleteDecoder.class);

        }
        else
        {
            // Encoders for World Model
            super.addMessageEncoder(DataResponseMessage.class,
                    DataResponseEncoder.class);
            super.addMessageEncoder(URISearchResponseMessage.class,
                    URISearchResponseEncoder.class);
            super.addMessageEncoder(RequestCompleteMessage.class,
                    RequestCompleteEncoder.class);
            super.addMessageEncoder(OriginAliasMessage.class,
                    OriginAliasEncoder.class);
            super.addMessageEncoder(AttributeAliasMessage.class,
                    AttributeAliasEncoder.class);

            // Decoders for World Model
            super.addMessageDecoder(SnapshotRequestDecoder.class);
            super.addMessageDecoder(RangeRequestDecoder.class);
            super.addMessageDecoder(StreamRequestDecoder.class);
            super.addMessageDecoder(CancelRequestDecoder.class);
            super.addMessageDecoder(URISearchDecoder.class);
            super.addMessageDecoder(OriginPreferenceDecoder.class);
        }
        // Decoders for both
        super.addMessageDecoder(KeepAliveDecoder.class);
        super.addMessageDecoder(HandshakeDecoder.class);

    }

}
