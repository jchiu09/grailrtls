/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *  
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.libsensor.protocol.codecs;

import java.io.IOException;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.apache.mina.filter.codec.demux.MessageEncoder;
import org.grailrtls.libcommon.util.NumericUtils;
import org.grailrtls.libsensor.protocol.messages.SampleMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SampleEncoder implements MessageEncoder<SampleMessage> {

	private static final Logger log = LoggerFactory
			.getLogger(SampleEncoder.class);

	public void dispose(IoSession arg0) throws Exception {
		// TODO Auto-generated method stub

	}

	public void encode(IoSession session, SampleMessage message,
			ProtocolEncoderOutput out) throws Exception {
		if (message.getLength() < 0) {
			throw new IOException("Message length is negative.");
		}

		IoBuffer buffer = IoBuffer.allocate(message.getLength() + 4);
		log.debug("Message length: {}.", message.getLength());
		buffer.putInt(message.getLength());
		buffer.put(message.getPhysicalLayer());
		buffer.put(message.getDeviceId());
		buffer.put(message.getReceiverId());
		buffer.putLong(message.getReceivedTimestamp());
		buffer.putFloat(message.getRssi());
		if (message.getSensorData() != null) {
			buffer.put(message.getSensorData());
		}

		buffer.flip();

		log.debug("Sample bytes: {}", NumericUtils.toHexString(buffer.array()));

		log.debug("Sending sample message {}.", message);
		out.write(buffer);
		buffer.free();
	}

}
