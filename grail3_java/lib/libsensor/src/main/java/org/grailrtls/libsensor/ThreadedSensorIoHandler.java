/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *  
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.libsensor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.grailrtls.libsensor.protocol.messages.HandshakeMessage;
import org.grailrtls.libsensor.protocol.messages.SampleMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThreadedSensorIoHandler extends SensorIoHandler {

	private static final Logger log = LoggerFactory
			.getLogger(ThreadedSensorIoHandler.class);

	private ExecutorService messageHandlerPool = Executors
			.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

	private static final class MessageHandlerTask implements Runnable {

		private static final Logger log = LoggerFactory
				.getLogger(MessageHandlerTask.class);

		private IoSession session = null;
		private SensorIoAdapter ioAdapter = null;
		private boolean isSending = false;
		private Object message = null;

		@Override
		public void run() {
			if (ioAdapter == null) {
				return;
			}
			if (session == null) {
				return;
			}
			if (message == null) {
				return;
			}

			if (message instanceof SampleMessage) {
				if (isSending) {
					this.ioAdapter.sampleMessageSent(session,
							(SampleMessage) message);
				} else {
					this.ioAdapter.sampleMessageReceived(session,
							(SampleMessage) message);
				}
			}

			else if (message instanceof HandshakeMessage) {
				if (isSending) {
					this.ioAdapter.handshakeMessageSent(session,
							(HandshakeMessage) message);
				} else {
					this.ioAdapter.handshakeMessageReceived(session,
							(HandshakeMessage) message);
				}
			} else {
				log.warn("Unknown message type received from {}: {}", session,
						message);
			}
		}

		public void setSession(IoSession session) {
			this.session = session;
		}

		public void setIoAdapter(SensorIoAdapter ioAdapter) {
			this.ioAdapter = ioAdapter;
		}

		public void setSending(boolean isSending) {
			this.isSending = isSending;
		}

		public void setMessage(Object message) {
			this.message = message;
		}
	}

	public ThreadedSensorIoHandler(SensorIoAdapter sensorIoAdapter) {
		super(sensorIoAdapter);
	}

	@Override
	public void exceptionCaught(final IoSession session, final Throwable cause)
			throws Exception {

		if (this.sensorIoAdapter != null) {
			this.messageHandlerPool.execute(new Runnable() {

				@Override
				public void run() {
					sensorIoAdapter.exceptionCaught(session, cause);

				}
			});
		} else {
			log.error("Unhandled exception caught in session {}: {}", session,
					cause);
		}
	}

	@Override
	public void messageReceived(final IoSession session, final Object message)
			throws Exception {
		if (this.sensorIoAdapter == null) {
			log.warn(
					"No SensorIoAdapter defined. Ignoring message from {}: {}",
					session, message);
			return;
		}

		MessageHandlerTask task = new MessageHandlerTask();
		task.setIoAdapter(this.sensorIoAdapter);
		task.setSession(session);
		task.setMessage(message);
		task.setSending(false);
		
		this.messageHandlerPool.execute(task);
	}

	@Override
	public void messageSent(final IoSession session, final Object message)
			throws Exception {

		log.debug("Sent message to {}: {}", session, message);
		if (this.sensorIoAdapter == null) {
			log.warn("No SensorIoAdapter defined. Ignoring message to {}: {}",
					session, message);
			return;
		}

		MessageHandlerTask task = new MessageHandlerTask();
		task.setIoAdapter(this.sensorIoAdapter);
		task.setSession(session);
		task.setMessage(message);
		task.setSending(true);
		
		this.messageHandlerPool.execute(task);

	}

	@Override
	public void sessionClosed(final IoSession session) throws Exception {
		log.debug("Session closed for sensor {}.", session);
		if (this.sensorIoAdapter != null) {
			this.messageHandlerPool.execute(new Runnable() {

				@Override
				public void run() {
					sensorIoAdapter.sensorDisconnected(session);
				}
			});
		}
	}

	@Override
	public void sessionIdle(final IoSession session, final IdleStatus status)
			throws Exception {
		log.debug("Sensor session for {} is idle: {}", session, status);
		if (this.sensorIoAdapter != null) {
			this.messageHandlerPool.execute(new Runnable() {

				@Override
				public void run() {
					sensorIoAdapter.sessionIdle(session, status);
				}
			});
		}

	}

	@Override
	public void sessionOpened(final IoSession session) throws Exception {
		log.debug("Session opened for sensor {}.", session);
		if (this.sensorIoAdapter != null) {
			this.messageHandlerPool.execute(new Runnable() {

				@Override
				public void run() {
					sensorIoAdapter.sensorConnected(session);
				}
			});
		}
	}

}
