/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *  
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.libsensor.protocol.messages;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.grailrtls.libcommon.util.NumericUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A network protocol message that represents a received "sample" from a
 * transmitter by a receiver.
 * 
 * @author Robert Moore II
 * 
 */
public class SampleMessage {

	/**
	 * Identifier for the PIPSQUEAK physical layer.
	 */
	public static final byte PHYISCAL_LAYER_PIPSQUEAK = 1;

	/**
	 * Identifier for the 802.11 (WiFi) physical layer.
	 */
	public static final byte PHYSICAL_LAYER_WIFI = 2;
	
	/**
	 * Identifier for the WINS physical layer.
	 */
	public static final byte PHYSICAL_LAYER_WINS = 3;

	/**
	 * The length (in octets) for the device identifier.
	 */
	public static final int DEVICE_ID_SIZE = 16;

	/**
	 * The length (in octets) for the receiver identifier.
	 */
	public static final int RECEIVER_ID_SIZE = 16;

	/**
	 * The physical layer type for this sample.
	 */
	protected byte physicalLayer;

	/**
	 * The device identifier for the transmitter.
	 */
	protected byte[] deviceId;

	/**
	 * The device identifier for the receiver.
	 */
	protected byte[] receiverId;

	/**
	 * The UNIX timestamp indicating when this sample was received by the
	 * receiver.
	 */
	protected long receivedTimestamp;
	
	/**
	 * The UNIX timestamp indicating when this sample was created.
	 */
	protected final long creationTimestamp;
	
	/**
	 * Creates a new SampleMessage.
	 */
	public SampleMessage(){
		this.creationTimestamp = System.currentTimeMillis();
	}
	
	public long getCreationTimestamp() {
		return creationTimestamp;
	}

	/**
	 * @return the UNIX timestamp indicating when the data in this sample was
	 *         received.
	 */
	public long getReceivedTimestamp() {
		return this.receivedTimestamp;
	}

	/**
	 * @param receivedTimestamp
	 *            the UNIX timestamp indicating when the data in this sample was
	 *            received.
	 */
	public void setReceivedTimestamp(long receivedTimestamp) {
		this.receivedTimestamp = receivedTimestamp;
	}

	/**
	 * The Received Signal Strength Indicator (RSSI) of the received packet.
	 */
	protected float rssi;

	/**
	 * Any sensed data associated with this sample.
	 */
	protected byte[] sensorData;

	/**
	 * Generates a test sample message. Useful for testing protocol
	 * implementations.
	 * 
	 * @return a test sample message.
	 */
	public static SampleMessage getTestMessage() {
		SampleMessage message = new SampleMessage();

		message.physicalLayer = (byte) 1;
		message.deviceId = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0 };
		message.receiverId = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0 };
		message.rssi = -50f;
		message.sensorData = null;
		message.receivedTimestamp = System.currentTimeMillis();

		return message;
	}

	/**
	 * Returns the length of this message in octets, ignoring the 4-byte length
	 * header of the byte-encoded form.
	 * 
	 * @return the length of this message in octets.
	 */
	public int getLength() {
		// messageType, physicalLayer, deviceId, receiverId, timestamp, rssi
		int length = 1 + DEVICE_ID_SIZE + RECEIVER_ID_SIZE + 8 + 4;
		if (this.sensorData != null) {
			length += this.sensorData.length;
		}

		return length;
	}

	/**
	 * Returns the physical layer identifier for this sample.
	 * 
	 * @return the physical layer identifier for this sample.
	 */
	public byte getPhysicalLayer() {
		return this.physicalLayer;
	}

	/**
	 * Sets the physical layer identifier for this sample.
	 * 
	 * @param physicalLayer
	 *            the physical layer identifier for this sample.
	 */
	public void setPhysicalLayer(byte physicalLayer) {
		this.physicalLayer = physicalLayer;
	}

	/**
	 * Returns the device identifier for the transmitter associated with this
	 * sample.
	 * 
	 * @return the transmitter's device identifier.
	 */
	public byte[] getDeviceId() {
		return this.deviceId;
	}

	/**
	 * Sets the transmitter device identifier for this sample.
	 * 
	 * @param deviceId
	 *            the transmitter device identifier.
	 */
	public void setDeviceId(byte[] deviceId) {
		this.deviceId = deviceId;
	}

	/**
	 * Returns the receiver device identifier for this sample.
	 * 
	 * @return the receiver device identifier for this sample.
	 */
	public byte[] getReceiverId() {
		return this.receiverId;
	}

	/**
	 * Sets the receiver device identifier for this sample.
	 * 
	 * @param receiverId
	 *            the receiver device identifier.
	 */
	public void setReceiverId(byte[] receiverId) {
		this.receiverId = receiverId;
	}

	/**
	 * Returns the Received Signal Strength Indicator (RSSI) observed by the
	 * receiver for this sample.
	 * 
	 * @return the RSSI of the sample.
	 */
	public float getRssi() {
		return this.rssi;
	}

	/**
	 * Sets the Received Signal Strength Indicator (RSSI) observed by the
	 * receiver for this sample.
	 * 
	 * @param rssi
	 *            the RSSI of the sample
	 */
	public void setRssi(float rssi) {
		this.rssi = rssi;
	}

	/**
	 * Retrieves the sensor data collected for this sample.
	 * 
	 * @return the sensor data collected for this sample.
	 */
	public byte[] getSensorData() {
		return this.sensorData;
	}

	/**
	 * Sets the sensor data collected for this sample.
	 * 
	 * @param sensorData
	 *            the sensor data collected for this sample.
	 */
	public void setSensorData(byte[] sensorData) {
		this.sensorData = sensorData;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(this.getLength());

		sb.append("Sensor Sample (").append(this.physicalLayer).append(", ")
				.append(NumericUtils.toHexString(this.getDeviceId())).append(
						", ").append(
						NumericUtils.toHexString(this.getReceiverId())).append(
						"): ").append(this.getRssi()).append(" @ ").append(
						this.getReceivedTimestamp());
		if (this.getSensorData() != null) {
			sb.append(" [").append(
					NumericUtils.toHexString(this.getSensorData())).append(']');
		}

		return sb.toString();
	}
}
