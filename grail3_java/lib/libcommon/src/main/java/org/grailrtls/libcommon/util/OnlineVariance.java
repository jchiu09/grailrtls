/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.libcommon.util;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OnlineVariance implements Cloneable {
	
	private static final Logger log = LoggerFactory.getLogger(OnlineVariance.class);

	protected float currentVariance = 0;

	public float getCurrentVariance() {
		return currentVariance;
	}

	protected final ConcurrentLinkedQueue<Float> history = new ConcurrentLinkedQueue<Float>();

	protected int sizeHistory = 0;

	protected int maxHistory = 5;

	public int getMaxHistory() {
		return maxHistory;
	}

	public void setMaxHistory(int maxHistory) {
		this.maxHistory = maxHistory;
	}

	protected long maxAge = 10000;

	public long getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(long maxAge) {
		this.maxAge = maxAge;
	}

	float sum;
	float sum_squares;
	long last_time;

	public float addValue(final float value)
	{
		long now = System.currentTimeMillis();
		//If more than 15 seconds passed then clear the data since it is
	    //too old at this point.
	    if (now - last_time > this.maxAge) {
	      this.history.clear();
	      sum = 0;
	      sum_squares = 0;
	      this.sizeHistory = 0;
	      this.currentVariance = 0;
	    }
	    last_time = now;
	    if (this.sizeHistory < this.maxHistory) {
	      sum += value;
	      sum_squares += Math.pow(value,2);
	      ++this.sizeHistory;
	    } else {
	      Float oldest_val = this.history.poll();
	      if(oldest_val != null){
		      sum = sum - oldest_val + value;
		      sum_squares = sum_squares - (oldest_val*oldest_val) + (float)Math.pow(value,2);
	      }
	      else
	      {
	    	  log.warn("Could not poll history to update variance.");
	      }
	    }
	    this.history.offer(value);
	    
	    float degrees_of_freedom = this.sizeHistory - 1;
	    if (degrees_of_freedom < 1.0) {
	      return 0f;
	    }
	    this.currentVariance =(sum_squares - (sum*sum / this.sizeHistory))/degrees_of_freedom; 
	    return this.currentVariance;
	    
	}

	public void reset() {
		this.sum = 0f;
		this.sum_squares = 0f;
		this.currentVariance = 0;
		this.history.clear();
		this.sizeHistory = 0;
	}
	
	public OnlineVariance clone(){
		OnlineVariance clone = new OnlineVariance();
		clone.currentVariance = this.currentVariance;
		clone.maxAge = this.maxAge;
		clone.maxHistory = this.maxHistory;
		clone.sum = this.sum;
		clone.sum_squares = this.sum_squares;
		clone.last_time = this.last_time;
		for(Float value : this.history){
			clone.history.offer(value);
		}
		clone.sizeHistory = clone.history.size();
		return clone;
	}
}
