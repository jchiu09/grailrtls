/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *  
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.libcommon.util;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Translates world server data types into Java data types.
 * @author Robert Moore
 *
 */
public class FieldDecoder {

	/**
	 * Private logging facility.
	 */
	private static final Logger log = LoggerFactory.getLogger(FieldDecoder.class);
	
	/**
	 * Converts a byte[] into a Java object depending on the value of {@code fieldName}.
	 * @param fieldName the world server field type name.
	 * @param data the binary representation of the field.
	 * @return a Java object generated from the world server data, or {@code data} if
	 * the field type is unrecognized.
	 */
	public static Object decodeField(String fieldName, byte[] data)
	{
		log.debug("Decoding {}", fieldName);
		if("uint32_t".equals(fieldName))
		{
			ByteBuffer buff = ByteBuffer.wrap(data);
			return Integer.valueOf(buff.getInt());
		}
		else if("uint64_t".equals(fieldName))
		{
			ByteBuffer buff = ByteBuffer.wrap(data);
			return Long.valueOf(buff.getLong());
		}
		else if("double".equals(fieldName))
		{
			ByteBuffer buff = ByteBuffer.wrap(data);
			return Double.valueOf(buff.getDouble());
		}
		else if("string".equals(fieldName))
		{
			return new String(data,Charset.forName("UTF-16BE"));
		}
		else
		{
			log.warn("Unknown field type {}.",fieldName);
			return data;
		}
	}
	
}
