package org.grailrtls.app.weather;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import org.grailrtls.libworldmodel.client.ClientWorldModelInterface;
import org.grailrtls.libworldmodel.client.listeners.DataListener;
import org.grailrtls.libworldmodel.client.protocol.messages.AbstractRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.AttributeAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.DataResponseMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.DataResponseMessage.Attribute;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.StreamRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.URISearchResponseMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.grailrtls.util.NumericUtils;
import com.google.gson.Gson;
import javax.mail.*;
import java.util.*;

public class WeatherWindowApp implements DataListener
{
    private final String API_KEY = "63bc28623ce6c6a2";

    private final ClientWorldModelInterface worldModel = new ClientWorldModelInterface();

    private static int TEMP_THRESHOLD = 50;

    private static long WAIT_TIME = 1000 * 20;

    private Emailer emailer = new Emailer();

    private Hashtable<String, UriObject> uriObjects = new Hashtable<String, UriObject>();

    private Hashtable<String, WeatherInfo> regionWeather = new Hashtable<String, WeatherInfo>();

    private Hashtable<String, NotifyThread> notifyThreads = new Hashtable<String, NotifyThread>();

    private Hashtable<String, Long> regionTime = new Hashtable<String, Long>();

    private String dirXml = "http://www.cs.rutgers.edu/people/directory.php?output=xml";

    private static boolean debug = false;

    private String worldHost;

    private int worldPort;

    private static String originName = null;

    private final String WINDOW_OPEN_URI = "open";

    private final String REGION = "region";

    private final String OPEN = "open";

    private final String CLOSED = "closed";

    private String uriRegex = ".*window.*";

    private static final Logger log = LoggerFactory.getLogger(WeatherWindowApp.class);

    private static boolean sendEmail = true;

    /**
     * URI Object class that holds the attributes that are needed to determine whether or not
     * a notification thread should be started.
     * @author Jonathan
     *
     */
    public class UriObject
    {
	private String room = null;
	private String region = null;
	private String open = null;

	public String getRegion()
	{
	    return region;
	}

	public String getOpen()
	{
	    return open;
	}

	public String getRoom()
	{
	    return room;
	}

	public void setRoom(String room)
	{
	    this.room = room;
	}

	public void setRegion(String region)
	{
	    this.region = region;
	}

	public void setOpen(String open)
	{
	    this.open = open;
	}

	public boolean isOpen()
	{
	    try
	    {
		if (this.open.equals(OPEN))
		    return true;
		else
		    return false;
	    } catch (Exception e)
	    {
		return false;
	    }
	}

	public boolean ready()
	{
	    if (room != null && region != null && open != null)
	    {
		if (open.equals(OPEN))
		    return true;
		else
		    return false;
	    }
	    else
		return false;
	}

	@Override
	public String toString()
	{
	    return "UriObject [room=" + room + ", region=" + region + ", open=" + open + "]";
	}

    }

    public static void main(String[] args)
    {
	if (args.length < 2)
	{
	    printUsageInfo();
	    System.exit(1);
	}

	if (args.length >= 3)
	{
	    for (String s : args)
	    {
		if (s.equalsIgnoreCase("d"))
		{
		    debug = true;
		    System.out.println("Debug mode enabled.");
		}
		else if (s.equalsIgnoreCase("h"))
		{
		    printUsageInfo();
		    System.exit(1);
		}
		else
		{
		    String[] temp = s.split("=");
		    if (temp.length != 2)
			continue;
		    if (temp[0].equalsIgnoreCase("origin"))
		    {
			originName = temp[1];
			System.out.println("Origin name filter enabled. (" + originName + ")");
		    }
		    else if (temp[0].equalsIgnoreCase("threshold"))
		    {
			try
			{
			    TEMP_THRESHOLD = Integer.parseInt(temp[1]);
			    System.out.println("Temperature threshold set to " + TEMP_THRESHOLD + ".");
			} catch (Exception e)
			{

			}
		    }
		    else if (temp[0].equalsIgnoreCase("wait"))
		    {
			try
			{
			    WAIT_TIME = Long.parseLong(temp[1]) * 1000;
			    System.out.println("Window open wait time set to " + WAIT_TIME / 1000 + " seconds.");
			} catch (Exception e)
			{

			}
		    }
		    else if (temp[0].equalsIgnoreCase("email"))
		    {
			if (temp[1].equalsIgnoreCase("disable"))
			{
			    sendEmail = false;
			    System.out.println("Email disabled. Actual emails will not be sent.");
			}
		    }
		}

	    }
	}

	int worldPort = Integer.parseInt(args[1]);
	WeatherWindowApp wwapp = new WeatherWindowApp(args[0], worldPort);
	wwapp.connect();
    }

    protected static void printUsageInfo()
    {
	System.out.println("Requires two parameters: <World Model host> <World Model port> [<debug>] [origin=<origin name>] [threshold=<threshold>] [wait=<wait time in seconds>].");
	System.out.println("Example: grail-dev1.rutgers.edu 7013 d origin=sample.solver threshold=50 wait=10");
	System.out.println("Once running, type \"exit\" to exit.");
    }

    public WeatherWindowApp(String worldHost, int worldPort)
    {
	this.worldHost = worldHost;
	this.worldPort = worldPort;

	this.worldModel.setHost(worldHost);
	this.worldModel.setPort(worldPort);

	this.worldModel.setStayConnected(true);
	this.worldModel.setDisconnectOnException(true);
	this.worldModel.addDataListener(this);

	this.worldModel.registerSearchRequest(uriRegex);
    }

    public void connect()
    {
	if (!this.worldModel.doConnectionSetup())
	{
	    System.out.println("Unable to establish a connection to the world model.");
	    System.exit(1);
	}
	System.out.println("Connected to world model at " + this.worldModel.getHost() + ":" + this.worldModel.getPort());

    }

    /**
     * Notification thread that starts when someone opens a window
     * @author Jonathan
     *
     */
    public class NotifyThread extends Thread implements DataListener
    {
	private String email = null;
	private String room = null;
	private long sleepTime = WAIT_TIME;
	private long idleTime = 1000 * 60 * 10; // 10 minutes
	private long longTime = 1000 * 60 * 60 * 10; // 10 hours
	private long waitTime = 1000 * 5; // 5 seconds
	private String latitude = null, longitude = null;
	private String region;
	private boolean llexist = true;
	private int lowTemp = 1000;
	private WeatherInfo weather = null;
	private long waitThreshold = 1000 * 60 * 60 * 4; // 4 hours

	ClientWorldModelInterface worldModel = new ClientWorldModelInterface();

	public NotifyThread(String room, String region)
	{
	    this.room = room;
	    this.region = region;

	    this.worldModel.setHost(worldHost);
	    this.worldModel.setPort(worldPort);

	    this.worldModel.setStayConnected(true);
	    this.worldModel.setDisconnectOnException(true);
	    this.worldModel.addDataListener(this);
	    this.worldModel.registerSearchRequest(this.region);

	}

	public void connect()
	{
	    if (!this.worldModel.doConnectionSetup())
	    {
		dprint("Unable to establish a connection to the world model.");
		System.exit(1);
	    }
	    dprint("Connected to world model at " + this.worldModel.getHost() + ":" + this.worldModel.getPort());

	}

	public void dprint(String s)
	{
	    if (debug)
	    {
		System.out.println("NotifyThread [" + this.getName() + "]: " + s);
	    }
	}

	public void run()
	{
	    dprint("NotifyThread started for " + this.getName() + "...");

	    connect();

	    try
	    {
		// Wait sleepTime and then send email, then wait sleepTime before sending another
		// if interrupted, end thread
		while (true)
		{
		    if (latitude != null && longitude != null)
		    {
			dprint("Waiting " + sleepTime / 1000 + " seconds.");
			Thread.sleep(sleepTime);

			if (!regionTime.containsKey(region))
			{
			    dprint("Fetching weather info for this region for the first time.");
			    regionTime.put(region, System.currentTimeMillis());
			    weather = getWeather(latitude, longitude);
			    regionWeather.put(region, weather);
			    lowTemp = getLowTemp(weather);
			}
			else
			{
			    dprint("Weather info was recently fetched for this region.");
			    if (System.currentTimeMillis() - regionTime.get(region) > waitThreshold) // weather info is outdated
			    {
				dprint("Local weather info is outdated, fetching weather from web.");
				regionTime.put(region, System.currentTimeMillis());
				weather = getWeather(latitude, longitude);
				regionWeather.put(region, weather);
				lowTemp = getLowTemp(weather);
			    }
			    else
			    {
				dprint("Local weather info is recent, using local weather info.");
				weather = regionWeather.get(region);
				lowTemp = getLowTemp(weather);
			    }
			}

			if (lowTemp < TEMP_THRESHOLD) // if the low is less than threshold, send email
			{
			    dprint("The low (" + lowTemp + "F) is lower than threshold (" + TEMP_THRESHOLD + "F), sending email.");

			    this.email = getEmail(this.room);
			    if (this.email != null)
			    {
				if (sendEmail)
				{
				    sendEmail(this.email, lowTemp);
				}
				else
				{
				    dprint("Email sending has been disabled, so an email was not actually sent to " + this.email);
				}
				sleepTime = idleTime;
			    }
			    else
			    {
				dprint("No matching email found for room " + this.room + " in directory!");
			    }
			}
			else
			// otherwise, wait longTime before checking weather again
			{
			    dprint("The low is above threshold, not sending email.");
			    sleepTime = longTime;
			}
		    }
		    else if (llexist) // latitude and longitude exist for region, but have not been retrieved yet
		    {
			Thread.sleep(waitTime);
		    }
		    else if (!llexist) // latitude and longitude do not exist for region
		    {
			dprint("Latitude and Longitude do not exist for region " + region + ", ending NotifyThread.");
			return;
		    }
		}
	    } catch (InterruptedException e)
	    {
		dprint("Thread interrupted, ending NotifyThread for " + this.getName() + "...");
		this.worldModel.doConnectionTearDown();
	    }
	}

	/**
	 * Gets the email of the person corresponding to the room from XML directory
	 */
	private String getEmail(String room)
	{
	    Hashtable<String, Person> directory = XMLDirectoryParser.getPersons(dirXml);
	    String email = null;
	    if (directory.containsKey(room))
	    {
		email = directory.get(room).getEmail();
	    }

	    return email;
	}

	@Override
	public void attributeAliasesReceived(ClientWorldModelInterface arg0,
		AttributeAliasMessage arg1)
	{
	    // TODO Auto-generated method stub

	}

	@Override
	public void dataResponseReceived(ClientWorldModelInterface arg0, DataResponseMessage msg)
	{

	    Attribute[] attr = msg.getAttributes();

	    try
	    {
		for (Attribute a : attr)
		{
		    if (a.getAttributeName().equals("latitude"))
		    {
			latitude = new String(a.getData(), "UTF-16BE");
		    }
		    else if (a.getAttributeName().equals("longitude"))
		    {
			longitude = new String(a.getData(), "UTF-16BE");
		    }
		}
	    } catch (UnsupportedEncodingException e1)
	    {
		e1.printStackTrace();
	    }

	    if (latitude == null || longitude == null)
	    {
		dprint("ERROR: Latitude or longitude does not exist for this region! (" + region + ")");
		llexist = false;
	    }
	    else
	    {
		dprint("Latitude and longitude found: " + latitude + ", " + longitude);
	    }

	}

	@Override
	public void originAliasesReceived(ClientWorldModelInterface arg0, OriginAliasMessage arg1)
	{
	    // TODO Auto-generated method stub

	}

	@Override
	public void requestCompleted(ClientWorldModelInterface arg0, AbstractRequestMessage arg1,
		int arg2)
	{
	    // TODO Auto-generated method stub

	}

	@Override
	public void requestTicketReceived(ClientWorldModelInterface arg0,
		AbstractRequestMessage arg1, int arg2)
	{
	    // TODO Auto-generated method stub

	}

	@Override
	public void uriSearchResponseReceived(ClientWorldModelInterface arg0,
		URISearchResponseMessage message)
	{
	    log.info("{} sent {}", this.worldModel, message);
	    long now = System.currentTimeMillis();
	    if (message.getMatchingUris() != null)
	    {
		for (String uri : message.getMatchingUris())
		{
		    StreamRequestMessage request = new StreamRequestMessage();
		    request.setBeginTimestamp(now);
		    request.setUpdateInterval(0l);
		    request.setQueryURI(uri);
		    request.setQueryAttributes(new String[] { ".*" });
		    this.worldModel.sendMessage(request);
		}
	    }
	}
    } // end NotifyThread

    /**
     * Reads in the attributes and determines whether or not a notification thread should be started
     */
    public synchronized void readIn(DataResponseMessage msg)
    {
	// dprint("Reading in DataResponseMessage from " + msg.getUri() + "...");
	try
	{
	    Attribute[] attr = msg.getAttributes();
	    String uri = msg.getUri();

	    if (!uriObjects.containsKey(uri))
	    {
		UriObject temp = new UriObject();
		uriObjects.put(uri, temp);
	    }
	    boolean stateChanged = false;

	    for (Attribute a : attr)
	    {
		String attrName = a.getAttributeName();
		if (originName == null)
		{
		    // do nothing, go on
		}
		else if (!a.getOriginName().equals(originName))
		{
		    // if originName is set, then filter out attributes that don't match
		    dprint("Attribute origin '" + a.getOriginName() + "' does not match '" + originName + "', ignoring...");
		    continue;
		}

		if (attrName.equals(WINDOW_OPEN_URI))
		{
		    String s = new String(a.getData(), "UTF-16BE");
		    // dprint("Reading attribute... " + attrName + " = " + s);

		    if (s.equals(OPEN))
		    {
			// dprint(uri + " is open...");
			uriObjects.get(uri).setOpen(OPEN);
		    }
		    else if (s.equals(CLOSED))
		    {
			// dprint(uri + " is closed...");
			uriObjects.get(uri).setOpen(CLOSED);
		    }
		    stateChanged = true;
		}
		else if (attrName.equals("room"))
		{
		    String room = new String(a.getData(), "UTF-16BE");
		    dprint("Reading attribute... " + attrName + " = " + room);
		    uriObjects.get(uri).setRoom(room);
		}
		else if (attrName.equals(REGION))
		{
		    String reg = new String(a.getData(), "UTF-16BE");
		    uriObjects.get(uri).setRegion(reg);
		    dprint("Reading attribute... " + attrName + " = " + reg);
		}
	    }

	    if (uriObjects.get(uri).ready() && !stateChanged) // if room or region changed, restart the thread
	    {
		restartNotifyThread(uri);
	    }
	    else if (uriObjects.get(uri).ready() && stateChanged) // if window is open, start a thread if there isn't already one started
	    {
		startNotifyThread(uri);
	    }
	    else if (!uriObjects.get(uri).isOpen()) // if window is closed, end the thread
	    {
		endNotifyThread(uri);
	    }

	} catch (Exception e)
	{
	    e.printStackTrace();
	}
    }

    /**
     * Restarts the notification thread that is mapped to uri
     */
    private void restartNotifyThread(String uri)
    {
	endNotifyThread(uri);
	startNotifyThread(uri);
    }

    /**
     * Ends the notification thread for uri
     */
    private void endNotifyThread(String uri)
    {
	if (notifyThreads.containsKey(uri))
	{
	    dprint("Ending thread " + uri);
	    notifyThreads.get(uri).interrupt();
	    notifyThreads.remove(uri);
	}
    }

    /**
     * Starts a new notification thread for uri
     */
    private void startNotifyThread(String uri)
    {
	if (!notifyThreads.containsKey(uri))
	{
	    dprint("Starting thread " + uri);
	    NotifyThread n = new NotifyThread(uriObjects.get(uri).getRoom(), uriObjects.get(uri).getRegion());
	    n.setName(uri);
	    notifyThreads.put(uri, n);
	    n.start();
	}
    }

    /**
     * Method to print only if debug mode enabled
     */
    public void dprint(String s)
    {
	if (debug)
	    System.out.println("[DEBUG] " + s);
	return;
    }

    /**
     * Gets the lowest temperature for the next 3 days
     */
    private int getLowTemp(WeatherInfo weatherInfo)
    {
	int lowTemp = 1000;
	if (weatherInfo == null)
	    return lowTemp;
	try
	{
	    for (int i = 0; i < 3; i++)
	    {
		int tmp = Integer.parseInt(weatherInfo.getForecast().getSimpleforecast().getForecastday().get(i).getLow().getFahrenheit());
		if (tmp < lowTemp)
		    lowTemp = tmp;
	    }

	} catch (Exception e)
	{
	    e.printStackTrace();
	}

	return lowTemp;
    }

    /**
     * Sends a notification email to specified email
     */
    private boolean sendEmail(String email, int temperature)
    {
	String[] recipients = { email };
	if (!emailer.sendNotification(recipients, "Please close your window", "Please remember to close your window before you leave for the weekend. The low this weekend is expected to be around " + temperature + " degrees Fahrenheit."))
	{
	    System.out.println("Error sending email.");
	    return false;
	}
	else
	{
	    System.out.println("Email has been sent.");
	    return true;
	}
    }

    /**
     * Gets the weather info for latitude and longitude
     */
    private WeatherInfo getWeather(String lat, String lon)
    {
	try
	{
	    String jsonTxt = "";
	    String url = "http://api.wunderground.com/api/" + API_KEY + "/geolookup/forecast/q/" + lat + "," + lon + ".json";

	    if (lat == null || lon == null)
	    {
		return null;
	    }

	    dprint("Requesting weather data...");
	    dprint(url);

	    URL jsonWeatherUrl = new URL(url);

	    BufferedReader in = new BufferedReader(new InputStreamReader(jsonWeatherUrl.openStream()));

	    String inputLine;

	    while ((inputLine = in.readLine()) != null)
	    {
		jsonTxt += inputLine;
	    }

	    // Parse json into WeatherInfo object
	    WeatherInfo data = new Gson().fromJson(jsonTxt, WeatherInfo.class);

	    return data;
	} catch (Exception e)
	{
	    return null;
	}
    }

    @Override
    public void requestTicketReceived(ClientWorldModelInterface worldModel,
	    AbstractRequestMessage message, int ticketNumber)
    {
	log.info("Request {} assigned ticket {} from " + worldModel.toString(), message, Integer.valueOf(ticketNumber));
    }

    @Override
    public void requestCompleted(ClientWorldModelInterface worldModel,
	    AbstractRequestMessage message, int ticketNumber)
    {
	log.info("Request {} completed ticket {} from " + worldModel.toString(), message, Integer.valueOf(ticketNumber));

    }

    @Override
    public void dataResponseReceived(ClientWorldModelInterface worldModel,
	    DataResponseMessage message)
    {
	// log.info("{} sent {}", worldModel, message);
	readIn(message);
    }

    @Override
    public void uriSearchResponseReceived(ClientWorldModelInterface worldModel,
	    URISearchResponseMessage message)
    {
	log.info("{} sent {}", worldModel, message);
	long now = System.currentTimeMillis();
	/*
	 * if (message.getMatchingUris() != null) { for (String uri : message.getMatchingUris()) { StreamRequestMessage request = new StreamRequestMessage(); request.setBeginTimestamp(now);
	 * request.setUpdateInterval(0l); request.setQueryURI(uri); request.setQueryAttributes(new String[] { ".*" }); worldModel.sendMessage(request); } }
	 */
	StreamRequestMessage request = new StreamRequestMessage();
	request.setBeginTimestamp(now);
	request.setUpdateInterval(0l);
	request.setQueryURI(uriRegex);
	request.setQueryAttributes(new String[] { ".*" });
	worldModel.sendMessage(request);

    }

    @Override
    public void attributeAliasesReceived(ClientWorldModelInterface clientWorldModelInterface,
	    AttributeAliasMessage message)
    {
	log.info("Received {} from {}", message, clientWorldModelInterface);
    }

    @Override
    public void originAliasesReceived(ClientWorldModelInterface clientWorldModelInterface,
	    OriginAliasMessage message)
    {
	log.info("Received {} from {}.", message, clientWorldModelInterface);

    }

}
