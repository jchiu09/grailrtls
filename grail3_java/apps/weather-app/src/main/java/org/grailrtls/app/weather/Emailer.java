package org.grailrtls.app.weather;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Emailer
{
    public boolean sendNotification(String[] recipients, String subject, String msg)
    {
	System.out.println("Sending email...");
	try
	{
	    String host = "smtp.gmail.com";
	    String from = "grail.notifier";
	    Properties props = System.getProperties();
	    props.put("mail.smtp.host", host);
	    props.put("mail.smtp.user", from);
	    props.put("mail.smtp.password", "Grail335");
	    props.put("mail.smtp.port", "587");
	    props.put("mail.smtp.auth", "true");

	    Session session = Session.getDefaultInstance(props, null);
	    MimeMessage message = new MimeMessage(session);
	    message.setFrom(new InternetAddress(from));
	    for (String s : recipients)
	    {
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(s));
	    }
	    message.setSubject("PLEASE CLOSE YOUR WINDOW");
	    message.setText(msg);

	    // alternately, to send HTML mail:
	    // message.setContent("<p>Welcome to JavaMail</p>", "text/html");
	    Transport transport = session.getTransport("smtps");
	    transport.connect("smtp.gmail.com", "grail.notifier", "Grail335");
	    transport.sendMessage(message, message.getAllRecipients());
	    transport.close();
	    return true;
	} catch (Exception e)
	{
	    e.printStackTrace();
	    return false;
	}
    }
}