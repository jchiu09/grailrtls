package org.grailrtls.app.weather;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLDirectoryParser
{
    private static Document dom;

    public static Hashtable<String, Person> getPersons(String file)
    {
	parseXmlFile(file);
	return parseDocument();
    }

    private static void parseXmlFile(String file)
    {
	// System.out.println("Parsing XML to DOM");
	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	try
	{
	    DocumentBuilder db = dbf.newDocumentBuilder();
	    dom = db.parse(file);

	} catch (ParserConfigurationException pce)
	{
	    pce.printStackTrace();
	} catch (SAXException se)
	{
	    se.printStackTrace();
	} catch (IOException ioe)
	{
	    ioe.printStackTrace();
	}
    }

    private static Hashtable<String,Person> parseDocument()
    {

	Hashtable<String, Person> persons = new Hashtable<String, Person>();
	// System.out.println("Parsing DOM to Person objects");
	Element docEle = dom.getDocumentElement();
	NodeList nl = docEle.getElementsByTagName("person");
	if (nl != null && nl.getLength() > 0)
	{
	    for (int i = 0; i < nl.getLength(); i++)
	    {
		Element el = (Element) nl.item(i);
		Person p = getPerson(el);
		if (p.getRoom() != null)
		    persons.put(p.getRoom(), p);
		// System.out.println(p);
	    }
	}
	return persons;
    }

    private static Person getPerson(Element e)
    {

	String name = getTextValue(e, "name");
	String phoneNumber = getTextValue(e, "phone");
	String room = getTextValue(e, "room");
	String email = getTextValue(e, "email");

	Person p = new Person(name, phoneNumber, room, email);

	return p;
    }

    /**
     * I take a xml element and the tag name, look for the tag and get the text content i.e for <employee><name>John</name></employee> xml snippet if the Element points to employee node and tagName is
     * name I will return John
     * 
     * @param ele
     * @param tagName
     * @return
     */
    private static String getTextValue(Element ele, String tagName)
    {
	String textVal = null;
	NodeList nl = ele.getElementsByTagName(tagName);
	if (nl != null && nl.getLength() > 0)
	{
	    Element el = (Element) nl.item(0);
	    textVal = el.getFirstChild().getNodeValue();
	}

	return textVal;
    }

    /**
     * Calls getTextValue and returns a int value
     * 
     * @param ele
     * @param tagName
     * @return
     */
    private int getIntValue(Element ele, String tagName)
    {
	// in production application you would catch the exception
	return Integer.parseInt(getTextValue(ele, tagName));
    }

}
