package org.grailrtls.app.weather;

public class Person
{
    private String name = null;

    private String phoneNumber = null;

    private String room = null;

    private String email = null;

    public Person(String name, String phoneNumber, String room, String email)
    {
	this.name = name;
	this.phoneNumber = phoneNumber;
	this.room = room;
	this.email = email;
    }

    @Override
    public String toString()
    {
	return "Person [name=" + name + ", phoneNumber=" + phoneNumber + ", room=" + room + ", email=" + email + "]";
    }

    public String getName()
    {
        return name;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public String getRoom()
    {
        return room;
    }

    public String getEmail()
    {
        return email;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public void setRoom(String room)
    {
        this.room = room;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

}
