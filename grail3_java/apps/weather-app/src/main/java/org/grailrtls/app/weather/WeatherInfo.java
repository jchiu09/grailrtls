package org.grailrtls.app.weather;

import java.util.List;

public class WeatherInfo
{
    private Forecast forecast;
    private WLocation location;

    @Override
    public String toString()
    {
	return "WeatherInfo [forecast=" + forecast + ", location=" + location + "]";
    }

    public Forecast getForecast()
    {
	return forecast;
    }

    public WLocation getLocation()
    {
	return location;
    }

    public void setForecast(Forecast forecast)
    {
	this.forecast = forecast;
    }

    public void setLocation(WLocation location)
    {
	this.location = location;
    }

}

class WLocation
{
    private String type;
    private String country_name;
    private String state;
    private String city;
    private String lat;
    private String lon;
    private String zip;

    @Override
    public String toString()
    {
	return "WLocation [type=" + type + ", country_name=" + country_name + ", state=" + state + ", city=" + city + ", lat=" + lat + ", lon=" + lon + ", zip=" + zip + "]";
    }

    public String getType()
    {
	return type;
    }

    public String getCountry_name()
    {
	return country_name;
    }

    public String getState()
    {
	return state;
    }

    public String getCity()
    {
	return city;
    }

    public String getLat()
    {
	return lat;
    }

    public String getLon()
    {
	return lon;
    }

    public String getZip()
    {
	return zip;
    }

    public void setType(String type)
    {
	this.type = type;
    }

    public void setCountry_name(String country_name)
    {
	this.country_name = country_name;
    }

    public void setState(String state)
    {
	this.state = state;
    }

    public void setCity(String city)
    {
	this.city = city;
    }

    public void setLat(String lat)
    {
	this.lat = lat;
    }

    public void setLon(String lon)
    {
	this.lon = lon;
    }

    public void setZip(String zip)
    {
	this.zip = zip;
    }

}

class Forecast
{
    private TxtForecast txt_forecast;
    private SimpleForecast simpleforecast;

    @Override
    public String toString()
    {
	return "Forecast [txt_forecast=" + txt_forecast + ", simpleforecast=" + simpleforecast + "]";
    }

    public TxtForecast getTxt_forecast()
    {
	return txt_forecast;
    }

    public SimpleForecast getSimpleforecast()
    {
	return simpleforecast;
    }

    public void setTxt_forecast(TxtForecast txt_forecast)
    {
	this.txt_forecast = txt_forecast;
    }

    public void setSimpleforecast(SimpleForecast simpleforecast)
    {
	this.simpleforecast = simpleforecast;
    }

}

class TxtForecast
{
    private String date;
    private List<ForecastDay> forecastday;

    @Override
    public String toString()
    {
	return "TxtForecast [date=" + date + ", forecastday=" + forecastday + "]";
    }

    public String getDate()
    {
	return date;
    }

    public List<ForecastDay> getForecastday()
    {
	return forecastday;
    }

    public void setDate(String date)
    {
	this.date = date;
    }

    public void setForecastday(List<ForecastDay> forecastday)
    {
	this.forecastday = forecastday;
    }

}

class ForecastDay
{
    private int period;
    // private String icon;
    private String title;
    private String fcttext;

    @Override
    public String toString()
    {
	return "ForecastDay [period=" + period + ", title=" + title + ", fcttext=" + fcttext + "]";
    }

    public int getPeriod()
    {
	return period;
    }

    public String getTitle()
    {
	return title;
    }

    public String getFcttext()
    {
	return fcttext;
    }

    public void setPeriod(int period)
    {
	this.period = period;
    }

    public void setTitle(String title)
    {
	this.title = title;
    }

    public void setFcttext(String fcttext)
    {
	this.fcttext = fcttext;
    }

}

class SimpleForecast
{
    public List<ForecastDay1> getForecastday()
    {
	return forecastday;
    }

    public void setForecastday(List<ForecastDay1> forecastday)
    {
	this.forecastday = forecastday;
    }

    private List<ForecastDay1> forecastday;

    @Override
    public String toString()
    {
	return "SimpleForecast [forecastday=" + forecastday + "]";
    }

}

class ForecastDay1
{
    private WDate date;
    private String period;
    private High high;
    private Low low;
    private String conditions;

    @Override
    public String toString()
    {
	return "ForecastDay1 [date=" + date + ", period=" + period + ", high=" + high + ", low=" + low + ", conditions=" + conditions + "]";
    }

    public WDate getDate()
    {
	return date;
    }

    public String getPeriod()
    {
	return period;
    }

    public High getHigh()
    {
	return high;
    }

    public Low getLow()
    {
	return low;
    }

    public String getConditions()
    {
	return conditions;
    }

    public void setDate(WDate date)
    {
	this.date = date;
    }

    public void setPeriod(String period)
    {
	this.period = period;
    }

    public void setHigh(High high)
    {
	this.high = high;
    }

    public void setLow(Low low)
    {
	this.low = low;
    }

    public void setConditions(String conditions)
    {
	this.conditions = conditions;
    }

}

class WDate
{
    private String epoch;
    private String pretty;
    private String tz_long;
    private String weekday;

    

    @Override
    public String toString()
    {
	return "WDate [epoch=" + epoch + ", pretty=" + pretty + ", tz_long=" + tz_long + ", weekday=" + weekday + "]";
    }

    public String getWeekday()
    {
        return weekday;
    }

    public void setWeekday(String weekday)
    {
        this.weekday = weekday;
    }

    public String getEpoch()
    {
	return epoch;
    }

    public String getPretty()
    {
	return pretty;
    }

    public String getTz_long()
    {
	return tz_long;
    }

    public void setEpoch(String epoch)
    {
	this.epoch = epoch;
    }

    public void setPretty(String pretty)
    {
	this.pretty = pretty;
    }

    public void setTz_long(String tz_long)
    {
	this.tz_long = tz_long;
    }

}

class Temperature
{
    private String fahrenheit;
    private String celsius;

    @Override
    public String toString()
    {
	return "Temperature [fahrenheit=" + fahrenheit + ", celsius=" + celsius + "]";
    }

    public String getFahrenheit()
    {
	return fahrenheit;
    }

    public String getCelsius()
    {
	return celsius;
    }

    public void setFahrenheit(String fahrenheit)
    {
	this.fahrenheit = fahrenheit;
    }

    public void setCelsius(String celsius)
    {
	this.celsius = celsius;
    }
}

class Low extends Temperature
{    
	
}

class High extends Temperature
{
   
}
