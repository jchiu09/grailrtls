package org.grailrtls.legacy.gcs.conversion.message;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ActiveLocalizationResultMessage extends ServerMessage {

	public static final byte MESSAGE_TYPE = 2;

	private List<DeviceLocationResult> locationResults;

	public List<DeviceLocationResult> getLocationResults() {
		return locationResults;
	}

	public void setLocationResults(List<DeviceLocationResult> locationResults) {
		this.locationResults = locationResults;
	}

	public static class DeviceLocationResult {

		private int regionId;

		private long deviceId;

		private int physicalLayer;

		private long timestamp;

		private float xCenter;

		private float yCenter;

		private float xRadius;

		private float yRadius;

		public int getRegionId() {
			return regionId;
		}

		public void setRegionId(int regionId) {
			this.regionId = regionId;
		}

		public long getDeviceId() {
			return deviceId;
		}

		public void setDeviceId(long deviceId) {
			this.deviceId = deviceId;
		}

		public int getPhysicalLayer() {
			return physicalLayer;
		}

		public void setPhysicalLayer(int physicalLayer) {
			this.physicalLayer = physicalLayer;
		}

		public long getTimestamp() {
			return timestamp;
		}

		public void setTimestamp(long timestamp) {
			this.timestamp = timestamp;
		}

		public float getxCenter() {
			return xCenter;
		}

		public void setxCenter(float xCenter) {
			this.xCenter = xCenter;
		}

		public float getyCenter() {
			return yCenter;
		}

		public void setyCenter(float yCenter) {
			this.yCenter = yCenter;
		}

		public float getxRadius() {
			return xRadius;
		}

		public void setxRadius(float xRadius) {
			this.xRadius = xRadius;
		}

		public float getyRadius() {
			return yRadius;
		}

		public void setyRadius(float yRadius) {
			this.yRadius = yRadius;
		}

	}

	@Override
	protected boolean decodeSpecificMessage(byte[] messageBytes)
			throws IOException {

		DataInputStream in = new DataInputStream(new ByteArrayInputStream(
				messageBytes));

		this.locationResults = new ArrayList<DeviceLocationResult>();

		while (in.available() > 0) {
			DeviceLocationResult location = new DeviceLocationResult();
			location.regionId = in.readInt();
			location.deviceId = in.readLong();
			location.physicalLayer = in.readInt();
			location.timestamp = in.readLong();
			location.xCenter = in.readFloat();
			location.yCenter = in.readFloat();
			location.xRadius = in.readFloat();
			location.yRadius = in.readFloat();
			this.locationResults.add(location);
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();

		sb.append(super.toString());
		sb.append(" | Localization Result");

		for (DeviceLocationResult result : this.getLocationResults()) {
			sb.append("\n");
			sb.append("  RegId: ").append(result.getRegionId());
			sb.append(" DevId|Phy: ").append(
					String.format("%08d", result.getDeviceId())).append("|")
					.append(result.getPhysicalLayer());
			sb.append(" TS: ").append(result.getTimestamp());
			sb.append(" Loc: (").append(
					String.format("%,06.2f", result.getxCenter()))
					.append("+/-").append(
							String.format("%,05.2f", result.getxRadius()))
					.append(", ").append(
							String.format("%,06.2f", result.getyCenter()))
					.append("+/-").append(
							String.format("%,05.2f", result.getyRadius()))
					.append(")");
		}

		return sb.toString();
	}

}
