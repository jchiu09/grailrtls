package org.grailrtls.legacy.gcs.conversion.message;

import java.awt.geom.Point2D;
import java.io.IOException;
import java.io.StringReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.grailrtls.legacy.gcs.conversion.message.GZIPFullInfoMessage.Hub.Landmark;
import org.grailrtls.legacy.gcs.conversion.message.GZIPFullInfoMessage.Hub.Landmark.LandmarkLocation;
import org.grailrtls.legacy.gcs.conversion.message.GZIPFullInfoMessage.Region.TrainingFile;
import org.grailrtls.legacy.gcs.conversion.message.GZIPFullInfoMessage.Region.TrainingFile.Landmark.RSSI;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GZIPFullInfoMessage extends GZIPMessage {

	private static final Logger log = LoggerFactory
			.getLogger(GZIPFullInfoMessage.class);
	
	public static final byte MESSAGE_TYPE = 10;

	private String xmlData;

	private List<Region> regions;
	private List<Hub> hubs;

	public static class Region {
		private String name;
		private int databaseId;
		private int units;
		private float xMin;
		private float xMax;
		private float yMin;
		private float yMax;
		private float zMin;
		private float zMax;

		private String imageURL;

		private List<TrainingFile> trainingFiles;

		public static class TrainingFile {
			private String fileName;
			private String displayName;

			private List<Landmark> landmarks;

			public static class Landmark {
				private String hubIdString;
				private String physicalLayerName;
				private int antenna;

				private float xPostion;
				private float yPosition;

				private List<RSSI> rssiValues;

				public static class RSSI {
					private float xPosition;
					private float yPosition;
					private float rssi;

					public float getxPosition() {
						return xPosition;
					}

					public void setxPosition(float xPosition) {
						this.xPosition = xPosition;
					}

					public float getyPosition() {
						return yPosition;
					}

					public void setyPosition(float yPosition) {
						this.yPosition = yPosition;
					}

					public float getRssi() {
						return rssi;
					}

					public void setRssi(float rssi) {
						this.rssi = rssi;
					}
				}

				public String getHubIdString() {
					return hubIdString;
				}

				public void setHubIdString(String hubIdString) {
					this.hubIdString = hubIdString;
				}

				public String getPhysicalLayerName() {
					return physicalLayerName;
				}

				public void setPhysicalLayerName(String physicalLayerName) {
					this.physicalLayerName = physicalLayerName;
				}

				public int getAntenna() {
					return antenna;
				}

				public void setAntenna(int antenna) {
					this.antenna = antenna;
				}

				public float getxPostion() {
					return xPostion;
				}

				public void setxPostion(float xPostion) {
					this.xPostion = xPostion;
				}

				public float getyPosition() {
					return yPosition;
				}

				public void setyPosition(float yPosition) {
					this.yPosition = yPosition;
				}

				public List<RSSI> getRssiValues() {
					return rssiValues;
				}

				public void setRssiValues(List<RSSI> rssiValues) {
					this.rssiValues = rssiValues;
				}
			}

			public String getFileName() {
				return fileName;
			}

			public void setFileName(String fileName) {
				this.fileName = fileName;
			}

			public String getDisplayName() {
				return displayName;
			}

			public void setDisplayName(String displayName) {
				this.displayName = displayName;
			}

			public List<Landmark> getLandmarks() {
				return landmarks;
			}

			public void setLandmarks(List<Landmark> landmarks) {
				this.landmarks = landmarks;
			}
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getDatabaseId() {
			return databaseId;
		}

		public void setDatabaseId(int databaseId) {
			this.databaseId = databaseId;
		}

		public int getUnits() {
			return units;
		}

		public void setUnits(int units) {
			this.units = units;
		}

		public float getxMin() {
			return xMin;
		}

		public void setxMin(float xMin) {
			this.xMin = xMin;
		}

		public float getxMax() {
			return xMax;
		}

		public void setxMax(float xMax) {
			this.xMax = xMax;
		}

		public float getyMin() {
			return yMin;
		}

		public void setyMin(float yMin) {
			this.yMin = yMin;
		}

		public float getyMax() {
			return yMax;
		}

		public void setyMax(float yMax) {
			this.yMax = yMax;
		}

		public float getzMin() {
			return zMin;
		}

		public void setzMin(float zMin) {
			this.zMin = zMin;
		}

		public float getzMax() {
			return zMax;
		}

		public void setzMax(float zMax) {
			this.zMax = zMax;
		}

		public String getImageURL() {
			return imageURL;
		}

		public void setImageURL(String imageURL) {
			this.imageURL = imageURL;
		}

		public List<TrainingFile> getTrainingFiles() {
			return trainingFiles;
		}

		public void setTrainingFiles(List<TrainingFile> trainingFiles) {
			this.trainingFiles = trainingFiles;
		}
	}

	public static class Hub {
		private String hubIdString;
		private String hubName;
		private boolean connected;
		private long connectedSince;
		private InetAddress ipAddress;

		private List<Landmark> landmarks;

		public static class Landmark {
			private String physicalLayerName;
			private int antenna;
			private List<LandmarkLocation> locations;

			public static class LandmarkLocation {
				private String regionName;
				private float xPosition;
				private float yPosition;

				public String getRegionName() {
					return regionName;
				}

				public void setRegionName(String regionName) {
					this.regionName = regionName;
				}

				public float getxPosition() {
					return xPosition;
				}

				public void setxPosition(float xPosition) {
					this.xPosition = xPosition;
				}

				public float getyPosition() {
					return yPosition;
				}

				public void setyPosition(float yPosition) {
					this.yPosition = yPosition;
				}
			}

			public String getPhysicalLayerName() {
				return physicalLayerName;
			}

			public void setPhysicalLayerName(String physicalLayerName) {
				this.physicalLayerName = physicalLayerName;
			}

			public int getAntenna() {
				return antenna;
			}

			public void setAntenna(int antenna) {
				this.antenna = antenna;
			}

			public List<LandmarkLocation> getLocations() {
				return locations;
			}

			public void setLocations(List<LandmarkLocation> locations) {
				this.locations = locations;
			}
		}

		public String getHubIdString() {
			return hubIdString;
		}

		public void setHubIdString(String hubIdString) {
			this.hubIdString = hubIdString;
		}

		public String getHubName() {
			return hubName;
		}

		public void setHubName(String hubName) {
			this.hubName = hubName;
		}

		public boolean isConnected() {
			return connected;
		}

		public void setConnected(boolean connected) {
			this.connected = connected;
		}

		public long getConnectedSince() {
			return connectedSince;
		}

		public void setConnectedSince(long connectedSince) {
			this.connectedSince = connectedSince;
		}

		public InetAddress getIpAddress() {
			return ipAddress;
		}

		public void setIpAddress(InetAddress ipAddress) {
			this.ipAddress = ipAddress;
		}

		public List<Landmark> getLandmarks() {
			return landmarks;
		}

		public void setLandmarks(List<Landmark> landmarks) {
			this.landmarks = landmarks;
		}
	}

	public String getXmlData() {
		byte[] decompressedBytes = this.decompressData();
		if (decompressedBytes == null) {
			return null;
		}

		this.xmlData = new String(decompressedBytes, Charset.forName("ASCII"));

		return xmlData;
	}
	
	public void parseXmlData()
	{
		if(this.xmlData == null)
		{
			log.error("No decompressed data to parse. Try calling getXmlData() first.");
			return;
		}
		
		this.handleXML(this.xmlData);
	}

	public void setXmlData(String xmlData) {
		this.xmlData = xmlData;
	}

	protected static final String XPATH_EVENT = "/event";
	protected static final String XPATH_STAT = "/event/statistics";
	protected static final String XPATH_RESULT = "/event/result";
	protected static final String XPATH_FULL_INFO = "/event/fullinfo";
	protected static final String XPATH_FINGERPRINT = "/event/fingerprint";
	protected static final String XPATH_HUB = "/event/hub";

	protected String handleXML(String xmlString) {
		int xmlStart = xmlString.indexOf("<event");
		int xmlEnd = xmlString.indexOf("</event>");
		if (xmlStart == -1 || xmlEnd == -1)
			return xmlString;

		String returnString = xmlString.substring(0, xmlStart)
				+ xmlString.substring(xmlEnd + 8);
		SAXBuilder saxBuilder = new SAXBuilder(
				"org.apache.xerces.parsers.SAXParser");
		Document xmlDocument;
		try {
			xmlDocument = saxBuilder.build(new StringReader(xmlString
					.substring(xmlStart, xmlEnd + 8)));
		} catch (IOException ioe) {
			System.err.println("Invalid XML?: " + xmlString);
			ioe.printStackTrace(System.err);
			return xmlString;
		} catch (JDOMException jdome) {
			System.err.println("Couldn't parse XML?: " + xmlString);
			jdome.printStackTrace(System.err);
			return xmlString;
		}

		// Check for a full info update (once only)
		XPath xPath = this.getXPath(XPATH_FULL_INFO);
		Element element = this.getElement(xPath, xmlDocument);
		if (element != null) {
			this.parseFullInfoXML(xmlDocument);
			return returnString;
		}

		return xmlString.trim();
	}

	protected static final String XPATH_FULL_INFO_REGION = "/event/fullinfo/region";
	protected static final String XPATH_FULL_INFO_HUB = "/event/fullinfo/hub";

	protected void parseFullInfoXML(Document xmlDocument) {
		XPath xPath = getXPath(XPATH_FULL_INFO);
		Element fullInfoElem = getElement(xPath, xmlDocument);
		if (fullInfoElem == null) {
			System.err.println("No full info element. Nothing to parse.");
			return;
		}
		xPath = getXPath(XPATH_EVENT);
		Element eventElem = getElement(xPath, xmlDocument);

		long timestamp = 0l;
		try {
			timestamp = Long
					.parseLong(eventElem.getAttributeValue("timestamp"));
		} catch (NumberFormatException nfe) {
			System.err.println("Could not parse timestamp for full info.");
		}

		// Load regions
		xPath = getXPath(XPATH_FULL_INFO_REGION);
		List<Element> regionElems = getElements(xPath, xmlDocument);

		for (Element regionElem : regionElems) {
			String regionName = regionElem.getAttributeValue("name");
			if (regionName == null || regionName.length() == 0) {
				System.err.println("Missing region name!");
				return;
			}
			int databaseID = -1;
			try {
				databaseID = Integer.parseInt(regionElem
						.getAttributeValue("dbid"));
			} catch (NumberFormatException nfe) {
				System.err.println("Invalid database ID for region "
						+ regionName);
			}
			int units = -1;
			try {
				units = Integer.parseInt(regionElem.getChildText("units"));
			} catch (NumberFormatException nfe) {
				System.err.println("Invalid units format for region "
						+ regionName);
			}
			if (units == -1)
				continue;
			float xMin = -1;
			try {
				xMin = Float.parseFloat(regionElem.getChildText("xmin"));
			} catch (NumberFormatException nfe) {
				System.err.println("Invalid x-min for " + regionName);
			}
			float xMax = -1;
			try {
				xMax = Float.parseFloat(regionElem.getChildText("xmax"));
			} catch (NumberFormatException nfe) {
				System.err.println("Invalid x-max for " + regionName);
			}
			float yMin = -1;
			try {
				yMin = Float.parseFloat(regionElem.getChildText("ymin"));
			} catch (NumberFormatException nfe) {
				System.err.println("Invalid y-min for " + regionName);
			}
			float yMax = -1;
			try {
				yMax = Float.parseFloat(regionElem.getChildText("ymax"));
			} catch (NumberFormatException nfe) {
				System.err.println("Invalid y-max for " + regionName);
			}
			float zMin = -1;
			try {
				zMin = Float.parseFloat(regionElem.getChildText("zmin"));
			} catch (NumberFormatException nfe) {
				System.err.println("Invalid z-min for " + regionName);
			}
			float zMax = -1;
			try {
				zMax = Float.parseFloat(regionElem.getChildText("zmax"));
			} catch (NumberFormatException nfe) {
				System.err.println("Invalid z-max for " + regionName);
			}

			String imageURL = regionElem.getChildText("imageurl");

			Region regionInfo = new Region();
			regionInfo.setName(regionName);
			regionInfo.setDatabaseId(databaseID);
			regionInfo.setUnits(units);
			regionInfo.setxMin(xMin);
			regionInfo.setxMax(xMax);
			regionInfo.setyMin(yMin);
			regionInfo.setyMax(yMax);
			regionInfo.setzMin(zMin);
			regionInfo.setzMax(zMax);
			regionInfo.setImageURL(imageURL);

			if (this.regions == null) {
				this.regions = new ArrayList<Region>();
			}
			this.regions.add(regionInfo);
		}

		xPath = getXPath(XPATH_FULL_INFO_HUB);
		List<Element> hubElems = getElements(xPath, xmlDocument);

		for (Element hubElem : hubElems) {
			String hubIdString = null;
			try {
				hubIdString = hubElem.getAttributeValue("id");
			} catch (NumberFormatException nfe) {
				System.err.println("Invalid id for hub.");
			}
			if (hubIdString == null)
				continue;
			String name = hubElem.getChildText("name");
			Element connectionElem = hubElem.getChild("connection");
			String connectionString = connectionElem
					.getAttributeValue("status");
			String connectedSinceString = connectionElem
					.getAttributeValue("since");
			long connectedSince = 0l;
			try {
				connectedSince = Long.parseLong(connectedSinceString);
			} catch (NumberFormatException nfe) {
				System.err.println("Invalid connected since value.");
			}
			boolean connected = connectionString.equalsIgnoreCase("true") ? true
					: false;
			String inetAddressString = connectionElem.getChildText("address");
			InetAddress hubAddress = null;
			if (inetAddressString != null) {
				try {
					hubAddress = InetAddress.getByName(connectionElem
							.getChildText("address"));
				} catch (UnknownHostException uhe) {
					System.err.println("Invalid hub address: "
							+ connectionElem.getChildText("address"));
				}
			}
			Hub hubInfo = new Hub();
			hubInfo.setHubIdString(hubIdString);
			hubInfo.setHubName(name);
			hubInfo.setConnected(connected);
			hubInfo.setIpAddress(hubAddress);
			if (connected && connectedSince != 0l)
				hubInfo.setConnectedSince(connectedSince);
			else
				hubInfo.setConnectedSince(0l);

			if (this.hubs == null) {
				this.hubs = new ArrayList<Hub>();
			}
			this.hubs.add(hubInfo);

			List<Element> landmarkElems = hubElem.getChildren("landmark");
			for (Element landmarkElem : landmarkElems) {
				String phy = landmarkElem.getAttributeValue("phy");
				int antenna = -1;
				try {
					antenna = Integer.parseInt(landmarkElem
							.getAttributeValue("antenna"));
				} catch (NumberFormatException nfe) {
					System.err.println("Unparseable antenna value.");
				}
				if (antenna == -1) {
					System.err.println("Invalid antenna value.");
					continue;
				}

				List<Element> positionElems = landmarkElem
						.getChildren("position");
				float x = 0f;
				float y = 0f;
				for (Element posElem : positionElems) {
					String regionName = posElem.getAttributeValue("region");
					// TODO: BIG HACK!!
					if (regionName != null) {
						Region region = null;
						for (Region someRegion : this.regions) {
							if (someRegion.getName().equalsIgnoreCase(
									regionName)) {
								region = someRegion;
								break;
							}
						}

						if (region == null) {
							System.err
									.println("Parsing full info XML: Could not find region "
											+ regionName);
							continue;
						}
						try {
							x = Float
									.parseFloat(posElem.getAttributeValue("x"));
						} catch (NumberFormatException nfe) {
							System.err
									.println("Unparseable landmark x position.");
						}
						if (x == 0f) {
							System.err.println("Invalid landmark x position.");
							continue;
						}

						try {
							y = Float
									.parseFloat(posElem.getAttributeValue("y"));
						} catch (NumberFormatException nfe) {
							System.err
									.println("Unparseable landmark y position.");
						}
						if (y == 0f) {
							System.err.println("Invalid landmark y position.");
							continue;
						}

						Landmark landmark = new Landmark();
						landmark.setPhysicalLayerName(phy);
						landmark.setAntenna(antenna);
						if (landmark.getLocations() == null) {
							landmark
									.setLocations(new ArrayList<LandmarkLocation>());
						}
						LandmarkLocation location = new LandmarkLocation();
						location.setxPosition(x);
						location.setyPosition(y);
						location.setRegionName(region.getName());

						if (hubInfo.getLandmarks() == null) {
							hubInfo.setLandmarks(new ArrayList<Landmark>());
						}
						hubInfo.getLandmarks().add(landmark);
					}
				}
			}
		}

		// Load training data

		xPath = getXPath(XPATH_FULL_INFO_REGION);
		regionElems = getElements(xPath, xmlDocument);

		for (Element regionElem : regionElems) {
			String regionName = regionElem.getAttributeValue("name");
			if (regionName == null || regionName.length() == 0) {
				System.err.println("Missing region name!");
				return;
			}

			Region region = null;
			for (Region someRegion : this.regions) {
				if (regionName.equalsIgnoreCase(someRegion.getName())) {
					region = someRegion;
					break;
				}
			}

			List<Element> trainingElems = regionElem.getChildren("training");
			for (Element trainingElem : trainingElems) {
				String fileName = trainingElem.getAttributeValue("file");
				String name = trainingElem.getAttributeValue("name");

				TrainingFile trainingInfo = new TrainingFile();
				trainingInfo.setDisplayName(name);
				trainingInfo.setFileName(fileName);
				
				if(region.getTrainingFiles() == null)
				{
					region.setTrainingFiles(new ArrayList<TrainingFile>());
				}
				region.getTrainingFiles().add(trainingInfo);
				
				
				boolean devicePositionsRead = false;
				List<Element> landmarkElems = trainingElem
						.getChildren("landmark");
				for (Element landmarkElem : landmarkElems) {
					String hubID = null;
					try {
						hubID = landmarkElem
								.getAttributeValue("id");
					} catch (NumberFormatException nfe) {
						System.err.println("Bad landmark id.");
					}
					if (hubID == null)
						continue;

					String phy = landmarkElem
							.getAttributeValue("phy");

					int antenna = -1;
					try {
						antenna = Integer.parseInt(landmarkElem
								.getAttributeValue("ant"));
					} catch (NumberFormatException nfe) {
						System.err.println("Bad landmark antenna.");
					}
					if (antenna == -1)
						continue;
					
					
					org.grailrtls.legacy.gcs.conversion.message.GZIPFullInfoMessage.Region.TrainingFile.Landmark landmarkInfo = new org.grailrtls.legacy.gcs.conversion.message.GZIPFullInfoMessage.Region.TrainingFile.Landmark();
					
					String xString = landmarkElem.getChildText("x");
					float x = -1f;
					try {
						x = Float.parseFloat(xString);
					} catch (NumberFormatException nfe) {
						System.err.println("Bad training x coordinate.");
					}
					if (x == -1f)
						continue;

					String yString = landmarkElem.getChildText("y");
					float y = -1f;
					try {
						y = Float.parseFloat(yString);
					} catch (NumberFormatException nfe) {
						System.err.println("Bad training y coordinate.");
					}
					if (y == -1f)
						continue;

					if(trainingInfo.getLandmarks() == null)
					{
						trainingInfo.setLandmarks(new ArrayList<org.grailrtls.legacy.gcs.conversion.message.GZIPFullInfoMessage.Region.TrainingFile.Landmark>());
					}
					
					trainingInfo.getLandmarks().add(landmarkInfo);
					landmarkInfo.setxPostion(x);
					landmarkInfo.setyPosition(y);
					List<Element> rssiElems = landmarkElem.getChildren("rssi");
					int index = 0;
					for (Element rssiElem : rssiElems) {
						float rssi = Float.NaN;
						try {
							rssi = Float.parseFloat(rssiElem.getText());
						} catch (NumberFormatException nfe) {
							System.err.println("Bad rssi value.");
						}
						if (!devicePositionsRead) {
							float devX = -1f;
							try {
								devX = Float.parseFloat(rssiElem
										.getAttributeValue("x"));
							} catch (NumberFormatException nfe) {
								System.err.println("Bad device x-position.");
							}
							float devY = -1f;
							try {
								devY = Float.parseFloat(rssiElem
										.getAttributeValue("y"));
							} catch (NumberFormatException nfe) {
								System.err.println("Bad device y-position.");
							}
							RSSI rssiObj = new RSSI();
							rssiObj.setxPosition(devX);
							rssiObj.setyPosition(devY);
							rssiObj.setRssi(rssi);
						}
					}
					devicePositionsRead = true;
				}
				if(region.getTrainingFiles() == null)
				{
					region.setTrainingFiles(new ArrayList<TrainingFile>());
				}
				region.getTrainingFiles().add(trainingInfo);
			}
		}

	}

	/**
	 * Converts an XPath string into an XPath object.
	 * 
	 * @param path
	 *            an String representing an XPath.
	 * @return the XPath object represented by {@code path}, or {@code null} if
	 *         the specified XPath is invalid.
	 */
	protected XPath getXPath(String path) {
		XPath xpath;
		try {
			xpath = XPath.newInstance(path);
		} catch (JDOMException jdome) {
			System.err.println("Could not create xpath for \"" + path + "\".");
			jdome.printStackTrace(System.err);
			return null;
		}
		return xpath;
	}

	/**
	 * Retrieves a list of XML elements from the specified document at the
	 * specified XPath.
	 * 
	 * @param xpath
	 *            the XPath to retrieve from the document.
	 * @param document
	 *            the XML document from which to retrieve the elements.
	 * @return a list of XML elements from the specified document/XPath, or
	 *         {@code null} if the XPath does not exist in the document.
	 */
	@SuppressWarnings("unchecked")
	protected List<Element> getElements(XPath xpath, Document document) {
		List<Element> elements;
		try {
			elements = xpath.selectNodes(document);
		} catch (JDOMException jdome) {
			System.err.println("Error while selecting elements. XPath: \""
					+ xpath.toString() + "\".");
			jdome.printStackTrace(System.err);
			return null;
		}
		return elements;
	}

	/**
	 * Retrieves an XML element from the specified document at the specified
	 * XPath.
	 * 
	 * @param xpath
	 *            the XPath to retrieve from the document.
	 * @param document
	 *            the XML document from which to retrieve the specified element.
	 * @return an XML element contained at the XPath within the document, or
	 *         {@code null} if it does not exist.
	 */
	protected Element getElement(XPath xpath, Document document) {
		Element element;
		try {
			element = (Element) xpath.selectSingleNode(document);
		} catch (JDOMException jdome) {
			System.err.println("Error while selecting an element. XPath: \""
					+ xpath.toString() + "\".");
			jdome.printStackTrace(System.err);
			return null;
		}
		return element;
	}

	public List<Region> getRegions() {
		return regions;
	}

	public void setRegions(List<Region> regions) {
		this.regions = regions;
	}

	public List<Hub> getHubs() {
		return hubs;
	}

	public void setHubs(List<Hub> hubs) {
		this.hubs = hubs;
	}

	@Override
	protected boolean decodeSpecificMessage(byte[] message) throws IOException {
		super.setZippedMessage(message);
		byte[] messageBytes = super.decompressData();
		String xmlData = new String(messageBytes, Charset.forName("ASCII"));

		this.handleXML(xmlData);
		
		return true;
	}
	
	@Override
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(super.toString()).append(" | Full Info\n");
		if(this.getXmlData() == null)
		{
			byte[] messageBytes = super.decompressData();
			this.setXmlData(new String(messageBytes,Charset.forName("ASCII")));
		}
		sb.append(this.getXmlData());
		return sb.toString();
	}
}
