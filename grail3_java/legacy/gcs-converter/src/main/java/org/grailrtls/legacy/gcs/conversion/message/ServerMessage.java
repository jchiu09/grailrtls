package org.grailrtls.legacy.gcs.conversion.message;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ServerMessage {
	
	public static final Logger log = LoggerFactory.getLogger(ServerMessage.class);

	private long messageArrivalTime;
	
	private int messageLength;
	
	private byte messageType;

	public long getMessageArrivalTime() {
		return messageArrivalTime;
	}

	public void setMessageArrivalTime(long messageArrivalTime) {
		this.messageArrivalTime = messageArrivalTime;
	}

	public int getMessageLength() {
		return messageLength;
	}

	public void setMessageLength(int messageLength) {
		this.messageLength = messageLength;
	}

	public byte getMessageType() {
		return messageType;
	}

	public void setMessageType(byte messageType) {
		this.messageType = messageType;
	}
	
	public static List<ServerMessage> decodeMessages(DataInputStream in) throws IOException
	{
		List<ServerMessage> messages = new ArrayList<ServerMessage>();
		
		while (in.available() > 0) {
			ServerMessage message = null;
			long messageArrivalTime = in.readLong();
			int messageLength = in.readInt();
			byte messageType = in.readByte();

			byte[] messageBytes = new byte[messageLength - 1];
			int k = 0, read = 0;
			while (read < messageBytes.length) {
				try {
					k = in.read(messageBytes, read, messageBytes.length - read);
					if (k == -1) {
						continue;
					}
					if (k == 0) {
						System.err.println("No data read...");
						continue;
					}
					read += k;
				} catch (SocketTimeoutException ste) {
					continue;
				} catch (IOException ioe) {
					break;
				}
			}
			
			switch (messageType) {
			case ActiveLocalizationResultMessage.MESSAGE_TYPE:
				message = new ActiveLocalizationResultMessage();
				message.setMessageLength(messageLength);
				message.setMessageType(messageType);
				message.setMessageArrivalTime(messageArrivalTime);
				if(!message.decodeSpecificMessage(messageBytes))
				{
					log.error("Error occurred while decoding ActiveLocalizationResultMessage.");
					return messages;
				}
				break;
			case ConsoleMessage.MESSAGE_TYPE:
				message = new ConsoleMessage();
				message.setMessageLength(messageLength);
				message.setMessageType(messageType);
				message.setMessageArrivalTime(messageArrivalTime);
				if(!message.decodeSpecificMessage(messageBytes))
				{
					log.error("Error occurred while decoding ConsoleMessage.");
					return messages;
				}
				break;
			case GZIPFingerprintMessage.MESSAGE_TYPE_GZIP_MEAN:
			case GZIPFingerprintMessage.MESSAGE_TYPE_GZIP_VARIANCE:
				message = new GZIPFingerprintMessage();
				if(messageType == GZIPFingerprintMessage.MESSAGE_TYPE_GZIP_MEAN)
				{
					message.setMessageType(GZIPFingerprintMessage.MESSAGE_TYPE_MEAN);
				}
				else
				{
					message.setMessageType(GZIPFingerprintMessage.MESSAGE_TYPE_VARIANCE);
				}
				message.setMessageLength(messageLength);
				message.setMessageArrivalTime(messageArrivalTime);
				if(!message.decodeSpecificMessage(messageBytes))
				{
					log.error("Error occurred while decoding GZIPFingerprintMessage.");
					return messages;
				}
				break;
			case GZIPFullInfoMessage.MESSAGE_TYPE:
				message = new GZIPFullInfoMessage();
				message.setMessageLength(messageLength);
				message.setMessageType(messageType);
				message.setMessageArrivalTime(messageArrivalTime);
				if(!message.decodeSpecificMessage(messageBytes)){
					log.error("Error occurred while decoding GZIPFullInfoMessage.");
					return messages;
				}
				break;
			case GZIPStatisticsMessage.MESSAGE_TYPE:
				message = new GZIPStatisticsMessage();
				message.setMessageLength(messageLength);
				message.setMessageType(messageType);
				message.setMessageArrivalTime(messageArrivalTime);
				if(!message.decodeSpecificMessage(messageBytes)){
					log.error("Error occurred while decoding GZIPStatisticsMessage.");
					return messages;
				}
				break;
			case HubConnectionMessage.MESSAGE_TYPE:
				message = new HubConnectionMessage();
				message.setMessageLength(messageLength);
				message.setMessageType(messageType);
				message.setMessageArrivalTime(messageArrivalTime);
				if(!message.decodeSpecificMessage(messageBytes))
				{
					log.error("Error occurred while decoding HubConnectionMessage.");
					return messages;
				}
				break;
			default:
				log.error("Unhandled message type {}.", messageType);

				break;
			}
			if(message != null)
			{
				messages.add(message);
			}
		}
		
		return messages;
	}

	@Override
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("Arrival: ").append(String.format("%,d",this.messageArrivalTime)).append("ms");
		return sb.toString();
	}
	
	protected abstract boolean decodeSpecificMessage(byte[] message) throws IOException;
	
}
