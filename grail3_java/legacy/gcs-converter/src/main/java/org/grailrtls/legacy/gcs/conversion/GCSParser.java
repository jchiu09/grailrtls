package org.grailrtls.legacy.gcs.conversion;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.grailrtls.legacy.gcs.conversion.message.ActiveLocalizationResultMessage;
import org.grailrtls.legacy.gcs.conversion.message.ConsoleMessage;
import org.grailrtls.legacy.gcs.conversion.message.GZIPFingerprintMessage;
import org.grailrtls.legacy.gcs.conversion.message.GZIPFullInfoMessage;
import org.grailrtls.legacy.gcs.conversion.message.GZIPStatisticsMessage;
import org.grailrtls.legacy.gcs.conversion.message.HubConnectionMessage;
import org.grailrtls.legacy.gcs.conversion.message.ServerMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GCSParser {

	private static final Logger log = LoggerFactory.getLogger(GCSParser.class);

	public static ServerMessage parseMessage(byte[] rawByteStream) {
		return null;
	}

	public static List<ServerMessage> parseGCSStream(DataInputStream in)
			throws IOException {
		return ServerMessage.decodeMessages(in);
	}
	
	public static List<ServerMessage> parseGCSStream(byte[] streamBytes) throws IOException
	{
		DataInputStream in = new DataInputStream(new ByteArrayInputStream(
				streamBytes));
		return GCSParser.parseGCSStream(in);
	}
	
	public static List<ServerMessage> parseGCSStream(File gcsFile) throws IOException
	{
		DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(gcsFile)));
		return GCSParser.parseGCSStream(in);
	}
}
