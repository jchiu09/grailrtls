package org.grailrtls.legacy.gcs.conversion.message;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;

public class ConsoleMessage extends ServerMessage {

	public static final byte MESSAGE_TYPE = 1;
	
	private String consoleMessage;

	public String getConsoleMessage() {
		return consoleMessage;
	}

	public void setConsoleMessage(String consoleMessage) {
		this.consoleMessage = consoleMessage;
	}

	@Override
	protected boolean decodeSpecificMessage(byte[] messageBytes)
			throws IOException {
		DataInputStream in = new DataInputStream(new ByteArrayInputStream(messageBytes));
		
		byte[] consoleBytes = new byte[super.getMessageLength()- 1];
		int k = 0, read = 0;
		while (read < messageBytes.length) {
			try {
				k = in.read(consoleBytes, read, consoleBytes.length - read);
				if (k == -1) {
					continue;
				}
				if (k == 0) {
					System.err.println("No data read...");
					continue;
				}
				read += k;
			} catch (SocketTimeoutException ste) {
				continue;
			} catch (IOException ioe) {
				break;
			}
		}
		
		this.setConsoleMessage(new String(messageBytes,Charset.forName("ASCII")));
		return true;
	}

	@Override
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(super.toString()).append(" | Console \"").append(this.getConsoleMessage().trim()).append("\".");
		return sb.toString();
	}
	
}
