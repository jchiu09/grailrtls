package org.grailrtls.legacy.gcs.conversion.message;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.grailrtls.legacy.gcs.conversion.message.GZIPFingerprintMessage.Fingerprint.Landmark;

public class GZIPFingerprintMessage extends GZIPMessage {

	public static final byte MESSAGE_TYPE_GZIP_MEAN = 8;
	public static final byte MESSAGE_TYPE_GZIP_VARIANCE = 9;

	public static final byte MESSAGE_TYPE_MEAN = 3;
	public static final byte MESSAGE_TYPE_VARIANCE = 6;

	private List<Fingerprint> fingerprints;
	
	public List<Fingerprint> getFingerprints() {
		return fingerprints;
	}

	public void setFingerprints(List<Fingerprint> fingerprints) {
		this.fingerprints = fingerprints;
	}

	public static class Fingerprint {
		private int regionId;

		private long deviceId;

		private int physicalLayer;

		private long timestamp;

		private int numLandmarks;

		private List<Landmark> landmarks;

		public static class Landmark {
			private long hubId;
			private int physicalLayer;
			private int antennaNumber;
			private float rssi;

			public long getHubId() {
				return hubId;
			}

			public void setHubId(long hubId) {
				this.hubId = hubId;
			}

			public int getPhysicalLayer() {
				return physicalLayer;
			}

			public void setPhysicalLayer(int physicalLayer) {
				this.physicalLayer = physicalLayer;
			}

			public int getAntennaNumber() {
				return antennaNumber;
			}

			public void setAntennaNumber(int antennaNumber) {
				this.antennaNumber = antennaNumber;
			}

			public float getRssi() {
				return rssi;
			}

			public void setRssi(float rssi) {
				this.rssi = rssi;
			}
		}

		public int getRegionId() {
			return regionId;
		}

		public void setRegionId(int regionId) {
			this.regionId = regionId;
		}

		public long getDeviceId() {
			return deviceId;
		}

		public void setDeviceId(long deviceId) {
			this.deviceId = deviceId;
		}

		public int getPhysicalLayer() {
			return physicalLayer;
		}

		public void setPhysicalLayer(int physicalLayer) {
			this.physicalLayer = physicalLayer;
		}

		public long getTimestamp() {
			return timestamp;
		}

		public void setTimestamp(long timestamp) {
			this.timestamp = timestamp;
		}

		public int getNumLandmarks() {
			return numLandmarks;
		}

		public void setNumLandmarks(int numLandmarks) {
			this.numLandmarks = numLandmarks;
		}

		public List<Landmark> getLandmarks() {
			return landmarks;
		}

		public void setLandmarks(List<Landmark> landmarks) {
			this.landmarks = landmarks;
		}
	}

	@Override
	protected boolean decodeSpecificMessage(byte[] message) throws IOException {

		super.setZippedMessage(message);
		byte[] messageBytes = super.decompressData();

		DataInputStream in = new DataInputStream(new ByteArrayInputStream(
				messageBytes));
		
		this.fingerprints = new ArrayList<Fingerprint>();
		
		while (in.available() > 0) {
			Fingerprint fingerprint = new Fingerprint();
			fingerprint.regionId = in.readInt();
			fingerprint.deviceId = in.readLong();
			fingerprint.physicalLayer = in.readInt();
			fingerprint.timestamp = in.readLong();
			fingerprint.numLandmarks = in.readInt();
			fingerprint.landmarks = new ArrayList<Landmark>();
			for (int landmarkIndex = 0; landmarkIndex < fingerprint.numLandmarks; ++landmarkIndex) {
				Landmark landmark = new Landmark();

				landmark.setHubId(in.readLong());
				landmark.setPhysicalLayer(in.readInt());
				landmark.setAntennaNumber(in.readInt());
				landmark.setRssi(in.readFloat());

				fingerprint.landmarks.add(landmark);
			}
			this.fingerprints.add(fingerprint);
		}

		return true;
	}
	
	@Override
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		
		sb.append(super.toString()).append(" | Fingerprint ");
		if(this.getMessageType() == MESSAGE_TYPE_GZIP_MEAN || this.getMessageType() == MESSAGE_TYPE_MEAN)
		{
			sb.append("(Mean)\n");
		}
		else
		{
			sb.append("(StdDev)\n");
		}
		if(this.getFingerprints() != null)
		{
			for(Fingerprint fingerprint : this.getFingerprints())
			{
				sb.append("  RegId: ").append(fingerprint.getRegionId()).append(" DevId|Phy: ").append(fingerprint.getDeviceId()).append('|').append(fingerprint.getPhysicalLayer());
				sb.append(" TS: " ).append(fingerprint.getTimestamp()).append('\n');
				if(fingerprint.getLandmarks() != null)
				{
					int newlineCount = 0;
					sb.append("    ");
					for(Landmark landmark : fingerprint.getLandmarks())
					{
						sb.append("Hub|Phy|Ant: ").append(landmark.getHubId()).append('|').append(landmark.getPhysicalLayer()).append('|').append(landmark.getAntennaNumber())
						  .append(" RSSI: ").append(String.format("%04.2f",landmark.getRssi())).append(", ");
						if(++newlineCount >= 4)
						{
							sb.append("\n    ");
							newlineCount = 0;
						}
					}
					if(newlineCount != 0)
					{
						sb.append('\n');
					}
				}
				
			}
		}
		return sb.toString();
	}

}
