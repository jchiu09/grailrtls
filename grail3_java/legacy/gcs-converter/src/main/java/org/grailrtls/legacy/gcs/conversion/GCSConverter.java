package org.grailrtls.legacy.gcs.conversion;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.grailrtls.legacy.gcs.conversion.message.ServerMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GCSConverter {

	private static final Logger log = LoggerFactory
			.getLogger(GCSConverter.class);
	
	private static final String APPLICATION_NAME = "GRAIL RTLS Legacy Client Stream Converter";

	public static void main(String[] args) {
		if (args.length < 1) {
			printHelp();
			return;
		}
		
		log.info(APPLICATION_NAME + " is starting up.");
		log.debug("Loading input and output files.");
		String inputFileName = args[0];
		String outputFileName = null;
		if (args.length == 2) {
			outputFileName = args[1];
		} else {
			int extensionIndex = inputFileName.lastIndexOf('.');
			if (extensionIndex == -1) {
				log.warn("File extension not found for \"{}\".", inputFileName);
				outputFileName = inputFileName + ".csv";
			} else {
				outputFileName = inputFileName.substring(0, extensionIndex)+".csv";
			}
		}
		log.info("Input: \"{}\"",inputFileName);
		log.info("Output: \"{}\".", outputFileName);

		File inputFile = new File(inputFileName);
		File outputFile = new File(outputFileName);

		log.info("Performing file checks on \"{}\".",inputFile);
		
		if (!performInputFileChecks(inputFile)) {
			log.error("Problems with input file prevent conversion.");
			return;
		}
		log.info("Performing file checks on \"{}\".",outputFile);
		
//		if (!performOutputFileChecks(outputFile)) {
//			log.error("Problems with output file prevent conversion.");
//		}
		FileInputStream inFileStream = null;
		FileOutputStream outFileStream = null;
		
		try {
			inFileStream = new FileInputStream(inputFile);
		}
		catch(FileNotFoundException fnfe)
		{
			log.error("Could not find input file \"{}\".",inputFile.getName());
			return;
		}
		
//		try {
//			outFileStream = new FileOutputStream(outputFile);
//		}
//		catch(FileNotFoundException fnfe)
//		{
//			log.error("Could not find output file \"{}\".",outputFile.getName());
//			return;
//		}
		
		DataInputStream inputStream = new DataInputStream(new BufferedInputStream(inFileStream));
		
		
		List<ServerMessage> messages = null;
		
		try {
			long startTime = System.currentTimeMillis();
			messages = GCSParser.parseGCSStream(inputStream);
			long duration = System.currentTimeMillis() - startTime;
			log.info(String.format("Parsed %,d messages (%,d bytes) in %,dms.",messages.size(),inputFile.length(),duration));
			for(ServerMessage message : messages)
			{
				log.debug("{}",message);
			}
		}
		catch(IOException ioe)
		{
			log.error("Caught IOException while parsing GCS data.",ioe);
			return;
		}
		
//		OutputStreamWriter outputStream = null;
//		try {
//			outputStream = new OutputStreamWriter(outFileStream, "ASCII");
//		}
//		catch(UnsupportedEncodingException uee)
//		{
//			log.error("Unable to create output stream for \"ASCII\" encoding.",uee);
//			return;
//		}
		
		
	}

	private static boolean performInputFileChecks(File inputFile) {
		
		if (!inputFile.exists()) {
			try {
				log.error("File {} does not exist.", inputFile.getCanonicalPath());
			} catch (IOException e) {
				log.error("Unable to retrieve canonical path for {}.", inputFile
						.getName());
			}
			return false;
		}
		
		if(!inputFile.isFile())
		{
			try {
				log.error("{} is not a file.", inputFile.getCanonicalPath());
			}
			catch(IOException ioe)
			{
				log.error("Unable to retrieve canonical path for {}.",inputFile.getName());
			}
			return false;
		}
		
		if(!performFileChecks(inputFile))
		{
			return false;
		}
		
		if(!inputFile.canRead())
		{
			try {
				log.error("Unable to open input file \"{}\" for reading.",inputFile.getCanonicalPath());
			}
			catch(IOException ioe)
			{
				log.error("Unable to retrieve canonical path for {}.",inputFile.getName());
			}
			return false;
		}
		
		if(inputFile.length() <= 0)
		{
			try {
				log.error("{} is empty or has no contents.",inputFile.getCanonicalPath());
			}
			catch(IOException ioe)
			{
				log.error("Unable to retrieve canonical path for {}.",inputFile.getName());
			}
			return false;
		}
		
		return true;
	}

	private static boolean performOutputFileChecks(File outputFile) {
		
		if (outputFile.exists()) {
			try {
				log.error("File {} already exists.", outputFile.getCanonicalPath());
			} catch (IOException e) {
				log.error("Unable to retrieve canonical path for {}.", outputFile
						.getName());
			}
			return false;
		}
		
		if(!performFileChecks(outputFile))
		{
			return false;
		}
		
		try {
		if(!outputFile.createNewFile())
		{
			try {
				log.error("Unable to create new output file \"{}\".",outputFile.getCanonicalPath());
			}
			catch(IOException ioe)
			{
				log.error("Unable to retrieve canonical path for {}.",outputFile.getName());
			}
			return false;
		}
		}
		catch(IOException ioe)
		{
			log.error("IOException while creating file.",ioe);
			return false;
		}
		
		if(!outputFile.canWrite())
		{
			try {
				log.error("Unable to open output file \"{}\" for writing.",outputFile.getCanonicalPath());
			}
			catch(IOException ioe)
			{
				log.error("Unable to retrieve canonical path for {}.",outputFile.getName());
			}
			return false;
		}
		
		return true;
	}

	private static boolean performFileChecks(File file) {
		

		return true;
	}

	private static void printHelp() {
		System.out.println(APPLICATION_NAME);
		System.out
				.println("\tUsage: java -jar JAR_FILE INPUT_FILE.gcs [OUTPUT_FILE.csv]");
	}
}
