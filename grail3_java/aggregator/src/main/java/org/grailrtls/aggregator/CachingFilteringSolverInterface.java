/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.aggregator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.grailrtls.libcommon.util.HashableByteArray;
import org.grailrtls.libcommon.util.LRUCache;
import org.grailrtls.libcommon.util.NumericUtils;
import org.grailrtls.libsolver.protocol.messages.SampleMessage;
import org.grailrtls.libsolver.protocol.messages.Transmitter;
import org.grailrtls.libsolver.rules.DeviceIdHashEntry;
import org.grailrtls.libsolver.rules.SubscriptionRequestRule;
import org.grailrtls.libsolver.rules.SubscriptionRuleFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CachingFilteringSolverInterface extends SolverInterface {

	private static final Logger log = LoggerFactory
			.getLogger(CachingFilteringSolverInterface.class);

	private static final int MAX_DEVICES = 200;

	ConcurrentLinkedQueue<SubscriptionRequestRule> effectiveRules = new ConcurrentLinkedQueue<SubscriptionRequestRule>();

	Map<HashableByteArray, DeviceIdHashEntry> ruleCache = Collections
			.synchronizedMap(new LRUCache<HashableByteArray, DeviceIdHashEntry>(
					MAX_DEVICES));

	protected boolean hasEffectiveRules = false;
	
	protected volatile boolean reportedDrop = false;

	@Override
	public boolean sendSample(SampleMessage sampleMessage) {

		if (this.session.getScheduledWriteMessages() > SolverInterface.MAX_OUTSTANDING_SAMPLES) {
			if(!this.reportedDrop){
			this.reportedDrop = true;
			log.warn("Dropping a sample for {}",this);
			}
			return false;
		}
		this.reportedDrop = false;

		if (!this.sentSubscriptionResponse) {
			return false;
		}

		if (!this.hasEffectiveRules) {
			return super.sendSample(sampleMessage);
		}
		log.debug("Checking rules for {}.", sampleMessage);

		HashableByteArray deviceHasher = new HashableByteArray(
				sampleMessage.getDeviceId());

		DeviceIdHashEntry cacheResult = this.ruleCache.get(deviceHasher);

		if (cacheResult == null) {
			SubscriptionRequestRule passedRule = null;
			for (SubscriptionRequestRule rule : this.effectiveRules) {
				log.debug("Checking rule {}.", rule);
				if (SubscriptionRuleFilter.applyRule(rule, sampleMessage)) {
					passedRule = rule;
					break;
				}
			}

			if (passedRule != null) {
				log.debug("{} passed all rules.", sampleMessage);
				DeviceIdHashEntry hashEntry = new DeviceIdHashEntry();
				hashEntry.setPassedRules(true);
				hashEntry.setUpdateInterval(passedRule.getUpdateInterval());
				hashEntry.setNextPermittedTransmit(
						sampleMessage.getReceiverId(),
						System.currentTimeMillis()
								+ passedRule.getUpdateInterval());
				this.ruleCache.put(deviceHasher, hashEntry);
				return super.sendSample(sampleMessage);
			}
			DeviceIdHashEntry hashEntry = new DeviceIdHashEntry();
			hashEntry.setPassedRules(false);
			this.ruleCache.put(deviceHasher, hashEntry);
			return false;
		}
		if (cacheResult.isPassedRules()) {
			log.debug("{} passed all rules (cache).", sampleMessage);
			long now = System.currentTimeMillis();
			long nextTransmit = cacheResult
					.getNextPermittedTransmit(sampleMessage.getReceiverId());
			if (nextTransmit <= now) {
				cacheResult.setNextPermittedTransmit(
						sampleMessage.getReceiverId(),
						now + cacheResult.getUpdateInterval());
				return super.sendSample(sampleMessage);
			}
			log.debug("Not able to send sample yet.");
			return false;
		}

		// log.debug("{} failed one or more rules (cache).", sampleMessage);
		return false;
	}

	public Collection<SubscriptionRequestRule> getEffectiveRules() {
		return effectiveRules;
	}

	/**
	 * A convenience method for adding a single rule to this Solver.
	 * 
	 * @param rule
	 */
	public void addEffectiveRule(SubscriptionRequestRule newRule) {

		this.effectiveRules.add(newRule);

		for (SubscriptionRequestRule rule : this.effectiveRules) {
			if (rule.getNumTransmitters() > 0 || rule.getPhysicalLayer() != 0
					|| rule.getUpdateInterval() > 0) {
				this.hasEffectiveRules = true;
				break;
			}
		}
		log.info("Added {} to {}.", newRule, this);
	}

	public void clearEffectiveRules() {
		this.effectiveRules.clear();
		this.hasEffectiveRules = false;
		log.debug("Cleared effective rules.");
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("CachingFilteringSolver @").append(
				this.session.getRemoteAddress());
		return sb.toString();
	}
}
