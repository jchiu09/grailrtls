/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.solver.weather;

import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.grailrtls.libcommon.util.FieldDecoder;
import org.grailrtls.libworldmodel.client.ClientWorldModelInterface;
import org.grailrtls.libworldmodel.client.listeners.DataListener;
import org.grailrtls.libworldmodel.client.protocol.messages.AbstractRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.Attribute;
import org.grailrtls.libworldmodel.client.protocol.messages.AttributeAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.DataResponseMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginPreferenceMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.SnapshotRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.URISearchResponseMessage;
import org.grailrtls.libworldmodel.solver.SolverWorldModelInterface;
import org.grailrtls.libworldmodel.solver.listeners.ConnectionListener;
import org.grailrtls.libworldmodel.solver.protocol.messages.DataTransferMessage.Solution;
import org.grailrtls.libworldmodel.solver.protocol.messages.StartTransientMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.StopTransientMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage.TypeSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A GRAIL RTLS solver that pulls weather information from Yahoo! Weather RSS
 * feed.
 * 
 * This code is based on the example code provided by Sonatype at <a href=
 * "http://www.sonatype.com/books/mvnex-book/reference/customizing-sect-simple-weather-source.html"
 * >http://www.sonatype.com/books/mvnex-book/reference/customizing-sect-simple-
 * weather-source.html</a>.
 * 
 * @author Robert Moore
 */
public class WeatherSolver extends Thread implements DataListener,
		org.grailrtls.libworldmodel.solver.listeners.DataListener,
		ConnectionListener,
		org.grailrtls.libworldmodel.client.listeners.ConnectionListener {

	/**
	 * Logging facility.
	 */
	private static final Logger log = LoggerFactory
			.getLogger(WeatherSolver.class);

	/**
	 * The solution URI for temperature information.
	 */
	public static final String SOLUTION_URI_TEMPERATURE = "weather.temperature";

	/**
	 * The solution URI for weather condition information.
	 */
	public static final String SOLUTION_URI_CONDITION = "weather.condition";

	/**
	 * The solution URI for humidity information.
	 */
	public static final String SOLUTION_URI_HUMIDITY = "weather.humidity";

	/**
	 * The solution URI for wind chill temperatures.
	 */
	public static final String SOLUTION_URI_WINDCHILL = "weather.windchill";

	/**
	 * Identifying origin string for this particular solver.
	 */
	public static final String SOLVER_ORIGIN_STRING = "grail.weather_solver.zip_code";

	/**
	 * List of regions to request from the World Server.
	 */
	protected Collection<String> regionList = new ConcurrentLinkedQueue<String>();

	/**
	 * Maps region names to ZIP codes. As a result, only works in the U.S. for
	 * now.
	 */
	protected final Map<String, String> regionToZip = new ConcurrentHashMap<String, String>();

	/**
	 * Interface to the World Model to request ZIP codes.
	 */
	protected final ClientWorldModelInterface clientWMI = new ClientWorldModelInterface();

	/**
	 * Interface to the World Model to publish weather solutions.
	 */
	protected final SolverWorldModelInterface solverWMI = new SolverWorldModelInterface();

	/**
	 * Timer to take care of weather updates.
	 */
	protected final Timer updateTimer = new Timer();

	/**
	 * Weather service interface to retrieve weather results.
	 */
	protected final WeatherServiceInterface weatherService = new YahooWeatherService();

	/**
	 * Flag indicating whether the update task has been scheduled or not.
	 */
	protected boolean startedUpdateTask = false;

	/**
	 * Flag indicating whether we can send updates to the distributor.
	 */
	protected boolean canSendSolutions = false;

	/**
	 * Parse command-line parameters and start the solver.
	 * 
	 * @param args
	 *            distributor host, distributor port, world server host, world
	 *            server port, region names
	 */
	public static void main(String[] args) {
		if (args.length < 4) {
			printUsageInfo();
			return;
		}

		int distributorPort = Integer.parseInt(args[1]);
		int worldServerPort = Integer.parseInt(args[3]);

		// Set up the world server
		WeatherSolver ws = new WeatherSolver(args[0], distributorPort, args[2],
				worldServerPort);

		if (args.length > 4) {
			for (int i = 4; i < args.length; ++i) {
				ws.addRegionName(args[i]);
			}
		}

		// Start it up.
		ws.start();
	}

	/**
	 * Prints a simple help message about how to use the solver.
	 */
	protected static void printUsageInfo() {
		StringBuffer sb = new StringBuffer();

		sb.append("Requires four parameters:\n\t")
				.append("<World Model Solver host> <World Model Solver port> <World Model Client host> <World Model Client port> <Region name list>.");

		System.err.println(sb.toString());
	}

	/**
	 * Creates a new WeatherSolver and initializes the connection information
	 * for the Distributor and World Server.
	 * 
	 * @param distHost
	 *            the hostname/IP address for the distributor.
	 * @param distPort
	 *            the port on which the Distributor is listening for solver
	 *            connections.
	 * @param worldHost
	 *            the hostname/IP address for the world server.
	 * @param worldPort
	 *            the port on which the World Server is listening for
	 *            connections.
	 */
	public WeatherSolver(final String distHost, final int distPort,
			final String worldHost, final int worldPort) {
		// Take care of configuration/intitialization here.
		this.clientWMI.setHost(worldHost);
		this.clientWMI.setPort(worldPort);
		this.clientWMI.addConnectionListener(this);
		this.clientWMI.addDataListener(this);

		this.solverWMI.setHost(distHost);
		this.solverWMI.setPort(distPort);
		this.solverWMI.setOriginString(SOLVER_ORIGIN_STRING);
		this.solverWMI.addConnectionListener(this);

		// Add the solution types to the distributor interface
		TypeSpecification[] specs = new TypeSpecification[4];
		specs[0] = new TypeSpecification();
		specs[0].setIsTransient(false);
		specs[0].setUriName(SOLUTION_URI_TEMPERATURE);

		specs[1] = new TypeSpecification();
		specs[1].setIsTransient(false);
		specs[1].setUriName(SOLUTION_URI_CONDITION);

		specs[2] = new TypeSpecification();
		specs[2].setIsTransient(false);
		specs[2].setUriName(SOLUTION_URI_WINDCHILL);

		specs[3] = new TypeSpecification();
		specs[3].setIsTransient(false);
		specs[3].setUriName(SOLUTION_URI_HUMIDITY);

		for (TypeSpecification spec : specs) {
			this.solverWMI.addType(spec);
		}

	}

	/**
	 * Contacts the world server to get ZIP codes for regions, then starts the
	 * update timer to retrieve weather results and publishes them to the
	 * distributor.
	 */
	@Override
	public void run() {

		this.solverWMI.setConnectionRetryDelay(30000);
		this.solverWMI.setConnectionTimeout(10000);
		this.solverWMI.setDisconnectOnException(true);
		this.solverWMI.setStayConnected(true);

		this.clientWMI.setConnectionRetryDelay(30000);
		this.clientWMI.setConnectionTimeout(10000);
		this.clientWMI.setDisconnectOnException(true);
		this.solverWMI.setStayConnected(true);

		this.startConnections();

	}

	/**
	 * Starts the interfaces for the Distributor and World Server. Exits the
	 * application if either interface cannot connect.
	 */
	protected void startConnections() {
		if (!this.solverWMI.doConnectionSetup()) {
			log.error(
					"Could not establish connection to the distributor at {}:{}.",
					this.solverWMI.getHost(),
					Integer.valueOf(this.solverWMI.getPort()));
			// FIXME: Exit in a more graceful way.
			System.exit(1);
		}
		if (!this.clientWMI.doConnectionSetup()) {
			log.error(
					"Could not establish connection to the world server at {}:{}.",
					this.clientWMI.getHost(),
					Integer.valueOf(this.clientWMI.getPort()));
			System.exit(1);
		}
	}

	/**
	 * Starts the update timer to request weather updates from the web sevice.
	 */
	protected void startUpdateTask() {
		// Set the update task to run every 15 minutes after 10 seconds
		this.updateTimer.schedule(new TimerTask() {

			@Override
			public void run() {
				WeatherSolver.this.performUpdate();
			}
		}, 1000 * 10, 1000 * 60 * 15);
		this.startedUpdateTask = true;
	}

	/**
	 * Actually performs the work of contacting the weather service and pushing
	 * the results to the distributor.
	 */
	protected void performUpdate() {
		if (!this.canSendSolutions) {
			log.info("Not yet able to send solutions to the distributor.");
			return;
		}

		log.info("Performing weather scrape.");

		// List of weather objects that were retrieved.
		LinkedList<WeatherInfo> retrievedWeather = new LinkedList<WeatherInfo>();
		WeatherInfo weather = null;

		// Get the weather for the appropriate regions/zip codes.
		for (String regionName : this.regionToZip.keySet()) {
			String zipCode = this.regionToZip.get(regionName);
			log.info("Querying for {}->{}", regionName, zipCode);
			weather = this.weatherService.getCurrentWeather(zipCode);
			if (weather != null) {
				weather.setGrailRegion(regionName);
				retrievedWeather.add(weather);
			}
		}

		for (WeatherInfo wInfo : retrievedWeather) {
			Collection<Solution> solutions = new LinkedList<Solution>();
			log.info("Generating solutions for {}", wInfo.getGrailRegion());
			Solution solution = null;

			// Current temperature (Fahrenheit)
			if (wInfo.getTemperature() != null
					&& wInfo.getTemperature().length() > 0) {
				solution = new Solution();
				solution.setTime(System.currentTimeMillis());
				solution.setTargetName(wInfo.getGrailRegion());
				solution.setAttributeName(SOLUTION_URI_TEMPERATURE);
				try {
					solution.setData(wInfo.getTemperature()
							.getBytes("UTF-16BE"));
				} catch (UnsupportedEncodingException e) {
					log.error("Unable to encode {} as UTF-16BE bytes.",
							wInfo.getTemperature(), e);
					continue;
				}
				solutions.add(solution);
			}

			// Current condition (sunny, cloudy, fog, rain, etc.)
			if (wInfo.getCondition() != null
					&& wInfo.getCondition().length() > 0) {
				solution = new Solution();
				solution.setTime(System.currentTimeMillis());
				solution.setTargetName(wInfo.getGrailRegion());
				solution.setAttributeName(SOLUTION_URI_CONDITION);
				try {
					solution.setData(wInfo.getCondition().getBytes("UTF-16BE"));
				} catch (UnsupportedEncodingException e) {
					log.error("Unable to encode {} as UTF-16BE bytes.",
							wInfo.getCondition(), e);
					continue;
				}
				solutions.add(solution);
			}

			// Current humidity (e.g., 44%, 100%)
			if (wInfo.getHumidity() != null && wInfo.getHumidity().length() > 0) {
				solution = new Solution();
				solution.setTime(System.currentTimeMillis());
				solution.setTargetName(wInfo.getGrailRegion());
				solution.setAttributeName(SOLUTION_URI_HUMIDITY);
				try {
					solution.setData(wInfo.getHumidity().getBytes("UTF-16BE"));
				} catch (UnsupportedEncodingException e) {
					log.error("Unable to encode {} as UTF-16BE bytes.",
							wInfo.getHumidity(), e);
					continue;
				}
				solutions.add(solution);
			}

			// Current perceived temperature (Fahrenheit)
			if (wInfo.getWindChill() != null
					&& wInfo.getWindChill().length() > 0) {
				solution = new Solution();
				solution.setTime(System.currentTimeMillis());
				solution.setTargetName(wInfo.getGrailRegion());
				solution.setAttributeName(SOLUTION_URI_WINDCHILL);
				try {
					solution.setData(wInfo.getWindChill().getBytes("UTF-16BE"));
				} catch (UnsupportedEncodingException e) {
					log.error("Unable to encode {} as UTF-16BE bytes.",
							wInfo.getWindChill(), e);
					continue;
				}
				solutions.add(solution);
			}

			// Only send if at least one solution was generated
			if (!solutions.isEmpty()) {
				this.solverWMI.sendSolutions(solutions);
			}
		}
	}

	/**
	 * Adds a region to the list of requested regions for this solver.
	 * 
	 * @param regionName
	 *            the region name to request from the World Server.
	 */
	public void addRegionName(String regionName) {
		this.regionList.add(regionName);
	}

	@Override
	public void connectionInterrupted(ClientWorldModelInterface worldModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connectionEnded(ClientWorldModelInterface worldModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connectionEstablished(ClientWorldModelInterface worldModel) {

	}

	/**
	 * Disables scraping the weather service.
	 */
	@Override
	public void connectionInterrupted(SolverWorldModelInterface worldModel) {
		this.canSendSolutions = false;
	}

	/**
	 * Disables scraping the weather service.
	 */
	@Override
	public void connectionEnded(SolverWorldModelInterface worldModel) {
		this.canSendSolutions = false;
	}

	@Override
	public void connectionEstablished(SolverWorldModelInterface worldModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void requestCompleted(ClientWorldModelInterface worldModel,
			AbstractRequestMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dataResponseReceived(ClientWorldModelInterface worldModel,
			DataResponseMessage message) {
		for (Attribute field : message.getAttributes()) {
			if (field.getAttributeName().equalsIgnoreCase("zip code")) {
				// Assuming that the "zip code" field is stored as a String
				this.regionToZip.put(
						message.getUri(),
						(String) FieldDecoder.decodeField("string",
								field.getData()));
				break;
			}
		}

		// Only start the scrape timer task if we have something to request...
		if (!this.startedUpdateTask) {
			if (this.regionToZip.size() > 0) {
				this.startUpdateTask();
			}
		}
	}

	@Override
	public void uriSearchResponseReceived(ClientWorldModelInterface worldModel,
			URISearchResponseMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void attributeAliasesReceived(
			ClientWorldModelInterface clientWorldModelInterface,
			AttributeAliasMessage message) {
		// Send the first round of requests in 500ms, sending again every hour
		// to check for updates.
		this.updateTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				for (String regionName : WeatherSolver.this.regionList) {
					SnapshotRequestMessage request = new SnapshotRequestMessage();
					long now = System.currentTimeMillis();
					request.setBeginTimestamp(now - 1000 * 60 * 60);
					request.setEndTimestamp(now);
					request.setQueryURI(regionName);
					request.setQueryAttributes(new String[] { ".*" });

					WeatherSolver.this.clientWMI.sendMessage(request);
				}
			}
		}, 500, 1000 * 60 * 60);
	}

	@Override
	public void originAliasesReceived(
			ClientWorldModelInterface clientWorldModelInterface,
			OriginAliasMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void startTransientReceived(SolverWorldModelInterface worldModel,
			StartTransientMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void stopTransientReceived(SolverWorldModelInterface worldModel,
			StopTransientMessage message) {
		// TODO Auto-generated method stub

	}

	/**
	 * Enables scraping the weather service.
	 */
	@Override
	public void typeSpecificationsSent(SolverWorldModelInterface worldModel,
			TypeAnnounceMessage message) {
		this.canSendSolutions = true;
	}

	@Override
	public void originPreferenceSent(ClientWorldModelInterface worldModel,
			OriginPreferenceMessage message) {
		// TODO Auto-generated method stub

	}
}
