/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.solver.weather;

/**
 * Represents the current weather information for a location.
 * 
 * @author Robert Moore
 *
 */
public class WeatherInfo {
	/**
	 * The name of the GRAIL region this weather information is for.
	 */
	protected String grailRegion;
	
	/**
	 * Returns the name of the GRAIL region for this weather information.
	 * @return the name of the GRAIL region for this weather information.
	 */
	public String getGrailRegion() {
		return grailRegion;
	}
	
	/**
	 * Sets the GRAIL region name for this weather information.
	 * @param grailRegion the new GRAIL region name for this weather information. 
	 */
	public void setGrailRegion(String grailRegion) {
		this.grailRegion = grailRegion;
	}
	
	/**
	 * The city name that this weather is for.
	 */
	protected String city;
	
	/**
	 * The region that this weather is for.
	 */
	protected String region;
	
	/**
	 * The country that this weather is or.
	 */
	protected String country;
	
	/**
	 * The current condition of the weather (e.g., Fog).
	 */
	protected String condition;
	
	/**
	 * The current temperature of the weather, unitless.
	 */
	protected String temperature;
	
	/**
	 * The current perceived temperature, unitless.
	 */
	protected String windChill;
	
	/**
	 * The current humidity percentage, with %.
	 */
	protected String humidity;
	
	/**
	 * Retrieves the city for this weather information.
	 * @return the city for this weather information.
	 */
	public String getCity() {
		return city;
	}
	
	/**
	 * Sets the city for this weather information.
	 * @param city
	 */
	public void setCity(String city) {
		this.city = city;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getTemperature() {
		return temperature;
	}
	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}
	public String getWindChill() {
		return windChill;
	}
	public void setWindChill(String windChill) {
		this.windChill = windChill;
	}
	public String getHumidity() {
		return humidity;
	}
	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
}
