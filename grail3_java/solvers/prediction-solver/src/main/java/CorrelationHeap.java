

import java.util.ArrayList;
import java.util.List;

/**
 * Class that implements a binary max heap where each node contains the Solution URI, 
 * Solution target, solution data, and the actual correlation value. The node with the highest 
 * Correlation is at the top of the heap. Allows for easy construction and manipulation of an ordered 
 * representation of the entire correlations collection.
 * 
 * @author Sumedh Sawant
 *
 */
public class CorrelationHeap {
	
	/**
	 * Typical List representation of the heap. Root is at index 1. Children of a node at index "n" are 
	 * at "2n" (left child) and "2n+1" (right child) respectively.
	 */
	List<HeapNode> heap = new ArrayList<HeapNode>();
	
	public CorrelationHeap(){
		this.heap.add(0,null);
	}
	
	public void add(String solutionURI, String solutionTarget, int solutionData, double correlation){
		HeapNode node = new HeapNode(solutionURI, solutionTarget, solutionData, correlation);
		int newIndex = this.heap.size();
		this.heap.add(newIndex, node);
		this.bubbleUp(newIndex);
	}
	
	public void add(HeapNode n){
		this.add(n.solutionURI, n.solutionTarget, n.solutionData, n.correlation);
	}
	
	public HeapNode removeMax(){
		if(this.heap.size() == 1){
			return null;
		}
		HeapNode max = this.heap.get(1);
		HeapNode lastNode = this.heap.get(this.heap.size()-1);
		
		//Remove and replace the root with the rightmost node on the bottom level
		this.heap.remove(this.heap.size()-1);
		
		if(this.heap.size()==1){
			return max;
		}
		
		this.heap.set(1, lastNode);
		
		this.bubbleDown(1);
		
		return max;
	}
	
	public void bubbleDown(int nodeIndex){
		while(true){
			HeapNode node = this.heap.get(nodeIndex);
			HeapNode leftChild = null;
			HeapNode rightChild = null;
			if(!((nodeIndex*2) >= heap.size())){
				leftChild = this.heap.get(nodeIndex*2);
			}
			else{
				//A node can never have a null left child and a non-null right child.
				break;
			}
			
			if(!(((nodeIndex*2) + 1) >= heap.size())){
				rightChild = this.heap.get((nodeIndex*2)+1);
			}
			
			if((rightChild == null) || (leftChild.correlation >= rightChild.correlation)){
				if(leftChild.correlation <= node.correlation){
					break;
				}
				else{
					this.heap.set(nodeIndex, leftChild);
					this.heap.set(nodeIndex*2, node);
					nodeIndex = nodeIndex*2;
				}
			}
			else{
				if(rightChild.correlation <= node.correlation){
					break;
				}
				else{
					this.heap.set(nodeIndex, rightChild);
					this.heap.set((nodeIndex*2) + 1, node);
					nodeIndex = (nodeIndex*2) + 1;
				}
			}
		}
	}
	
	public void bubbleUp(int nodeIndex){
		while(true){
			HeapNode node = this.heap.get(nodeIndex);
			//Node is the root
			if(nodeIndex == 1 || nodeIndex == 0){
				break;
			}
			//Current node is the left child of another node.
			if(nodeIndex%2 == 0){
				HeapNode parent = this.heap.get(nodeIndex/2);
				if(parent.correlation >= node.correlation){
					//Current positioning maintains the heap invariants.
					break;
				}
				else{
					//Switch the current node with its parent.
					heap.set(nodeIndex/2, node);
					heap.set(nodeIndex, parent);
					nodeIndex = nodeIndex/2;
				}
			}
			//Current node is the right child of another node.
			else if(nodeIndex%2 == 1){
				HeapNode parent = this.heap.get((nodeIndex-1)/2);
				if(parent.correlation >= node.correlation){
					//Current positioning maintains the heap invariants.
					break;
				}
				else{
					//Switch the current node with its parent.
					heap.set((nodeIndex-1)/2, node);
					heap.set(nodeIndex, parent);
					nodeIndex = (nodeIndex-1)/2;
				}
			}
		}
	}
	
	class HeapNode{
		private String solutionURI = "";
		private String solutionTarget = "";
		private int solutionData = 0;
		private double correlation = 0.0;
		
		public HeapNode(String solutionURI, String solutionTarget, int solutionData, double correlation){
			this.solutionURI = solutionURI;
			this.solutionTarget = solutionTarget;
			this.solutionData = solutionData;
			this.correlation = correlation;
		}
		
		public String getSolutionURI(){
			return this.solutionURI;
		}
		
		public String getSolutionTarget(){
			return this.solutionTarget;
		}
		
		public int getSolutionData(){
			return this.solutionData;
		}
		
		public double getCorrelationValue(){
			return this.correlation;
		}
	}
	
	
	//For testing purposes only. Should print out the numbers in the array below in order.
	public static void main(String[] args){
		int[] temp = new int[]{1,34,25,46,4,45,7,8,76,656,48,43,56,444,7676};
		
		ArrayList<HeapNode> arr = new ArrayList<HeapNode>();
		
		CorrelationHeap shit = new CorrelationHeap();
		
		for(int i: temp){
			arr.add(shit.new HeapNode("s", "d", 4, i));
		}
		
		for(HeapNode n: arr){
			shit.add(n);
		}
		
		HeapNode a;
		
		while(true){
			a = shit.removeMax();
			if(a != null){
				System.out.println(a.correlation);
			}
			else{
				break;
			}
		}
	}
}
