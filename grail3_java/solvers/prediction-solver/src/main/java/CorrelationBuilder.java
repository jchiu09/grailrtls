

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import org.grailrtls.distributor.client.messages.DataTransferMessage;
import org.grailrtls.distributor.client.messages.DataTransferMessage.Solution;

/**
 * Class that builds Correlations. One instance will be created for every 15 minute interval.
 * 
 * @author Sumedh Sawant
 *
 */
public class CorrelationBuilder {

	/**
	 * Hashtable<[Solution Type, Solution Target Name], [[SolutionData, # Actually Happened for trials], ... ]>
	 */
	private Hashtable<String[], ArrayList<int[]>> correlationRecord;
	
	private static Hashtable<Integer, int[][]> locationTiles;
	
	private Hashtable<String[], Boolean> key_SeenThisTrial;
	
	private Hashtable<ArrayList<int[]>, Boolean> value_SeenThisTrial;
	
	private int trialCount = 1;
	
	public CorrelationBuilder(){
		this.correlationRecord = new Hashtable<String[], ArrayList<int[]>>();
		locationTiles = new Hashtable<Integer, int[][]>();
		this.key_SeenThisTrial = new Hashtable<String[], Boolean>();
		this.value_SeenThisTrial = new Hashtable<ArrayList<int[]>, Boolean>();
		this.fillLocationTiles();
	}
	
	/**
	 * Fills the Hash-table "locationTiles" with all the location tile information.
	 */
	private void fillLocationTiles(){
		locationTiles.put(1, new int[][]{new int[]{0,0}, new int[]{44,48}});
		locationTiles.put(2, new int[][]{new int[]{44,24}, new int[]{87,48}});
		locationTiles.put(3, new int[][]{new int[]{44,0}, new int[]{146,24}});
		locationTiles.put(4, new int[][]{new int[]{87,24}, new int[]{146,48}});
		locationTiles.put(5, new int[][]{new int[]{0,48}, new int[]{44,97}});
		locationTiles.put(6, new int[][]{new int[]{44,48}, new int[]{87,97}});
		locationTiles.put(7, new int[][]{new int[]{87,48}, new int[]{105,60}});
		locationTiles.put(8, new int[][]{new int[]{87,60}, new int[]{105,83}});
		locationTiles.put(9, new int[][]{new int[]{87,83}, new int[]{105,105}});
		locationTiles.put(10, new int[][]{new int[]{105,48}, new int[]{200,105}});
		locationTiles.put(11, new int[][]{new int[]{0,97}, new int[]{87,145}});
		locationTiles.put(12, new int[][]{new int[]{87,105}, new int[]{200,145}});
		locationTiles.put(13, new int[][]{new int[]{0,145}, new int[]{44,160}});
		locationTiles.put(14, new int[][]{new int[]{44,145}, new int[]{149,160}});
		locationTiles.put(15, new int[][]{new int[]{149,145}, new int[]{200,160}} );
		locationTiles.put(16, new int[][]{new int[]{146,0}, new int[]{200,48}} );
	}
	
	/**
	 * Takes solution data regarding a location and converts it to an integer representing the
	 * appropriate tile region.
	 * 
	 * @param byte[] solutionData
	 * @return int Tile
	 */
	private static int convertLocationToTile(byte[] solutionData){
		ByteBuffer bb = ByteBuffer.wrap(solutionData);
		double locationX = Math.round(bb.getDouble());
		double locationY = Math.round(bb.getDouble(8));
		
		Enumeration<Integer> keys = locationTiles.keys();
		while(keys.hasMoreElements()){
			int currentKey = Integer.valueOf(keys.nextElement());
			int[][] coordinatePair = locationTiles.get(currentKey);
			int[] bottomLeft = coordinatePair[0];
			int[] topRight = coordinatePair[1];
			
			//If the location solution is contained inside this tile
			if(((locationX >= bottomLeft[0]) && (locationX <= topRight[0]))&&
					((locationY >= bottomLeft[1]) && (locationY <= topRight[1]))){
				//Return the integer representing the location tile
				return currentKey;
			}
			else{
				continue;
			}
		}
		//Should never get here. This means the location solution received was invalid.
		return 0;
	}
	
	/**
	 * Converts temperature data into numerical integer. 
	 * 
	 * 0 ==> 0 to 9 degrees
	 * 1 ==> 10 to 19 degrees
	 * 2 ==> 20 to -29 degrees
	 * 
	 * etc.
	 * 
	 * @param solutionData
	 * @return int
	 */
	private static int convertTemperatureSolution(byte[] solutionData){
		ByteBuffer bb = ByteBuffer.wrap(solutionData);
		double temperature = Math.round(bb.getShort());
		temperature = Math.floor(temperature);
		return (int) (temperature/10);
	}
	
	private static int convertPassiveMotionSolution(byte[] solutionData){
		// TODO
		return 0;
	}
	
	private static int convertPrinterStatusSolution(byte[] solutionData){
		// TODO
		return 0;
	}

	
	/**
	 * Processes a non-binary solution and creates an integer representation of it by using the appropriate
	 * conversion.
	 * 
	 * @param byte[] solutionData
	 * @param String solutionType
	 * 
	 * @return int
	 */
	private static int processNonBinarySolution(byte[] solutionData, String solutionType){
		if("location".equalsIgnoreCase(solutionType)){
			return convertLocationToTile(solutionData);
		}
		if("temperature".equalsIgnoreCase(solutionType)){
			return convertTemperatureSolution(solutionData);
		}
		if("passive motion.tile".equalsIgnoreCase(solutionType)){
			return convertPassiveMotionSolution(solutionData);
		}
		if("printer status".equalsIgnoreCase(solutionType)){
			return convertPrinterStatusSolution(solutionData);
		}
		
		//Should never get here
		return 0;
	}
	
	
	/**
	 * Returns a boolean indicating whether the solutionData is binary or not.
	 * 
	 * @param byte[] solutionData
	 * @return boolean
	 */
	private static boolean isBinarySolution(byte[] solutionData){
		if((solutionData.length == 1) && ((solutionData[0] == (byte)0) || (solutionData[1] == (byte)1))){
			return true;
		}
		else{
			return false;
		}
	}
	
	public static int convertSolutionDataToInt(byte[] data, String solutionURI){
		if(isBinarySolution(data)){
			return data[0];
		}
		else{
			return processNonBinarySolution(data, solutionURI);
		}
	}
	
	/**
	 * Adds a particular solution to the total correlation record being constructed. 
	 * 
	 * @param DataTransferMessage solutions
	 */
	public void addSolutionDataPacket(DataTransferMessage solutions){
		
		for(Solution solution: solutions.getSolutions()){
			String[] key = new String[]{solution.getTypeUri(), solution.getTargetName()};
			int solutionData = 0;
			if(isBinarySolution(solution.getData())){
				solutionData = solution.getData()[0];
			}
			else{
				solutionData = processNonBinarySolution(solution.getData(), solution.getTypeUri());
				
				//One of the conversion methods above did not work because the solution received was invalid
				if(solutionData == 0){
					return;
				}
			}
			//Correlation Record contains no entry of this particular solution
			if(this.correlationRecord.get(key) == null){
				ArrayList<int[]> value = new ArrayList<int[]>();
				value.add(new int[]{solutionData, 1});
				this.correlationRecord.put(key, value);
				
				//Make sure each solution is incremented only once every trial
				this.key_SeenThisTrial.put(key, false);
				this.value_SeenThisTrial.put(value, false);
			}
			//Correlation Record contains an entry for this particular solution, but must only be updated at 
			//appropriate times.
			else{
				ArrayList<int[]> oldValues = this.correlationRecord.get(key);
				for(int[] arr: oldValues){
					if(arr[0] == solutionData){
						if((this.key_SeenThisTrial.get(key) == null)&&
								(this.value_SeenThisTrial.get(oldValues) == null)){
							arr[1] += 1;
							
							this.key_SeenThisTrial.put(key, false);
							this.value_SeenThisTrial.put(oldValues, false);
						}
						else{
							//Make sure that each solution is incremented only once every trial.
							continue;
						}
					}
					else{
						continue;
					}
				}
			}
		}
	}
	
	public Hashtable<String[], ArrayList<int[]>> getCorrelationRecord(){
		return this.correlationRecord;
	}
	
	public int getTrialCount(){
		return this.trialCount;
	}
	
	public void increaseTrialCount(){
		this.trialCount++;
		this.key_SeenThisTrial = new Hashtable<String[], Boolean>();
		this.value_SeenThisTrial = new Hashtable<ArrayList<int[]>, Boolean>();
	}
	
	//For testing purposes only.
	/*public static void main(String[] args){
		CorrelationBuilder c = new CorrelationBuilder();
		System.out.println(c.locationTiles.get(1)[0][0] + "," + c.locationTiles.get(1)[0][1]);
		System.out.println(c.locationTiles.get(1)[1][0] + "," + c.locationTiles.get(1)[1][1]);
		System.out.println(c.locationTiles.get(2)[0][0] + "," + c.locationTiles.get(2)[0][1]);
		System.out.println(c.locationTiles.get(2)[1][0] + "," + c.locationTiles.get(2)[1][1]);
	}*/
	
}




