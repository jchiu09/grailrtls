package org.grailrtls.java_tutorials.solver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Calendar;
import java.util.Hashtable;
import java.text.SimpleDateFormat;

import org.grailrtls.distributor.DistributorClientInterface;
import org.grailrtls.distributor.DistributorSolverInterface;
import org.grailrtls.distributor.client.listeners.SolutionListener;
import org.grailrtls.distributor.client.messages.DataTransferMessage;
import org.grailrtls.distributor.client.messages.DataTypeRequestMessage;
import org.grailrtls.distributor.client.messages.DataTypeSpecificationMessage;
import org.grailrtls.distributor.client.messages.DataTransferMessage.Solution;
import org.grailrtls.distributor.client.messages.HistoricStreamingCompleteMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that predicts that Teatime is about to occur soon. Sends solution to distributor
 * with time of prediction. Assumes that Teatime always starts sometime between 3 and 5 pm.
 * 
 * The algorithm uses two different methods of making a prediction:
 * 
 * 1.)  If the time is between 3 and 5 pm, teatime has not occurred today, and Ben's mug localizes inside the
 * 		kitchen twice, then we can declare that teatime is about to occur. This is a neat and easy shortcut to
 * 		use since Ben almost always starts teatime if he is in WINLAB. 
 * 
 * 2.)  If the time is between 3 and 5 pm and teatime has not occurred today, the algorithm will continue to check
 * 		if any one mug localizes inside the kitchen. If one mug localizes inside the kitchen, the algorithm will 
 * 		declare it as the "kitchen mug" and go into a different state. In this other state, it checks the solutions 
 * 		regarding all the other mugs and coffee brewing. 
 * 
 * 		If 2 of 3 following occur: 
 * 	    	a.) 2 different mugs experience multiple movements
 * 			b.) The Euclidean distance of 2 different mugs to the center point of the kitchen decreases
 * 			c.) Coffee is brewing 
 * 		
 * 		Then the solver can declare teatime is about to occur. 
 * 
 * 
 * @author Sumedh Sawant
 *
 */

public class TeatimePredictionSolver extends Thread implements
	SolutionListener,
	org.grailrtls.distributor.client.listeners.ConnectionListener,
	org.grailrtls.distributor.solver.listeners.ConnectionListener{

	/**
     * Logging facility for this class.
     */
    private static final Logger log = LoggerFactory.getLogger(TeatimePredictionSolver.class);
    
    /**
     * Keep track of the solution URI name.
     */
    private static final String SOLUTION_URI_NAME = "prediction.teatime";

    /**
     * Region URI.
     */
    private static final String SOLUTION_REGION_URI = "Winlab";
    
    
    /**
     * Used to get user input.
     */
    private BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));

    /**
     * Flag to keep running until the connection is closed.
     */
    private boolean keepRunning = true;

    /**
     * Interface to the distributor as a client, takes care of all the basic
     * protocol stuff.
     */
    private DistributorClientInterface clientDistributorInterface = new DistributorClientInterface();
    
    /**
     * Interface to the distributor for publishing solutions. Takes care of all
     * the basic protocol stuff.
     */
    private DistributorSolverInterface solverDistributorInterface = new DistributorSolverInterface();

    private String kitchenMug = "";
    
    private int teatimeAlias = -1;
    private int coffeeBrewingAlias = -1;
    private int locationAlias = -1;
    private int mobilityAlias = -1;
    private int mugsMovedCount = 0;
    private int closerMugsCount = 0;
    private int shortcutLocalizationCount = 0;
    private int brewingCount = 0;
    private int kitchenMugAstrayCount = 0;
    
    
    private boolean teatimeHappened = false;
    private boolean madePredictionAlready = false;
    private boolean mugInKitchen = false;
    
    private static final int MOBILITY_THRESHOLD = 2;
    private static final int MUG_LOCATION_THRESHOLD = 2;
    private static final int SHORTCUT_LOCATION_THRESHOLD = 2;
    private static final int COFFEE_BREWING_THRESHOLD = 1;
    private static final String BENS_MUG_ID = "53";
    private static final String COFFEEPOT_ID = "26";
    
    private String[] mugIDs = new String[]{"55", "631", "88", "608", "537", "57", "228", "533", "538"};
    private Hashtable<String, Double> mugEuclideanDistances = new Hashtable<String, Double>();
    private Hashtable<String, Integer> mugIncreaseDecrease = new Hashtable<String, Integer>();
    private Hashtable<String, Integer> mugMovementCount = new Hashtable<String, Integer>();

    
    /**
     * Creates a new TeatimePredictionSolver and starts it.
     */
    public static void main(String[] args) {
        if (args.length != 4) {
            printUsageInfo();
            System.exit(1);
        }

        int clientDistPort = Integer.parseInt(args[1]);
        int solverDistPort = Integer.parseInt(args[3]);
        
        TeatimePredictionSolver solver = new TeatimePredictionSolver(args[0], clientDistPort, args[2], solverDistPort);
        solver.start();       
    }

    /**
     * Prints a simple message about the parameters for this program.
     */
    protected static void printUsageInfo() {
        System.out.println("Requires four parameters: <ClientDistributor host> <ClientDistributor port> <SolverDistributor host> <SolverDistributor port>.");
        System.out.println("Once running, type \"exit\" to exit.");
    }
    
    /**
     * CONSTRUCTOR
     * 
     * Creates a new "TeatimePredictionSolver" that connects to the
     * distributor and world server at the specified host name/IP addresses and port numbers. 
     * 
     * @param clientDistHost
     *            the host-name or IP address of the distributor.
     * @param clientDistPort
     *            the port-number the distributor is listening for clients on.
     * @param solverDistHost
     * 			  the host-name or IP address of the distributor
     * @param solverDistPort
     * 			  the port-number the distributor is listening for solvers on.    
     */
    public TeatimePredictionSolver(String clientDistHost, int clientDistPort, String solverDistHost, int solverDistPort){
        this.clientDistributorInterface.setHost(clientDistHost);
        this.clientDistributorInterface.setPort(clientDistPort);
        this.solverDistributorInterface.setHost(solverDistHost);
        this.solverDistributorInterface.setPort(solverDistPort);
    }
	
    /**
     * Connects to the distributor as a client and as a solver. Checks for user
     * input every 100ms, and exits if the connection is ended, or
     * the user types "exit".
     */
    @Override
    public void run() {
        /*
         * "Client Distributor" interface set-up.
         */

        // Add this class as a dataTransferMessage listener so that the data can
        // be used to generate solutions.
        this.clientDistributorInterface.addSolutionListener(this);

        // Add this class as a connection listener to know when the connection
        // closes and also when the type specification message has been sent.
        this.clientDistributorInterface.addConnectionListener(this);

        // Disconnect if we have any problems
        this.clientDistributorInterface.setDisconnectOnException(true);

        // Don't try to reconnect after a disconnect
        this.clientDistributorInterface.setStayConnected(true);

        // Wait 30 seconds between attempts to connect to the distributor as a
        // client.
        this.clientDistributorInterface.setConnectionRetryDelay(10000l);
   
        
        /*
         * "Solver Distributor" interface set-up.
         */

        // Add the solution type URI name to the distributor interface
        this.solverDistributorInterface.addSolutionType(SOLUTION_URI_NAME);

        // Add this class as a connection listener to know when the connection
        // closes
        this.solverDistributorInterface.addConnectionListener(this);

        // Set the time between connection attempts to 10s.
        this.solverDistributorInterface.setConnectionRetryDelay(10000l);

        // Try to stay connected to the distributor.
        this.solverDistributorInterface.setStayConnected(true);

        // Close the current connection if there is a protocol exception.
        this.solverDistributorInterface.setDisconnectOnException(true);

        
        // Connect to the distributor as a solver.
        if (!this.solverDistributorInterface.doConnectionSetup()) {
            System.out.println("Unable to establish a connection to the distributor as a solver.");
            System.exit(1);
        }
        System.out.println("Connected to distributor as a solver at "
                + this.solverDistributorInterface.getHost() + ":"
                + this.solverDistributorInterface.getPort());
        
        
        // Connect to the distributor as a client.
        if (!this.clientDistributorInterface.doConnectionSetup()) {
            System.out.println("Unable to establish a connection to the distributor as a client.");
            System.exit(1);
        }
        System.out.println("Connected to Distributor as a client at "
                + this.clientDistributorInterface.getHost() + ":"
                + this.clientDistributorInterface.getPort());
        
        // Read the user input into this string
        String command = null;
       
        // Keep running until either we get disconnected, or the user wants to
        // quit.
        while (this.keepRunning) {
            try {
                // If the user has something to read in the buffer.
                if (this.userInput.ready()) {
                    command = this.userInput.readLine();

                    // Allow the solver to shut down gracefully if the user
                    // wants to exit.
                    if ("exit".equalsIgnoreCase(command)) {
                        System.out.println("Exiting solver (user).");
                        break;
                    } else {
                        System.out.println("Unknown command \"" + command
                                + "\". Type \"exit\" to exit.");
                    }
                }
            } catch (IOException e1) {
                System.err.println("Error reading user input.");
                e1.printStackTrace();
            }
            // Sleep for 100ms, since there isn't much else to do.
            try {
                Thread.sleep(100l); 
            } 
            catch (InterruptedException e) {
                // Ignored
            }
        }
        // Stop running
        this.clientDistributorInterface.doConnectionTearDown();
        this.solverDistributorInterface.doConnectionTearDown();
        System.exit(0);
    }
    
    public void connectionEnded(DistributorClientInterface distributor) {
		 // Stop running after the connection is finally closed.
       System.out.println("Connection closed.");
       this.keepRunning = false;
       this.interrupt();
	}

	public void connectionEstablished(DistributorClientInterface distributor) {
		// NOT USED
		
	}

	public void connectionInterrupted(DistributorClientInterface distributor) {
		// NOT USED
		
	}
    
	public void specificationMessageReceived(
			DistributorClientInterface distributor,
			DataTypeSpecificationMessage typeSpecificationMessage) {

		DataTypeRequestMessage request = new DataTypeRequestMessage();
		
		//Request live data
		request.setBeginTime(0l);
		request.setEndTime(0l);
		
		org.grailrtls.distributor.client.messages.DataTypeSpecificationMessage.DataType[] types = typeSpecificationMessage
        																							.getTypeSpecifications();
		ArrayList<Integer> aliases = new ArrayList<Integer>();
		
		for (org.grailrtls.distributor.client.messages.DataTypeSpecificationMessage.DataType type : types) {
	    	
			log.info(type.getUriName() + " " + type.getTypeAlias());
			
			if("happening".equalsIgnoreCase(type.getUriName())){
	    		aliases.add(type.getTypeAlias());
	    		this.teatimeAlias = type.getTypeAlias();
	    	}
			if("coffee brewing".equalsIgnoreCase(type.getUriName())){
	    		aliases.add(type.getTypeAlias());
	    		this.coffeeBrewingAlias = type.getTypeAlias();
	    	}
			if("mobility".equalsIgnoreCase(type.getUriName())){
	    		aliases.add(type.getTypeAlias());
	    		this.mobilityAlias = type.getTypeAlias();
	    	}
			if("location".equalsIgnoreCase(type.getUriName())){
	    		aliases.add(type.getTypeAlias());
	    		this.locationAlias = type.getTypeAlias();
	    	}
	    }
		
		if(!aliases.isEmpty()){
			int[] temp = new int[aliases.size()];
			int j = 0;
			for(Integer i: aliases){
				temp[j] = i;
				j++;
			}
			request.setTypeAliases(temp);
		}
		
	    if (request.getTypeAliases() == null) {
	        log.error("FAILED TO GENERATE Data Request MESSAGE");
	        System.exit(1);
	    }
	    
		this.clientDistributorInterface.getSession().write(request);
	}
	
	
	/**
	 * This method returns a boolean of whether a particular String is contained in a String array.
	 */
	public boolean containedInStringArray(String[] arr, String str){
		for(String s: arr){
			if(s.equalsIgnoreCase(str)){
				return true;
			}
		}
		return false;
	}

	public void dataReceived(DistributorClientInterface distributor,
			DataTransferMessage solutions) {
		
		//There is use in wasting time calculating things if there is no reason to predict.
		if(this.noReasonToPredict()){
			return;
		}
		
		//Checks conditions required to predict teatime
		if(this.teatimeConditionsSatisfied()){
			this.sendPredictionSolution();
		}
		
		Solution[] solutionArray = solutions.getSolutions();
		
		for(Solution solution: solutionArray){
			
			String solutionTargetName = solution.getTargetName();
			int solutionTypeAlias = solution.getTypeAlias();
			
			
			//Handle solutions regarding Ben's mug
			if(TeatimePredictionSolver.BENS_MUG_ID.equalsIgnoreCase(solutionTargetName)){
				log.debug("Solution Received {}", solutions);
				
				//If a location solution is received
				if(solutionTypeAlias == this.locationAlias){
					this.handleBensMugLocationSolution(solution.getData());
				}
			}
			
			
			//Handles other mugs' solutions
			if(this.containedInStringArray(this.mugIDs, solutionTargetName)){
				if(solutionTypeAlias == this.locationAlias){
					this.handleOtherMugsLocationSolution(solutionTargetName, solution.getData());
				}
				
				if(solutionTypeAlias == this.mobilityAlias){
					this.handleOtherMugsMobilitySolution(solutionTargetName);
				}	
			}
			
			//Handle solutions regarding teatime.
			if("teatime".equalsIgnoreCase(solutionTargetName)){
				log.debug("Solution Received {}", solutions);
				
				//If a "teatime-happening" solution is received
				if(solutionTypeAlias == this.teatimeAlias){
					this.teatimeHappened = true;
					log.info("Teatime is happening");
				}
			}
			
			
			//Handle solutions regarding coffee brewing.
			if(TeatimePredictionSolver.COFFEEPOT_ID.equalsIgnoreCase(solutionTargetName)){
				log.debug("Solution Received {}", solutions);
				
				//If a coffee brewing solution is received
				if(solutionTypeAlias == this.coffeeBrewingAlias){
					this.brewingCount++;
					log.info("fresh coffee is being made.");
				}
			}
				
		}
	}
	
	
	private void handleOtherMugsMobilitySolution(String solutionTargetName){
		if(this.mugInKitchen){
			if(this.mugMovementCount.get(solutionTargetName) == null){
				this.mugMovementCount.put(solutionTargetName, 1);
				this.mugsMovedCount++;
			}
			else{
				this.mugMovementCount.put(solutionTargetName, this.mugMovementCount.get(solutionTargetName) + 1);
			}
		}
	}
	
	private void handleOtherMugsLocationSolution(String solutionTargetName, byte[] solutionData){
		byte[] b = solutionData;
		ByteBuffer bb = ByteBuffer.wrap(b);
		double locationX = Math.round(bb.getDouble());
		double locationY = Math.round(bb.getDouble(8));
		
		double euclidDistance = this.getEuclideanDistance(locationX, locationY);
		
		if(!this.mugInKitchen){
			//If the mug localizes inside the kitchen
			if((locationX >=85 && locationX<=110) &&
					((locationY>=45 && locationY <= 60))){
				this.mugInKitchen = true;
				this.kitchenMug = solutionTargetName;
			}
		}
		
		
		//If we are in the state in which there is a mug in the kitchen
		if(this.mugInKitchen){
			
			//First make some checks to make sure that "kitchen mug" is still in the kitchen
			this.verifyKitchenMugPosition(solutionTargetName, locationX, locationY);
			
			
			if(this.mugEuclideanDistances.get(solutionTargetName) == null){
				this.mugEuclideanDistances.put(solutionTargetName, euclidDistance);
				this.mugIncreaseDecrease.put(solutionTargetName, 0);
			}
			else{
				double previous = this.mugEuclideanDistances.get(solutionTargetName);
				if(previous > euclidDistance){
					this.mugIncreaseDecrease.put(solutionTargetName, -1);
					this.closerMugsCount++;
				}
				else{
					this.mugIncreaseDecrease.put(solutionTargetName, 1);
				}
			}
		}
	}
	
	
	/**
	 * This method handles a location solution about Ben's mug. If the mug localizes twice inside the kitchen,
	 * we can automatically predict teatime is about to occur soon. 
	 * 
	 * @param solutionData
	 */
	private void handleBensMugLocationSolution(byte[] solutionData){
		byte[] b = solutionData;
		ByteBuffer bb = ByteBuffer.wrap(b);
		double locationX = Math.round(bb.getDouble());
		double locationY = Math.round(bb.getDouble(8));
		
		//If the mug localizes inside the kitchen
		if((locationX >=85 && locationX<=110) &&
				((locationY>=45 && locationY <= 60))){
			log.info("Ben's mug is in kitchen!");
			this.mugInKitchen = true;
			this.kitchenMug = TeatimePredictionSolver.BENS_MUG_ID;
			this.shortcutLocalizationCount++;
			if(this.shortcutLocalizationCount == TeatimePredictionSolver.SHORTCUT_LOCATION_THRESHOLD){
				
				//This is a shortcut since Ben almost always starts teatime.
				this.sendPredictionSolution();
				log.info("Ben's Mug is inside the kitchen for long enough, so teatime is about to occur.");
			}
		}
	}
	
	
	/**
	 * This method verifies that the "kitchen mug" that localized in the kitchen earlier is still present there.
	 * If this mug localizes outside the kitchen twice, the algorithm reverts back to its earlier state where 
	 * it has to find a mug that localizes in the kitchen first. 
	 * 
	 * @param solutionTargetName
	 * @param locationX
	 * @param locationY
	 */
	private void verifyKitchenMugPosition(String solutionTargetName, double locationX, double locationY){
		if(solutionTargetName.equalsIgnoreCase(this.kitchenMug)){
			//If the kitchen mug is NOT in the kitchen anymore
			if(!((locationX >=85 && locationX<=110) &&
					((locationY>=45 && locationY <= 60)))){
				this.kitchenMugAstrayCount++;

			}
			
			//The "kitchen Mug" localized outside the kitchen twice, so its not in the kitchen anymore
			if(this.kitchenMugAstrayCount >=2){
				this.mugInKitchen = false;
				this.kitchenMug = "";
				this.kitchenMugAstrayCount = 0;
			}
		}
	}
	
	/**
	 * If this returns true, it means a prediction is not required to be calculated.
	 * Self-explanatory from code in method. 
	 * 
	 * @return boolean
	 */
	public boolean noReasonToPredict(){
		if(this.teatimeHappened || this.madePredictionAlready || !this.timeCheck()){
			return true;
		}
		else{
			return false;
		}
	}
	
	/**
	 * Resets instance variables.
	 */
	private void resetSolver(){
		this.madePredictionAlready = true;
		this.mugInKitchen = false;
		this.kitchenMug = "";
		this.mugEuclideanDistances = new Hashtable<String, Double>();
		this.mugIncreaseDecrease = new Hashtable<String, Integer>();
		this.mugMovementCount = new Hashtable<String, Integer>();
    	this.teatimeHappened = false;
    	this.madePredictionAlready = false;
    	this.closerMugsCount = 0;
    	this.mugsMovedCount = 0;
    	this.brewingCount = 0;
    	this.kitchenMugAstrayCount = 0;
	}
	
	/**
	 * Sends a solution to the distributor with time of prediction.
	 */
	private void sendPredictionSolution(){
		
		//Reset the solver.
		this.resetSolver();
		
		log.info("Solver Predicts TEATIME IS ABOUT TO HAPPEN!");
		
		org.grailrtls.distributor.solver.messages.DataTransferMessage.Solution solution = new org.grailrtls.distributor.solver.messages.DataTransferMessage.Solution();
		solution.setTargetName("teatime");
   
		solution.setTypeUri(SOLUTION_URI_NAME);
       
		ByteBuffer buff = ByteBuffer.allocate(8);
        buff.putLong(System.currentTimeMillis());
        
        solution.setData(buff.array());

        Collection<org.grailrtls.distributor.solver.messages.DataTransferMessage.Solution> returnedList = new ArrayList<org.grailrtls.distributor.solver.messages.DataTransferMessage.Solution>();
        returnedList.add(solution);
       
        if (!this.solverDistributorInterface.sendSolutions(SOLUTION_REGION_URI,
                returnedList)) {
            log.warn("Unable to send solution to distributor: {}", returnedList);
        }
        log.info("Sent Prediction Solution!");
	}
	
	
	/**
	 * If there is a mug in the kitchen,
	 * We can predict Teatime is about to occur if 4 of the 5 conditions are satisfied:
	 * 
	 * 1.) Time is between 3 and 5 PM
	 * 2.) Teatime has not occurred today.
	 * 3.) Coffee is brewing --> once 
	 * 4.) 2 different mugs experience multiple movements
	 * 5.) The Euclidean distance of 2 different mugs to the center point of the kitchen decreases
	 * 
	 * @return boolean
	 */
	
	private boolean teatimeConditionsSatisfied(){
		int conditionsSatisfied = 0;
		
		if(!this.mugInKitchen){
			return false;
		}
		
			if(this.timeCheck()){
				conditionsSatisfied++;
			}
			
			if(!this.madePredictionAlready){
				conditionsSatisfied++;
			}
			
			if(this.closerMugsCount >= TeatimePredictionSolver.MUG_LOCATION_THRESHOLD){
				conditionsSatisfied++;
			}
			
			if(this.mugsMovedCount >= TeatimePredictionSolver.MOBILITY_THRESHOLD){
				conditionsSatisfied++;
			}
			
			if(this.brewingCount >= TeatimePredictionSolver.COFFEE_BREWING_THRESHOLD){
				conditionsSatisfied++;
			}
		
		if(conditionsSatisfied >= 4){
			return true;
		}
		else{
			return false;
		}
	}
	

	
	/**
	 * Returns true if the hour of day is between 3 and 5 PM.
	 * 
	 * @return boolean
	 */
	private boolean timeCheck(){
		String DATE_FORMAT_NOW = "HH";
		Calendar cal = Calendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
	    String time = sdf.format(cal.getTime());
	    
	    int hour;
	    
	    try{
	    	hour = Integer.parseInt(time);
	    }catch (NumberFormatException e){
	    	hour = 0;
	    }
	    
	    if(hour>=15 && hour<=17){
	    	return true;
	    }
	    
	    return false;
	}
	
	/**
	 * Returns Euclidean distance to the center point of the kitchen.
	 */
	private double getEuclideanDistance(double x, double y){
		double kitchenX = 92.5;
		double kitchenY = 85;
		
		double temp = Math.pow((kitchenX - x), 2);
		double temp2 = Math.pow((kitchenY - y), 2);
		double result = Math.sqrt((temp + temp2));
		
		return result;
	}

	public void connectionEnded(DistributorSolverInterface distributor) {
		// Stop running after the connection is finally closed.
        System.out.println("Connection closed.");
        this.keepRunning = false;
        this.interrupt();	
	}

	public void connectionEstablished(DistributorSolverInterface distributor) {
		// NOT USED
		
	}

	public void connectionInterrupted(DistributorSolverInterface distributor) {
		// NOT USED
		
	}

	public void specificationMessageSent(DistributorSolverInterface distributor) {
		// NOT USED
		
	}

	public void historicQueryEnded(DistributorClientInterface distributor,
			HistoricStreamingCompleteMessage message) {
		// NOT USED
		
	}

}
