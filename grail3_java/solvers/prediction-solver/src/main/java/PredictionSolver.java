

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;

import org.grailrtls.distributor.DistributorClientInterface;
import org.grailrtls.distributor.DistributorSolverInterface;
import org.grailrtls.distributor.client.listeners.SolutionListener;
import org.grailrtls.distributor.client.messages.DataTransferMessage;
import org.grailrtls.distributor.client.messages.DataTransferMessage.Solution;
import org.grailrtls.distributor.client.messages.DataTypeRequestMessage;
import org.grailrtls.distributor.client.messages.DataTypeSpecificationMessage;
import org.grailrtls.distributor.client.messages.HistoricStreamingCompleteMessage;
import org.grailrtls.java_tutorials.solver.CorrelationHeap.HeapNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 * **Note: Throughout the comments "Historic Solution Data" refers to the specific times that the solution 
 * 			that we are trying to predict occurred.
 * 
 * 			"Historic Correlation Data" refers to all the solutions about the environment just before all the
 * 			times denoted in the "Historic Solution Data"
 * 
 * 
 * @author Sumedh Sawant
 *
 */
public class PredictionSolver extends Thread implements
	SolutionListener,
	org.grailrtls.distributor.client.listeners.ConnectionListener,
	org.grailrtls.distributor.solver.listeners.ConnectionListener{

	/**
     * Logging facility for this class.
     */
    private static final Logger log = LoggerFactory.getLogger(PredictionSolver.class);
    
    /**
     * Keep track of the solution URI name. The event being predicted must be appended to the end of the
     * String to create the real Solution URI Name.
     */
    private String SOLUTION_URI_NAME_TO_SUBMIT = "prediction.";

    /**
     * Region URI.
     */
	private static final String SOLUTION_REGION_URI = "Winlab";
    
    
    /**
     * Used to get user input.
     */
    private BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));

    /**
     * Flag to keep running until the connection is closed.
     */
    private boolean keepRunning = true;

    /**
     * Interface to the distributor as a client, takes care of all the basic
     * protocol stuff.
     */
    private DistributorClientInterface clientDistributorInterface = new DistributorClientInterface();
    
    /**
     * Interface to the distributor for publishing solutions. Takes care of all
     * the basic protocol stuff.
     */
    private DistributorSolverInterface solverDistributorInterface = new DistributorSolverInterface();
	
    private String solutionURIName;
    private String solutionTargetName;
    
    /**
     * This boolean is true when this solver is receiving data regarding the past occurrences of the solution
     * that the solver is trying to predict (ex. all the previous times Teatime occurred in the last 2 weeks)
     */
    private boolean gettingHistoricSolutionData = true;
    
    /**
     * This boolean is true when the solver is receiving data regarding the events that 
     * occur right before the past occurrences of the solution that the solver is trying to predict.
     * (ex. get solutions about everything else right before Teatime occurs)
     */
    private boolean gettingHistoricCorrelationData = false;
    
    private boolean receivingLiveSolutionData = false;
    
    /**
     * This int keeps track of how many historic query ended messages have been received.
     */
    private int HistoricQueryEndedCount = 0;
    
    /**
     * This queue keeps track of the historic Solution data times in order.
     */
    private ArrayList<Long> historicSolutionTimes = new ArrayList<Long>();
    
    private int currentHistoricSolutionIndex = 0;
    private int solutionData;
    
    private DataTypeSpecificationMessage specificationMessage;
    
    private String[] requestedSolutionsList = new String[]{"closed","coffee brewing","cups poured","door ajar","event","happening","location","mobility","on","passive motion.tile",
    		"prediction","printer status","stolen","temperature","used",};
    
    private ArrayList<String> requestedSolutions = new ArrayList<String>();
    
    private final static int CORRELATION_THRESHOLD_60_45 = 2;
    private CorrelationBuilder correlations_60_45 = new CorrelationBuilder();
    private CorrelationHeap correlationHeap_60_45 = new CorrelationHeap();
    private int correlationsSatisfied_60_45 = 0;
    private ArrayList<HeapNode> topCorrelations_60_45 = new ArrayList<HeapNode>();
    private ArrayList<HeapNode> tempList_60_45 = new ArrayList<HeapNode>();
    
    
    private final static int CORRELATION_THRESHOLD_45_30 = 2;
    private CorrelationBuilder correlations_45_30 = new CorrelationBuilder();
    private CorrelationHeap correlationHeap_45_30 = new CorrelationHeap();
    private int correlationsSatisfied_45_30 = 0;
    private ArrayList<HeapNode> topCorrelations_45_30 = new ArrayList<HeapNode>();
    private ArrayList<HeapNode> tempList_45_30 = new ArrayList<HeapNode>();
    
    
    private final static int CORRELATION_THRESHOLD_30_15 = 4;
    private CorrelationBuilder correlations_30_15 = new CorrelationBuilder();
    private CorrelationHeap correlationHeap_30_15 = new CorrelationHeap();
    private int correlationsSatisfied_30_15 = 0;
    private ArrayList<HeapNode> topCorrelations_30_15 = new ArrayList<HeapNode>();
    private ArrayList<HeapNode> tempList_30_15 = new ArrayList<HeapNode>();
    
    
    private final static int CORRELATION_THRESHOLD_15_0 = 6;
    private CorrelationBuilder correlations_15_0 = new CorrelationBuilder();
    private CorrelationHeap correlationHeap_15_0 = new CorrelationHeap();
    private int correlationsSatisfied_15_0 = 0;
    private ArrayList<HeapNode> topCorrelations_15_0 = new ArrayList<HeapNode>();
    private ArrayList<HeapNode> tempList_15_0 = new ArrayList<HeapNode>();
    
    
    private int totalCorrelationsSatisfiedCount = 0;
    private Timer timer = new Timer();
    
    
    /**
     * Creates a new OracleSolver and starts it.
     */
    public static void main(String[] args) {
        if (args.length != 7) {
            printUsageInfo();
            System.exit(1);
        }

        int clientDistPort = Integer.parseInt(args[1]);
        int solverDistPort = Integer.parseInt(args[3]);
        
        PredictionSolver solver = new PredictionSolver(args[0], clientDistPort, args[2], solverDistPort, args[4], args[5]);
        solver.solutionData = Integer.parseInt(args[6]);
        solver.start();
    }

    
    /**
     * Prints a simple message about the parameters for this program.
     */
    protected static void printUsageInfo() {
        System.out.println("Requires seven parameters: <ClientDistributor host> <ClientDistributor port> <SolverDistributor host> <SolverDistributor port><solutionURIToBePredicted><solutionTargetToBePredicted><Solution Data>.");
        System.out.println("solutionURIToBePredicted is the String solution URI of the event that you are trying to predict.");
        System.out.println("solutionTargetToBePredicted is the solution target name of the event that you are trying to predict.");
        System.out.println("solutionData is 1 or 0 for a binary solution type");
        System.out.println("Once running, type \"exit\" to exit.");
    }
    
    
    /**
     * CONSTRUCTOR
     * 
     * Creates a new "TeatimePredictionSolver" that connects to the
     * distributor and world server at the specified host name/IP addresses and port numbers. 
     * 
     * @param clientDistHost
     *            the host-name or IP address of the distributor.
     * @param clientDistPort
     *            the port-number the distributor is listening for clients on.
     * @param solverDistHost
     * 			  the host-name or IP address of the distributor
     * @param solverDistPort
     * 			  the port-number the distributor is listening for solvers on.  
     * @param solutionURIToBePredicted
     * 			  the String solution URI of the event that you are trying to predict.
     * @param solutionTargetToBePredicted
     * 			  the solution target name of the event that you are trying to predict.
     */
    public PredictionSolver(String clientDistHost, int clientDistPort, String solverDistHost, int solverDistPort, String solutionURIToBePredicted, String solutionTargetToBePredicted){
        this.clientDistributorInterface.setHost(clientDistHost);
        this.clientDistributorInterface.setPort(clientDistPort);
        this.solverDistributorInterface.setHost(solverDistHost);
        this.solverDistributorInterface.setPort(solverDistPort);
        this.solutionURIName = solutionURIToBePredicted;
        this.solutionTargetName = solutionTargetToBePredicted;
        this.SOLUTION_URI_NAME_TO_SUBMIT += this.solutionURIName;
        for(String requestedSolution: this.requestedSolutionsList){
        	this.requestedSolutions.add(requestedSolution);
        }
    }
	
    
    /**
     * Connects to the distributor as a client and as a solver. Checks for user
     * input every 100ms, and exits if the connection is ended, or
     * the user types "exit".
     */
    @Override
    public void run() {
        /*
         * "Client Distributor" interface set-up.
         */

        // Add this class as a dataTransferMessage listener so that the data can
        // be used to generate solutions.
        this.clientDistributorInterface.addSolutionListener(this);

        // Add this class as a connection listener to know when the connection
        // closes and also when the type specification message has been sent.
        this.clientDistributorInterface.addConnectionListener(this);

        // Disconnect if we have any problems
        this.clientDistributorInterface.setDisconnectOnException(true);

        // Don't try to reconnect after a disconnect
        this.clientDistributorInterface.setStayConnected(true);

        // Wait 30 seconds between attempts to connect to the distributor as a
        // client.
        this.clientDistributorInterface.setConnectionRetryDelay(10000l);
   
        
        /*
         * "Solver Distributor" interface set-up.
         */

        // Add the solution type URI name to the distributor interface
        this.solverDistributorInterface.addSolutionType(SOLUTION_URI_NAME_TO_SUBMIT);

        // Add this class as a connection listener to know when the connection
        // closes
        this.solverDistributorInterface.addConnectionListener(this);

        // Set the time between connection attempts to 10s.
        this.solverDistributorInterface.setConnectionRetryDelay(10000l);

        // Try to stay connected to the distributor.
        this.solverDistributorInterface.setStayConnected(true);

        // Close the current connection if there is a protocol exception.
        this.solverDistributorInterface.setDisconnectOnException(true);

        
        // Connect to the distributor as a solver.
        if (!this.solverDistributorInterface.doConnectionSetup()) {
            System.out.println("Unable to establish a connection to the distributor as a solver.");
            System.exit(1);
        }
        System.out.println("Connected to distributor as a solver at "
                + this.solverDistributorInterface.getHost() + ":"
                + this.solverDistributorInterface.getPort());
        
        
        // Connect to the distributor as a client.
        if (!this.clientDistributorInterface.doConnectionSetup()) {
            System.out.println("Unable to establish a connection to the distributor as a client.");
            System.exit(1);
        }
        System.out.println("Connected to Distributor as a client at "
                + this.clientDistributorInterface.getHost() + ":"
                + this.clientDistributorInterface.getPort());
        
        // Read the user input into this string
        String command = null;
       
        // Keep running until either we get disconnected, or the user wants to
        // quit.
        while (this.keepRunning) {
            try {
                // If the user has something to read in the buffer.
                if (this.userInput.ready()) {
                    command = this.userInput.readLine();

                    // Allow the solver to shut down gracefully if the user
                    // wants to exit.
                    if ("exit".equalsIgnoreCase(command)) {
                        System.out.println("Exiting solver (user).");
                        break;
                    } else {
                        System.out.println("Unknown command \"" + command
                                + "\". Type \"exit\" to exit.");
                    }
                }
            } catch (IOException e1) {
                System.err.println("Error reading user input.");
                e1.printStackTrace();
            }
            // Sleep for 100ms, since there isn't much else to do.
            try {
                Thread.sleep(100l); 
            } 
            catch (InterruptedException e) {
                // Ignored
            }
        }
        // Stop running
        this.clientDistributorInterface.doConnectionTearDown();
        this.solverDistributorInterface.doConnectionTearDown();
        System.exit(0);
    }
    
    
    public void connectionEnded(DistributorSolverInterface distributor) {
		// Stop running after the connection is finally closed.
        System.out.println("Connection closed.");
        this.keepRunning = false;
        this.interrupt();	
	}

	public void connectionEstablished(DistributorSolverInterface distributor) {
		// NOT USED
	}

	public void connectionInterrupted(DistributorSolverInterface distributor) {
		// NOT USED
	}

	public void specificationMessageSent(DistributorSolverInterface distributor) {
		// NOT USED
	}
	
	public void connectionEnded(DistributorClientInterface distributor) {
		 // Stop running after the connection is finally closed.
      System.out.println("Connection closed.");
      this.keepRunning = false;
      this.interrupt();
	}

	public void connectionEstablished(DistributorClientInterface distributor) {
		// NOT USED
	}

	public void connectionInterrupted(DistributorClientInterface distributor) {
		// NOT USED
	}
	

	public void specificationMessageReceived(
			DistributorClientInterface distributor,
			DataTypeSpecificationMessage typeSpecificationMessage) {

	    this.specificationMessage = typeSpecificationMessage;
	    ArrayList<String> requestedTypes = new ArrayList<String>();
	    requestedTypes.add(this.solutionURIName);
	    //1000 milliseconds * 60 seconds * 60 minutes * 24 hours * 14 days = 2 weeks
	    //Want the last 2 weeks of solutions so we can find the times that the solution that we are trying to predict occurred.
		this.clientDistributorInterface.setRequestedSolutions(requestedTypes, System.currentTimeMillis() - 1000*60*60*24*14l, System.currentTimeMillis());
	}


	public void dataReceived(DistributorClientInterface distributor,
			DataTransferMessage solutions) {

		log.debug("The solution timestamp is: {}", solutions.getTimestamp());
		log.debug("The current System time is: " + System.currentTimeMillis());
		log.debug("");
		
		
		//We are still receiving solutions regarding the specific historic times
		if(this.gettingHistoricSolutionData){
			for(Solution solution: solutions.getSolutions()){
				if((solution.getTargetName().equalsIgnoreCase(this.solutionTargetName)) &&
						(solution.getData()[0] == this.solutionData)){
					
					this.historicSolutionTimes.add(solutions.getTimestamp());
				}
			}
		}
		
		
		//We are receiving historic correlation data
		if(this.gettingHistoricCorrelationData){
			
			//This indicates that the data received relates to correlation data 60-45 minutes before 
			//the historic solution.
			if(this.HistoricQueryEndedCount%4 == 1){
				this.correlations_60_45.addSolutionDataPacket(solutions);
			}
			
			//This indicates that the data received relates to correlation data 45-30 minutes before 
			//the historic solution.
			if(this.HistoricQueryEndedCount%4 == 2){
				this.correlations_45_30.addSolutionDataPacket(solutions);
			}
			
			//This indicates that the data received relates to correlation data 30-15 minutes before 
			//the historic solution.
			if(this.HistoricQueryEndedCount%4 == 3){
				this.correlations_30_15.addSolutionDataPacket(solutions);
			}
			
			//This indicates that the data received relates to the correlation data 15 minutes before
			//the historic solution.
			if(this.HistoricQueryEndedCount%4 == 0){
				this.correlations_15_0.addSolutionDataPacket(solutions);
			}
		}
		
		
		//Here is where the live predictions (also maybe later some sort of machine learning) will take place after initial correlations have been built
		if(this.receivingLiveSolutionData){
			
			if(this.totalCorrelationsSatisfiedCount == 0){
				for(Solution solution: solutions.getSolutions()){
					for(HeapNode node: this.tempList_60_45){
						if((node.getSolutionURI().equalsIgnoreCase(solution.getTypeUri())) && 
								(node.getSolutionTarget().equalsIgnoreCase(solution.getTargetName())) &&
								(node.getSolutionData() == CorrelationBuilder.convertSolutionDataToInt(solution.getData(), solution.getTypeUri()))){
							
							this.tempList_60_45.remove(node);
							this.correlationsSatisfied_60_45++;
						}
					}
				}
				if(this.correlationsSatisfied_60_45 >= PredictionSolver.CORRELATION_THRESHOLD_60_45){
					this.totalCorrelationsSatisfiedCount++;
				}
			}
			//15 minutes have passed without any top correlations being satisfied.
			if((this.timer.elapsed() > 1000*60*15l) && (this.totalCorrelationsSatisfiedCount == 0)){
				this.timer.reset();
				this.resetTempLists();
				this.totalCorrelationsSatisfiedCount = 0;
			}
			
			
			if((this.totalCorrelationsSatisfiedCount == 1) && (this.timer.elapsed() > 1000*60*15l)){
				for(Solution solution: solutions.getSolutions()){
					for(HeapNode node: this.tempList_45_30){
						if((node.getSolutionURI().equalsIgnoreCase(solution.getTypeUri())) && 
								(node.getSolutionTarget().equalsIgnoreCase(solution.getTargetName())) &&
								(node.getSolutionData() == CorrelationBuilder.convertSolutionDataToInt(solution.getData(), solution.getTypeUri()))){
							
							this.tempList_45_30.remove(node);
							this.correlationsSatisfied_45_30++;
						}
					}
				}
				if(this.correlationsSatisfied_45_30 >= PredictionSolver.CORRELATION_THRESHOLD_45_30){
					this.totalCorrelationsSatisfiedCount++;
				}
			}
			//15 more minutes have passed without any top correlations being satisfied.
			if((this.timer.elapsed() > 1000*60*30l) && (this.totalCorrelationsSatisfiedCount == 1)){
				this.timer.reset();
				this.resetTempLists();
				this.totalCorrelationsSatisfiedCount = 0;
			}
			
			
			if(this.totalCorrelationsSatisfiedCount == 2){
				for(Solution solution: solutions.getSolutions()){
					for(HeapNode node: this.tempList_30_15){
						if((node.getSolutionURI().equalsIgnoreCase(solution.getTypeUri())) && 
								(node.getSolutionTarget().equalsIgnoreCase(solution.getTargetName())) &&
								(node.getSolutionData() == CorrelationBuilder.convertSolutionDataToInt(solution.getData(), solution.getTypeUri()))){
							
							this.tempList_30_15.remove(node);
							this.correlationsSatisfied_30_15++;
						}
					}
				}
				if(this.correlationsSatisfied_30_15 >= PredictionSolver.CORRELATION_THRESHOLD_30_15){
					this.totalCorrelationsSatisfiedCount++;
				}
			}
			//15 more minutes have passed without any top correlations being satisfied.
			if((this.timer.elapsed() > 1000*60*45l) && (this.totalCorrelationsSatisfiedCount == 2)){
				this.timer.reset();
				this.resetTempLists();
				this.totalCorrelationsSatisfiedCount = 0;
			}
			
			
			if(this.totalCorrelationsSatisfiedCount == 3){
				for(Solution solution: solutions.getSolutions()){
					for(HeapNode node: this.tempList_15_0){
						if((node.getSolutionURI().equalsIgnoreCase(solution.getTypeUri())) && 
								(node.getSolutionTarget().equalsIgnoreCase(solution.getTargetName())) &&
								(node.getSolutionData() == CorrelationBuilder.convertSolutionDataToInt(solution.getData(), solution.getTypeUri()))){
							
							this.tempList_15_0.remove(node);
							this.correlationsSatisfied_15_0++;
						}
					}
				}
				if(this.correlationsSatisfied_15_0 >= PredictionSolver.CORRELATION_THRESHOLD_15_0){
					this.totalCorrelationsSatisfiedCount++;
				}
				if(this.totalCorrelationsSatisfiedCount == 4){
					// Send solution
					this.createAndSendSolution();
					this.resetTempLists();
					this.totalCorrelationsSatisfiedCount = 0;
				}
			}
			//15 more minutes have passed without any top correlations being satisfied.
			if((this.timer.elapsed() > 1000*60*60l) && (this.totalCorrelationsSatisfiedCount == 3)){
				this.timer.reset();
				this.resetTempLists();
				this.totalCorrelationsSatisfiedCount = 0;
			}
		}
	}
	
	
	/**
	 * Sends a solution to the distributor with time of prediction.
	 */
	public void createAndSendSolution(){
		log.info("Solver Predicts "+ this.solutionTargetName + " IS ABOUT TO HAPPEN!");
		
		org.grailrtls.distributor.solver.messages.DataTransferMessage.Solution solution = new org.grailrtls.distributor.solver.messages.DataTransferMessage.Solution();
		solution.setTargetName(this.solutionTargetName);
   
		solution.setTypeUri(this.SOLUTION_URI_NAME_TO_SUBMIT);
       
		ByteBuffer buff = ByteBuffer.allocate(8);
        buff.putLong(System.currentTimeMillis());
        
        solution.setData(buff.array());

        Collection<org.grailrtls.distributor.solver.messages.DataTransferMessage.Solution> returnedList = new ArrayList<org.grailrtls.distributor.solver.messages.DataTransferMessage.Solution>();
        returnedList.add(solution);
       
        if (!this.solverDistributorInterface.sendSolutions(SOLUTION_REGION_URI,
                returnedList)) {
            log.warn("Unable to send solution to distributor: {}", returnedList);
        }
        log.info("Sent Prediction Solution!");
	}

	
	/**
	 * Since this method is called every time a historic query ends, it takes care of making all the 
	 * necessary historic query requests as well. When the last history query needed finishes, this 
	 * method will finally make a data request to the distributor for live data.
	 */
	public void historicQueryEnded(DistributorClientInterface distributor,
			HistoricStreamingCompleteMessage message) {
		
		this.HistoricQueryEndedCount++;
		log.info("HistoricStreamingCompleteMessage received: {}", message);

		if(this.HistoricQueryEndedCount == 0){
			log.info("Received all historic solution time data. Total of " + this.historicSolutionTimes.size() + " solutions");
			log.info("Requesting correlation data now.");
		}
		
		log.info("Historic Query Count is: " + this.HistoricQueryEndedCount + ". There should be " + (this.historicSolutionTimes.size()*4) + " total historic queries");
		
		//This indicates that the correlation data has all been received. A request for live data
		//can be made now.
		if(this.currentHistoricSolutionIndex == this.historicSolutionTimes.size()){
			this.gettingHistoricCorrelationData = false;
			this.receivingLiveSolutionData = true;
			
			//Live Data Streaming and set Correlations.
			log.info("REQUESTING LIVE DATA NOW.");
			
			this.timer.start();
			this.setCorrelations();
			this.setTopCorrelationLists();
			this.makeHistoricDataRequest(0l, 0l);
		}
		
		//Get the historic Solution Time being considered currently.
		long historicSolutionTime = Long.valueOf(
				this.historicSolutionTimes.get(this.currentHistoricSolutionIndex));
		
		
		//Make a request for correlation data 60-45 minutes before the historic solution time being considered
		if(this.HistoricQueryEndedCount%4 == 1){
			if(this.HistoricQueryEndedCount == 1){
				this.gettingHistoricSolutionData = false;
				this.gettingHistoricCorrelationData = true;
			}
			if(this.HistoricQueryEndedCount != 1){
				this.correlations_15_0.increaseTrialCount();
			}
			
			this.makeHistoricDataRequest(historicSolutionTime - 1000*60*60l, 
					historicSolutionTime - 1000*60*45l);
		}
		
		//Make a request for correlation data 45-30 minutes before the historic solution time being considered
		if(this.HistoricQueryEndedCount%4 == 2){
			
			this.correlations_60_45.increaseTrialCount();
			
			this.makeHistoricDataRequest(historicSolutionTime - 1000*60*45l, 
				historicSolutionTime - 1000*60*30l);
		}
		
		//Make a request for correlation data 30-15 minutes before the historic solution time being considered
		if(this.HistoricQueryEndedCount%4 == 3){
			
			this.correlations_45_30.increaseTrialCount();
			
			this.makeHistoricDataRequest(historicSolutionTime - 1000*60*30l, 
				historicSolutionTime - 1000*60*15l);
		}
		
		//Make a request for correlation data 15 minutes before the historic solution time being considered
		if(this.HistoricQueryEndedCount%4 == 0){
			
			this.correlations_30_15.increaseTrialCount();
			
			this.makeHistoricDataRequest(historicSolutionTime - 1000*60*15l, 
				historicSolutionTime);
			
			//Update the index so the next historic solution time considered is correct.
			this.currentHistoricSolutionIndex++;
		}
	}
	
	
	public static void setCorrelationHeap(CorrelationHeap heap, CorrelationBuilder builder){
		Hashtable<String[], ArrayList<int[]>> correlationRecord = builder.getCorrelationRecord();
		int trialCount = builder.getTrialCount();
		
		Enumeration<String[]> keys = correlationRecord.keys();
		while(keys.hasMoreElements()){
			String[] key = keys.nextElement();
			ArrayList<int[]> value = correlationRecord.get(key);
			for(int[] solution: value){
				//parameters: String solutionURI, String solutionTarget, int solutionData, double correlation
				//correlation = (# of trials the solution occurred in)/(Total trials)
				heap.add(key[0], key[1], solution[0], (double)solution[1]/trialCount);
			}
		}
	}
	
	
	public void setCorrelations(){
		PredictionSolver.setCorrelationHeap(this.correlationHeap_15_0, this.correlations_15_0);
		PredictionSolver.setCorrelationHeap(this.correlationHeap_30_15, this.correlations_30_15);
		PredictionSolver.setCorrelationHeap(this.correlationHeap_45_30, this.correlations_45_30);
		PredictionSolver.setCorrelationHeap(this.correlationHeap_60_45, this.correlations_60_45);
	}
	
	public static void setTopCorrelationList(ArrayList<HeapNode> list, CorrelationHeap heap, int threshold){
		for(int i = 1; i<=threshold; i++){
			list.add(heap.removeMax());
		}
	}
	
	public void setTopCorrelationLists(){
		PredictionSolver.setTopCorrelationList(topCorrelations_15_0, correlationHeap_15_0, CORRELATION_THRESHOLD_15_0);
		PredictionSolver.setTopCorrelationList(topCorrelations_30_15, correlationHeap_30_15, CORRELATION_THRESHOLD_30_15);
		PredictionSolver.setTopCorrelationList(topCorrelations_45_30, correlationHeap_45_30, CORRELATION_THRESHOLD_45_30);
		PredictionSolver.setTopCorrelationList(topCorrelations_60_45, correlationHeap_60_45, CORRELATION_THRESHOLD_60_45);
	}
	
	/**
	 * Resets the temp lists so they contain the top correlations from the heap again. Every element from the 
	 * topCorrelationList (which doesn't change) is added to temp list after the temp list has been emptied.
	 */
	public void resetTempLists(){
		this.tempList_15_0 = new ArrayList<HeapNode>();
		this.tempList_30_15 = new ArrayList<HeapNode>();
		this.tempList_45_30 = new ArrayList<HeapNode>();
		this.tempList_60_45 = new ArrayList<HeapNode>();
		
		for(HeapNode n: this.topCorrelations_15_0){
			this.tempList_15_0.add(n);
		}
		for(HeapNode n: this.topCorrelations_30_15){
			this.tempList_30_15.add(n);
		}
		for(HeapNode n: this.topCorrelations_45_30){
			this.tempList_45_30.add(n);
		}
		for(HeapNode n: this.topCorrelations_60_45){
			this.tempList_60_45.add(n);
		}
	}
	
	/**
	 * Makes a solution data request to the distributor for all solutions within the begin and end time inputed
	 * 
	 * @param long beginTime
	 * @param long endTime
	 */
	public void makeHistoricDataRequest(long beginTime, long endTime){
		DataTypeRequestMessage request = new DataTypeRequestMessage();
		
		//Request live data
		request.setBeginTime(beginTime);
		request.setEndTime(endTime);

		org.grailrtls.distributor.client.messages.DataTypeSpecificationMessage.DataType[] types = 
				this.specificationMessage.getTypeSpecifications();
		ArrayList<Integer> aliases = new ArrayList<Integer>();

		for (org.grailrtls.distributor.client.messages.DataTypeSpecificationMessage.DataType type : types) {
			if(this.requestedSolutions.contains((String)type.getUriName())){
				aliases.add(type.getTypeAlias());
			}
			else{
				for(String requestedSolutionURI: this.requestedSolutions){
					if(requestedSolutionURI.contains(type.getUriName())){
						aliases.add(type.getTypeAlias());
					}
				}
			}
		}

		if(!aliases.isEmpty()){
			int[] temp = new int[aliases.size()];
			int j = 0;
			for(Integer i: aliases){
				temp[j] = i;
				j++;
			}
			request.setTypeAliases(temp);
		}

		if (request.getTypeAliases() == null) {
			log.error("FAILED TO GENERATE Historic Data Request MESSAGE");
			System.exit(1);
		}
		
		this.clientDistributorInterface.getSession().write(request);
	}
	
	
	/**
     *  Implements a simple Stop-Watch/Timer type class based on Java wall-clock time.
     *
     *  RUNNING() == true  <==>  start() called with no corresponding call to stop()
     *  All times are given in units of milliseconds.
     */
    private class Timer {
        private boolean running;
        private long tStart;
        private long tFinish;
        private long tAccum; // total time
        
      public Timer(){
          reset(); //Initializes the timer to 0 ms.
      }
 
      /**
       *  Starts the timer.  Accumulates time across multiple calls to start.
       */
      public void start(){
          running = true;
          tStart = System.currentTimeMillis();
          tFinish = tStart;
      }
   
      /**
       *  Stops the timer. returns the time elapsed since the last matching call to start(), or
       *  0 if no such matching call was made.
       */
      @SuppressWarnings("unused")
    public long stop(){

          tFinish = System.currentTimeMillis();

          if(running){
              running = false;
              long diff = (tFinish - tStart);
              tAccum += diff;
              return diff;
          }
          
          return 0;
      }

      /**
       *  if RUNNING()   ==>  returns the time since last call to start()
       *  if !RUNNING()  ==>  returns total elapsed time
       */
      public long elapsed(){
          if(running)
              return System.currentTimeMillis() - tStart;
        
          return tAccum;
      }
 
      /**
       *  Stops timing, if currently RUNNING(); resets accumulated time to 0.
       */
      public void reset(){
          running = false;
          tStart = tFinish = 0;
          tAccum = 0;
      }
    }

}
