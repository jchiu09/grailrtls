/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Sumedh Sawant
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.solver.fridge_door;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.grailrtls.libsolver.SolverAggregatorInterface;
import org.grailrtls.libsolver.protocol.messages.SampleMessage;
import org.grailrtls.libworldmodel.solver.SolverWorldModelInterface;
import org.grailrtls.libworldmodel.solver.listeners.DataListener;
import org.grailrtls.libworldmodel.solver.protocol.messages.DataTransferMessage.Solution;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Solver that gives data about refrigerator door usage. Solutions tell you if
 * the door is open or closed.
 * 
 * Utilizes a Histogram and looks for significant changes in the median.
 * 
 * @author Sumedh Sawant
 * 
 */
public class FridgeDoorHistSolver extends FridgeDoorUsageSolver {

	private static final Logger log = LoggerFactory
			.getLogger(FridgeDoorHistSolver.class);

	private static final String SOLVER_ORIGIN_STRING = "Sumedh.Sawant";

	protected float minRssi = -110f;

	protected float maxRssi = 0f;

	protected float histogramBucketSize = 5f;

	protected int maxHistogramSize = 200;

	protected int modeIndex = 0;

	protected int medianIndex = -1;

	private boolean starting = true;

	protected ConcurrentLinkedQueue<Integer> bucketValueList = new ConcurrentLinkedQueue<Integer>();

	// histogram.get(I) == (MIN_RANGE + histogramBucketSize*i, MIN_RANGE +
	// histogramBucketSize*(i+1))
	protected int[] histogram = null;

	public FridgeDoorHistSolver(String aggHost, int aggPort, String distHost,
			int distPort) {
		super(aggHost, aggPort, distHost, distPort);
	}

	public void sampleReceived(SolverAggregatorInterface aggregator,
			SampleMessage sample) {
		float rssi = sample.getRssi();
		if (rssi > maxRssi || rssi < minRssi) {
			log.warn("RSSI value out of range: {}", rssi);
			return;
		}

		this.updateHistogram(rssi);

		// String histogramString = this.prettyPrintHistogram();

		// log.info("\n{}", histogramString);
	}

	protected String printHistogram() {
		if (this.histogram == null)
			return null;

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < this.histogram.length; ++i) {
			sb.append("[")
					.append(String.format("%.1f-%.1f", i
							* this.histogramBucketSize + this.minRssi, (i + 1)
							* this.histogramBucketSize + this.minRssi))
					.append("]: ").append(this.histogram[i]).append('\n');
		}
		return sb.toString();
	}

	protected String prettyPrintHistogram() {
		if (this.histogram == null)
			return null;

		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < this.histogram.length; ++i) {
			sb.append("[")
					.append(String.format("%.1f-%.1f", i
							* this.histogramBucketSize + this.minRssi, (i + 1)
							* this.histogramBucketSize + this.minRssi))
					.append("]: ");
			int count = this.histogram[i];
			while (count >= 5) {
				sb.append("#");
				count -= 5;
			}
			sb.append('\n');
		}

		return sb.toString();
	}

	protected Integer updateHistogram(float rssi) {
		float rssiDiff = rssi - minRssi;

		log.debug("RSSI Diff: {}", rssiDiff);

		// Force to integer truncates any fractional component
		int index = (int) (rssiDiff / histogramBucketSize);

		log.debug("Histogram index: {}/{}", index, this.histogram.length);

		synchronized (this.histogram) {
			++this.histogram[index];
		}
		this.bucketValueList.offer(index);

		if (this.bucketValueList.size() > this.maxHistogramSize) {
			synchronized (this.histogram) {
				while (this.bucketValueList.size() > this.maxHistogramSize) {
					int someIndex = this.bucketValueList.poll();
					--this.histogram[someIndex];
				}
			}
		}

		int mode = 0;
		int modeIndex = 0;
		int medianCount = 0;
		int medianIndex = -1;
		for (int i = 0; i < this.histogram.length; ++i) {
			medianCount += this.histogram[i];
			if (medianIndex < 0 && medianCount > this.maxHistogramSize / 2) {
				medianIndex = i;
			}

			if (this.histogram[i] > mode) {
				modeIndex = i;
				mode = this.histogram[i];
			}
		}

		log.debug("Found a median: {}", medianIndex);
		if (medianIndex > this.medianIndex + 2) {
			if (this.medianIndex >= 0) {
				log.info("Median Guess: DOOR IS OPEN");
				this.sendSolution(new byte[] { 0 });
			} else
				log.info("Updating median for the first time.");
			this.medianIndex = medianIndex;
		} else if (medianIndex < this.medianIndex - 2) {
			if (this.medianIndex >= 0) {
				log.info("Median Guess: DOOR IS CLOSED");
				if (starting) {
					starting = false;
				} else {
					this.sendSolution(new byte[] { 1 });
				}
			} else
				log.info("Updating median for the first time.");
			this.medianIndex = medianIndex;
		}

		log.debug("Mode: {}@{}", mode, modeIndex);

		if (modeIndex > this.modeIndex + 1) {
			log.debug("Mode guess: DOOR IS OPEN");
			this.modeIndex = modeIndex;
		} else if (modeIndex < this.modeIndex - 1) {
			log.debug("Mode guess: DOOR IS CLOSED");
			this.modeIndex = modeIndex;
		}

		return count;
	}

	/**
	 * Prints a simple message about the parameters for this program.
	 */
	protected static void printUsageInfo() {
		System.out
				.println("Requires five parameters: <Aggregator host> <Aggregator port> <Distributor host> <Distributor port> <Bucket size>.");
		System.out.println("Once running, type \"exit\" to exit.");
	}

	public static void main(String[] args) {
		if (args.length < 5) {
			printUsageInfo();
			System.exit(1);
		}
		int aggPort = Integer.parseInt(args[1]);
		int distPort = Integer.parseInt(args[3]);
		float bucketSize = Float.parseFloat(args[4]);

		FridgeDoorHistSolver solver = new FridgeDoorHistSolver(args[0],
				aggPort, args[2], distPort);

		solver.setHistogramBucketSize(bucketSize);

		solver.start();
	}

	public float getMinRssi() {
		return minRssi;
	}

	public void setMinRssi(float minRssi) {
		this.minRssi = minRssi;
	}

	public float getMaxRssi() {
		return maxRssi;
	}

	public void setMaxRssi(float maxRssi) {
		this.maxRssi = maxRssi;
	}

	public float getHistogramBucketSize() {
		return histogramBucketSize;
	}

	public void setHistogramBucketSize(float histogramBucketSize) {
		this.histogramBucketSize = histogramBucketSize;

		int newSize = (int) Math.ceil((this.maxRssi - this.minRssi)
				/ this.histogramBucketSize);
		log.debug("Setting bucket size to: {} ({} buckets)",
				histogramBucketSize, newSize);
		if (this.histogram == null) {
			this.histogram = new int[newSize];
		}
		synchronized (this.histogram) {
			if (this.histogram.length != newSize) {
				this.histogram = new int[newSize];
				log.info("Creating new histogram list of size {}.",
						this.histogram.length);
			}
			Arrays.fill(this.histogram, 0);
		}
	}

	/**
	 * Generates and sends a solution.
	 * 
	 * @param byte[] solutionData
	 * @return generated solutions, or {@code null} if none were generated.
	 */
	private void sendSolution(byte[] solutionData) {
		Solution solution = new Solution();
		solution.setTargetName("winlab.refrigerator.kitchen");
		solution.setAttributeName(FridgeDoorUsageSolver.SOLUTION_URI_NAME);
		solution.setTime(System.currentTimeMillis());

		solution.setData(solutionData);

		ArrayList<Solution> returnedList = new ArrayList<Solution>();
		returnedList.add(solution);

		// Send the solution to the distributor
		if (!this.solverWM.sendSolutions(
				returnedList)) {
			log.info("Unable to send solution to distributor: {}", solution);
		}
		log.info("SOLUTION SENT");
	}

	@Override
	public void typeSpecificationsSent(SolverWorldModelInterface worldModel, TypeAnnounceMessage message) {
		this.canSendSolutions = true;
	}

}
