/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Sumedh Sawant
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package org.grailrtls.solver.fridge_door;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import org.grailrtls.libworldmodel.client.ClientWorldModelInterface;
import org.grailrtls.libworldmodel.client.listeners.ConnectionListener;
import org.grailrtls.libworldmodel.client.listeners.DataListener;
import org.grailrtls.libworldmodel.client.protocol.messages.AbstractRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.AttributeAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.AttributeAliasMessage.AttributeAlias;
import org.grailrtls.libworldmodel.client.protocol.messages.DataResponseMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginPreferenceMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.StreamRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.URISearchResponseMessage;
import org.grailrtls.libworldmodel.solver.SolverWorldModelInterface;
import org.grailrtls.libworldmodel.solver.protocol.messages.DataTransferMessage.Solution;
import org.grailrtls.libworldmodel.solver.protocol.messages.StartTransientMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.StopTransientMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage.TypeSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Solver that gives data about refrigerator door being ajar. Solutions tell you
 * start and end time of when the door was ajar.
 * 
 * When door has been open without closing for 20 seconds, it is declared ajar.
 * Ajar solutions continue to be submitted every 20 seconds until the door is
 * closed.
 * 
 * First time the door is declared ajar, the solver sends a SMS (text message)
 * to select recipients, notifying them that their fridge door is open and
 * should be closed. The solver also sends SMS messages 5 minutes after this
 * first initial SMS, and every 10 minutes after that so the recipient's phones
 * are not spammed.
 * 
 * The email account used to send SMS messages is
 * "winlab.refrigerator.kitchen@gmail.com"
 * 
 * 
 * "FridgeDoorHistSolver.java" MUST BE RUNNING FOR THIS SOLVER TO WORK.
 * 
 * Utilizes a Histogram and looks for significant changes in the median.
 * 
 * @author Sumedh Sawant
 * 
 */
public class FridgeDoorAjarSolver extends Thread implements DataListener,
        org.grailrtls.libworldmodel.solver.listeners.DataListener,
        ConnectionListener,
        org.grailrtls.libworldmodel.solver.listeners.ConnectionListener
{

    /**
     * Logging facility for this class.
     */
    private static final Logger log = LoggerFactory
            .getLogger(FridgeDoorAjarSolver.class);

    /**
     * Keep track of the solution URI name.
     */
    private static final String SOLUTION_URI_NAME = "door ajar";

    /**
     * Region URI.
     */
    private static final String SOLUTION_REGION_URI = "Winlab";

    private static final String SOLVER_ORIGIN_STRING = "Sumedh.Sawant";

    /**
     * Used to get user input.
     */
    private BufferedReader userInput = new BufferedReader(
            new InputStreamReader(System.in));

    /**
     * Flag to keep running until the connection is closed.
     */
    private boolean keepRunning = true;

    /**
     * Interface to the distributor as a client, takes care of all the basic
     * protocol stuff.
     */
    private ClientWorldModelInterface clientWM = new ClientWorldModelInterface();

    /**
     * Interface to the distributor for publishing solutions. Takes care of all
     * the basic protocol stuff.
     */
    private SolverWorldModelInterface solverWM = new SolverWorldModelInterface();

    /**
     * Indicates whether the solution type specification message has been sent
     * to the distributor, and therefore that we can write solutions.
     */
    @SuppressWarnings("unused")
    private boolean canSendSolutions = false;

    private boolean open = false;
    private long startTime = 0l;
    private Timer timer = new Timer();
    private final static long DOOR_AJAR_TIME = 20000l;
    private long ajarCount = 1;
    private boolean seenOne = false;

    /**
     * Creates a new FridgeDoorAjarSolver and starts it.
     * 
     * @param args
     *            clientDistributor host, clientDistributor port,
     *            solverDistributor host, and solverDistributor port, in that
     *            order.
     */
    public static void main(String[] args)
    {
        if (args.length < 4)
        {
            printUsageInfo();
            System.exit(1);
        }

        int aggPort = Integer.parseInt(args[1]);
        int distPort = Integer.parseInt(args[3]);

        FridgeDoorAjarSolver solver = new FridgeDoorAjarSolver(args[0],
                aggPort, args[2], distPort);
        solver.start();
    }

    /**
     * Prints a simple message about the parameters for this program.
     */
    protected static void printUsageInfo()
    {
        System.out
                .println("Requires four parameters: <ClientDistributor host> <ClientDistributor port> <SolverDistributor host> <SolverDistributor port>.");
        System.out.println("Once running, type \"exit\" to exit.");
    }

    /**
     * CONSTRUCTOR
     * 
     * Creates a new "FridgeDoorAjarSolver" that connects to the distributor at
     * the specified host name/IP addresses and port numbers. (As a client AND
     * as a solver)
     * 
     * @param clientDistHost
     *            the host-name or IP address of the distributor.
     * @param clientDistPort
     *            the port-number the distributor is listening for clients on.
     * @param solverDistHost
     *            the host-name or IP address of the distributor.
     * @param solverDistPort
     *            the port-number the distributor is listening for solvers on.
     */
    public FridgeDoorAjarSolver(String clientDistHost, int clientDistPort,
            String solverDistHost, int solverDistPort)
    {
        this.clientWM.setHost(clientDistHost);
        this.clientWM.setPort(clientDistPort);

        this.solverWM.setHost(solverDistHost);
        this.solverWM.setPort(solverDistPort);
    }

    /**
     * Connects to the distributor as a client and as a solver. Checks for user
     * input every 100ms, and exits if either of the connections are ended, or
     * the user types "exit".
     */
    @Override
    public void run()
    {
        /*
         * "Client Distributor" interface set-up.
         */

        // Add this class as a dataTransferMessage listener so that the data can
        // be used to generate solutions.
        this.clientWM.addDataListener(this);

        // Add this class as a connection listener to know when the connection
        // closes and also when the type specification message has been sent.
        this.clientWM.addConnectionListener(this);

        // Disconnect if we have any problems
        this.clientWM.setDisconnectOnException(true);

        // Don't try to reconnect after a disconnect
        this.clientWM.setStayConnected(true);

        // Wait 30 seconds between attempts to connect to the distributor as a
        // client.
        this.clientWM.setConnectionRetryDelay(10000l);

        /*
         * "Solver Distributor" interface set-up.
         */

        // Add the solution type URI name to the distributor interface
        TypeSpecification typeSpec = new TypeSpecification();
        typeSpec.setIsTransient(false);
        typeSpec.setUriName(SOLUTION_URI_NAME);
        this.solverWM.addType(typeSpec);
        this.solverWM.addConnectionListener(this);
        this.solverWM.setOriginString(SOLVER_ORIGIN_STRING);

        // Add this class as a connection listener to know when the connection
        // closes
        // this.solverDistributorInterface.addConnectionListener(this);

        // Set the time between connection attempts to 10s.
        this.solverWM.setConnectionRetryDelay(10000l);

        // Try to stay connected to the distributor.
        this.solverWM.setStayConnected(true);

        // Close the current connection if there is a protocol exception.
        this.solverWM.setDisconnectOnException(true);

        // Connect to the distributor as a solver.
        if (!this.solverWM.doConnectionSetup())
        {
            System.out
                    .println("Unable to establish a connection to the distributor as a solver.");
            System.exit(1);
        }
        System.out.println("Connected to distributor as a solver at "
                + this.solverWM.getHost() + ":"
                + this.solverWM.getPort());

        // Connect to the distributor as a client.
        if (!this.clientWM.doConnectionSetup())
        {
            System.out
                    .println("Unable to establish a connection to the distributor as a client.");
            System.exit(1);
        }
        System.out.println("Connected to distributor as a client at "
                + this.clientWM.getHost() + ":"
                + this.clientWM.getPort());

        // Read the user input into this string
        String command = null;

        // Keep running until either we get disconnected, or the user wants to
        // quit.
        while (this.keepRunning)
        {
            try
            {
                // If the user has something to read in the buffer.
                if (this.userInput.ready())
                {
                    command = this.userInput.readLine();

                    // Allow the solver to shut down gracefully if the user
                    // wants to exit.
                    if ("exit".equalsIgnoreCase(command))
                    {
                        System.out.println("Exiting solver (user).");
                        break;
                    }
                    else
                    {
                        System.out.println("Unknown command \"" + command
                                + "\". Type \"exit\" to exit.");
                    }
                }
            }
            catch (IOException e1)
            {
                System.err.println("Error reading user input.");
                e1.printStackTrace();
            }
            // Sleep for 100ms, since there isn't much else to do.
            try
            {
                Thread.sleep(100l);
                long elapsed = this.timer.elapsed();

                if ((elapsed >= (DOOR_AJAR_TIME * this.ajarCount))
                        && (this.open))
                {
                    log.info("The Door has been AJAR for " + elapsed / 1000
                            + " seconds!");
                    log.info("Sending solution of how long door has been AJAR.");

                    if (ajarCount == 1)
                    {
                        // Send solution again if the door is ajar 2 minutes
                        // after first being declared ajar
                        ajarCount = ajarCount + 6;
                    }
                    else
                    {
                        // Send Solution every 10 minutes periodically is door
                        // is still ajar.
                        ajarCount = ajarCount + 30;
                    }

                    Solution solution = new Solution();
                    solution.setTargetName("winlab.refrigerator.kitchen");

                    solution.setAttributeName(SOLUTION_URI_NAME);
                    solution.setTime(System.currentTimeMillis());
                    // ByteBuffer buff = ByteBuffer.allocate(16);
                    // buff.putLong(this.startTime);
                    // buff.putLong(System.currentTimeMillis());
                    solution.setData(new byte[]
                    { 1 });

                    ArrayList<Solution> returnedList = new ArrayList<Solution>();
                    returnedList.add(solution);

                    if (!this.solverWM.sendSolutions(
                            returnedList))
                    {
                        log.warn(
                                "Unable to send solution to distributor: {}",
                                returnedList);
                    }
                    log.info("SENT AJAR SOLUTION!");
                }

            }
            catch (InterruptedException e)
            {
                // Ignored
            }
        }
        // Stop running
        this.clientWM.doConnectionTearDown();
        this.solverWM.doConnectionTearDown();
        System.exit(0);
    }

    public static String printByteArray(byte[] b)
    {
        if (b.length == 0)
        {
            return null;
        }
        else
        {
            String result = "[ ";
            for (byte byt : b)
            {
                result += byt + " ";
            }
            result += "]";
            return result;
        }
    }

    /**
     * Implements a simple Stop-Watch/Timer type class based on Java wall-clock
     * time.
     * 
     * RUNNING() == true <==> start() called with no corresponding call to
     * stop() All times are given in units of milliseconds.
     */
    private class Timer
    {
        private boolean running;
        private long tStart;
        private long tFinish;
        private long tAccum; // total time

        public Timer()
        {
            reset(); // Initializes the timer to 0 ms.
        }

        /**
         * Starts the timer. Accumulates time across multiple calls to start.
         */
        public void start()
        {
            running = true;
            tStart = System.currentTimeMillis();
            tFinish = tStart;
        }

        /**
         * Stops the timer. returns the time elapsed since the last matching
         * call to start(), or 0 if no such matching call was made.
         */
        @SuppressWarnings("unused")
        public long stop()
        {

            tFinish = System.currentTimeMillis();

            if (running)
            {
                running = false;
                long diff = (tFinish - tStart);
                tAccum += diff;
                return diff;
            }
            return 0;
        }

        /**
         * if RUNNING() ==> returns the time since last call to start() if
         * !RUNNING() ==> returns total elapsed time
         */
        public long elapsed()
        {
            if (running)
                return System.currentTimeMillis() - tStart;

            return tAccum;
        }

        /**
         * Stops timing, if currently RUNNING(); resets accumulated time to 0.
         */
        public void reset()
        {
            running = false;
            tStart = tFinish = 0;
            tAccum = 0;
        }

    }

    @Override
    public void connectionInterrupted(ClientWorldModelInterface worldModel)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void connectionEnded(ClientWorldModelInterface worldModel)
    {
        // Stop running after the connection is finally closed.
        System.out.println("Connection closed.");
        this.keepRunning = false;
        this.interrupt();
    }

    @Override
    public void connectionEstablished(ClientWorldModelInterface worldModel)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void requestCompleted(ClientWorldModelInterface worldModel,
            AbstractRequestMessage message)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void dataResponseReceived(ClientWorldModelInterface worldModel,
            DataResponseMessage message)
    {
        log.debug("Data received: " + message.getUri());

        if (!message.getUri().equalsIgnoreCase("winlab.refrigerator.kitchen"))
        {
            return;
        }
        if (message.getAttributes()[0].getData()[0] == (byte) 0)
        {
            log.info("Door was OPENED.");
            this.open = true;
            this.seenOne = false;
            this.startTime = System.currentTimeMillis();
            this.timer.start();
        }
        else
        {
            if (seenOne)
            {
                return;
            }
            log.info("Door was CLOSED.");

            Solution solution = new Solution();
            solution.setTargetName("winlab.refrigerator.kitchen");
            solution.setAttributeName(SOLUTION_URI_NAME);
            solution.setTime(System.currentTimeMillis());

            // ByteBuffer buff = ByteBuffer.allocate(16);
            // buff.putLong(this.startTime);
            // buff.putLong(System.currentTimeMillis());
            solution.setData(new byte[]
            { 0 });

            ArrayList<Solution> returnedList = new ArrayList<Solution>();
            returnedList.add(solution);

            if (!this.solverWM.sendSolutions(
                    returnedList))
            {
                log.warn("Unable to send solution to distributor: {}",
                        returnedList);
            }
            log.info("SENT AJAR SOLUTION!");

            this.ajarCount = 1;
            this.seenOne = true;
            open = false;
            this.timer.reset();
            this.startTime = 0l;
        }
    }

    @Override
    public void uriSearchResponseReceived(
            ClientWorldModelInterface worldModel,
            URISearchResponseMessage message)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void attributeAliasesReceived(
            ClientWorldModelInterface worldModel,
            AttributeAliasMessage message)
    {
        StreamRequestMessage request = new StreamRequestMessage();
        request.setBeginTimestamp(System.currentTimeMillis());
        request.setUpdateInterval(0l);

        AttributeAlias[] types = message.getAliases();
        int alias = -1;

        for (AttributeAlias type : types)
        {
            if ("closed".equalsIgnoreCase(type.aliasName))
            {
                alias = type.aliasNumber;
            }
        }

        if ((alias != -1))
        {
            request.setQueryAttributes(new String[]
            { "closed" });
            request.setQueryURI(".*");
        }

        if (request.getQueryAttributes() == null)
        {
            log.error("FAILED TO GENERATE DATA REQUEST MESSAGE");
            System.exit(1);
        }

        this.clientWM.sendMessage(request);
    }

    @Override
    public void originAliasesReceived(ClientWorldModelInterface worldModel,
            OriginAliasMessage message)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void connectionInterrupted(SolverWorldModelInterface worldModel)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void connectionEnded(SolverWorldModelInterface worldModel)
    {
        // Stop running after the connection is finally closed.
        System.out.println("Connection closed.");
        this.keepRunning = false;
        this.interrupt();
    }

    @Override
    public void connectionEstablished(SolverWorldModelInterface worldModel)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void startTransientReceived(SolverWorldModelInterface worldModel,
            StartTransientMessage message)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void stopTransientReceived(SolverWorldModelInterface worldModel,
            StopTransientMessage message)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void typeSpecificationsSent(SolverWorldModelInterface worldModel,
            TypeAnnounceMessage message)
    {
        this.canSendSolutions = true;
    }

	@Override
	public void originPreferenceSent(ClientWorldModelInterface worldModel,
			OriginPreferenceMessage message) {
		// TODO Auto-generated method stub
		
	}

}
