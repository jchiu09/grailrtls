/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Sumedh Sawant
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.solver.fridge_door;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Hashtable;

import org.grailrtls.libsolver.SolverAggregatorInterface;
import org.grailrtls.libsolver.listeners.ConnectionListener;
import org.grailrtls.libsolver.listeners.SampleListener;
import org.grailrtls.libsolver.protocol.messages.SampleMessage;
import org.grailrtls.libsolver.protocol.messages.Transmitter;
import org.grailrtls.libsolver.rules.SubscriptionRequestRule;
import org.grailrtls.libworldmodel.solver.SolverWorldModelInterface;
import org.grailrtls.libworldmodel.solver.listeners.DataListener;
import org.grailrtls.libworldmodel.solver.protocol.messages.DataTransferMessage.Solution;
import org.grailrtls.libworldmodel.solver.protocol.messages.StartTransientMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.StopTransientMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage.TypeSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Solver that gives data about refrigerator door usage.
 * Solutions include start and end time for door opened and closed.
 * 
 * @author Sumedh Sawant
 * 
 */
public class FridgeDoorUsageSolver extends Thread implements SampleListener,
		ConnectionListener, 
		org.grailrtls.libworldmodel.solver.listeners.ConnectionListener,
		DataListener{

	/**
	 * Keep track of the solution URI name.
	 */
	protected static final String SOLUTION_URI_NAME = "closed";

	/**
	 * Region URI for this example.
	 */
	protected static final String SOLUTION_REGION_URI = "Winlab";
	
	private static final Logger log = LoggerFactory
			.getLogger(FridgeDoorUsageSolver.class);

	private SolverAggregatorInterface aggregatorInterface = new SolverAggregatorInterface();
	protected SolverWorldModelInterface solverWM = new SolverWorldModelInterface();
	
	private static final String SOLVER_ORIGIN_STRING = "Sumedh.Sawant";

	private boolean keepRunning = true;
	
	protected boolean canSendSolutions = false;

	private BufferedReader userInput = new BufferedReader(
			new InputStreamReader(System.in));
	
	
	private long startTime = 0l;
	//private long endTime = 0l;
	private boolean starting = false;
	private boolean open = false;
	
	private Hashtable<Integer, Float> closedAverageRSSIs;
	private Hashtable<Integer, Float> closedTestResults;
	private Hashtable<Integer, Float> numSamplesSeen;
	int count = 0;
	private int[] receiverIDs = new int[] {123,125,127,128,129,130,131,132,134,135,137,138,139,140,155,196,197};
	
	
	private int closedCount = 0;
	private int openCount = 0;
	
	
	

	public static void main(String[] args) {
		if (args.length < 4) {
			printUsageInfo();
			System.exit(1);
		}
		int aggPort = Integer.parseInt(args[1]);
		int distPort = Integer.parseInt(args[3]);

		FridgeDoorUsageSolver solver = new FridgeDoorUsageSolver(args[0], aggPort,
				args[2], distPort);
				
		
		solver.start();
	}

	/**
	 * Prints a simple message about the parameters for this program.
	 */
	protected static void printUsageInfo() {
		System.out
				.println("Requires four parameters: <Aggregator host> <Aggregator port> <Distributor host> <Distributor port>.");
		System.out.println("Once running, type \"exit\" to exit.");
	}

	/**
	 * Creates a new {@code TemperatureSolver} that connects to an aggregator
	 * and distributor at the specified host name/IP addresses and port numbers.
	 * 
	 * @param aggHost
	 *            the hostname or IP address of the aggregator.
	 * @param aggPort
	 *            the port number the aggregator is listening for solvers on.
	 */
	public FridgeDoorUsageSolver(String aggHost, int aggPort, String distHost, int distPort) {
		this.aggregatorInterface.setHost(aggHost);
		this.aggregatorInterface.setPort(aggPort);
		
		this.solverWM.setHost(distHost);
		this.solverWM.setPort(distPort);
		
		this.closedAverageRSSIs = new Hashtable<Integer, Float>();
		this.closedTestResults = new Hashtable<Integer, Float>();
		this.numSamplesSeen = new Hashtable<Integer, Float>();
	}

	public void run() {

		/*
		 * Aggregator interface set-up.
		 */

		// Create a subscription rule to limit samples to 1 per 4 seconds for
		// each transmitter/receiver pair (simple), and only receive PIPSqueak
		// samples (both).
		SubscriptionRequestRule[] rules = new SubscriptionRequestRule[1];
		rules[0] = SubscriptionRequestRule.generateGenericRule();
		Transmitter fridgePip = new Transmitter();
		fridgePip.setBaseId(new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0x12});
		fridgePip
				.setMask(new byte[] { (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
						(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
						(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
						(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
						(byte) 0xFF });

		rules[0].setTransmitters(new Transmitter[] {fridgePip});
		rules[0].setUpdateInterval(0l);
		rules[0].setPhysicalLayer((byte) 1);
		this.aggregatorInterface.setRules(rules);

		// Add this class as a sample listener to print them out.
		this.aggregatorInterface.addSampleListener(this);

		// Add this class as a connection listener to know when the connection
		// closes.
		this.aggregatorInterface.addConnectionListener(this);

		// Disconnect if we have any problems
		this.aggregatorInterface.setDisconnectOnException(true);

		// Don't try to reconnect after a disconnect
		this.aggregatorInterface.setStayConnected(true);

		// Wait 30 seconds between attempts to connect to the aggregator.
		this.aggregatorInterface.setConnectionRetryDelay(10000l);

		
		/*
		 * Distributor interface set-up.
		 */

		// Add the solution type URI name to the distributor interface
		TypeSpecification typeSpec = new TypeSpecification();
		typeSpec.setIsTransient(false);
		typeSpec.setUriName(SOLUTION_URI_NAME);
		
		this.solverWM.addType(typeSpec);

		// Add this class as a connection listener to know when the connection
		// closes,
		// and also when the type specification message has been sent.
		this.solverWM.addConnectionListener(this);

		// Set the time between connection attempts to 10s.
		this.solverWM.setConnectionRetryDelay(10000l);

		// Try to stay connected to the distributor.
		this.solverWM.setStayConnected(true);

		// Close the current connection if there is a protocol exception.
		this.solverWM.setDisconnectOnException(true);
		
		this.solverWM.setOriginString(SOLVER_ORIGIN_STRING);

		/*
		 * Connect to the distributor. Note that this is being done before the
		 * aggregator to simplify the sample-handling logic in this example
		 * class.
		 */
		if (!this.solverWM.doConnectionSetup()) {
			System.out
					.println("Unable to establish a connection to the distributor.");
			System.exit(1);
		}
		System.out.println("Connected to distributor at "
				+ this.solverWM.getHost() + ":"
				+ this.solverWM.getPort());
		
		
		// Connect to the aggregator
		if (!this.aggregatorInterface.doConnectionSetup()) {
			System.out
					.println("Unable to establish a connection to the aggregator.");
			System.exit(1);
		}
		System.out.println("Connected to aggregator at "
				+ this.aggregatorInterface.getHost() + ":"
				+ this.aggregatorInterface.getPort());

		// Read the user input into this string
		String command = null;

		// Keep running until either we get disconnected, or the user wants to
		// quit.
		while (this.keepRunning) {
			try {
				// If the user has something to read in the buffer.
				if (this.userInput.ready()) {
					command = this.userInput.readLine();

					// Allow the solver to shut down gracefully if the user
					// wants to exit.
					if ("exit".equalsIgnoreCase(command)) {
						System.out.println("Exiting solver (user).");
						break;
					}
					System.out.println("Unknown command \"" + command
							+ "\". Type \"exit\" to exit.");

				}
			} catch (IOException e1) {
				System.err.println("Error reading user input.");
				e1.printStackTrace();
			}
			// Sleep for 100ms, since there isn't much else to do.
			try {
				Thread.sleep(100l);
			} catch (InterruptedException e) {
				// Ignored
			}
		}
		// Stop running
		this.aggregatorInterface.doConnectionTearDown();
		this.solverWM.doConnectionTearDown();
		System.exit(0);
	}

	/**
	 * Returns an integer representation of a 23-bit PIPSqueak device ID.
	 * 
	 * @param deviceId
	 * @return the 23-bit PIPSqueak device ID as a 32-bit integer
	 */
	private static int getPipId(byte[] deviceId) {
		// PIPSqueak tags only have a 23-bit ID, so we can extract the last 3
		// bytes as an integer
		return ((deviceId[SampleMessage.DEVICE_ID_SIZE - 3] << 16) & 0xFF)
				+ ((deviceId[SampleMessage.DEVICE_ID_SIZE - 2] << 8) & 0xFF)
				+ (deviceId[SampleMessage.DEVICE_ID_SIZE - 1] & 0xFF);
	}

	/**
	 * Convert the byte array to an int.
	 * 
	 * @param b
	 *            The byte array
	 * @return The integer
	 */
	public static int byteArrayToInt(byte[] b) {
		return byteArrayToInt(b, 0);
	}

	/**
	 * Convert the byte array to an int starting from the given offset.
	 * 
	 * @param b
	 *            The byte array
	 * @param offset
	 *            The array offset
	 * @return The integer
	 */
	public static int byteArrayToInt(byte[] b, int offset) {
		int value = 0;
		for (int i = 0; i < 4; i++) {
			int shift = (4 - 1 - i) * 8;
			value += (b[i + offset] & 0x000000FF) << shift;
		}
		return value;
	}

	public static String byteArrayToString(byte[] b) {
		String result = "[ ";
		if (b.length == 0) {
			return "[ ]";
		}
		for (int i = 0; i < b.length; i++) {
			result += b[i] + " ";
		}
		result += "]";
		return result;
	}

	public void sampleReceived(SolverAggregatorInterface aggregator,
			SampleMessage sample) {
		if(sample == null){
			return;
		}
		
		log.info("Sample received {}", sample);
		
		if(!this.open){
			log.debug("Door is CLOSED!");
		}
		else{
			log.debug("Door is OPEN!");
		}
		
		this.simpleCalculateDoorOpen(sample);
	}
	
	
	public void simpleCalculateDoorOpen(SampleMessage sample){
		if(this.open){
			if(sample.getRssi()<-90){
				if(closedCount == 10){
					log.debug("Door is now CLOSED");
					this.open = false;
					closedCount = 0;
					this.sendSolution(this.startTime, System.currentTimeMillis(), sample);
				}
				closedCount++;
			}
		}
		else{
			if(sample.getRssi()>-60){
				if(openCount == 5){
					log.debug("Door is now OPEN");
					this.open = true;
					this.startTime = System.currentTimeMillis();
					openCount = 0;
				}
				
				openCount++;
			}
		}
		
		//log.debug("CALCULATION MADE.");
	}
	
	
	
	public void calculateDoorOpen(SampleMessage sample){
		
		int receiverID = FridgeDoorUsageSolver.getPipId(sample.getReceiverId());
		float RSSIreceived = sample.getRssi();
		
		if (sample.getSensedData() == null){
			//|| (!this.canSendSolutions))
			return;
		}
		
		if(this.starting){
			if(this.numSamplesSeen.get(receiverID) == null){
				this.closedAverageRSSIs.put(receiverID, RSSIreceived);
				this.numSamplesSeen.put(receiverID, (float)1);
			}
			else{
				float newNumSamples = this.numSamplesSeen.get(receiverID) + 1;
				float newRSSI = (this.closedAverageRSSIs.get(receiverID)* (newNumSamples - 1))/newNumSamples;
				this.closedAverageRSSIs.put(receiverID, newRSSI);
				this.numSamplesSeen.put(receiverID, this.numSamplesSeen.get(receiverID) + 1);
			}
			
			count++;
			
			if(this.count ==90){
				this.starting = false;
			}
			return;
		}
		
		if((RSSIreceived > this.closedAverageRSSIs.get(receiverID) + 2) 
				|| (RSSIreceived < this.closedAverageRSSIs.get(receiverID) - 2)){
			this.closedTestResults.put(receiverID, (float)1);
		}
		else{
			this.closedTestResults.put(receiverID, (float)0);
		}
		
		if(!this.open){
			int count = 0;
			
			for(int i=0; i<this.receiverIDs.length; i++){
				if((float)0 == this.closedTestResults.get(receiverID)){
					count++;
				}
			}
			
			if(count >= 9){
				this.open = true;
				log.info("THE FRIDGE DOOR IS NOW OPEN!");
			}
		}
		else if(this.open){
			int count = 0;
			
			for(int i=0; i<this.receiverIDs.length; i++){
				if((float)1 == this.closedTestResults.get(receiverID)){
					count++;
				}
			}
			
			if(count >= 9){
				this.open = false;
				log.info("THE FRIDGE DOOR IS NOW CLOSED!");
			}
		}
		
		log.debug("CALCULATION MADE!");
	}
	
	
	/**
	 * Generates and sends a solution based off the RSSI values.
	 * 
	 * @param sample
	 * @return generated solutions, or {@code null} if none were generated.
	 */
	private void sendSolution(long start, long end, SampleMessage sample) {
		Solution solution = new Solution();
		solution.setTargetName("18");
		solution.setAttributeName(SOLUTION_URI_NAME);
		solution.setTime(System.currentTimeMillis());
		
		ByteBuffer buff = ByteBuffer.allocate(16);
		buff.putLong(start);
		buff.putLong(end);
		
		// "Reset" the start time and end time.
		this.startTime = 0l;
		
		solution.setData(buff.array());

		ArrayList<Solution> returnedList = new ArrayList<Solution>();
		returnedList.add(solution);

		// Send the solution to the distributor
		if(this.canSendSolutions){
			if (!this.solverWM.sendSolutions(
					returnedList)) {
				log.warn("Unable to send solution to distributor: {}", solution);
			}
		}
	}

	
	/**
	 * Shuts down this solver because the aggregator interface has "given up" on
	 * reconnecting.
	 * 
	 * @param aggregator
	 */
	public void connectionEnded(SolverAggregatorInterface aggregator) {
		// Stop running after the connection is finally closed.
		System.out.println("Connection closed to " + aggregator);
		this.keepRunning = false;
		this.interrupt();
	}

	public void connectionEstablished(SolverAggregatorInterface aggregator) {
		// NOT USED

	}

	public void connectionInterrupted(SolverAggregatorInterface aggregator) {
		// NOT USED

	}
	

	@Override
	public void connectionInterrupted(SolverWorldModelInterface worldModel) {
		this.canSendSolutions = false;
	}

	@Override
	public void connectionEnded(SolverWorldModelInterface worldModel) {
		// Stop running after the connection is finally closed.
				System.out.println("Connection closed to " + worldModel);
				this.keepRunning = false;
				this.interrupt();
	}

	@Override
	public void connectionEstablished(SolverWorldModelInterface worldModel) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startTransientReceived(SolverWorldModelInterface worldModel,
			StartTransientMessage message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stopTransientReceived(SolverWorldModelInterface worldModel,
			StopTransientMessage message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void typeSpecificationsSent(SolverWorldModelInterface worldModel,
			TypeAnnounceMessage message) {
		this.canSendSolutions = true;
	}

}
