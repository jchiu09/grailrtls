/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Sumedh Sawant
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.solver.fridge_door;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class Histogram{
	
	private int bucketSize;
	private int maxValue;
	private int minValue;
	private int maxSamples;
	private int[] buckets;
	public Queue<Integer> queue = new LinkedList<Integer>();
	
	public static void main(String[] args){
		Histogram h = new Histogram(5, 100, 1, 10);
		h.addSample(-98);
		h.addSample(-67);
		h.addSample(-100);
		h.addSample(-85);
		h.addSample(-34);
		
		h.addSample(-56);
		h.addSample(-23);
		h.addSample(-9);
		h.addSample(-1);
		h.addSample(-43);
		
		h.addSample(-67);
		System.out.println(h.queue);
		//System.out.println(intArrayToString(h.buckets));
		System.out.println(h.getProbabilityDistribution());
	}
	
	public Histogram(int bucketSize, int maxValue, int minValue, int maxSamples){
		this.bucketSize = bucketSize;
		this.maxValue = maxValue;
		this.minValue = minValue;
		this.maxSamples = maxSamples;
		this.buckets = new int[(maxValue - minValue + 1)/bucketSize];
	}
	
	/*public static String intArrayToString(int[] array){
		String result = "[";
		for(int i: array){
			result += ", " + i;
		}
		return result + "]";
	}*/
	
	/*
	public void setBucketSize(int size){
		this.bucketSize = size;
	}
	
	public void setMaxValue(int max){
		this.maxValue = max;
	}
	
	public void setMinValue(int min){
		this.minValue = min;
	}
	
	public void setMaxSamples(int maxSamples){
		this.maxSamples = maxSamples;
	}*/
	
	public void addSample(float RSSI){
		RSSI = -1 * RSSI;
		if(RSSI>this.maxValue){
			return;
		}
		int bucketIndex = (int) Math.floor((double)((int) (RSSI - this.minValue))/this.bucketSize);
		this.queue.add(bucketIndex);
		if(this.buckets[bucketIndex] == 0){
			this.buckets[bucketIndex] = 1;
		}
		else{
			this.buckets[bucketIndex] = this.buckets[bucketIndex] + 1;
		}
		//Only keeps the most recent "maxSamples" number of samples
		if(this.queue.size() == this.maxSamples + 1){
			int removeIndex = Integer.valueOf(this.queue.poll());
			this.buckets[removeIndex] = this.buckets[removeIndex] - 1;
		}
	}
	
	public ArrayList<Double> getProbabilityDistribution(){
		if(this.queue.size() != this.maxSamples){
			return null;
		}
		ArrayList<Double> result = new ArrayList<Double>();
		for(int bucketContent: this.buckets){
				result.add((double) bucketContent/this.maxSamples);
		}
		return result;
	}
	
	private double getEntropy(){
		if(this.queue.size() != this.maxSamples){
			return -10;
		}
		double result = 0;
		for(Double probability: this.getProbabilityDistribution()){
			double p = (Double.valueOf(probability));
			result += p*(Math.log(p));
		}
		return result;
	}
	
}