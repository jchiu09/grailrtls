/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Sumedh Sawant
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.solver.fridge_door;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Queue;

import org.grailrtls.libsolver.SolverAggregatorInterface;
import org.grailrtls.libsolver.listeners.ConnectionListener;
import org.grailrtls.libsolver.listeners.SampleListener;
import org.grailrtls.libsolver.protocol.messages.SampleMessage;
import org.grailrtls.libsolver.protocol.messages.Transmitter;
import org.grailrtls.libsolver.rules.SubscriptionRequestRule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RSSIprinter extends Thread implements SampleListener,
	ConnectionListener{
	
	private static final Logger log = LoggerFactory.getLogger(RSSIprinter.class);

	private SolverAggregatorInterface aggregatorInterface = new SolverAggregatorInterface();
	
	private boolean keepRunning = true;
	
	private BufferedReader userInput = new BufferedReader(
			new InputStreamReader(System.in));
	
	private int baseId;
	
	private String tagName;
	
	private int[] recieverIds = new int[]{123,125,127,128,129,130,131,132,134,135,137,138,139,140,155,196,197};
	
	private Hashtable<Integer, Histogram> histograms = new Hashtable<Integer, Histogram>();
	
	public static void main(String[] args) {
		final RSSIprinter printer = new RSSIprinter("grail1.winlab.rutgers.edu", 7008, "Chair", 0x278);
		//final CoLocationSolver printer2 = new CoLocationSolver("grail1.winlab.rutgers.edu", 7008, "Mug B", 0x219);
		printer.start();
		//printer2.start();
		/*Thread thread = new Thread() {
		    @Override
		    public void run() {
				while(true){
					try {
						ArrayList<Double> JSDs = new ArrayList<Double>();
						Hashtable<Integer, Histogram> temp1 = printer.getHistograms();
						Hashtable<Integer, Histogram> temp2 = printer2.getHistograms();
						Enumeration<Integer> keys = temp1.keys();
						Enumeration<Integer> keys2 = temp2.keys();
						while(keys.hasMoreElements() && keys2.hasMoreElements()){
							ArrayList<Double> distribution1 = temp1.get(keys.nextElement()).getProbabilityDistribution();
							ArrayList<Double> distribution2 = temp2.get(keys2.nextElement()).getProbabilityDistribution();
							if(distribution1 == null || distribution2 == null){
								continue;
							}
							double df = CoLocationSolver.jensenShannonDivergence(distribution1, distribution2);
							if(df != 0){
								JSDs.add(df);
							}
						}
						if(JSDs.size() >= 12){
							log.info("The JSDs list is: " + JSDs);
							double rd = 0;
							for(double d: JSDs){
								rd += d;
							}
							rd = (double) rd/JSDs.size();
							log.info("The average of the JSDs is: " + rd);
							System.exit(MAX_PRIORITY);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
		    }
		};
		thread.start();*/
	}
	
	/**
	 * Prints a simple message about the parameters for this program.
	 */
	protected static void printUsageInfo() {
		System.out.println("Requires four parameters: <Aggregator host> <Aggregator port> <String tagName> <byte baseId>");
		System.out.println("Once running, type \"exit\" to exit.");
	}

	/**
	 * Creates a new {@code DoorUseIdentifierSolver} that connects to an aggregator
	 * and distributor at the specified host name/IP addresses and port numbers.
	 * 
	 * @param aggHost
	 *            the hostname or IP address of the aggregator.
	 * @param aggPort
	 *            the port number the aggregator is listening for solvers on.
	 */
	public RSSIprinter(String aggHost, int aggPort, String tagName, int baseId){
		this.aggregatorInterface.setHost(aggHost);
		this.aggregatorInterface.setPort(aggPort);
		this.baseId = baseId;
		this.tagName = tagName;
		for(int receiver: this.recieverIds){
			this.histograms.put(receiver, new Histogram(3, 99, 1, 10));
		}
	}
	
	private Hashtable<Integer, Histogram> getHistograms(){
		return this.histograms;
	}

	public void run() {

		/*
		 * Aggregator interface set-up.
		 */

		// Create a subscription rule to limit samples to 1 per 4 seconds for
		// each transmitter/receiver pair (simple), and only receive PIPSqueak
		// samples (both).
		SubscriptionRequestRule[] rules = new SubscriptionRequestRule[1];
		rules[0] = SubscriptionRequestRule.generateGenericRule();
		Transmitter pip = new Transmitter();
		pip.setBaseId(new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, (byte) this.baseId});
		pip
				.setMask(new byte[] { (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
						(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
						(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
						(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
						(byte) 0xFF });

		rules[0].setTransmitters(new Transmitter[] {pip});
		rules[0].setUpdateInterval(0l);
		rules[0].setPhysicalLayer((byte) 1);
		this.aggregatorInterface.setRules(rules);

		// Add this class as a sample listener to print them out.
		this.aggregatorInterface.addSampleListener(this);

		// Add this class as a connection listener to know when the connection
		// closes.
		this.aggregatorInterface.addConnectionListener(this);

		// Disconnect if we have any problems
		this.aggregatorInterface.setDisconnectOnException(true);

		// Don't try to reconnect after a disconnect
		this.aggregatorInterface.setStayConnected(true);

		// Wait 30 seconds between attempts to connect to the aggregator.
		this.aggregatorInterface.setConnectionRetryDelay(10000l);
		
		// Connect to the aggregator
		if (!this.aggregatorInterface.doConnectionSetup()) {
			System.out
					.println("Unable to establish a connection to the aggregator.");
			System.exit(1);
		}
		System.out.println("Connected to aggregator at "
				+ this.aggregatorInterface.getHost() + ":"
				+ this.aggregatorInterface.getPort());

		// Read the user input into this string
		String command = null;

		// Keep running until either we get disconnected, or the user wants to
		// quit.
		while (this.keepRunning) {
			try {
				// If the user has something to read in the buffer.
				if (this.userInput.ready()) {
					command = this.userInput.readLine();

					// Allow the solver to shut down gracefully if the user
					// wants to exit.
					if ("exit".equalsIgnoreCase(command)) {
						System.out.println("Exiting solver (user).");
						break;
					}
					System.out.println("Unknown command \"" + command
							+ "\". Type \"exit\" to exit.");

				}
			} catch (IOException e1) {
				System.err.println("Error reading user input.");
				e1.printStackTrace();
			}
			// Sleep for 100ms, since there isn't much else to do.
			try {
				Thread.sleep(100l);
			} catch (InterruptedException e) {
				// Ignored
			}
		}
		// Stop running
		this.aggregatorInterface.doConnectionTearDown();
		System.exit(0);
	}

	

	public void connectionEnded(SolverAggregatorInterface aggregator) {
		// Stop running after the connection is finally closed.
		System.out.println("Connection closed to " + aggregator);
		this.keepRunning = false;
		this.interrupt();
	}

	public void connectionEstablished(SolverAggregatorInterface aggregator) {
		// NOT USED
		
	}

	public void connectionInterrupted(SolverAggregatorInterface aggregator) {
		// NOT USED
		
	}

	public static String byteArrayToString(byte[] b) {
		String result = "[ ";
		if (b.length == 0) {
			return "[ ]";
		}
		for (int i = 0; i < b.length; i++) {
			result += b[i] + " ";
		}
		result += "]";
		return result;
	}
	
	private static int getPipId(byte[] deviceId) {
		// PIPSqueak tags only have a 23-bit ID, so we can extract the last 3
		// bytes as an integer
		return ((deviceId[SampleMessage.DEVICE_ID_SIZE - 3] << 16) & 0xFF)
				+ ((deviceId[SampleMessage.DEVICE_ID_SIZE - 2] << 8) & 0xFF)
				+ (deviceId[SampleMessage.DEVICE_ID_SIZE - 1] & 0xFF);
	}
	
	public void sampleReceived(SolverAggregatorInterface aggregator,
			SampleMessage sample) {
		log.info(this.tagName + ": Sample received by Receiver " + getPipId(sample.getReceiverId()) + " of " + sample.getRssi());
	}
	
	private static double kullbackLeiblerDivergence(ArrayList<Double> firstDistribution, ArrayList<Double> secondDistribution){
		double result = 0;
		for(int i = 0; i < firstDistribution.size(); i++){
			if(firstDistribution.get(i) == 0  || secondDistribution.get(i) == 0){
				continue;
			}
			result += (firstDistribution.get(i)) * (Math.log(firstDistribution.get(i)) - Math.log(secondDistribution.get(i)));
		}
		return result;
	}
	
	private static double jensenShannonDivergence(ArrayList<Double> firstDistribution, ArrayList<Double> secondDistribution){
		double result = 0;
		ArrayList<Double> M = new ArrayList<Double>();
		for(int i = 0; i<firstDistribution.size(); i++){
			double p = Double.valueOf(firstDistribution.get(i));
			double q = Double.valueOf(secondDistribution.get(i));
			M.add((double)0.5*(p + q));
		}
		double kld1 = kullbackLeiblerDivergence(firstDistribution, M);
		double kld2 = kullbackLeiblerDivergence(secondDistribution, M);
		result = (0.5) * (kld1 + kld2);	
		return result;
	}
	
	private class Histogram{
		
		private int bucketSize;
		private int maxValue;
		private int minValue;
		private int maxSamples;
		private int[] buckets;
		private Queue<Integer> queue = new LinkedList<Integer>();
		
		public Histogram(int bucketSize, int maxValue, int minValue, int maxSamples){
			this.bucketSize = bucketSize;
			this.maxValue = maxValue;
			this.minValue = minValue;
			this.maxSamples = maxSamples;
			this.buckets = new int[(maxValue - minValue + 1)/bucketSize];
		}
		
		/*public static String intArrayToString(int[] array){
			String result = "[";
			for(int i: array){
				result += ", " + i;
			}
			return result + "]";
		}*/
		
		/*
		public void setBucketSize(int size){
			this.bucketSize = size;
		}
		
		public void setMaxValue(int max){
			this.maxValue = max;
		}
		
		public void setMinValue(int min){
			this.minValue = min;
		}
		
		public void setMaxSamples(int maxSamples){
			this.maxSamples = maxSamples;
		}*/
		
		public void addSample(float RSSI){
			RSSI = -1 * RSSI;
			if(RSSI>this.maxValue){
				return;
			}
			int bucketIndex = (int) Math.floor((double)((int) (RSSI - this.minValue))/this.bucketSize);
			this.queue.add(bucketIndex);
			if(this.buckets[bucketIndex] == 0){
				this.buckets[bucketIndex] = 1;
			}
			else{
				this.buckets[bucketIndex] = this.buckets[bucketIndex] + 1;
			}
			//Only keeps the most recent "maxSamples" number of samples
			if(this.queue.size() == this.maxSamples + 1){
				int removeIndex = Integer.valueOf(this.queue.poll());
				this.buckets[removeIndex] = this.buckets[removeIndex] - 1;
			}
		}
		
		public ArrayList<Double> getProbabilityDistribution(){
			if(this.queue.size() != this.maxSamples){
				return null;
			}
			ArrayList<Double> result = new ArrayList<Double>();
			for(int bucketContent: this.buckets){
					result.add((double) bucketContent/this.maxSamples);
			}
			return result;
		}
		
		private double getEntropy(){
			if(this.queue.size() != this.maxSamples){
				return -10;
			}
			double result = 0;
			for(Double probability: this.getProbabilityDistribution()){
				double p = (Double.valueOf(probability));
				result += p*(Math.log(p));
			}
			return result;
		}
	}
}
