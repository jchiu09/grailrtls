/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Sumedh Sawant
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.solver.fridge_door;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import org.grailrtls.libworldmodel.client.ClientWorldModelInterface;
import org.grailrtls.libworldmodel.client.listeners.ConnectionListener;
import org.grailrtls.libworldmodel.client.listeners.DataListener;
import org.grailrtls.libworldmodel.client.protocol.messages.AbstractRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.Attribute;
import org.grailrtls.libworldmodel.client.protocol.messages.AttributeAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.DataResponseMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginPreferenceMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.SnapshotRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.StreamRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.URISearchResponseMessage;
import org.grailrtls.libworldmodel.solver.SolverWorldModelInterface;
import org.grailrtls.libworldmodel.solver.protocol.messages.DataTransferMessage.Solution;
import org.grailrtls.libworldmodel.solver.protocol.messages.StartTransientMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.StopTransientMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage.TypeSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProjectorPredictionSolver extends Thread implements DataListener,
		org.grailrtls.libworldmodel.solver.listeners.DataListener,
		ConnectionListener,
		org.grailrtls.libworldmodel.solver.listeners.ConnectionListener {

	/**
	 * Logging facility for this class.
	 */
	private static final Logger log = LoggerFactory
			.getLogger(ProjectorPredictionSolver.class);

	/**
	 * Keep track of the solution URI name.
	 */
	private static final String SOLUTION_URI_NAME = "prediction.on";

	/**
	 * Region URI.
	 */
	private static final String SOLUTION_REGION_URI = "Winlab";

	private static final String SOLVER_ORIGIN_STRING = "Sumedh.Sawant";

	/**
	 * Used to get user input.
	 */
	private BufferedReader userInput = new BufferedReader(
			new InputStreamReader(System.in));

	/**
	 * Flag to keep running until the connection is closed.
	 */
	private boolean keepRunning = true;

	/**
	 * Interface to the distributor as a client, takes care of all the basic
	 * protocol stuff.
	 */
	private ClientWorldModelInterface clientWM = new ClientWorldModelInterface();

	/**
	 * Interface to the distributor for publishing solutions. Takes care of all
	 * the basic protocol stuff.
	 */
	private SolverWorldModelInterface solverWM = new SolverWorldModelInterface();

	/**
	 * Indicates whether the solution type specification message has been sent
	 * to the distributor, and therefore that we can write solutions.
	 */
	private boolean canSendSolutions = false;

	// This stores a string(target name) and a 2 element integer array [# of
	// times happened, total #]
	private Hashtable<String, int[]> probabilities = new Hashtable<String, int[]>();

	private Hashtable<String, int[]> currentEventCorrelations = new Hashtable<String, int[]>();

	// This is used to store the "nth" highest probabilty correlated events to
	// the projector being on.
	private ArrayList<String> highestProbabilities = new ArrayList<String>();
	private ArrayList<String> recentProbabilities = new ArrayList<String>();

	private final static int NUMBER_CORRELATED_EVENTS_USED = 3;
	private final static int NUMBER_OF_PROJECTOR_TIMES = 1;

	private boolean lookingForCorrelations = true;
	private boolean recentlySeenOff = false;
	private boolean gettingProjectorTimes = true;
	private boolean needHighProbabilityList = true;

	private TypeAnnounceMessage specificationMessage;

	private ArrayList<Long> projectorTimes = new ArrayList<Long>();

	/**
	 * Creates a new ProjectorPredictionSolver and starts it.
	 * 
	 * @param args
	 *            clientDistributor host, clientDistributor port,
	 *            solverDistributor host, and solverDistributor port, in that
	 *            order.
	 */
	public static void main(String[] args) {
		if (args.length < 4) {
			printUsageInfo();
			System.exit(1);
		}

		int aggPort = Integer.parseInt(args[1]);
		int distPort = Integer.parseInt(args[3]);

		ProjectorPredictionSolver solver = new ProjectorPredictionSolver(
				args[0], aggPort, args[2], distPort);
		solver.start();
	}

	/**
	 * Prints a simple message about the parameters for this program.
	 */
	protected static void printUsageInfo() {
		System.out
				.println("Requires four parameters: <ClientDistributor host> <ClientDistributor port> <SolverDistributor host> <SolverDistributor port>.");
		System.out.println("Once running, type \"exit\" to exit.");
	}

	/**
	 * CONSTRUCTOR
	 * 
	 * Creates a new "ProjectorPredictionSolver" that connects to the
	 * distributor at the specified host name/IP addresses and port numbers. (As
	 * a client AND as a solver)
	 * 
	 * @param clientDistHost
	 *            the host-name or IP address of the distributor.
	 * @param clientDistPort
	 *            the port-number the distributor is listening for clients on.
	 * @param solverDistHost
	 *            the host-name or IP address of the distributor.
	 * @param solverDistPort
	 *            the port-number the distributor is listening for solvers on.
	 */
	public ProjectorPredictionSolver(String clientDistHost, int clientDistPort,
			String solverDistHost, int solverDistPort) {
		this.clientWM.setHost(clientDistHost);
		this.clientWM.setPort(clientDistPort);

		this.solverWM.setHost(solverDistHost);
		this.solverWM.setPort(solverDistPort);
	}

	/**
	 * Connects to the distributor as a client and as a solver. Checks for user
	 * input every 100ms, and exits if either of the connections are ended, or
	 * the user types "exit".
	 */
	@Override
	public void run() {
		/*
		 * "Client Distributor" interface set-up.
		 */

		// Add this class as a dataTransferMessage listener so that the data can
		// be used to generate solutions.
		this.clientWM.addDataListener(this);

		// Add this class as a connection listener to know when the connection
		// closes and also when the type specification message has been sent.
		this.clientWM.addConnectionListener(this);

		// Disconnect if we have any problems
		this.clientWM.setDisconnectOnException(true);

		// Don't try to reconnect after a disconnect
		this.clientWM.setStayConnected(true);

		// Wait 30 seconds between attempts to connect to the distributor as a
		// client.
		this.clientWM.setConnectionRetryDelay(10000l);

		/*
		 * "Solver Distributor" interface set-up.
		 */

		// Add the solution type URI name to the distributor interface
		TypeSpecification solutionType = new TypeSpecification();
		solutionType.setIsTransient(false);
		solutionType.setUriName(SOLUTION_URI_NAME);
		this.solverWM.addType(solutionType);
		this.solverWM.addConnectionListener(this);

		// Add this class as a connection listener to know when the connection
		// closes
		// this.solverDistributorInterface.addConnectionListener(this);

		// Set the time between connection attempts to 10s.
		this.solverWM.setConnectionRetryDelay(10000l);

		// Try to stay connected to the distributor.
		this.solverWM.setStayConnected(true);

		// Close the current connection if there is a protocol exception.
		this.solverWM.setDisconnectOnException(true);
		
		this.solverWM.setOriginString(SOLVER_ORIGIN_STRING);

		// Connect to the distributor as a solver.
		if (!this.solverWM.doConnectionSetup()) {
			System.out
					.println("Unable to establish a connection to the distributor as a solver.");
			System.exit(1);
		}
		System.out.println("Connected to distributor as a solver at "
				+ this.solverWM.getHost() + ":"
				+ this.solverWM.getPort());

		// Connect to the distributor as a client.
		if (!this.clientWM.doConnectionSetup()) {
			System.out
					.println("Unable to establish a connection to the distributor as a client.");
			System.exit(1);
		}
		System.out.println("Connected to distributor as a client at "
				+ this.clientWM.getHost() + ":"
				+ this.clientWM.getPort());

		// Read the user input into this string
		String command = null;

		// Keep running until either we get disconnected, or the user wants to
		// quit.
		while (this.keepRunning) {
			try {
				// If the user has something to read in the buffer.
				if (this.userInput.ready()) {
					command = this.userInput.readLine();

					// Allow the solver to shut down gracefully if the user
					// wants to exit.
					if ("exit".equalsIgnoreCase(command)) {
						System.out.println("Exiting solver (user).");
						break;
					} else {
						System.out.println("Unknown command \"" + command
								+ "\". Type \"exit\" to exit.");
					}
				}
			} catch (IOException e1) {
				System.err.println("Error reading user input.");
				e1.printStackTrace();
			}
			// Sleep for 100ms, since there isn't much else to do.
			try {
				Thread.sleep(100l);
			} catch (InterruptedException e) {
				// Ignored
			}
		}
		// Stop running
		this.clientWM.doConnectionTearDown();
		this.solverWM.doConnectionTearDown();
		System.exit(0);
	}

	public StreamRequestMessage createProjectorDataRequestMessage(
			TypeAnnounceMessage typeSpecificationMessage) {
		StreamRequestMessage request = new StreamRequestMessage();
		request.setBeginTimestamp(-4200000l);
		request.setUpdateInterval(0l);

		TypeSpecification[] types = typeSpecificationMessage
				.getTypeSpecifications();
		int alias = -1;

		for (TypeSpecification type : types) {
			if ("on".equalsIgnoreCase(type.getUriName())) {
				alias = type.getTypeAlias();
				break;
			}
		}

		if (alias != -1) {
			request.setQueryAttributes(new String[] { "on" });
		} else {
			log.error("FAILED TO GENERATE PROJECTOR DATA REQUEST MESSAGE");
			System.exit(1);
		}
		return request;
	}

	public static ArrayList<String> getHighestProbabilities(
			Hashtable<String, int[]> probabilities, int nth) {
		Enumeration<String> keys = probabilities.keys();
		ArrayList<String> result = new ArrayList<String>();
		ArrayList<Double> probabilityRatios = new ArrayList<Double>();

		while (keys.hasMoreElements()) {
			int[] temp = probabilities.get(keys.nextElement());
			probabilityRatios.add((double) (temp[0] / temp[1]));
		}

		while (nth > 0) {
			result.add(Double.toString(ProjectorPredictionSolver
					.removeLargest(probabilityRatios)));
			nth--;
		}

		return result;
	}

	public static double removeLargest(ArrayList<Double> a) {
		double largest = 0;
		int largestIndex = 0;

		for (double d : a) {
			if (largest > d) {
				continue;
			} else {
				largest = d;
				largestIndex = a.indexOf((Double) d);
			}
		}
		a.remove(largestIndex);
		return largest;
	}

	public void makePrediction(DataResponseMessage solutions) {
		// TODO
		for (Attribute s : solutions.getAttributes()) {
			if (!"on".equals(s.getAttributeName())) {
				continue;
			}
			if (this.currentEventCorrelations.get(solutions.getUri()) == null) {
				if (s.getData()[0] == (byte) 1) {
					this.currentEventCorrelations.put(solutions.getUri(),
							new int[] { 1, 1 });
				} else {
					this.currentEventCorrelations.put(solutions.getUri(),
							new int[] { 0, 1 });
				}
			} else {
				int[] oldProbability = this.currentEventCorrelations
						.get(solutions.getUri());
				if (s.getData()[0] == (byte) 1) {
					oldProbability[0] = ++oldProbability[0];
					oldProbability[1] = ++oldProbability[1];
					this.currentEventCorrelations.put(solutions.getUri(),
							oldProbability);
				} else {
					oldProbability[1] = ++oldProbability[1];
					this.currentEventCorrelations.put(solutions.getUri(),
							oldProbability);
				}
			}
		}

		// When done call compareAndSendPrediction()
		// TODO

	}

	public void compareAndSendPrediction() {
		this.recentProbabilities = this.getHighestProbabilities(
				this.currentEventCorrelations,
				this.NUMBER_CORRELATED_EVENTS_USED);

		int count = 0;

		// check if the top correlated events are the same for the "pattern"
		// found AND current time
		for (int i = 0; i < this.NUMBER_CORRELATED_EVENTS_USED; i++) {
			for (int j = 0; j < this.NUMBER_CORRELATED_EVENTS_USED; j++) {
				if (this.highestProbabilities.get(i).equalsIgnoreCase(
						this.recentProbabilities.get(j))) {
					count++;
				}
			}
		}

		boolean projectorPrediction = false;

		if (count == this.NUMBER_CORRELATED_EVENTS_USED) {
			projectorPrediction = true;
		}

		Solution solution = new Solution();

		solution.setTargetName("winlab.projectors.large conference room");
		solution.setAttributeName(ProjectorPredictionSolver.SOLUTION_URI_NAME);

		if (projectorPrediction) {
			solution.setData(new byte[] { 1 });
		} else {
			solution.setData(new byte[] { 0 });
		}

		ArrayList<Solution> returnedList = new ArrayList<Solution>();
		returnedList.add(solution);

		// Send the solution to the distributor
		if (!this.solverWM.sendSolutions(
				returnedList)) {
			log.info("Unable to send solution to distributor: {}", solution);
		}
		log.info("SOLUTION SENT");
	}

	public void requestLiveFifteenMinutesOfData() {
		this.clientWM.sendMessage(this
				.generateDataRequest(0l));
	}

	public void generateProjectorTimes(DataResponseMessage solution) {
		if (!(solution.getUri())
				.equals("winlab.projectors.large conference room")) {
			return;
		}
		for (Attribute attrib : solution.getAttributes()) {
			if (!"on".equals(attrib.getAttributeName())) {
				continue;
			}
			if (attrib.getData()[0] == (byte) 0) {
				this.recentlySeenOff = true;
				continue;
			} else {
				if (!this.recentlySeenOff) {
					continue;
				} else {
					this.projectorTimes.add(System.currentTimeMillis()
							- attrib.getCreationDate());
					if (this.projectorTimes.size() == this.NUMBER_OF_PROJECTOR_TIMES) {
						this.gettingProjectorTimes = false;
						for (Long time : this.projectorTimes) {
							this.clientWM.sendMessage(this
									.generateDataRequest(time));
						}
						return;
					}
					this.recentlySeenOff = false;
				}
			}
		}

	}

	public SnapshotRequestMessage generateDataRequest(long startTime) {

		SnapshotRequestMessage request = new SnapshotRequestMessage();

		request.setBeginTimestamp(-startTime);
		request.setEndTimestamp(-startTime + 900000l);

		TypeSpecification[] types = this.specificationMessage
				.getTypeSpecifications();

		String[] requestStrings = new String[types.length - 1];
		int i = 0;

		for (TypeSpecification type : types) {
			log.debug(type.getUriName() + " alias: " + type.getTypeAlias());
			if ("on".equalsIgnoreCase(type.getUriName())) {
				continue;
			}
			requestStrings[i++] = type.getUriName();
		}

		request.setQueryAttributes(requestStrings);

		if (request.getQueryAttributes() == null) {
			log.error("No past solutions available.");
			System.exit(1);
		}

		return request;
	}

	public void getCorrelations(DataResponseMessage solution) {

		if (this.probabilities.get(solution.getUri()) == null) {
			for (Attribute attrib : solution.getAttributes()) {
				if(!"on".equals(attrib.getAttributeName()))
						{
				continue;
						}
				if (attrib.getData()[0] == (byte) 1) {
					this.probabilities.put(solution.getUri(),
							new int[] { 1, 1 });
				} else {
					this.probabilities.put(solution.getUri(),
							new int[] { 0, 1 });
				}
			}
		} else {
			int[] oldProbability = this.probabilities.get(solution.getUri());
			for(Attribute attrib : solution.getAttributes()){
				if(!"on".equals(attrib.getAttributeName())){
					continue;
				}
			if (attrib.getData()[0] == (byte) 1) {
				oldProbability[0] = ++oldProbability[0];
				oldProbability[1] = ++oldProbability[1];
				this.probabilities.put(solution.getUri(), oldProbability);
			} else {
				oldProbability[1] = ++oldProbability[1];
				this.probabilities.put(solution.getUri(), oldProbability);
			}
			}
		}

	}

	@Override
	public void connectionInterrupted(SolverWorldModelInterface worldModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connectionEnded(SolverWorldModelInterface worldModel) {
		// Stop running after the connection is finally closed.
		System.out.println("Connection closed.");
		this.keepRunning = false;
		this.interrupt();
	}

	@Override
	public void connectionEstablished(SolverWorldModelInterface worldModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connectionInterrupted(ClientWorldModelInterface worldModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connectionEnded(ClientWorldModelInterface worldModel) {
		// Stop running after the connection is finally closed.
		System.out.println("Connection closed.");
		this.keepRunning = false;
		this.interrupt();
	}

	@Override
	public void connectionEstablished(ClientWorldModelInterface worldModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void requestCompleted(ClientWorldModelInterface worldModel,
			AbstractRequestMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dataResponseReceived(ClientWorldModelInterface worldModel,
			DataResponseMessage message) {
		if (this.gettingProjectorTimes) {
			this.generateProjectorTimes(message);
		} else {
			this.lookingForCorrelations = true;
		}

		// TODO
		// The solution data is binary at this point.

		if (this.lookingForCorrelations) {
			this.getCorrelations(message);
		} else {
			if (this.needHighProbabilityList) {
				this.highestProbabilities = this.getHighestProbabilities(
						this.probabilities, this.NUMBER_CORRELATED_EVENTS_USED);
				this.needHighProbabilityList = false;
				this.requestLiveFifteenMinutesOfData();
			}
			// String ArrayList of highest probabilities has been generated.
			// request for live 15 minutes of data has also been made so a
			// prediction can be made.

			this.makePrediction(message);
		}
	}

	@Override
	public void uriSearchResponseReceived(ClientWorldModelInterface worldModel,
			URISearchResponseMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void startTransientReceived(SolverWorldModelInterface worldModel,
			StartTransientMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void stopTransientReceived(SolverWorldModelInterface worldModel,
			StopTransientMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void typeSpecificationsSent(SolverWorldModelInterface worldModel,
			TypeAnnounceMessage message) {
		this.canSendSolutions = true;
	}

	@Override
	public void attributeAliasesReceived(ClientWorldModelInterface worldModel,
			AttributeAliasMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void originAliasesReceived(ClientWorldModelInterface worldModel,
			OriginAliasMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void originPreferenceSent(ClientWorldModelInterface worldModel,
			OriginPreferenceMessage message) {
		// TODO Auto-generated method stub
		
	}

}
