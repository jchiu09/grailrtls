package org.grailrtls.solver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import org.grailrtls.libsolver.SolverAggregatorInterface;
import org.grailrtls.libsolver.listeners.SampleListener;
import org.grailrtls.libsolver.protocol.messages.SampleMessage;
import org.grailrtls.libsolver.rules.SubscriptionRequestRule;
import org.grailrtls.libworldmodel.client.ClientWorldModelInterface;
import org.grailrtls.libworldmodel.client.listeners.DataListener;
import org.grailrtls.libworldmodel.client.protocol.messages.AbstractRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.Attribute;
import org.grailrtls.libworldmodel.client.protocol.messages.AttributeAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.DataResponseMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginPreferenceMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.StreamRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.URISearchResponseMessage;
import org.grailrtls.libworldmodel.solver.SolverWorldModelInterface;
import org.grailrtls.libworldmodel.solver.listeners.ConnectionListener;
import org.grailrtls.libworldmodel.solver.protocol.messages.DataTransferMessage.Solution;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage.TypeSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This solver detects whether a bike sensor is in the vicinity of a designated bike hub and puts the location of the bike into the world model
 * 
 * @author Jonathan
 * 
 */
public class BikePresenceSolver extends Thread implements ConnectionListener,
	org.grailrtls.libsolver.listeners.ConnectionListener, SampleListener, DataListener
{
    private static final Logger log = LoggerFactory.getLogger(BikePresenceSolver.class);

    private boolean keepRunning = true;

    private SolverWorldModelInterface worldModelInterface = new SolverWorldModelInterface();

    private SolverAggregatorInterface aggregatorInterface = new SolverAggregatorInterface();

    private final ClientWorldModelInterface worldModel = new ClientWorldModelInterface();

    private static boolean debug = false;

    private static String ORIGIN_STRING = "jon.solver";

    private static final String SOLUTION_REGION_URI = "CoRE.bikes";

    private Hashtable<String, Integer> aliases = new Hashtable<String, Integer>();

    private Hashtable<Integer, String> tagIds = new Hashtable<Integer, String>();

    private Hashtable<Integer, String> hubIds = new Hashtable<Integer, String>();

    private Hashtable<String, Integer> uris = new Hashtable<String, Integer>();

    private Hashtable<Integer, MonitorThread> monitorThreads = new Hashtable<Integer, MonitorThread>();

    private String uriRegex = ".*(bike|hub).*";

    private BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));

    private final String LOCATION_ATTRIBUTE = "location";

    private String[] specs = { LOCATION_ATTRIBUTE };

    private static boolean showSamples = false;

    public BikePresenceSolver(String aggHost, int aggPort, String worldHost, int worldPort)
    {
	this.worldModelInterface.setHost(worldHost);
	this.worldModelInterface.setPort(worldPort);
	this.aggregatorInterface.setHost(aggHost);
	this.aggregatorInterface.setPort(aggPort);
	this.worldModel.setHost(worldHost);
	this.worldModel.setPort(7013);
    }

    public static void main(String[] args)
    {
	if (args.length < 4)
	{
	    printUsageInfo();
	    System.exit(1);
	}
	else
	{
	    for (int i = 4; i < args.length; i++)
	    {
		String[] temp = args[i].split("=");
		if (args[i].equalsIgnoreCase("d"))
		{
		    debug = true;
		    System.out.println("Debug mode enabled");
		}
		else if (temp[0].equals("origin"))
		{
		    ORIGIN_STRING = temp[1];
		}
		else if ("showsamples".equalsIgnoreCase(args[i]))
		{
		    showSamples = true;
		}
	    }
	}

	int aggPort = Integer.parseInt(args[1]);
	int worldPort = Integer.parseInt(args[3]);
	BikePresenceSolver solver = new BikePresenceSolver(args[0], aggPort, args[2], worldPort);
	solver.start();
    }

    protected static void printUsageInfo()
    {
	System.out.println("Requires two parameters: <Aggregator host> <Aggregator port> <World Model host> <World Model port>.");
	System.out.println("Once running, type \"exit\" to exit.");
    }

    public void run()
    {

	// Set up solver-aggregator interface
	SubscriptionRequestRule[] rules = new SubscriptionRequestRule[1];
	rules[0] = SubscriptionRequestRule.generateGenericRule();
	rules[0].setUpdateInterval(0l);
	rules[0].setPhysicalLayer((byte) 1);
	this.aggregatorInterface.setRules(rules);
	this.aggregatorInterface.addSampleListener(this);
	this.aggregatorInterface.addConnectionListener(this);
	this.aggregatorInterface.setDisconnectOnException(true);
	this.aggregatorInterface.setStayConnected(true);
	this.aggregatorInterface.setConnectionRetryDelay(10000l);

	// Set up solver-world model interface
	for (int i = 0; i < specs.length; i++)
	{
	    TypeSpecification spec = new TypeSpecification();
	    spec.setIsTransient(false);
	    spec.setUriName(specs[i]);
	    spec.setTypeAlias(i);
	    aliases.put(specs[i], i);
	    this.worldModelInterface.addType(spec);
	}

	this.worldModelInterface.setOriginString(ORIGIN_STRING);
	this.worldModelInterface.addConnectionListener(this);
	this.worldModelInterface.setConnectionRetryDelay(10000l);
	this.worldModelInterface.setStayConnected(true);
	this.worldModelInterface.setDisconnectOnException(true);

	// Set up client-world model interface
	this.worldModel.setStayConnected(true);
	this.worldModel.setDisconnectOnException(true);
	this.worldModel.addDataListener(this);

	this.worldModel.registerSearchRequest(uriRegex);

	/*
	 * Connect to the worldModel. Note that this is being done before the aggregator to simplify the sample-handling logic in this example class.
	 */
	if (!this.worldModelInterface.doConnectionSetup())
	{
	    System.out.println("Unable to establish a connection to the World Model.");
	    System.exit(1);
	}
	System.out.println("Connected to World Model at " + this.worldModelInterface.getHost() + ":" + this.worldModelInterface.getPort());

	// Connect to the aggregator
	if (!this.aggregatorInterface.doConnectionSetup())
	{
	    System.out.println("Unable to establish a connection to the aggregator.");
	    System.exit(1);
	}
	System.out.println("Connected to aggregator at " + this.aggregatorInterface.getHost() + ":" + this.aggregatorInterface.getPort());

	if (!this.worldModel.doConnectionSetup())
	{
	    System.out.println("Unable to establish a connection to the world model.");
	    System.exit(1);
	}
	System.out.println("Connected to world model at " + this.worldModel.getHost() + ":" + this.worldModel.getPort());

	// Read the user input into this string
	String command = null;

	// Keep running until either we get disconnected, or the user wants to
	// quit.
	while (this.keepRunning)
	{
	    try
	    {
		// If the user has something to read in the buffer.
		if (this.userInput.ready())
		{
		    command = this.userInput.readLine();

		    // Allow the solver to shut down gracefully if the user
		    // wants to exit.
		    if ("exit".equalsIgnoreCase(command))
		    {
			System.out.println("Exiting solver (user).");
			break;
		    }
		    else if ("show threads".equalsIgnoreCase(command))
		    {
			System.out.println("");
			for (Enumeration<Integer> e = monitorThreads.keys(); e.hasMoreElements();)
			{
			    int num = e.nextElement();
			    System.out.println("[SHOW THREADS] Tag ID: " + num + ", Target URI: " + monitorThreads.get(num).getTargetUri());
			}
			System.out.println("");
		    }
		    else
		    {
			System.out.println("Unknown command \"" + command + "\". Type \"exit\" to exit.");
		    }

		}
	    } catch (IOException e1)
	    {
		System.err.println("Error reading user input.");
		e1.printStackTrace();
	    }
	    // Sleep for 100ms, since there isn't much else to do.
	    try
	    {
		Thread.sleep(100l);
	    } catch (InterruptedException e)
	    {
		// Ignored
	    }
	}
	// Stop running
	this.aggregatorInterface.doConnectionTearDown();
	this.worldModelInterface.doConnectionTearDown();
	this.worldModel.doConnectionTearDown();
	System.exit(0);
    }

    public void connectionEnded(SolverAggregatorInterface arg0)
    {

    }

    public void connectionEstablished(SolverAggregatorInterface arg0)
    {

    }

    public void connectionInterrupted(SolverAggregatorInterface arg0)
    {

    }

    public void connectionEnded(ClientWorldModelInterface arg0)
    {

    }

    public void connectionEstablished(ClientWorldModelInterface arg0)
    {

    }

    public void connectionInterrupted(ClientWorldModelInterface arg0)
    {

    }

    public void attributeAliasesReceived(ClientWorldModelInterface arg0, AttributeAliasMessage arg1)
    {

    }

    public void dataResponseReceived(ClientWorldModelInterface arg0, DataResponseMessage arg1)
    {
	updateTagIds(arg1);
	dprint("DataResponseReceived: " + arg1.getUri());
    }

    public void originAliasesReceived(ClientWorldModelInterface arg0, OriginAliasMessage arg1)
    {

    }

    public void requestCompleted(ClientWorldModelInterface arg0, AbstractRequestMessage arg1,
	    int arg2)
    {

    }

    public void requestTicketReceived(ClientWorldModelInterface arg0, AbstractRequestMessage arg1,
	    int arg2)
    {

    }

    public void uriSearchResponseReceived(ClientWorldModelInterface worldModel,
	    URISearchResponseMessage message)
    {
	dprint("UriSearchResponseReceived: Querying world model for URIs...");
	long now = System.currentTimeMillis();

	StreamRequestMessage request = new StreamRequestMessage();
	request.setBeginTimestamp(now);
	request.setUpdateInterval(0l);
	request.setQueryURI(uriRegex);
	request.setQueryAttributes(new String[] { ".*" });
	worldModel.sendMessage(request);

    }

    public void sampleReceived(SolverAggregatorInterface aggregator, SampleMessage sample)
    {
	int did = BikePresenceSolver.getPipId(sample.getDeviceId());
	if (tagIds.containsKey(did))
	{
	    int receiverId = BikePresenceSolver.getPipId(sample.getReceiverId());
	    if (showSamples)
		dprint("Sample Received: Device ID = " + did + ", Receiver ID = " + receiverId);
	    if (!hubIds.containsKey(receiverId))
		return;

	    String target = tagIds.get(did);
	    synchronized (monitorThreads)
	    {
		if (!monitorThreads.containsKey(did))
		{
		    MonitorThread thread = new MonitorThread(target, did);
		    thread.addReceiverId(BikePresenceSolver.getPipId(sample.getReceiverId()));
		    monitorThreads.put(did, thread);
		    thread.start();
		}
		else
		{
		    monitorThreads.get(did).addReceiverId(BikePresenceSolver.getPipId(sample.getReceiverId()));
		    monitorThreads.get(did).setTargetUri(target);
		    monitorThreads.get(did).setLastUpdated(System.currentTimeMillis());
		}
	    }
	}
    }

    public void connectionEnded(SolverWorldModelInterface arg0)
    {

    }

    public void connectionEstablished(SolverWorldModelInterface arg0)
    {

    }

    public void connectionInterrupted(SolverWorldModelInterface arg0)
    {

    }

    public void dprint(String s)
    {
	if (debug)
	    System.out.println("[DEBUG] " + s);
	return;
    }

    /**
     * Returns an integer representation of a 23-bit PIPSqueak device ID.
     * 
     * @param deviceId
     * @return the 23-bit PIPSqueak device ID as a 32-bit integer
     */
    private static int getPipId(byte[] deviceId)
    {
	// PIPSqueak tags only have a 23-bit ID, so we can extract the last 3
	// bytes as an integer
	return ((deviceId[SampleMessage.DEVICE_ID_SIZE - 3] << 16) & 0xFF) + ((deviceId[SampleMessage.DEVICE_ID_SIZE - 2] << 8) & 0xFF) + (deviceId[SampleMessage.DEVICE_ID_SIZE - 1] & 0xFF);
    }

    /**
     * Convert the byte array to an int.
     * 
     * @param b
     *            The byte array
     * @return The integer
     */
    public static int byteArrayToInt(byte[] b)
    {
	return byteArrayToInt(b, 0);
    }

    /**
     * Convert the byte array to an int starting from the given offset.
     * 
     * @param b
     *            The byte array
     * @param offset
     *            The array offset
     * @return The integer
     */
    public static int byteArrayToInt(byte[] b, int offset)
    {
	int value = 0;
	for (int i = 0; i < 4; i++)
	{
	    int shift = (4 - 1 - i) * 8;
	    value += (b[i + offset] & 0x000000FF) << shift;
	}
	return value;
    }

    public static String byteArrayToString(byte[] b)
    {
	String result = "[ ";
	if (b.length == 0)
	{
	    return "[ ]";
	}
	for (int i = 0; i < b.length; i++)
	{
	    result += b[i] + " ";
	}
	result += "]";
	return result;
    }

    /**
     * Maps the bike uri to the appropriate tag id Maps location to the appropriate hub
     * 
     * @param msg
     *            from dataResponseReceived method
     */
    private void updateTagIds(DataResponseMessage msg)
    {
	Attribute[] attr = msg.getAttributes();
	int id = -1;
	boolean tagId = false;
	boolean hubId = false;
	String location = null;
	for (Attribute a : attr)
	{
	    if (!a.getOriginName().equals(ORIGIN_STRING))
		continue;
	    if (a.getAttributeName().equals("tag_id"))
	    {
		tagId = true;
		hubId = false;
		try
		{
		    String temp = new String(a.getData(), "UTF-16BE");
		    id = Integer.parseInt(temp);
		} catch (Exception e)
		{
		    e.printStackTrace();
		}
		break;
	    }
	    else if (a.getAttributeName().equals("hub_id"))
	    {
		try
		{
		    String temp = new String(a.getData(), "UTF-16BE");
		    id = Integer.parseInt(temp);
		    if (!"-1".equals(temp))
		    {
			hubId = true;
			tagId = false;
		    }

		} catch (Exception e)
		{
		    e.printStackTrace();
		}

	    }
	    else if (a.getAttributeName().equals("hub_location"))
	    {
		try
		{
		    location = new String(a.getData(), "UTF-16BE");
		} catch (Exception e)
		{
		    e.printStackTrace();
		}
	    }
	}

	if (id != -1 && tagId)
	{
	    String uri = msg.getUri();
	    try
	    {
		int previousId = uris.put(uri, id);
		if (previousId != id)
		{
		    tagIds.remove(previousId);
		    dprint("Previous tag id for " + uri + " was " + previousId + ", new tag id is " + id);
		    monitorThreads.get(previousId).interrupt();
		    monitorThreads.remove(previousId);
		}

	    } catch (Exception e)
	    {
		dprint(e.getMessage());
	    }

	    try
	    {
		String previousUri = tagIds.put(id, uri);
		if (previousUri != uri)
		{
		    uris.remove(previousUri);
		    dprint("Previous URI for " + id + " was " + previousUri + ", new URI is " + uri);
		}
	    } catch (Exception e)
	    {
		dprint(e.getMessage());
	    }
	    dprint("[UpdateTag] ID: " + id + ", URI: " + uri);
	}
	else if (id != -1 && hubId && location != null)
	{
	    hubIds.put(id, location);
	    System.out.println("Hub found: " + id + ", " + location);
	}
    }

    /**
     * Sends a solution created from sample to targetUri
     * 
     * @param sample
     *            Sample that solution is created from
     * @param targetUri
     *            Target URI that the solution should be sent to
     */
    public void sendSolution(int receiverId, String targetUri)
    {
	Collection<Solution> solution = generateSolution(receiverId, targetUri);
	if (solution == null)
	{
	    sendUnknownSolution(targetUri);
	    return;
	}

	// Send the solution to the worldModel
	if (!this.worldModelInterface.sendSolutions(solution))
	{
	    System.out.println("Unable to send solution");
	}
	else
	{
	    System.out.println("Solution has been sent: {" + targetUri + " :: " + receiverId + "}");
	}
    }

    /**
     * Sends a solution with location as unknown
     * 
     * @param sample
     *            Sample that solution is created from
     * @param targetUri
     *            Target URI that the solution should be sent to
     */
    public void sendUnknownSolution(String targetUri)
    {
	Collection<Solution> solution = generateUnknownSolution(targetUri);
	if (solution == null)
	{
	    dprint("Null solution! Solution not sent!");
	    return;
	}

	// Send the solution to the worldModel
	if (!this.worldModelInterface.sendSolutions(solution))
	{
	    System.out.println("Unable to send solution");
	}
	else
	{
	    System.out.println("Unknown solution has been sent to " + targetUri);
	}
    }

    /**
     * Sets the location of the bike based on the location of the hub
     * 
     * @param sample
     *            from sampleReceived method
     * @param targetUri
     *            from the tagIds hashtable
     * @return solution to be sent
     */
    private Collection<Solution> generateSolution(int receiverId, String targetUri)
    {
	Solution solution = new Solution();
	solution.setTargetName(targetUri);
	solution.setAttributeName(LOCATION_ATTRIBUTE);
	solution.setAttributeNameAlias(aliases.get(LOCATION_ATTRIBUTE));

	try
	{
	    String location = hubIds.get(receiverId);
	    if (location != null)
	    {
		byte[] data = location.getBytes("UTF-16BE");
		solution.setData(data);
	    }
	    else
	    {
		dprint("Location for hub " + receiverId + " not found!");
		return null;
	    }
	} catch (Exception e)
	{
	    e.printStackTrace();
	}

	solution.setTime(System.currentTimeMillis());

	ArrayList<Solution> returnedList = new ArrayList<Solution>();
	returnedList.add(solution);

	return returnedList;
    }

    /**
     * Sets the location of the bike to unknown
     * 
     * @param sample
     *            from sampleReceived method
     * @param targetUri
     *            from the tagIds hashtable
     * @return solution to be sent
     */
    private Collection<Solution> generateUnknownSolution(String targetUri)
    {
	Solution solution = new Solution();
	solution.setTargetName(targetUri);
	solution.setAttributeName(LOCATION_ATTRIBUTE);
	solution.setAttributeNameAlias(aliases.get(LOCATION_ATTRIBUTE));

	try
	{
	    String location = "Unknown";
	    byte[] data = location.getBytes("UTF-16BE");
	    solution.setData(data);
	} catch (Exception e)
	{
	    e.printStackTrace();
	}

	solution.setTime(System.currentTimeMillis());

	ArrayList<Solution> returnedList = new ArrayList<Solution>();
	returnedList.add(solution);

	return returnedList;
    }

    /**
     * Thread that waits a set amount of time before sending a solution
     * 
     * @author Jonathan
     * 
     */
    public class MonitorThread extends Thread
    {
	private String targetUri = null;
	private boolean running = true;
	private long sleepTime = 1000 * 5;
	private long lastUpdated = 0;
	private int deviceId;
	private List<Integer> knownHubs = new ArrayList<Integer>();

	public MonitorThread(String targetUri, int deviceId)
	{
	    this.targetUri = targetUri;
	    this.lastUpdated = System.currentTimeMillis();
	    this.deviceId = deviceId;
	}

	public void run()
	{
	    dprint("Monitor Thread started for " + targetUri);
	    while (running)
	    {
		try
		{
		    dprint("Sample received and matched. Device ID: " + deviceId);
		    Thread.sleep(sleepTime);
		    if (System.currentTimeMillis() - lastUpdated < sleepTime && !knownHubs.isEmpty())
		    {
			sendSolution(knownHubs.get(0), targetUri);
		    }
		    else
		    {
			sendUnknownSolution(targetUri);
		    }
		} catch (InterruptedException e)
		{
		    running = false;
		}
	    }
	    dprint("Monitor Thread ended for " + targetUri);
	    sendUnknownSolution(targetUri);
	}

	public void setRunning(boolean running)
	{
	    this.running = running;
	}

	public void setTargetUri(String targetUri)
	{
	    this.targetUri = targetUri;
	}

	public void setSleepTime(long sleepTime)
	{
	    this.sleepTime = sleepTime;
	}

	public long getLastUpdated()
	{
	    return lastUpdated;
	}

	public void setLastUpdated(long lastUpdated)
	{
	    this.lastUpdated = lastUpdated;
	}

	public String getTargetUri()
	{
	    return targetUri;
	}

	public boolean isRunning()
	{
	    return running;
	}

	public long getSleepTime()
	{
	    return sleepTime;
	}

	public int getDeviceId()
	{
	    return deviceId;
	}

	public void setDeviceId(int deviceId)
	{
	    this.deviceId = deviceId;
	}

	public void addReceiverId(int receiverId)
	{
	    if (hubIds.containsKey(receiverId) && !knownHubs.contains(receiverId))
	    {
		knownHubs.add(receiverId);
	    }
	}
    }

    @Override
    public void originPreferenceSent(ClientWorldModelInterface arg0, OriginPreferenceMessage arg1)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void requestCompleted(ClientWorldModelInterface arg0, AbstractRequestMessage arg1)
    {
	// TODO Auto-generated method stub

    }

}
