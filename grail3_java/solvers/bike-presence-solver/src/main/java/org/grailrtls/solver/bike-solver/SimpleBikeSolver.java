/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2012 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package org.grailrtls.solver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.grailrtls.libworldmodel.solver.SolverWorldModelInterface;
import org.grailrtls.libworldmodel.solver.listeners.ConnectionListener;
import org.grailrtls.libworldmodel.solver.listeners.DataListener;
import org.grailrtls.libworldmodel.solver.protocol.messages.DataTransferMessage.Solution;
import org.grailrtls.libworldmodel.solver.protocol.messages.StartTransientMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.StopTransientMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage.TypeSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleBikeSolver extends Thread implements ConnectionListener, DataListener{

	private static final Logger log = LoggerFactory.getLogger(SimpleBikeSolver.class);
	
	private static final String TARGET_URI = "bike.test.1";
	
	private static final String ATTRIBUTE = "location";
	
	private static final String ORIGIN = "test.solver";
	
	private final SolverWorldModelInterface worldModel = new SolverWorldModelInterface();
	
	private boolean wmReadyForSolutions = false;
	
	private boolean keepRunning = true;
	
	public static void main(String[]args){
		if (args.length < 2) {
			printUsageInfo();
			return;
		}

		int port = 7013;
		try {
			port = Integer.parseInt(args[1]);
		} catch (NumberFormatException nfe) {
			log.error(
					"Unable to format {} into a valid port number. Defaulting to 7013.",
					args[1]);
		}

		final SimpleBikeSolver solver = new SimpleBikeSolver(args[0], port);
		Runtime.getRuntime().addShutdownHook(new Thread(){
			public void run(){
				solver.keepRunning = false;
				solver.worldModel.doConnectionTearDown();
			}
		});
		solver.start();
		
	}
	
	public static void printUsageInfo() {
		StringBuffer sb = new StringBuffer(
				"Usage: <World Model IP> <World Model Port>\n");
		System.err.println(sb.toString());
	}

	
	public SimpleBikeSolver(final String wmHost, final int wmPort){
		this.worldModel.setHost(wmHost);
		this.worldModel.setPort(wmPort);
	
		this.worldModel.addConnectionListener(this);
		this.worldModel.addDataListener(this);
		TypeSpecification testType = new TypeSpecification();
		testType.setIsTransient(false);
		testType.setUriName(ATTRIBUTE);
		this.worldModel.addType(testType);
		this.worldModel.setOriginString(ORIGIN);
		this.worldModel.setCreateUris(true);
		this.worldModel.setDisconnectOnException(true);
		this.worldModel.setStayConnected(true);
	}
	
	@Override
	public void run(){
		if(this.worldModel.doConnectionSetup()){
			BufferedReader userIn = new BufferedReader(new InputStreamReader(System.in));
			while(this.keepRunning){
				try {
					String userString = userIn.readLine();
		
					if(userString != null){
						this.sendSolution(userString.trim());
					}
				} catch (IOException e) {
					log.error("IOException occurred while reading from console: " + e);
					e.printStackTrace();
					System.exit(1);
				}
			}
		}
	}
	
	private void sendSolution(final String userString) throws UnsupportedEncodingException{
		Solution sol = new Solution();
		sol.setTargetName(TARGET_URI);
		sol.setAttributeName(ATTRIBUTE);
		sol.setTime(System.currentTimeMillis());
		sol.setData(userString.getBytes("UTF-16BE"));
		if(this.wmReadyForSolutions){
			this.worldModel.sendSolution(sol);
		}
	}

	@Override
	public void startTransientReceived(SolverWorldModelInterface worldModel,
			StartTransientMessage message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stopTransientReceived(SolverWorldModelInterface worldModel,
			StopTransientMessage message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void typeSpecificationsSent(SolverWorldModelInterface worldModel,
			TypeAnnounceMessage message) {
		log.info("Announced type specifications. Ready to send solutions.");
		System.out.println("Send solutions by typing a string and pressing [Return].");
		this.wmReadyForSolutions = true;
	}

	@Override
	public void connectionInterrupted(SolverWorldModelInterface worldModel) {
		log.warn("Temporarily lost connection to {}", worldModel);
		this.wmReadyForSolutions = false;
		
	}

	@Override
	public void connectionEnded(SolverWorldModelInterface worldModel) {
		log.error("Permanently lost connection to {}", worldModel);
		System.exit(1);
		
	}

	@Override
	public void connectionEstablished(SolverWorldModelInterface worldModel) {
		log.info("Connected to {}", worldModel);
	}
	
}
