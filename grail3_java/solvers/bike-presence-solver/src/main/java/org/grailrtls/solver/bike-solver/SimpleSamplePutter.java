/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Robert Moore
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

//package org.grailrtls.java_tutorials.solver;

package org.grailrtls.solver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;

import org.grailrtls.libsolver.SolverAggregatorInterface;
import org.grailrtls.libworldmodel.solver.SolverWorldModelInterface;
import org.grailrtls.libworldmodel.solver.protocol.messages.DataTransferMessage.Solution;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage.TypeSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An example solver used as a tutorial, demonstrating the use of the Aggregator-Solver protocol library (libsolver) and the Solver-Distributor protocol library (libdistributor). This class is based
 * on the C++ temperature solver tutorial written by Ben Firner. A single class that analyzes packet data from PIPSqueak devices and generates temperature solutions.
 * 
 * This example relies on libcommon for printing of device addresses, libsolver to communicate with the Aggregator, and libdistributor to communicate with the Distributor.
 * 
 * @author Robert Moore
 * 
 */

/*
 * Implement SampleListener to be able to handle samples sent by the aggregator. Implement ConnectionListener to stop running after the connection is closed.
 */
public class SimpleSamplePutter extends Thread implements
	org.grailrtls.libworldmodel.solver.listeners.ConnectionListener
{
    /**
     * Logging facility for this class.
     */
    private static final Logger log = LoggerFactory.getLogger(SimpleSamplePutter.class);

    /**
     * Keep track of the solution URI name.
     */
    private static final String SOLUTION_URI_NAME = "open";

    /**
     * Region URI for this example.
     */
    private static final String SOLUTION_REGION_URI = "CoRE.window.127";

    /**
     * Origin string for this solver.
     */
    private static String ORIGIN_STRING = "jon.sample.putter";

    /**
     * Flag to keep running until the connection is closed.
     */
    private boolean keepRunning = true;

    /**
     * Interface to the aggregator, takes care of all the basic protocol stuff.
     */
    private SolverAggregatorInterface aggregatorInterface = new SolverAggregatorInterface();

    /**
     * Interface to the World Model for publishing solutions. Takes care of all the basic protocol stuff.
     */
    private SolverWorldModelInterface worldModelInterface = new SolverWorldModelInterface();

    /**
     * Used to get user input.
     */
    private BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));

    /**
     * Indicates whether the solution type specification message has been sent to the distributor, and therefore that we can write solutions.
     */
    private boolean canSendSolutions = false;

    private volatile long lastUpdate = 0l;
    /**
     * How often to generate temperature solutions are generated
     */
    private volatile long updateInterval = 10000l;

    private int worldPort;

    private String worldHost;

    private String[] types = { "open", "region", "tag_id", "owner_email", "latitude", "longitude",
	    "name", "room", "location", "hub_location", "hub_id" };

    /**
     * Creates a new {@code SimpleSamplePutter} and starts it.
     * 
     * @param args
     *            Aggregator host, aggregator port, distributor host, and distributor port (solvers), in that order.
     */
    public static void main(String[] args)
    {
	if (args.length < 2)
	{
	    printUsageInfo();
	    System.exit(1);
	}
	else
	{
	    for (String s : args)
	    {
		String[] temp = s.split("=");
		if (temp[0].equals("origin"))
		{
		    ORIGIN_STRING = temp[1];
		    System.out.println("Setting origin string to: " + ORIGIN_STRING);
		}
	    }
	}

	// Normally, catching the exception would be good, but this is just a
	// tutorial.
	int distPort = Integer.parseInt(args[1]);

	SimpleSamplePutter solver = new SimpleSamplePutter(args[0], distPort);
	solver.start();
    }

    /**
     * Prints a simple message about the parameters for this program.
     */
    protected static void printUsageInfo()
    {
	System.out.println("Requires two parameters: <Distributor host> <Distributor port>.");
	System.out.println("Once running, type \"exit\" to exit.");
    }

    /**
     * Creates a new {@code SimpleSamplePutter} that connects to an aggregator and distributor at the specified host name/IP addresses and port numbers.
     * 
     * @param aggHost
     *            the hostname or IP address of the aggregator.
     * @param aggPort
     *            the port number the aggregator is listening for solvers on.
     * @param distHost
     *            the hostname or IP address of the distributor.
     * @param distPort
     *            the port number the distributor is listening for solvers on.
     */
    public SimpleSamplePutter(String distHost, int distPort)
    {

	this.worldModelInterface.setHost(distHost);
	this.worldModelInterface.setPort(distPort);
	this.worldPort = distPort;
	this.worldHost = distHost;
    }

    ArrayList<String> typesList = new ArrayList<String>();

    int aliasCount = 0;

    /**
     * Connects to the distributor. Checks for user input every 100ms, and exits if the aggregator or distributor connections are ended, or the user types "exit".
     */
    @Override
    public void run()
    {
	for (int i = 0; i < types.length; i++)
	{
	    TypeSpecification spec = new TypeSpecification();
	    spec.setIsTransient(false);
	    spec.setUriName(types[i]);
	    spec.setTypeAlias(i);
	    typesList.add(types[i]);
	    this.worldModelInterface.addType(spec);
	    aliasCount = i;
	}

	this.worldModelInterface.setOriginString(ORIGIN_STRING);
	this.worldModelInterface.addConnectionListener(this);
	this.worldModelInterface.setConnectionRetryDelay(10000l);
	this.worldModelInterface.setStayConnected(true);
	this.worldModelInterface.setDisconnectOnException(true);

	/*
	 * Connect to the distributor. Note that this is being done before the aggregator to simplify the sample-handling logic in this example class.
	 */
	if (!this.worldModelInterface.doConnectionSetup())
	{
	    System.out.println("Unable to establish a connection to the distributor.");
	    System.exit(1);
	}
	System.out.println("Connected to distributor at " + this.worldModelInterface.getHost() + ":" + this.worldModelInterface.getPort());

	// Read the user input into this string
	String command = null;

	// Keep running until either we get disconnected, or the user wants to
	// quit.
	while (this.keepRunning)
	{
	    try
	    {
		// If the user has something to read in the buffer.
		if (this.userInput.ready())
		{
		    command = this.userInput.readLine();

		    // Allow the solver to shut down gracefully if the user
		    // wants to exit.
		    if ("exit".equalsIgnoreCase(command))
		    {
			System.out.println("Exiting solver (user).");
			break;
		    }
		    else
		    {
			String[] args = command.split(",");
			if (args.length >= 3)
			{
			    if (!typesList.contains(args[1]))
			    {
				System.out.println("New attribute [" + args[1] + "] detected. Adding new type spec and reconnecting.");
				typesList.add(args[1]);
				reconnect();
			    }
			    int retry = 0;
			    while (retry < 3 && !sampleReceived(command))
			    {
				++retry;
				System.out.println("Solution was not sent, retries left: " + (3 - retry));
				try
				{
				    Thread.sleep(3000l);
				} catch (Exception e)
				{
				    e.printStackTrace();
				}
			    }

			}
			else
			{
			    System.out.println("Not enough parameters.");
			    System.out.println("Usage: <Target URI>,<Attribute Name>,<Attribute Value>");
			}
			System.out.println("Sending command \"" + command + "\" to World Model.");
		    }

		}
	    } catch (IOException e1)
	    {
		System.err.println("Error reading user input.");
		e1.printStackTrace();
	    }
	    // Sleep for 100ms, since there isn't much else to do.
	    try
	    {
		Thread.sleep(100l);
	    } catch (InterruptedException e)
	    {
		// Ignored
	    }
	}
	// Stop running
	this.worldModelInterface.doConnectionTearDown();
	System.exit(0);
    }

    public void reconnect()
    {
	System.out.println("Reconnecting...");

	this.worldModelInterface.doConnectionTearDown();

	this.worldModelInterface = new SolverWorldModelInterface();
	this.worldModelInterface.setOriginString(ORIGIN_STRING);
	this.worldModelInterface.addConnectionListener(this);
	this.worldModelInterface.setConnectionRetryDelay(10000l);
	this.worldModelInterface.setStayConnected(true);
	this.worldModelInterface.setDisconnectOnException(true);
	this.worldModelInterface.setHost(worldHost);
	this.worldModelInterface.setPort(worldPort);

	for (int i = 0; i < typesList.size(); i++)
	{
	    TypeSpecification spec = new TypeSpecification();
	    spec.setIsTransient(false);
	    spec.setUriName(typesList.get(i));
	    spec.setTypeAlias(i);
	    this.worldModelInterface.addType(spec);
	}

	if (!this.worldModelInterface.doConnectionSetup())
	{
	    System.out.println("Unable to establish a connection to the distributor.");
	    System.exit(1);
	}
	System.out.println("Connected to distributor at " + this.worldModelInterface.getHost() + ":" + this.worldModelInterface.getPort());

    }

    /**
     * Sends sensed temperature to the distributor.
     */
    public boolean sampleReceived(String sample)
    {

	Collection<Solution> solution = this.generateSimpleSolution(sample);
	// No solution generated so nothing to send.
	if (solution == null)
	{
	    System.out.println("[JON] Null solution");
	    return false;
	}

	// Send the solution to the distributor
	if (!this.worldModelInterface.sendSolutions(solution))
	{
	    log.warn("Unable to send solution to distributor: {}", solution);
	    System.out.println("[JON] Unable to send solution");
	    return false;
	}
	else
	{
	    System.out.println("[JON] Solution sent to distributor");
	    return true;
	}
    }

    /**
     * Generates a very simple temperature solution based on the raw sensed data.
     * 
     * @param sample
     * @return generated solutions, or {@code null} if none were generated.
     */
    private Collection<Solution> generateSimpleSolution(String sample)
    {
	String[] args = sample.split(",");

	if (args.length < 3)
	{
	    System.out.println("Not enough parameters. (" + args.length + ")");
	    return null;
	}

	Solution solution = new Solution();

	solution.setAttributeName(args[1]);
	int aliasNo = -1;
	for (int i = 0; i < typesList.size(); i++)
	{
	    if (args[1].equals(typesList.get(i)))
	    {
		aliasNo = i;
		break;
	    }

	}
	if (aliasNo != -1)
	    solution.setAttributeNameAlias(aliasNo);
	else
	{
	    System.out.println("Attribute name not found. ");
	    return null;
	}

	try
	{
	    byte[] fakeData = args[2].getBytes("UTF-16BE");
	    solution.setData(fakeData);
	} catch (Exception e)
	{
	    System.out.println("Error in getBytes");
	}

	solution.setTime(System.currentTimeMillis());

	solution.setTargetName(args[0]);

	ArrayList<Solution> returnedList = new ArrayList<Solution>();
	returnedList.add(solution);
	return returnedList;
    }

    /**
     * Shuts down this solver because the distributor interface has "given up" on reconnecting.
     * 
     * @param distributor
     */
    public void connectionEnded(SolverWorldModelInterface worldModel)
    {

    }

    public void connectionEstablished(SolverWorldModelInterface worldModel)
    {
	// Not used
    }

    public void connectionInterrupted(SolverWorldModelInterface worldModel)
    {
	System.out.println("Connection interrupted.");
	this.canSendSolutions = false;

    }

    public void specificationMessagesSent(SolverWorldModelInterface worldModel)
    {
	System.out.println("Spec messages sent.");
	this.canSendSolutions = true;
    }

}
