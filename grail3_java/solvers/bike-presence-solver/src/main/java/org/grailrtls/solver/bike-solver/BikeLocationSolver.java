package org.grailrtls.solver;

import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;

import org.grailrtls.libsolver.SolverAggregatorInterface;
import org.grailrtls.libsolver.listeners.ConnectionListener;
import org.grailrtls.libsolver.listeners.SampleListener;
import org.grailrtls.libsolver.protocol.messages.SampleMessage;
import org.grailrtls.libsolver.protocol.messages.Transmitter;
import org.grailrtls.libsolver.rules.SubscriptionRequestRule;
import org.grailrtls.libworldmodel.client.ClientWorldConnection;
import org.grailrtls.libworldmodel.client.Response;
import org.grailrtls.libworldmodel.client.StepResponse;
import org.grailrtls.libworldmodel.client.WorldState;
import org.grailrtls.libworldmodel.client.protocol.messages.Attribute;
import org.grailrtls.libworldmodel.solver.SolverWorldModelInterface;
import org.grailrtls.libworldmodel.solver.listeners.DataListener;
import org.grailrtls.libworldmodel.solver.protocol.messages.DataTransferMessage.Solution;
import org.grailrtls.libworldmodel.solver.protocol.messages.StartTransientMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.StopTransientMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage.TypeSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BikeLocationSolver extends Thread
{
    private static final Logger log = LoggerFactory.getLogger(BikeLocationSolver.class);

    private final String TAG_ID_ATTRIBUTE = "tag_id";
    private final String HUB_ID_ATTRIBUTE = "hub_id";
    private final String HUB_LOCATION_ATTRIBUTE = "location";
    private final String BIKE_LOCATION_ATTRIBUTE = "location";
    private final String ORIGIN = "bike.location.solver";

    private ClientWorldConnection wmc;
    private Hashtable<String, Integer> bikes = new Hashtable<String, Integer>();
    private Hashtable<String, Integer> bike_hash = new Hashtable<String, Integer>();
    private Hashtable<String, BikeReaderThread> bike_reader_threads = new Hashtable<String, BikeLocationSolver.BikeReaderThread>();
    private static String aggHost;
    private static int aggPort;
    private static String wmHost;
    private static int wmPort = 7012;

    private static boolean debug = false;
    private static boolean showSamples = false, useFilter = false;

    public static void main(String[] args)
    {
	if (args.length < 5)
	{
	    System.err.println("I need at least 5 things: <World Model Host> <World Model Port> <World Model Port> <Aggregator Host> <Aggregator Port>");
	    return;
	}
	else
	{
	    for (String s : args)
	    {
		if ("d".equalsIgnoreCase(s))
		{
		    System.out.println("Showing debug messages...");
		    debug = true;
		}
		else if ("showSamples".equalsIgnoreCase(s))
		{
		    dprint("Showing incoming aggregator samples...");
		    showSamples = true;
		}
		else if ("useFilter".equalsIgnoreCase(s))
		{
		    dprint("Using subscription rule filter...");
		    useFilter = true;
		}
	    }
	}

	String host = args[0];
	wmHost = args[0];
	int port = Integer.parseInt(args[1]);
	wmPort = Integer.parseInt(args[2]);
	aggHost = args[3];
	aggPort = Integer.parseInt(args[4]);

	BikeLocationSolver bls = new BikeLocationSolver(host, port);
	bls.start();
    }

    public BikeLocationSolver(final String host, final int port)
    {
	wmc = new ClientWorldConnection();
	wmc.setHost(host);
	wmc.setPort(port);
	if (!wmc.connect())
	{
	    System.err.println("Couldn't connect to the world model!  Check your connection parameters.");
	    return;
	}
    }

    public void run()
    {
	long now = System.currentTimeMillis();
	long period = 5 * 60 * 1000; // 5 minutes
	System.out.println("Requesting from " + new Date(now) + " every " + period + " ms.");
	StepResponse response = wmc.getStreamRequest(".*bike.*", now, period, ".*");
	WorldState state = null;

	// The next line will block until the response gets at least one state
	// Streaming requests only complete when they are cancelled
	while (!response.isComplete())
	{
	    try
	    {
		state = response.next();
	    } catch (Exception e)
	    {
		System.err.println("Error occured during request: " + e);
		e.printStackTrace();
		break;
	    }
	    Collection<String> uris = state.getURIs();
	    if (uris != null)
	    {
		for (String uri : uris)
		{
		    Collection<Attribute> attribs = state.getState(uri);
		    long tagIdCreationDate = 0;
		    for (Attribute att : attribs)
		    {
			if (TAG_ID_ATTRIBUTE.equals(att.getAttributeName()) && att.getCreationDate() > tagIdCreationDate)
			{
			    try
			    {
				int tagId = Integer.parseInt(new String(att.getData(), "UTF-16BE"));
				bikes.put(uri, tagId);
				tagIdCreationDate = att.getCreationDate();
			    } catch (Exception e)
			    {
				e.printStackTrace();
			    }
			}
		    }
		}
	    }

	    // Remove old bikes or changed bikes
	    Enumeration<String> e = bike_hash.keys();
	    while (e.hasMoreElements())
	    {
		String key = (String) e.nextElement();
		if (bikes.get(key) != bike_hash.get(key))
		{
		    bike_reader_threads.get(key).interrupt();
		    bike_reader_threads.remove(key);

		    bike_hash.remove(key);
		}
	    }

	    // Add new bikes
	    e = bikes.keys();
	    while (e.hasMoreElements())
	    {
		String key = (String) e.nextElement();
		if (!bike_hash.containsKey(key))
		{
		    BikeReaderThread brThread = new BikeReaderThread(key, bikes.get(key), aggHost, aggPort, wmHost, wmPort);
		    brThread.start();
		    bike_reader_threads.put(key, brThread);

		    bike_hash.put(key, bikes.get(key));
		}
	    }
	}

	wmc.disconnect();
    }

    /**
     * Thread that retrieves samples from the aggregator and creates solutions for the corresponding bikes in the world model
     * 
     * @author Jonathan
     * 
     */
    public class BikeReaderThread extends Thread implements SampleListener, ConnectionListener,
	    org.grailrtls.libworldmodel.solver.listeners.ConnectionListener, DataListener
    {
	private final long UPDATE_INTERVAL = 0;
	private int tag_id;
	private String targetUri;
	private final SolverAggregatorInterface aggregatorInterface = new SolverAggregatorInterface();
	private final SolverWorldModelInterface worldModel = new SolverWorldModelInterface();
	private boolean running = true;
	private boolean wmReadyForSolutions = false;
	private UnknownThread uThread = null;

	public BikeReaderThread(String targetUri, int tag_id, String aggHost, int aggPort,
		String wmHost, int wmPort)
	{
	    dprint("BikeReaderThread started for Tag " + tag_id);
	    this.tag_id = tag_id;
	    this.targetUri = targetUri;
	    this.aggregatorInterface.setHost(aggHost);
	    this.aggregatorInterface.setPort(aggPort);

	    SubscriptionRequestRule[] rules = new SubscriptionRequestRule[1];
	    rules[0] = SubscriptionRequestRule.generateGenericRule();
	    rules[0].setUpdateInterval(UPDATE_INTERVAL);
	    rules[0].setPhysicalLayer((byte) 1);
	    if (useFilter)
	    {
		Transmitter transmitterFilter[] = { createTransmitter(this.tag_id) };
		rules[0].setTransmitters(transmitterFilter);
	    }
	    this.aggregatorInterface.setRules(rules);
	    this.aggregatorInterface.addSampleListener(this);
	    this.aggregatorInterface.addConnectionListener(this);
	    this.aggregatorInterface.setDisconnectOnException(true);
	    this.aggregatorInterface.setStayConnected(true);
	    this.aggregatorInterface.setConnectionRetryDelay(10000l);

	    this.worldModel.setHost(wmHost);
	    this.worldModel.setPort(wmPort);

	    this.worldModel.addConnectionListener(this);
	    this.worldModel.addDataListener(this);
	    TypeSpecification testType = new TypeSpecification();
	    testType.setIsTransient(false);
	    testType.setUriName(BIKE_LOCATION_ATTRIBUTE);
	    this.worldModel.addType(testType);
	    this.worldModel.setOriginString(ORIGIN);
	    this.worldModel.setCreateUris(true);
	    this.worldModel.setDisconnectOnException(true);
	    this.worldModel.setStayConnected(true);
	}

	public void run()
	{
	    if (!this.aggregatorInterface.doConnectionSetup())
	    {
		System.out.println("Unable to establish a connection to the aggregator.");
		System.exit(1);
	    }
	    System.out.println("Connected to aggregator at " + this.aggregatorInterface.getHost() + ":" + this.aggregatorInterface.getPort());

	    if (!this.worldModel.doConnectionSetup())
	    {
		System.out.println("Unable to establish a connection to the world model.");
		System.exit(1);
	    }
	    System.out.println("Connected to world model at " + this.worldModel.getHost() + ":" + this.worldModel.getPort());

	    this.uThread = new UnknownThread(targetUri);
	    uThread.start();
	}

	@Override
	public void sampleReceived(final SolverAggregatorInterface arg0, final SampleMessage sample)
	{
	    if (BikeLocationSolver.getPipId(sample.getDeviceId()) != this.tag_id && !useFilter)
		return;

	    if (showSamples)
		dprint("Sample Received: Device ID = " + BikeLocationSolver.getPipId(sample.getDeviceId()) + ", Receiver ID = " + BikeLocationSolver.getReceiverId(sample.getReceiverId()));

	    final long s_time = sample.getReceiverTimeStamp();
	    final int s_hub_id = BikeLocationSolver.getReceiverId(sample.getReceiverId());

	    long now = System.currentTimeMillis();
	    Response response = wmc.getSnapshot(".*hub.*", now, now, ".*");
	    dprint("Searching for Hub " + s_hub_id + " in world model...");
	    WorldState state = null;
	    try
	    {
		state = response.get();
		Collection<String> uris = state.getURIs();
		if (uris != null)
		{
		    for (String uri : uris)
		    {
			boolean matched_hub_id = false;
			byte[] data = null;

			Collection<Attribute> attribs = state.getState(uri);
			for (Attribute att : attribs)
			{
			    if (HUB_ID_ATTRIBUTE.equals(att.getAttributeName()))
			    {
				try
				{
				    if (s_hub_id == Integer.parseInt(new String(att.getData(), "UTF-16BE")))
				    {
					dprint("Hub " + s_hub_id + " found!");
					matched_hub_id = true;
				    }
				} catch (Exception e)
				{

				}
			    }
			    else if (HUB_LOCATION_ATTRIBUTE.equals(att.getAttributeName()))
			    {
				try
				{
				    if (matched_hub_id)
					dprint("Hub " + s_hub_id + " location found!");
				    String location = new String(att.getData(), "UTF-16BE");
				    if (location != "")
				    {
					data = att.getData();
				    }
				    else
				    {
					data = "Unknown".getBytes("UTF-16BE");
				    }
				} catch (UnsupportedEncodingException e)
				{
				    e.printStackTrace();
				}
			    }
			}

			// send solution if the receiverId of the sample matches the hub id attribute
			// start unknown thread
			if (data != null && matched_hub_id)
			{
			    if (this.uThread != null)
			    {
				if (this.uThread.isRunning())
				{
				    this.uThread.setRunning(false);
				    this.uThread.interrupt();
				}
			    }

			    sendSolution(this.targetUri, data, s_time);

			    this.uThread = new UnknownThread(targetUri);
			    this.uThread.start();
			}

		    }
		}
	    } catch (Exception e)
	    {
		System.err.println("Exception thrown while getting response: " + e.getMessage());
	    }
	}

	/**
	 * Sends a solution to the URI
	 * 
	 * @param targetUri
	 *            URI of object in the world model
	 * @param data
	 *            Byte array of data
	 * @param time
	 *            Creation time
	 */
	public void sendSolution(String targetUri, byte[] data, long time)
	{
	    Solution solution = new Solution();
	    solution.setTargetName(targetUri);
	    solution.setAttributeName(BIKE_LOCATION_ATTRIBUTE);
	    solution.setTime(time);
	    solution.setData(data);
	    if (this.wmReadyForSolutions)
	    {
		this.worldModel.sendSolution(solution);
		try
		{
		    dprint("Solution has been sent to " + targetUri + ": " + BIKE_LOCATION_ATTRIBUTE + " = " + new String(data, "UTF-16BE"));
		} catch (UnsupportedEncodingException e)
		{
		    e.printStackTrace();
		}
	    }
	}

	@Override
	public void connectionEnded(SolverAggregatorInterface arg0)
	{

	}

	@Override
	public void connectionEstablished(SolverAggregatorInterface arg0)
	{

	}

	@Override
	public void connectionInterrupted(SolverAggregatorInterface arg0)
	{

	}

	@Override
	public void startTransientReceived(SolverWorldModelInterface worldModel,
		StartTransientMessage message)
	{

	}

	@Override
	public void stopTransientReceived(SolverWorldModelInterface worldModel,
		StopTransientMessage message)
	{

	}

	@Override
	public void typeSpecificationsSent(SolverWorldModelInterface worldModel,
		TypeAnnounceMessage message)
	{
	    log.info("Announced type specifications. Ready to send solutions.");
	    this.wmReadyForSolutions = true;
	}

	@Override
	public void connectionInterrupted(SolverWorldModelInterface worldModel)
	{
	    log.warn("Temporarily lost connection to {}", worldModel);
	    this.wmReadyForSolutions = false;

	}

	@Override
	public void connectionEnded(SolverWorldModelInterface worldModel)
	{
	    log.error("Permanently lost connection to {}", worldModel);
	    System.exit(1);

	}

	@Override
	public void connectionEstablished(SolverWorldModelInterface worldModel)
	{
	    log.info("Connected to {}", worldModel);
	}

	/**
	 * Sends an unknown solution to the target URI every 5 minutes
	 * 
	 * @author Jonathan
	 * 
	 */
	public class UnknownThread extends Thread
	{
	    private boolean running = true;
	    private String target;
	    private final int UPDATE_INTERVAL = 5000 * 60;

	    public UnknownThread(String target)
	    {
		this.target = target;
	    }

	    public void run()
	    {
		dprint("Unknown Thread started for " + target);
		while (running)
		{
		    try
		    {
			Thread.sleep(UPDATE_INTERVAL);
			sendSolution(this.target, "Unknown".getBytes("UTF-16BE"), System.currentTimeMillis());
		    } catch (Exception e)
		    {
			dprint("Unknown Thread for " + target + " has been interrupted!");
		    }
		}
		dprint("Unknown Thread for " + target + " has been stopped");
	    }

	    public boolean isRunning()
	    {
		return running;
	    }

	    public void setRunning(boolean running)
	    {
		this.running = running;
	    }
	}

	public boolean isRunning()
	{
	    return running;
	}

	public void setRunning(boolean running)
	{
	    this.running = running;
	}
    }

    /**
     * Returns an integer representation of a 23-bit PIPSqueak device ID.
     * 
     * @param deviceId
     * @return the 23-bit PIPSqueak device ID as a 32-bit integer
     */
    private static int getPipId(byte[] deviceId)
    {
	// PIPSqueak tags only have a 23-bit ID, so we can extract the last 3
	// bytes as an integer
	return ((deviceId[SampleMessage.DEVICE_ID_SIZE - 3] << 16)) + ((deviceId[SampleMessage.DEVICE_ID_SIZE - 2] << 8)) + (deviceId[SampleMessage.DEVICE_ID_SIZE - 1]);
    }

    /**
     * Prints debugging messages
     * 
     * @param s
     *            Input string
     */
    public static void dprint(String s)
    {
	if (debug)
	    System.out.println("[DEBUG] " + s);
    }

    /**
     * Creates a Transmitter object to be used for filtering samples from the aggregator.
     * 
     * @param deviceId
     *            Device ID to be used. The ID must be between 0-255.
     * @return Transmitter object.
     */
    private Transmitter createTransmitter(int deviceId)
    {
	Transmitter t = new Transmitter();
	t.setBaseId(new byte[] { (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0,
		(byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0,
		(byte) ((deviceId & 0xFF0000) >> 16), (byte) ((deviceId & 0xFF00) >> 8),
		(byte) (deviceId & 0xFF) });
	t.setMask(new byte[] { (byte) 255, (byte) 255, (byte) 255, (byte) 255, (byte) 255,
		(byte) 255, (byte) 255, (byte) 255, (byte) 255, (byte) 255, (byte) 255, (byte) 255,
		(byte) 255, (byte) 255, (byte) 255, (byte) 255 });
	return t;
    }

    private static int getReceiverId(byte[] receiverId)
    {
	return ((receiverId[SampleMessage.DEVICE_ID_SIZE - 3] << 16) & 0xFF) + ((receiverId[SampleMessage.DEVICE_ID_SIZE - 2] << 8) & 0xFF) + (receiverId[SampleMessage.DEVICE_ID_SIZE - 1] & 0xFF);
    }
}
