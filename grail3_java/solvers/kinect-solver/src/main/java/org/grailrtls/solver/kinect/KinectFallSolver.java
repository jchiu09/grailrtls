package org.grailrtls.solver.kinect;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import org.grailrtls.libsolver.SolverAggregatorInterface;
import org.grailrtls.libsolver.listeners.ConnectionListener;
import org.grailrtls.libsolver.listeners.SampleListener;
import org.grailrtls.libsolver.protocol.messages.SampleMessage;
import org.grailrtls.libsolver.rules.SubscriptionRequestRule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A fake solver that simply prints received samples and some statistics about
 * the sample rate from the aggregator. Most useful for testing/debugging
 * purposes, but may be used as a base class for other solvers.
 * 
 * @author Xiaokang Qin
 * 
 */
public class KinectFallSolver implements ConnectionListener, SampleListener {

	/**
	 * Logging facility for this class.
	 */
	public static final Logger log = LoggerFactory.getLogger(KinectFallSolver.class);

	/**
	 * Formatter for printing floating point numbers with 4 decimal places of
	 * precision.
	 */
	private static final DecimalFormat fourPlacesFormat = new DecimalFormat("###,##0.0000");

	/**
	 * Number of samples received since the statistics were last generated.
	 */
	private int samplesReceived = 0;

	/**
	 * Mean latency of samples received from the aggregator. This value assumes
	 * that the sensors are sending valid timestamps to the aggregator.
	 */
	private float meanReceiveLatency = 0;

	/**
	 * The last time that statistics were generated.
	 */
	private long lastReportTime = System.currentTimeMillis();

	/**
	 * Number of bytes read from the aggregator since the last time statistics
	 * were generated.
	 */
	private long bytesRead = 0;

	/**
	 * Number of bytes written to the aggregator since the last time statistics
	 * were generated.
	 */
	private long bytesWritten = 0;

	/**
	 * The host name/IP address of the aggregator.
	 */
	protected String host = null;

	/**
	 * The port number of the aggregator.
	 */
	protected int port = -1;

	/**
	 * Interface to the aggregator. Handles all the network protocol stuff for
	 * us.
	 */
	protected SolverAggregatorInterface aggregatorInterface = new SolverAggregatorInterface();

	/**
	 * Parses the command-line parameters and creates a new fake solver.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		log.info("FallSolver starting up.");

		String aggHost = "";
		int aggPort = 0;

		if (args.length < 2) {
			printUsageInfo();
			System.exit(1);
		}

		aggHost = args[0];
		aggPort = Integer.parseInt(args[1]);

		KinectFallSolver solver = new KinectFallSolver();
		solver.setHost(aggHost);
		solver.setPort(aggPort);

		solver.start();

	}

	/**
	 * Prints out information about the command-line parameters and their use.
	 */
	protected static void printUsageInfo() {
		StringBuffer sb = new StringBuffer();

		sb.append("Parameters: <host> <port>");

		System.err.println(sb.toString());
	}

	/**
	 * Called when a sample is received from the aggregator. Computes and logs
	 * statistics every 10 seconds.
	 */
	ArrayList<Integer> avg15 = new ArrayList<Integer>();
	ArrayList<Integer> avg = new ArrayList<Integer>();

	public void sampleReceived(SolverAggregatorInterface aggregator, SampleMessage sampleMessage) {

		if (sampleMessage.getSensedData() == null) {
			log.warn("Whoa, got a Kinect packet without any sensed data!");
			return;
		}

		++this.samplesReceived;
		long now = System.currentTimeMillis();

		byte[] headYBytes = new byte[4];
		System.arraycopy(sampleMessage.getSensedData(), 28, headYBytes, 0, 4);
		int headY = 0;

		DataInputStream din = new DataInputStream(new ByteArrayInputStream(headYBytes));
		try {
			headY = din.readInt();
			din.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		avg15.add(headY);

		while (avg15.size() > 15) {
			avg15.remove(0);
		}

		this.meanReceiveLatency = (this.meanReceiveLatency * ((this.samplesReceived - 1f) / this.samplesReceived)) + ((now - sampleMessage.getReceiverTimeStamp()) * (1f / this.samplesReceived));
		if (now - this.lastReportTime > 1000) {

			if(now - this.lastReportTime > 10000){
				this.avg.clear();
			}
			
			this.lastReportTime = now;

			if (avg15.size() >= 15) {
				int count = 0;
				for (Integer i : avg15) {
					count += i;
				}
				avg.add(count / 15);
				
				if (avg.size() > 10) {
					avg.remove(0);
				}
				if (avg.size() >= 10) {
					testFall();
				}
			}

		}
		//log.debug("Received sample {}, {}ms delay.", sampleMessage, Long.valueOf((now - sampleMessage.getReceiverTimeStamp())));
	}

	private void testFall() {
		int[] avgs = new int[5];

		log.info(this.avg.toString());

		for (int i = 0; i < 5; i++) {
			avgs[i] = (avg.get(i) + avg.get(i + 1) + avg.get(i + 2) + avg.get(i + 3) + avg.get(i + 4)) / 5;
		}
		if (avgs[0] > avgs[1] * 1.1 && avgs[1] > avgs[2] * 1.1 && avgs[2] > avgs[3] * 1.1 && avgs[3] > avgs[4] * 1.1) {
			log.info("Kinect -> fall detected");
		}
	}

	/**
	 * Logs network statistics.
	 */
	protected void printStatistics() {
		long now = System.currentTimeMillis();

		log.info("Received {} S/s over last {}ms.", fourPlacesFormat.format(this.samplesReceived / ((now - this.lastReportTime) / 1000f)), Long.valueOf(now - this.lastReportTime));
		log.info("Mean receive latency: {}ms.", Float.valueOf(this.meanReceiveLatency));
		long newBytesRead = this.aggregatorInterface.getSession().getReadBytes();
		long newBytesWritten = this.aggregatorInterface.getSession().getWrittenBytes();
		log.info("Received {} B/s and wrote {} B/s.", fourPlacesFormat.format((double) (newBytesRead - this.bytesRead) / ((now - this.lastReportTime) / 1000f)), fourPlacesFormat.format((double) (newBytesWritten - this.bytesWritten) / ((now - this.lastReportTime) / 1000f)));
		double avgSampleSize = (double) (newBytesRead - this.bytesRead) / this.samplesReceived;
		double overhead = avgSampleSize == 0 ? 1.0 : (5.0 * this.samplesReceived) / (newBytesRead - this.bytesRead);
		log.info("Average sample size is {} B ({}% overhead)\n", fourPlacesFormat.format(avgSampleSize), fourPlacesFormat.format(overhead * 100));
		this.bytesRead = newBytesRead;
		this.bytesWritten = newBytesWritten;
		this.samplesReceived = 0;
		this.meanReceiveLatency = 0;
		this.lastReportTime = now;
	}

	/**
	 * Logs a warning message. The solver will exit once the connection to the
	 * aggregator is closed.
	 */
	@Override
	public void connectionEnded(SolverAggregatorInterface aggregator) {
		log.warn("Lost connection to {}.", aggregator);
	}

	/**
	 * Logs a message about the connection opening.
	 */
	@Override
	public void connectionEstablished(SolverAggregatorInterface aggregator) {
		log.info("Connected to {}.", aggregator);
	}

	/**
	 * Logs a message about the connection being interrupted.
	 */
	@Override
	public void connectionInterrupted(SolverAggregatorInterface aggregator) {
		log.info("Temporarily lost connection to {}.", aggregator);
	}

	/**
	 * Starts the aggregator interface.
	 */
	public void start() {
		if (this.host == null || this.port < 0) {
			log.warn("Missing one or more required parameters.");
			return;
		}

		this.aggregatorInterface.setHost(this.host);
		this.aggregatorInterface.setPort(this.port);
		this.aggregatorInterface.addConnectionListener(this);
		this.aggregatorInterface.addSampleListener(this);

		SubscriptionRequestRule rule = SubscriptionRequestRule.generateGenericRule();
		rule.setPhysicalLayer((byte) 4);
		this.aggregatorInterface.setRules(new SubscriptionRequestRule[] { rule });

		log.debug("Attempting connection to {}:{}", this.host, Integer.valueOf(this.port));

		if (!this.aggregatorInterface.doConnectionSetup()) {
			log.warn("Aggregator connection failed.");
			return;
		}
	}

	/**
	 * Sets the host/IP address for the aggregator interface.
	 * 
	 * @param host
	 *            the host name or IP address of the aggregator.
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * Sets the port number for the aggregator interface.
	 * 
	 * @param port
	 *            the port number of the aggregator.
	 */
	public void setPort(int port) {
		this.port = port;
	}

	public SolverAggregatorInterface getAggregatorInterface() {
		return aggregatorInterface;
	}
}
