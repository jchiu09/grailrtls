package org.grailrtls.solver.kinect;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import org.grailrtls.libsolver.SolverAggregatorInterface;
import org.grailrtls.libsolver.listeners.ConnectionListener;
import org.grailrtls.libsolver.listeners.SampleListener;
import org.grailrtls.libsolver.protocol.messages.SampleMessage;
import org.grailrtls.libsolver.rules.SubscriptionRequestRule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A solver that detect if someone is drinking.
 * 
 * @author Xiaokang Qin
 * 
 */
public class KinectDrinkSolver implements ConnectionListener, SampleListener {

	/**
	 * Logging facility for this class.
	 */
	public static final Logger log = LoggerFactory.getLogger(KinectDrinkSolver.class);

	/**
	 * Formatter for printing floating point numbers with 4 decimal places of
	 * precision.
	 */
	private static final DecimalFormat fourPlacesFormat = new DecimalFormat("###,##0.0000");

	/**
	 * Number of samples received since the statistics were last generated.
	 */
	private int samplesReceived = 0;

	/**
	 * Mean latency of samples received from the aggregator. This value assumes
	 * that the sensors are sending valid timestamps to the aggregator.
	 */
	private float meanReceiveLatency = 0;

	/**
	 * The last time that statistics were generated.
	 */
	private long lastReportTime = System.currentTimeMillis();

	/**
	 * Number of bytes read from the aggregator since the last time statistics
	 * were generated.
	 */
	private long bytesRead = 0;

	/**
	 * Number of bytes written to the aggregator since the last time statistics
	 * were generated.
	 */
	private long bytesWritten = 0;

	/**
	 * The host name/IP address of the aggregator.
	 */
	protected String host = null;

	/**
	 * The port number of the aggregator.
	 */
	protected int port = -1;

	/**
	 * Interface to the aggregator. Handles all the network protocol stuff for
	 * us.
	 */
	protected SolverAggregatorInterface aggregatorInterface = new SolverAggregatorInterface();

	/**
	 * Parses the command-line parameters and creates a new fake solver.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		log.info("DrinkSolver starting up.");

		String aggHost = "";
		int aggPort = 0;

		if (args.length < 2) {
			printUsageInfo();
			System.exit(1);
		}

		aggHost = args[0];
		aggPort = Integer.parseInt(args[1]);

		KinectDrinkSolver solver = new KinectDrinkSolver();
		solver.setHost(aggHost);
		solver.setPort(aggPort);

		solver.start();

	}

	/**
	 * Prints out information about the command-line parameters and their use.
	 */
	protected static void printUsageInfo() {
		StringBuffer sb = new StringBuffer();

		sb.append("Parameters: <host> <port>");

		System.err.println(sb.toString());
	}

	/**
	 * Called when a sample is received from the aggregator. Computes and logs
	 * statistics every 10 seconds.
	 */
	ArrayList<Integer> avgR = new ArrayList<Integer>();
	ArrayList<Integer> avgL = new ArrayList<Integer>();
	ArrayList<Integer> secR = new ArrayList<Integer>();
	ArrayList<Integer> secL = new ArrayList<Integer>();

	public void sampleReceived(SolverAggregatorInterface aggregator, SampleMessage sampleMessage) {

		if (sampleMessage.getSensedData() == null) {
			log.warn("Whoa, got a Kinect packet without any sensed data!");
			return;
		}

		this.samplesReceived++;
		long now = System.currentTimeMillis();

		byte[] coordBytes = new byte[24];
		
		System.arraycopy(sampleMessage.getSensedData(), 24, coordBytes, 0, 8);
		System.arraycopy(sampleMessage.getSensedData(), 56, coordBytes, 8, 8);
		System.arraycopy(sampleMessage.getSensedData(), 88, coordBytes, 16, 8);
		int headX = 0, headY = 0, handLX = 0, handLY = 0, handRX = 0, handRY = 0;

		try {

			DataInputStream din = new DataInputStream(new ByteArrayInputStream(coordBytes));
			headX = din.readInt();
			headY = din.readInt();
			
			handLX = din.readInt();
			handLY = din.readInt();
			
			handRX = din.readInt();
			handRY = din.readInt();
			din.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		

		avgL.add((int)Math.sqrt(Math.pow(Math.abs(headX - handLX), 2) + Math.pow(Math.abs(headY - handLY), 2)));
		avgR.add((int)Math.sqrt(Math.pow(Math.abs(headX - handRX), 2) + Math.pow(Math.abs(headY - handRY), 2)));
		if (avgL.size() > 15) {
			avgL.remove(0);
			avgR.remove(0);
		}
		
		assert(avgL.size()==avgR.size());

		this.meanReceiveLatency = (this.meanReceiveLatency * ((this.samplesReceived - 1f) / this.samplesReceived)) + ((now - sampleMessage.getReceiverTimeStamp()) * (1f / this.samplesReceived));
		if (now - this.lastReportTime > 1000) {

			if(now - this.lastReportTime > 10000){
				this.avgL.clear();
				this.avgR.clear();
			}
			
			this.lastReportTime = now;

			if (avgL.size() >= 15) {
				int count = 0;
				for (Integer i : avgL) {
					count += i;
				}
				secL.add(count / 15);
				
				count = 0;
				for(Integer i: avgR){
					count += i;
				}
				secR.add(count /15);
				
				if (secL.size() > 3) {
					secL.remove(0);
					secR.remove(0);
				}
				
				assert(secL.size()==secR.size());
				
				if (secL.size() >= 3) {
					testDrink();
				}
				
				assert(secL.size()==secR.size());
			}

		}
		//log.debug("Received sample {}, {}ms delay.", sampleMessage, Long.valueOf((now - sampleMessage.getReceiverTimeStamp())));
	}

	private void testDrink() {
		int sumL = 0, sumR = 0;
		for(Integer i: secL){
			sumL += i;
		}
		for(Integer i: secR){
			sumR += i;
		}
		log.info("left sum: "+sumL+". right sum: "+sumR);
		if(sumL<=150 && sumR<=150){
			log.info("Kinect -> drink detected(both hands?)");
			secL.clear();
			secR.clear();
		}else if(sumL<=150){
			log.info("Kinect -> drink detected(left hand)");
			secL.clear();
			secR.clear();
		}else if(sumR<=150){
			log.info("Kinect -> drink detected(right hand)");
			secL.clear();
			secR.clear();
		}
	}

	/**
	 * Logs network statistics.
	 */
	protected void printStatistics() {
		long now = System.currentTimeMillis();

		log.info("Received {} S/s over last {}ms.", fourPlacesFormat.format(this.samplesReceived / ((now - this.lastReportTime) / 1000f)), Long.valueOf(now - this.lastReportTime));
		log.info("Mean receive latency: {}ms.", Float.valueOf(this.meanReceiveLatency));
		long newBytesRead = this.aggregatorInterface.getSession().getReadBytes();
		long newBytesWritten = this.aggregatorInterface.getSession().getWrittenBytes();
		log.info("Received {} B/s and wrote {} B/s.", fourPlacesFormat.format((double) (newBytesRead - this.bytesRead) / ((now - this.lastReportTime) / 1000f)), fourPlacesFormat.format((double) (newBytesWritten - this.bytesWritten) / ((now - this.lastReportTime) / 1000f)));
		double avgSampleSize = (double) (newBytesRead - this.bytesRead) / this.samplesReceived;
		double overhead = avgSampleSize == 0 ? 1.0 : (5.0 * this.samplesReceived) / (newBytesRead - this.bytesRead);
		log.info("Average sample size is {} B ({}% overhead)\n", fourPlacesFormat.format(avgSampleSize), fourPlacesFormat.format(overhead * 100));
		this.bytesRead = newBytesRead;
		this.bytesWritten = newBytesWritten;
		this.samplesReceived = 0;
		this.meanReceiveLatency = 0;
		this.lastReportTime = now;
	}

	/**
	 * Logs a warning message. The solver will exit once the connection to the
	 * aggregator is closed.
	 */
	@Override
	public void connectionEnded(SolverAggregatorInterface aggregator) {
		log.warn("Lost connection to {}.", aggregator);
	}

	/**
	 * Logs a message about the connection opening.
	 */
	@Override
	public void connectionEstablished(SolverAggregatorInterface aggregator) {
		log.info("Connected to {}.", aggregator);
	}

	/**
	 * Logs a message about the connection being interrupted.
	 */
	@Override
	public void connectionInterrupted(SolverAggregatorInterface aggregator) {
		log.info("Temporarily lost connection to {}.", aggregator);
	}

	/**
	 * Starts the aggregator interface.
	 */
	public void start() {
		if (this.host == null || this.port < 0) {
			log.warn("Missing one or more required parameters.");
			return;
		}

		this.aggregatorInterface.setHost(this.host);
		this.aggregatorInterface.setPort(this.port);
		this.aggregatorInterface.addConnectionListener(this);
		this.aggregatorInterface.addSampleListener(this);

		SubscriptionRequestRule rule = SubscriptionRequestRule.generateGenericRule();
		rule.setPhysicalLayer((byte) 4);
		this.aggregatorInterface.setRules(new SubscriptionRequestRule[] { rule });

		log.debug("Attempting connection to {}:{}", this.host, Integer.valueOf(this.port));

		if (!this.aggregatorInterface.doConnectionSetup()) {
			log.warn("Aggregator connection failed.");
			return;
		}
	}

	/**
	 * Sets the host/IP address for the aggregator interface.
	 * 
	 * @param host
	 *            the host name or IP address of the aggregator.
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * Sets the port number for the aggregator interface.
	 * 
	 * @param port
	 *            the port number of the aggregator.
	 */
	public void setPort(int port) {
		this.port = port;
	}

	public SolverAggregatorInterface getAggregatorInterface() {
		return aggregatorInterface;
	}
}
