/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Sumedh Sawant
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package org.grailrtls.solver.chair_stolen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collection;

import org.grailrtls.libworldmodel.client.ClientWorldModelInterface;
import org.grailrtls.libworldmodel.client.listeners.ConnectionListener;
import org.grailrtls.libworldmodel.client.listeners.DataListener;
import org.grailrtls.libworldmodel.client.protocol.messages.AbstractRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.Attribute;
import org.grailrtls.libworldmodel.client.protocol.messages.AttributeAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.AttributeAliasMessage.AttributeAlias;
import org.grailrtls.libworldmodel.client.protocol.messages.DataResponseMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginPreferenceMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.StreamRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.URISearchResponseMessage;
import org.grailrtls.libworldmodel.solver.SolverWorldModelInterface;
import org.grailrtls.libworldmodel.solver.protocol.messages.DataTransferMessage.Solution;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage.TypeSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that calculates a "Chair Stolen" solution utilizing mobility solutions,
 * location solutions and thresholding.
 * 
 * @author Sumedh Sawant
 */
public class ChairStolenSolver extends Thread implements DataListener,
		ConnectionListener,
		org.grailrtls.libworldmodel.solver.listeners.ConnectionListener {

	/**
	 * Logging facility for this class.
	 */
	private static final Logger log = LoggerFactory
			.getLogger(ChairStolenSolver.class);

	/**
	 * Keep track of the solution URI name.
	 */
	private static final String SOLUTION_URI_NAME = "stolen";

	/**
	 * Region URI.
	 */
	private static final String SOLUTION_REGION_URI = "Winlab";

	private static final String SOLVER_ORIGIN_STRING = "sumedh.sawant";

	/**
	 * Used to get user input.
	 */
	private BufferedReader userInput = new BufferedReader(
			new InputStreamReader(System.in));

	/**
	 * Flag to keep running until the connection is closed.
	 */
	private boolean keepRunning = true;

	/**
	 * Interface to the distributor as a client, takes care of all the basic
	 * protocol stuff.
	 */
	private ClientWorldModelInterface clientWMI = new ClientWorldModelInterface();

	/**
	 * Interface to the distributor for publishing solutions. Takes care of all
	 * the basic protocol stuff.
	 */
	private SolverWorldModelInterface solverWMI = new SolverWorldModelInterface();

	private int locationAlias = -1;
	private int mobilityAlias = -1;
	private double averageX = 0;
	private double averageY = 0;
	private double averageA = 0;
	private double averageB = 0;
	private int locationSolutionCount = 0;
	private static final double THRESHOLD_DIFFERENCE = 5;
	private static final int ACCUMULATION_LOWER_BOUND = 5;
	private boolean chairMovedRecently = false;
	private Timer timer = new Timer();
	private boolean seenOne = false;
	private long oneTime = 0l;
	private long chairMovementTime = 0l;
	private String chairTagID = "";
	private String chairURI = "";

	/**
	 * Creates a new ChairStolenSolver and starts it.
	 */
	public static void main(String[] args) {
		if (args.length != 6) {
			printUsageInfo();
			System.exit(1);
		}

		int clientDistPort = Integer.parseInt(args[1]);
		int solverDistPort = Integer.parseInt(args[3]);

		ChairStolenSolver solver = new ChairStolenSolver(args[0],
				clientDistPort, args[2], solverDistPort, args[4], args[5]);
		solver.start();
	}

	/**
	 * Prints a simple message about the parameters for this program.
	 */
	protected static void printUsageInfo() {
		System.out
				.println("Requires six parameters: <ClientDistributor host> <ClientDistributor port> <SolverDistributor host> <SolverDistributor port> <Chair Tag ID> <Chair URI Name>.");
		System.out.println("Once running, type \"exit\" to exit.");
	}

	/**
	 * CONSTRUCTOR
	 * 
	 * Creates a new "ChairStolenSolver" that connects to the distributor and
	 * world server at the specified host name/IP addresses and port numbers.
	 * 
	 * @param clientDistHost
	 *            the host-name or IP address of the distributor.
	 * @param clientDistPort
	 *            the port-number the distributor is listening for clients on.
	 * @param solverDistHost
	 *            the host-name or IP address of the distributor
	 * @param solverDistPort
	 *            the port-number the distributor is listening for solvers on.
	 * @param tagID
	 *            the ID number of the tag that is attached to the chair
	 * @param URI
	 *            the URI String that corresponds to the chair in the world
	 *            server
	 * 
	 */
	public ChairStolenSolver(String clientDistHost, int clientDistPort,
			String solverDistHost, int solverDistPort, String tagID, String URI) {
		this.clientWMI.setHost(clientDistHost);
		this.clientWMI.setPort(clientDistPort);
		this.solverWMI.setHost(solverDistHost);
		this.solverWMI.setPort(solverDistPort);
		this.chairTagID = tagID;
		this.chairURI = URI;
	}

	/**
	 * Connects to the distributor as a client and as a solver. Checks for user
	 * input every 100ms, and exits if the connection is ended, or the user
	 * types "exit".
	 */

	@Override
	public void run() {
		/*
		 * "Client Distributor" interface set-up.
		 */

		// Add this class as a dataTransferMessage listener so that the data can
		// be used to generate solutions.
		this.clientWMI.addDataListener(this);

		// Add this class as a connection listener to know when the connection
		// closes and also when the type specification message has been sent.
		this.clientWMI.addConnectionListener(this);

		// Disconnect if we have any problems
		this.clientWMI.setDisconnectOnException(true);

		// Don't try to reconnect after a disconnect
		this.clientWMI.setStayConnected(true);

		// Wait 30 seconds between attempts to connect to the distributor as a
		// client.
		this.clientWMI.setConnectionRetryDelay(10000l);

		/*
		 * "Solver Distributor" interface set-up.
		 */

		// Add the solution type URI name to the distributor interface
		TypeSpecification spec = new TypeSpecification();
		spec.setIsTransient(false);
		spec.setUriName(SOLUTION_URI_NAME);
		this.solverWMI.addType(spec);
		this.solverWMI.addConnectionListener(this);

		// Add this class as a connection listener to know when the connection
		// closes
		// this.solverDistributorInterface.addConnectionListener(this);

		// Set the time between connection attempts to 10s.
		this.solverWMI.setConnectionRetryDelay(10000l);

		// Try to stay connected to the distributor.
		this.solverWMI.setStayConnected(true);

		// Close the current connection if there is a protocol exception.
		this.solverWMI.setDisconnectOnException(true);
		
		this.solverWMI.setOriginString(SOLVER_ORIGIN_STRING);

		// Connect to the distributor as a solver.
		if (!this.solverWMI.doConnectionSetup()) {
			System.out
					.println("Unable to establish a connection to the distributor as a solver.");
			System.exit(1);
		}
		System.out.println("Connected to distributor as a solver at "
				+ this.solverWMI.getHost() + ":"
				+ this.solverWMI.getPort());

		// Connect to the distributor as a client.
		if (!this.clientWMI.doConnectionSetup()) {
			System.out
					.println("Unable to establish a connection to the distributor as a client.");
			System.exit(1);
		}
		System.out.println("Connected to Distributor as a client at "
				+ this.clientWMI.getHost() + ":"
				+ this.clientWMI.getPort());

		// Read the user input into this string
		String command = null;

		// Keep running until either we get disconnected, or the user wants to
		// quit.
		while (this.keepRunning) {
			try {
				// If the user has something to read in the buffer.
				if (this.userInput.ready()) {
					command = this.userInput.readLine();

					// Allow the solver to shut down gracefully if the user
					// wants to exit.
					if ("exit".equalsIgnoreCase(command)) {
						System.out.println("Exiting solver (user).");
						break;
					} else {
						System.out.println("Unknown command \"" + command
								+ "\". Type \"exit\" to exit.");
					}
				}
			} catch (IOException e1) {
				System.err.println("Error reading user input.");
				e1.printStackTrace();
			}
			// Sleep for 100ms, since there isn't much else to do.
			try {
				Thread.sleep(100l);
				// Most "Recent" chair movement is only valid for 30 seconds
				if (this.timer.elapsed() >= 60000l
						&& this.timer.elapsed() <= 60500l) {
					this.timer.reset();
					this.chairMovedRecently = false;
					this.chairMovementTime = 0l;
					log.info("Timer was reset. ChairMovedRecently was set to false.");
				}
			} catch (InterruptedException e) {
				// Ignored
			}
		}
		// Stop running
		this.clientWMI.doConnectionTearDown();
		this.solverWMI.doConnectionTearDown();
		System.exit(0);
	}

	/**
	 * This method updates the moving averages used in calculating whether the
	 * chair is stolen or not.
	 * 
	 * @param locationX
	 * @param locationY
	 * @param locationA
	 * @param locationB
	 */
	private void updateAverages(double locationX, double locationY,
			double locationA, double locationB) {
		double xTotal = (this.averageX * (this.locationSolutionCount - 1))
				+ locationX;
		double yTotal = (this.averageY * (this.locationSolutionCount - 1))
				+ locationY;
		double aTotal = (this.averageA * (this.locationSolutionCount - 1))
				+ locationA;
		double bTotal = (this.averageB * (this.locationSolutionCount - 1))
				+ locationB;

		this.averageX = (double) xTotal / this.locationSolutionCount;
		this.averageY = (double) yTotal / this.locationSolutionCount;
		this.averageA = (double) aTotal / this.locationSolutionCount;
		this.averageB = (double) bTotal / this.locationSolutionCount;
		log.info("New Average Locations: " + this.averageX + ", "
				+ this.averageY + ", " + this.averageA + ", " + this.averageB);
	}

	/**
	 * Compares A Location Solution received to moving average of location
	 * solutions
	 * 
	 * @param locationX
	 * @param locationY
	 * @param locationA
	 * @param locationB
	 * 
	 * @return (number of conditions that were satisfied)
	 */
	private int checkConditions(double locationX, double locationY,
			double locationA, double locationB) {
		int conditionSatisfiedCount = 0;

		if ((locationX > this.averageX + ChairStolenSolver.THRESHOLD_DIFFERENCE)
				|| (locationX < this.averageX
						- ChairStolenSolver.THRESHOLD_DIFFERENCE)) {
			conditionSatisfiedCount++;
		}

		if ((locationY > this.averageY + ChairStolenSolver.THRESHOLD_DIFFERENCE)
				|| (locationY < this.averageY
						- ChairStolenSolver.THRESHOLD_DIFFERENCE)) {
			conditionSatisfiedCount++;
		}

		if ((locationA > this.averageA + ChairStolenSolver.THRESHOLD_DIFFERENCE)
				|| (locationA < this.averageA
						- ChairStolenSolver.THRESHOLD_DIFFERENCE)) {
			conditionSatisfiedCount++;
		}

		if ((locationB > this.averageB + ChairStolenSolver.THRESHOLD_DIFFERENCE)
				|| (locationB < this.averageB
						- ChairStolenSolver.THRESHOLD_DIFFERENCE)) {
			conditionSatisfiedCount++;
		}

		return conditionSatisfiedCount;
	}

	/**
	 * Sends a solution to the distributor containing the time the chair was
	 * stolen.
	 */
	private void sendSolution() {
		Solution solution = new Solution();
		
		solution.setTargetName(this.chairURI);
		solution.setTime(System.currentTimeMillis());
		solution.setAttributeName(SOLUTION_URI_NAME);

		ByteBuffer buff = ByteBuffer.allocate(8);
		buff.putLong(System.currentTimeMillis());

		solution.setData(buff.array());

		Collection<Solution> returnedList = new ArrayList<Solution>();
		returnedList.add(solution);

		if (!this.solverWMI.sendSolutions(
				returnedList)) {
			log.warn("Unable to send solution to distributor: {}", returnedList);
		}
		log.info("SENT CHAIR STOLEN SOLUTION!");
	}

	/**
	 * Used to reset the solver.
	 */
	private void resetSolver() {
		this.averageA = 0;
		this.averageB = 0;
		this.averageX = 0;
		this.averageY = 0;
		this.locationSolutionCount = 0;
		this.seenOne = false;
		this.chairMovedRecently = false;
		this.chairMovementTime = 0l;
		this.oneTime = 0l;
		this.timer.reset();
		log.info("Solver was reset.");
	}

	/**
	 * Implements a simple Stop-Watch/Timer type class based on Java wall-clock
	 * time.
	 * 
	 * RUNNING() == true <==> start() called with no corresponding call to
	 * stop() All times are given in units of milliseconds.
	 */
	private class Timer {
		private boolean running;
		private long tStart;
		private long tFinish;
		private long tAccum; // total time

		public Timer() {
			reset(); // Initializes the timer to 0 ms.
		}

		/**
		 * Starts the timer. Accumulates time across multiple calls to start.
		 */
		public void start() {
			running = true;
			tStart = System.currentTimeMillis();
			tFinish = tStart;
		}

		/**
		 * Stops the timer. returns the time elapsed since the last matching
		 * call to start(), or 0 if no such matching call was made.
		 */
		@SuppressWarnings("unused")
		public long stop() {

			tFinish = System.currentTimeMillis();

			if (running) {
				running = false;
				long diff = (tFinish - tStart);
				tAccum += diff;
				return diff;
			}

			return 0;
		}

		/**
		 * if RUNNING() ==> returns the time since last call to start() if
		 * !RUNNING() ==> returns total elapsed time
		 */
		public long elapsed() {
			if (running)
				return System.currentTimeMillis() - tStart;

			return tAccum;
		}

		/**
		 * Stops timing, if currently RUNNING(); resets accumulated time to 0.
		 */
		public void reset() {
			running = false;
			tStart = tFinish = 0;
			tAccum = 0;
		}

	}

	@Override
	public void connectionInterrupted(SolverWorldModelInterface worldModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connectionEnded(SolverWorldModelInterface worldModel) {
		// Stop running after the connection is finally closed.
		System.out.println("Connection closed.");
		this.keepRunning = false;
		this.interrupt();
	}

	@Override
	public void connectionEstablished(SolverWorldModelInterface worldModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connectionInterrupted(ClientWorldModelInterface worldModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connectionEnded(ClientWorldModelInterface worldModel) {
		// Stop running after the connection is finally closed.
		System.out.println("Connection closed.");
		this.keepRunning = false;
		this.interrupt();
	}

	@Override
	public void connectionEstablished(ClientWorldModelInterface worldModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void requestCompleted(ClientWorldModelInterface worldModel,
			AbstractRequestMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dataResponseReceived(ClientWorldModelInterface worldModel,
			DataResponseMessage message) {
		// TODO Auto-generated method stub
		Attribute[] solutionArray = message.getAttributes();
		
		for (int i = 0; i < solutionArray.length; i++) {
			if (this.chairTagID.equalsIgnoreCase(message.getUri())) {
				log.debug("Solution Received {}", message.getUri());

				// If a location solution is received
				if (solutionArray[i].getAttributeNameAlias() == this.locationAlias) {
					this.locationSolutionCount++;

					byte[] b = solutionArray[i].getData();
					ByteBuffer bb = ByteBuffer.wrap(b);
					double locationX = Math.round(bb.getDouble());
					double locationY = Math.round(bb.getDouble(8));
					double locationA = Math.round(bb.getDouble(16));
					double locationB = Math.round(bb.getDouble(24));

					log.info("Chair Position: " + locationX + ", " + locationY
							+ "   " + locationA + ", " + locationB);

					// Compare Location Solution to moving average of location
					// solutions
					int conditionSatisfiedCount = this.checkConditions(
							locationX, locationY, locationA, locationB);

					// If at least 1 is significantly different and the chair
					// moved recently, the chair was stolen
					// Also checks to make sure at least 5 location solutions
					// have been considered
					// Also assumes people take at least 5 seconds to
					// "Steal a Chair"
					if (conditionSatisfiedCount >= 1
							&& (this.chairMovedRecently)
							&& (locationSolutionCount >= ChairStolenSolver.ACCUMULATION_LOWER_BOUND)) {
						if (this.chairMovementTime >= 5000l) {
							log.info("CHAIR WAS STOLEN!");

							// send the solution which contains the time stolen
							this.sendSolution();

							// reset the solver so it takes this new placement
							// of the chair as its "rightful position"
							this.resetSolver();
							return;
						}
					}

					// Called to update the averages.
					this.updateAverages(locationX, locationY, locationA,
							locationB);

				}

				// If a mobility solution is received
				if (solutionArray[i].getAttributeNameAlias() == this.mobilityAlias) {

					// If any movement happens before the averageX and averageY
					// values have been accumulated significantly
					// enough, then reset the averages and locationSolution
					// count.
					if (this.locationSolutionCount < ChairStolenSolver.ACCUMULATION_LOWER_BOUND) {
						this.resetSolver();
					}

					if (solutionArray[i].getData()[0] == (byte) 1) {
						log.info("Chair Moved!");
						this.seenOne = true;
						this.chairMovedRecently = true;
						this.oneTime = System.currentTimeMillis();
					} else {
						if (this.seenOne) {
							long temp = System.currentTimeMillis()
									- this.oneTime;
							if (temp > this.chairMovementTime) {
								this.chairMovementTime = temp;
							}
						}
						this.timer.start();
						log.info("Timer started!");
						this.seenOne = false;
					}
					log.info("Total Chair Movement Time So Far: "
							+ (int) this.chairMovementTime / 1000 + " seconds.");
				}

				log.info("The location solution count is: "
						+ this.locationSolutionCount);
			}
		}
	}

	@Override
	public void uriSearchResponseReceived(ClientWorldModelInterface worldModel,
			URISearchResponseMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void attributeAliasesReceived(
			ClientWorldModelInterface clientWorldModelInterface,
			AttributeAliasMessage message) {
		StreamRequestMessage request = new StreamRequestMessage();
		request.setBeginTimestamp(0l);
		request.setUpdateInterval(0l);
		request.setQueryURI(".*");

		AttributeAlias[] types = message.getAliases();
		ArrayList<String> aliases = new ArrayList<String>();

		for (AttributeAlias type : types) {

			log.info(type.aliasName + "->" + type.aliasNumber);

			if ("mobility".equalsIgnoreCase(type.aliasName)) {
				aliases.add(type.aliasName);
				this.mobilityAlias = type.aliasNumber;
			}
			if ("location".equalsIgnoreCase(type.aliasName)) {
				aliases.add(type.aliasName);
				this.locationAlias = type.aliasNumber;
			}
		}
		
		if (aliases.size() == 0) {
			log.error("FAILED TO GENERATE DATA REQUEST MESSAGE");
			System.exit(1);
		}
		
		
		request.setQueryAttributes(aliases.toArray(new String[]{}));


		this.clientWMI.sendMessage(request);
	}

	@Override
	public void originAliasesReceived(
			ClientWorldModelInterface clientWorldModelInterface,
			OriginAliasMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void originPreferenceSent(ClientWorldModelInterface worldModel,
			OriginPreferenceMessage message) {
		// TODO Auto-generated method stub
		
	}
}
