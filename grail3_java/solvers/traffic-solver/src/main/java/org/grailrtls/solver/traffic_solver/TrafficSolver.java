/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Sumedh Sawant
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package org.grailrtls.solver.traffic_solver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collection;

import org.grailrtls.libworldmodel.solver.SolverWorldModelInterface;
import org.grailrtls.libworldmodel.solver.listeners.ConnectionListener;
import org.grailrtls.libworldmodel.solver.protocol.messages.DataTransferMessage.Solution;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage.TypeSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A GRAIL RTLS solver that pulls traffic information(severity [0-10], description) from the 
 * MapQuest Traffic Web Service using an HTTP GET request. Can be run utilizing the postal zip 
 * code or the city&state of the region in which you desire traffic alerts.
 * 
 * Submits solutions every 15 minutes to the distributor where the first byte of data is the 
 * severity of the alert, and the rest of the bytes are the description.
 * 
 * @author Sumedh Sawant
 *
 */
public class TrafficSolver extends Thread implements
	ConnectionListener{

	/**
     * Logging facility for this class.
     */
    private static final Logger log = LoggerFactory.getLogger(TrafficSolver.class);
    
    /**
     * Keep track of the solution URI name.
     */
    private static final String SOLUTION_URI_NAME = "traffic";

    /**
     * Region URI.
     */
    private static final String SOLUTION_REGION_URI = "Winlab";
    
    private static final String SOLVER_ORIGIN_STRING = "sumedh.sawant";
    
    
    /**
     * Used to get user input.
     */
    private BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));

    /**
     * Flag to keep running until the connection is closed.
     */
    private boolean keepRunning = true;
    
    /**
     * Interface to the distributor for publishing solutions. Takes care of all
     * the basic protocol stuff.
     */
    private SolverWorldModelInterface solverWMI = new SolverWorldModelInterface();
    
    private MapquestTrafficService trafficService = null;
    
    private static final String MAPQUEST_KEY = "Fmjtd%7Cluu2256rn1%2C2x%3Do5-huzsg";
    
    private int timerCount = 1;
    
    private Timer timer = new Timer();
    
    private boolean starting = true;
    
    /**
     * Creates a new TrafficSolver and starts it.
     */
    public static void main(String[] args) {
        if (args.length > 4) {
            printUsageInfo();
            System.exit(1);
        }

        int solverDistPort = Integer.parseInt(args[1]);
        char[] thirdArg = args[2].toCharArray();
        
        boolean isNumber = true;
        
        for(char character: thirdArg){
        	if(!Character.isDigit(character)){
        		isNumber = false;
        	}
        }
        
        TrafficSolver solver = null;
        
        if(isNumber){
        	if(thirdArg.length != 5){
        		System.out.println("Please enter a valid zip code.");
        		System.exit(1);
        	}
        
        	solver = new TrafficSolver(args[0], solverDistPort, args[2]);
        }
        else{
        	if(args.length != 4){
        		printUsageInfo();
                System.exit(1);
        	}
        	solver = new TrafficSolver(args[0], solverDistPort, args[2], args[3]);
        }
        
        
        if(solver != null){
        	solver.start();
        }
    }

    /**
     * Prints a simple message about the parameters for this program.
     */
    protected static void printUsageInfo() {
    	System.out.println("Requires 3 or 4 parameters. Two options are given below:");
    	System.out.println("4 parameters: <SolverDistributor host> <SolverDistributor port> <City> <State>");
        System.out.println("3 parameters: <SolverDistributor host> <SolverDistributor port> <Postal Zip Code>.");
        System.out.println("Once running, type \"exit\" to exit.");
    }
    
    /**
     * CONSTRUCTOR
     * 
     * Creates a new "TrafficSolver" that connects to the
     * distributor and world server at the specified host name/IP addresses and port numbers. 
	 *
     * @param solverDistHost
     * 			  the host-name or IP address of the distributor
     * @param solverDistPort
     * 			  the port-number the distributor is listening for solvers on.  
     * @param zipCode
     *   		  the postal zip code of the region traffic alerts are needed for.
     */
    public TrafficSolver(String solverDistHost, int solverDistPort, String zipCode){
        this.solverWMI.setHost(solverDistHost);
        this.solverWMI.setPort(solverDistPort);
        this.trafficService = new MapquestTrafficService(zipCode, TrafficSolver.MAPQUEST_KEY);
    }
    
    /**
     * Second CONSTRUCTOR
     * 
     * Creates a new "TrafficSolver" that connects to the
     * distributor and world server at the specified host name/IP addresses and port numbers. 
     * 
     * @param solverDistHost
     * 			  the host-name or IP address of the distributor
     * @param solverDistPort
     * 			  the port-number the distributor is listening for solvers on.  
     * @param city
     * 			  the city of the region traffic alerts are needed for.
     * @param state
     * 			  the state of the region traffic alerts are needed for.
     */
    public TrafficSolver(String solverDistHost, int solverDistPort, String city, String state){
    	 this.solverWMI.setHost(solverDistHost);
         this.solverWMI.setPort(solverDistPort);
         this.trafficService = new MapquestTrafficService(city, state, TrafficSolver.MAPQUEST_KEY);
    }

    /**
     * Connects to the distributor as a solver. Checks for user
     * input every 100ms, and exits if the connection is ended, or
     * the user types "exit".
     */
    @Override
    public void run() {
        
        /*
         * "Solver Distributor" interface set-up.
         */

        // Add the solution type URI name to the distributor interface
    	TypeSpecification spec = new TypeSpecification();
    	spec.setIsTransient(false);
    	spec.setUriName(SOLUTION_URI_NAME);
        this.solverWMI.addType(spec);

        // Add this class as a connection listener to know when the connection
        // closes
        this.solverWMI.addConnectionListener(this);

        // Set the time between connection attempts to 10s.
        this.solverWMI.setConnectionRetryDelay(10000l);

        // Try to stay connected to the distributor.
        this.solverWMI.setStayConnected(true);

        // Close the current connection if there is a protocol exception.
        this.solverWMI.setDisconnectOnException(true);

        this.solverWMI.setOriginString(SOLVER_ORIGIN_STRING);
        
        // Connect to the distributor as a solver.
        if (!this.solverWMI.doConnectionSetup()) {
            System.out.println("Unable to establish a connection to the distributor as a solver.");
            System.exit(1);
        }
        System.out.println("Connected to distributor as a solver at "
                + this.solverWMI.getHost() + ":"
                + this.solverWMI.getPort());
        
        // Read the user input into this string
        String command = null;
       
        // Keep running until either we get disconnected, or the user wants to
        // quit.
        while (this.keepRunning) {
            try {
                // If the user has something to read in the buffer.
                if (this.userInput.ready()) {
                    command = this.userInput.readLine();

                    // Allow the solver to shut down gracefully if the user
                    // wants to exit.
                    if ("exit".equalsIgnoreCase(command)) {
                        System.out.println("Exiting solver (user).");
                        break;
                    } else {
                        System.out.println("Unknown command \"" + command
                                + "\". Type \"exit\" to exit.");
                    }
                }
            } catch (IOException e1) {
                System.err.println("Error reading user input.");
                e1.printStackTrace();
            }
            // Sleep for 100ms, since there isn't much else to do.
            try {
                Thread.sleep(100l);
                
                //Only send solutions every 15 minutes 
                if(this.timer.elapsed() == (this.timerCount*900000l)){
                	this.createAndSendSolution();
                	this.timerCount++;
                }
                
            } 
            catch (InterruptedException e) {
                // Ignored
            }
        }
        // Stop running
        this.solverWMI.doConnectionTearDown();
        System.exit(0);
    }
    
    /**
     * Sends solutions with byte arrays where the first byte is the severity of the traffic alert, 
     * and the rest of the bytes are the description of the traffic alert.
     */
    public void createAndSendSolution(){
    	ArrayList<String[]> trafficAlerts = this.trafficService.getTrafficAlerts();
    	for(String[] alert: trafficAlerts){
    		Solution solution = new Solution();
    		solution.setTargetName("traffic");
    		solution.setTime(System.currentTimeMillis());
    		solution.setAttributeName(SOLUTION_URI_NAME);

    		int temp = Integer.parseInt(alert[0]);
    		byte severity = (byte) temp;
    		
    		
           
    		ByteBuffer buff = ByteBuffer.allocate(alert[1].length() + 1);
            buff.put(severity);
            buff.put(alert[1].getBytes());
            
            solution.setData(buff.array());

            Collection<Solution> returnedList = new ArrayList<Solution>();
            returnedList.add(solution);
           
            if (!this.solverWMI.sendSolutions(
                    returnedList)) {
                log.warn("Unable to send solution to distributor: {}", returnedList);
            }
            log.info("Sent Traffic Solution!");
    	}
    }
	
	/**
     *  Implements a simple Stop-Watch/Timer type class based on Java wall-clock time.
     *
     *  RUNNING() == true  <==>  start() called with no corresponding call to stop()
     *  All times are given in units of milliseconds.
     */
    private class Timer {
        private boolean running;
        private long tStart;
        private long tFinish;
        private long tAccum; // total time
        

      public Timer(){
          reset(); //Initializes the timer to 0 ms.
      }

      
      /**
       *  Starts the timer.  Accumulates time across multiple calls to start.
       */
      public void start(){
          running = true;
          tStart = System.currentTimeMillis();
          tFinish = tStart;
      }

      
      /**
       *  Stops the timer. returns the time elapsed since the last matching call to start(), or
       *  0 if no such matching call was made.
       */
      @SuppressWarnings("unused")
    public long stop(){

          tFinish = System.currentTimeMillis();

          if(running){
              running = false;
              long diff = (tFinish - tStart);
              tAccum += diff;
              return diff;
          }
          
          return 0;
      }

      
      /**
       *  if RUNNING()   ==>  returns the time since last call to start()
       *  if !RUNNING()  ==>  returns total elapsed time
       */
      public long elapsed(){
          if(running)
              return System.currentTimeMillis() - tStart;
        
          return tAccum;
      }

      
      /**
       *  Stops timing, if currently RUNNING(); resets accumulated time to 0.
       */
      public void reset(){
          running = false;
          tStart = tFinish = 0;
          tAccum = 0;
      }

    }

	@Override
	public void connectionInterrupted(SolverWorldModelInterface worldModel) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connectionEnded(SolverWorldModelInterface worldModel) {
		// Stop running after the connection is finally closed.
        System.out.println("Connection closed.");
        this.keepRunning = false;
        this.interrupt();
	}

	 /**
     * Send solution at time of creating solver instance.
     */
	@Override
	public void connectionEstablished(SolverWorldModelInterface worldModel) {
		if(starting){
			this.timer.start();
			this.createAndSendSolution();
			starting = false;
		}
	}
}
