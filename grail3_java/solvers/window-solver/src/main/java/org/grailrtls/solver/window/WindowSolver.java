package org.grailrtls.solver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;

import org.grailrtls.libworldmodel.solver.SolverWorldModelInterface;
import org.grailrtls.libworldmodel.solver.WorldModelIoHandler;
import org.grailrtls.libworldmodel.solver.protocol.messages.DataTransferMessage.Solution;
import org.grailrtls.libworldmodel.solver.protocol.messages.TypeAnnounceMessage.TypeSpecification;
import org.grailrtls.libworldmodel.solver.listeners.ConnectionListener;
import org.grailrtls.libworldmodel.client.ClientWorldModelInterface;
import org.grailrtls.libworldmodel.client.listeners.DataListener;
import org.grailrtls.libworldmodel.client.protocol.messages.AbstractRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.AttributeAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.DataResponseMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.StreamRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.DataResponseMessage.Attribute;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.URISearchResponseMessage;
import org.grailrtls.solver.SolverAggregatorInterface;
import org.grailrtls.solver.listeners.SampleListener;
import org.grailrtls.solver.protocol.messages.SampleMessage;
import org.grailrtls.solver.protocol.messages.Transmitter;
import org.grailrtls.solver.rules.SubscriptionRequestRule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.grailrtls.util.NumericUtils;

/**
 * This is a solver that puts window sensor data into the World Model
 * 
 * @author Jonathan Chiu
 */

public class WindowSolver extends Thread implements ConnectionListener,
	org.grailrtls.solver.listeners.ConnectionListener, SampleListener, DataListener
{

    private static final Logger log = LoggerFactory.getLogger(WindowSolver.class);

    private static final String SOLUTION_URI_NAME = "";

    private static String ORIGIN_STRING = "jon.solver";

    private static final String SOLUTION_REGION_URI = "CoRE.window";

    private Hashtable<Integer, String> tagIds = new Hashtable<Integer, String>();

    private boolean keepRunning = true;

    private SolverWorldModelInterface worldModelInterface = new SolverWorldModelInterface();

    private SolverAggregatorInterface aggregatorInterface = new SolverAggregatorInterface();

    private final ClientWorldModelInterface worldModel = new ClientWorldModelInterface();

    private boolean canSendSolutions = false;

    private boolean sentStaticSolutions = false;

    private BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));

    private Hashtable<String, Integer> aliases = new Hashtable<String, Integer>();

    private String uriRegex = ".*window.*";

    private static boolean debug = false;

    public static void main(String[] args)
    {
	if (args.length < 4)
	{
	    printUsageInfo();
	    System.exit(1);
	}
	else
	{
	    for (int i = 4; i < args.length; i++)
	    {
		String[] temp = args[i].split("=");
		if (args[i].equalsIgnoreCase("d"))
		{
		    debug = true;
		    System.out.println("Debug mode enabled");
		}
		else if (temp[0].equals("origin"))
		{
		    ORIGIN_STRING = temp[1];
		}
	    }
	}

	int aggPort = Integer.parseInt(args[1]);
	int worldPort = Integer.parseInt(args[3]);
	WindowSolver solver = new WindowSolver(args[0], aggPort, args[2], worldPort);
	solver.start();
    }

    protected static void printUsageInfo()
    {
	System.out.println("Requires two parameters: <Aggregator host> <Aggregator port> <World Model host> <World Model port>.");
	System.out.println("Once running, type \"exit\" to exit.");
    }

    public WindowSolver(String aggHost, int aggPort, String worldHost, int worldPort)
    {
	this.worldModelInterface.setHost(worldHost);
	this.worldModelInterface.setPort(worldPort);
	this.aggregatorInterface.setHost(aggHost);
	this.aggregatorInterface.setPort(aggPort);
	this.worldModel.setHost(worldHost);
	this.worldModel.setPort(7013);
    }

    @Override
    public void run()
    {

	// Set up solver-aggregator interface
	SubscriptionRequestRule[] rules = new SubscriptionRequestRule[1];
	rules[0] = SubscriptionRequestRule.generateGenericRule();
	rules[0].setUpdateInterval(0l);
	rules[0].setPhysicalLayer((byte) 1);
	this.aggregatorInterface.setRules(rules);
	this.aggregatorInterface.addSampleListener(this);
	this.aggregatorInterface.addConnectionListener(this);
	this.aggregatorInterface.setDisconnectOnException(true);
	this.aggregatorInterface.setStayConnected(true);
	this.aggregatorInterface.setConnectionRetryDelay(10000l);

	// Set up solver-world model interface
	String[] specs = { "open", "name", "longitude", "latitude", "owner_email", "region" };
	for (int i = 0; i < specs.length; i++)
	{
	    TypeSpecification spec = new TypeSpecification();
	    spec.setIsTransient(false);
	    spec.setUriName(specs[i]);
	    spec.setTypeAlias(i);
	    aliases.put(specs[i], i);
	    // spec.setUriName("window."+w+".open");
	    this.worldModelInterface.addType(spec);
	}

	this.worldModelInterface.setOriginString(ORIGIN_STRING);
	this.worldModelInterface.addConnectionListener(this);
	this.worldModelInterface.setConnectionRetryDelay(10000l);
	this.worldModelInterface.setStayConnected(true);
	this.worldModelInterface.setDisconnectOnException(true);

	// Set up client-world model interface
	this.worldModel.setStayConnected(true);
	this.worldModel.setDisconnectOnException(true);
	this.worldModel.addDataListener(this);

	this.worldModel.registerSearchRequest(uriRegex);

	/*
	 * Connect to the worldModel. Note that this is being done before the aggregator to simplify the sample-handling logic in this example class.
	 */
	if (!this.worldModelInterface.doConnectionSetup())
	{
	    System.out.println("Unable to establish a connection to the World Model.");
	    System.exit(1);
	}
	System.out.println("Connected to World Model at " + this.worldModelInterface.getHost() + ":" + this.worldModelInterface.getPort());

	// Connect to the aggregator
	if (!this.aggregatorInterface.doConnectionSetup())
	{
	    System.out.println("Unable to establish a connection to the aggregator.");
	    System.exit(1);
	}
	System.out.println("Connected to aggregator at " + this.aggregatorInterface.getHost() + ":" + this.aggregatorInterface.getPort());

	if (!this.worldModel.doConnectionSetup())
	{
	    System.out.println("Unable to establish a connection to the world model.");
	    System.exit(1);
	}
	System.out.println("Connected to world model at " + this.worldModel.getHost() + ":" + this.worldModel.getPort());

	// Read the user input into this string
	String command = null;

	// Keep running until either we get disconnected, or the user wants to
	// quit.
	while (this.keepRunning)
	{
	    try
	    {
		// If the user has something to read in the buffer.
		if (this.userInput.ready())
		{
		    command = this.userInput.readLine();

		    // Allow the solver to shut down gracefully if the user
		    // wants to exit.
		    if ("exit".equalsIgnoreCase(command))
		    {
			System.out.println("Exiting solver (user).");
			break;
		    }
		    System.out.println("Unknown command \"" + command + "\". Type \"exit\" to exit.");

		}
	    } catch (IOException e1)
	    {
		System.err.println("Error reading user input.");
		e1.printStackTrace();
	    }
	    // Sleep for 100ms, since there isn't much else to do.
	    try
	    {
		Thread.sleep(100l);
	    } catch (InterruptedException e)
	    {
		// Ignored
	    }
	}
	// Stop running
	this.aggregatorInterface.doConnectionTearDown();
	this.worldModelInterface.doConnectionTearDown();
	this.worldModel.doConnectionTearDown();
	System.exit(0);
    }

    /**
     * Returns an integer representation of a 23-bit PIPSqueak device ID.
     * 
     * @param deviceId
     * @return the 23-bit PIPSqueak device ID as a 32-bit integer
     */
    private static int getPipId(byte[] deviceId)
    {
	// PIPSqueak tags only have a 23-bit ID, so we can extract the last 3
	// bytes as an integer
	return ((deviceId[SampleMessage.DEVICE_ID_SIZE - 3] << 16) & 0xFF) + ((deviceId[SampleMessage.DEVICE_ID_SIZE - 2] << 8) & 0xFF) + (deviceId[SampleMessage.DEVICE_ID_SIZE - 1] & 0xFF);
    }

    /**
     * Convert the byte array to an int.
     * 
     * @param b
     *            The byte array
     * @return The integer
     */
    public static int byteArrayToInt(byte[] b)
    {
	return byteArrayToInt(b, 0);
    }

    /**
     * Convert the byte array to an int starting from the given offset.
     * 
     * @param b
     *            The byte array
     * @param offset
     *            The array offset
     * @return The integer
     */
    public static int byteArrayToInt(byte[] b, int offset)
    {
	int value = 0;
	for (int i = 0; i < 4; i++)
	{
	    int shift = (4 - 1 - i) * 8;
	    value += (b[i + offset] & 0x000000FF) << shift;
	}
	return value;
    }

    public static String byteArrayToString(byte[] b)
    {
	String result = "[ ";
	if (b.length == 0)
	{
	    return "[ ]";
	}
	for (int i = 0; i < b.length; i++)
	{
	    result += b[i] + " ";
	}
	result += "]";
	return result;
    }

    public void sendSolution(SampleMessage sample, String targetUri)
    {
	Collection<Solution> solution = generateSolution(sample, targetUri);

	if (solution == null)
	{
	    dprint("Null solution!");
	    return;
	}

	if (!sentStaticSolutions)
	{
	    Collection<Solution> staticSolution = generateStaticSolutions();

	    if (!this.worldModelInterface.sendSolutions("CoRE.region", staticSolution))
		System.out.println("Unable to send static solutions");
	    else
	    {
		System.out.println("Static solutions sent");
		sentStaticSolutions = true;
	    }
	}
	// Send the solution to the worldModel
	if (!this.worldModelInterface.sendSolutions(SOLUTION_REGION_URI, solution))
	{
	    System.out.println("Unable to send solution");
	}
	else
	{
	    System.out.println("Solution has been sent: {" + Integer.toString(WindowSolver.getPipId(sample.getDeviceId())) + " :: " + NumericUtils.toHexString(sample.getSensedData()) + "}");
	}
    }

    private Collection<Solution> generateSolution(SampleMessage sample, String targetUri)
    {
	// Update "open" attribute
	Solution solution = new Solution();
	// solution.setTargetName(SOLUTION_REGION_URI + "." + WindowSolver.getPipId(sample.getDeviceId()));
	solution.setTargetName(targetUri);
	solution.setAttributeName("open");
	solution.setAttributeNameAlias(aliases.get("open"));

	try
	{
	    String h = NumericUtils.toHexString(sample.getSensedData());
	    byte[] data = "unknown".getBytes("UTF-16BE");
	    if (h.equals("0xFF"))
		data = "closed".getBytes("UTF-16BE");
	    else if (h.equals("0x00"))
		data = "open".getBytes("UTF-16BE");
	    solution.setData(data);
	} catch (Exception e)
	{
	    e.printStackTrace();
	}

	solution.setTime(System.currentTimeMillis());

	ArrayList<Solution> returnedList = new ArrayList<Solution>();
	returnedList.add(solution);

	/*
	 * // Update email attribute solution = new Solution(); // solution.setTargetName(SOLUTION_REGION_URI + "." + WindowSolver.getPipId(sample.getDeviceId())); solution.setTargetName(targetUri);
	 * solution.setAttributeName("owner_email"); try { solution.setData("jchiu09@gmail.com".getBytes("UTF-16BE")); } catch (Exception e) { e.printStackTrace(); } solution.setAttributeNameAlias(4);
	 * solution.setTime(System.currentTimeMillis()); returnedList.add(solution);
	 */

	return returnedList;
    }

    private Collection<Solution> generateStaticSolutions()
    {
	ArrayList<Solution> returnedList = new ArrayList<Solution>();
	try
	{
	    Solution solution = new Solution();
	    solution.setTargetName("CoRE.region");
	    solution.setAttributeName("name");
	    solution.setAttributeNameAlias(aliases.get("name"));
	    solution.setData("Computing Research and Technology Building".getBytes("UTF-16BE"));
	    solution.setTime(System.currentTimeMillis());
	    returnedList.add(solution);

	    solution = new Solution();
	    solution.setTargetName("CoRE.region");
	    solution.setAttributeName("latitude");
	    solution.setAttributeNameAlias(aliases.get("latitude"));
	    solution.setData("40.521368".getBytes("UTF-16BE"));
	    solution.setTime(System.currentTimeMillis());
	    returnedList.add(solution);

	    solution = new Solution();
	    solution.setTargetName("CoRE.region");
	    solution.setAttributeName("longitude");
	    solution.setAttributeNameAlias(aliases.get("longitude"));
	    solution.setData("-74.461412".getBytes("UTF-16BE"));
	    solution.setTime(System.currentTimeMillis());
	    returnedList.add(solution);

	} catch (Exception e)
	{
	    e.printStackTrace();
	}
	return returnedList;
    }

    /**
     * Shuts down this solver because the worldModel interface has "given up" on reconnecting.
     * 
     * @param worldModel
     */
    public void connectionEnded(SolverWorldModelInterface worldModel)
    {
	// Stop running after the connection is finally closed.
	System.out.println("Connection closed to " + worldModel);
	this.keepRunning = false;
	this.interrupt();

    }

    public void connectionEstablished(SolverWorldModelInterface worldModel)
    {
	// Not used
    }

    public void connectionInterrupted(SolverWorldModelInterface worldModel)
    {
	this.canSendSolutions = false;

    }

    public void specificationMessageSent(SolverWorldModelInterface worldModel)
    {
	this.canSendSolutions = true;
    }

    public void sampleReceived(SolverAggregatorInterface aggregator, SampleMessage sample)
    {
	// System.out.println("Sample Received.");

	int did = WindowSolver.getPipId(sample.getDeviceId());
	if (tagIds.containsKey(did))
	{
	    dprint("Sample received and matched. (" + did + ")");
	    String target = tagIds.get(did);
	    sendSolution(sample, target);
	}
    }

    @Override
    public void connectionEnded(SolverAggregatorInterface arg0)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void connectionEstablished(SolverAggregatorInterface arg0)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void connectionInterrupted(SolverAggregatorInterface arg0)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void attributeAliasesReceived(ClientWorldModelInterface arg0, AttributeAliasMessage arg1)
    {
	// TODO Auto-generated method stub

    }

    // Add tag ID to the list
    private void updateTagIds(DataResponseMessage msg)
    {
	Attribute[] attr = msg.getAttributes();
	int id = -1;
	for (Attribute a : attr)
	{
	    if (!a.getOriginName().equals(ORIGIN_STRING))
		continue;
	    if (a.getAttributeName().equals("tag_id"))
	    {
		try
		{
		    String temp = new String(a.getData(), "UTF-16BE");
		    id = Integer.parseInt(temp);
		} catch (Exception e)
		{
		    e.printStackTrace();
		}
		break;
	    }
	}

	if (id != -1 && !tagIds.containsKey(id))
	{
	    tagIds.put(id, msg.getUri());
	    dprint("Tag ID " + id + " has been registered to URI " + msg.getUri());
	}
    }

    public void dprint(String s)
    {
	if (debug)
	    System.out.println("[DEBUG] " + s);
	return;
    }

    @Override
    public void dataResponseReceived(ClientWorldModelInterface arg0, DataResponseMessage arg1)
    {
	updateTagIds(arg1);
	dprint("DataResponseReceived: " + arg1.getUri());
    }

    @Override
    public void originAliasesReceived(ClientWorldModelInterface arg0, OriginAliasMessage arg1)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void requestCompleted(ClientWorldModelInterface arg0, AbstractRequestMessage arg1,
	    int arg2)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void requestTicketReceived(ClientWorldModelInterface arg0, AbstractRequestMessage arg1,
	    int arg2)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void uriSearchResponseReceived(ClientWorldModelInterface worldModel,
	    URISearchResponseMessage message)
    {
	// log.info("{} sent {}", worldModel, message);
	dprint("UriSearchResponseReceived: Querying world model for URIs...");
	long now = System.currentTimeMillis();

	StreamRequestMessage request = new StreamRequestMessage();
	request.setBeginTimestamp(now);
	request.setUpdateInterval(0l);
	request.setQueryURI(uriRegex);
	request.setQueryAttributes(new String[] { ".*" });
	worldModel.sendMessage(request);

    }
}
