/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Sumedh Sawant
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.solver.notifications;

import java.util.ArrayList;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *  Class that uses open-source JavaMail API method to send an email 
 *  via Gmail SMTP server, using an SSL connection.
 *  
	 *  from: http://www.mkyong.com/java/javamail-api-sending-email-via-gmail-smtp-example/
	 *  
	 * @author mkyong, Sumedh Sawant
 * 
 * Greatly modified by Sumedh Sawant for GRAIL purposes, used to send SMS and email notifications, 
 * as well to update the twitter feed. 
 *
 */
public class SendMessageSSL {
	private ArrayList<String> recipients;
	private static final Logger log = LoggerFactory.getLogger(SendMessageSSL.class);
	
	private String username = null;
	private String password = null;
	
	public SendMessageSSL(ArrayList<String> recpients){
		this.recipients = recpients;
	}
	
	public void sendMessage(String msg){
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
 
		Session session = Session.getInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(SendMessageSSL.this.username,SendMessageSSL.this.password);
				}
			});
 
		try {
			for(String recipient: this.recipients){
				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress("from@no-spam.com"));
				message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(recipient));
				message.setSubject("");
				message.setText(msg);
 
				Transport.send(message);
			}
 
			log.info("Message was sent!");
 
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void sendRemovalMessage(String recipient){
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
 
		Session session = Session.getInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(SendMessageSSL.this.username,SendMessageSSL.this.password);
				}
			});
 
		try {
				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress("from@no-spam.com"));
				message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(recipient));
				message.setSubject("Removal Confirmation");
				message.setText("You have been removed from the WINLAB notification system.");
 
				Transport.send(message);
 
			System.out.println("Done");
 
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
		
		log.info("Send removal message to recipient.");
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	/*public static void main(String[] args){
		SendMessageSSL s = new SendMessageSSL(new String[]{""});
		s.sendSMS();
	}*/
	
	/**
	 * Below info found at: http://edgedirector.com/htm/page.htm
	 * 
	 * CARRIER                             EMAIL to SMS GATEWAY ADDRESS

		3 Rivers Wireless                   number@sms.3rivers.net
		ACS Wireless                        number@paging.acswireless.com
		Advantage Communications            number@advantagepaging.com
		Airtouch Pagers                     number@myairmail.com
		Airtouch Pagers                     number@alphapage.airtouch.com
		Airtouch Pagers                     number@airtouch.net
		Airtouch Pagers                     number@airtouchpaging.com
		AlphNow                             pin@alphanow.net
		Alltel                              number@alltelmessage.com
		Alltel PCS                          number@message.alltel.com
		Ameritech Paging                    number@paging.acswireless.com
		Ameritech Paging                    number@pageapi.com
		Ameritech Clearpath                 number@clearpath.acswireless.com
		Ameritech Clearpath                 number@clearpath.acswireless.com
		American Messaging (SBC/Ameritech)  number@page.americanmessaging.net
		Andhra Pradesh Airtel               number@airtelap.com
		Arch Pagers (PageNet)               number@archwireless.net
		Arch Pagers (PageNet)               number@epage.arch.com
		Arch Pagers (PageNet)               number@archwireless.net
		AT&T Free2Go                        number@mmode.com
		AT&T PCS                            number@mobile.att.net
		AT&T SMS                            number@txt.att.net
		AT&T Pocketnet PCS                  number@dpcs.mobile.att.net
		Beepwear                            number@beepwear.net
		BeeLine GSM                         number@sms.beemail.ru
		Bell Atlantic                       number@message.bam.com
		Bell Canada                         number@txt.bellmobility.ca
		Bell Canada                         number@bellmobility.ca
		Bell Mobility (Canada)              number@txt.bell.ca
		Bell Mobility                       number@txt.bellmobility.ca
		Bell South (Blackberry)             number@bellsouthtips.com
		Bell South                          number@sms.bellsouth.com
		Bell South                          number@wireless.bellsouth.com
		Bell South                          number@blsdcs.net
		Bell South                          number@bellsouth.cl
		Bell South Mobility                 number@blsdcs.net
		Blue Sky Frog                       number@blueskyfrog.com
		Bluegrass Cellular                  number@sms.bluecell.com
		Boost                               number@myboostmobile.com
		BPL mobile                          number@bplmobile.com
		Cable & wireless, Panama            number@cwmovil.com
		Carolina West Wireless              number@cwwsms.com
		Carolina Mobile Communications      number@cmcpaging.com
		Cellular One East Coast             number@phone.cellone.net
		Cellular One South West             number@swmsg.com
		Cellular One PCS                    number@paging.cellone-sf.com
		Cellular One                        number@mobile.celloneusa.com
		Cellular One                        number@cellularone.txtmsg.com
		Cellular One                        number@cellularone.textmsg.com
		Cellular One                        number@cell1.textmsg.com
		Cellular One                        number@message.cellone-sf.com
		Cellular One                        number@sbcemail.com
		Cellular One West                   number@mycellone.com
		Cellular South                      number@csouth1.com
		Centennial Wireless                 number@cwemail.com
		Central Vermont Communications      number@cvcpaging.com
		CenturyTel                          number@messaging.centurytel.net
		Chennai RPG Cellular                number@rpgmail.net
		Chennai Skycell / Airtel            number@airtelchennai.com
		Cincinnati Bell                     number@mobile.att.net
		Cingular                            number@cingularme.com
		Cingular                            number@mycingular.com
		Cingular                            number@mycingular.net
		Cingular                            number@mms.cingularme.com
		Cingular                            number@page.cingular.com
		Cingular Wireless                   number@mycingular.textmsg.com
		Cingular Wireless                   number@mobile.mycingular.com
		Cingular Wireless                   number@mobile.mycingular.net
		Cingular Wireless                   number@mycingular.textmsg.com
		Cingular Wireless                   number@mobile.mycingular.com
		Cingular Wireless                   number@mobile.mycingular.net
		Clearnet                            number@msg.clearnet.com
		Comcast                             number@comcastpcs.textmsg.com
		Communication Specialists           pin@pageme.comspeco.net
		Communication Specialist Companies  pin@pager.comspeco.com
		Comviq                              number@sms.comviq.se
		Cook Paging                         number@cookmail.com
		Corr Wireless Communications        number@corrwireless.net
		Delhi Aritel                        number@airtelmail.com
		Delhi Hutch                         number@delhi.hutch.co.in
		Digi-Page / Page Kansas             number@page.hit.net
		Dobson Cellular Systems             number@mobile.dobson.net
		Dobson-Alex Wireless                number@mobile.cellularone.com
		Dobson-Cellular One                 number@mobile.cellularone.com
		DT T-Mobile                         number@t-mobile-sms.de
		Dutchtone / Orange-NL               number@sms.orange.nl
		Edge Wireless                       number@sms.edgewireless.com
		EMT                                 number@sms.emt.ee
		Escotel                             number@escotelmobile.com
		Fido                                number@fido.ca
		Galaxy Corporation                  number.epage@sendabeep.net
		GCS Paging                          number@webpager.us
		Goa BPLMobil                        number@bplmobile.com
		Golden Telecom                      number@sms.goldentele.com
		GrayLink / Porta-Phone              number@epage.porta-phone.com
		GTE                                 number@airmessage.net
		GTE                                 number@gte.pagegate.net
		GTE                                 number@messagealert.com
		Gujarat Celforce                    number@celforce.com
		Houston Cellular                    number@text.houstoncellular.net
		Idea Cellular                       number@ideacellular.net
		Infopage Systems                    pin@page.infopagesystems.com
		Inland Cellular Telephone           number@inlandlink.com
		The Indiana Paging Co               pin@pager.tdspager.com
		IPIPI.COM                           number@opensms.ipipi.com
		JSM Tele-Page                       pinnumber@jsmtel.com
		Kerala Escotel                      number@escotelmobile.com
		Kolkata Airtel                      number@airtelkol.com
		Kyivstar - contract                 number@sms.kyivstar.net
		Kyivstar - prepaid                  number@2sms.kyivstar.net
		Lauttamus Communication             number@e-page.net
		LMT                                 number@smsmail.lmt.lv
		Maharashtra BPL Mobile              number@bplmobile.com
		Maharashtra Idea Cellular           number@ideacellular.net
		Manitoba Telecom Systems            number@text.mtsmobility.com
		MCI Phone                           number@mci.com
		MCI                                 number@pagemci.com
		Meteor                              number@mymeteor.ie
		Meteor                              number@sms.mymeteor.ie
		Metrocall                           number@page.metrocall.com
		Metrocall 2-way                     number@my2way.com
		Metro PCS                           number@mymetropcs.com
		Metro PCS                           number@metropcs.sms.us
		Microcell                           number@fido.ca
		Midwest Wireless                    number@clearlydigital.com
		MiWorld                             number@m1.com.sg
		Mobilecom PA                        number@page.mobilcom.net
		Mobilecomm                          number@mobilecomm.net
		Mobileone                           number@m1.com.sg
		Mobilfone                           number@page.mobilfone.com
		Mobility Bermuda                    number@ml.bm
		Mobistar Belgium                    number@mobistar.be
		Mobitel Tanzania                    number@sms.co.tz
		Mobtel Srbija                       number@mobtel.co.yu
		Morris Wireless                     number@beepone.net
		Motient                             number@isp.com
		Movistar                            number@correo.movistar.net
		Mumbai BPL Mobile                   number@bplmobile.com
		Mumbai Orange                       number@orangemail.co.in
		NBTel                               number@wirefree.informe.ca
		Netcom                              number@sms.netcom.no
		Nextel                              number@messaging.nextel.com
		Nextel                              number@page.nextel.com
		Nextel                              number@nextel.com.br
		NPI Wireless                        number@npiwireless.com
		Ntelos                              number@pcs.ntelos.com
		O2                                  name@o2.co.uk
		O2                                  number@o2imail.co.uk
		O2 (M-mail)                         number@mmail.co.uk
		Omnipoint                           number@omnipoint.com
		Omnipoint                           number@omnipointpcs.com
		One Connect Austria                 number@onemail.at
		OnlineBeep                          number@onlinebeep.net
		Optus Mobile                        number@optusmobile.com.au
		Orange                              number@orange.net
		Orange Mumbai                       number@orangemail.co.in
		Orange - NL / Dutchtone             number@sms.orange.nl
		Oskar                               number@mujoskar.cz
		P&T Luxembourg                      number@sms.luxgsm.lu
		Pacific Bell                        number@pacbellpcs.net
		PageMart                            pin@pagemart.net
		PageMart Advanced /2way             number@airmessage.net
		PageMart Canada                     number@pmcl.net
		PageNet Canada                      number@pagegate.pagenet.ca
		PageOne NorthWest                   number@page1nw.com
		PCS One                             number@pcsone.net
		Pelephone Israel                    number@pelephone.net.il
		Personal Communication              sms@pcom.ru (number in subject line)
		Pioneer / Enid Cellular             number@msg.pioneerenidcellular.com
		PlusGSM                             number@text.plusgsm.pl
		Pondicherry BPL Mobile              number@bplmobile.com
		Powertel                            number@voicestream.net
		Price Communications                number@mobilecell1se.com
		Primeco                             number@primeco@textmsg.com
		Primtel                             number@sms.primtel.ru
		ProPage                             number@page.propage.net
		Public Service Cellular             number@sms.pscel.com
		Qualcomm                            name@pager.qualcomm.com
		Qwest                               number@qwestmp.com
		RAM Page                            number@ram-page.com
		Rogers AT&T Wireless                number@pcs.rogers.com
		Rogers Canada                       number@pcs.rogers.com
		Safaricom                           number@safaricomsms.com
		Satelindo GSM                       number@satelindogsm.com
		Satellink                           number.pageme@satellink.net
		SBC Ameritech Paging                number@paging.acswireless.com
		SCS-900                             number@scs-900.ru
		SFR France                          number@sfr.fr
		Skytel Pagers                       pin@skytel.com
		Skytel Pagers                       number@email.skytel.com
		Simple Freedom                      number@text.simplefreedom.net
		Smart Telecom                       number@mysmart.mymobile.ph
		Southern LINC                       number@page.southernlinc.com
		Southwestern Bell                   number@email.swbw.com
		Sprint                              number@sprintpaging.com
		Sprint PCS                          number@messaging.sprintpcs.com
		ST Paging                           pin@page.stpaging.com
		SunCom                              number@tms.suncom.com
		SunCom                              number@suncom1.com
		Sunrise Mobile                      number@mysunrise.ch
		Sunrise Mobile                      number@freesurf.ch
		Surewest Communications             number@mobile.surewest.com
		Swisscom                            number@bluewin.ch
		T-Mobile                            number@tmomail.net
		T-Mobile                            number@voicestream.net
		T-Mobile Austria                    number@sms.t-mobile.at
		T-Mobile Germany                    number@t-d1-sms.de
		T-Mobile UK                         number@t-mobile.uk.net
		Tamil Nadu BPL Mobile               number@bplmobile.com
		Tele2 Latvia                        number@sms.tele2.lv
		Telefonica Movistar                 number@movistar.net
		Telenor                             number@mobilpost.no
		Teletouch                           number@pageme.teletouch.com
		Telia Denmark                       number@gsm1800.telia.dk
		Telus                               number@msg.telus.com
		TIM                                 number@timnet.com
		Triton                              number@tms.suncom.com
		TSR Wireless                        number@alphame.com
		TSR Wireless                        number@beep.com
		UMC                                 number@sms.umc.com.ua
		Unicel                              number@utext.com
		Uraltel                             number@sms.uraltel.ru
		US Cellular                         number@email.uscc.net
		US Cellular                         number@uscc.textmsg.com
		US West                             number@uswestdatamail.com
		Uttar Pradesh Escotel               number@escotelmobile.com
		Verizon Pagers                      number@myairmail.com
		Verizon PCS (SMS)                   number@vtext.com
		Verizon PCS (MMS)                   number@myvzw.com
		Verizon PCS (MMS)                   number@vswpix.com
		Vessotel                            number@pager.irkutsk.ru
		Virgin Mobile (CA)                  number@vmobile.ca
		Virgin Mobile (US)                  number@vmobl.com
		Virgin Mobile                       number@vxtras.com
		Vodacom (S Africa)                  number@voda.co.za
		Vodafone Italy                      number@sms.vodafone.it
		Vodafone Japan                      number@c.vodafone.ne.jp
		Vodafone Japan                      number@h.vodafone.ne.jp
		Vodafone Japan                      number@t.vodafone.ne.jp
		Vodafone Spain                      number@vodafone.es
		Vodafone UK                         number@vodafone.net
		VoiceStream / T-Mobile              number@voicestream.net
		WebLink Wireless                    number@airmessage.net
		WebLink Wireless                    number@pagemart.net
		WEBTEXT                             number@webtext.com
		West Central Wireless               number@sms.wcc.net
		Western Wireless                    number@cellularonewest.com
		Wildmist Wireless                   number@sms.wildmist.net
		Wyndtell                            number@wyndtell.com
	 */
}