/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Sumedh Sawant
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.solver.notifications;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.grailrtls.libworldmodel.client.ClientWorldModelInterface;
import org.grailrtls.libworldmodel.client.listeners.ConnectionListener;
import org.grailrtls.libworldmodel.client.listeners.DataListener;
import org.grailrtls.libworldmodel.client.protocol.messages.AbstractRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.AttributeAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.AttributeAliasMessage.AttributeAlias;
import org.grailrtls.libworldmodel.client.protocol.messages.DataResponseMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginPreferenceMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.StreamRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.URISearchResponseMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Java Solver that connects to the distributor as a client, checks for
 * solutions [teatime, refrigerator door ajar, fresh coffee, events] and
 * notifies recipients accordingly.
 * 
 * @author Sumedh Sawant
 * 
 */
public class NotificationSenderApplication implements DataListener,
		ConnectionListener {

	/**
	 * Logging facility for this class.
	 */
	private static final Logger log = LoggerFactory
			.getLogger(NotificationSenderApplication.class);

	/**
	 * Used to get user input.
	 */
	private BufferedReader userInput = new BufferedReader(
			new InputStreamReader(System.in));

	/**
	 * Flag to keep running until the connection is closed.
	 */
	private boolean keepRunning = true;

	/**
	 * Interface to the distributor as a client, takes care of all the basic
	 * protocol stuff.
	 */
	private ClientWorldModelInterface clientDistributorInterface = new ClientWorldModelInterface();

	/**
	 * Interface to send SMS and Email notifications, as well as tweets to
	 * twitter.
	 */
	private NotificationInterface notificationInterface;

	private RecipientRemover recipientRemover = null;

	private boolean sentTrafficNotifications = false;

	private String dbUsername = null;

	// TODO: Shouldn't keep it plaintext in memory.
	private String dbPassword = null;

	private String gmUsername = null;

	private String gmPassword = null;

	protected JdbcConnector dbConnector = new JdbcConnector();

	/**
	 * Creates a new NotificationSenderApplication and starts it.
	 * 
	 * @param args
	 *            : clientDistributor host, clientDistributor port, in that
	 *            order.
	 */
	public static void main(String[] args) {
		if (args.length < 6) {
			printUsageInfo();
			System.exit(1);
		}

		int aggPort = Integer.parseInt(args[1]);

		NotificationSenderApplication solver = new NotificationSenderApplication(
				args[0], aggPort, args[2], args[3], args[4], args[5]);
		solver.initializeAndRun();
	}

	/**
	 * Prints a simple message about the parameters for this program.
	 */
	protected static void printUsageInfo() {
		System.out
				.println("Requires six parameters: <ClientDistributor host> <ClientDistributor port> <DB User> <DB Password> <GMail User> <GMail Password>.");
		System.out.println("Once running, type \"exit\" to exit.");
	}

	/**
	 * CONSTRUCTOR
	 * 
	 * Creates a new "NotificationSenderApplication" that connects to the
	 * distributor at the specified host name/IP addresses and port numbers. (As
	 * a client AND as a solver)
	 * 
	 * @param clientDistHost
	 *            the host-name or IP address of the distributor.
	 * @param clientDistPort
	 *            the port-number the distributor is listening for clients on.
	 */
	public NotificationSenderApplication(String clientDistHost,
			int clientDistPort, String dbUsername, String dbPassword,
			String gmailUser, String gmailPass) {
		this.clientDistributorInterface.setHost(clientDistHost);
		this.clientDistributorInterface.setPort(clientDistPort);
		this.dbUsername = dbUsername;
		this.dbPassword = dbPassword;
		this.gmUsername = gmailUser;
		this.gmPassword = gmailPass;

		this.recipientRemover = new RecipientRemover(this.dbUsername,
				this.dbPassword);

		this.dbConnector.setUsername(this.dbUsername);
		this.dbConnector.setPassword(this.dbPassword);

		this.notificationInterface = new NotificationInterface(this.dbConnector);
		this.notificationInterface.setGmailUser(this.gmUsername);
		this.notificationInterface.setGmailPass(this.gmPassword);
	}

	/**
	 * Connects to the distributor as a client. Checks for user input every
	 * 100ms, and exits if the connection is ended, or the user types "exit".
	 */
	public void initializeAndRun() {

		this.recipientRemover.setUsername(gmUsername);
		this.recipientRemover.setPassword(this.gmPassword);

		/*
		 * "Client Distributor" interface set-up.
		 */

		// Add this class as a dataTransferMessage listener so that the data can
		// be used to generate solutions.
		this.clientDistributorInterface.addDataListener(this);

		// Add this class as a connection listener to know when the connection
		// closes and also when the type specification message has been sent.
		this.clientDistributorInterface.addConnectionListener(this);

		// Disconnect if we have any problems
		this.clientDistributorInterface.setDisconnectOnException(true);

		// Don't try to reconnect after a disconnect
		this.clientDistributorInterface.setStayConnected(true);

		// Wait 30 seconds between attempts to connect to the distributor as a
		// client.
		this.clientDistributorInterface.setConnectionRetryDelay(10000l);

		// Connect to the distributor as a client.
		if (!this.clientDistributorInterface.doConnectionSetup()) {
			System.out
					.println("Unable to establish a connection to the distributor as a client.");
			System.exit(1);
		}
		System.out.println("Connected to distributor as a client at "
				+ this.clientDistributorInterface.getHost() + ":"
				+ this.clientDistributorInterface.getPort());

		// Read the user input into this string
		String command = null;

		// Keep running until either we get disconnected, or the user wants to
		// quit.
		while (this.keepRunning) {
			try {
				// If the user has something to read in the buffer.
				if (this.userInput.ready()) {
					command = this.userInput.readLine();

					// Allow the solver to shut down gracefully if the user
					// wants to exit.
					if ("exit".equalsIgnoreCase(command)) {
						System.out.println("Exiting solver (user).");
						break;
					} else {
						System.out.println("Unknown command \"" + command
								+ "\". Type \"exit\" to exit.");
					}
				}
			} catch (IOException e1) {
				System.err.println("Error reading user input.");
				e1.printStackTrace();
			}
			// Sleep for 100ms, since there isn't much else to do.
			try {
				Thread.sleep(100l);
				if (this.timeCheck()) {
					this.getAndSendTrafficAlerts();
				}
			} catch (InterruptedException e) {
				// Ignored
			}

			this.recipientRemover.startUpdates();
			while (this.keepRunning) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// Ignored
				}
			}
		}
	}

	private void getAndSendTrafficAlerts() {
		int count = 0;
		System.out.println("Getting and Sending Traffic Alerts.");
		if (!this.sentTrafficNotifications) {
			MapquestTrafficService trafficService = new MapquestTrafficService(
					"08902", "Fmjtd%7Cluu2256rn1%2C2x%3Do5-huzsg");
			ArrayList<String[]> trafficAlerts = trafficService
					.getTrafficAlerts();
			System.out.println("Received Traffic Alerts");
			for (String[] alert : trafficAlerts) {
				if (Integer.parseInt(alert[0]) >= 3) {
					this.notificationInterface
							.sendTrafficNotifications(alert[1]);
					count++;
				}
				if (count == 2) {
					return;
				}
			}
			this.sentTrafficNotifications = true;
		}
	}

	/**
	 * Returns true if the hour of day is between 3 and 5 PM.
	 * 
	 * @return boolean
	 */
	private boolean timeCheck() {
		String DATE_FORMAT_NOW = "HH mm";
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		String time = sdf.format(cal.getTime());

		int hour;
		int minutes;

		try {
			hour = Integer.parseInt(time.substring(0, 2));
			minutes = Integer.parseInt(time.substring(3, 5));
		} catch (NumberFormatException e) {
			hour = 0;
			minutes = 0;
		}

		if (hour == 17 && minutes == 10) {
			return true;
		} else {
			this.sentTrafficNotifications = false;
		}

		return false;
	}

	public static String byteArrayToString(byte[] b) {
		String result = "[ ";
		if (b.length == 0) {
			return "[ ]";
		}
		for (int i = 0; i < b.length; i++) {
			result += b[i] + " ";
		}
		result += "]";
		return result;
	}

	@Override
	public void connectionInterrupted(ClientWorldModelInterface worldModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connectionEnded(ClientWorldModelInterface worldModel) {
		// Stop running after the connection is finally closed.
		System.out.println("Connection closed.");
		this.keepRunning = false;
		System.exit(1);
	}

	@Override
	public void connectionEstablished(ClientWorldModelInterface worldModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void requestCompleted(ClientWorldModelInterface worldModel,
			AbstractRequestMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dataResponseReceived(ClientWorldModelInterface worldModel,
			DataResponseMessage message) {
		log.debug("Data received: " + message.getUri());
		log.debug(byteArrayToString(message.getAttributes()[0].getData()));

		// If the refrigerator door is declared ajar, notify relevant
		// recipients.
		if (message.getUri().equalsIgnoreCase("winlab.refrigerator.kitchen")) {
			if (message.getAttributes()[0].getData()[0] == (byte) 1) {
				this.notificationInterface.sendRefrigeratorNotifications();
			}
			return;
		}

		// If teatime is happening, notify relevant recipients.
		if (message.getUri().equalsIgnoreCase("teatime")) {
			log.info("WENT INSIDE IF STATEMENT");
			if (message.getAttributes()[0].getData()[0] == (byte) 1) {
				log.info("2nd IF STATEMENT!");
				this.notificationInterface.sendTeatimeNotifications();
			}
			return;
		}

		// If fresh coffee is made, notify relevant recipients.
		if (message.getUri().equalsIgnoreCase("coffee brewing")) {
			if (message.getAttributes()[0].getData()[0] == (byte) 1) {
				this.notificationInterface.sendFreshCoffeeNotifications();
				return;
			}
		}

		// If an important event is about to happen, notify relevant recipients.
		if (message.getUri().equalsIgnoreCase("event")) {
			String event = new String(message.getAttributes()[0].getData());
			this.notificationInterface.sendEventNotifications(event);
		}
	}

	@Override
	public void uriSearchResponseReceived(ClientWorldModelInterface worldModel,
			URISearchResponseMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void attributeAliasesReceived(ClientWorldModelInterface worldModel,
			AttributeAliasMessage message) {
		StreamRequestMessage request = new StreamRequestMessage();
		request.setBeginTimestamp(System.currentTimeMillis());
		request.setUpdateInterval(0l);
		request.setQueryURI(".*");

		AttributeAlias[] aliases = message.getAliases();
		
		ArrayList<String> queryAttributes = new ArrayList<String>();

		for (AttributeAlias alias : aliases) {

			log.info(alias.aliasName + " -> " + alias.aliasNumber);

			if ("door ajar".equalsIgnoreCase(alias.aliasName)) {
				queryAttributes.add(alias.aliasName);
			}
			if ("fresh coffee".equalsIgnoreCase(alias.aliasName)) {
				queryAttributes.add(alias.aliasName);
			}
			if ("happening".equalsIgnoreCase(alias.aliasName)) {
				queryAttributes.add(alias.aliasName);
			}
			if ("event".equalsIgnoreCase(alias.aliasName)) {
				queryAttributes.add(alias.aliasName);
			}
			if ("traffic".equalsIgnoreCase(alias.aliasName)) {
				queryAttributes.add(alias.aliasName);
			}
		}

		if (!queryAttributes.isEmpty()) {
			request.setQueryAttributes(queryAttributes.toArray(new String[]{}));
		}

		if (request.getQueryAttributes() == null) {
			log.error("FAILED TO GENERATE DATA REQUEST MESSAGE");
			System.exit(1);
		}

		this.clientDistributorInterface.sendMessage(request);
	}

	@Override
	public void originAliasesReceived(ClientWorldModelInterface worldModel,
			OriginAliasMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void originPreferenceSent(ClientWorldModelInterface worldModel,
			OriginPreferenceMessage message) {
		// TODO Auto-generated method stub
		
	}

}
