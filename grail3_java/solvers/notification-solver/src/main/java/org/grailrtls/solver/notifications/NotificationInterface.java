/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Sumedh Sawant
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.solver.notifications;

import java.util.ArrayList;
import java.util.Hashtable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class sends SMS, email, and twitter messages according to the mySQL database of recipients.
 * 
 * @author Sumedh Sawant
 *
 */
public class NotificationInterface {

	private JdbcConnector dbConnector = null;
	private Hashtable<String, ArrayList<String>> recipients;
	private static final Logger log = LoggerFactory.getLogger(NotificationInterface.class);
	
	private String gmailUser = null;
	
	private String gmailPass = null;
	
	public NotificationInterface(JdbcConnector connector){
		this.dbConnector = connector;
		this.recipients = this.dbConnector.getData();
	}
	
	/**
	 * Sends email/SMS notifications about Teatime happening. Also updates twitter feed.
	 */
	public void sendTeatimeNotifications(){
		log.info("Sending Teatime Notifications");
		ArrayList<String> teatimeRecipients = this.recipients.get("teatime");
		System.out.println(teatimeRecipients);
		if(teatimeRecipients == null){
			log.error("Teatime recipients not found.");
			return;
		}
		log.info("Creating SSL message sender.");
		SendMessageSSL messageSender = new SendMessageSSL(teatimeRecipients);
		messageSender.setUsername(this.gmailUser);
		messageSender.setPassword(gmailPass);
		log.info("Creating teatime notifications");
		messageSender.sendMessage("Teatime is Happening! Hurry to the kitchen for cookies and some hot water!");
		log.info("Sent teatime notifications.");
	}
	
	/**
	 * Sends email/SMS notifications about fresh coffee having been made. Also updates twitter feed.
	 */
	public void sendFreshCoffeeNotifications(){
		ArrayList<String> coffeeRecipients = this.recipients.get("fresh coffee");
		if(coffeeRecipients == null){
			log.error("Coffee recipients not found.");
			return;
		}
		SendMessageSSL messageSender = new SendMessageSSL(coffeeRecipients);
		messageSender.setUsername(this.gmailUser);
		messageSender.setPassword(gmailPass);
		messageSender.sendMessage("Fresh Coffee is ready! Hurry to the kitchen and get some before its gone!");
		log.info("Sent fresh coffee notifications.");
	}
	
	/**
	 * Sends email/SMS notifications about fridge door being left open. Also updates twitter feed.
	 */
	public void sendRefrigeratorNotifications(){
		ArrayList<String> fridgeRecipients = this.recipients.get("refrigerator");
		if(fridgeRecipients == null){
			log.error("Refrigerator recipients not found.");
			return;
		}
		SendMessageSSL messageSender = new SendMessageSSL(fridgeRecipients);
		messageSender.setUsername(this.gmailUser);
		messageSender.setPassword(gmailPass);
		messageSender.sendMessage("SOMEONE CLOSE THE REFRIGERATOR BEFORE THE MILK GOES BAD!");
		log.info("Sent refrigerator ajar notifications.");
	}
	
	
	/**
	 * Sends email/SMS notifications about events. Also updates twitter feed.
	 */
	public void sendEventNotifications(String event){
		ArrayList<String> eventRecipients = this.recipients.get("events");
		if(eventRecipients == null){
			log.error("Event recipients not found.");
			return;
		}
		SendMessageSSL messageSender = new SendMessageSSL(eventRecipients);
		messageSender.setUsername(this.gmailUser);
		messageSender.setPassword(gmailPass);
		messageSender.sendMessage(event + " is happening.");
		log.info("Sent events notifications.");
	}
	
	public void sendTrafficNotifications(String incident){
		ArrayList<String> eventRecipients = this.recipients.get("traffic");
		if(eventRecipients == null){
			log.error("Traffic recipients not found.");
			return;
		}
		SendMessageSSL messageSender = new SendMessageSSL(eventRecipients);
		messageSender.sendMessage("TRAFFIC ALERT: " + incident);
		System.out.println("Sent Traffic notifications.");
	}
	
	public static void main(String[] args){
		//For testing purposes only.
		//NotificationInterface ni = new NotificationInterface();
		//ni.sendTrafficNotifications("Rt. 1 is down");
	}

	public String getGmailUser() {
		return gmailUser;
	}

	public void setGmailUser(String gmailUser) {
		this.gmailUser = gmailUser;
	}

	public String getGmailPass() {
		return gmailPass;
	}

	public void setGmailPass(String gmailPass) {
		this.gmailPass = gmailPass;
	}
}
