/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Sumedh Sawant
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.solver.notifications;

import java.sql.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Class that uses the JDBC api to connect to the mySQL database and make queries to 
 * delete or just get recipients.
 * 
 * @author Sumedh Sawant
 *
 */
public class JdbcConnector{

	Hashtable<String, ArrayList<String>> data;

	String[] categories = new String[]{"teatime", "fresh coffee", "refrigerator", "events", "traffic"};


	private static final Logger log = LoggerFactory.getLogger(JdbcConnector.class);
	
	protected String dbUrl = "jdbc:mysql://localhost/recipients";
	protected String dbClass = "com.mysql.jdbc.Driver";
	protected String deleteStatement = "DELETE FROM People WHERE ?=" + "'?'";
	protected String selectStatement = null;
	
	private String username = null;
	
	private String password = null;
	
	public JdbcConnector(){
		this.data = new Hashtable<String, ArrayList<String>>();
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT ");
		boolean isFirst = true;
		for(String category : categories){
			if(!isFirst)
			{
				sb.append(" ,");
			}
			isFirst = false;
			sb.append(category);
		}
		
		sb.append(" FROM recipients_list");
		log.info(sb.toString());
		this.selectStatement = sb.toString();
	}
	
	public void getFromSQL(){
		String dbUrl = "jdbc:mysql://localhost/notifications";
		String dbClass = "com.mysql.jdbc.Driver";
		

		try {
			Class.forName(dbClass);
			Connection con = DriverManager.getConnection(dbUrl, this.username, this.password);
			con.setAutoCommit(true);
			
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(this.selectStatement);
			
			String s1 = "";
			String s2 = "";
			String s3 = "";
			String s4 = "";
			String s5 = "";
			
		while (rs.next()){
			//the index number is the column number (ex. 1-4 for a 4 column table)
			s1 += rs.getString(1) + "\n";
			s2 += rs.getString(2) + "\n";
			s3 += rs.getString(3) + "\n";
			s4 += rs.getString(4) + "\n";
			s5 += rs.getString(5) + "\n";
		} 
			String[] array1 = s1.split("[\\r\\n]+");
			String[] array2 = s2.split("[\\r\\n]+");
			String[] array3 = s3.split("[\\r\\n]+");
			String[] array4 = s4.split("[\\r\\n]+");
			String[] array5 = s5.split("[\\r\\n]+");
			
			this.data.put(categories[0], getRecipients(array1));
			this.data.put(categories[1], getRecipients(array2));
			this.data.put(categories[2], getRecipients(array3));
			this.data.put(categories[3], getRecipients(array4));
			this.data.put(categories[4], getRecipients(array5));
	
			con.close();
		} 
		
		catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		catch(SQLException e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Deletes recipient from mySQL database in case they choose they do not want to receive notifications.
	 * 
	 * @param recipient
	 */
	public void deleteFromSQL(String recipient){
		this.getData();
		
		Enumeration<String> keys = this.data.keys();
		String category = null;
		while(keys.hasMoreElements()){
			String key = keys.nextElement();
			if(this.data.get(key).contains(recipient)){
				category = key;
				break;
			}
		}
		
		if(category==null){
			log.error("Recipient not found in table!");
			return;
		}
		
		Connection con = null;
		PreparedStatement ps = null;
		try{
//			Class.forName(dbClass);
			con = DriverManager.getConnection(this.dbUrl, this.username, this.password);
			con.setAutoCommit(false);
			ps = con.prepareStatement(this.deleteStatement);
			ps.setString(1, category);
			ps.setString(2, recipient);
			ps.execute();
			con.commit();
		} catch(SQLException e) {
			e.printStackTrace();
			if(con != null)
			{
				log.info("Rolling back transaction.");
				try {
					con.rollback();
				}
				catch(SQLException sqle)
				{
					e.printStackTrace();
				}
			}
		}
		
		log.info("Recipient successfully deleted from mySQL database!");
		this.getFromSQL();
	}
	

	/**
	 * This method returns the Hashtable<String, ArrayList<String>> representation of the mySQL database.
	 * 
	 * @return Hashtable<String, ArrayList<String>>
	 */
	public Hashtable<String, ArrayList<String>> getData(){
		this.getFromSQL();
		return this.data;
	}
	
	public static ArrayList<String> getRecipients(String[] strArray){
		ArrayList<String> result = new ArrayList<String>();
		for(String recipient: strArray){
			if(recipient.contains("@")){
				result.add(recipient);
			}
		}
		return result;
	}
	

	//For testing purposes, this main method can be run to see the contents of the database.
	public static void main(String args[]){
		JdbcConnector j = new JdbcConnector();
		j.getFromSQL();
		System.out.println(j.data.get(j.categories[0]));
		System.out.println(j.data.get(j.categories[1]));
		System.out.println(j.data.get(j.categories[2]));
		System.out.println(j.data.get(j.categories[3]));
		System.out.println(j.data.get(j.categories[4]));
	}
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}  

} 