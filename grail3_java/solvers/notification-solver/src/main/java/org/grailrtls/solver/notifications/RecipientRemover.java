/*
 * GRAIL Real Time Localization System
 * Copyright (C) 2011 Rutgers University and Sumedh Sawant
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.grailrtls.solver.notifications;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Class that keeps checking email using the imap protocol at winlab.grailrtls@gmail.com for unsubcriptions.
 * Checks mail every 30 seconds assuming the first email in the inbox is the one from someone 
 * that wants to unsubscribe.
 * 
 * @author Sumedh Sawant
 *
 */
public class RecipientRemover {

	private JdbcConnector jdbcConnector = new JdbcConnector();
	private SendMessageSSL sendMessageSSL = new SendMessageSSL(null);
	
	private String username = null;
	private String password = null;
	private static final Logger log = LoggerFactory.getLogger(RecipientRemover.class);
	
	protected final Timer updateTimer = new Timer();
	
	public RecipientRemover(final String dbUsername, final String dbPassword){
		this.jdbcConnector.setUsername(dbUsername);
		this.jdbcConnector.setPassword(dbPassword);
		this.jdbcConnector.getFromSQL();
	}
	
	public void startUpdates()
	{
		// Perform scrapes every 10 minutes
		this.updateTimer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				RecipientRemover.this.checkAndRemoveSender();
			}
		}, 1000, 1000*60*10);
	}
	
	public String checkNewestMailSender(){
		Properties props = System.getProperties();
		props.setProperty("mail.store.protocol", "imaps");
 
		Session session = Session.getDefaultInstance(props,
			null);
 
		String result = "";
		try {
			Store store = session.getStore("imaps");
			store.connect("imap.gmail.com", this.username, this.password);
			// Get folder
			Folder folder = store.getFolder("INBOX");
			folder.open(Folder.READ_ONLY);
			// Get directory
			result = folder.getMessage(folder.getMessageCount()).getFrom()[0].toString();
			// Close connection
			folder.close(false);
			store.close();
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
		return result;
	}
	
	public void checkAndRemoveSender(){
		log.info("Checking Inbox for newest email and seeing if the recipient should be removed.");
		String sender = this.checkNewestMailSender();
		String recipientToBeRemoved = null;
		boolean shouldBreak = false;
		Hashtable<String, ArrayList<String>> temp = this.jdbcConnector.getData();
		Enumeration<String> keys = temp.keys();
		
		while(keys.hasMoreElements()){
			ArrayList<String> recipients = temp.get(keys.nextElement());
			for(String recipient: recipients){
				if(sender.contains(recipient.trim().toLowerCase().split("@")[0])){
					recipientToBeRemoved = recipient;
					shouldBreak = true;
					break;
				}
			}
			if(shouldBreak){
				break;
			}
		}
		
		if(recipientToBeRemoved == null){
			log.info("Nothing to be removed.");
			return;
		}
		else{
			log.info("Found a recipient to be removed!");
			this.jdbcConnector.deleteFromSQL(recipientToBeRemoved);
			this.sendMessageSSL.sendRemovalMessage(recipientToBeRemoved);
		}
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}	
}
