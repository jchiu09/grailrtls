package org.grailrtls.client.gui.swing;

import org.grailrtls.libworldmodel.client.protocol.messages.DataResponseMessage;

public class WorldServerInfoNode {

	private DataResponseMessage data;

	@Override
	public String toString() {
		if (this.data == null) {
			return "[NULL]";
		} else {
			return this.data.getUri();
		}
	}

	public DataResponseMessage getData() {
		return data;
	}

	public void setData(DataResponseMessage data) {
		this.data = data;
	}
}
