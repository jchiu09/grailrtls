package org.grailrtls.client.gui.swing.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.DefaultListModel;
import javax.swing.SwingUtilities;

import org.grailrtls.client.gui.swing.SolutionPrinter;
import org.grailrtls.libworldmodel.client.protocol.messages.Attribute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SolutionsListModel extends DefaultListModel {

	private static final Logger log = LoggerFactory
			.getLogger(SolutionsListModel.class);

	private static final int DEFAULT_MAX_SOLUTIONS = 100;

	private int maxSolutions = DEFAULT_MAX_SOLUTIONS;

	// private List<TimestampSolution> recentSolutions = Collections
	// .synchronizedList(new ArrayList<TimestampSolution>());

	public static class TimestampSolution {

		private final Attribute solution;

		private final String toString;

		public TimestampSolution(final String objectUri,
				final Attribute solution, final long timestamp) {
			SimpleDateFormat sdf = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss.SSS");
			this.solution = solution;
			StringBuffer sb = new StringBuffer();
			sb.append('[').append(sdf.format(new Date(timestamp))).append("] ");
			sb.append(

			SolutionPrinter.clientSolutionToString(objectUri, solution));
			this.toString = sb.toString();
		}

		@Override
		public String toString() {
			return this.toString;
		}
	}

	public void addSolutions(final String objectUri, final Attribute[] solutions, final long timestamp) {
		if (solutions == null || solutions.length == 0) {
			return;
		}

		SwingUtilities.invokeLater(new Runnable() {

			public void run() {

				for (Attribute solution : solutions) {
					add(0, new TimestampSolution(objectUri, solution, timestamp));
				}

				if (size() > maxSolutions) {

					while (size() > maxSolutions) {
						remove(size() - 1);
					}
				}

			}
		});

	}
}
