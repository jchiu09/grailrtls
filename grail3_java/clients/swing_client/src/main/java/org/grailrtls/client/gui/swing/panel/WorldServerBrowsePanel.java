package org.grailrtls.client.gui.swing.panel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

import org.grailrtls.client.gui.swing.WorldServerInfoNode;
import org.grailrtls.client.gui.swing.model.FieldInfoTableModel;
import org.grailrtls.libworldmodel.client.ClientWorldModelInterface;
import org.grailrtls.libworldmodel.client.listeners.ConnectionListener;
import org.grailrtls.libworldmodel.client.listeners.DataListener;
import org.grailrtls.libworldmodel.client.protocol.codec.SnapshotRequestEncoder;
import org.grailrtls.libworldmodel.client.protocol.messages.AbstractRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.AttributeAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.DataResponseMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginPreferenceMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.SnapshotRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.URISearchResponseMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WorldServerBrowsePanel extends JPanel implements DataListener,
		ConnectionListener, TreeSelectionListener {

	private static final Logger log = LoggerFactory
			.getLogger(WorldServerBrowsePanel.class);

	protected ClientWorldModelInterface worldModel;

	private final DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode();

	private final ConcurrentHashMap<String, DefaultMutableTreeNode> nodesByUriMap = new ConcurrentHashMap<String, DefaultMutableTreeNode>();

	private final DefaultTreeModel treeModel = new DefaultTreeModel(
			this.rootNode);

	private final JTree jTree = new JTree(this.treeModel);

	private final FieldInfoTableModel fieldsTableModel = new FieldInfoTableModel();

	private final JTable fieldsTable = new JTable(this.fieldsTableModel);

	private String displayedUri = null;

	public WorldServerBrowsePanel() {
		this.setLayout(new BorderLayout());

		JScrollPane scrollPane = new JScrollPane(this.jTree);

		this.jTree.getSelectionModel().setSelectionMode(
				TreeSelectionModel.SINGLE_TREE_SELECTION);

		this.jTree.addTreeSelectionListener(this);

		JScrollPane scrollTable = new JScrollPane(this.fieldsTable,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollTable.setPreferredSize(new Dimension(640, 100));

		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
				scrollPane, scrollTable);
		this.add(splitPane, BorderLayout.CENTER);
	}

	public ClientWorldModelInterface getWorldModel() {
		return this.worldModel;
	}

	public void setWorldServer(ClientWorldModelInterface worldServer) {
		if (this.worldModel != null) {
			this.worldModel.removeDataListener(this);
			this.worldModel.removeConnectionListener(this);
		}
		this.worldModel = worldServer;
		this.worldModel.addDataListener(this);
		this.worldModel.addConnectionListener(this);
		WorldServerInfoNode node = new WorldServerInfoNode();
		DataResponseMessage message = new DataResponseMessage();
		message.setUri(this.worldModel.getHost() + ":"
				+ this.worldModel.getPort());
		node.setData(message);
		this.rootNode.setUserObject(node);
		this.treeModel.reload(this.rootNode);

	}

	protected void redraw() {
		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				repaint();
			}
		});
	}

	public void valueChanged(TreeSelectionEvent e) {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) this.jTree
				.getLastSelectedPathComponent();

		log.info("Selected {}", node);

		if (node == null || node == this.rootNode) {
			return;
		}
		WorldServerInfoNode info = null;
		synchronized (node) {
			info = (WorldServerInfoNode) node.getUserObject();
		}
		if (info == null) {

			if (!node.equals(this.rootNode)) {
				log.warn("Found a null user object at {}", node);
			}
			return;
		}
		DataResponseMessage data = info.getData();

		if (data == null) {
			log.warn("Found a null PDM at {}", info);
			return;
		}
		String objectUri = data.getUri();
		if (objectUri != null) {
			SnapshotRequestMessage request = new SnapshotRequestMessage();
			request.setBeginTimestamp(0l);
			request.setEndTimestamp(System.currentTimeMillis());
			request.setQueryAttributes(new String[]{".*"});
			request.setQueryURI(objectUri);
			this.worldModel.sendMessage(request);
			this.displayedUri = objectUri;
			log.info("Requested {}", objectUri);
		}
	}


	@Override
	public void connectionInterrupted(ClientWorldModelInterface worldModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connectionEnded(ClientWorldModelInterface worldModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connectionEstablished(ClientWorldModelInterface worldModel) {
		this.rootNode.setUserObject(worldModel.getHost() + ":"
				+ worldModel.getPort());
		this.treeModel.reload(this.rootNode);
	}

	@Override
	public void requestCompleted(ClientWorldModelInterface worldModel,
			AbstractRequestMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dataResponseReceived(ClientWorldModelInterface worldModel,
			DataResponseMessage message) {
		String responseUri = message.getUri();

		DefaultMutableTreeNode node = this.nodesByUriMap.get(responseUri);
		if (node == null) {
			log.warn("Unrecognized URI: {}", responseUri);
			return;
		}
		WorldServerInfoNode info = (WorldServerInfoNode) node.getUserObject();

		if (info == null) {
			info = new WorldServerInfoNode();
			node.setUserObject(info);
		}
		DataResponseMessage oldMessage = info.getData();
		info.setData(message);
		log.info(String.format("Replaced %s, %s->%s", info, oldMessage,
				message));

		if (message.getUri().equals(this.displayedUri)) {
			this.fieldsTableModel.setFields(message.getAttributes());
			this.displayedUri = null;
		} else {
			log.info("Displayed uri doesn't match {}|{}",
					message.getUri(), this.displayedUri);
		}
	}

	@Override
	public void uriSearchResponseReceived(ClientWorldModelInterface worldModel,
			URISearchResponseMessage message) {
		if (message.getMatchingUris() == null) {
			log.warn("No search results returned: {}", message);
			return;
		}
		for (String uri : message.getMatchingUris()) {
			// Start at the root, and walk until we find a missing node, then
			// add.
			DefaultMutableTreeNode parentNode = this.rootNode;

			log.info("URI: {}", uri);

			String[] uriComponents = uri.split("\\.");
			StringBuffer partialUri = new StringBuffer();
			for (int i = 0; i < uriComponents.length; ++i) {
				if (i > 0) {
					partialUri.append('.');
				}
				partialUri.append(uriComponents[i]);
				log.info("Curr node: {}/{}", uriComponents[i],
						partialUri.toString());

				// See if there is already a node for this URI
				DefaultMutableTreeNode currNode = this.nodesByUriMap
						.get(partialUri.toString());

				// There is already a node
				if (currNode != null) {
					// Set our parent node, and go to the next URI component
					parentNode = currNode;
					continue;
				}
				// Create the new node
				currNode = new DefaultMutableTreeNode();
				// Set the current URI component as the User Object
				DataResponseMessage data = new DataResponseMessage();
				data.setUri(partialUri.toString());
				WorldServerInfoNode info = new WorldServerInfoNode();
				info.setData(data);

				currNode.setUserObject(info);
				// Map the partial URI string to this node.
				this.nodesByUriMap.put(partialUri.toString(), currNode);
				// Add this node to the parent's children.
				parentNode.add(currNode);
				parentNode = currNode;
			}

		}

		this.redraw();
	}

	@Override
	public void attributeAliasesReceived(
			ClientWorldModelInterface clientWorldModelInterface,
			AttributeAliasMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void originAliasesReceived(
			ClientWorldModelInterface clientWorldModelInterface,
			OriginAliasMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void originPreferenceSent(ClientWorldModelInterface worldModel,
			OriginPreferenceMessage message) {
		// TODO Auto-generated method stub
		
	}

}
