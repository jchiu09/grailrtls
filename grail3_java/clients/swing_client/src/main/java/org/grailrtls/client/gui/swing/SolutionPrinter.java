package org.grailrtls.client.gui.swing;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

import org.grailrtls.libworldmodel.client.protocol.messages.Attribute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SolutionPrinter {

	private static final Logger log = LoggerFactory
			.getLogger(SolutionPrinter.class);

	public static String clientSolutionToString(final String objectUri,
			final Attribute attribute) {
		StringBuffer sb = new StringBuffer();

		if (attribute.getAttributeName() == null) {
			return "[Unknown]";
		}

		if ("passive motion.tile".equals(attribute.getAttributeName())) {
			sb.append("Passive Motion @ ").append(objectUri);
			int numBytes = attribute.getData().length;
			int numTiles = numBytes / 20;
			sb.append(": ").append(numTiles).append(" tiles");
		} else if ("closed".equals(attribute.getAttributeName())) {
			sb.append(objectUri);
			if (attribute.getData()[0] == 1) {
				sb.append(" is closed.");
			} else {
				sb.append(" is open.");
			}
		} else if ("location".equals(attribute.getAttributeName())) {
			double x, y, width, height;
			ByteBuffer buff = ByteBuffer.wrap(attribute.getData());
			x = buff.getDouble();
			y = buff.getDouble();
			width = buff.getDouble();
			height = buff.getDouble();
			sb.append(objectUri)
					.append(" is at ")
					.append(String.format("(%.2f\u00B1%.2f, %.2f\u00B1%.2f)",
							x, width, y, height));
		} else if ("mobility".equals(attribute.getAttributeName())) {
			sb.append(objectUri).append(" is ");
			if (attribute.getData()[0] == 0) {
				sb.append("not ");
			}
			sb.append("moving");
		} else if ("on".equals(attribute.getAttributeName())) {
			sb.append(objectUri).append(" is ");
			if (attribute.getData()[0] == 0) {
				sb.append("off");
			} else {
				sb.append("on");
			}
		} else if ("used".equals(attribute.getAttributeName())) {
			sb.append(objectUri).append(" is ");
			if (attribute.getData()[0] == 0) {
				sb.append("not ");
			}
			sb.append("in use.");
		} else if ("weather.temperature".equals(attribute.getAttributeName())) {
			try {
				sb.append(objectUri)
						.append("'s temperature is ")
						.append(new String(attribute.getData(), "UTF-16BE"))
						.append(" F");
			} catch (UnsupportedEncodingException e) {
				log.error("Unable to decode weather temperature information.",
						e);
			}
		} else if ("weather.condition".equals(attribute.getAttributeName())) {
			try {
				sb.append(objectUri).append(" currently ")
						.append(new String(attribute.getData(), "UTF-16BE"));
			} catch (UnsupportedEncodingException e) {
				log.error("Unable to decode weather temperature information.",
						e);
			}
		} else if ("weather.humidity".equals(attribute.getAttributeName())) {
			try {
				sb.append(objectUri).append(" has ")
						.append(new String(attribute.getData(), "UTF-16BE"))
						.append("% humidity.");
			} catch (UnsupportedEncodingException e) {
				log.error("Unable to decode weather temperature information.",
						e);
			}
		} else if ("weather.windchill".equals(attribute.getAttributeName())) {
			try {
				sb.append(objectUri).append(" feels like ")
						.append(new String(attribute.getData(), "UTF-16BE"))
						.append("F");
			} catch (UnsupportedEncodingException e) {
				log.error("Unable to decode weather temperature information.",
						e);
			}
		} else {
			sb.append(attribute);
		}

		return sb.toString();

	}
}
