package org.grailrtls.client.gui.swing.panel;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.grailrtls.client.gui.swing.model.SolutionInfoTableModel;
import org.grailrtls.client.gui.swing.model.SolutionsListModel;
import org.grailrtls.libworldmodel.client.ClientWorldModelInterface;
import org.grailrtls.libworldmodel.client.listeners.ConnectionListener;
import org.grailrtls.libworldmodel.client.listeners.DataListener;
import org.grailrtls.libworldmodel.client.protocol.messages.AbstractRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.AttributeAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.DataResponseMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginPreferenceMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.URISearchResponseMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DistributorRequestPanel extends JPanel implements
		ConnectionListener, DataListener {
	private static final Logger log = LoggerFactory
			.getLogger(DistributorRequestPanel.class);

	protected ClientWorldModelInterface distributorInterface;

	private JLabel distributorAddressLabel = new JLabel("Disconnected");

	private SolutionInfoTableModel solutionTableModel = new SolutionInfoTableModel();

	private JTable solutionStatisticsTable = new JTable(solutionTableModel);

	private SolutionsListModel recentSolutionModel = new SolutionsListModel();

	private JList recentSolutionsList = new JList(this.recentSolutionModel);

	public DistributorRequestPanel() {
		super();
		this.setLayout(new BorderLayout());
		this.add(this.distributorAddressLabel, BorderLayout.NORTH);

		this.recentSolutionsList
				.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		JScrollPane statisticsScroll = new JScrollPane(
				this.solutionStatisticsTable);

		JScrollPane recentSolutionScroll = new JScrollPane(
				this.recentSolutionsList);

		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
				statisticsScroll, recentSolutionScroll);
		this.add(splitPane, BorderLayout.CENTER);
	}

	public ClientWorldModelInterface getWorldModelInterface() {
		return this.distributorInterface;
	}

	public void setWorldModelInterface(
			ClientWorldModelInterface distributorInterface) {
		if (this.distributorInterface != null) {
			this.distributorInterface.removeConnectionListener(this);
			this.distributorInterface.removeDataListener(this);
		}
		this.distributorInterface = distributorInterface;
		this.distributorInterface.addConnectionListener(this);
		this.distributorInterface.addDataListener(this);
	}

	@Override
	public void requestCompleted(ClientWorldModelInterface worldModel,
			AbstractRequestMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dataResponseReceived(ClientWorldModelInterface worldModel,
			DataResponseMessage message) {
		if (message == null)
			return;

		this.solutionTableModel.addSolution(message);

		this.recentSolutionModel.addSolutions(message.getUri(), message.getAttributes(),
				System.currentTimeMillis());

		log.info("Received {}", message);
	}

	@Override
	public void uriSearchResponseReceived(ClientWorldModelInterface worldModel,
			URISearchResponseMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void attributeAliasesReceived(
			ClientWorldModelInterface clientWorldModelInterface,
			AttributeAliasMessage message) {
		this.solutionTableModel.setDataTypes(message.getAliases());
		this.solutionTableModel.fireTableDataChanged();
	}

	@Override
	public void originAliasesReceived(
			ClientWorldModelInterface clientWorldModelInterface,
			OriginAliasMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connectionInterrupted(ClientWorldModelInterface worldModel) {
		this.distributorAddressLabel.setText("Disconnected");
	}

	@Override
	public void connectionEnded(ClientWorldModelInterface worldModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connectionEstablished(ClientWorldModelInterface worldModel) {
		this.distributorAddressLabel.setText(worldModel.getHost() + ":"
				+ worldModel.getPort());
	}

	@Override
	public void originPreferenceSent(ClientWorldModelInterface worldModel,
			OriginPreferenceMessage message) {
		// TODO Auto-generated method stub
		
	}

}
