package org.grailrtls.client.gui.swing.location;

import java.awt.Component;
import java.awt.Graphics;

public abstract class Location {

	protected double x;
	protected double y;
	protected String uri;
	
	protected int screenX, screenY;
	
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public int getScreenX() {
		return screenX;
	}
	public void setScreenX(int screenX) {
		this.screenX = screenX;
	}
	public int getScreenY() {
		return screenY;
	}
	public void setScreenY(int screenY) {
		this.screenY = screenY;
	}
	
	public abstract void paint(Graphics g, int height, float objScale, float xScale, float yScale);
	
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		
		if(this.uri != null)
		{
			sb.append(this.uri).append(' ');
		}
		sb.append("(").append(String.format("%.2f",getX())).append(", ").append(String.format("%.2f",getY())).append(')');
		
		return sb.toString();
	}
}
