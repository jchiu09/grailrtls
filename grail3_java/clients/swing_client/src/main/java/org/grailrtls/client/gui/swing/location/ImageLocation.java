package org.grailrtls.client.gui.swing.location;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImageLocation extends Location {
	private static final Logger log = LoggerFactory
			.getLogger(ImageLocation.class);

	protected BufferedImage image;

	public BufferedImage getImage() {
		return image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

	public void paint(Graphics g, int height, float objScale, float xScale,
			float yScale) {
		if (this.getImage() == null) {
			log.info("Not drawing {}", this);
			return;
		}
		this.setScreenX((int) (this.getX() * xScale));
		this.setScreenY((int) (height - this.getY() * yScale));

		g.drawImage(this.getImage(), (int) (this.getScreenX() - this.getImage()
				.getWidth()
				* objScale / 2), (int) (this.getScreenY() - this.getImage()
				.getHeight()
				* objScale / 2), (int) (this.getScreenX() + this.getImage()
				.getWidth()
				* objScale / 2), (int) (this.getScreenY() + this.getImage()
				.getHeight()
				* objScale / 2), 0, 0, this.getImage().getWidth(), this
				.getImage().getHeight(), null);
	}
}
