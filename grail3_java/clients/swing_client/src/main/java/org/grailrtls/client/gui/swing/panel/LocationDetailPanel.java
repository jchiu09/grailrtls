package org.grailrtls.client.gui.swing.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSplitPane;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


import org.grailrtls.libworldmodel.client.ClientWorldModelInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LocationDetailPanel extends JPanel implements ChangeListener {

	private static final Logger log = LoggerFactory
			.getLogger(LocationDetailPanel.class);

	private JSlider iconSizeSlider = new JSlider(SwingConstants.HORIZONTAL);
	
	private JCheckBox showEllipse = new JCheckBox("Use Ellipse (Slow)");
	
	private JCheckBox showUnknown = new JCheckBox("Unknown Devices");
	
	private JCheckBox showFiduciaryTransmitters = new JCheckBox("Fiduciary Transmitters");
	
	private JCheckBox showReceivers = new JCheckBox("Receivers");
	
	private JCheckBox showMugs = new JCheckBox("Mugs");
	
	private JCheckBox showDoors = new JCheckBox("Doors");
	
	private JCheckBox showOthers = new JCheckBox("Others");
	
	private JCheckBox showPassiveMotion = new JCheckBox("Passive Motion");

	private LayeredLocationPanel mapPanel = new LayeredLocationPanel();

	public LocationDetailPanel() {
		super();
		
		this.iconSizeSlider.setMinimum(2);
		this.iconSizeSlider.setMaximum(10);
		this.iconSizeSlider.setMinorTickSpacing(1);
		this.iconSizeSlider.setMajorTickSpacing(2);
		this.iconSizeSlider
				.setValue((int) (this.mapPanel.getIconScale() * 10f));
		
		this.showEllipse.setSelected(this.mapPanel.isShowEllipse());
		this.showUnknown.setSelected(this.mapPanel.isShowUnknownDevices());
		this.showFiduciaryTransmitters.setSelected(this.mapPanel.isShowFiduciaryTransmitters());
		this.showReceivers.setSelected(this.mapPanel.isShowReceivers());
		this.showMugs.setSelected(this.mapPanel.isShowMugs());
		this.showDoors.setSelected(this.mapPanel.isShowDoors());
		this.showOthers.setSelected(this.mapPanel.isShowOthers());
		this.showPassiveMotion.setSelected(this.mapPanel.isShowPassiveMotion());

		this.showEllipse.addChangeListener(this);
		this.showUnknown.addChangeListener(this);
		this.showFiduciaryTransmitters.addChangeListener(this);
		this.showReceivers.addChangeListener(this);
		this.showMugs.addChangeListener(this);
		this.showDoors.addChangeListener(this);
		this.showOthers.addChangeListener(this);
		this.showPassiveMotion.addChangeListener(this);
		
		this.setLayout(new BorderLayout());

		JPanel rightPanel = new JPanel();
		rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
		this.iconSizeSlider.setAlignmentX(LEFT_ALIGNMENT);
		this.showEllipse.setAlignmentX(LEFT_ALIGNMENT);
		this.showUnknown.setAlignmentX(LEFT_ALIGNMENT);
		this.showFiduciaryTransmitters.setAlignmentX(LEFT_ALIGNMENT);
		this.showReceivers.setAlignmentX(LEFT_ALIGNMENT);
		this.showMugs.setAlignmentX(LEFT_ALIGNMENT);
		this.showDoors.setAlignmentX(LEFT_ALIGNMENT);
		this.showOthers.setAlignmentX(LEFT_ALIGNMENT);
		this.showPassiveMotion.setAlignmentX(LEFT_ALIGNMENT);
		rightPanel.add(this.iconSizeSlider);
		rightPanel.add(this.showEllipse);
		rightPanel.add(this.showUnknown);
		rightPanel.add(this.showFiduciaryTransmitters);
		rightPanel.add(this.showReceivers);
		rightPanel.add(this.showMugs);
		rightPanel.add(this.showDoors);
		rightPanel.add(this.showOthers);
		rightPanel.add(this.showPassiveMotion);
		
		this.iconSizeSlider.addChangeListener(this);
		
		Border blackline = BorderFactory.createLineBorder(Color.BLACK);
		this.iconSizeSlider.setBorder(BorderFactory.createTitledBorder("Icon Size"));
		
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				new JScrollPane(mapPanel), rightPanel);
		splitPane.setResizeWeight(.9);
		splitPane.setOneTouchExpandable(true);
		splitPane.setContinuousLayout(true);

		this.add(splitPane, BorderLayout.CENTER);
	}

	public void setWorldModelInterface(
			ClientWorldModelInterface worldModelInterface) {
		this.mapPanel.setWorldModelInterface(worldModelInterface);
	}

	public void addRegion(String regionUri) {
		this.mapPanel.addRegion(regionUri);
	}

	public void stateChanged(ChangeEvent arg0) {
		if (arg0.getSource().equals(this.iconSizeSlider)) {
			this.mapPanel.setIconScale(this.iconSizeSlider.getValue() / 10f);
		}
		else if(arg0.getSource().equals(this.showEllipse))
		{
			this.mapPanel.setShowEllipse(this.showEllipse.isSelected());
		}else if(arg0.getSource().equals(this.showUnknown))
		{
			this.mapPanel.setShowUnknownDevices(this.showUnknown.isSelected());
		}
		else if(arg0.getSource().equals(this.showFiduciaryTransmitters))
		{
			this.mapPanel.setShowFiduciaryTransmitters(this.showFiduciaryTransmitters.isSelected());
		}
		else if(arg0.getSource().equals(this.showReceivers))
		{
			this.mapPanel.setShowReceivers(this.showReceivers.isSelected());
		}
		else if(arg0.getSource().equals(this.showMugs))
		{
			this.mapPanel.setShowMugs(this.showMugs.isSelected());
		}
		else if(arg0.getSource().equals(this.showDoors))
		{
			this.mapPanel.setShowDoors(this.showDoors.isSelected());
		}
		else if(arg0.getSource().equals(this.showOthers))
		{
			this.mapPanel.setShowOthers(this.showOthers.isSelected());
		}
		else if(arg0.getSource().equals(this.showPassiveMotion))
		{
			this.mapPanel.setShowPassiveMotion(this.showPassiveMotion.isSelected());
		}
	}
}
