package org.grailrtls.client.gui.swing.location;

import java.awt.AlphaComposite;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EllipseLocation extends ImageLocation {
	
	private static final Logger log = LoggerFactory.getLogger(EllipseLocation.class);

	protected double width;

	protected double height;

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}
	
	@Override
	public void paint(Graphics g, int screenHeight, float objScale, float xScale, float yScale)
	{
		this.paint(g, screenHeight, objScale, xScale, yScale, false);
	}
	
	public void paint(Graphics g, int screenHeight, float objScale, float xScale, float yScale, boolean asEllipse){
		
		if(!asEllipse || this.getImage() == null)
		{
			super.paint(g, screenHeight, objScale, xScale, yScale);
			return;
		}
		
		Graphics2D g2 = (Graphics2D)g;
		Composite origComposite = g2.getComposite();
		g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, .5f));
		
		this.setScreenX((int) (this.getX() * xScale));
		this
				.setScreenY((int) (screenHeight - this
						.getY() * yScale));
		
		Ellipse2D.Double drawEllipse = new Ellipse2D.Double();
		drawEllipse.x = this.getX() - this.getWidth() / 2;
		drawEllipse.x *= xScale;
		drawEllipse.y = (screenHeight - this
				.getY()*yScale)
				- this.getHeight()*yScale / 2;
		drawEllipse.width = this.getWidth() * xScale;
		drawEllipse.height = this.getHeight() * yScale;
		
		g2.fill(drawEllipse);
		g2.setComposite(origComposite);
	}
}
