package org.grailrtls.client.gui.swing.model;

import javax.swing.table.AbstractTableModel;

import org.grailrtls.libworldmodel.client.protocol.messages.Attribute;
import org.grailrtls.libworldmodel.client.protocol.messages.AttributeAliasMessage.AttributeAlias;
import org.grailrtls.libworldmodel.client.protocol.messages.DataResponseMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SolutionInfoTableModel extends AbstractTableModel {

	private static final Logger log = LoggerFactory
			.getLogger(SolutionInfoTableModel.class);

	private AttributeAlias[] dataTypes;
	private int[] dataTypeCount;

	private String[] COLUMN_NAMES = { "Alias", "Attribute", "Received" };

	public AttributeAlias[] getDataTypes() {
		return dataTypes;
	}

	public void setDataTypes(AttributeAlias[] dataTypes) {
		this.dataTypes = dataTypes;
		if (this.dataTypes == null) {
			this.dataTypeCount = null;
			return;
		}

		this.dataTypeCount = new int[this.dataTypes.length];
		for (int i = 0; i < this.dataTypes.length; ++i) {
			this.dataTypeCount[i] = 0;
		}
		this.fireTableDataChanged();
	}

	public int getColumnCount() {
		// Alias ID, Solution name, # solutions received
		return 3;
	}

	public int getRowCount() {
		if (this.dataTypes == null) {
			return 0;
		}
		return this.dataTypes.length;
	}

	public Object getValueAt(int row, int col) {
		if (this.dataTypes == null)
			return null;
		if (row < 0 || row >= this.dataTypes.length)
			return null;
		if (col < 0 || col >= 3)
			return null;

		switch (col) {
		case 0:
			return Integer.valueOf(this.dataTypes[row].aliasNumber);
		case 1:
			return this.dataTypes[row].aliasName;
		case 2:
			return Integer.valueOf(this.dataTypeCount[row]);
		}
		return null;
	}

	public void addSolution(final DataResponseMessage solution) {
		if (solution == null)
			return;
		if (this.dataTypes == null)
			return;
		for (Attribute attrib : solution.getAttributes()) {
			int typeAlias = attrib.getAttributeNameAlias();
			if (typeAlias < 0 || typeAlias >= this.dataTypes.length)
				return;

			++this.dataTypeCount[typeAlias];
			this.fireTableCellUpdated(typeAlias, 2);
		}
	}

	@Override
	public String getColumnName(int col) {
		if (col < 0 || col >= COLUMN_NAMES.length)
			return null;
		return COLUMN_NAMES[col];
	}

}
