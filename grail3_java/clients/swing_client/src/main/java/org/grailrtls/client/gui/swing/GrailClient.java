package org.grailrtls.client.gui.swing;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import org.grailrtls.client.gui.swing.panel.DistributorRequestPanel;
import org.grailrtls.client.gui.swing.panel.LocationDetailPanel;
import org.grailrtls.client.gui.swing.panel.WorldServerBrowsePanel;
import org.grailrtls.libworldmodel.client.ClientWorldModelInterface;
import org.grailrtls.libworldmodel.client.listeners.ConnectionListener;
import org.grailrtls.libworldmodel.client.listeners.DataListener;
import org.grailrtls.libworldmodel.client.protocol.messages.AbstractRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.AttributeAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.AttributeAliasMessage.AttributeAlias;
import org.grailrtls.libworldmodel.client.protocol.messages.DataResponseMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginPreferenceMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.SnapshotRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.StreamRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.URISearchResponseMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GrailClient implements Runnable, ConnectionListener, DataListener{

	private static final Logger log = LoggerFactory.getLogger(GrailClient.class);
	
	protected ClientWorldModelInterface worldModelInterface = new ClientWorldModelInterface();
	
	protected String worldModelHost;
	
	protected int worldModelPort;
	
	protected boolean requestSolutionsOnRestart = false;
	
	protected boolean worldModelConnectedOnce = false;
	
	public static void main(String args[])
	{
		if(args.length < 2)
		{
			printUsage();
			return;
		}
		
		int distributorPort = 0;
		try {
			distributorPort = Integer.parseInt(args[1]);
		}
		catch(NumberFormatException nfe)
		{
			nfe.printStackTrace();
			return;
		}
		
		
		GrailClient gc = new GrailClient();
		gc.setWorldModelHost(args[0]);
		gc.setWorldModelPort(distributorPort);
		
		Thread thread = new Thread(gc, "Client Frame");
		
		thread.start();
	}
	
	private static void printUsage()
	{
		StringBuffer sb = new StringBuffer();
		
		sb.append("Parameters: <World Model Host> <World Model Port>");
		
		System.err.println(sb.toString());
	}

	public String getWorldModelHost() {
		return worldModelHost;
	}

	public void setWorldModelHost(String distributorHost) {
		this.worldModelHost = distributorHost;
	}

	public int getWorldModelPort() {
		return worldModelPort;
	}

	public void setWorldModelPort(int worldModelPort) {
		this.worldModelPort = worldModelPort;
	}

	public void run()
	{
		JFrame frame = new JFrame();
		JTabbedPane tabPanel = new JTabbedPane();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		LocationDetailPanel locationDetails = new LocationDetailPanel();
		
		locationDetails.setWorldModelInterface(this.worldModelInterface);
		locationDetails.addRegion("winlab");
		
		locationDetails.setPreferredSize(new Dimension(640, 480));
		
		WorldServerBrowsePanel worldServerPanel = new WorldServerBrowsePanel();
		worldServerPanel.setWorldServer(this.worldModelInterface);
		
		DistributorRequestPanel distributorPanel = new DistributorRequestPanel();
		distributorPanel.setWorldModelInterface(this.worldModelInterface);
		
		tabPanel.addTab("Winlab Map", locationDetails);
		tabPanel.addTab("World Server", worldServerPanel);
		tabPanel.addTab("Distributor Stats", distributorPanel);
		frame.setLayout(new BorderLayout());
		frame.add(tabPanel, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);
		
		this.worldModelInterface.setHost(worldModelHost);
		this.worldModelInterface.setPort(worldModelPort);
		this.worldModelInterface.setStayConnected(true);
		this.worldModelInterface.addConnectionListener(this);
		
		this.worldModelInterface.registerSearchRequest(".*");
		
		if(!this.worldModelInterface.doConnectionSetup())
		{
			log.warn("World Model connection failed.");
			return;
		}
		
	}	
	boolean sentFullSearch = false;

	@Override
	public void requestCompleted(ClientWorldModelInterface worldModel,
			AbstractRequestMessage message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dataResponseReceived(ClientWorldModelInterface worldModel,
			DataResponseMessage message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void uriSearchResponseReceived(ClientWorldModelInterface worldModel,
			URISearchResponseMessage message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void attributeAliasesReceived(
			ClientWorldModelInterface clientWorldModelInterface,
			AttributeAliasMessage message) {
		if(!this.sentFullSearch)
		{
			StreamRequestMessage request = new StreamRequestMessage();
			request.setQueryURI(".*");
			request.setQueryAttributes(new String[]{".*"});
			request.setBeginTimestamp(System.currentTimeMillis());
			request.setUpdateInterval(0l);
			this.worldModelInterface.sendMessage(request);
			this.sentFullSearch = true;
		}
		
		AttributeAlias[] types = message.getAliases();

		if (types == null) {
			log.warn("No type specifications available.");
			return;
		}

	
	}

	@Override
	public void originAliasesReceived(
			ClientWorldModelInterface clientWorldModelInterface,
			OriginAliasMessage message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connectionInterrupted(ClientWorldModelInterface worldModel) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connectionEnded(ClientWorldModelInterface worldModel) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connectionEstablished(ClientWorldModelInterface worldModel) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void originPreferenceSent(ClientWorldModelInterface worldModel,
			OriginPreferenceMessage message) {
		// TODO Auto-generated method stub
		
	}
}
