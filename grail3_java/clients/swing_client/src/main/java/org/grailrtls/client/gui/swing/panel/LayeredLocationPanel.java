package org.grailrtls.client.gui.swing.panel;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Transparency;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.grailrtls.client.gui.swing.location.EllipseLocation;
import org.grailrtls.client.gui.swing.location.ImageLocation;
import org.grailrtls.client.gui.swing.location.Location;
import org.grailrtls.client.gui.swing.location.RectangleLocation;
import org.grailrtls.libcommon.util.FieldDecoder;
import org.grailrtls.libworldmodel.client.ClientWorldModelInterface;
import org.grailrtls.libworldmodel.client.listeners.ConnectionListener;
import org.grailrtls.libworldmodel.client.listeners.DataListener;
import org.grailrtls.libworldmodel.client.protocol.messages.AbstractRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.Attribute;
import org.grailrtls.libworldmodel.client.protocol.messages.AttributeAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.DataResponseMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginAliasMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.OriginPreferenceMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.StreamRequestMessage;
import org.grailrtls.libworldmodel.client.protocol.messages.URISearchResponseMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LayeredLocationPanel extends JPanel implements DataListener,
		ConnectionListener {

	private static final Logger log = LoggerFactory
			.getLogger(LayeredLocationPanel.class);

	protected static final String[] INTERESTING_URI = { "coffee pot", "doors",
			"microwaves", "transmitter", "receiver", "mug", "projector" };

	protected static final String LAYER_OTHER = "other";

	protected HashMap<String, Boolean> dirtyLayers = new HashMap<String, Boolean>();

	protected ConcurrentLinkedQueue<String> regionUris = new ConcurrentLinkedQueue<String>();

	protected ClientWorldModelInterface distributorInterface;

	protected ConcurrentHashMap<String, EllipseLocation> ellipseLocations = new ConcurrentHashMap<String, EllipseLocation>();

	protected ConcurrentLinkedQueue<RectangleLocation> rectangleLocations = new ConcurrentLinkedQueue<RectangleLocation>();

	// protected ConcurrentHashMap<String, ImageLocation> persistentLocations =
	// new ConcurrentHashMap<String, ImageLocation>();

	protected ConcurrentHashMap<String, ImageLocation> mugLocations = new ConcurrentHashMap<String, ImageLocation>();

	protected ConcurrentHashMap<String, ImageLocation> coffeePotLocations = new ConcurrentHashMap<String, ImageLocation>();

	protected ConcurrentHashMap<String, ImageLocation> doorLocations = new ConcurrentHashMap<String, ImageLocation>();

	protected ConcurrentHashMap<String, ImageLocation> microwaveLocations = new ConcurrentHashMap<String, ImageLocation>();

	protected ConcurrentHashMap<String, ImageLocation> fiduciaryTransmitterLocations = new ConcurrentHashMap<String, ImageLocation>();

	protected ConcurrentHashMap<String, ImageLocation> receiverLocations = new ConcurrentHashMap<String, ImageLocation>();

	protected ConcurrentHashMap<String, ImageLocation> projectorLocations = new ConcurrentHashMap<String, ImageLocation>();

	protected ConcurrentHashMap<String, String> mugMap = new ConcurrentHashMap<String, String>();

	protected BufferedImage transmitterImage;

	// Images are 64x64 pixels, start at 32x32
	protected double iconScale = .5;

	protected BufferedImage mugImage;

	protected BufferedImage coffeePotImage;

	protected BufferedImage doorOpenImage;

	protected BufferedImage doorClosedImage;

	protected BufferedImage microwaveOffImage;

	protected BufferedImage microwaveOnImage;

	protected BufferedImage fiduciaryTransmitterImage;

	protected BufferedImage receiverImage;

	protected BufferedImage projectorOnImage;

	protected BufferedImage projectorOffImage;

	protected boolean showEllipse = false;

	protected boolean showUnknownDevices = true;

	protected boolean showReceivers = false;

	protected boolean showFiduciaryTransmitters = false;

	protected boolean showMugs = true;

	protected boolean showDoors = true;

	protected boolean showOthers = true;

	protected boolean showPassiveMotion = true;

	public boolean isShowPassiveMotion() {
		return showPassiveMotion;
	}

	public void setShowPassiveMotion(boolean showPassiveMotion) {
		this.showPassiveMotion = showPassiveMotion;
	}

	protected Dimension regionDimensions;

	protected BufferedImage regionImage = null;

	protected String regionImageUri = null;

	protected boolean drawAsImage = true;

	protected long motionClearDelay = 1000l;

	protected Timer motionClearTimer = new Timer();

	protected TimerTask motionClearTask = null;

	public LayeredLocationPanel() {
		super();

		try {
			this.transmitterImage = ImageIO.read(getClass()
					.getResourceAsStream("/transmitter.png"));
			this.mugImage = ImageIO.read(getClass().getResourceAsStream(
					"/mug.png"));
			this.coffeePotImage = ImageIO.read(getClass().getResourceAsStream(
					"/coffee_maker_green.png"));
			this.doorOpenImage = ImageIO.read(getClass().getResourceAsStream(
					"/door_open.png"));
			this.doorClosedImage = ImageIO.read(getClass().getResourceAsStream(
					"/door_closed.png"));
			this.microwaveOffImage = ImageIO.read(getClass()
					.getResourceAsStream("/microwave_off.png"));
			this.microwaveOnImage = ImageIO.read(getClass()
					.getResourceAsStream("/microwave_on.png"));
			this.fiduciaryTransmitterImage = ImageIO.read(getClass()
					.getResourceAsStream("/fiduciary_transmitter.png"));
			this.receiverImage = ImageIO.read(getClass().getResourceAsStream(
					"/receiver.png"));
			this.projectorOffImage = ImageIO.read(getClass()
					.getResourceAsStream("/projector_off.png"));
			this.projectorOnImage = ImageIO.read(getClass()
					.getResourceAsStream("/projector_on.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.setToolTipText("Location status for Winlab");
	}

	public void setWorldModelInterface(
			ClientWorldModelInterface distributorInterface) {
		if (this.distributorInterface != null) {
			this.distributorInterface.removeConnectionListener(this);
			this.distributorInterface.removeDataListener(this);
		}

		this.distributorInterface = distributorInterface;
		this.distributorInterface.addConnectionListener(this);
		this.distributorInterface.addDataListener(this);
	}

	@Override
	public String getToolTipText(MouseEvent me) {

		log.debug("Searching for nearest tool-tip.");
		Location closestLocation = null;
		double shortestDistance = Float.MAX_VALUE;

		for (Location someLoc : this.ellipseLocations.values()) {
			double distance = Math.sqrt(Math.pow(
					someLoc.getScreenX() - me.getX(), 2)
					+ Math.pow(someLoc.getScreenY() - me.getY(), 2));
			if (distance < shortestDistance) {
				shortestDistance = distance;
				closestLocation = someLoc;
			}
		}

		for (Location someLoc : this.mugLocations.values()) {
			double distance = Math.sqrt(Math.pow(
					someLoc.getScreenX() - me.getX(), 2)
					+ Math.pow(someLoc.getScreenY() - me.getY(), 2));
			if (distance < shortestDistance) {
				shortestDistance = distance;
				closestLocation = someLoc;
			}
		}

		for (Location someLoc : this.coffeePotLocations.values()) {
			double distance = Math.sqrt(Math.pow(
					someLoc.getScreenX() - me.getX(), 2)
					+ Math.pow(someLoc.getScreenY() - me.getY(), 2));
			if (distance < shortestDistance) {
				shortestDistance = distance;
				closestLocation = someLoc;
			}
		}

		for (Location someLoc : this.doorLocations.values()) {
			double distance = Math.sqrt(Math.pow(
					someLoc.getScreenX() - me.getX(), 2)
					+ Math.pow(someLoc.getScreenY() - me.getY(), 2));
			if (distance < shortestDistance) {
				shortestDistance = distance;
				closestLocation = someLoc;
			}
		}

		for (Location someLoc : this.fiduciaryTransmitterLocations.values()) {
			double distance = Math.sqrt(Math.pow(
					someLoc.getScreenX() - me.getX(), 2)
					+ Math.pow(someLoc.getScreenY() - me.getY(), 2));
			if (distance < shortestDistance) {
				shortestDistance = distance;
				closestLocation = someLoc;
			}
		}

		for (Location someLoc : this.microwaveLocations.values()) {
			double distance = Math.sqrt(Math.pow(
					someLoc.getScreenX() - me.getX(), 2)
					+ Math.pow(someLoc.getScreenY() - me.getY(), 2));
			if (distance < shortestDistance) {
				shortestDistance = distance;
				closestLocation = someLoc;
			}
		}

		for (Location someLoc : this.receiverLocations.values()) {
			double distance = Math.sqrt(Math.pow(
					someLoc.getScreenX() - me.getX(), 2)
					+ Math.pow(someLoc.getScreenY() - me.getY(), 2));
			if (distance < shortestDistance) {
				shortestDistance = distance;
				closestLocation = someLoc;
			}
		}

		for (Location someLoc : this.projectorLocations.values()) {
			double distance = Math.sqrt(Math.pow(
					someLoc.getScreenX() - me.getX(), 2)
					+ Math.pow(someLoc.getScreenY() - me.getY(), 2));
			if (distance < shortestDistance) {
				shortestDistance = distance;
				closestLocation = someLoc;
			}
		}

		if (shortestDistance < 15) {

			return closestLocation.toString();
		}
		return null;

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		long startRender = System.currentTimeMillis();
		if (this.regionDimensions == null) {
			log.warn("Region dimensions are unknown");
			return;
		}

		Graphics2D g2 = (Graphics2D) g;
		Dimension windowSize = this.getSize();

		float xScale = (float) (windowSize.getWidth() / this.regionDimensions
				.getWidth());
		float yScale = (float) (windowSize.getHeight() / this.regionDimensions
				.getHeight());

		if (this.regionImage != null) {
			g2.drawImage(this.regionImage, 0, 0, (int) windowSize.getWidth(),
					(int) windowSize.getHeight(), 0, 0,
					this.regionImage.getWidth(), this.regionImage.getHeight(),
					null);
		}

		long startStatic = System.currentTimeMillis();
		// Image locations (mugs, coffee pots, microwaves, doors, etc.)
		if (this.showOthers) {
			BufferedImage overlay = g2.getDeviceConfiguration()
					.createCompatibleImage(this.getWidth(), this.getHeight(),
							Transparency.BITMASK);
			this.drawImageLocations(this.coffeePotLocations, g2, xScale, yScale);
			this.drawImageLocations(this.microwaveLocations, g2, xScale, yScale);
			this.drawImageLocations(this.projectorLocations, g2, xScale, yScale);
			this.overlayImage(g, overlay);
			overlay.getGraphics().dispose();
		}
		if (this.showDoors) {
			this.drawImageLocations(this.doorLocations, g2, xScale, yScale);
		}
		if (this.showFiduciaryTransmitters) {
			this.drawImageLocations(this.fiduciaryTransmitterLocations, g2,
					xScale, yScale);
		}
		if (this.showMugs) {
			this.drawImageLocations(this.mugLocations, g2, xScale, yScale);
		}
		if (this.showReceivers) {
			this.drawImageLocations(this.receiverLocations, g2, xScale, yScale);
		}

		long startLocations = System.currentTimeMillis();

		Color origColor;
		Composite origComposite;
		if (this.showUnknownDevices) {
			// Device locations
			origColor = g2.getColor();

			g2.setColor(Color.RED);

			for (String target : this.ellipseLocations.keySet()) {
				EllipseLocation ellipse = this.ellipseLocations.get(target);

				if (ellipse == null) {
					log.warn("Null ellipse found!");
					continue;
				}

				ellipse.paint(g, this.getHeight(), (float) this.iconScale,
						xScale, yScale, this.showEllipse);

			}
			g2.setColor(origColor);

		}

		long startPassive = System.currentTimeMillis();
		if (this.showPassiveMotion) {
			// Passive motion tiles
			origColor = g2.getColor();
			g2.setColor(Color.BLUE);
			for (RectangleLocation location : this.rectangleLocations) {
				float alpha = (float) location.getScore() / 5f;
				if (alpha > 1.0) {
					alpha = 1.0f;
				}
				AlphaComposite composite = this.makeComposite(alpha);
				origComposite = g2.getComposite();
				g2.setComposite(composite);
				Rectangle2D.Float drawRect = new Rectangle2D.Float();
				double tileHeight = location.getY2() - location.getY();
				drawRect.setRect(
						location.getX() * xScale,
						(this.regionDimensions.getHeight() - location.getY() - tileHeight)
								* yScale, (location.getX2() - location.getX())
								* xScale, (tileHeight) * yScale);
				g2.fill(drawRect);
				g2.setComposite(origComposite);
			}

			g2.setColor(origColor);
		}
		long now = System.currentTimeMillis();
		log.info(String.format("Rendered in %dms (S: %dms L: %dms P:%dms).",
				now - startRender, startLocations - startStatic, startPassive
						- startLocations, now - startPassive));
	}

	protected void overlayImage(Graphics baseGraphics, BufferedImage image) {
		Graphics2D g2 = (Graphics2D) baseGraphics;
		g2.drawImage(image, 0, 0, this.getWidth(), this.getHeight(), 0, 0,
				image.getWidth(), image.getHeight(), null);
	}

	protected void drawImageLocations(
			final Map<String, ImageLocation> locationsMap, final Graphics g,
			final float xScale, final float yScale) {
		for (String target : locationsMap.keySet()) {
			log.debug("Drawing image for {}", target);
			ImageLocation location = locationsMap.get(target);
			if (location == null) {
				log.warn("No location info for {}", target);
			}
			location.paint(g, this.getHeight(), (float) this.iconScale, xScale,
					yScale);
		}
	}

	private AlphaComposite makeComposite(float alpha) {
		int type = AlphaComposite.SRC_OVER;
		return (AlphaComposite.getInstance(type, alpha));
	}

	void clearPassiveMotionResults() {
		this.rectangleLocations.clear();
		log.debug("Clearing passive motion locations.");
		this.redraw();
	}

	protected void redraw() {
		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				repaint();
			}
		});
	}

	public Dimension getRegionDimensions() {
		return regionDimensions;
	}

	public void setRegionDimensions(Dimension regionDimensions) {
		this.regionDimensions = regionDimensions;
	}

	public void setRegionImageUri(String regionImageUri) {
		this.regionImageUri = regionImageUri;

		if (this.regionImageUri.indexOf("http://") == -1) {
			this.regionImageUri = "http://" + this.regionImageUri;
		}

		try {
			this.regionImage = ImageIO.read(new URL(this.regionImageUri));
		} catch (MalformedURLException e) {
			log.warn("Invalid region URI: {}", this.regionImageUri);
			e.printStackTrace();
		} catch (IOException e) {
			log.warn("Could not load region URI at {}.", this.regionImageUri);
			e.printStackTrace();
		}

	}

	public void addRegion(String region) {
		this.regionUris.add(region);
	}

	public void removeRegion(String region) {
		this.regionUris.remove(region);
	}

	public double getIconScale() {
		return iconScale;
	}

	public void setIconScale(double iconScale) {
		this.iconScale = iconScale;
		this.redraw();
	}

	public boolean isShowEllipse() {
		return showEllipse;
	}

	public void setShowEllipse(boolean showEllipse) {
		this.showEllipse = showEllipse;
		this.redraw();
	}

	public boolean isShowUnknownDevices() {
		return showUnknownDevices;
	}

	public void setShowUnknownDevices(boolean showUnknownDevices) {
		this.showUnknownDevices = showUnknownDevices;
		this.redraw();
	}

	public boolean isShowReceivers() {
		return showReceivers;
	}

	public void setShowReceivers(boolean showReceivers) {
		this.showReceivers = showReceivers;
		this.redraw();
	}

	public boolean isShowFiduciaryTransmitters() {
		return showFiduciaryTransmitters;
	}

	public void setShowFiduciaryTransmitters(boolean showFiduciaryTransmitters) {
		this.showFiduciaryTransmitters = showFiduciaryTransmitters;
		this.redraw();
	}

	public boolean isShowMugs() {
		return showMugs;
	}

	public void setShowMugs(boolean showMugs) {
		this.showMugs = showMugs;
		this.redraw();
	}

	public boolean isShowDoors() {
		return showDoors;
	}

	public void setShowDoors(boolean showDoors) {
		this.showDoors = showDoors;
		this.redraw();
	}

	public boolean isShowOthers() {
		return showOthers;
	}

	public void setShowOthers(boolean showOthers) {
		this.showOthers = showOthers;
		this.redraw();
	}

	@Override
	public void connectionInterrupted(ClientWorldModelInterface worldModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connectionEnded(ClientWorldModelInterface worldModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connectionEstablished(ClientWorldModelInterface worldModel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void requestCompleted(ClientWorldModelInterface worldModel,
			AbstractRequestMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dataResponseReceived(ClientWorldModelInterface worldModel,
			DataResponseMessage message) {
		log.debug("Received {}", message);
		Attribute[] solutionArray = message.getAttributes();
		if (solutionArray == null || solutionArray.length == 0) {
			log.debug("No solutions to add.");
		}

		boolean redrawScreen = false;
		boolean passiveMotionAdded = false;

		for (Attribute solution : message.getAttributes()) {
			if ("location".equals(solution.getAttributeName())) {
				if (solution.getData() != null
						&& solution.getData().length == 32) {
					String targetUri = this.mugMap.get(message.getUri());
					// This is a mug
					if (targetUri != null) {
						ImageLocation location = this.mugLocations
								.get(targetUri);
						if (location == null) {
							location = new ImageLocation();
							location.setUri(targetUri);
							location.setImage(this.mugImage);
							this.mugLocations.put(message.getUri(), location);
						} else {
							location.setUri(message.getUri());
						}
						ByteBuffer locBuff = ByteBuffer
								.wrap(solution.getData());
						location.setX(locBuff.getDouble());
						location.setY(locBuff.getDouble());
					} else {

						// General ellipse locations
						EllipseLocation location = new EllipseLocation();
						location.setImage(this.transmitterImage);
						location.setUri(message.getUri());
						ByteBuffer locBuff = ByteBuffer
								.wrap(solution.getData());
						location.setX(locBuff.getDouble());
						location.setY(locBuff.getDouble());
						location.setWidth(locBuff.getDouble());
						location.setHeight(locBuff.getDouble());

						this.ellipseLocations.put(message.getUri(), location);
					}
					redrawScreen = true;
				}
			} else if ("passive motion.tile"
					.equals(solution.getAttributeName())) {
				this.rectangleLocations.clear();
				passiveMotionAdded = true;
				redrawScreen = true;
				if (solution.getData() != null
						&& solution.getData().length >= 20) {
					ByteBuffer buff = ByteBuffer.wrap(solution.getData());
					while (buff.remaining() >= 20) {
						RectangleLocation location = new RectangleLocation();
						location.setX(buff.getFloat());
						location.setY(buff.getFloat());
						location.setX2(buff.getFloat());
						location.setY2(buff.getFloat());
						location.setScore(buff.getFloat());
						this.rectangleLocations.add(location);
					}
				}
			}
			// Doors
			else if ("closed".equals(solution.getAttributeName())) {
				ImageLocation doorLocation = this.doorLocations.get(message
						.getUri());
				if (doorLocation != null) {
					if (solution.getData().length == 1) {
						redrawScreen = true;
						doorLocation
								.setImage(solution.getData()[0] == 0x01 ? this.doorClosedImage
										: this.doorOpenImage);
					}
				}
			}
			// Check projectors, microwave
			else if ("on".equals(solution.getAttributeName())) {
				ImageLocation projectorLocation = this.projectorLocations
						.get(message.getUri());
				if (projectorLocation != null) {
					if (solution.getData().length == 1) {
						redrawScreen = true;
						projectorLocation
								.setImage(solution.getData()[0] == 0x01 ? this.projectorOnImage
										: this.projectorOffImage);
					}
				}
			}

			else {
				log.info("Received {}", solution);
			}

		}

		if (message.getAttributes() == null) {
			return;
		}

		// Handle interesting regions
		if ("winlab".equals(message.getUri())) {
			Attribute[] fields = message.getAttributes();
			if (fields == null) {
				log.warn("No information about {} available.", "winlab");
				return;
			}

			Dimension regionDimensions = new Dimension();
			double width = 0.0;
			double height = 0.0;
			String mapURL = null;
			for (Attribute field : fields) {
				if ("width".equals(field.getAttributeName())) {
					width = ((Double) FieldDecoder.decodeField(
							"double", field.getData()))
							.doubleValue();
					continue;
				} else if ("height".equals(field.getAttributeName())) {
					height = ((Double) FieldDecoder.decodeField(
							"double", field.getData()))
							.doubleValue();
					continue;
				} else if ("map url".equals(field.getAttributeName())) {
					;
					mapURL = (String) FieldDecoder.decodeField(
							"string", field.getData());
					continue;
				}
			}
			regionDimensions.setSize(width, height);
			this.regionDimensions = regionDimensions;
			this.setRegionImageUri(mapURL);
			redrawScreen = true;
		}
		// Found a mug, keep track of its ID
		else if (message.getUri().indexOf("mug") != -1) {
			String idString = null;
			String uri = message.getUri();
			for (Attribute field : message.getAttributes()) {
				if ("id".equals(field.getAttributeName())) {
					Long id = (Long) FieldDecoder.decodeField(
							"long", field.getData());
					idString = String.valueOf(id.longValue());
				}
			}

			if (idString != null) {
				this.mugMap.put(idString, uri);
				redrawScreen = true;
			}

			ImageLocation mugLocation = this.ellipseLocations.get(idString);
			if (mugLocation != null) {
				this.ellipseLocations.remove(idString);
				mugLocation.setImage(this.mugImage);
				mugLocation.setUri(uri);
				this.mugLocations.put(idString, mugLocation);
				redrawScreen = true;
			}

		}

		// Static objects
		else {
			BufferedImage image = null;
			if (message.getUri().indexOf("winlab.coffee pots") != -1)
				image = this.coffeePotImage;
			else if (message.getUri().indexOf("winlab.doors") != -1)
				image = this.doorClosedImage;
			else if (message.getUri().indexOf("winlab.microwaves") != -1)
				image = this.microwaveOffImage;
			else if (message.getUri().indexOf("transmitter") != -1)
				image = this.fiduciaryTransmitterImage;
			else if (message.getUri().indexOf("receiver") != -1)
				image = this.receiverImage;
			else if (message.getUri().indexOf("projector") != -1) {
				image = this.projectorOffImage;
			}

			if (image == null) {
				return;
			}
			if (message.getAttributes() == null) {
				return;
			}

			ImageLocation location = new ImageLocation();
			location.setImage(image);
			location.setUri(message.getUri());

			for (Attribute field : message.getAttributes()) {
				if ("location.x".equals(field.getAttributeName())) {
					location.setX(((Double) FieldDecoder.decodeField(
							"double", field.getData()))
							.doubleValue());
				} else if ("location.y".equals(field.getAttributeName())) {
					location.setY(((Double) (FieldDecoder.decodeField(
							"double", field.getData())))
							.doubleValue());
				}
			}
			if (location.getX() > 0 && location.getY() > 0) {
				if (message.getUri().indexOf("winlab.coffee pots") != -1) {
					this.coffeePotLocations.put(message.getUri(),
							location);
				} else if (message.getUri()
						.indexOf("winlab.doors") != -1) {
					this.doorLocations.put(message.getUri(),
							location);
				} else if (message.getUri().indexOf(
						"winlab.microwaves") != -1) {
					this.microwaveLocations.put(message.getUri(),
							location);
				} else if (message.getUri().indexOf("transmitter") != -1) {
					this.fiduciaryTransmitterLocations.put(
							message.getUri(), location);
				} else if (message.getUri().indexOf("receiver") != -1) {
					this.receiverLocations.put(message.getUri(),
							location);
				} else if (message.getUri().indexOf("projector") != -1) {
					this.projectorLocations.put(message.getUri(),
							location);
				}

				redrawScreen = true;
			}
		}

		if (redrawScreen) {
			if (passiveMotionAdded) {
				if (this.motionClearTask != null) {
					this.motionClearTask.cancel();
				}
				this.motionClearTask = new TimerTask() {
					public void run() {
						clearPassiveMotionResults();
					}
				};
				this.motionClearTimer.schedule(this.motionClearTask,
						this.motionClearDelay);

			}
			this.redraw();
			return;
		}
	}

	@Override
	public void uriSearchResponseReceived(ClientWorldModelInterface worldModel,
			URISearchResponseMessage message) {
		String[] uris = message.getMatchingUris();
		if (uris == null || uris.length == 0) {
			log.warn("No search results returned.");
			return;
		}

		for (String uri : uris) {
			boolean sentRequest = false;
			for (String regionUri : this.regionUris) {
				if (regionUri.equals(uri)) {
					StreamRequestMessage request = new StreamRequestMessage();
					request.setBeginTimestamp(System.currentTimeMillis());
					request.setUpdateInterval(0l);
					request.setQueryAttributes(new String[] { ".*" });
					request.setQueryURI(uri);
					this.distributorInterface.sendMessage(request);
					sentRequest = true;
					break;
				}
			}
			if (!sentRequest) {
				for (String interest : INTERESTING_URI) {
					if (uri.indexOf(interest) != -1) {
						StreamRequestMessage request = new StreamRequestMessage();
						request.setBeginTimestamp(System.currentTimeMillis());
						request.setUpdateInterval(0l);
						request.setQueryAttributes(new String[] { ".*" });
						request.setQueryURI(uri);
						this.distributorInterface.sendMessage(request);
						sentRequest = true;
						break;
					}
				}
			}

			if (!sentRequest) {
				log.info("Discarding {}", uri);
			}
		}
	}

	@Override
	public void attributeAliasesReceived(
			ClientWorldModelInterface clientWorldModelInterface,
			AttributeAliasMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void originAliasesReceived(
			ClientWorldModelInterface clientWorldModelInterface,
			OriginAliasMessage message) {
		// TODO Auto-generated method stub

	}

	@Override
	public void originPreferenceSent(ClientWorldModelInterface worldModel,
			OriginPreferenceMessage message) {
		// TODO Auto-generated method stub
		
	}
}
