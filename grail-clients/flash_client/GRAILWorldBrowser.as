import flash.display.Sprite;
import flash.events.*;
import flash.text.TextFormat;
import flash.text.TextField;
import flash.text.TextFieldType;
import flash.display.DisplayObject;
import flash.display.SimpleButton;
import flash.utils.Timer;
import flash.utils.ByteArray;

import flashgrail.WorldServerProtocol;
import flashgrail.WorldServerClient;
import flashgrail.WorldData;
import flashgrail.FieldData;

import mx.collections.ArrayList;

//For dataGridEvent
import mx.events.*;

private var clock:Timer = new Timer(1000);

private var world:WorldServerClient;

//Row where user inputs new field information
private var lastrow:Row;

private var last_connected:Boolean = false;

[Bindable] public var uris:ArrayList = new ArrayList();
[Bindable] public var fields:ArrayList = new ArrayList();

private function sendNewFields(event:MouseEvent):void {
  //Update the changed fields of this URI
  var wd:WorldData = new WorldData;
  wd.object_uri = cur_uri.text;
  for (var i:uint = 0; i < fields.length; ++i) {
    if (fields.getItemAt(i)["changed"] != null &&
        fields.getItemAt(i)["changed"] == true) {
      var obj:Object = fields.getItemAt(i);
      var fd:FieldData = new FieldData();
      fd.description = obj["Description"];
      fd.creation_date = uint(obj["Created"]);
      fd.expiration_date = uint(obj["Expires"]);
      fd.data_type = obj["Data Type"];
      var datatext:String = obj["Data"];
      if (fd.data_type == "string") {
        fd.data = WorldServerProtocol.makeUnsizedUTF16(datatext);
      }
      else if (fd.data_type == "double") {
        fd.data.writeDouble(Number(datatext));
      }
      else if (fd.data_type == "uint32_t") {
        fd.data.writeUnsignedInt(uint(datatext));
      }
      else if (fd.data_type == "int32_t") {
        fd.data.writeInt(int(datatext));
      }
      else if (fd.data_type == "uint64_t") {
        fd.data.writeUnsignedInt(0);
        fd.data.writeUnsignedInt(uint(datatext));
      }
      else if (fd.data_type == "uint128_t") {
        fd.data.writeUnsignedInt(0);
        fd.data.writeUnsignedInt(0);
        fd.data.writeUnsignedInt(0);
        fd.data.writeUnsignedInt(uint(datatext));
      }
      wd.fields.push(fd);
    }
  }
  //Hide the submit fields button again
  change_fields.visible = false;
  change_fields.includeInLayout = false;
  world.send(WorldServerProtocol.makePushDataMsg(wd));
  cur_action.text = "Sending new URI field data to world server.";
}

private function sendNewURI(event:MouseEvent):void {
  //Send the URI and then go back to the control view.
  //The uri is in query_field.text
  var wd:WorldData = new WorldData;
  wd.object_uri = query_field.text;
  world.send(WorldServerProtocol.makePushDataMsg(wd));
  cur_action.text = "Sending new URI to world server.";
  submit.label = "Search";
  search_form_label.label = "Enter URI search:";
  submit.removeEventListener(MouseEvent.CLICK, sendNewURI);
  submit.addEventListener(MouseEvent.CLICK, submitURIQuery);
}

private function getNewURI(event:MouseEvent):void {
  submit.label = "Submit new URI";
  search_form_label.label = "Enter new URI:";
  submit.removeEventListener(MouseEvent.CLICK, submitURIQuery);
  submit.addEventListener(MouseEvent.CLICK, sendNewURI);
}

private function submitURIQuery(event:MouseEvent):void {
  cur_action.text = "Searching URI \""+query_field.text+"\"";
  world.send(WorldServerProtocol.makeURISearchMsg(query_field.text));
}

private function getURIData(uri:String):void {
  cur_action.text = "Getting data for URI \""+uri+"\"";
  world.send(WorldServerProtocol.makeObjectDataQueryMsg(uri));
}

private function clearStartView():void {
  uilist.removeElement(connect_form);
}

private function setupControlView():void {
  //Make everything visible
  for (var el:uint = 0; el < uilist.numElements; ++el) {
    uilist.getElementAt(el).visible = true;
    uilist.getElementAt(el).includeInLayout = true;
  }
  //Only show these when results are available
  uri_grid.visible = false;
  uri_grid.includeInLayout = false;
  field_grid.visible = false;
  field_grid.includeInLayout = false;
  cur_uri.visible = false;
  cur_uri.includeInLayout = false;
  change_fields.visible = false;
  change_fields.includeInLayout = false;
}

private function updateStats(event:TimerEvent):void {
  if (world.connected) {
    stat.text = "Status: connected";
    if ( false == last_connected) {
      clearStartView();
      setupControlView();
      last_connected = true;
    }
  }
  else {
    last_connected = false;
    stat.text = "Status: disconnected";
  }
}

public function statCallback(str:String):void {
  cur_action.text = str;
}

public function findURI(evt:DataGridEvent):void {
  //Don't actually allow this cell to be edited.
  evt.preventDefault();
  getURIData(uris.getItemAt(evt.rowIndex)["World Server URI"] as String);
}

public function editFieldData(evt:DataGridEvent):void {
  //Edit this field
  var fieldname:String = evt.dataField;

  //If this is the data type field only allow editing through a dropdown box.
  if (fieldname == "Data Type") {
    statCallback("Trying to edit the data type field");
    //evt.preventDefault();
  }
  //Add a new field if this is the add field button
  else if (fields.getItemAt(evt.rowIndex)["addfield"] != null) {
    //Don't edit this field
    evt.preventDefault();
    //Add a new field before this one
    var field_obj:Object = {"Description":"description",
                            "Created":String(new Date().getTime()),
                            "Expires":"0",
                            "Data Type":"string",
                            "Data":"data",
                            "changed":false};
    fields.addItemAt(field_obj, fields.length - 1);
  }
}

public function endEditField(evt:DataGridEvent):void {
  //Make sure this is not the addfield button
  if (fields.getItemAt(evt.rowIndex)["addfield"] == null) {
    //Edit this field
    var fieldname:String = evt.dataField;
    var field_val:String;
    var new_val:String;
    if ("Data Type" != fieldname) {
      field_val = fields.getItemAt(evt.rowIndex)[evt.dataField] as String;
      new_val = evt.currentTarget.itemEditorInstance.text;
    }
    else {
      field_val = fields.getItemAt(evt.rowIndex)[evt.dataField] as String;
      new_val = evt.currentTarget.itemEditorInstance.text;
    }
    if (field_val != new_val) {
      fields.getItemAt(evt.rowIndex)["changed"] = true;
      //Make the change fields button visible
      change_fields.visible = true;
      change_fields.includeInLayout = true;
    }
  }
}

/*
 * This is called when a search response returns from the world server.
 * This function adds the uris to a table.
 */
private function addURI(uri:String, idx:int, vect:Vector.<String>):void {
  var u:Object = {"World Server URI":uri};
  uris.addItem(u);
  //TODO Add a back button.
}

/*
 * Convenience function to make a text input field.
 */
private function makeTInput(str:String):TextField {
  var tfield:TextField = new TextField();
  tfield.text = str;
  tfield.autoSize = "left";
  tfield.border   = true;
  tfield.type     = TextFieldType.INPUT;
  return tfield;
}

/*
 * Convenience function to make a text field.
 */
private function makeTField(str:String):TextField {
  var tfield:TextField = new TextField();
  tfield.text = str;
  tfield.autoSize = "left";
  tfield.border   = true;
  tfield.type     = TextFieldType.DYNAMIC;
  tfield.height   = tfield.textHeight;
  tfield.width    = tfield.textWidth;
  return tfield;
}

/*
 * Convenience function to make a button with a text label
 */
private function makeTButton(str:String):SimpleButton {
  var tfield:TextField = makeTField(str);
  return new SimpleButton(tfield, tfield, tfield, tfield);
}

private function UTF16toString(bytes:ByteArray):String {
  //TODO converting UTF16 to UTF8 but actionscript should support this
  var str:String = new String();
  var strlen:uint = bytes.length;
  for (var j:uint = 0; j < strlen; ++j) {
    var s:String = bytes.readUTFBytes(1);
    if (j % 2 == 1) {
      str = str + s;
    }
  }
  return str;
}

/*
 * Display the field data for a queried URI.
 */
public function queryCallback(wd:WorldData):void {
  statCallback("Showing query results");
  //Clear the current uri list contents
  fields.removeAll();
  //Display the field grid and hide the URI grid
  uri_grid.visible = false;
  uri_grid.includeInLayout = false;
  field_grid.visible = true;
  field_grid.includeInLayout = true;
  cur_uri.visible = true;
  cur_uri.includeInLayout = true;

  //Label the URI where this fields originated.
  cur_uri.text = wd.object_uri;

  //Make rows for the data
  for (var J:uint = 0; J < wd.fields.length; ++J) {
    var desc:String = wd.fields[J].description;
    var create:String = String(wd.fields[J].creation_date);
    var expire:String = String(wd.fields[J].expiration_date);
    var d_type:String = wd.fields[J].data_type;
    //If the data type is known convert it to a string.
    var rawdata:String = new String();
    if (d_type == "string") {
      rawdata = UTF16toString(wd.fields[J].data);
    }
    else if (d_type == "double") {
      rawdata = (wd.fields[J].data.readDouble()).toString();
    }
    else if (d_type == "uint32_t") {
      rawdata = (wd.fields[J].data.readUnsignedInt()).toString();
    }
    else if (d_type == "int32_t") {
      rawdata = (wd.fields[J].data.readInt()).toString();
    }
    else if (d_type == "uint64_t") {
      //Discard the upper 32 bits
      wd.fields[J].data.readUnsignedInt();
      rawdata = (wd.fields[J].data.readUnsignedInt()).toString();
    }
    else if (d_type == "uint128_t") {
      //Discard the uper 96 bits
      wd.fields[J].data.readUnsignedInt();
      wd.fields[J].data.readUnsignedInt();
      wd.fields[J].data.readUnsignedInt();
      rawdata = wd.fields[J].data.readUnsignedInt().toString()
    }
    else {
      //Otherwise convert the raw data into hexedecimal.  Do this one byte at a
      //time so that it is evenly padded (uint.toString does not do this).
      //Stop after 50 bytes.
      //var rawdata:TextField = makeTField(String(wd.fields[J].data));
      var hex_str:String = "0x";
      for (var b:uint = 0; b < wd.fields[J].data.length && b < 50; ++b) {
        var bstring:String = (wd.fields[J].data.readUnsignedByte().toString(16));
        //uint.toSTring does not pad so add an extr 0 if it is needed.
        if (bstring.length < 2) { bstring = "0" + bstring;}
        hex_str += bstring;
      }
      rawdata = hex_str;
    }
    
    //Set the selected type to the actual type
    var field_obj:Object = {"Description":desc,
                            "Created":create,
                            "Expires":expire,
                            "Data Type":d_type,
                            "Data":rawdata,
                            "changed":false};
    fields.addItem(field_obj);
  }
  //Add one more field to allow the user to add new fields to this URI.
  var addField:Object = {"Description":"Create a new field",
                         "addfield":true};
  fields.addItem(addField);
}

/*
 * This function adds each type in the given vector to a table.
 */
public function uriSearchCallback(found_uris:Vector.<String>):void {
  //Clear the current uri list contents
  uris.removeAll();
  //Display the URI grid and hide the field grid
  uri_grid.visible = true;
  uri_grid.includeInLayout = true;
  field_grid.visible = false;
  field_grid.includeInLayout = false;
  cur_uri.visible = false;
  cur_uri.includeInLayout = false;

  found_uris.forEach(addURI);
}

public function typeSelected(e:Event):void {
  //cur_action.text = cb.selectedItem.label;
}

/*
 * Connect to the GRAIL3 world server when the button is clicked.
 */
public function grailWSConnect():void {
  world = new WorldServerClient(ipfield.text, int(portfield.text), queryCallback, uriSearchCallback, statCallback);
  //Event that responds to the timer that fires once a second.
  clock.addEventListener(TimerEvent.TIMER, updateStats);
  clock.start();
}
