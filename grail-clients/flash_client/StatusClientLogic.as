import flash.display.DisplayObject;
import flash.utils.Timer;
import flash.utils.Dictionary;

//GRAIL3 Distributor stuff
import flashgrail.Grail3Client;
import flashgrail.GRAIL3Type;
import flashgrail.GRAILSolution;
import flashgrail.SolverData;
import flashgrail.GRAILSolutionRequest;
import flashgrail.ClientAggregator;

//GRAIL3 World Server stuff
import flashgrail.WorldServerProtocol;
import flashgrail.WorldServerClient;
import flashgrail.WorldData;
import flashgrail.FieldData;

//Stuff to display transmitters
import flashgrail.Location;
import flashgrail.TileLocation;
import flashgrail.EllipseLocation;
import flashgrail.AntennaStatus;

//For the drop down box
import mx.collections.ArrayList;
import spark.events.IndexChangeEvent;

//For the check box area
import spark.components.CheckBox;

//For events
import spark.components.Label;
import spark.components.RichText;

//Map of the area
//[Embed("winlab.png")] public var MapImage:Class;
[Embed("cleanlab.jpg")] public var MapImage:Class;
public var floormap:DisplayObject = new MapImage();

//Icons to show on the map
[Embed("antenna.png")] public var AntennaImage:Class;
[Embed("mug.png")] public var MugImage:Class;
[Embed("microwave.png")] public var MWaveImage:Class;
[Embed("microwaveon.png")] public var MWaveImageOn:Class;
[Embed("door.png")] public var Door:Class;
[Embed("door_open.png")] public var DoorOpen:Class;
[Embed("gluegun.png")] public var GlueGun:Class;
[Embed("projector.png")] public var Project:Class;
[Embed("projectoron.png")] public var ProjectOn:Class;
[Embed("coffeepot.png")] public var Coffeepot:Class;
[Embed("duct_tape.png")] public var Dtape:Class;
[Embed("packtape.png")] public var Ptape:Class;
[Embed("chair.png")] public var Chair:Class;
[Embed("chairempty.png")] public var ChairEmpty:Class;
[Embed("wins_temperature.png")] public var WinsTemp:Class;
[Embed("wins_hybrid.png")] public var WinsHybrid:Class;
public var images:Object = {"mugs":MugImage, "microwaves":MWaveImage,  "microwaveson":MWaveImageOn,
                            "doors":DoorOpen, "doorsclosed":Door, "hot glue gun":GlueGun,
                            "projectors":Project, "projectorson":ProjectOn, "coffee pots":Coffeepot,
                            "duct tape":Dtape, "packing tape":Ptape, "chairs":Chair, "chairsempty":ChairEmpty,
                            "temp":WinsTemp, "hybrid":WinsHybrid};

//The objects seen by the localization system
public var antennas:Dictionary = new Dictionary();
//Groups of transmitters - "mugs", "doors", etc - all stored as vectors of strings.
//public var groups:Dictionary = new Dictionary();
public var groups:Object = {"All":new Dictionary()};

//Whether or not status updates should be printed for different groups.
public var interested:Dictionary = new Dictionary();

//Locations of fixed items
public var fixed:Dictionary = new Dictionary();

//Mapping from object names to sensor IDs
public var idToName:Dictionary = new Dictionary();
public var nameToID:Dictionary = new Dictionary();

public const map_scale:Number = 1.88 * 3.63;

public var clock:Timer = new Timer(1000);
public var retry_timer:uint = 0;

public var gclient:Grail3Client;
public var world:WorldServerClient;

public var last_connected:Boolean = false;

public var location_type:String = "location";
public var location_alias:int = -1;
public var event_alias:int = -1;
public var coffee_alias:int = -1;
public var misc_aliases:Dictionary = new Dictionary();
public var status_aliases:Dictionary = new Dictionary();

//The time of the last data update, in seconds since January 1, 1970.
public var last_updated:Number = 0;
public var world_update:Number = 0;

public var coffee_brewtime:Date = null;
public var brew_graphic:AntennaStatus = null;

[Bindable] public var ddoptions:ArrayList = new ArrayList(["All"]);


public function statCallback(str:String):void {
  cur_action.text = "Distributor: "+str;
}

public function worldStatCallback(str:String):void {
  world_cur_action.text = "World: "+str;
}

//Called when a type is selected in the dropdown
public function filterDisplay(e:IndexChangeEvent):void {
  var filt_name:String = target_dropdown.selectedItem as String;
  //See if this is an individual antenna
  if (null != antennas[filt_name]) {
    //Remove all antennas and then display this one
    removeAntennas();
    maparea.addChild(antennas[filt_name]);
  }
  else if (null != groups[filt_name]) {
    var dict_filter:Dictionary = groups[filt_name];
    //Remove all antennas and then add all of the antennas from this group.
    removeAntennas();
    for each (var ant_stat:AntennaStatus in dict_filter) {
      maparea.addChild(ant_stat);
    }
  }
}

/*
 * Callback for the solution type message from the GRAIL3 distributor.
 * This function adds each available type in the given vector to a table.
 * Types of interest are requested from the distributor after seeing them
 * declared here.
 */
public function newTypesCallback(types:Vector.<GRAIL3Type>):void {
  //Request "tile location" data.
  var request:GRAILSolutionRequest = new GRAILSolutionRequest();
  //Check all of the types the distributor has available.
  for (var i:uint = 0; i < types.length; ++i) {
    //Check for location information of the desired type (ellipses, boxes, etc)
    if (types[i].name == location_type) {
      location_alias = types[i].alias;
      request.aliases.push(location_alias);
    }
    //Otherwise is not location data
    else {
      //Record this alias if it is of interest and should be printed
      //in a status event box.
      var sol_name:String = types[i].name;
      if ("at teatime" == sol_name ||
          "happening" == sol_name ||
          "occupied" == sol_name ||
          "coffee brewing" == sol_name ||
          "coffee ready" == sol_name) {
        //Record any additional information needed for this solution type
        if (sol_name == "coffee brewing") {
          coffee_alias = types[i].alias;
        }
        //Remember the names of these solution types
        status_aliases[types[i].alias] = types[i].name;
        request.aliases.push(types[i].alias);
        //Add this group to the check boxes in the solution select area.
        //This allows the user to select what they are interested in
        var gbox:CheckBox = new CheckBox();
        gbox.label = types[i].name;
        gbox.selected = true;
        //Start off interested in each group
        interested[gbox.label] = true;
        //gbox.click = groupSelect;
        gbox.addEventListener(MouseEvent.CLICK, groupSelect);
        solution_select_area.addElement(gbox);
      } else {
        //Check for other event types that require special handling.
        //These are all swich events.
        //Door open/closed status
        if ("closed" == sol_name ||
            "on" == sol_name ||
            "empty" == sol_name) {
          status_aliases[types[i].alias] = types[i].name;
          request.aliases.push(types[i].alias);
        } else {
          //Calendar events
          if ("event" == sol_name) {
            event_alias = types[i].alias;
            request.aliases.push(types[i].alias);
          }
          //Otherwise this is a type of data without a specific method for displaying it.
          else {
            misc_aliases[types[i].alias] = types[i].name;
            request.aliases.push(types[i].alias);
          }
        }
      }
    }
  }
  //If location data is available and we have solutions to request
  //then make a solution request.
  if (location_alias != -1 && request.aliases.length > 0) {
    statCallback("Sending data request to server (waiting for data).");
    //Request everything in the past minute plus current updates
    request.begin = (new Date()).getTime() - (60000);
    gclient.send(ClientAggregator.makeSolutionRequest(request));
    //Send another message to search for coffee brewing events
    if (coffee_alias != -1) {
      var cof_request:GRAILSolutionRequest = new GRAILSolutionRequest();
      cof_request.aliases.push(coffee_alias);
      //Request the last three hours of coffee data (minus the minute
      //requested above)
      cof_request.begin = (new Date()).getTime() - (1000 * 60 * 60 * 3);
      cof_request.end = (new Date()).getTime() - (1000 * 60);
      gclient.send(ClientAggregator.makeSolutionRequest(cof_request));
    }
  }
  else {
    statCallback("Location data is not available.");
  }
}

//Remove all of the viewable antennas
private function removeAntennas():void {
  var idx:uint = 0;
  while (idx < maparea.numChildren) {
    var ant:AntennaStatus = maparea.getChildAt(idx) as AntennaStatus;
    if (ant != null) {
      maparea.removeChildAt(idx);
    }
    else {
      ++idx;
    }
  }
}

/*
 * Append the given message to the event status log.
 */
public function logEvent(str:String):void {
  status_log.appendText("\n"+str);
}

/***
 * Draw the given item at the given location.
 * The filterDisplay function should be called after calls to this function to
 * render any new items.
 ***/
public function drawAtLocation(loc:Location, item_name:String):void {
  var aimg:DisplayObject = new AntennaImage();
  var astat:AntennaStatus = new AntennaStatus(aimg, loc, item_name);
  if (null == antennas[item_name]) {
    //statCallback("Adding location of "+item_name);
    //Push the new target into the dropdown
    ddoptions.addItem(item_name);
  }
  else {
    //statCallback("Updating location of "+item_name);
    //Remove the previous image before we lose the reference to it.
    if (maparea.contains(antennas[item_name])) {
      maparea.removeChild(antennas[item_name]);
    }
    //Copy over the previous details
    astat.setDetails(antennas[item_name].getDetails());
    astat.image = antennas[item_name].image;
  }
  antennas[item_name] = astat;
  //Images are a little big - shrink them by half
  astat.imgScale = 0.5 * floormap.scaleX;
  //statCallback("Setting location of "+item_name);
  //Scale and set location according to the map image scale
  astat.x = floormap.x + floormap.scaleX * map_scale * astat.location.x;
  astat.y = floormap.y + floormap.height - floormap.scaleY * map_scale * astat.location.y;
  //Update the "All" group.
  groups["All"][item_name] = astat;
  //Check to see if this target is in any other known groups and update them
  if (null != idToName[item_name]) {
    var full_name:String = idToName[item_name];
    var group_name:String = full_name.split(".")[0];
    if (null != groups[group_name]) {
      //Change the image to the group's image if there is one
      if (null != images[group_name] &&
          null == groups[group_name][full_name]) {
        astat.image = new images[group_name];
      }
      astat.addDetail("Name:", full_name);
      //Update the image for the group dropdown
      groups[group_name][full_name] = astat;
    }
  }
}

/*
 * This function updates the locations of transmitters as location data arrives.
 * If other kinds of information arrive they are displayed in the status box.
 * Other specialized information is handled in solution specific ways.
 * For instance open/close solutions might make door graphics change.
 */
public function newSolutionsCallback(solutions:SolverData):void {
  for (var i:uint = 0; i < solutions.solutions.length; ++i) {
    var soln:GRAILSolution = solutions.solutions[i];
    //See if the user expressed interest in this group.
    var group_pieces:Array = soln.target.split(".");
    //First part is region, last part is name, everything in between is the group
    var gname:String = group_pieces[1];
    for (var name_part:uint = 2; name_part < group_pieces.length - 1; ++name_part) {
      gname += "." + group_pieces[name_part];
    }
    //Draw the item if this is a location solution and this is not a fixed item.
    if (location_alias == soln.type_alias &&
        null == fixed[soln.target]) {
      statCallback("Receiving location data.");
      last_updated = (new Date()).getTime();
      statCallback("Processing location of "+soln.target);
      //If this antenna is not currently being displayed then
      var eloc:Location;
      if (location_type == "location") {
        statCallback("Making new ellipse for "+soln.target);
        eloc = new EllipseLocation(soln.data);
      }
      else if (location_type == "tile_location") {
        statCallback("Making new tile for "+soln.target);
        eloc = new TileLocation(soln.data);
      }
      else {
        statCallback("Unrecognized location type");
        return;
      }
      drawAtLocation(eloc, soln.target);
      statCallback("New location results displayed.");
    }
    else if (event_alias == soln.type_alias) {
      statCallback("Receiving event data.");
      //Pull the event data into the string.
      var num_items:uint = soln.data.readUnsignedInt();
      var event_text:String = new String();
      for (var item:uint = 0; item < num_items; ++item) {
        //Get the label and description and add them to the label text
        var lab:String = ClientAggregator.readSizedUTF16(soln.data);
        var desc:String = ClientAggregator.readSizedUTF16(soln.data);
        //FIXME Workaround for messed up in the description fields when the email script reads too long
        desc = desc.replace(/=/g, "");
        var end_idx:int = desc.search("--_[_0-9A-F]*ms2winlab");
        if (-1 != end_idx) {
          desc = desc.substr(0, end_idx-1);
        }

        //TODO These are workarounds for previously poorly processed email that had html in them.
        if ("" != lab && "BSTRACT" != lab) {
          event_text += lab + ":\t" + desc + "\n";
          //Search for other events with the same title and remove them.
          if (lab == "TITLE") {
            for (var other_el:int = 0; other_el < upcoming_area.numElements; ++other_el) {
              //Check for a title the same as this one
              var oth:RichText = upcoming_area.getElementAt(other_el) as RichText;
              if (null != oth &&
                  -1 != oth.text.search(desc)) {
                //Remove duplicate element
                upcoming_area.removeElementAt(other_el);
                --other_el;
              }
            }
          }
        }
      }
      //Change the upcoming label's text to indicate that there are some events
      upcoming_label.text = "Upcoming calendar events:";
      var event_label:RichText = new RichText();
      event_label.text = event_text + "\n\n";
      upcoming_area.addElement(event_label);
      //Set the width to match the container's width
      event_label.percentWidth = 100;
    }
    //Non-location, non-event data. Check the map of printable
    //events of interest to see if this solution is one of them.
    else if (null != status_aliases[soln.type_alias]) {
      //Show this event if we know its name.
      if (null != status_aliases[soln.type_alias]) {
        var sol_name:String = status_aliases[soln.type_alias];
        //Push this new solution to the status log.
        var sol_time:Date = new Date();
        sol_time.setTime(solutions.time);
        var update_text:String = sol_time.toLocaleString() + ": " + soln.target;
        var img_string:String = gname;
        if (sol_name == "coffee brewing") {
          update_text += " has ";
          coffee_brewtime = sol_time;
        }
        else {
          if (0 == soln.data.readByte()) {
            update_text += " is not ";
          }
          else {
            update_text += " is ";
            img_string += sol_name;
          }
        }
        update_text += sol_name;
        //Only log events the user is interested in.
        if (interested[status_aliases[soln.type_alias]] as Boolean) {
          logEvent(update_text);
        }
        //TODO FIXME should very old text be removed?
        //Update the image to reflect its current status
        var item_id:String = nameToID[gname + "." + group_pieces[group_pieces.length-1]];
        if (item_id != null &&
            antennas[item_id] != null &&
            images[img_string] != null) {
          antennas[item_id].image = new images[img_string];
        }
      }
      /*
      else {
        logEvent("Ignoring event from group " + gname);
      }
      */
    }
    //Otherwise see if this is known miscellaneous data
    else if (null != misc_aliases[soln.type_alias]) {
      var sol_name:String = misc_aliases[soln.type_alias];
      var item_id:String = nameToID[soln.target];
      //Some solutions only point to a transmitter id
      if (group_pieces.length == 1) {
        item_id = soln.target;
      }
      //Add this detail to the object if the object is on the map
      if ( item_id != null &&
           antennas[item_id] != null) {
        var solver_data:String = "0x";
        while (soln.data.bytesAvailable > 0) {
          var new_char:uint = soln.data.readUnsignedByte();
          //Convert this number to a hex string
          var char_str:String = new_char.toString(16);
          //Make sure that each hex value consumes two characters.
          if (char_str.length == 1) {
            char_str = "0" + char_str;
          }
          solver_data += char_str;
        }
        antennas[item_id].addDetail(sol_name, solver_data);
      }
    }
    /*
    else {
      logEvent("Ignoring solution with alias " + soln.type_alias);
    }
    */
  }
  //Call filter display to make sure we are displaying the correct antennas
  filterDisplay(null);
}

public function queryCallback(wd:WorldData):void {
  //Fill in idToName and nameToID information
  var pieces:Array = wd.object_uri.split(".");
  if (pieces.length == 3) {
    var tx_group:String = pieces[1];
    /*
    if (pieces[0] == "wins" ||
        pieces[0] == "pipsqueak") {
      tx_group = pieces[0] + "." + tx_group;
    }
    */
    var tx_id:String = pieces[2];
    var obj_name:String = tx_group + "." + tx_id;
    var x_location:Number = -1;
    var y_location:Number = -1;
    var numericIDs:Vector.<String> = new Vector.<String>();
    for each (var field:FieldData in wd.fields) {
      //Check if this is a transmitter ID
      //TODO should be a better of doing this than comparing the data type
      if (field.data_type == "uint64_t") {
        //Discard the upper 32 bits
        field.data.readUnsignedInt();
        var numericID:String = field.data.readUnsignedInt().toString();
        //Fill in idToName and nameToID information.
        idToName[numericID] = obj_name;
        nameToID[obj_name] = numericID;
        //Add this if we need to
        if (null != groups[tx_group] &&
            null == groups[tx_group][obj_name] &&
            null != antennas[numericID]) {
          //Add this to the group list so dropdown group selection works.
          groups[tx_group][obj_name] = antennas[numericID];
          antennas[numericID].addDetail("Name:", obj_name);
          //Change the image to the group's image if there is one
          if ( null != images[tx_group]) {
            antennas[numericID].image = new images[tx_group];
          }
        }
        numericIDs.push(numericID);
      }
      else if ("location.x" == field.description) {
        x_location = field.data.readDouble();
      }
      else if ("location.y" == field.description) {
        y_location = field.data.readDouble();
      }
    }
    //See if this item has a location
    if (0 != numericIDs.length &&
        -1 != x_location &&
        -1 != y_location) {
      var loc:Location = new Location();
      loc.x = x_location;
      loc.y = y_location;
      for each (var nID:String in numericIDs) {
        fixed[nID] = loc;
      }
      drawAtLocation(loc, numericIDs[0]);
      filterDisplay(null);
    }
  }
  worldStatCallback("Static items loaded.");
}

public function groupSelect(evt:MouseEvent):void {
  var cbox:CheckBox = evt.target as CheckBox;
  if (cbox.selected) {
    status_log.appendText("\nNow printing events for objects from group " + cbox.label);
    interested[cbox.label] = true;
  }
  else {
    status_log.appendText("\nNo longer printing events for objects from group " + cbox.label);
    interested[cbox.label] = false;
  }
}

public function uriSearchCallback(uris:Vector.<String>):void {
  //See if this group class exists in the dropdown.
  worldStatCallback("Updating transmitter groups.");
  var add_index:int = 1;
  for each (var uri:String in uris) {
    var pieces:Array = uri.split(".");
    if (pieces.length == 3) {
      var tx_group:String = pieces[1];
      /*
      if (pieces[0] == "wins" ||
          pieces[0] == "pipsqueak") {
        tx_group = pieces[0] + "." + tx_group;
      }
      */
      var tx_id:String = pieces[2];
      var obj_name:String = tx_group + "." + tx_id;
      //See if we need to add this group.
      if (null == groups[tx_group]) {
        groups[tx_group] = new Dictionary();
        ddoptions.addItemAt(tx_group, add_index);
        ++add_index;
      }
      //If this id is not in the group list but it is a valid
      //antenna id add it in
      //(This won't work if this is a name (ie, winlab.mugs.Ben) rather than an id
      if (null == groups[tx_group][obj_name] &&
          null != antennas[tx_id]) {
        //Add this to the group list so dropdown group selection works.
        groups[tx_group][obj_name] = antennas[tx_id];
        antennas[tx_id].addDetail("Name:", obj_name);
        //Fill in idToName and nameToID information.
        idToName[tx_id] = obj_name;
        nameToID[obj_name] = tx_id;
        //Change the image to the group's image if there is one
        if ( null != images[tx_group]) {
          antennas[tx_id].image = new images[tx_group];
        }
      }

      //If this ID could not be matched to an antenna because it lacks
      //a name to antenna lookup then look one up.
      if (null == groups[tx_group][tx_id] &&
          null == nameToID[obj_name]) {
        world.send(WorldServerProtocol.makeObjectDataQueryMsg(uri));
      }
    }
  }
  //Call filter display to make sure we are displaying the correct images for items
  filterDisplay(null);
}

public function connect():void {
  if (null == gclient ||
      false == gclient.connected) {
    gclient = new Grail3Client(ipfield.text, int(portfield.text), newTypesCallback, newSolutionsCallback, statCallback);
  }
  if (null == world ||
      false == world.connected) {
    world = new WorldServerClient(ws_ipfield.text, int(ws_portfield.text), queryCallback, uriSearchCallback, worldStatCallback);
  }
  clock.start();
  //Event that responds to the timer that fires once a second.
  clock.addEventListener(TimerEvent.TIMER, updateStats);
  stat.text = "Status: connecting.";
  //Set up the dropdown list.
  target_dropdown.selectedItem = ddoptions[0];
}

//Draw a pie slice that is frac fraction of a circle with the given radius.
private function makeSlice(frac:Number, radius:Number):Sprite {
  var cir:Sprite = new Sprite();
  //Have a resolution of 1%
  var resolution:Number = 2 * Math.PI / 100;
  var ref_radian:Number = (Math.PI / 2);
  var full:Number = 2 * Math.PI;
  //Use curveTo to draw an arc
  cir.graphics.beginFill(0x00AA33);
  //Draw the outline of the circle.
  cir.graphics.drawCircle(0, 0, radius+2);
  cir.graphics.endFill();
  return cir;
}

private function updateStats(event:TimerEvent):void {
  //Reconnect when the timer runs down
  if (retry_timer > 0) {
    --retry_timer;
    if (retry_timer == 0 && gclient.connected == false) {
      stat.text = "Reconnecting...";
      gclient.reconnect();
    }
  }

  if (gclient.connected) {
    stat.text = "Status: connected";
    if ( false == last_connected) {
      //Remove the connect form and add status fields and the map.
      //connect_form.visible = false;
      uilist.removeElement(connect_form);
      for (var el:uint = 0; el < uilist.numElements; ++el) {
        uilist.getElementAt(el).visible = true;
        //uilist.getElementAt(el).includeInLayout = true;
      }
      last_connected = true;
    }
  }
  else {
    if (last_connected) {
      //Try to reconnect in two seconds if we lost our connection.
      retry_timer = 5;
    }
    last_connected = false;
    stat.text = "Status: disconnected";
  }
  var now:Date = new Date();
  if (last_updated != 0) {
    updated_field.text = "Updated " + (now.getTime()-last_updated)/1000.0 + " seconds ago.";
  }
  //Update world data every 20 seconds
  if ( 20000 <= (now.getTime() - world_update)) {
    world_update = now.getTime();
    //Query for region.* to find out what classes of objects are available.
    world.send(WorldServerProtocol.makeURISearchMsg("winlab.*"));
    //Also query for transmitters and receivers
    world.send(WorldServerProtocol.makeURISearchMsg("pipsqueak.*.*"));
    world.send(WorldServerProtocol.makeURISearchMsg("wins.*.*"));
  }

  //Check the status of coffee brews and display a countdown graphic as coffee ages.
  if (null != coffee_brewtime &&
      null != groups["coffee pots"]) {
    var cof_antenna:AntennaStatus = null;
    //Coffee age in minutes
    var age:Number = (now.getTime() - coffee_brewtime.getTime()) / (1000 * 60.0);
    for each (var gr:AntennaStatus in groups["coffee pots"]) {
      cof_antenna = gr;
    }
    //Make sure there is a coffee pot being displayed on the screen
    if (null != cof_antenna) {
      if (age < 60) {
        //brew_graphic.addDetail("Freshness", age + " minutes old");
        cof_antenna.addDetail("Freshness", Math.floor(age) + " minutes old");
      }
      else {
        //brew_graphic.addDetail("Freshness", age/60.0 + " hours old");
        cof_antenna.addDetail("Freshness", Math.floor((age/60.0)) + " hours old");
      }
    }
  }
}

