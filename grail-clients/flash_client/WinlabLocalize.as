package {
  public class WinlabLocalize extends LocalizationClient {

    public function WinlabLocalize() {
      super();
      portfield.text = "7010";
      ipfield.text = "grail1.winlab.rutgers.edu";
      ws_portfield.text = "7011";
      ws_ipfield.text = "grail1.winlab.rutgers.edu";
      grailDistributorConnect(null);
    }
  }
}
