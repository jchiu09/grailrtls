package {
  import flash.display.Sprite;
  import flash.display.DisplayObject;
  import flash.events.MouseEvent;

  public class Dropdown extends Sprite {
    //The objects in the drop down list and the currently select object
    public var objects:Vector.<DisplayObject>;
    private var _selected:uint = 0;
    //The space in between objects
    private var spacing:Number = 1;

    private var expanded:Boolean = false;

    public var selectedCallback:Function = null;

    //Get the display object currently selected or set which index
    //should be selected. Setting the selected value causes the
    //selectedCallback to be called.
    public function get selected():DisplayObject {
      return objects[_selected];
    }

    public function setSelected(index:uint):void {
      _selected = index;
      if (selectedCallback != null) {
        selectedCallback(selected);
      }
    }

    private function select(e:MouseEvent):void {
      var index:uint = e.localY / spacing;
      _selected = index;
      collapse(e);
      if (selectedCallback != null) {
        selectedCallback(selected);
      }
    }

    private function collapse(e:MouseEvent):void {
      while (numChildren > 0) {
        removeChildAt(numChildren-1);
      }
      objects[_selected].y = 0;
      addChild(objects[_selected]);
    }

    private function expand(e:MouseEvent):void {
      while (numChildren > 0) {
        removeChildAt(numChildren-1);
      }
      for (var i:uint = 0; i < objects.length; ++i) {
        objects[i].y = i * spacing;
        addChild(objects[i]);
      }
    }

    public function Dropdown(items:Vector.<DisplayObject>) {
      //Don't let children steal the mouse input.
      mouseChildren = false;
      //Find the max height so that we can properly set the spacing
      for (var i:uint = 0; i < items.length; ++i) {
        if (items[i].height > spacing) {
          spacing = items[i].height;
        }
      }
      objects = items;
      //Event listeners for dropdown behavior.
      addEventListener(MouseEvent.MOUSE_OVER, expand);
      addEventListener(MouseEvent.MOUSE_OUT, collapse);
      addEventListener(MouseEvent.CLICK, select);

      //Start in the collapsed state.
      collapse(null);
    }
  }
}

