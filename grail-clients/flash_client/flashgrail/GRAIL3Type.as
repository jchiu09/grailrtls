package flashgrail {

  public class GRAIL3Type {
    public var alias:uint;
    public var name:String;

    public function GRAIL3Type(alias:uint, name:String) {
      this.alias = alias;
      this.name  = name;
    }
  }
}

