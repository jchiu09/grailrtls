package flashgrail {

  public class SolverData {
    public var region:String = new String();
    public var time:Number = 0;
    public var solutions:Vector.<GRAILSolution> = new Vector.<GRAILSolution>();

    public function SolverData() {
    }
  }
}

