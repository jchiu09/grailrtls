package flashgrail {

  import flash.events.*;
  import flash.net.Socket;
  import flash.utils.ByteArray;
  import flashgrail.WorldServerProtocol;
  import flashgrail.FieldData;
  import flashgrail.WorldData;

  public class WorldServerClient extends Socket {
    private var segment_len:uint;
    //Callback when a query response message is received
    private var queryCallback:Function;
    private var uriSearchCallback:Function;
    private var statCallback:Function;

    /*
     * World server address as a string and port number as in integer.
     * queryCallbck(WorldData) - Callback for when a query response message is received.
     * uriSearchCallback(Vector.<String>) - Callback for when a uri search response is received.
     * statCallback(status:String) - callback for updates in the connection status.
     */
    public function WorldServerClient(host:String, port:int, queryCallback:Function,
                                 uriSearchCallback:Function, statCallback:Function) {
      //Set up the callbacks for this socket before connecting.
      super();
      this.queryCallback     = queryCallback;
      this.uriSearchCallback = uriSearchCallback;
      this.statCallback      = statCallback;

      //No data segments have been received yet.
      segment_len = 0;

      addEventListener(Event.CLOSE, closeHandler);
      addEventListener(Event.CONNECT, connectHandler);
      //The first piece of data should be a handshake message.
      addEventListener(ProgressEvent.SOCKET_DATA, handshakeHandler);
      //TODO Just letting the user see these errors for now.
      //addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
      //addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);

      //Now that callbacks are set up connect to the distributor.
      this.connect(host, port);
    }

    //Send the given message if we are connected.
    public function send(buff:ByteArray):void {
      if (connected) {
        statCallback("Sending request...");
        this.writeBytes(buff);
        this.flush();
      }
      else {
        statCallback("Could not send message: disconnected.");
      }
    }

    //Sends a handshake message when we connect to the distributor.
    private function connectHandler(connect:Event):void {
      //Send the handshake message
      send(WorldServerProtocol.makeHandshake());
    }

    private function closeHandler(close:Event):void {
      this.close();
      statCallback("Connection was closed.");
    }

    private function handshakeHandler(progress:ProgressEvent): void {
      //Compare the received message to a handshake.
      statCallback("Handshake received.");
      var handshake:ByteArray = new ByteArray();
      var buff:ByteArray = new ByteArray();
      this.readBytes(buff, 0, handshake.length);
      //TODO check the handshake here

      //Set the data handler to the normal data handler
      removeEventListener(ProgressEvent.SOCKET_DATA, handshakeHandler);
      addEventListener(ProgressEvent.SOCKET_DATA, socketDataHandler);
      //If there was another packet beyond the handshake then process it.
      if (bytesAvailable > 4) {
        socketDataHandler(progress);
      }
    }

    //Process a message from the world server.
    private function socketDataHandler(progress:ProgressEvent):void {
      statCallback("Handling socket data");
      //Check if we are starting a new segment.
      if ( 0 == segment_len && bytesAvailable >= 4) {
        segment_len = this.readInt();
      }
      //statCallback("handling socket data with length "+String(segment_len)+" with "+String(bytesAvailable)+" available bytes");
      //Keep reading data one segment at a time.
      //Multiple TCP messages may be merged so we must manually split data into segments.
      while ( 0 != segment_len &&
              bytesAvailable >= segment_len) {
        //Use the WorldServer module to decode the message based upon its type.
        var buff:ByteArray = new ByteArray();
        this.readBytes(buff, 0, segment_len);
        var msg_type:uint = buff.readByte();
        //Reset the segment length to 0 (do this here because an error
        //during decoding would prevent this from being set later)
        segment_len = 0;
        if ( WorldServerProtocol.QUERY_RESPONSE == msg_type) {
          statCallback("Received query response");
          var wd:WorldData = WorldServerProtocol.decodeQueryResponse(buff);
          queryCallback(wd);
        }
        else if ( WorldServerProtocol.URI_SEARCH_RESPONSE == msg_type) {
          statCallback("Received URI search response");
          var uris:Vector.<String> = WorldServerProtocol.decodeURISearchResponse(buff);
          uriSearchCallback(uris);
        }
        //No other message types should arrive here so drop this data.
        //Just ignore the contents of buff.
        segment_len = 0;
        //Check for more data.
        if (bytesAvailable >= 4) {
          segment_len = this.readInt();
        }
      }
    }
  }
}

