
package flashgrail {
  import flash.utils.ByteArray;

  public class SolverWorldModel {

    //Message type constants
    public static const KEEP_ALIVE:uint       = 0;
    public static const TYPE_ANNOUNCE:uint    = 1;
    public static const START_TRANSIENT:uint  = 2;
    public static const STOP_TRANSIENT:uint   = 3;
    public static const SOLVER_DATA:uint      = 4;
    public static const CREATE_URI:uint       = 5;
    public static const EXPIRE_URI:uint       = 6;
    public static const DELETE_URI:uint       = 7;
    public static const EXPIRE_ATTRIBUTE:uint = 8;
    public static const DELETE_ATTRIBUTE:uint = 9;

    public static function makeHandshake():ByteArray {
      var buff:ByteArray = new ByteArray();
      //The length of the handshake string
      var handshake:String = "GRAIL world model protocol";
      buff.writeInt(handshake.length);
      buff.writeUTFBytes(handshake);
      //Version numbers
      buff.writeByte(0);
      buff.writeByte(0);
      return buff;
    }

    /*
     * Function to read a sized utf 16 string from a byte array.
     * Necessary since I cannot get actionscript's UTF16 support to work.
     */
    public static function readSizedUTF16(buff:ByteArray):String {
      var str:String = new String();
      var strlen:int = buff.readUnsignedInt();
      for (var j:uint = 0; j < strlen; ++j) {
        var s:String = buff.readUTFBytes(1);
        if (j % 2 == 1) {
          str = str + s;
        }
      }
      return str;
    }

    //TODO I am having trouble using actionscript's UTF16 support so I am doing this manually.
    private static function makeSizedUTF16(uri:String):ByteArray {
      var buff:ByteArray = new ByteArray();

      //Two bytes for each character
      buff.writeUnsignedInt(2*uri.length);
      for (var i:uint = 0; i < uri.length; ++i) {
        buff.writeByte(0);
        buff.writeByte(uri.charCodeAt(i));
      }
      return buff;
    }

    //TODO I am having trouble using actionscript's UTF16 support so I am doing this manually.
    private static function makeUnsizedUTF16(uri:String):ByteArray {
      var buff:ByteArray = new ByteArray();

      for (var i:uint = 0; i < uri.length; ++i) {
        buff.writeByte(0);
        buff.writeByte(uri.charCodeAt(i));
      }
      return buff;
    }

    //Announce the solution types of this solver and its origin string
    public static function makeTypeAnnounce(types:Vector.<GRAIL3Type>, origin:String):ByteArray {
      var buff:ByteArray = new ByteArray();

      //Store the message type and data
      buff.writeByte(TYPE_ANNOUNCE);
      
      //Push back the number of aliases and push them into the buffer
      buff.writeUnsignedInt(types.length);
      for (var i:uint = 0; i < types.length; ++i) {
        buff.writeUnsignedInt(types[i].alias);
        buff.writeBytes(makeSizedUTF16(types[i].name));
        //TODO Not supporting transient values.
        buff.writeByte(0);
      }

      //Now store the origin
      buff.writeBytes(makeUnsizedUTF16(origin));

      //Store the size of the buffer at the beginning of the message.
      var msg:ByteArray = new ByteArray();
      msg.writeUnsignedInt(buff.length);
      msg.writeBytes(buff);

      return msg;
    }

    //Decode start and stop transient messages.
    public static function decodeStartTransient(length:uint, buff:ByteArray):Vector.<TransientCommand> {
      var aliases:Vector.<TransientCommand> = new Vector.<uint>();
      var total_aliases:uint = buff.readUnsignedInt();

      for (var i:uint = 0; i < total_aliases; ++i) {
        var tc:TransientCommand = new TransientCommand();
        tc.name_alias = buff.readUnsignedInt();
        var total_attribs:uint = buff.readUnsignedInt();
        for (var j:uint = 0; j < total_attribs; ++j) {
          tc.uris.push(readSizedUTF16(buff));
        }
        aliases.push(tc);
      }
      return aliases;
    }

    public static function decodeStopTransient(length:uint, buff:ByteArray):Vector.<TransientCommand> {
      return decodeStartTransient(length, buff);
    }

    public static function makeSolutionMsg(create_uris:Boolean, solutions:Vector.<GRAILSolution>):ByteArray {
      //Reuse the snapshot request to assemble this message
      var buff:ByteArray = new ByteArray();
      buff.writeByte(SOLVER_DATA);

      if (create_uris) {
        buff.writeByte(1);
      } else {
        buff.writeByte(0);
      }

      //Push back the solutions
      buff.writeUnsignedInt(solutions.length);
      for (var i:uint = 0; i < solutions.length; ++i) {
        buff.writeUnsignedInt(solutions[i].type_alias);

        //Split the time into an upper and lower 4 bytes
        buff.writeUnsignedInt(solutions[i].time/Math.pow(2,32));
        buff.writeUnsignedInt(solutions[i].time%Math.pow(2,32));
        buff.writeBytes(makeSizedUTF16(solutions[i].target));
        buff.writeUnsignedInt(solutions[i].data.length);
        buff.writeBytes(solutions[i].data);
      }

      //Reassamble this message with the correct message ID
      var msg:ByteArray = new ByteArray();
      msg.writeUnsignedInt(buff.length);
      msg.writeBytes(buff);
      return msg;
    }

    //Create a URI
    public static function makeCreateURI(uri:String, creation:Number, origin:String):ByteArray {
      var buff:ByteArray = new ByteArray();

      //Store the message type and data
      buff.writeByte(CREATE_URI);

      buff.writeBytes(makeSizedUTF16(uri));
      //Split the time into an upper and lower 4 bytes
      buff.writeUnsignedInt(creation/Math.pow(2,32));
      buff.writeUnsignedInt(creation%Math.pow(2,32));
      //Push back the origin string
      buff.writeBytes(makeUnsizedUTF16(origin));

      //Insert the length at the beginning
      var msg:ByteArray = new ByteArray();
      msg.writeUnsignedInt(buff.length);
      msg.writeBytes(buff);
      return msg;
    }

    //Make an expire URI message
    public static function makeExpireURI(uri:String, expiration:Number, origin:String):ByteArray {
      var buff:ByteArray = new ByteArray();

      //Store the message type and data
      buff.writeByte(EXPIRE_URI);

      buff.writeBytes(makeSizedUTF16(uri));
      //Split the time into an upper and lower 4 bytes
      buff.writeUnsignedInt(expiration/Math.pow(2,32));
      buff.writeUnsignedInt(expiration%Math.pow(2,32));
      //Push back the origin string
      buff.writeBytes(makeUnsizedUTF16(origin));

      //Insert the length at the beginning
      var msg:ByteArray = new ByteArray();
      msg.writeUnsignedInt(buff.length);
      msg.writeBytes(buff);
      return msg;
    }

    //Make an expire attribute message
    public static function makeExpireAttribute(uri:String, attribute:String, expiration:Number, origin:String):ByteArray {
      var buff:ByteArray = new ByteArray();

      //Store the message type and data
      buff.writeByte(EXPIRE_ATTRIBUTE);

      buff.writeBytes(makeSizedUTF16(uri));
      buff.writeBytes(makeSizedUTF16(attribute));
      //Split the time into an upper and lower 4 bytes
      buff.writeUnsignedInt(expiration/Math.pow(2,32));
      buff.writeUnsignedInt(expiration%Math.pow(2,32));
      //Push back the origin string
      buff.writeBytes(makeUnsizedUTF16(origin));

      //Insert the length at the beginning
      var msg:ByteArray = new ByteArray();
      msg.writeUnsignedInt(buff.length);
      msg.writeBytes(buff);
      return msg;
    }

    //Make a delete message
    public static function makeDeleteURI(uri:String, origin:String):ByteArray {
      var buff:ByteArray = new ByteArray();

      //Store the message type and data
      buff.writeByte(DELETE_URI);

      buff.writeBytes(makeSizedUTF16(uri));
      //Push back the origin string
      buff.writeBytes(makeUnsizedUTF16(origin));

      //Insert the length at the beginning
      var msg:ByteArray = new ByteArray();
      msg.writeUnsignedInt(buff.length);
      msg.writeBytes(buff);
      return msg;
    }

    //Make a delete attribute message
    public static function makeDeleteAttribute(uri:String, attribute:String, origin:String):ByteArray {
      var buff:ByteArray = new ByteArray();

      //Store the message type and data
      buff.writeByte(DELETE_ATTRIBUTE);

      buff.writeBytes(makeSizedUTF16(uri));
      buff.writeBytes(makeSizedUTF16(attribute));
      //Push back the origin string
      buff.writeBytes(makeUnsizedUTF16(origin));

      //Insert the length at the beginning
      var msg:ByteArray = new ByteArray();
      msg.writeUnsignedInt(buff.length);
      msg.writeBytes(buff);
      return msg;
    }
  }
}
