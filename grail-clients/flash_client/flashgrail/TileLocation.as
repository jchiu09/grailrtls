/*******************************************************************************
 * Interpret a bytearray as a tile location.
 * Tile location is stored as:
 * center x coordinate (double), center y coordinate (double),
 * tile width (double), tile height (double)
 * number of tiles (uint32_t),
 * (for each tile): column (uint32_t), row (uint32_t)
 ******************************************************************************/
package flashgrail {
  import flash.utils.ByteArray;
  public class TileLocation extends Location {
    public var tiles:Vector.<LocationTile> = new Vector.<LocationTile>();
    public var tile_width:Number;
    public var tile_height:Number;

    public function TileLocation(buff:ByteArray) {
      x = buff.readDouble();
      y = buff.readDouble();
      tile_width = buff.readDouble();
      tile_height = buff.readDouble();
      var num_tiles:uint = buff.readUnsignedInt();
      for (var i:uint = 0; i < num_tiles; ++i) {
        var tloc:LocationTile = new LocationTile();
        tloc.column = buff.readUnsignedInt();
        tloc.row = buff.readUnsignedInt();
        tiles.push(tloc);
      }
    }
  }
}
