package flashgrail {
  import flash.utils.ByteArray;

  public class RawSolution {
    public var type:String = "";
    public var target:String = "";
    public var time:Number = 0;
    public var data:ByteArray = new ByteArray();

    public function RawSolution() {
    }
  }
}

