package flashgrail {

  import flash.events.*;
  import flash.net.Socket;
  import flash.utils.ByteArray;
  import flash.utils.Dictionary;
  import flashgrail.ClientWorldModel;
  import flashgrail.WMAttribute;

  public class ClientWMClient extends Socket {
    //Length of an ongoing message.
    private var segment_len:uint;
    //Callback for string status updates
    private var statCallback:Function;

    //Remember associations from aliases to names
    private var types:Dictionary = new Dictionary();
    private var origins:Dictionary = new Dictionary();

    //Object with callbacks of interest
    private var callbacks:Object = {};

    private var host:String = "";
    private var port:int = 0;

    /*
     * World server address as a string and port number as in integer.
     * The callbacks Objects contains callbacks for any of the messages from
     * the world model. At the least it should have entries for
     * the DATA_RESPONSE and URI_RESPONSE messages (found under
     * callbacks.DATA_RESPONSE and callbacks.URI_RESPONSE respectively).
     * It may also have a callback for REQUEST_COMPLETE.
     * There are the types:
     * DATA_RESPONSE: callback(WDData)
     * URI_RESPONSE: callback(Vector.<String>)
     * REQUEST_COMPLETE: callback()
     * statCallback(status:String) - callback for updates in the connection status.
     */
    public function ClientWMClient(host:String, port:int, callbacks:Object, statCallback:Function) {
      //Set up the callbacks for this socket before connecting.
      super();
      this.callbacks    = callbacks;
      this.statCallback = statCallback;
      this.host = host;
      this.port = port;

      //No data segments have been received yet.
      segment_len = 0;

      addEventListener(Event.CLOSE, closeHandler);
      addEventListener(Event.CONNECT, connectHandler);
      //The first piece of data should be a handshake message.
      addEventListener(ProgressEvent.SOCKET_DATA, handshakeHandler);
      //TODO Just letting the user see these errors for now.
      //addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
      //addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);

      //Now that callbacks are set up connect to the world model.
      this.connect(host, port);
    }

    public function reconnect():void {
      //Clear set variables
      segment_len = 0;
      types = new Dictionary();
      origins = new Dictionary();
      //Set up the handshake handler again
      removeEventListener(ProgressEvent.SOCKET_DATA, socketDataHandler);
      addEventListener(ProgressEvent.SOCKET_DATA, handshakeHandler);
      this.connect(host, port);
    }

    //Send the given message if we are connected.
    public function send(buff:ByteArray):void {
      if (connected) {
        statCallback("Sending request...");
        this.writeBytes(buff);
        this.flush();
      }
      else {
        statCallback("Could not send message: disconnected.");
      }
    }

    //Sends a handshake message when we connect to the distributor.
    private function connectHandler(connect:Event):void {
      //Reset the values of types and origins.
      segment_len = 0;
      types = new Dictionary()
      origins = new Dictionary()
      //Send the handshake message
      send(ClientWorldModel.makeHandshake());
      statCallback("Handshake sent.");
    }

    private function closeHandler(close:Event):void {
      this.close();
      statCallback("Connection was closed.");
    }

    private function handshakeHandler(progress:ProgressEvent): void {
      //Compare the received message to a handshake.
      statCallback("Client handshake received.");
      var handshake:ByteArray = ClientWorldModel.makeHandshake();
      var buff:ByteArray = new ByteArray();
      this.readBytes(buff, 0, handshake.length);
      //TODO check the handshake here

      //Set the data handler to the normal data handler
      removeEventListener(ProgressEvent.SOCKET_DATA, handshakeHandler);
      addEventListener(ProgressEvent.SOCKET_DATA, socketDataHandler);
      //If there was another packet beyond the handshake then process it.
      if (bytesAvailable > 4) {
        socketDataHandler(progress);
      }
    }

    //Process a message from the world server.
    private function socketDataHandler(progress:ProgressEvent):void {
      statCallback("Handling socket data");
      //Check if we are starting a new segment.
      if ( 0 == segment_len && bytesAvailable >= 4) {
        segment_len = this.readUnsignedInt();
      }
      //statCallback("handling socket data with length "+String(segment_len)+" with "+String(bytesAvailable)+" available bytes");
      //Keep reading data one segment at a time.
      //Multiple TCP messages may be merged so we must manually split data into segments.
      while ( 0 != segment_len &&
              bytesAvailable >= segment_len) {
        //Use the WorldServer module to decode the message based upon its type.
        var buff:ByteArray = new ByteArray();
        this.readBytes(buff, 0, segment_len);
        var msg_type:uint = buff.readByte();
        //Reset the segment length to 0 (do this here because an error
        //during decoding would prevent this from being set later)
        segment_len = 0;
        if ( ClientWorldModel.KEEP_ALIVE == msg_type) {
          statCallback("Received keep alive");
        }
        else if ( ClientWorldModel.ATTRIBUTE_ALIAS == msg_type) {
          statCallback("Received URI attribute alias");
          var new_types:Vector.<GRAIL3Type> = ClientWorldModel.decodeAttrAliasMsg(segment_len, buff);
          for (var type:uint = 0; type < new_types.length; ++type) {
            types[new_types[type].alias] = new_types[type].name;
          }
          if (null != callbacks["ATTRIBUTE_ALIAS"]) {
            callbacks.ATTRIBUTE_ALIAS(types);
          }
        }
        else if ( ClientWorldModel.ORIGIN_ALIAS == msg_type) {
          statCallback("Received origin alias");
          var new_origins:Vector.<GRAIL3Type> = ClientWorldModel.decodeOriginAliasMsg(segment_len, buff);
          for (var og:uint = 0; og < new_origins.length; ++og) {
            origins[new_origins[og].alias] = new_origins[og].name;
          }
          if (null != callbacks["ORIGIN_ALIAS"]) {
            callbacks.ORIGIN_ALIAS(types);
          }
        }
        else if ( ClientWorldModel.URI_RESPONSE == msg_type) {
          statCallback("Received uri response");
          if (null != callbacks["URI_RESPONSE"]) {
            callbacks.URI_RESPONSE(ClientWorldModel.decodeURISearchResponse(segment_len, buff));
          }
        }
        else if ( ClientWorldModel.DATA_RESPONSE == msg_type) {
          statCallback("Received data response");
          if (null != callbacks["DATA_RESPONSE"]) {
            //Translate the aliases in this message to strings using
            //the origins and aliases maps.
            var awd:AliasedWorldData = ClientWorldModel.decodeDataMsg(segment_len, buff);
            var wmd:WMData = new WMData();
            wmd.ticket = awd.ticket;
            wmd.object_uri = awd.object_uri;
            for (var attr:uint = 0; attr < awd.attribs.length; ++attr) {
              var wma:WMAttribute = new WMAttribute();
              wma.name = types[awd.attribs[attr].name_alias];
              wma.creation_date = awd.attribs[attr].creation_date;
              wma.expiration_date = awd.attribs[attr].expiration_date;
              wma.origin = origins[awd.attribs[attr].origin_alias];
              wma.data = awd.attribs[attr].data;
              //Note that the name or origin could end up being null if there
              //haven't been alias messages for them.
              wmd.attribs.push(wma);
            }
            callbacks.DATA_RESPONSE(wmd);
          }
        }
        else if ( ClientWorldModel.REQUEST_COMPLETE == msg_type) {
          statCallback("Received request complete");
          var ticket:uint = ClientWorldModel.decodeTicketMsg(segment_len, buff);
          if (null != callbacks.REQUEST_COMPLETE) {
            callbacks.REQUEST_COMPLETE(ticket);
          }
        }
        //No other message types should arrive here so drop this data.
        //Just ignore the contents of buff.
        segment_len = 0;
        //Check for more data.
        if (bytesAvailable >= 4) {
          segment_len = this.readUnsignedInt();
        }
      }
    }
  }
}

