/*******************************************************************************
 * Package to convert GRAIL3 types to and from string representations.
 * The representation of data is dependent upon a type's name, as in
 * MIME types.
 ******************************************************************************/
package flashgrail {
  import flash.utils.ByteArray;

  public class TypeLookup {

    //Converts a character to its hex value (assumes upper case)
    private static function hexcharToInt(code:Number):uint {
      //65 is "A"
      //48 is "0"
      if (code < 65) {
        return code - 48;
      }
      else {
        return 10 + code - 65;
      }
    }

    //Convert a byte array to a UTF16 string
    private static function UTF16ToString(bytes:ByteArray):String {
      //TODO converting UTF16 to UTF8 but actionscript should support this
      var str:String = new String();
      var strlen:uint = bytes.length;
      for (var j:uint = 0; j < strlen; ++j) {
        var s:String = bytes.readUTFBytes(1);
        if (j % 2 == 1) {
          str = str + s;
        }
      }
      return str;
    }

    //TODO I am having trouble using actionscript's UTF16 support so I am doing this manually.
    public static function makeUnsizedUTF16(uri:String):ByteArray {
      var buff:ByteArray = new ByteArray();

      for (var i:uint = 0; i < uri.length; ++i) {
        buff.writeByte(0);
        buff.writeByte(uri.charCodeAt(i));
      }
      return buff;
    }

    /*
     * Function to read a sized utf 16 string from a byte array.
     * Necessary since I cannot get actionscript's UTF16 support to work.
     */
    public static function readSizedUTF16(buff:ByteArray):String {
      var str:String = new String();
      var strlen:int = buff.readUnsignedInt();
      for (var j:uint = 0; j < strlen; ++j) {
        var s:String = buff.readUTFBytes(1);
        if (j % 2 == 1) {
          str = str + s;
        }
      }
      return str;
    }

    //TODO I am having trouble using actionscript's UTF16 support so I am doing this manually.
    private static function makeSizedUTF16(uri:String):ByteArray {
      var buff:ByteArray = new ByteArray();

      //Two bytes for each character
      buff.writeUnsignedInt(2*uri.length);
      for (var i:uint = 0; i < uri.length; ++i) {
        buff.writeByte(0);
        buff.writeByte(uri.charCodeAt(i));
      }
      return buff;
    }

    //Translate a type to a string representation
    //If the type is known then translate it to values.
    //If it is not known then translate it to a binary string
    public static function typeToString(typename:String, data:ByteArray):String {
      //Make a human readable string from the data.
      var hr_string:String = "";
      var vec_regex:RegExp = /vector<(.*)>/;
      //If this is a vector then it has many primitive types inside
      if (vec_regex.test(typename)) {
        var result:Array = vec_regex.exec(typename);
        var inner_typename:String = result[1];
        var total_parts:uint = data.readUnsignedInt();
        while (total_parts > 0) {
          if (hr_string != "") {
            hr_string += ", ";
          }
          //Get the next part
          hr_string += typeToString(inner_typename, data);
          --total_parts;
        }
      }
      //Non vector type
      else {
        var tname_parts:Array = typename.split(".");
        var last:uint = tname_parts.length - 1;
        if (typename == "string" ||
            tname_parts[last] == "string" ||
            tname_parts[last] == "uri" ||
            tname_parts[last] == "url") {
          hr_string = UTF16ToString(data);
        }
        else if (typename == "sized string") {
          hr_string = readSizedUTF16(data);
        }
        //Location values are doubles
        else if (typename == "double" ||
              (tname_parts[last] == "double") ||
              (tname_parts[0] == "location") ||
              (tname_parts[0] == "temperature") ||
              (tname_parts[0] == "RSS")) {
          hr_string = (data.readDouble()).toString();
        }
        else if (typename == "uint32_t") {
          hr_string = (data.readUnsignedInt()).toString();
        }
        else if (typename == "int32_t") {
          hr_string = (data.readInt()).toString();
        }
        else if (typename == "uint64_t") {
          //Assemble from multiple ints.
          var val:Number = 0.0;
          val += data.readUnsignedInt() * Math.pow(2, 32);
          val += data.readUnsignedInt();
          hr_string = val.toString();
        }
        else if (typename == "uint128_t") {
          //Assemble from multiple ints.
          var uint128val:Number = 0.0;
          uint128val += data.readUnsignedInt() * Math.pow(2, 3*32);
          uint128val += data.readUnsignedInt() * Math.pow(2, 2*32);
          uint128val += data.readUnsignedInt() * Math.pow(2, 32);
          uint128val += data.readUnsignedInt();
          hr_string = uint128val.toString()
        }
        //Sensors are stored as a physical layer and ID in <phy>.<id> format.
        else if (tname_parts[0] == "sensor" ||
            tname_parts[last] == "sensor") {
          var phystring:String = (data.readUnsignedByte().toString(10));
          var idnum:Number = 0.0;
          idnum += data.readUnsignedInt() * Math.pow(2, 3*32);
          idnum += data.readUnsignedInt() * Math.pow(2, 2*32);
          idnum += data.readUnsignedInt() * Math.pow(2, 32);
          idnum += data.readUnsignedInt();
          hr_string += phystring + "." + idnum.toString();
        }
        else {
          //Otherwise convert the raw data into hexedecimal.  Do this one byte at a
          //time so that it is evenly padded (uint.toString does not do this).
          //Stop after 50 bytes.
          //var hr_string:TextField = makeTField(String(data));
          var hex_str:String = "0x";
          for (var b:uint = 0; b < data.length && b < 50; ++b) {
            var bstring:String = (data.readUnsignedByte().toString(16));
            //uint.toSTring does not pad so add an extr 0 if it is needed.
            if (bstring.length < 2) { bstring = "0" + bstring;}
            hex_str += bstring;
          }
          hr_string = hex_str;
        }
      }
      return hr_string;
    }

    public static function typeFromString(typename:String, datatext:String):ByteArray {
      var data:ByteArray = new ByteArray();
      var vec_regex:RegExp = /vector<(.*)>/;
      //If this is a vector then it has many primitive types inside
      if (vec_regex.test(typename)) {
        var result:Array = vec_regex.exec(typename);
        var inner_typename:String = result[1];
        var inner_elements:Array = datatext.split(", ");
        //First record the total number of inner elements
        data.writeUnsignedInt(inner_elements.length);
        for (var d_index:uint = 0; d_index < inner_elements.length; ++d_index) {
          data.writeBytes(typeFromString(inner_typename, inner_elements[d_index]));
        }
      }
      else {
        //Not a vector
        var tname_parts:Array = typename.split(".");
        var last:uint = tname_parts.length - 1;
        if (typename == "string" ||
            tname_parts[last] == "string" ||
            tname_parts[last] == "uri" ||
            tname_parts[last] == "url") {
          data = makeUnsizedUTF16(datatext);
        }
        else if (typename == "sized string") {
          data.writeBytes(makeSizedUTF16(datatext));
        }
        //Location values are doubles
        else if (typename == "double" ||
            (tname_parts[last] == "double") ||
            (tname_parts[0] == "location") ||
            (tname_parts[0] == "temperature") ||
            (tname_parts[0] == "RSS")) {
          data.writeDouble(Number(datatext));
        }
        else if (typename == "uint32_t") {
          data.writeUnsignedInt(uint(datatext));
        }
        else if (typename == "int32_t") {
          data.writeInt(int(datatext));
        }
        else if (typename == "uint64_t") {
          data.writeUnsignedInt(0);
          data.writeUnsignedInt(uint(datatext));
        }
        else if (typename == "uint128_t") {
          data.writeUnsignedInt(uint(Number(datatext)/Math.pow(2, 3*32)));
          data.writeUnsignedInt(uint(Number(datatext)/Math.pow(2, 2*32))%Math.pow(2, 32));
          data.writeUnsignedInt(uint(Number(datatext)/Math.pow(2, 32))%Math.pow(2, 32));
          data.writeUnsignedInt(uint(datatext)%Math.pow(2, 32));
        }
        //Sensors are stored as a physical layer and ID in <phy>.<id> format.
        else if (tname_parts[0] == "sensor" ||
            tname_parts[last] == "sensor") {
          var pieces:Array = datatext.split(".");
          //Write the phy
          data.writeByte(uint(pieces[0]));
          //Write the ID
          data.writeBytes(typeFromString("uint128_t", pieces[1]));
        }
        else {
          //Otherwise convert the raw data.
          //If the first two characters are 0x then treat this as hexedecimal.
          if (datatext.length >= 2 && "0x" == datatext.substr(0, 2)) {
            //Process 2 characters at a time
            var hex_str:String = datatext.substr(2).toUpperCase();
            //Make sure there are an even number of characters
            if (hex_str.length % 2 == 1) {
              hex_str = "0" + hex_str;
            }
            for (var idx:uint = 0; idx < hex_str.length; idx += 2) {
              var byte_val:uint = hexcharToInt(hex_str.charCodeAt(idx))*16 + hexcharToInt(hex_str.charCodeAt(idx+1));
              data.writeByte(byte_val);
            }
          }
          else {
            for (var idx2:uint = 0; idx2 < datatext.length; ++idx2) {
              var byte_val2:uint = datatext.charCodeAt(idx2)*8 + datatext.charCodeAt(idx2);
              data.writeByte(byte_val2);
            }
          }
        }
      }
      return data;
    }
  }
}
