package flashgrail {

  public class TransientCommand {
    //The alias for this transient attribute.
    public var name_alias:uint = 0;
    //URI patterns for this attribute
    public var uris:Vector.<String> = new Vector.<String>();

    public function TransientCommand() {
    }
  }
}
