package flashgrail {
  import flash.utils.ByteArray;

  public class FieldData {

    //Description of this field.
    public var description:String;
    //Creation and expiration data. If expiration_data < creation_date then this does not expire.
    public var creation_date:uint;
    public var expiration_date:uint;
    //Data type of the data array.
    public var data_type:String;
    //Raw data
    public var data:ByteArray = new ByteArray();

    public function FieldData(){}
    /*
    public function FieldData(desc:String, create:uint, expire:uint, data_type:String, data:ByteArray) {
      description = desc;
      creation_date = create;
      expiration_date = expire;
      this.data_type = data_type;
      this.data = data;
    }
    */
  }
}
