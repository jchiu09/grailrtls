package flashgrail {

  public class WMData {
    //The URI of this data
    public var object_uri:String = "";
    //The request ticket that generated this data
    public var ticket:uint = 0;

    //This URI's attributes
    public var attribs:Vector.<WMAttribute> = new Vector.<WMAttribute>();

    public function WMData() {
    }
  }
}
