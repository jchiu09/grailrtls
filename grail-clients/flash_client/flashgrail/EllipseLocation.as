/*******************************************************************************
 * Interpret a bytearray as an ellipse location.
 * Ellipse location data is stored as four doubles
 * representing x coordinate, y coordinate, width, and height.
 ******************************************************************************/
package flashgrail {
  import flash.utils.ByteArray;
  public class EllipseLocation extends Location {
    public var width:Number;
    public var height:Number;

    public function EllipseLocation(buff:ByteArray) {
      x = buff.readDouble();
      y = buff.readDouble();
      width = buff.readDouble();
      height = buff.readDouble();
    }
  }
}
