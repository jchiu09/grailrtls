package flashgrail {
  import flash.utils.ByteArray;
  import flashgrail.WMAttribute;


  public class ClientWorldModel {

    //Message type constants
    public static const KEEP_ALIVE:uint       = 0;
    public static const SNAPSHOT_REQUEST:uint = 1;
    public static const RANGE_REQUEST:uint    = 2;
    public static const STREAM_REQUEST:uint   = 3;
    public static const ATTRIBUTE_ALIAS:uint  = 4;
    public static const ORIGIN_ALIAS:uint     = 5;
    public static const REQUEST_COMPLETE:uint = 6;
    public static const CANCEL_REQUEST:uint   = 7;
    public static const DATA_RESPONSE:uint    = 8;
    public static const URI_SEARCH:uint       = 9;
    public static const URI_RESPONSE:uint     = 10;

    public static function makeHandshake():ByteArray {
      var buff:ByteArray = new ByteArray();
      //The length of the handshake string
      buff.writeInt(21);
      buff.writeUTFBytes("GRAIL client protocol");
      //Version numbers
      buff.writeByte(0);
      buff.writeByte(0);
      return buff;
    }

    /*
     * Function to read a sized utf 16 string from a byte array.
     * Necessary since I cannot get actionscript's UTF16 support to work.
     */
    public static function readSizedUTF16(buff:ByteArray):String {
      var str:String = new String();
      var strlen:int = buff.readUnsignedInt();
      for (var j:uint = 0; j < strlen; ++j) {
        var s:String = buff.readUTFBytes(1);
        if (j % 2 == 1) {
          str = str + s;
        }
      }
      return str;
    }
    public static function readUnsizedUTF16(buff:ByteArray, strlen:uint):String {
      var str:String = new String();
      for (var j:uint = 0; j < strlen; ++j) {
        var s:String = buff.readUTFBytes(1);
        if (j % 2 == 1) {
          str = str + s;
        }
      }
      return str;
    }

    //TODO I am having trouble using actionscript's UTF16 support so I am doing this manually.
    private static function makeSizedUTF16(uri:String):ByteArray {
      var buff:ByteArray = new ByteArray();

      //Two bytes for each character
      buff.writeUnsignedInt(2*uri.length);
      for (var i:uint = 0; i < uri.length; ++i) {
        buff.writeByte(0);
        buff.writeByte(uri.charCodeAt(i));
      }
      return buff;
    }

    //TODO I am having trouble using actionscript's UTF16 support so I am doing this manually.
    private static function makeUnsizedUTF16(uri:String):ByteArray {
      var buff:ByteArray = new ByteArray();

      for (var i:uint = 0; i < uri.length; ++i) {
        buff.writeByte(0);
        buff.writeByte(uri.charCodeAt(i));
      }
      return buff;
    }

    //Return the GRAIL3 types in this message
    public static function decodeAttrAliasMsg(length:uint, buff:ByteArray):Vector.<GRAIL3Type> {
      var msg:Vector.<GRAIL3Type> = new Vector.<GRAIL3Type>();
      var total_types:uint = buff.readUnsignedInt();

      for (var i:uint = 0; i < total_types; ++i) {
        var alias:uint = buff.readUnsignedInt();
        //The string is in Big-Endian UTF16 so we cannot just use readUTF
        var name:String = readSizedUTF16(buff);
        msg.push(new GRAIL3Type(alias, name));
      }
      return msg;
    }

    //Return the origins and their aliases in this message
    public static function decodeOriginAliasMsg(length:uint, buff:ByteArray):Vector.<GRAIL3Type> {
      return decodeAttrAliasMsg(length, buff);
    }

    public static function makeSnapshotRequest(request:WorldModelRequest, ticket:uint):ByteArray {
      var buff:ByteArray = new ByteArray();

      //Store the message type and data
      buff.writeByte(SNAPSHOT_REQUEST);

      //Write the ticket number first
      buff.writeUnsignedInt(ticket);

      buff.writeBytes(makeSizedUTF16(request.object_uri));

      //Record the number of attributes and then store them
      buff.writeUnsignedInt(request.attribs.length);
      for (var i:uint = 0; i < request.attribs.length; ++i) {
        buff.writeBytes(makeSizedUTF16(request.attribs[i]));
      }
      //These should be 64 bit integers but are stored as floats
      //because flash does not actually have support for 64 bit integers
      buff.writeUnsignedInt(request.begin / Math.pow(2,32));
      buff.writeUnsignedInt(uint(request.begin) % Math.pow(2,32));
      buff.writeUnsignedInt(request.end_period / Math.pow(2,32));
      buff.writeUnsignedInt(uint(request.end_period) % Math.pow(2,32));

      //Store the size of the buffer at the beginning of the message.
      var msg:ByteArray = new ByteArray();
      msg.writeUnsignedInt(buff.length);
      msg.writeBytes(buff);

      return msg;
    }

    public static function makeRangeRequest(request:WorldModelRequest, ticket:uint):ByteArray {
      //Reuse the snapshot request to assemble this message
      var buff:ByteArray = makeSnapshotRequest(request, ticket);
      buff.position = 4;
      buff.writeByte(RANGE_REQUEST);
      return buff;
    }

    public static function makeStreamRequest(request:WorldModelRequest, ticket:uint):ByteArray {
      //Reuse the snapshot request to assemble this message
      var buff:ByteArray = makeSnapshotRequest(request, ticket);
      buff.position = 4;
      buff.writeByte(STREAM_REQUEST);
      return buff;
    }

    public static function decodeRequestComplete(length:uint, buff:ByteArray):String {
      var name:String = readUnsizedUTF16(buff, buff.bytesAvailable);
      return name;
    }

    //Search for a URI regex in the world model
    public static function makeCancelRequest(ticket:uint):ByteArray {
      var buff:ByteArray = new ByteArray();

      //Store the message type and data
      buff.writeByte(ticket);

      //Store the size of the buffer at the beginning of the message.
      var msg:ByteArray = new ByteArray();
      msg.writeUnsignedInt(buff.length);
      msg.writeBytes(buff);

      return msg;
    }

    //Decode a ticket message.
    public static function decodeTicketMsg(length:uint, buff:ByteArray):uint {
      //The total length and message type were already read from the message.

      //Read in the ticket value
      var ticket:uint = buff.readUnsignedInt();

      return ticket;
    }

    //Return the GRAIL3 solutions in this message
    public static function decodeDataMsg(length:uint, buff:ByteArray):AliasedWorldData {
      var wd:AliasedWorldData = new AliasedWorldData();

      //The total length and message type were already read from the message.

      //Get the region uri
      wd.object_uri = readSizedUTF16(buff);
      wd.ticket = buff.readUnsignedInt();

      //Read in the number of attributes
      var total_attributes:uint = buff.readUnsignedInt();
      for (var i:uint = 0; i < total_attributes; ++i) {
        var aa:AliasedAttribute = new AliasedAttribute();
        aa.name_alias = buff.readUnsignedInt();
        //Get the time (convert from a 64 bit integer to a floating point number).
        aa.creation_date = Math.pow(2,32)*buff.readUnsignedInt();
        aa.creation_date += buff.readUnsignedInt();
        aa.expiration_date = Math.pow(2,32)*buff.readUnsignedInt();
        aa.expiration_date += buff.readUnsignedInt();
        aa.origin_alias = buff.readUnsignedInt();
        //Get the number of bytes in the data field and then read it in
        var data_len:uint = buff.readUnsignedInt();
        if (0 < data_len) {
          buff.readBytes(aa.data, 0, data_len);
        }

        //Store this solution
        wd.attribs.push(aa);
      }

      return wd;
    }

    //Search for a URI regex in the world model
    public static function makeURISearch(uri:String):ByteArray {
      var buff:ByteArray = new ByteArray();

      //Store the message type and data
      buff.writeByte(URI_SEARCH);

      buff.writeBytes(makeUnsizedUTF16(uri));
      //Store the size of the buffer at the beginning of the message.
      var msg:ByteArray = new ByteArray();
      msg.writeUnsignedInt(buff.length);
      msg.writeBytes(buff);

      return msg;
    }

    //Decode the URI search response
    public static function decodeURISearchResponse(length:uint, buff:ByteArray):Vector.<String> {
      var uris:Vector.<String> = new Vector.<String>();
      while (0 < buff.bytesAvailable) {
        var uri:String = readSizedUTF16(buff);
        uris.push(uri);
      }
      return uris;
    }
  }
}
