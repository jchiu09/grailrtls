package flashgrail {
  import flash.utils.ByteArray;

  public class WMAttribute {
    //The alias for the description of this attribute.
    //The attribute name indicates the data type.
    public var name:String = "";
    //The creation date and the date when this attribute's data expires.
    public var creation_date:Number = 0;
    public var expiration_date:Number = 0;

    public var origin:String = "";
    public var data:ByteArray = new ByteArray();

    public function WMAttribute() {
    }
  }
}
