package flashgrail {

  import flash.events.*;
  import flash.net.Socket;
  import flash.utils.ByteArray;
  import flash.utils.Dictionary;
  import flashgrail.SolverWorldModel;
  import flashgrail.RawSolution;

  public class SolverWMClient extends Socket {
    //Length of an ongoing message
    private var segment_len:uint;
    private var aliases:Dictionary = new Dictionary()
    private var total_aliases:uint = 0;
    private var origin:String = "";
    //Callback for string status updates
    private var statCallback:Function;

    private var host:String = "";
    private var port:int = 0;

    /*
     * World model address as a string and port number as in integer.
     */
    public function SolverWMClient(host:String, port:int, statCallback:Function, origin:String) {
      //Set up the callbacks for this socket before connecting.
      super();
      this.statCallback      = statCallback;
      this.origin = origin;
      this.host = host;
      this.port = port;

      //No data segments have been received yet.
      segment_len = 0;

      addEventListener(Event.CLOSE, closeHandler);
      addEventListener(Event.CONNECT, connectHandler);
      //The first piece of data should be a handshake message.
      addEventListener(ProgressEvent.SOCKET_DATA, handshakeHandler);
      //TODO Just letting the user see these errors for now.
      //addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
      //addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);

      //Now that callbacks are set up connect to the world model.
      this.connect(host, port);
    }

    public function reconnect():void {
      //Clear set variables
      segment_len = 0;
      aliases = new Dictionary();
      total_aliases = 0;
      //Set up the handshake handler again
      removeEventListener(ProgressEvent.SOCKET_DATA, socketDataHandler);
      addEventListener(ProgressEvent.SOCKET_DATA, handshakeHandler);
      this.connect(host, port);
    }

    //Send the given message if we are connected.
    public function send(buff:ByteArray):void {
      if (connected) {
        statCallback("Sending request...");
        this.writeBytes(buff);
        this.flush();
      }
      else {
        statCallback("Could not send message: disconnected.");
      }
    }

    //Sends a handshake message when we connect to the distributor.
    private function connectHandler(connect:Event):void {
      //Clear some initial data
      segment_len = 0;
      aliases = new Dictionary();
      total_aliases = 0;
      //Send the handshake message
      send(SolverWorldModel.makeHandshake());
      statCallback("Handshake sent.");
    }

    private function closeHandler(close:Event):void {
      this.close();
      statCallback("Connection was closed.");
    }

    private function handshakeHandler(progress:ProgressEvent): void {
      //Compare the received message to a handshake.
      statCallback("Solver handshake received.");
      var handshake:ByteArray = SolverWorldModel.makeHandshake();
      var buff:ByteArray = new ByteArray();
      this.readBytes(buff, 0, handshake.length);
      //TODO check the handshake here

      //Set the data handler to the normal data handler
      removeEventListener(ProgressEvent.SOCKET_DATA, handshakeHandler);
      addEventListener(ProgressEvent.SOCKET_DATA, socketDataHandler);
      //If there was another packet beyond the handshake then process it.
      if (bytesAvailable > 4) {
        socketDataHandler(progress);
      }
    }

    public function sendSolutions(solns:Vector.<RawSolution>, create_uris:Boolean):void {
      var aliased_solutions:Vector.<GRAILSolution> = new Vector.<GRAILSolution>();
      var new_types:Vector.<GRAIL3Type> = new Vector.<GRAIL3Type>();
      for (var i:uint = 0; i < solns.length; ++i) {
        var gs:GRAILSolution = new GRAILSolution();
        //Assign a new alias if necessary (requires sending a new alias message).
        if (null == aliases[solns[i].type]) {
          ++total_aliases;
          aliases[solns[i].type] = total_aliases;
          new_types.push(new GRAIL3Type(total_aliases, solns[i].type));
        }
        gs.type_alias = aliases[solns[i].type];
        gs.target = solns[i].target;
        gs.time = solns[i].time;
        gs.data = solns[i].data;
        aliased_solutions.push(gs);
      }
      //Send any new types to the world model.
      if (0 != new_types.length) {
        send(SolverWorldModel.makeTypeAnnounce(new_types, origin));
      }

      //Send the solutions
      send(SolverWorldModel.makeSolutionMsg(create_uris, aliased_solutions));
    }

    //Process a message from the world model.
    private function socketDataHandler(progress:ProgressEvent):void {
      statCallback("Handling socket data");
      //Check if we are starting a new segment.
      if ( 0 == segment_len && bytesAvailable >= 4) {
        segment_len = this.readUnsignedInt();
      }
      //statCallback("handling socket data with length "+String(segment_len)+" with "+String(bytesAvailable)+" available bytes");
      //Keep reading data one segment at a time.
      //Multiple TCP messages may be merged so we must manually split data into segments.
      while ( 0 != segment_len &&
              bytesAvailable >= segment_len) {
        //Use the World Model module to decode the message based upon its type.
        var buff:ByteArray = new ByteArray();
        this.readBytes(buff, 0, segment_len);
        var msg_type:uint = buff.readByte();
        //Reset the segment length to 0 (do this here because an error
        //during decoding would prevent this from being set later)
        segment_len = 0;
        if ( SolverWorldModel.KEEP_ALIVE == msg_type) {
          statCallback("Received keep alive");
        }
        else if ( SolverWorldModel.START_TRANSIENT == msg_type) {
          statCallback("Received start transient message");
        }
        else if ( SolverWorldModel.STOP_TRANSIENT == msg_type) {
          statCallback("Received stop transient message");
        }
        //No other message types should arrive here so drop this data.
        //Just ignore the contents of buff.
        segment_len = 0;
        //Check for more data.
        if (bytesAvailable >= 4) {
          segment_len = this.readUnsignedInt();
        }
      }
    }
  }
}

