package flashgrail {
  import flash.utils.ByteArray;

  public class GRAILSolution {
    public var type_alias:uint = 0;
    public var target:String = "";
    public var time:Number = 0;
    public var data:ByteArray = new ByteArray();

    public function GRAILSolution() {
    }
  }
}

