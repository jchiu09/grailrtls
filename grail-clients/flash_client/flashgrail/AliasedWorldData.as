package flashgrail {

  public class AliasedWorldData {
    //URI of this data
    public var object_uri:String = "";
    //The ticket of the request that this data came as a response to.
    public var ticket:uint = 0;

    //This URI's attributes
    public var attribs:Vector.<AliasedAttribute> = new Vector.<AliasedAttribute>();

    public function AliasedWorldData() {
    }
  }
}
