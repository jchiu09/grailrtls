package flashgrail {
  import flash.utils.ByteArray;


  public class WorldServerProtocol {

    //Message type constant
    public static const KEEP_ALIVE:uint          = 0; //Should be used!
    public static const CERTIFICATE:uint         = 1; //Not used!
    public static const ACK_CERTIFICATE:uint     = 2; //Not used!
    public static const OBJECT_QUERY:uint        = 3;
    public static const OBJECT_DATA_QUERY:uint   = 4;
    public static const QUERY_RESPONSE:uint      = 5;
    public static const URI_SEARCH:uint          = 6;
    public static const URI_SEARCH_RESPONSE:uint = 7;
    public static const PUSH_DATA:uint           = 8;

    //Handshake message
    public static function makeHandshake():ByteArray {
      var buff:ByteArray = new ByteArray();
      //The length of the handshake string
      buff.writeInt(20);
      buff.writeUTFBytes("GRAIL world protocol");
      //Version numbers
      buff.writeByte(0);
      buff.writeByte(0);
      return buff;
    }

    /*
     * Function to read a sized utf 16 string from a byte array.
     * Necessary since I cannot get actionscript's UTF16 support to work.
     */
    private static function readSizedUTF16(buff:ByteArray):String {
      var str:String = new String();
      var strlen:int = buff.readUnsignedInt();
      for (var j:uint = 0; j < strlen; ++j) {
        var s:String = buff.readUTFBytes(1);
        if (j % 2 == 1) {
          str = str + s;
        }
      }
      return str;
    }

    //TODO I am having trouble using actionscript's UTF16 support so I am doing this manually.
    public static function makeUnsizedUTF16(uri:String):ByteArray {
      var buff:ByteArray = new ByteArray();

      for (var i:uint = 0; i < uri.length; ++i) {
        buff.writeByte(0);
        buff.writeByte(uri.charCodeAt(i));
      }
      return buff;
    }

    //TODO I am having trouble using actionscript's UTF16 support so I am doing this manually.
    public static function makeSizedUTF16(uri:String):ByteArray {
      var buff:ByteArray = new ByteArray();

      //Two bytes for each character
      buff.writeUnsignedInt(2*uri.length);
      for (var i:uint = 0; i < uri.length; ++i) {
        buff.writeByte(0);
        buff.writeByte(uri.charCodeAt(i));
      }
      return buff;
    }

    public static function makeObjectQueryMsg(object_uri:String):ByteArray {
      var buff:ByteArray = new ByteArray();


      //Store the message type and data
      buff.writeByte(OBJECT_QUERY);
      buff.writeBytes(makeSizedUTF16(object_uri));

      //Store the size of the buffer at the beginning of the message.
      var msg:ByteArray = new ByteArray();
      msg.writeUnsignedInt(buff.length);
      msg.writeBytes(buff);

      return msg;
    }

    public static function makeObjectDataQueryMsg(object_uri:String):ByteArray {
      var buff:ByteArray = new ByteArray();

      //Store the message type and data
      buff.writeByte(OBJECT_DATA_QUERY);
      buff.writeBytes(makeSizedUTF16(object_uri));

      //Store the size of the buffer at the beginning of the message.
      var msg:ByteArray = new ByteArray();
      msg.writeUnsignedInt(buff.length);
      msg.writeBytes(buff);

      return msg;
    }

    public static function decodeQueryResponse(buff:ByteArray):WorldData {
      //Assuming that the length and message type were already read from the buffer.

      //Decode the world data
      var wd:WorldData = new WorldData();

      //Get the object uri
      //Having a bugger of a time reading the unicode string with actionscript's
      //functions so we'll just read it byte by byte and drop every other char.
      wd.object_uri = readSizedUTF16(buff);
      var num_fields:uint = buff.readUnsignedInt();
      var descriptions:String = "";
      for (var i:uint = 0; i < num_fields; ++i) {
        var fd:FieldData = new FieldData();
        fd.description     = readSizedUTF16(buff);
        descriptions = descriptions + ", " + fd.description;
        //Discard the upper 4 bytes of the creation and expiration dates since
        //flash doesn't support 64 bit integers.
        var creation_upper:uint = buff.readUnsignedInt();
        fd.creation_date   = buff.readUnsignedInt();
        var expiration_upper:uint = buff.readUnsignedInt();
        fd.expiration_date = buff.readUnsignedInt();
        fd.data_type = readSizedUTF16(buff);
        //Get the number of bytes in the raw data field and then read it in
        var data_len:uint = buff.readUnsignedInt();
        buff.readBytes(fd.data, 0, data_len);
        /*
        for (var j:uint = 0; j < data_len; ++j) {
          fd.data.writeByte(buff.readByte());
        }
        */
        //Push back the new field
        wd.fields.push(fd);
      }
      return wd;
    }

    public static function makeURISearchMsg(search_uri:String):ByteArray {
      var buff:ByteArray = new ByteArray();

      //Store the message type and data
      buff.writeByte(URI_SEARCH);
      buff.writeBytes(makeSizedUTF16(search_uri));

      //Store the size of the buffer at the beginning of the message.
      var msg:ByteArray = new ByteArray();
      msg.writeUnsignedInt(buff.length);
      msg.writeBytes(buff);

      return msg;
    }

    public static function decodeURISearchResponse(buff:ByteArray):Vector.<String> {
      //Assuming that the length and message type were already read from the buffer.

      var uris:Vector.<String> = new Vector.<String>();

      //Determine the number of URIs.
      var num_uris:uint = buff.readUnsignedInt();
      //Read in the URIs.
      for (var i:uint = 0; i < num_uris; ++i) {
        var str:String = readSizedUTF16(buff);
        uris.push(str);
      }
      //Return the URIs.
      return uris;
    }

    public static function makePushDataMsg(world_data:WorldData):ByteArray {
      var buff:ByteArray = new ByteArray();

      //Push the message type
      buff.writeByte(PUSH_DATA);
      //Push the world data into the buffer.
      buff.writeBytes(makeSizedUTF16(world_data.object_uri));
      //Now write the fields.
      buff.writeUnsignedInt(world_data.fields.length);
      for (var i:uint = 0; i < world_data.fields.length; ++i) {
        var fd:FieldData = world_data.fields[i];
        buff.writeBytes(makeSizedUTF16(fd.description));
        //TODO Flash does not support 64 bit integers so padding the upper byte here.
        buff.writeUnsignedInt(0);
        buff.writeUnsignedInt(fd.creation_date);
        buff.writeUnsignedInt(0);
        buff.writeUnsignedInt(fd.expiration_date);
        buff.writeBytes(makeSizedUTF16(fd.data_type));
        //Finally push back the raw data's length and the data.
        buff.writeUnsignedInt(fd.data.length);
        buff.writeBytes(fd.data);
      }

      //Store the size of the buffer at the beginning of the message.
      var msg:ByteArray = new ByteArray();
      msg.writeUnsignedInt(buff.length);
      msg.writeBytes(buff);
      return msg;
    }
  }
}
