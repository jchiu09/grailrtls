package flashgrail {

  public class WorldModelRequest {
    //Regex URI that matches any URIs of interest
    public var object_uri:String = "";

    //Attributes of interest
    public var attribs:Vector.<String> = new Vector.<String>();

    //The beginning and ending of the request, or the beginning and periodicity
    //of the request.
    public var begin:Number = 0;
    public var end_period:Number   = 0;

    public function WorldModelRequest() {
    }
  }
}
