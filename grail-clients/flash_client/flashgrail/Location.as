/*******************************************************************************
 * Base location class that specific kinds of location data should extend from.
 ******************************************************************************/
package flashgrail {
  public class Location {
    public var x:Number;
    public var y:Number;

    public function Location() {
    }
  }
}
