package flashgrail {

  public class WorldData {
    //The URI of an object in the world server.
    public var object_uri:String = new String();
    //Fields of this URI
    public var fields:Vector.<FieldData> = new Vector.<FieldData>();

    public function WorldData(){};
    /*
    public function WorldData(uri:String, fields) {
      this.object_uri = uri;
      this.fields = fields;
    }
    */
  }
}
