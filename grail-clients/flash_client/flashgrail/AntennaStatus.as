package flashgrail {
  import flash.text.TextFormat;
  import flash.text.TextField;
  import flash.text.TextFieldType;
  import flash.display.DisplayObject;
  import flash.display.SimpleButton;
  import flash.display.Sprite;
  import flash.utils.Dictionary;

  public class AntennaStatus extends Sprite {
    private var img:DisplayObject;
    public var _location:Location;
    public var id:String;
    public var id_button:SimpleButton;
    private var _details:TextField;
    private var details:Dictionary = new Dictionary();

    private var _img_scale:Number = 1.0;

    public function addDetail(d_name:String, d_text:String):void {
      details[d_name] = d_text;
      //Readjust how everything looks
      layoutComponents();
    }
    //TODO These should just be get and set functions.
    public function getDetails():Dictionary {
      return details;
    }
    public function setDetails(d:Dictionary):void {
      details = d;
      layoutComponents();
    }

    /*
     * Convenience function to make a text field.
     */
    private function makeTField(str:String):TextField {
      var tfield:TextField = new TextField();
      tfield.text = str;
      tfield.autoSize = "center";
      tfield.border   = true;
      tfield.type     = TextFieldType.DYNAMIC;
      return tfield;
    }

    private function layoutComponents():void {
      while (numChildren != 0) {
        this.removeChildAt(numChildren - 1);
      }
      var empty:Sprite = new Sprite();
      var hover:Sprite = new Sprite();
      with(empty.graphics) {
        lineStyle(1, 0x000000);
        beginFill(0xFFFFFF);
        drawRect(-1 * img.width/2.0, -1 * img.height / 2.0, img.width, img.height);
        endFill;
      }
      empty.visible = false;
      //Center the image over the location
      img.x = -1 * (img.width/2.0);
      img.y = -1 * (img.height/2.0);
      //Use the image to set the size of the button, but img will actually be added
      //as this sprite's child later on.
      addChild(img);

      //Loop through the dictionary to update the _details textfield
      var d_string:String = "ID: " + id + "\n" + "Location: " + location.x.toFixed(0) + ", " + location.y.toFixed(0);
      for (var k:Object in details) {
        var key:String = k as String;
        if (null != details[key]) {
          d_string += "\n" + key + " " + details[key];
        }
      }
      _details = makeTField(d_string);
      _details.background = true;
      _details.backgroundColor = 0xFFFFFF;
      //Keep this field even with the id field.
      _details.x = -1 * (_details.textWidth) / 2.0;
      _details.y = img.height/2.0;
      hover.addChild(_details);

      id_button = new SimpleButton(empty, hover, hover, empty);
      id_button.width = img.width;
      id_button.height = img.height;
      id_button.hitTestState = img;
      addChild(id_button);
    }

    public function AntennaStatus(img:DisplayObject, loc:Location, id:String) {
      this.id = id;
      this.img = img;
      this._location = loc;
      layoutComponents();
    }

    public function set location(loc:Location):void {
      this._location = loc;
      layoutComponents();
    }
    public function get location():Location {
      return this._location;
    }

    public function set imgScale(scale:Number):void {
      this._img_scale = scale;
      img.scaleX = scale;
      img.scaleY = scale;
      layoutComponents();
    }
    
    //Set the image and rescale it to the current scale.
    //That causes layout components to be called as well.
    public function set image(img:DisplayObject):void {
      this.img = img;
      //Force layout and resize to current scale.
      this.imgScale = _img_scale;
    }
    public function get image():DisplayObject {
      return img;
    }
  }
}
