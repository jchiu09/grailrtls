package flashgrail {
  import flash.utils.ByteArray;


  public class ClientAggregator {

    //Message type constant
    public static const KEEP_ALIVE:uint           = 0; //Should be used!
    public static const CERTIFICATE:uint          = 1; //Not used!
    public static const ACK_CERTIFICATE:uint      = 2; //Not used!
    public static const SOLUTION_PUBLICATION:uint = 3;
    public static const SOLUTION_REQUEST:uint     = 4;
    public static const SOLUTION_RESPONSE:uint    = 5; //Not used!
    public static const SOLUTION_SAMPLE:uint      = 6;
    public static const HISTORIC_COMPLETE:uint    = 7;

    public static function makeHandshake():ByteArray {
      var buff:ByteArray = new ByteArray();
      //The length of the handshake string
      buff.writeInt(21);
      buff.writeUTFBytes("GRAIL client protocol");
      //Version numbers
      buff.writeByte(0);
      buff.writeByte(0);
      return buff;
    }

    /*
     * Function to read a sized utf 16 string from a byte array.
     * Necessary since I cannot get actionscript's UTF16 support to work.
     */
    public static function readSizedUTF16(buff:ByteArray):String {
      var str:String = new String();
      var strlen:int = buff.readUnsignedInt();
      for (var j:uint = 0; j < strlen; ++j) {
        var s:String = buff.readUTFBytes(1);
        if (j % 2 == 1) {
          str = str + s;
        }
      }
      return str;
    }

    //TODO I am having trouble using actionscript's UTF16 support so I am doing this manually.
    private static function makeSizedUTF16(uri:String):ByteArray {
      var buff:ByteArray = new ByteArray();

      //Two bytes for each character
      buff.writeUnsignedInt(2*uri.length);
      for (var i:uint = 0; i < uri.length; ++i) {
        buff.writeByte(0);
        buff.writeByte(uri.charCodeAt(i));
      }
      return buff;
    }

    //Return the GRAIL3 types in this message
    public static function decodeTypeSpec(length:uint, buff:ByteArray):Vector.<GRAIL3Type> {
      var msg:Vector.<GRAIL3Type> = new Vector.<GRAIL3Type>();
      var total_types:uint = buff.readInt();

      for (var i:uint = 0; i < total_types; ++i) {
        var alias:uint = buff.readInt();
        //The string is in Big-Endian UTF16 so we cannot just use readUTF
        var strlen:int = buff.readUnsignedInt();
        //var name:String = buff.readMultiByte(strlen, "unicodeFFEE");
        //var name:String = buff.readMultiByte(strlen, "unicode");
        //Having a bugger of a time reading the unicode string with actionscript's
        //functions so we'll just read it byte by byte and drop every other char.
        var name:String = new String();
        for (var j:uint = 0; j < strlen; ++j) {
          var s:String = buff.readUTFBytes(1);
          if (j % 2 == 1) {
            name = name + s;
          }
        }
        msg.push(new GRAIL3Type(alias, name));
      }
      return msg;
    }

    //Make a solution request
    public static function makeSolutionRequest(request:GRAILSolutionRequest):ByteArray {
      var buff:ByteArray = new ByteArray();

      //Store the message type and data
      buff.writeByte(SOLUTION_REQUEST);

      //Write the begin and end times (convert from floating to int)
      buff.writeUnsignedInt(request.begin / Math.pow(2,32));
      buff.writeUnsignedInt(uint(request.begin) % Math.pow(2,32));
      buff.writeUnsignedInt(request.end / Math.pow(2,32));
      buff.writeUnsignedInt(uint(request.end) % Math.pow(2,32));

      //Push back the total number of type specifications in this message and
      //then push back the type alias for each desired type
      buff.writeUnsignedInt(request.aliases.length);
      for (var i:uint = 0; i < request.aliases.length; ++i) {
        buff.writeUnsignedInt(request.aliases[i]);
      }

      //Store the size of the buffer at the beginning of the message.
      var msg:ByteArray = new ByteArray();
      msg.writeUnsignedInt(buff.length);
      msg.writeBytes(buff);

      return msg;
    }

    //Return the GRAIL3 solutions in this message
    public static function decodeSolutionMsg(length:uint, buff:ByteArray, statCallback:Function):SolverData {
      var solutions:SolverData = new SolverData();

      //The total length and message type were already read from the message.

      //Get the region uri
      solutions.region = readSizedUTF16(buff);
      //Get the time (convert from a 64 bit integer to a floating point number).
      solutions.time = Math.pow(2,32)*buff.readUnsignedInt();
      solutions.time += buff.readUnsignedInt();

      //Read in the number of solutions
      var num_solutions:uint = buff.readUnsignedInt();
      //statCallback("Attempting to read in "+num_solutions+" solutions.");
      for (var i:uint = 0; i < num_solutions; ++i) {
        var solution:GRAILSolution = new GRAILSolution();
        solution.type_alias = buff.readUnsignedInt();
        solution.target = readSizedUTF16(buff);
        //Get the number of bytes in the data field and then read it in
        var data_len:uint = buff.readUnsignedInt();
        buff.readBytes(solution.data, 0, data_len);

        //Store this solution
        solutions.solutions.push(solution);
      }

      return solutions;
    }
  }
}
