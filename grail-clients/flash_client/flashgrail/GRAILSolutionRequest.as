package flashgrail {

  public class GRAILSolutionRequest {
    //Begin and end times for this request
    //If begin < end then data from time begin to time end will be sent.
    //If being == end then the most recent value will be sent and then real-time data will be sent
    //If end < begin then data from begin to the current time will be sent, and then
    //real-time data will be sent.
    public var begin:Number = 0;
    public var end:Number   = 0;

    //Solution aliases.
    public var aliases:Vector.<uint> = new Vector.<uint>();

    public function GRAILSolutionRequest() {
    }
  }
}
