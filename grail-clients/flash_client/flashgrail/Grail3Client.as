package flashgrail {

  import flash.events.*;
  import flash.net.Socket;
  import flash.utils.ByteArray;
  import flashgrail.ClientAggregator;

  public class Grail3Client extends Socket {
    //private var sock:Socket;
    private var segment_len:uint;
    //Callback when a type spec message is received with new types.
    private var typesCallback:Function;
    private var solutionsCallback:Function;
    private var statCallback:Function;

    private var host:String;
    private var port:int;
    private var handshake_handled:Boolean = false;

    /*
     * World server address as a string and port number as in integer.
     * typesCallback(types:Vector.<GRAIL3Type>) - Callback for a type spec message.
     * statCallback(status:String) - callback for updates in the connection status.
     */
    public function Grail3Client(host:String, port:int, typesCallback:Function, solutionsCallback:Function, statCallback:Function) {
      //Set up the callbacks for this socket before connecting.
      super();
      this.typesCallback = typesCallback;
      this.solutionsCallback = solutionsCallback;
      this.statCallback  = statCallback;
      this.host = host;
      this.port = port;

      //No data segments have been received yet.
      segment_len = 0;

      addEventListener(Event.CLOSE, closeHandler);
      addEventListener(Event.CONNECT, connectHandler);
      //The first piece of data should be a handshake message.
      addEventListener(ProgressEvent.SOCKET_DATA, handshakeHandler);
      //TODO Just letting the user see these errors for now.
      //addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
      //addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);

      //Now that callbacks are set up connect to the distributor.
      this.connect(host, port);
    }

    public function reconnect():void {
      //Go back to the handshake handler and connect again.
      this.close();
      removeEventListener(ProgressEvent.SOCKET_DATA, socketDataHandler);
      addEventListener(ProgressEvent.SOCKET_DATA, handshakeHandler);
      this.connect(host, port);
      handshake_handled = false;
    }

    //Send the given message if we are connected.
    public function send(buff:ByteArray):void {
      if (connected) {
        this.writeBytes(buff);
        this.flush();
      }
    }

    //Sends a handshake message when we connect to the distributor.
    private function connectHandler(connect:Event):void {
      //Send the handshake message
      //Try doing this later
      //send(ClientAggregator.makeHandshake());
    }

    private function closeHandler(close:Event):void {
      this.close();
    }

    private function handshakeHandler(progress:ProgressEvent): void {
      //This event handler could get multiple packets without having a chance
      //to turn on the regular packet handler. Make sure this only handles
      //a handshake once per connection with the handshake_handled variable.
      if (false == handshake_handled) {
        //Compare the received message to a handshake.
        statCallback("Handshake received.");
        handshake_handled = true;
        var handshake:ByteArray = new ByteArray();
        var buff:ByteArray = new ByteArray();
        this.readBytes(buff, 0, handshake.length);
        //Send our own handshake back
        send(ClientAggregator.makeHandshake());
        //TODO check the handshake here

        //Set the data handler to the normal data handler
        removeEventListener(ProgressEvent.SOCKET_DATA, handshakeHandler);
        addEventListener(ProgressEvent.SOCKET_DATA, socketDataHandler);
      }
      if (bytesAvailable > 4) {
        statCallback("More data after handshake.");
        socketDataHandler(progress);
      }
    }

    private function socketDataHandler(progress:ProgressEvent):void {
      //Check if we are starting a new segment.
      if ( 0 == segment_len && bytesAvailable >= 4) {
        segment_len = this.readUnsignedInt();
      }
      statCallback("" + bytesAvailable + " bytes available with segment size " + segment_len);
      //Keep reading data one segment at a time.
      //Multiple TCP messages may be merged so we must manually split data into segments.
      while ( 0 != segment_len &&
              bytesAvailable >= segment_len) {
        statCallback("Processing message of type "+msg_type);
        //Use the ClientAggregator module to decode the message based upon its type.
        var buff:ByteArray = new ByteArray();
        this.readBytes(buff, 0, segment_len);
        var msg_type:uint = buff.readByte();
        statCallback("Got a packet with message type "+msg_type+" of length "+segment_len+" leaving "+bytesAvailable+" bytes available");
        //Reset the segment length to 0 (do this here because an error
        //during decoding would prevent this from being set later)
        segment_len = 0;
        if ( ClientAggregator.SOLUTION_PUBLICATION == msg_type) {
          statCallback("Received solution type information.");
          var types:Vector.<GRAIL3Type> = ClientAggregator.decodeTypeSpec(segment_len - 1, buff);
          typesCallback(types);
        }
        else if ( ClientAggregator.SOLUTION_SAMPLE == msg_type) {
          statCallback("Received solution sample information.");
          var solutions:SolverData = ClientAggregator.decodeSolutionMsg(segment_len - 1, buff, statCallback);
          solutionsCallback(solutions);
        }
        else if ( ClientAggregator.HISTORIC_COMPLETE == msg_type) {
          statCallback("Historic streaming complete.");
        }
        else {
          statCallback("Unknown message type: "+msg_type);
        }
        //Check for more data.
        if (bytesAvailable >= 4) {
          segment_len = this.readUnsignedInt();
        }
      }
    }
  }
}

