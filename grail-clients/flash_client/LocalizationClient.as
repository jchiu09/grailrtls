package {
  import flash.display.Sprite;
  import flash.display.StageAlign;
  import flash.events.*;
  import flash.net.Socket;
  import flash.text.TextFormat;
  import flash.text.TextField;
  import flash.text.TextFieldType;
  import flash.display.DisplayObject;
  import flash.display.SimpleButton;
  import flash.display.StageScaleMode;
  import flash.utils.Timer;
  import flash.utils.Dictionary;
  import flash.system.Capabilities;

  //GRAIL3 Distributor stuff
  import flashgrail.Grail3Client;
  import flashgrail.GRAIL3Type;
  import flashgrail.GRAILSolution;
  import flashgrail.SolverData;
  import flashgrail.GRAILSolutionRequest;
  import flashgrail.ClientAggregator;

  //GRAIL3 World Server stuff
  import flashgrail.WorldServerProtocol;
  import flashgrail.WorldServerClient;
  import flashgrail.WorldData;
  import flashgrail.FieldData;

  //Stuff to display transmitters
  import flashgrail.EllipseLocation;
  import flashgrail.AntennaStatus;

  public class LocalizationClient extends Sprite {
    [Embed("winlab.png")] private var MapImage:Class;
    private var floormap:DisplayObject = new MapImage();

    [Embed("antenna.png")] private var AntennaImage:Class;

    //The objects seen by the localization system
    private var antennas:Dictionary = new Dictionary();
    //A dropdown box of objects so that the user can select which ones to see.
    private var target_dropdown:Dropdown;
    //Groups of transmitters - "mugs", "doors", etc - all stored as vectors of strings.
    private var groups:Dictionary = new Dictionary();

    //Mapping from object names to sensor IDs
    private var idToName:Dictionary = new Dictionary();
    private var nameToID:Dictionary = new Dictionary();

    private const map_scale:Number = 3.63;

    public var ipfield:TextField;
    public var portfield:TextField;
    public var ws_ipfield:TextField;
    public var ws_portfield:TextField;
    private var done:SimpleButton;
    private var clock:Timer = new Timer(1000);
    private var retry_timer:uint = 0;

    private var gclient:Grail3Client;
    private var world:WorldServerClient;
    private var stat:TextField;

    private var cur_action:TextField = new TextField();
    private var world_cur_action:TextField = new TextField();
    private var updated_field:TextField = new TextField();

    private var last_connected:Boolean = false;

    private var location_alias:int = -1;

    //The time of the last data update, in seconds since January 1, 1970.
    private var last_updated:Number = 0;
    private var world_update:Number = 0;

    //Constructor
    public function LocalizationClient() {
      //TODO Can't seem to get scaling to work well.
      stage.scaleMode = StageScaleMode.SHOW_ALL;
      stage.align = StageAlign.TOP_LEFT;
      //stage.scaleMode = StageScaleMode.NO_SCALE;
      //stage.scaleMode = StageScaleMode.NO_BORDER;
      //stage.stageWidth = flash.system.Capabilities.screenResolutionX;
      //stage.stageHeight = flash.system.Capabilities.screenResolutionY;
      //Make a status field
      stat = new TextField();
      stat.text = "Status: disconnected";
      stat.type = TextFieldType.DYNAMIC;
      stat.width = stat.textWidth*2;
      stat.height = stat.textHeight;
      stat.x = 0;
      stat.y = 0;
      addChild(stat);
      //Event that responds to the timer that fires once a second.
      clock.addEventListener(TimerEvent.TIMER, updateStats);

      //Set up fields for the user to fill in distributor information.
      setupStartView();

      //Add an "all" category into the dropdown.
      var v:Vector.<DisplayObject> = new Vector.<DisplayObject>();
      v.push(makeTField("All"));
      target_dropdown = new Dropdown(v);
      target_dropdown.selectedCallback = filterDisplay;

      //There is always an "All" group. Whenever a new tag is found it is added
      //to it.
      groups["All"] = new Dictionary();
    }

    private function setupStartView():void {
      //Make input fields the GRAIL distributor's ip and port
      ipfield = makeTField("Distributor Address");
      portfield = makeTField("Distributor Port");
      ws_ipfield = makeTField("World Server Address");
      ws_portfield = makeTField("World Server Port");

      portfield.type = ipfield.type = TextFieldType.INPUT;
      ws_portfield.type = ws_ipfield.type = TextFieldType.INPUT;
      portfield.multiline = ipfield.multiline = false;
      ws_portfield.multiline = ws_ipfield.multiline = false;
      ipfield.width = ipfield.textWidth * 3;
      portfield.width = portfield.textWidth * 2;
      ws_ipfield.width = ws_ipfield.textWidth * 3;
      ws_portfield.width = ws_portfield.textWidth * 2;
      ipfield.y = stat.y + 2*stat.height
      ws_ipfield.y = stat.y + 3*stat.height;
      portfield.y = ipfield.y + 2*ipfield.height;
      ws_portfield.y = ws_ipfield.y + 2*ws_ipfield.height;
      ws_portfield.x = ws_ipfield.x = ipfield.x + ipfield.width + 10;
      addChild(ipfield);
      addChild(portfield);
      addChild(ws_ipfield);
      addChild(ws_portfield);

      //Add a button for the user to click when they're done
      var donetext:TextField = new TextField();
      donetext.text = "Click when done.";
      donetext.type = TextFieldType.DYNAMIC;
      //donetext.y = portfield.y + 2*portfield.height;
      //addChild(donetext);
      done = new SimpleButton(donetext, donetext, donetext, donetext);
      done.y = portfield.y + 2*portfield.height;
      donetext.height = donetext.textHeight;
      done.addEventListener(MouseEvent.CLICK, grailDistributorConnect);
      addChild(done);

      //Set up a label that informs the user of the current action.
      cur_action.y = stat.y + stat.height;
      cur_action.autoSize = "left";
      if (cur_action.text == "") {
        cur_action.text = "Distributor: No action in progress.";
      }
      world_cur_action.x = cur_action.x + cur_action.textWidth + 5;
      world_cur_action.y = stat.y + stat.height;
      world_cur_action.autoSize = "left";
      if (world_cur_action.text == "") {
        world_cur_action.text = "World: No action in progress.";
      }
    }

    /*
     * Convenience function to make a text field.
     */
    private function makeTField(str:String):TextField {
      var tfield:TextField = new TextField();
      tfield.text = str;
      tfield.autoSize = "left";
      tfield.border   = true;
      tfield.type     = TextFieldType.DYNAMIC;
      tfield.height   = tfield.textHeight;
      return tfield;
    }

    private function clearStartView():void {
      removeChild(ipfield);
      removeChild(portfield);
      removeChild(ws_ipfield);
      removeChild(ws_portfield);
      removeChild(done);
    }

    private function setupMapView():void {
      //Display the current action information
      addChild(cur_action);
      addChild(world_cur_action);
      updated_field = makeTField("Waiting for update");
      updated_field.border = false;
      updated_field.x = cur_action.x;
      updated_field.y = cur_action.y + 1.1*cur_action.height;
      addChild(updated_field);

      floormap.y = updated_field.y + 1.1*updated_field.height;
      floormap.x = 0;
      //Scale the width
      floormap.width = stage.width;
      floormap.scaleY = floormap.scaleX;
      //If the map is still too tall scale it down more
      if (floormap.height > stage.height - floormap.y) {
        floormap.height = stage.height - floormap.y;
        floormap.scaleX = floormap.scaleY;
      }
      addChild(floormap);
      var drop_label:TextField = makeTField("Select transmitters:");
      drop_label.y = floormap.y + floormap.height + 5;
      drop_label.x = floormap.x;
      drop_label.border = false;
      target_dropdown.x = drop_label.x + drop_label.width + 5;
      target_dropdown.y = drop_label.y;
      addChild(drop_label);
      addChild(target_dropdown);
    }

    private function updateStats(event:TimerEvent):void {
      //Reconnect when the timer runs down
      if (retry_timer > 0) {
        --retry_timer;
        if (retry_timer == 0 && gclient.connected == false) {
          stat.text = "Reconnecting...";
          gclient.reconnect();
        }
      }

      if (gclient.connected) {
        stat.text = "Status: connected";
        if ( false == last_connected) {
          clearStartView();
          setupMapView();
          last_connected = true;
        }
      }
      else {
        if (last_connected) {
          //Try to reconnect in two seconds if we lost our connection.
          retry_timer = 5;
        }
        last_connected = false;
        stat.text = "Status: disconnected";
      }
      var now:Date = new Date();
      if (last_updated != 0) {
        updated_field.text = "Updated " + (now.getTime()-last_updated)/1000.0 + " seconds ago.";
      }
      //Update world data every 15 seconds
      if ( 15000 <= (now.getTime() - world_update)) {
        world_update = now.getTime();
        //Query for region.* to find out what classes of objects are available.
        world.send(WorldServerProtocol.makeURISearchMsg("winlab.*"));
      }
    }

    public function statCallback(str:String):void {
      cur_action.text = "Distributor: "+str;
      world_cur_action.x = cur_action.x + cur_action.textWidth + 5;
    }

    public function worldStatCallback(str:String):void {
      world_cur_action.text = "World: "+str;
    }

    /*
     * This function adds each type in the given vector to a table.
     */
    public function newTypesCallback(types:Vector.<GRAIL3Type>):void {
      //Request "location" data.
      var request:GRAILSolutionRequest = new GRAILSolutionRequest();
      for (var i:uint = 0; i < types.length; ++i) {
        if (types[i].name == "location") {
          location_alias = types[i].alias;
          request.aliases.push(location_alias);
        }
      }
      if (request.aliases.length > 0) {
        gclient.send(ClientAggregator.makeSolutionRequest(request));
        statCallback("Sending data request to server.");
      }
      else {
        statCallback("Location data is not available.");
      }
    }

    //Remove all of the viewable antennas
    private function removeAntennas():void {
      var idx:uint = 0;
      while (idx < numChildren) {
        var ant:AntennaStatus = getChildAt(idx) as AntennaStatus;
        if (ant != null) {
          removeChildAt(idx);
        }
        else {
          ++idx;
        }
      }
    }

    //Called when a type is selected in the dropdown
    public function filterDisplay(obj:DisplayObject):void {
      if (null == (obj as TextField)) {
        return;
      }
      var filt_name:String = (obj as TextField).text;
      //See if this is an individual antenna
      if (null != antennas[filt_name]) {
        //Remove all antennas and then display this one
        removeAntennas();
        addChild(antennas[filt_name]);
      }
      else if (null != groups[filt_name]) {
        var dict_filter:Dictionary = groups[filt_name];
        //Remove all antennas and then add all of the antennas from this group.
        removeAntennas();
        for each (var ant_stat:AntennaStatus in dict_filter) {
          addChild(ant_stat);
        }
      }
    }

    /*
     * This function updates the locations of transmitters as their data arrives.
     */
    public function newSolutionsCallback(solutions:SolverData):void {
      statCallback("Receiving solution data.");
      last_updated = (new Date()).getTime();
      for (var i:uint = 0; i < solutions.solutions.length; ++i) {
        var soln:GRAILSolution = solutions.solutions[i];
        //GRAIL solutions are in seconds so multiply to get milliseconds.
        if (location_alias == soln.type_alias) {
          //If this antenna is not currently being displayed then
          var eloc:EllipseLocation = new EllipseLocation(soln.data);
          var astat:AntennaStatus;
          if (null == antennas[soln.target]) {
            var aimg:DisplayObject = new AntennaImage();
            astat = new AntennaStatus(aimg, eloc, soln.target);
            astat.imgScale = 0.5;
            astat.scaleX = 0.4;
            astat.scaleY = 0.4;
            antennas[soln.target] = astat;
            addChild(antennas[soln.target]);
            //Push the new target into the dropdown
            target_dropdown.objects.push(makeTField(soln.target));
          }
          else {
            if (contains(antennas[soln.target])) {
                removeChild(antennas[soln.target]);
            }
            var aimg2:DisplayObject = new AntennaImage();
            astat = new AntennaStatus(aimg2, eloc, soln.target);
            astat.imgScale = 0.5;
            astat.scaleX = 0.4;
            astat.scaleY = 0.4;
            astat.setDetails(antennas[soln.target].getDetails());
            antennas[soln.target] = astat;
            addChild(antennas[soln.target]);
          }
          astat.x = floormap.x + floormap.scaleX * map_scale * astat.location.x;
          astat.y = floormap.y + floormap.height - floormap.scaleY * map_scale * astat.location.y;
          //Update the "All" group.
          groups["All"][soln.target] = astat;
          //Check to see if this target is in any other known groups and update them
          if (null != idToName[soln.target]) {
            var full_name:String = idToName[soln.target];
            var group_name:String = full_name.split(".")[0];
            if (null != groups[group_name]) {
              astat.addDetail("Name:", full_name);
              groups[group_name][full_name] = astat;
            }
          }
        }
      }
      //Call filter display to make sure we are displaying the correct antennas
      filterDisplay(target_dropdown.selected);
      statCallback("New results displayed.");
    }

    public function queryCallback(wd:WorldData):void {
      //Fill in idToName and nameToID information
      var pieces:Array = wd.object_uri.split(".");
      if (pieces.length == 3) {
        var tx_group:String = pieces[1];
        var tx_id:String = pieces[2];
        var obj_name:String = tx_group + "." + tx_id;
        for each (var field:FieldData in wd.fields) {
          //Check if this is a transmitter ID
          //TODO should be a better of doing this than comparing the data type
          if (field.data_type == "uint64_t") {
            var numericID:String = new String();
            //Discard the upper 32 bits
            field.data.readUnsignedInt();
            numericID = field.data.readUnsignedInt().toString();
            //Fill in idToName and nameToID information.
            idToName[numericID] = obj_name;
            nameToID[obj_name] = numericID;
            //Add this if we need to
            if (null != groups[tx_group] &&
                null == groups[tx_group][obj_name] &&
                null != antennas[numericID]) {
              //Add this to the group list so dropdown group selection works.
              groups[tx_group][obj_name] = antennas[numericID];
              antennas[numericID].addDetail("Name:", obj_name);
            }
          }
        }
      }
    }

    public function uriSearchCallback(uris:Vector.<String>):void {
      //See if this group class exists in the dropdown.
      worldStatCallback("Updating transmitter groups.");
      for each (var uri:String in uris) {
        var pieces:Array = uri.split(".");
        if (pieces.length == 3) {
          var tx_group:String = pieces[1];
          var tx_id:String = pieces[2];
          var obj_name:String = tx_group + "." + tx_id;
          //See if we need to add this group.
          if (null == groups[tx_group]) {
            groups[tx_group] = new Dictionary();
            target_dropdown.objects.push(makeTField(tx_group));
          }
          //If this id is not in the group list but it is a valid
          //antenna id add it in
          //(This won't work if this is a name (ie, winlab.mugs.Ben) rather than an id
          if (null == groups[tx_group][obj_name] &&
              null != antennas[tx_id]) {
            //Add this to the group list so dropdown group selection works.
            groups[tx_group][obj_name] = antennas[tx_id];
            antennas[tx_id].addDetail("Name:", obj_name);
            //Fill in idToName and nameToID information.
            idToName[tx_id] = obj_name;
            nameToID[obj_name] = tx_id;
          }

          //If this ID could not be matched to an antenna because it lacks
          //a name to antenna lookup then look one up.
          if (null == groups[tx_group][tx_id] &&
              null == nameToID[obj_name]) {
            world.send(WorldServerProtocol.makeObjectDataQueryMsg(uri));
          }
        }
      }
    }

    public function typeSelected(e:Event):void {
      //cur_action.text = cb.selectedItem.label;
    }

    /*
     * Connect to the GRAIL3 distributo when the button is clicked.
     */
    public function grailDistributorConnect(event:MouseEvent):void {
      gclient = new Grail3Client(ipfield.text, int(portfield.text), newTypesCallback, newSolutionsCallback, statCallback);
      world = new WorldServerClient(ws_ipfield.text, int(ws_portfield.text), queryCallback, uriSearchCallback, worldStatCallback);
      clock.start();
    }
  }
}
