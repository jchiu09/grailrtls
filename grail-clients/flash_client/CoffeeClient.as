package {
  import flash.display.Sprite;
  import flash.events.*;
  import flash.net.Socket;
  import flash.text.TextFormat;
  import flash.text.TextField;
  import flash.text.TextFieldType;
  import flash.display.DisplayObject;
  import flash.display.SimpleButton;
  import flash.utils.Timer;

  import flashgrail.Grail3Client;
  import flashgrail.GRAIL3Type;
  import flashgrail.GRAILSolution;
  import flashgrail.SolverData;
  import flashgrail.GRAILSolutionRequest;
  import flashgrail.ClientAggregator;

  public class CoffeeClient extends Sprite {

    private var ipfield:TextField;
    private var portfield:TextField;
    private var done:SimpleButton;
    private var clock:Timer = new Timer(1000);
    private var retry_timer:uint = 0;

    private var gclient:Grail3Client;
    private var stat:TextField;

    private var cur_action:TextField = new TextField();

    private var last_connected:Boolean = false;

    private var fresh_alias:int = -1;
    private var brew_alias:int = -1;
    private var coffee_status:TextField = new TextField();
    private var time_status:TextField = new TextField();
    private var fresh_time:Number = 0;
    private var brew_time:Number = 0;

    //Constructor
    public function CoffeeClient() {
      //Make a status field
      stat = new TextField();
      stat.text = "Status: disconnected";
      stat.type = TextFieldType.DYNAMIC;
      stat.width = stat.textWidth*2;
      stat.height = stat.textHeight;
      stat.x = 0;
      stat.y = 0;
      addChild(stat);
      //Event that responds to the timer that fires once a second.
      clock.addEventListener(TimerEvent.TIMER, updateStats);

      //Set up fields for the user to fill in distributor information.
      setupStartView();
    }

    private function setupStartView():void {
      //Make input fields the GRAIL distributor's ip and port
      ipfield = new TextField();
      portfield = new TextField();
      ipfield.text = "Host Address";
      portfield.text = "Host Port";

      portfield.type = ipfield.type = TextFieldType.INPUT;
      portfield.border = ipfield.border = true;
      portfield.multiline = ipfield.multiline = false;
      portfield.height = ipfield.height = ipfield.textHeight;
      ipfield.width = ipfield.textWidth * 3;
      portfield.width = portfield.textWidth * 2;
      ipfield.y = stat.y + 2*stat.height
      addChild(ipfield);
      portfield.y = ipfield.y + 2*ipfield.height;
      addChild(portfield);

      //Add a button for the user to click when they're done
      var donetext:TextField = new TextField();
      donetext.text = "Click when done.";
      donetext.type = TextFieldType.DYNAMIC;
      //donetext.y = portfield.y + 2*portfield.height;
      //addChild(donetext);
      done = new SimpleButton(donetext, donetext, donetext, donetext);
      done.y = portfield.y + 2*portfield.height;
      donetext.height = donetext.textHeight;
      done.addEventListener(MouseEvent.CLICK, grailDistributorConnect);
      addChild(done);

      //Set up a label that informs the user of the current action.
      cur_action.y = stat.y + stat.height;
      cur_action.autoSize = "left";
      if (cur_action.text == "") {
        cur_action.text = "No action in progress.";
      }
    }

    /*
     * Convenience function to make a text field.
     */
    private function makeTField(str:String):TextField {
      var tfield:TextField = new TextField();
      tfield.text = str;
      tfield.autoSize = "left";
      tfield.border   = true;
      tfield.type     = TextFieldType.DYNAMIC;
      return tfield;
    }

    private function clearStartView():void {
      removeChild(ipfield);
      removeChild(portfield);
      removeChild(done);
    }

    private function setupCoffeeView():void {
      //Display the current action information
      addChild(cur_action);

      var now:Date = new Date();
      time_status = makeTField("The current time is " + now.toString());
      time_status.y = cur_action.y + 2*cur_action.height;
      addChild(time_status);
      coffee_status = makeTField("Waiting for coffee status.");
      coffee_status.y = time_status.y + 2*time_status.height;
      addChild(coffee_status);
      //No borders on these.
      time_status.border = coffee_status.border = false;
    }

    private function updateStats(event:TimerEvent):void {
      //Reconnect when the timer runs down
      if (retry_timer > 0) {
        --retry_timer;
        if (retry_timer == 0 && gclient.connected == false) {
          stat.text = "Reconnecting...";
          gclient.reconnect();
        }
      }

      if (gclient.connected) {
        stat.text = "Status: connected";
        if ( false == last_connected) {
          clearStartView();
          setupCoffeeView();
          last_connected = true;
        }
      }
      else {
        if (last_connected) {
          //Try to reconnect in two seconds if we lost our connection.
          retry_timer = 5;
        }
        last_connected = false;
        stat.text = "Status: disconnected";
      }

      //Set the coffee status
      var five_minutes:Number = 1000*60*5;
      var fifteen_minutes:Number = 1000*60*15;
      var one_hour:Number = 1000*60*60;

      var btime:Date = new Date();
      btime.setTime(brew_time);

      var ftime:Date = new Date();
      ftime.setTime(fresh_time);

      var now:Date = new Date();

      var time_diff:Number = now.getTime() - brew_time;

      time_status.text = "The current time is " + now.toString();
      //Determine the coffee status by hold old it is.
      if (brew_time == 0) {
        coffee_status.text = "Coffee data is not currently available.";
      }
      else if (time_diff < five_minutes) {
        coffee_status.text = "Coffee is brewing. (started on " + btime.toString() +")";
      }
      else if (time_diff < fifteen_minutes) {
        coffee_status.text = "Coffee is fresh. (brewed on " + btime.toString() + ")";
      }
      else if (time_diff < one_hour) {
        coffee_status.text = "Coffee is meh. (brewed on " + btime.toString() + ")";
      }
      else {
        coffee_status.text = "Coffee is old. (brewed on " + btime.toString() + ")";
      }
      /*
      else if (brew_time > fresh_time) {
        coffee_status.text = "Coffee is brewing. (started on " + btime.toString() +")";
      }
      else if (fifteen_minutes < now.getTime() - fresh_time) {
        coffee_status.text = "Coffee is fresh. (brewed on " + ftime.toString() + ")";
      }
      else if (one_hour < now.getTime() - fresh_time) {
        coffee_status.text = "Coffee is meh. (brewed on " + ftime.toString() + ")";
      }
      else {
        coffee_status.text = "Coffee is old. (brewed on " + ftime.toString() + ")";
      }
      */
    }

    public function statCallback(str:String):void {
      cur_action.text = str;
    }

    /*
     * This function adds each type in the given vector to a table.
     */
    public function newTypesCallback(types:Vector.<GRAIL3Type>):void {
      //Request "coffee brewing" and "fresh coffee" data.
      var request:GRAILSolutionRequest = new GRAILSolutionRequest();
      //Request anything in the last three days so that we immediately grab the current status.
      //Divide by zero to convert from milliseconds to seconds for the grail server.
      var now:Date = new Date();
      request.begin = (now.getTime() - 1000*60*60*72)/1000.0;
      for (var i:uint = 0; i < types.length; ++i) {
        if (types[i].name == "coffee brewing") {
          brew_alias = types[i].alias;
          request.aliases.push(brew_alias);
        }
        if (types[i].name == "fresh coffee") {
          fresh_alias = types[i].alias;
          request.aliases.push(fresh_alias);
        }
      }
      if (request.aliases.length > 0) {
        gclient.send(ClientAggregator.makeSolutionRequest(request));
        cur_action.text = "Sending data request to server.";
      }
      else {
        cur_action.text = "Coffee data is not available.";
      }
    }

    /*
     * This function displays coffee status as it comes in.
     */
    public function newSolutionsCallback(solutions:SolverData):void {
      cur_action.text = "Receiving solution data.";
      for (var i:uint = 0; i < solutions.solutions.length; ++i) {
        var soln:GRAILSolution = solutions.solutions[i];
        //GRAIL solutions are in seconds so multiply to get milliseconds.
        if (fresh_alias == soln.type_alias) {
          fresh_time = 1000.0 * solutions.time;
        }
        if (brew_alias == soln.type_alias) {
          brew_time = 1000.0 * solutions.time;
        }
      }
    }

    public function typeSelected(e:Event):void {
      //cur_action.text = cb.selectedItem.label;
    }

    /*
     * Connect to the GRAIL3 distributo when the button is clicked.
     */
    public function grailDistributorConnect(event:MouseEvent):void {
      gclient = new Grail3Client(ipfield.text, int(portfield.text), newTypesCallback, newSolutionsCallback, statCallback);
      clock.start();
    }
  }
}
