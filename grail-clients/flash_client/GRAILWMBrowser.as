import flash.events.*;
import flash.utils.Timer;
import flash.utils.ByteArray;
import mx.collections.ArrayCollection;

import flashgrail.ClientWMClient;
import flashgrail.SolverWMClient;
import flashgrail.ClientWorldModel;
import flashgrail.SolverWorldModel;
import flashgrail.TypeLookup;
import flashgrail.WMData;
import flashgrail.RawSolution;
import flashgrail.WorldModelRequest;

import mx.collections.ArrayList;

//For dataGridEvent
import mx.events.*;

private var clock:Timer = new Timer(1000);

private var client_world:ClientWMClient;
private var solver_world:SolverWMClient;

//Row where user inputs new field information
private var lastrow:Row;

private var last_connected:Boolean = false;

private var client_callbacks:Object = {URI_RESPONSE:uriSearchCallback,
                                       DATA_RESPONSE:dataCallback};

private var changed:ArrayList = new ArrayList();

[Bindable] public var uris:ArrayList = new ArrayList();
[Bindable] public var fields:ArrayList = new ArrayList();
[Bindable] public var tree_data:ArrayCollection = new ArrayCollection();

private function sendNewFields(event:MouseEvent):void {
  var raw_solutions:Vector.<RawSolution> = new Vector.<RawSolution>();
  solverStatCallback("Processing "+changed.length+" changed attributes");
  //Update changed and created attributes
  for (var i:uint = 0; i < changed.length; ++i) {
    solverStatCallback("On attr "+i);
    var obj:Object = changed.getItemAt(i) as Object;
    solverStatCallback("Processing attr named "+obj.Attribute_Name);
    var rs:RawSolution = new RawSolution();
    rs.type = obj.Attribute_Name;
    rs.target = obj.parent_uri;
    rs.time = obj.Created;
    try {
      rs.data = TypeLookup.typeFromString(rs.type, obj.Data);
    }
    catch (err:Error) {
      solverStatCallback("Error understanding data field");
    }
    raw_solutions.push(rs);
  }
  //Hide the submit fields button again
  change_fields.enabled = false;
  changed.removeAll();

  //If this is just to add or update data send a new solution message
  //TODO Add support for expire and delete messages
  solver_world.sendSolutions(raw_solutions, false);
  solverStatCallback("Sending new URI attribute data to world server.");
}

private function deleteSelected(event:MouseEvent):void {
  var edit_obj:Object = tree_view.selectedItem;
  //If nothing is selected or this is just a containing folder then don't
  //try to delete anything.
  if (edit_obj != null) {
    //Delete an object URI
    if (null != edit_obj.is_object) {
      solver_world.send(SolverWorldModel.makeDeleteURI(edit_obj.full_uri, origin_field.text));
      solverStatCallback("Deleting URI "+edit_obj.full_uri);
      //Remove this from the tree_data list by refreshing
      //TODO FIXME The field may have changed but was not clicked so we should store the previous query.
      getURIData(query_field.text)
    }
    //Delete a URI attribute
    else if (null != edit_obj.parent_uri) {
      solver_world.send(SolverWorldModel.makeDeleteAttribute(edit_obj.parent_uri, edit_obj.Attribute_Name, origin_field.text));
      solverStatCallback("Deleting attribute "+edit_obj.Attribute_name+" of URI "+edit_obj.parent_uri);
      //Remove this from the tree_data list by refreshing
      //TODO FIXME The field may have changed but was not clicked so we should store the previous query.
      getURIData(query_field.text)
    }
  }
}

private function getNewURI(event:MouseEvent):void {
  submit.label = "Submit new URI";
  search_form_label.label = "Enter new URI:";
}

private function submitURIQuery(event:MouseEvent):void {
  switch (event.currentTarget.label) {
    case "Search":
      clientStatCallback("Searching URI \""+query_field.text+"\"");
      client_world.send(ClientWorldModel.makeURISearch(query_field.text));
      break;
    case "Submit new URI":
      //Send the URI and then go back to the control view.
      solverStatCallback("Sending new URI to world server.");
      //The uri is in query_field.text
      solver_world.send(SolverWorldModel.makeCreateURI(query_field.text, new Date().getTime(), origin_field.text));
      submit.label = "Search";
      search_form_label.label = "Enter URI search:";
  }
  clientStatCallback("target is "+event.currentTarget.label);
}

private function createNewURI(event:MouseEvent):void {
  //Send the URI to the world model if there is a URI to send.
  if (new_uri.text != "") {
    solverStatCallback("Sending new URI to world server.");
    //The uri is in query_field.text
    solver_world.send(SolverWorldModel.makeCreateURI(new_uri.text, new Date().getTime(), origin_field.text));
    new_uri.text = "";
  }
}

private function getURIData(uri:String):void {
  clientStatCallback("Getting data for URI \""+uri+"\"");
  var request:WorldModelRequest = new WorldModelRequest();
  request.object_uri = uri;
  //Get all attributes
  request.attribs.push(".*");
  //Clear previous results before these ones show up.
  tree_data.removeAll();
  //Don't specify any attributes - fetch them all
  //Don't specify a time interval - get the current snapshot.
  client_world.send(ClientWorldModel.makeSnapshotRequest(request, 1));
}

private function clearStartView():void {
  uilist.removeElement(connect_form);
}

private function setupControlView():void {
  //Make everything visible
  for (var el:uint = 0; el < uilist.numElements; ++el) {
    uilist.getElementAt(el).visible = true;
    uilist.getElementAt(el).includeInLayout = true;
  }
}

private function updateStats(event:TimerEvent):void {
  if (solver_world.connected && client_world.connected) {
    stat.text = "Status: connected";
    if ( false == last_connected) {
      clearStartView();
      setupControlView();
      last_connected = true;
    }
  }
  else {
    last_connected = false;
    stat.text = "Status: disconnected";
  }
}

public function solverStatCallback(str:String):void {
  solver_cur_action.text = "Solver WM: "+str;
}

public function clientStatCallback(str:String):void {
  client_cur_action.text = "Client WM: "+str;
}

public function editTreeFieldData(evt:AdvancedDataGridEvent):void {
  var edit_obj:Object = tree_view.selectedItem;
  if (edit_obj != null) {
    //If this is just a containing folder then don't allow editing.
    if (null == edit_obj.parent_uri) {
      evt.preventDefault();
    }
    else if (edit_obj.addattribute != null) {
      solverStatCallback("Adding attribute to "+edit_obj.parent_uri);
      //User wants to add a new field
      //Prevent editing
      evt.preventDefault();
      var new_obj:Object = new Object();
      new_obj.Object_URI = edit_obj.parent_uri;
      new_obj.Attribute_Name = "<insert name>";
      new_obj.Created = String((new Date().getTime()));
      new_obj.Expires = "0";
      new_obj.Origin = origin_field.text;
      new_obj.Data = "<insert data>";
      //Remember if this row has been changed and if what the full parent URI is
      new_obj.changed = false;
      new_obj.parent_uri = edit_obj.parent_uri;
      //Add this new object in front of the addattribute row.
      var location:Array = addURIToTreeView(edit_obj.parent_uri).branches;
      var add_attrib:Object = location.pop();
      location.push(new_obj);
      location.push(add_attrib);
      tree_view.selectedItem = new_obj;
      tree_view.dataProvider.refresh();
    }
  }
}

public function endTreeEditField(evt:AdvancedDataGridEvent):void {
  var edit_obj:Object = tree_view.selectedItem;
  //Make sure this is a field that is editable.
  if (edit_obj != null &&
      null != edit_obj.parent_uri &&
      edit_obj.addattribute == null) {
    var fieldname:String = evt.dataField;
    //Compare the new value to the old value and see if it changed.
    var field_val:String = tree_view.selectedItem[evt.dataField] as String;
    var new_val:String = evt.currentTarget.itemEditorInstance.text;
    //If a change occured show the change, update the time, and make the
    //commit button visible.
    if (field_val != new_val) {
      edit_obj.changed = true;
      //Change the created time unless that was what was edited
      if ("Created" != fieldname) {
        edit_obj.Created = String(new Date().getTime());
      }
      //Make sure each item can only end up in the list once.
      changed.removeItem(edit_obj);
      changed.addItem(edit_obj);
      //Make the change fields button visible
      change_fields.enabled = true;
    }
  }
}

/*
 * This is called when a search response returns from the world server.
 * This function adds the uris to a table.
 */
private function addURI(uri:String, idx:int, vect:Vector.<String>):void {
  var u:Object = {"World Model URI":uri};
  uris.addItem(u);
  //TODO Add a back button.
}

public function objectFilter(arr:Array, str:String):Object {
  //return arr.filter(function (arr:Array, index:int, source:Array):Boolean {return arr.Object_URI == str;}, this);
  for (var idx:uint = 0; idx < arr.length; ++idx) {
    if (arr[idx].Object_URI == str) {
      return arr[idx];
    }
  }
  return null;
}

//Add this URI to the tree view and return the object representing that node
//in the tree.
private function addURIToTreeView(uri:String):Object {
  var new_pieces:Array = uri.split(".");
  //Find the proper starting point
  //var here:Array = tree_data.source.filter(function (arr:Array):Boolean {return arr.Object_URI == new_pieces[0];});
  //var here:Array = objectFilter(tree_data.source);
  var here:Object = null;
  for (var idx:uint = 0; idx < tree_data.length; ++idx) {
    if (tree_data.getItemAt(idx).Object_URI == new_pieces[0]) {
      here = tree_data.getItemAt(idx);
      break;
    }
  }
  //New base URI? Add it into the tree view
  if (here == null) {
    var new_obj:Object = {Object_URI : new_pieces[0], branches : new Array()};
    tree_data.addItem(new_obj);
    //here = tree_data.getItemAt(tree_data.length-1);
    here = new_obj;
  }
  //Now go down the tree
  for (var part:uint = 1; part < new_pieces.length; ++part) {
    var next:Object = objectFilter(here.branches, new_pieces[part]);
    //Do we need a different branch? If so make one under this object URI.
    if (next == null) {
      var new_branch:Object = {Object_URI : new_pieces[part], branches : new Array()};
      here.branches.push(new_branch);
      here = new_branch;
    }
    else {
      here = next;
    }
  }
  //Return the terminating object.
  return here;
}

//TODO FIXME This is redundant with the addURIToTreeView function
private function addURIsToTreeView(found_uris:Vector.<String>):void {
  //Add these uris to the tree data.
  //First sort them, then combine URIs with similar bases.
  found_uris.sort(function compare(a:String, b:String):Number {return a.localeCompare(b)});
  var prev:String = "";
  var prev_pieces:Array = new Array();
  //Go to each URI and add it (and its path) into the tree view.
  for (var idx:uint = 0; idx < found_uris.length; ++idx) {
    var new_pieces:Array = found_uris[idx].split(".");
    //New base URI? Add into the tree view
    if (0 == prev_pieces.length || prev_pieces[0] != new_pieces[0]) {
      tree_data.addItem({Object_URI : new_pieces[0], branches : new Array()});
    }
    var here:Array = tree_data.getItemAt(tree_data.length-1).branches;
    //Now go down the tree
    for (var part:uint = 1; part < new_pieces.length; ++part) {
      //Do we need a different branch? If so make one under this object URI.
      if (part >= prev_pieces.length || new_pieces[part] != prev_pieces[part]) {
        here.push({Object_URI : new_pieces[part], branches : new Array()});
      }
      here = here[here.length - 1].branches;
    }
  }
}

/*
 * Display the field data for a queried URI.
 */
public function dataCallback(wd:WMData):void {
  clientStatCallback("Showing data query results");

  var here:Object = addURIToTreeView(wd.object_uri);

  //Mark this object as a terminating URI
  here.full_uri = wd.object_uri;
  here.is_object = true;

  //Make rows for the data
  for (var J:uint = 0; J < wd.attribs.length; ++J) {
    var desc:String = wd.attribs[J].name;
    var create:String = String(wd.attribs[J].creation_date);
    var expire:String = String(wd.attribs[J].expiration_date);
    var origin:String = wd.attribs[J].origin;
    //Convert the raw data into a readable format.
    //The representation is determined by the attribute name.
    var data:ByteArray = wd.attribs[J].data;
    //Convert the data to a human readable string representation
    var hr_string:String = "";
    try {
      hr_string = TypeLookup.typeToString(desc, data);
    }
    catch (err:Error) {
      solverStatCallback("Error reading data");
      hr_string = "<error interpreting data>";
    }
    
    //Set the selected type to the actual type
    var new_obj:Object = new Object();
    new_obj.Object_URI = wd.object_uri;
    new_obj.Attribute_Name = desc;
    new_obj.Created = create;
    new_obj.Expires = expire;
    new_obj.Origin = origin;
    new_obj.Data = hr_string;
    //Remember if this row has been changed and if what the full parent URI is
    new_obj.changed = false;
    new_obj.parent_uri = wd.object_uri;
    here.branches.push(new_obj);
  }
  //Add an "addattribute" button.
  var add_attrib:Object = new Object();
  add_attrib.Object_URI = wd.object_uri;
  add_attrib.Attribute_Name = "Add New Attribute";
  //TODO FIXME Need to add an item renderer to display either text or a button in this column
  //add_attrib.Attribute_Name = makeTButton("Add New Attribute");
  //Remember if this row has been changed and if what the full parent URI is
  add_attrib.changed = false;
  add_attrib.parent_uri = wd.object_uri;
  add_attrib.addattribute = true;
  //Add the button to the end
  here.branches.push(add_attrib);

  tree_view.dataProvider.refresh();
}

/*
 * This function adds each type in the given vector to a table.
 */
public function uriSearchCallback(found_uris:Vector.<String>):void {
  clientStatCallback("Displaying found URIs");
  //Clear the current uri list contents
  tree_data.removeAll();
  //Set up the tree view
  tree_view.visible = true;
  tree_view.includeInLayout = true;
  addURIsToTreeView(found_uris);
  tree_view.dataProvider.refresh();
}

public function typeSelected(e:Event):void {
  //cur_action.text = cb.selectedItem.label;
}

/*
 * Connect to the GRAIL3 world server when the button is clicked.
 */
public function grailWMConnect():void {
  stat.text = "Status: connecting to ip "+ipfield.text+" on solver port "+int(solver_portfield.text)+" and client port "+int(client_portfield.text);
  solver_world = new SolverWMClient(ipfield.text, int(solver_portfield.text), solverStatCallback, origin_field.text);
  client_world = new ClientWMClient(ipfield.text, int(client_portfield.text), client_callbacks, clientStatCallback);
  //Event that responds to the timer that fires once a second.
  clock.addEventListener(TimerEvent.TIMER, updateStats);
  clock.start();
}
