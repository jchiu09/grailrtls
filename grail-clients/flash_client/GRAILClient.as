package {
  import flash.display.Sprite;
  import flash.events.*;
  import flash.net.Socket;
  import flash.text.TextFormat;
  import flash.text.TextField;
  import flash.text.TextFieldType;
  import flash.display.DisplayObject;
  import flash.display.SimpleButton;
  import flash.utils.Timer;

  import flashgrail.Grail3Client;
  import flashgrail.GRAIL3Type;
  import flashgrail.GRAILSolution;

  public class GRAILClient extends Sprite {

    private var ipfield:TextField;
    private var portfield:TextField;
    private var done:SimpleButton;
    private var clock:Timer = new Timer(1000);

    private var gclient:Grail3Client;
    private var stat:TextField;

    private var type_grid:Column;
    private var cur_action:TextField = new TextField();

    private var last_connected:Boolean = false;

    //Constructor
    public function GRAILClient() {
      //Make a status field
      stat = new TextField();
      stat.text = "Status: disconnected";
      stat.type = TextFieldType.DYNAMIC;
      stat.width = stat.textWidth*2;
      stat.height = stat.textHeight;
      stat.x = 0;
      stat.y = 0;
      addChild(stat);
      //Event that responds to the timer that fires once a second.
      clock.addEventListener(TimerEvent.TIMER, updateStats);

      //Set up fields for the user to fill in distributor information.
      setupStartView();
    }

    private function setupStartView():void {
      //Make input fields the GRAIL distributor's ip and port
      ipfield = new TextField();
      portfield = new TextField();
      ipfield.text = "Host Address";
      portfield.text = "Host Port";

      portfield.type = ipfield.type = TextFieldType.INPUT;
      portfield.border = ipfield.border = true;
      portfield.multiline = ipfield.multiline = false;
      portfield.height = ipfield.height = ipfield.textHeight;
      ipfield.width = ipfield.textWidth * 3;
      portfield.width = portfield.textWidth * 2;
      ipfield.y = stat.y + 2*stat.height
      addChild(ipfield);
      portfield.y = ipfield.y + 2*ipfield.height;
      addChild(portfield);

      //Add a button for the user to click when they're done
      var donetext:TextField = new TextField();
      donetext.text = "Click when done.";
      donetext.type = TextFieldType.DYNAMIC;
      //donetext.y = portfield.y + 2*portfield.height;
      //addChild(donetext);
      done = new SimpleButton(donetext, donetext, donetext, donetext);
      done.y = portfield.y + 2*portfield.height;
      donetext.height = donetext.textHeight;
      done.addEventListener(MouseEvent.CLICK, grailDistributorConnect);
      addChild(done);

      //Set up a label that informs the user of the current action.
      cur_action.y = stat.y + stat.height;
      cur_action.autoSize = "left";
      if (cur_action.text == "") {
        cur_action.text = "No action in progress.";
      }

      //Make a table to store type information
      var label1:TextField = new TextField();
      var label2:TextField = new TextField();
      label1.text = "Type Name";
      label2.text = "Type Alias";
      label1.autoSize = label2.autoSize = "left";
      label1.border = label2.border = true;
      label1.type = label2.type = TextFieldType.DYNAMIC;

      type_grid = new Column(label1.height);
      type_grid.y = cur_action.y + 2*cur_action.height;
      
      var row:Row = new Row(uint(label1.width));
      row.addChild(label1);
      row.addChild(label2);
      type_grid.addChild(row); 
    }

    private function clearStartView():void {
      removeChild(ipfield);
      removeChild(portfield);
      removeChild(done);
    }

    private function setupControlView():void {
      //Display the current action information
      addChild(cur_action);

      //Display the tabel of type information
      addChild(type_grid);
    }

    private function updateStats(event:TimerEvent):void {
      if (gclient.connected) {
        stat.text = "Status: connected";
        if ( false == last_connected) {
          clearStartView();
          setupControlView();
          last_connected = true;
        }
      }
      else {
        last_connected = false;
        stat.text = "Status: disconnected";
      }
    }

    public function statCallback(str:String):void {
      cur_action.text = str;
    }

    /*
     * This is called when new types are offerend by the distributor.
     * This function adds the offered type to a table.
     */
    private function addType(t:GRAIL3Type, idx:int, vect:Vector.<GRAIL3Type>):void {
      var nametext:TextField = new TextField();
      nametext.text = t.name;
      var al_text:TextField = new TextField();
      al_text.text = String(t.alias);
      nametext.autoSize = al_text.autoSize = "left";
      nametext.border = al_text.border = true;

      var row:Row = new Row(uint(nametext.width));

      //Equalize spacing in the grid.
      type_grid.forEach( function (o:DisplayObject, i:uint):void {
        var r:Row = o as Row;
        if ( r.spacing < row.spacing) {
          r.spacing = row.spacing;
        }
        else {
          row.spacing = r.spacing;
        }
      });

      row.addChild(nametext);
      row.addChild(al_text);
      type_grid.addChild(row); 
    }

    /*
     * This function adds each type in the given vector to a table.
     */
    public function newTypesCallback(types:Vector.<GRAIL3Type>):void {
      types.forEach(addType);
    }

    /*
     * This function displays coffee status as it comes in.
     */
    public function newSolutionsCallback(solutions:GRAILSolution):void {
    }

    public function typeSelected(e:Event):void {
      //cur_action.text = cb.selectedItem.label;
    }

    /*
     * Connect to the GRAIL3 distributo when the button is clicked.
     */
    public function grailDistributorConnect(event:MouseEvent):void {
      gclient = new Grail3Client(ipfield.text, int(portfield.text), newTypesCallback, newSolutionsCallback, statCallback);
      clock.start();
    }
  }
}
