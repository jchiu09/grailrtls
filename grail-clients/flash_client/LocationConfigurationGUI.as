package {
  import flash.display.Sprite;
  import flash.events.*;
  import flash.text.TextFormat;
  import flash.text.TextField;
  import flash.text.TextFieldType;
  import flash.display.DisplayObject;
  import flash.display.SimpleButton;
  import flash.utils.Timer;
  import flash.utils.ByteArray;

  import flashgrail.WorldServerProtocol;
  import flashgrail.WorldServerClient;
  import flashgrail.WorldData;
  import flashgrail.FieldData;

  public class LocationConfigurationGUI extends Sprite {
    [Embed("winlab.png")] private var MapImage:Class;
    private var floormap:DisplayObject = new MapImage();
    private const map_scale:Number = 3.63;

    private var ipfield:TextField;
    private var portfield:TextField;
    private var done:SimpleButton;
    private var submit:SimpleButton;
    private var insert_button:SimpleButton;
    private var clock:Timer = new Timer(1000);

    private var xfield:TextField;
    private var yfield:TextField;
    private var namefield:TextField;
    private var idfield:TextField;

    private var world:WorldServerClient;
    private var stat:TextField;

    //Current status, displayed at the top of the screen.
    private var cur_action:TextField = new TextField();

    private var last_connected:Boolean = false;

    //Constructor
    public function LocationConfigurationGUI() {
      //Make a status field
      stat = new TextField();
      stat.text = "Status: disconnected";
      stat.type = TextFieldType.DYNAMIC;
      stat.width = stat.textWidth*2;
      stat.height = stat.textHeight;
      stat.x = 0;
      stat.y = 0;
      addChild(stat);
      //Event that responds to the timer that fires once a second.
      clock.addEventListener(TimerEvent.TIMER, updateStats);

      //Set up fields for the user to fill in world server information.
      setupStartView();
    }

    private function sendNewURI(event:MouseEvent):void {
      //Send the URI using the values in the fields.
      //Need to input x location, y location, id, and region.
      var wd:WorldData = new WorldData;
      var now:Date = new Date();
      wd.object_uri = namefield.text;
      var fdx:FieldData = new FieldData();
      var fdy:FieldData = new FieldData();
      var fdregion:FieldData = new FieldData();
      var fdid:FieldData = new FieldData();
      fdx.description = "location.x";
      fdy.description = "location.y";
      fdregion.description = "region_uri";
      fdid.description = "id";
      fdx.creation_date = fdy.creation_date = fdregion.creation_date = fdid.creation_date = uint(now.getTime()/1000.0);
      fdx.expiration_date = fdy.expiration_date = fdregion.expiration_date = fdid.expiration_date = 0;
      fdx.data_type = fdy.data_type = "double";
      fdregion.data_type = "string";
      fdid.data_type = "uint64_t";
      fdx.data.writeDouble(Number(xfield.text));
      fdy.data.writeDouble(Number(yfield.text));
      fdregion.data.writeBytes(WorldServerProtocol.makeUnsizedUTF16("winlab"));
      fdid.data.writeUnsignedInt(0);
      fdid.data.writeUnsignedInt(uint(idfield.text));
      wd.fields.push(fdx);
      wd.fields.push(fdy);
      wd.fields.push(fdregion);
      wd.fields.push(fdid);
      world.send(WorldServerProtocol.makePushDataMsg(wd));
      var str:String = new String("winlab");
      var str2:String = new String();
      var buff:ByteArray = new ByteArray();
      //Two bytes for each character
      buff.writeUnsignedInt(2*str.length);
      for (var i:uint = 0; i < str.length; ++i) {
        buff.writeByte(0);
        buff.writeByte(str.charCodeAt(i));
        str2 = str2 + "." + str.charCodeAt(i);
      }
      cur_action.text = "Sending new URI to world server.";
    }

    private function setCoordinates(event:MouseEvent):void {
      var xscale:Number = 1.0 / (map_scale * floormap.scaleX);
      var yscale:Number = 1.0 / (map_scale * floormap.scaleY);
      xfield.text = (event.localX * xscale).toFixed(1);
      yfield.text = ((floormap.height - event.localY) * yscale).toFixed(1);
    }

    private function setupStartView():void {
      //Make input fields the GRAIL world server's ip and port
      ipfield = new TextField();
      portfield = new TextField();
      ipfield.text = "Host Address";
      portfield.text = "Host Port";

      portfield.type = ipfield.type = TextFieldType.INPUT;
      portfield.border = ipfield.border = true;
      portfield.multiline = ipfield.multiline = false;
      portfield.height = ipfield.height = ipfield.textHeight;
      ipfield.width = ipfield.textWidth * 3;
      portfield.width = portfield.textWidth * 2;
      ipfield.y = stat.y + 2*stat.height
      addChild(ipfield);
      portfield.y = ipfield.y + 2*ipfield.height;
      addChild(portfield);

      //Add a button for the user to click when they're done
      var donetext:TextField = new TextField();
      donetext.text = "Click when done.";
      donetext.type = TextFieldType.DYNAMIC;
      done = new SimpleButton(donetext, donetext, donetext, donetext);
      done.y = portfield.y + 2*portfield.height;
      donetext.height = donetext.textHeight;
      done.addEventListener(MouseEvent.CLICK, grailWSConnect);
      addChild(done);

      //Set up a label that informs the user of the current action.
      cur_action.y = stat.y + stat.height;
      cur_action.autoSize = "left";
      if (cur_action.text == "") {
        cur_action.text = "No action in progress.";
      }

    }

    private function clearStartView():void {
      removeChild(ipfield);
      removeChild(portfield);
      //done.removeEventListener(MouseEvent.CLICK, grailWSConnect);
      removeChild(done);
      done = null;
    }

    private function setupControlView():void {
      while (numChildren != 0) {
        this.removeChildAt(numChildren - 1);
      }
      //Display the current status and action information
      addChild(stat);
      addChild(cur_action);

      //Fields to collect URI information
      namefield = makeTField("URI");
      namefield.type = TextFieldType.INPUT;
      idfield = makeTField("ID");
      idfield.type = TextFieldType.INPUT;
      xfield = makeTField("x coord");
      xfield.type = TextFieldType.INPUT;
      yfield = makeTField("y coord");
      yfield.type = TextFieldType.INPUT;

      //Add a button for the user to click when they're done
      submit = makeTButton("Click to submit");
      submit.addEventListener(MouseEvent.CLICK, sendNewURI);

      submit.y = cur_action.y + cur_action.height + 5;
      idfield.y = namefield.y = xfield.y = yfield.y = submit.y + submit.height + 5;
      submit.x = 0;
      xfield.x = 0;
      yfield.x = xfield.width + 5;
      idfield.x = yfield.x + yfield.width + 5;
      namefield.x = idfield.x + idfield.width + 5;

      addChild(namefield);
      addChild(idfield);
      addChild(xfield);
      addChild(yfield);
      addChild(submit);

      //Scale the width
      floormap.width = stage.width;
      floormap.scaleY = floormap.scaleX;
      //If the map is still too tall scale it down more
      if (floormap.height > stage.height - floormap.y) {
        floormap.height = stage.height - floormap.y;
        floormap.scaleX = floormap.scaleY;
      }

      //Make the floormap a button to set the coordinate fields
      var create_uri:TextField = makeTField("Click to create a new URI");
      insert_button = new SimpleButton(floormap, floormap, floormap, floormap);
      insert_button.y = xfield.y + xfield.height + 5;
      insert_button.height = floormap.height;
      insert_button.width = floormap.width;
      insert_button.height = floormap.height;
      insert_button.addEventListener(MouseEvent.CLICK, setCoordinates);
      addChild(insert_button);
    }

    private function updateStats(event:TimerEvent):void {
      if (world.connected) {
        stat.text = "Status: connected";
        if ( false == last_connected) {
          clearStartView();
          setupControlView();
          last_connected = true;
        }
      }
      else {
        last_connected = false;
        stat.text = "Status: disconnected";
      }
    }

    public function statCallback(str:String):void {
      cur_action.text = str;
    }

    /*
     * Convenience function to make a text input field.
     */
    private function makeTInput(str:String):TextField {
      var tfield:TextField = new TextField();
      tfield.text = str;
      tfield.autoSize = "left";
      tfield.border   = true;
      tfield.type     = TextFieldType.INPUT;
      return tfield;
    }

    /*
     * Convenience function to make a text field.
     */
    private function makeTField(str:String):TextField {
      var tfield:TextField = new TextField();
      tfield.text = str;
      tfield.autoSize = "left";
      tfield.border   = true;
      tfield.type     = TextFieldType.DYNAMIC;
      tfield.height   = tfield.textHeight;
      tfield.width    = tfield.textWidth;
      return tfield;
    }

    /*
     * Convenience function to make a button with a text label
     */
    private function makeTButton(str:String):SimpleButton {
      var tfield:TextField = makeTField(str);
      return new SimpleButton(tfield, tfield, tfield, tfield);
    }

    private function UTF16toString(bytes:ByteArray):String {
      //TODO converting UTF16 to UTF8 but actionscript should support this
      var str:String = new String();
      var strlen:uint = bytes.length;
      for (var j:uint = 0; j < strlen; ++j) {
        var s:String = bytes.readUTFBytes(1);
        if (j % 2 == 1) {
          str = str + s;
        }
      }
      return str;
    }

    /*
     * Display the world data for a queried URI.
     */
    public function queryCallback(wd:WorldData):void {
    }

    /*
     * This function adds each type in the given vector to a table.
     */
    public function uriSearchCallback(uris:Vector.<String>):void {
    }

    public function typeSelected(e:Event):void {
      //cur_action.text = cb.selectedItem.label;
    }

    /*
     * Connect to the GRAIL3 world server when the button is clicked.
     */
    public function grailWSConnect(event:MouseEvent):void {
      world = new WorldServerClient(ipfield.text, int(portfield.text), queryCallback, uriSearchCallback, statCallback);
      clock.start();
    }
  }
}
