/* Holds data in a column.
 * The spacing between items in the column is set by the spacing variable.
 */
package {
  import flash.display.Sprite;
  import flash.display.DisplayObject;

  public class Column extends Sprite {
    private var _spacing:uint;

    public function Column(spacing:uint) {
      _spacing = spacing;
      //width = 0;
      //height = 0;
    }

    override public function addChild(r:DisplayObject):DisplayObject {
      r.y = this.numChildren * _spacing;
      //this.height = this.height + r.height;
      return super.addChild(r);
    }

    public function forEach(f:Function):void {
      for (var i:uint = 0; i < this.numChildren; ++i) {
        f(this.getChildAt(i), i);
      }
    }

    public function autosizeColumns():void {
      var max_widths:Array = [0,0,0,0];
      var r:uint;
      var c:uint;
      //Find the maximum width of each column by search through the rows.
      for (r = 0; r < numChildren; ++r) {
        for (c = 0; c < (getChildAt(r) as Row).numChildren - 1; ++c) {
          var tfield:DisplayObject = (getChildAt(r) as Row).getChildAt(c);
          if (null != tfield && tfield.width > max_widths[c]) {
            max_widths[c] = tfield.width;
          }
        }
      }
      //Set the spacings in each row.
      for (r = 0; r < numChildren; ++r) {
        for (c = 0; c < max_widths.length; ++c) {
          (getChildAt(r) as Row).spacingAt(c+1, max_widths[c]);
        }
      }
    }

    public function set spacing(spacing:uint):void {
      //Fix spacing for new value
      _spacing = spacing;
      forEach(function (r:DisplayObject, i:uint):void { r.y = i * _spacing; });
    }

    public function get spacing():uint {
      return _spacing;
    }
  }
}
