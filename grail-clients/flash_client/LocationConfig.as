import flash.display.Sprite;
import flash.events.*;
import flash.display.DisplayObject;
import flash.utils.Timer;
import flash.utils.ByteArray; 
import flashgrail.WorldServerProtocol;
import flashgrail.WorldServerClient;
import flashgrail.WorldData;
import flashgrail.FieldData;

[Embed("cleanlab.jpg")] public var MapImage:Class;
[Bindable] public var fullmap:DisplayObject = new MapImage();
public const map_scale:Number = 1.88 * 3.63;

public var clock:Timer = new Timer(1000);

public var world:WorldServerClient;

public var last_connected:Boolean = false;

public function sendNewURI():void {
  //Send the URI using the values in the fields.
  //Need to input x location, y location, id, and region.
  var wd:WorldData = new WorldData;
  var now:Date = new Date();
  wd.object_uri = namefield.text;
  var fdx:FieldData = new FieldData();
  var fdy:FieldData = new FieldData();
  var fdregion:FieldData = new FieldData();
  var fdid:FieldData = new FieldData();
  fdx.description = "location.x";
  fdy.description = "location.y";
  fdregion.description = "region_uri";
  fdid.description = "id";
  fdx.creation_date = fdy.creation_date = fdregion.creation_date = fdid.creation_date = uint(now.getTime()/1000.0);
  fdx.expiration_date = fdy.expiration_date = fdregion.expiration_date = fdid.expiration_date = 0;
  fdx.data_type = fdy.data_type = "double";
  fdregion.data_type = "string";
  fdid.data_type = "uint64_t";
  fdx.data.writeDouble(Number(xfield.text));
  fdy.data.writeDouble(Number(yfield.text));
  fdregion.data.writeBytes(WorldServerProtocol.makeUnsizedUTF16("winlab"));
  fdid.data.writeUnsignedInt(0);
  fdid.data.writeUnsignedInt(uint(idfield.text));
  wd.fields.push(fdx);
  wd.fields.push(fdy);
  wd.fields.push(fdregion);
  wd.fields.push(fdid);
  world.send(WorldServerProtocol.makePushDataMsg(wd));
  var str:String = new String("winlab");
  var str2:String = new String();
  var buff:ByteArray = new ByteArray();
  //Two bytes for each character
  buff.writeUnsignedInt(2*str.length);
  for (var i:uint = 0; i < str.length; ++i) {
    buff.writeByte(0);
    buff.writeByte(str.charCodeAt(i));
    str2 = str2 + "." + str.charCodeAt(i);
  }
  cur_action.text = "Sending new URI to world server.";
}

public function setCoordinates(event:MouseEvent):void {
  var xscale:Number = 1.0 / (map_scale * floormap.width / fullmap.width);
  var yscale:Number = 1.0 / (map_scale * floormap.height / fullmap.height);
  xfield.text = (event.localX * xscale).toFixed(1);
  yfield.text = ((floormap.height - event.localY) * yscale).toFixed(1);
}

public function setupControlView():void {
  uilist.removeElement(connect_form);
  for (var el:uint = 0; el < uilist.numElements; ++el) {
    uilist.getElementAt(el).visible = true;
    //uilist.getElementAt(el).includeInLayout = true;
  }
}

public function updateStats(event:TimerEvent):void {
  if (world.connected) {
    stat.text = "Status: connected";
    if ( false == last_connected) {
      setupControlView();
      last_connected = true;
    }
  }
  else {
    last_connected = false;
    stat.text = "Status: disconnected";
  }
}

public function statCallback(str:String):void {
  cur_action.text = str;
}

public function UTF16toString(bytes:ByteArray):String {
  //TODO converting UTF16 to UTF8 but actionscript should support this
  var str:String = new String();
  var strlen:uint = bytes.length;
  for (var j:uint = 0; j < strlen; ++j) {
    var s:String = bytes.readUTFBytes(1);
    if (j % 2 == 1) {
      str = str + s;
    }
  }
  return str;
}

/*
 * Display the world data for a queried URI.
 */
public function queryCallback(wd:WorldData):void {
}

/*
 * This function adds each type in the given vector to a table.
 */
public function uriSearchCallback(uris:Vector.<String>):void {
}

public function typeSelected(e:Event):void {
  //cur_action.text = cb.selectedItem.label;
}

/*
 * Connect to the GRAIL3 world server when the button is clicked.
 */
public function connect():void {
  world = new WorldServerClient(ws_ipfield.text, int(ws_portfield.text), queryCallback, uriSearchCallback, statCallback);
  clock.start();
}
