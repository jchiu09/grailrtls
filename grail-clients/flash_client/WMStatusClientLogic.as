import flash.display.DisplayObject;
import flash.utils.Timer;
import flash.utils.Dictionary;
import flash.display.Loader;
import flash.net.URLRequest;

//GRAIL3 World model stuff
import flashgrail.ClientWMClient;
import flashgrail.SolverWMClient;
import flashgrail.ClientWorldModel;
import flashgrail.SolverWorldModel;
import flashgrail.TypeLookup;
import flashgrail.WMData;
import flashgrail.WMAttribute;
import flashgrail.WorldModelRequest;
import flashgrail.RawSolution;

//Stuff to display transmitters
import flashgrail.Location;
import flashgrail.TileLocation;
import flashgrail.EllipseLocation;
import flashgrail.AntennaStatus;

//For the drop down box
import mx.collections.ArrayList;
import spark.events.IndexChangeEvent;

//For the check box area
import spark.components.CheckBox;

//For events
import spark.components.Label;
import spark.components.RichText;

//Map of the area
[Embed("cleanlab.jpg")] public var MapImage:Class;
public var floormap:DisplayObject = new MapImage();
//[Embed("Halloween2011.House.png")] public var MapImage:Class;
//public var floormap:DisplayObject = new MapImage();

//Icons to show on the map
[Embed("antenna.png")] public var AntennaImage:Class;
[Embed("mug.png")] public var MugImage:Class;
[Embed("microwave.png")] public var MWaveImage:Class;
[Embed("microwaveon.png")] public var MWaveImageOn:Class;
[Embed("door.png")] public var Door:Class;
[Embed("door_open.png")] public var DoorOpen:Class;
[Embed("gluegun.png")] public var GlueGun:Class;
[Embed("projector.png")] public var Project:Class;
[Embed("projectoron.png")] public var ProjectOn:Class;
[Embed("coffeepot.png")] public var Coffeepot:Class;
[Embed("duct_tape.png")] public var Dtape:Class;
[Embed("packtape.png")] public var Ptape:Class;
[Embed("chair.png")] public var Chair:Class;
[Embed("chairempty.png")] public var ChairEmpty:Class;
[Embed("wins_temperature.png")] public var WinsTemp:Class;
[Embed("wins_hybrid.png")] public var WinsHybrid:Class;
//[Embed("player.png")] public var PlayerHat:Class;
public var images:Object = {"mug":MugImage, "microwave":MWaveImage,  "microwaveon":MWaveImageOn,
                            "door":DoorOpen, "doorclosed":Door, "hot glue gun":GlueGun,
                            "projector":Project, "projectoron":ProjectOn, "coffee pot":Coffeepot,
                            "duct tape":Dtape, "packing tape":Ptape, "chair":Chair, "chairempty":ChairEmpty,
                            "temp":WinsTemp, "hybrid":WinsHybrid};//, "players":PlayerHat};

//The objects seen by the localization system
public var visible_objects:Dictionary = new Dictionary();
//Groups of transmitters - "mug", "door", etc - all stored as vectors of strings.
public var groups:Object = {"All":new Dictionary()};

//Named locations with known coordinates
public var named_locations:Dictionary = new Dictionary();
public var items_at_locations:Vector.<String> = new Vector.<String>();

//Locations of fixed items
public var fixed:Dictionary = new Dictionary();

//Current state of the world - udpates when new
//data from a stream request is received.
public var world_state:Dictionary = new Dictionary();

private var client_world:ClientWMClient;
//private var solver_world:SolverWMClient;

//public const map_scale:Number = 1.88 * 3.63;
public var map_scale:Number = 1.0;

public var clock:Timer = new Timer(1000);
public var retry_timer:uint = 0;

public var last_connected:Boolean = false;

public var location_type:String = "location";
public var location_alias:int = -1;
public var event_alias:int = -1;
public var coffee_alias:int = -1;
public var misc_aliases:Dictionary = new Dictionary();
public var status_aliases:Dictionary = new Dictionary();

//The time of the last data update, in seconds since January 1, 1970.
public var last_updated:Number = 0;
public var world_update:Number = 0;

public var coffee_brewtime:Date = null;
public var brew_graphic:AntennaStatus = null;

[Bindable] public var regions:ArrayList = new ArrayList([]);

[Bindable] public var group_dropdown:ArrayList = new ArrayList(["All"]);


private var client_callbacks:Object = {URI_RESPONSE:uriSearchCallback,
                                       DATA_RESPONSE:dataCallback};

/*
public function solverStatCallback(str:String):void {
  solver_cur_action.text = "Solver WM: "+str;
}
*/

public function clientStatCallback(str:String):void {
  client_cur_action.text = "Client WM: "+str;
}

//Called when a type is selected in the dropdown
public function filterDisplay(e:IndexChangeEvent):void {
  var filt_name:String = target_dropdown.selectedItem as String;
  //See if this is an individual antenna
  if (null != visible_objects[filt_name]) {
    //Remove all visible_objects and then display this one
    removeAntennas();
    maparea.addChild(visible_objects[filt_name]);
  }
  else if (null != groups[filt_name]) {
    var dict_filter:Dictionary = groups[filt_name];
    //Remove all visible_objects and then add all of the visible_objects from this group.
    removeAntennas();
    for each (var ant_stat:AntennaStatus in dict_filter) {
      maparea.addChild(ant_stat);
    }
  }
}

//Remove all of the viewable visible_objects
private function removeAntennas():void {
  var idx:uint = 0;
  while (idx < maparea.numChildren) {
    var ant:AntennaStatus = maparea.getChildAt(idx) as AntennaStatus;
    if (ant != null) {
      maparea.removeChildAt(idx);
    }
    else {
      ++idx;
    }
  }
}

/*
 * Append the given message to the event status log.
 */
public function logEvent(str:String):void {
  status_log.appendText("\n"+str);
}

public function redrawWithImage(img:DisplayObject, item_group:String, full_name:String):void {
  if (null == visible_objects[full_name]) {
    return;
  }
  if (null != groups[item_group]) {
    groups[item_group][full_name].image = img;
    visible_objects[full_name].image = img;
  }
}

/***
 * Draw the given item at the given location.
 * The filterDisplay function should be called after calls to this function to
 * render any new items.
 ***/
public function drawAtLocation(loc:Location, item_name:String, item_group:String, full_name:String):void {
  var astat:AntennaStatus;

  //If this was drawn before then remember some things about it
  if (null != visible_objects[full_name]) {
    astat = visible_objects[full_name];
    //Remove the previous image before we lose the reference to it.
    if (maparea.contains(visible_objects[full_name])) {
      maparea.removeChild(visible_objects[full_name]);
    }
    /*
    //Copy over the previous details
    astat.setDetails(visible_objects[full_name].getDetails());
    astat.image = visible_objects[full_name].image;
    */
    astat.location = loc;
  }
  //Otherwise check if there is a special image for this item group
  else {
    var aimg:DisplayObject = new AntennaImage();
    astat = new AntennaStatus(aimg, loc, item_name);

    //Update the "All" group.
    groups["All"][full_name] = astat;
    if (null == groups[item_group]) {
      addGroup(item_group);
      groups[item_group] = new Dictionary();
    }
    //Update the image for the group dropdown
    groups[item_group][full_name] = astat;
    astat.addDetail("Name:", full_name);

    addURI(full_name);
    //Change the image to the group's image if there is one
    //and this if the first time we are drawing this image.
    if (null != images[item_group+"."+item_name]) {
      astat.image = new images[item_group+"."+item_name];
    }
    else if (null != images[item_group]) {
      astat.image = new images[item_group];
    }
  }


  //Set the new antenna status object for this item
  visible_objects[full_name] = astat;
  //Images are a little big - shrink them by half
  astat.imgScale = 0.5 * floormap.scaleX;
  //Scale and set location according to the map image scale
  astat.x = floormap.x + /*floormap.scaleX * */ map_scale * astat.location.x;
  astat.y = floormap.y + floormap.height - /*floormap.scaleY * */ map_scale * astat.location.y;
  //Check if this should be in the map
  var filt_name:String = target_dropdown.selectedItem as String;
  if (full_name == filt_name ||
      item_group == filt_name) {
    maparea.addChild(visible_objects[full_name]);
  }
}

/*
 * This function updates the locations of transmitters as location data arrives.
 * If other kinds of information arrive they are displayed in the status box.
 * Other specialized information is handled in solution specific ways.
 * For instance open/close solutions might make door graphics change.
 */
 /*
public function newSolutionsCallback(solutions:SolverData):void {
  for (var i:uint = 0; i < solutions.solutions.length; ++i) {
    var soln:GRAILSolution = solutions.solutions[i];
    //See if the user expressed interest in this group.
    var group_pieces:Array = soln.target.split(".");
    //First part is region, last part is name, everything in between is the group
    var gname:String = group_pieces[1];
    for (var name_part:uint = 2; name_part < group_pieces.length - 1; ++name_part) {
      gname += "." + group_pieces[name_part];
    }
    //Draw the item if this is a location solution and this is not a fixed item.
    if (location_alias == soln.type_alias &&
        null == fixed[soln.target]) {
      statCallback("Receiving location data.");
      last_updated = (new Date()).getTime();
      statCallback("Processing location of "+soln.target);
      //If this antenna is not currently being displayed then
      var eloc:Location;
      if (location_type == "location") {
        statCallback("Making new ellipse for "+soln.target);
        eloc = new EllipseLocation(soln.data);
      }
      else if (location_type == "tile_location") {
        statCallback("Making new tile for "+soln.target);
        eloc = new TileLocation(soln.data);
      }
      else {
        statCallback("Unrecognized location type");
        return;
      }
      drawAtLocation(eloc, soln.target);
      statCallback("New location results displayed.");
    }
    else if (event_alias == soln.type_alias) {
      statCallback("Receiving event data.");
      //Pull the event data into the string.
      var num_items:uint = soln.data.readUnsignedInt();
      var event_text:String = new String();
      for (var item:uint = 0; item < num_items; ++item) {
        //Get the label and description and add them to the label text
        var lab:String = ClientAggregator.readSizedUTF16(soln.data);
        var desc:String = ClientAggregator.readSizedUTF16(soln.data);
        //FIXME Workaround for messed up in the description fields when the email script reads too long
        desc = desc.replace(/=/g, "");
        var end_idx:int = desc.search("--_[_0-9A-F]*ms2winlab");
        if (-1 != end_idx) {
          desc = desc.substr(0, end_idx-1);
        }

        //TODO These are workarounds for previously poorly processed email that had html in them.
        if ("" != lab && "BSTRACT" != lab) {
          event_text += lab + ":\t" + desc + "\n";
          //Search for other events with the same title and remove them.
          if (lab == "TITLE") {
            for (var other_el:int = 0; other_el < upcoming_area.numElements; ++other_el) {
              //Check for a title the same as this one
              var oth:RichText = upcoming_area.getElementAt(other_el) as RichText;
              if (null != oth &&
                  -1 != oth.text.search(desc)) {
                //Remove duplicate element
                upcoming_area.removeElementAt(other_el);
                --other_el;
              }
            }
          }
        }
      }
      //Change the upcoming label's text to indicate that there are some events
      upcoming_label.text = "Upcoming calendar events:";
      var event_label:RichText = new RichText();
      event_label.text = event_text + "\n\n";
      upcoming_area.addElement(event_label);
      //Set the width to match the container's width
      event_label.percentWidth = 100;
    }
    //Non-location, non-event data. Check the map of printable
    //events of interest to see if this solution is one of them.
    else if (null != status_aliases[soln.type_alias]) {
      //Show this event if we know its name.
      if (null != status_aliases[soln.type_alias]) {
        var sol_name:String = status_aliases[soln.type_alias];
        //Push this new solution to the status log.
        var sol_time:Date = new Date();
        sol_time.setTime(solutions.time);
        var update_text:String = sol_time.toLocaleString() + ": " + soln.target;
        var img_string:String = gname;
        if (sol_name == "coffee brewing") {
          update_text += " has ";
          coffee_brewtime = sol_time;
        }
        else {
          if (0 == soln.data.readByte()) {
            update_text += " is not ";
          }
          else {
            update_text += " is ";
            img_string += sol_name;
          }
        }
        update_text += sol_name;
        //Only log events the user is interested in.
        if (interested[status_aliases[soln.type_alias]] as Boolean) {
          logEvent(update_text);
        }
        //TODO FIXME should very old text be removed?
        //Update the image to reflect its current status
        var item_id:String = nameToID[gname + "." + group_pieces[group_pieces.length-1]];
        if (item_id != null &&
            visible_objects[item_id] != null &&
            images[img_string] != null) {
          visible_objects[item_id].image = new images[img_string];
        }
      }
    }
    //Otherwise see if this is known miscellaneous data
    else if (null != misc_aliases[soln.type_alias]) {
      var sol_name:String = misc_aliases[soln.type_alias];
      var item_id:String = nameToID[soln.target];
      //Some solutions only point to a transmitter id
      if (group_pieces.length == 1) {
        item_id = soln.target;
      }
      //Add this detail to the object if the object is on the map
      if ( item_id != null &&
           visible_objects[item_id] != null) {
        var solver_data:String = "0x";
        while (soln.data.bytesAvailable > 0) {
          var new_char:uint = soln.data.readUnsignedByte();
          //Convert this number to a hex string
          var char_str:String = new_char.toString(16);
          //Make sure that each hex value consumes two characters.
          if (char_str.length == 1) {
            char_str = "0" + char_str;
          }
          solver_data += char_str;
        }
        visible_objects[item_id].addDetail(sol_name, solver_data);
      }
    }
  }
  //Call filter display to make sure we are displaying the correct visible_objects
  filterDisplay(null);
}
*/

/*
public function onLoaderReady(e:Event):void {     
  //Finished loading this image so put it into the image list
  //images[e.currentTarget.loader.group_name] = e.currentTarget.loader;
  var gname:String = loader_names[e.currentTarget.loader];
  images[gname] = e.currentTarget.loader.content;
  //Redraw anything of this class with this new image
  if (groups[gname] != null) {
    for each (var full_name:String in groups[gname]) {
      redrawWithImage(new images[gname], gname, full_name);
    }
  }
}

public var loader_names:Dictionary = new Dictionary();
public var myLoader:Loader = new Loader();
public var fileRequest:URLRequest = new URLRequest("http://winlab.rutgers.edu/~bfirner/microwave.png");
*/

public function connect():void {
  /*
  loader_names[myLoader] = "microwave";

  myLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoaderReady);
  myLoader.load(fileRequest);
  */

  stat.text = "Status: connecting to ip "+ipfield.text+" on client port "+int(client_portfield.text);
  //solver_world = new SolverWMClient(ipfield.text, int(solver_portfield.text), solverStatCallback, origin_field.text);
  client_world = new ClientWMClient(ipfield.text, int(client_portfield.text), client_callbacks, clientStatCallback);
  //Event that responds to the timer that fires once a second.
  clock.addEventListener(TimerEvent.TIMER, updateStats);
  clock.start();
  //Set up the dropdown list.
  target_dropdown.selectedItem = group_dropdown[0];
}

/*
 * Get the attributes with the given name.
 */
public function getAttributes(attrs:Vector.<WMAttribute>, desired:Vector.<String>):Vector.<WMAttribute> {
  //Function to check if an attribute name is in the attrs vector.
  var match_exp:Function = function (attr:WMAttribute, i:int, v:Vector.<WMAttribute>):Boolean {
    var attr_match:Function = function (cur_name:String, j:int, vname:Vector.<String>):Boolean {
      return attr.name == cur_name;
    };
    return desired.some(attr_match);
  }
  //Make sure every desired attribute has a match
  return attrs.filter(match_exp);
}

public function getOriginAttributes(attrs:Vector.<WMAttribute>, desired:Vector.<String>, origin:String):Vector.<WMAttribute> {
  //Function to check if an attribute name is in the attrs vector.
  var match_exp:Function = function (attr:WMAttribute, i:int, v:Vector.<WMAttribute>):Boolean {
    var attr_match:Function = function (cur_name:String, j:int, vname:Vector.<String>):Boolean {
      return attr.name == cur_name && attr.origin == origin;
    };
    return desired.some(attr_match);
  }
  //Make sure every desired attribute has a match
  return attrs.filter(match_exp);
}

/*
 * Check this attribute vector for all of the given attributes.
 */
public function hasAttributes(attrs:Vector.<WMAttribute>, desired:Vector.<String>):Boolean {
  //Function to check if string name matches a name in the attrs vector.
  var attr_match:Function = function (cur_name:String, j:int, vname:Vector.<String>):Boolean {
    var match_exp:Function = function (attr:WMAttribute, i:int, v:Vector.<WMAttribute>):Boolean {
      return attr.name == cur_name;
    };
    return attrs.some(match_exp);
  }
  //Make sure every desired attribute has a match
  return desired.every(attr_match);
}

/*
 * Search for an implicit or explicit region.
 * If no region can be found then just returns an empty string.
 */
public function findRegion(object_uri:String, attribs:Vector.<WMAttribute>):String {
  debug_field.text = "finding region";
  //If there is no region attribute check for an implicit name
  if (false == hasAttributes(attribs, new <String>["location.region.uri"])) {
    debug_field.text = "no region defined, searching through "+regions.length+" regions";
    //Find the region of the greatest length that matches
    var long_region:String = "";
    for (var i:int = 0; i < regions.length; ++i) {
      debug_field.text = "checking region  "+(regions.getItemAt(i) as String);
      if (regions.getItemAt(i) == object_uri.substr(0, regions.getItemAt(i).length) &&
          regions.getItemAt(i).length > long_region.length) {
        long_region = regions.getItemAt(i) as String;
      }
    }
    //Can return empty string if there is not match
    return long_region;
  }
  //Otherwise return the attribute URI
  else {
    debug_field.text = "region defined";
    var region_attrs:Vector.<WMAttribute> = getAttributes(attribs, new <String>["location.region.uri"]);
    var str:String = TypeLookup.typeToString(region_attrs[0].name, region_attrs[0].data);
    region_attrs[0].data.position = 0;
    return str;
  }
}

public function getRegionGroupName(object_uri:String, attribs:Vector.<WMAttribute>):Vector.<String> {
  //Break this object URI into a region, a group, and its name
  //object_uri = <region>.<group>.<name>
  var attr_region:String = findRegion(object_uri, attribs);
  debug_field.text = "Found region "+attr_region;
  var non_region:String = object_uri.substr(attr_region.length+1);
  if (attr_region == "") {
    non_region = object_uri;
  }
  debug_field.text = "Non region is "+non_region;
  try {
    var result:Array = /(.*)\.(.*)/.exec(non_region);
    debug_field.text = "Regex gave "+result.length+" parts."
  }
  catch (err:Error) {
    return new<String>["", "", object_uri];
  }
  return new<String>[attr_region, result[1], result[2]];
}

public function resetByteArray(attribs:Vector.<WMAttribute>):void {
  if (null != attribs) {
    //Reset the positions of all of the data
    attribs.forEach(function (attr:WMAttribute):void {
      try {
        if (attr.data.length != 0) {
            attr.data.position = 0;
        }
      }
      catch (err:Error) {
        debug_field.text = "Bad field ("+attr.name+")";
      }
    });
  }
}

/*
 * Set up an object so that it can be quickly references and displayed
 */
public function updateAttributes(attribs:Vector.<WMAttribute>, region:String, group_name:String, obj_name:String):void {
  var full_name:String = group_name + "." + obj_name;
  if (null != groups[group_name] && null != groups[group_name][full_name]) {
    var add_detail:Function = function (attr:WMAttribute, i:int, v:Vector.<WMAttribute>):void {
      try {
        attr.data.position = 0;
        groups[group_name][full_name].addDetail(attr.name + "("+attr.origin.split("\n")[0]+"):", TypeLookup.typeToString(attr.name, attr.data));
      }
      catch (err:Error) {
        debug_field.text = "Skipping invalid data";
      }
    }
    attribs.forEach(add_detail);
  }
  //resetByteArray(attribs);
}

/*
 * Add a region if it is new.
 */
public function addRegion(obj_uri:String):void {
  var splits:Array = obj_uri.split(".");
  if (splits[0] == "region") {
    var new_rname:String = obj_uri.substr(splits[0].length+1);
    if(false == regions.source.some(function (s:String, i:int, a:Array):Boolean {return s == new_rname;})) {
      regions.addItem(new_rname);
    }
  }
}

public function addGroup(group_uri:String):void {
  var group_match:Function = function (s:String, i:int, a:Array):Boolean {return s == group_uri;};
  if(false == regions.source.some(group_match)) {
    group_dropdown.addItem(group_uri);
  }
}

public function addURI(item_uri:String):void {
  var item_match:Function = function (s:String, i:int, a:Array):Boolean {return s == item_uri;};
  if(false == regions.source.some(item_match)) {
    group_dropdown.addItem(item_uri);
  }
}

public function getLocalWD(object_uri:String):Vector.<WMAttribute> {
  return world_state[object_uri] as Vector.<WMAttribute>;
}

public function updateLocalWorld(new_wd:WMData):Vector.<WMAttribute> {
  //Use this to store complete attributes for URIs that had a change in
  //any attributes.
  var wd:WMData = new WMData();
  var obj_uri:String = new_wd.object_uri;
  wd.object_uri = obj_uri;

  //Fetch and modify the historic data for this URI.
  if (world_state[obj_uri] == null) {
    //New URI so store all values
    world_state[obj_uri] = new_wd.attribs;
  }
  else {
    //Update values
    var update_changed:Function = function (attr:WMAttribute, i:int, v:Vector.<WMAttribute>):void {
      //For each matching attribute, update it if this attribute has the same
      //origin and name
      var changed:Boolean = false;
      for each (var local_attr:WMAttribute in getLocalWD(obj_uri)) {
        if (attr.name == local_attr.name &&
          attr.origin == local_attr.origin) {
          local_attr = attr;
          changed = true;
        }
      }
      if (changed == true) {
        getLocalWD(obj_uri).push(attr);
      }
    }
    new_wd.attribs.forEach(update_changed);
  }
  return getLocalWD(obj_uri);
}

/*
 * Display the field data for a queried URI.
 */
public function dataCallback(wd:WMData):void {
  debug_field.text = "Data callback for ticket "+wd.ticket;
  //See if the data is about a region or about objects
  //0 means objects, any other value means region
  if (wd.ticket == 1) {
    clientStatCallback("Showing data query results");

    world_state[wd.object_uri] = wd.attribs;

    var obj_uri:String = wd.object_uri;
    var splits:Array = wd.object_uri.split(".");

    //The search only selects objects in the region of interest so we do
    //not need to check if this object is from the correct region.

    //Some items are implicity placed in regions and others are
    //explicity place based upon their URI name.
    //Search for attributes to match implicitly placed ones and search
    //for explicit designations as we map these.
    //(Explicitly placed URIs have an attribute named location.region.uri).
    //Map to a named location
    var mappable_attrs:Vector.<String> = new <String>["location.uri"];
    //Map to a coordinate
    var coordinate_attrs:Vector.<String> = new <String>["location.xoffset", "location.yoffset"];

    //The locations of gathering spots and regions are implicit in their URI names.
    //A gathering spot
    var gather_attrs:Vector.<String> = new <String>["location.maxx", "location.maxy",
        "location.minx", "location.miny", "threshold", "objects of interest.uri"];

    //A region on the map
    var region_attrs:Vector.<String> = new <String>["location.maxx", "location.maxy",
        "location.minx", "location.miny"];

    //Get the region, group name, and local name for this URI
    var rgn:Vector.<String> = getRegionGroupName(obj_uri, getLocalWD(obj_uri));
    var gname:String = rgn[1];
    var full_name:String = gname + "." + rgn[2];

    ////Map this to the URI with this objects location.uri value
    //if (hasAttributes(getLocalWD(obj_uri), mappable_attrs) == true) {
      //var attr:WMAttribute = getAttributes(getLocalWD(obj_uri), new <String>["location.uri"])[0];
      //attr.data.position = 0;
      //var str_loc:String = TypeLookup.typeToString(attr.name, attr.data);
      //debug_field.text = "Going to put "+obj_uri+" on the map at "+str_loc;
      //if (null != named_locations[str_loc]) {
        //var bbox:Vector.<Number> = named_locations[str_loc];
        //var loc:Location = new Location();
        //var width:Number = bbox[2] - bbox[0];
        //var height:Number = bbox[3] - bbox[1];
        //loc.x = 0.25 * width + bbox[0] + Math.random() * (width / 2.0);
        //loc.y = 0.25 * height +  bbox[1] + Math.random() * (height / 2.0);
        //drawAtLocation(loc, rgn[2], rgn[1], rgn[1] + "." + rgn[2]);
        //updateAttributes(getLocalWD(obj_uri), rgn[0], rgn[1], rgn[2]);
        ////Call filter display to make sure we are displaying the correct images for items
        //filterDisplay(null);
      //}
    //}

    //Map this to the x,y location given
    if (hasAttributes(getLocalWD(obj_uri), coordinate_attrs) == true) {
      debug_field.text = "Going to put "+obj_uri+" on the map";
      debug_field.text = "Putting "+obj_uri+" in region "+rgn[0]+" and group "+rgn[1]+" with name "+rgn[2];
      //TODO Prefer attributes from more reliable sources (origin == "Ben")
      var x_attributes:Vector.<WMAttribute> = getAttributes(getLocalWD(obj_uri), new <String>["location.xoffset"]);
      var y_attributes:Vector.<WMAttribute> = getAttributes(getLocalWD(obj_uri), new <String>["location.yoffset"]);
      var loc:Location = new Location();
      debug_field.text = "here1";
      x_attributes[0].data.position = 0;
      loc.x = x_attributes[0].data.readDouble();
      y_attributes[0].data.position = 0;
      loc.y = y_attributes[0].data.readDouble();
      debug_field.text = "here2";
      debug_field.text = "Got x and y locations "+loc.x+" and "+loc.y;
      drawAtLocation(loc, rgn[2], rgn[1], rgn[1] + "." + rgn[2]);
      updateAttributes(getLocalWD(obj_uri), rgn[0], rgn[1], rgn[2]);
      //Call filter display to make sure we are displaying the correct images for items
      filterDisplay(null);
      debug_field.text = "here3";
    }

    //If this is a gathering spot mark it on the map.
    if (hasAttributes(getLocalWD(obj_uri), gather_attrs) == true) {
    }

    //If this is a region then mark it with a bounding box
    if (hasAttributes(getLocalWD(obj_uri), region_attrs) == true) {
      var bbox:Vector.<Number> = new Vector.<Number>();
      var minx:Vector.<WMAttribute> = getAttributes(getLocalWD(obj_uri), new <String>["location.minx"]);
      var miny:Vector.<WMAttribute> = getAttributes(getLocalWD(obj_uri), new <String>["location.miny"]);
      var maxx:Vector.<WMAttribute> = getAttributes(getLocalWD(obj_uri), new <String>["location.maxx"]);
      var maxy:Vector.<WMAttribute> = getAttributes(getLocalWD(obj_uri), new <String>["location.maxy"]);
      minx[0].data.position = 0;
      bbox.push(minx[0].data.readDouble());
      miny[0].data.position = 0;
      bbox.push(miny[0].data.readDouble());
      maxx[0].data.position = 0;
      bbox.push(maxx[0].data.readDouble());
      maxy[0].data.position = 0;
      bbox.push(maxy[0].data.readDouble());
      named_locations[obj_uri] = bbox;
      items_at_locations[obj_uri] = new Vector.<String>();
      var rectangle:Shape = new Shape();
      var xscale:Number = floormap.scaleX * map_scale;
      var yscale:Number = floormap.scaleY * map_scale;
      with (maparea.graphics) {
        beginFill(0xFF0000);
        rectangle.graphics.drawRect(bbox[0], bbox[1], xscale*(bbox[2] - bbox[0]), yscale*(bbox[3] - bbox [1]));
        rectangle.graphics.endFill();
        endFill();
      }
      ////Scale and set location according to the map image scale
      //rectangle.x = floormap.x + floormap.scaleX * map_scale * bbox[0];
      //rectangle.y = floormap.y + floormap.height - floormap.scaleY * map_scale * bbox[1];
      //maparea.addChild(rectangle);
    }

    //Reflect state (doors open/closed, etc)
    if (null != getLocalWD(obj_uri)) {
      getLocalWD(obj_uri).forEach( function(attr:WMAttribute, i:int, v:Vector.<WMAttribute>):void {
        //Check if there is an image type for this
        //Change the image to the group's image if there is one
        //and this if the first time we are drawing this image.
        //Check if this is a boolean type and check if there is an image for it
        try {
          attr.data.position = 0;
        }
        catch (err:Error) {
          debug_field.text = "Error resetting position for "+rgn[1]+" of "+obj_uri;
        }
        if (attr.data.bytesAvailable == 1) {
          var is_true:Boolean = attr.data.readByte() != 0;
          if (is_true == true) {
            if (null != images[gname+attr.name]) {
              redrawWithImage(new images[rgn[1]+attr.name], rgn[1], full_name);
            }
          }
          else {
            if (null != images[gname]) {
              redrawWithImage(new images[rgn[1]], rgn[1], full_name);
            }
          }
          filterDisplay(null);
        }
      });
    }
    else {
      debug_field.text = "attributes are null";
    }
  }
  //Items with attributes of type "region.uri"
  else if (wd.ticket == 2) {
    //TODO FIXME
  }
  //Otherwise this is a response with region details
  else {
    var obj_uri:String = wd.object_uri;
    var splits:Array = wd.object_uri.split(".");
    //var region_name:String = obj_uri.substr(splits[0].length+1);
    //The ticket number indicates the position of this region in the regions list
    //TODO FIXME Assuming that there is only one region right now
    //Fetch the max x and y parameters
    var maxx:Vector.<WMAttribute> = getAttributes(wd.attribs, new <String>["location.maxx"]);
    var maxy:Vector.<WMAttribute> = getAttributes(wd.attribs, new <String>["location.maxy"]);
    if (maxx.length > 0 && maxy.length > 0) {
      maxx[0].data.position = 0;
      maxy[0].data.position = 0;
      var xscale:Number = floormap.width / maxx[0].data.readDouble();
      var yscale:Number = floormap.height / maxy[0].data.readDouble();
      map_scale = xscale;
      //if (xscale != yscale) {
        //maxx[0].data.position = 0;
        //maxy[0].data.position = 0;
        ////TODO FIXME Maybe there should be two scale factors?
        //yscale = floormap.height * maxx[0].data.readDouble() / floormap.width;
      //}
      debug_field.text = "Updating region details, map_scale is now " + map_scale;
    }
  }
}

//Regions are search using the URI search method
public function uriSearchCallback(found_uris:Vector.<String>):void {
  clientStatCallback("Region search complete");
  //Add all discovered regions to the regions list and request a snapshot
  found_uris.forEach( function(obj_uri:String, i:int, v:Vector.<String>):void {
    addRegion(obj_uri);
    var request:WorldModelRequest = new WorldModelRequest();
    request.object_uri = obj_uri;
    //Get all attributes
    request.attribs.push(".*");
    //Don't specify a time interval - get the current snapshot.
    updated_field.text = "Requesting region snapshot " + (10 + regions.length);
    client_world.send(ClientWorldModel.makeSnapshotRequest(request, 10 + regions.length));
    });
}

private function getURIData(uri:String, ticket:uint, attribs:Vector.<String>):void {
  clientStatCallback("Getting data for URI \""+uri+"\"");
  var request:WorldModelRequest = new WorldModelRequest();
  request.object_uri = uri;
  //Get all attributes
  request.attribs = attribs;
  //Don't specify any attributes - fetch them all
  //Don't specify a time interval - get the current snapshot.
  client_world.send(ClientWorldModel.makeSnapshotRequest(request, ticket));
}

private function updateStats(event:TimerEvent):void {
  //Reconnect when the timer runs down
  if (retry_timer > 0) {
    --retry_timer;
    if (retry_timer == 0 && client_world.connected == false) {
      stat.text = "Reconnecting...";
      client_world.reconnect();
    }
  }

  if (client_world.connected) {
    stat.text = "Status: connected";
    if ( false == last_connected) {
      //Remove the connect form and add status fields and the map.
      //connect_form.visible = false;
      uilist.removeElement(connect_form);
      for (var el:uint = 0; el < uilist.numElements; ++el) {
        uilist.getElementAt(el).visible = true;
        //uilist.getElementAt(el).includeInLayout = true;
      }
      last_connected = true;
      //Request URIs that are regions
      clientStatCallback("Searching for regions");
      client_world.send(ClientWorldModel.makeURISearch("region\..*"));
      /*
      //Make a stream request for all URIs and their attributes.
      var request:WorldModelRequest = new WorldModelRequest();
      request.object_uri = ".*";
      //Get all attributes
      request.attribs.push(".*");
      client_world.send(ClientWorldModel.makeStreamRequest(request));
      */
    }
  }
  else {
    if (last_connected) {
      //Try to reconnect in two seconds if we lost our connection.
      retry_timer = 5;
    }
    last_connected = false;
    stat.text = "Status: disconnected";
  }
  var now:Date = new Date();
  if (last_updated != 0) {
    updated_field.text = "Updated " + (now.getTime()-last_updated)/1000.0 + " seconds ago.";
  }
  //Request a new snapshot every five seconds
  if ( 5000 <= (now.getTime() - world_update)) {
    if (region_dropdown.selectedItem != null) {
      world_update = now.getTime();
      //Search for all objects in the region of interest
      getURIData(region_dropdown.selectedItem+".*", 1, new <String>["location.xoffset", "location.yoffset", ".*"]);
      getURIData(".*", 2, new <String>["location.xoffset", "location.yoffset", "region.uri"]);
    }
  }
}

