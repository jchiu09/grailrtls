/* Holds data in a row.
 * The spacing between items in the row is set by the spacing variable.
 */
package {
  import flash.display.Sprite;
  import flash.display.DisplayObject;

  public class Row extends Sprite {
    private var _spacings:Vector.<uint> = new Vector.<uint>();
    private var _spacing:uint;

    public function Row(spacing:uint) {
      _spacing = spacing;
      _spacings.push(0);
      //width = 0;
      //height = 0;
    }

    public function spacingAt(idx:uint, spacing:uint):void {
      //Push the default spacing value to pad until the given index
      while (idx >= _spacings.length) {
        _spacings.push(_spacing);
      }
      _spacings[idx] = spacing;
      //Move children if this affects them

      var cur_offset:uint = 0;
      for (var mv_idx:uint = 0; mv_idx < numChildren; ++mv_idx) {
        //Use _spacings array if it exists.
        if (mv_idx < _spacings.length) {
          cur_offset += _spacings[mv_idx];
        }
        else {
          cur_offset += _spacing;
        }
        getChildAt(mv_idx).x = cur_offset;
      }
    }

    override public function addChild(obj:DisplayObject):DisplayObject {
      if (this.numChildren < _spacings.length) {
        obj.x = _spacings[this.numChildren];
      }
      else {
        obj.x = this.numChildren * _spacing;
      }
      //this.width = this.width + _spacing;
      //if (this.height < obj.height) { this.height = obj.height; }
      return super.addChild(obj);
    }

    public function forEach(f:Function):void {
      for (var i:uint = 0; i < this.numChildren; ++i) {
        f(this.getChildAt(i), i);
      }
    }

    public function set spacing(spacing:uint):void {
      //Fix spacing for new value
      _spacing = spacing;
      forEach(function (r:DisplayObject, i:uint):void { r.x = i * _spacing; });
    }

    public function get spacing():uint {
      return _spacing;
    }
  }
}

