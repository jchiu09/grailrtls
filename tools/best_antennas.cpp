/*******************************************************************************
 * Find the best antenna setup in a region with a few possible antennas.
 * The region size and the possible locations of the receivers are provided
 * in one file and another file holds the different antenna gain patterns.
 * The antenna information gain at a given confidence level is used to determine
 * the best antenna configuration.
 ******************************************************************************/

#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <utility>

#include "cross_entropy.hpp"

using namespace std;

struct Region {
  //The x and y range inside of the region
  double xrange;
  double yrange;
  vector<pair<double, double>> rxers;
  vector<vector<double>> antenna_gains;
  //The possible "error" in an rss measurement - this could
  //come from scattering and non-line-of-sight environmental effects
  double rss_err;
  //Root mean square error and mean error when localization has zero information
  double null_rmsd;
  double null_md;
};

typedef vector<double> AntennaGain;

void setNullPerformance(Region& region) {
  //Generate a map of expected fingerprints
  size_t xrange = floor(region.xrange);
  size_t yrange = floor(region.yrange);
  size_t total_tiles = (xrange+1) * (yrange+1);
  //Calculate the expected error from the zero-information localization
  // (just pick the center of the room and calculate the RMSD from that)
  double null_x = xrange / 2.0;
  double null_y = yrange / 2.0;
  //Keep a running sum of the square distances and distances.
  //Divide by the number of tiles at the end to make these averages.
  double null_msd = 0.0;
  double null_md = 0.0;
  for (size_t x = 0; x <= xrange; ++x) {
    for (size_t y = 0; y <= yrange; ++y) {
      double sd = pow(null_x - x, 2.0) + pow(null_y - y, 2.0);
      null_msd += sd;
      null_md += sqrt(sd);
    }
  }
  region.null_rmsd = sqrt(null_msd/total_tiles);
  region.null_md = null_md/total_tiles;
}

vector<AntennaGain> makeAntennaGains(const vector<bool>& antenna_bits,
                                           const Region& region) {
  vector<AntennaGain> gains;

  //See if there aren't any options
  if (0 == antenna_bits.size()) {
    for (size_t rxer = 0; rxer < region.rxers.size(); ++rxer) {
      gains.push_back(region.antenna_gains.front());
    }
    return gains;
  }

  int bits_per_rxer = antenna_bits.size() / region.rxers.size();
  for (size_t start = 0; start+bits_per_rxer-1 < antenna_bits.size(); start += bits_per_rxer) {
    //Populate a new antenna setting.
    AntennaGain gain;
    //Determine the selection from the bits - boolean values represent 0 or 1
    uint64_t selection = 0;
    for (size_t bit = 0; bit < bits_per_rxer; ++bit) {
      selection = 2*selection + ( antenna_bits[start + bit] ? 1 : 0);
    }
    //Find which antenna and rotation this selection refers to
    auto cur_antenna = region.antenna_gains.begin();
    int cur_rotation = 0;
    while (0 < selection and cur_antenna != region.antenna_gains.end()) {
      //See if we need to go to the next antenna and skip this one
      if (selection > cur_antenna->size()) {
        selection -= cur_antenna->size();
        ++cur_antenna;
      }
      //Otherwise determine the proper rotation
      else {
        cur_rotation = selection;
        selection = 0;
      }
    }
    //Return an empty gain vector if this failed to select an antenna
    if (cur_antenna == region.antenna_gains.end()) {
      return vector<AntennaGain>();
    }
    //Store the selected antenna gain pattern at the selected rotation.
    gain = *cur_antenna;
    //cerr<<"Rotating by "<<cur_rotation<<" slices\n";
    rotate(gain.begin(), gain.begin() + (gain.size() - cur_rotation), gain.end());
    gains.push_back(gain);
  }
  return gains;
}

//Interpolate the expected value at the given angle
//(in radians from -pi to +pi) with the slice estimates in slice_vals.
double interpolateGain(double angle, vector<double> slice_vals) {
  //First find the size of the slices in radians
  double slice_size = 2*M_PI / slice_vals.size();
  //The first slice is centered at 0 degrees so to calculate which two slices
  //the given angle is between ignore the centering here:
  //change the range from 0 to 2PI so that rounding away from 0 is always correct.
  if (angle < 0) { angle = 2*M_PI + angle;}
  double tween_slice = angle/slice_size; //Somewhere between 0 and the number of slices
  int slice_one = trunc(tween_slice);
  int slice_two = slice_one + 1;
  if (slice_one >= slice_vals.size()) { slice_one -= slice_vals.size();}
  if (slice_two >= slice_vals.size()) { slice_two -= slice_vals.size();}
  double percent = remainder(angle, slice_size) / slice_size;
  return slice_vals[slice_one]*(1.0 - percent) + slice_vals[slice_two]*(percent);
}

//The expected change in signal strength at a given distance and the given attenuation coefficient, alpha
double expectedRSS(double distance, double alpha, double initial = -10) {
  return initial + -10 * log10(distance) * alpha;
}

/*
 * Return a score equivalent to the expected value of
 *  1 - (max overlap between any two receivers)
 * A score of 1 means no two receivers overlap, a score of 0 means
 * that one receiver is completely useless.
 */
float scoringFunction(vector<bool> antenna_bits, const Region& region) {
  //Generate a vector of antenna settings
  //If this is an invalid setting then return a score of 0.0
  vector<AntennaGain> gains = makeAntennaGains(antenna_bits, region);
  if (gains.size() == 0) {
    return 0.0;
  }
  //Find the number of points where the receivers would generate the same
  //signal within the rssi error if they were actually transmitting
  //(wireless links should be symetric so this should be correct)
  //This map goes from a pair of receiver IDs to the number of spaces where
  //their coverage overlaps
  typedef map<pair<size_t, size_t>, size_t> OverlapMap;
  OverlapMap overlaps;
  size_t total_points = floor(region.xrange + 1) * floor(region.yrange + 1);
  for (size_t x = 0; x <= region.xrange; ++x) {
    for (size_t y = 0; y <= region.yrange; ++y) {
      //Keep track of the expected rss values for all of the receivers
      //NOTE: We will assume an propagation coefficient alpha of 3
      //This is slightly better than what might usually be seen in
      //an indoor scattering environment
      const double alpha = 3.5;
      vector<double> expected_rsses;
      //Find the expected RSS for each receiver at this location.
      //While doing this also check if two receivers overlap by the
      //provided rss error.
      for (size_t rxer = 0; rxer < region.rxers.size(); ++rxer) {
        //Distance along x and y axes to get to the given location from the receiver
        double xdiff = x - region.rxers[rxer].first;
        double ydiff = y - region.rxers[rxer].second;
        //Get the angle, from -pi to pi
        double tx_angle = atan2( ydiff, xdiff);
        double dist = sqrt(pow(xdiff, 2) + pow(ydiff, 2));
        double expected_rss = expectedRSS(dist, alpha, interpolateGain(tx_angle, gains[rxer]));
        //For each previous receiver see if the current receiver could overlap the previous one
        for (size_t rxer2 = 0; rxer2 < rxer; ++rxer2) {
          if (abs(expected_rss - expected_rsses[rxer2]) < 2*region.rss_err) {
            auto key = make_pair(rxer, rxer2);
            if (overlaps.find(key) == overlaps.end()) { overlaps[key] = 0; }
            overlaps[key]++;
          }
        }
        //Remember the expected signal from this receiver
        expected_rsses.push_back(expected_rss);
      }
    }
  }
  size_t max_overlap = max_element(overlaps.begin(), overlaps.end(),
      [](const OverlapMap::value_type& left, const OverlapMap::value_type& right) { return left.second < right.second;})->second;
  return 1.0 - ((float)max_overlap / total_points);
}

/*
 * Return a score equivalent to the rmse or a localization algorithm
 * that matches fingerprints in tiles with a signal's fingerprint.
 * This is basically the expected performance of a variant of the SPM algorithm.
 */
float rmsdScoringFunction(vector<bool> antenna_bits, const Region& region) {
  //Generate a vector of antenna settings
  //If this is an invalid setting then return a score of 0.0
  vector<AntennaGain> gains = makeAntennaGains(antenna_bits, region);
  if (gains.size() == 0) {
    return 0.0;
  }
  //Generate a map of expected fingerprints
  size_t xrange = floor(region.xrange);
  size_t yrange = floor(region.yrange);
  vector<vector<vector<double>>> fingerprints(1+xrange, vector<vector<double>>(1+yrange));
  for (size_t x = 0; x <= xrange; ++x) {
    for (size_t y = 0; y <= yrange; ++y) {
      //Make the fingerprint by finding the expected RSS value for each receiver.
      //NOTE: We will assume an propagation coefficient alpha of 3
      //This is slightly better than what might usually be seen in
      //an indoor scattering environment
      const double alpha = 3.5;
      //Find the expected RSS for each receiver at this location.
      //While doing this also check if two receivers overlap by the
      //provided rss error.
      for (size_t rxer = 0; rxer < region.rxers.size(); ++rxer) {
        //Distance along x and y axes to get to the given location from the receiver
        double xdiff = x - region.rxers[rxer].first;
        double ydiff = y - region.rxers[rxer].second;
        //Get the angle, from -pi to pi
        double tx_angle = atan2( ydiff, xdiff);
        double dist = sqrt(pow(xdiff, 2) + pow(ydiff, 2));
        fingerprints[x][y].push_back(expectedRSS(dist, alpha, interpolateGain(tx_angle, gains[rxer])));
      }
    }
  }
  //Find the RMSD
  double sum_square_distances = 0;
  size_t total_tiles = (xrange+1) * (yrange+1);
  for (size_t x = 0; x <= xrange; ++x) {
    for (size_t y = 0; y <= yrange; ++y) {
      //Find all of the tiles spots that match the fingerprint in this spot
      //Loop over the x and y range again
      size_t total_matches = 0;
      double xsum = 0.0;
      double ysum = 0.0;
      for (size_t xx = 0; xx <= xrange; ++xx) {
        for (size_t yy = 0; yy <= yrange; ++yy) {
          bool match = equal(fingerprints[x][y].begin(), fingerprints[x][y].end(),
              fingerprints[xx][yy].begin(),
              [&](double a, double b)->bool { return abs(a - b) < 2*region.rss_err;});
          if (match) {
            ++total_matches;
            xsum += xx;
            ysum += yy;
          }
        }
      }
      double centerx = xsum / total_matches;
      double centery = ysum / total_matches;
      double square_dist = pow(centerx - x, 2.0) + pow(centery - y, 2.0);
      sum_square_distances += square_dist;
      //cerr<<"Fingerprint from tile "<<x<<", "<<y<<" matches "<<total_matches<<" tiles out of "<<total_tiles<<'\n';
      //cerr<<"Distance is "<<sqrt(square_dist)<<'\n';
    }
  }
  double rmsd = sqrt(sum_square_distances / total_tiles);
  //cerr<<"Calculating expected rmsd "<<rmsd<<" versus region null rmsd "<<region.null_rmsd<<'\n';
  //Return the expected performance improvement over the zero-information case
  return region.null_rmsd / rmsd;
}

int main(int ac, char** av) {
  if (ac != 5) {
    cerr<<"This program requires five arguments\n";
    cerr<<av[0]<<" <region file> <antenna file> <rss measurement error (for a signal to a distance function)> <CE, brute, or CErmsd>\n";
    return 0;
  }

  bool use_ce = true;
  if (string(av[4]) == "brute") {
    cout<<"Using brute force.\n";
    use_ce = false;
  }
  else if (string(av[4]) == "CE") {
    cout<<"Using cross entropy.\n";
    use_ce = true;
  }
  else if (string(av[4]) == "CErmsd") {
    cout<<"Using cross entropy and scoring on rmsd.\n";
    use_ce = true;
  }
  else {
    cout<<"Unknown strategy: "<<av[4]<<" - this must be either 'brute' or 'CE' or 'CErmsd'\n";
    return 0;
  }

  //Set the region parameters
  Region region;
  region.rss_err = stod(av[3]);
  {
    //Open the region file and populate the locations of the receiver
    ifstream in(av[1]);
    string line;
    bool range_set = false;
    while (getline(in, line)) {
      //Process this line if it is not empty and is not a comment
      if (0 < line.size() and line[0] != '#') {
        //If the range is set this should be a x,y location of a receiver
        //Otherwise this is the x and y range of the region.
        istringstream is(line);
        double x, y;
        if ( is >> x >> y) {
          if (range_set) {
            region.rxers.push_back(make_pair(x, y));
          }
          else {
            range_set = true;
            region.xrange = x;
            region.yrange = y;
          }
        }
      }
    }
  }
  //Set the null performance setting in this region.
  setNullPerformance(region);
  //Make sure there are receivers to find antennas for
  if (0 == region.rxers.size()) {
    cerr<<"There were no receiver definitions in "<<av[1]<<'\n';
    return 0;
  }
  cout<<"Finding best antennas for "<<region.rxers.size()<<" receivers\n";

  //Set the antenna orientation information
  //Keep track of the number of antenna permutations
  int antenna_permutations = 0;
  //Also keep track of the maximum number of slices in the antennas
  int max_slices = 0;
  {
    //Open the antenna gain file record different antenna gains
    ifstream in(av[2]);
    string line;
    while (getline(in, line)) {
      //Process this line if it is not empty and is not a comment
      if (0 < line.size() and line[0] != '#') {
        //Each number on the line represents the gain on a slice
        vector<double> antenna_gain;
        istringstream is(line);
        double gain;
        while (is >> gain) {
          antenna_gain.push_back(gain);
        }
        max_slices = max((int)antenna_gain.size(), max_slices);
        //Each orientation of each antenna is another permutation to explore
        antenna_permutations += antenna_gain.size();
        region.antenna_gains.push_back(antenna_gain);
      }
    }
  }
  //Make sure there are antennas to use
  if (0 == max_slices) {
    cerr<<"There were no useable antenna definitions in "<<av[2]<<'\n';
    return 0;
  }

  vector<AntennaGain> final_antennas;

  //Each receiver can have any of the antenna orientations
  antenna_permutations = pow(antenna_permutations, region.rxers.size());
  cout<<"There are "<<antenna_permutations<<" possible permutations.\n";
  double total_bits = log(antenna_permutations)/log(2);
  cout<<"Using "<<total_bits<<" bits to represent these permutations.\n";

  if (use_ce) {
    //Use cross entropy to find a good antenna configuration
    //First make the initial probability vector
    //Determine the number of bits needed to represent the antenna permutations
    int num_bits = ceil(total_bits);
    vector<float> antenna_probs(num_bits, 0.5);
    vector<CEIteration> results;
    //Cross entropy requires the quantile to reject after each round, the goal,
    //and minimum improvement between rounds. Scores are determined by the scoringFunction.
    if (string(av[4]) == "CE") {
      float quantile = 0.75;
      float goal = 1.0;
      float min_improve = 0.005;
      results = runCE(1 + total_bits * region.rxers.size() * 10,
          quantile, goal, min_improve, antenna_probs,
          [&](vector<bool> bits) { return scoringFunction(bits, region); });
    }
    else if (string(av[4]) == "CErmsd") {
      float quantile = 0.65;
      float goal = 10.0; //Ten times better than zero information
      float min_improve = 0.10;
      results = runCE(1 + total_bits * region.rxers.size(),
          quantile, goal, min_improve, antenna_probs,
          [&](vector<bool> bits) { return rmsdScoringFunction(bits, region); });
    }
    for (auto I = results.begin(); I != results.end(); ++I) {
      cout<<"Got average quantile performance "<<I->avg_quantile_performance<<'\n';
    }
    std::cout<<"Finished in "<<results.size()<<" rounds with quantile performance "<<results.rbegin()->avg_quantile_performance<<'\n';

    vector<bool> final_settings;
    for (auto I = results.rbegin()->probability_vector.begin(); I != results.rbegin()->probability_vector.end(); ++I) {
      final_settings.push_back(0.5 < *I);
    }
    if (string(av[4]) == "CE") {
      cout<<"Final score is "<<scoringFunction(final_settings, region)<<'\n';
    }
    else {
      float score = rmsdScoringFunction(final_settings, region);
      cout<<"Final score is "<<score<<" (corresponds to RMSD of "<<region.null_rmsd/score<<")\n";
    }
    final_antennas = makeAntennaGains(final_settings, region);
  }
  else {
    //Try out every permutation and find the best one
    float best_score = 0.0;
    for (size_t permutation = 0; permutation < antenna_permutations; ++permutation) {
      //Make a boolean vector representing this permutation.
      if (0 == (permutation % 1000)) {
        cout<<"On permutation "<<permutation<<'\n';
      }
      vector<bool> bits;
      for (size_t bit = 0; bit < total_bits; ++bit) {
        bits.push_back(0x1 & (permutation / (0x1 << bit)));
      }
      float score = scoringFunction(bits, region);
      if (score > best_score) {
        best_score = score;
        final_antennas = makeAntennaGains(bits, region);
      }
    }
    cout<<"Final score is "<<best_score<<'\n';
  }
  cout<<"Zero information mean error and root mean squared error are "<<
    region.null_md<<" and "<<region.null_rmsd<<'\n';

  cout<<"Final antenna settings are:\n";
  for (auto A = final_antennas.begin(); A != final_antennas.end(); ++A) {
    for (auto G = A->begin(); G != A->end(); ++G) {
      cout<<"\t"<<*G;
    }
    cout<<'\n';
  }

  return 0;
}

