#!/bin/bash

GRAILSERVER=grail1.winlab.rutgers.edu
GRAILPORT=7007

#Wait for the spanning tree to be set up and for us to be online
nohup echo "Sleeping..."
sleep 20
nohup echo "Awake."
nohup dhclient ath0

#Set up the wireless
#rmmod ath_pci
#rmmod ath_rate_sample
#rmmod wlan           
#rmmod ath_hal
#modprobe ath_pci
#ifconfig ath0 up
#iwconfig ath0 essid WINMAIN
#dhclient ath0
#If the wireless is working take down eth0
ADDRESS=`ifconfig | grep ath0 | awk '{print $5}'`;
#working=`ifconfig ath0 | grep "192.168"`;
#if [ "$working" != "" ];
#then
  #Kill all of the eth0 daemons
#  kill `ps ax | grep "dhclient eth0" | awk '{print $1}' | head -n 1`
#  kill `ps ax | grep "/sbin/udhcpc -i eth0" | awk '{print $1}' | head -n 1`
#  ifconfig eth0 down;
#fi
  
#Optional hub announcement at startup
#nohup echo "route add -net 224.0.0.0 netmask 224.0.0.0 eth0"
#nohup echo "/usr/local/sbin/hub_discover 7717 224.0.0.1"
#nohup route add -net 224.0.0.0 netmask 224.0.0.0 eth0
#nohup /usr/local/sbin/hub_discover 7717 224.0.0.1 &

for ((;;)); do
	#Start the wifi sensing layer.
	/usr/local/sbin/wifi_sense_layer "$ADDRESS" &
	#Wait one second before restarting.
	sleep 1;
done
