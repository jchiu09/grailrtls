#!/bin/bash

file0=./ath0.pid
file1=./ath1.pid
zero=0;
ADDRESS0=`ifconfig | grep ath0 | awk '{print $5}'|sed 's/-/:/g'|cut -c1-17`;
ADDRESS1=`ifconfig | grep ath1 | awk '{print $5}'|sed 's/-/:/g'|cut -c1-17`;
INTERFACE0=ath0;
INTERFACE1=ath1;
CHANNEL0=6;
CHANNEL1=6;

while true;
do

count=`pidof wifi_sense|wc -w`
j=1
if [ $count -neq $zero ]
then
	while [ $j -le $count ];
	do		
		current=`pidof wifi_sense|awk '{print $($j)}'`;
		dev=`ps current|awk '{print $7}'`
		
		if [ $dev -eq $INTERFACE0 ]
		then
			if [ ! -f "$file0"  ]
			then
				kill $current
				./wifi_sense "$ADDRESS0" "$INTERFACE0" "$CHANNEL0"  &
				echo $! > $file0
				sleep 1
			else
				i=1		
				this=`head -n $i $file0|tail -n 1`
				if [ $this -eq $current ];
				then 
					echo running
				else 
					kill $current
					./wifi_sense "$ADDRESS0" "$INTERFACE0" "$CHANNEL0"  &
					echo $! > $file0
					sleep 1
				fi
			fi
		
		elif [ $dev -eq $INTERFACE1 ]
		then
					if [ ! -f "$file1"  ]
					then
						kill $current
						./wifi_sense "$ADDRESS1" "$INTERFACE1" "$CHANNEL1"  &
						echo $! > $file1
						sleep 1
					else
						i=1		
						this=`head -n $i $file1|tail -n 1`
						if [ $this -eq $current ];
						then 
							echo running
						else 
							kill $current
							./wifi_sense "$ADDRESS1" "$INTERFACE1" "$CHANNEL1"  &
							echo $! > $file1
							sleep 1
						fi						
					fi			
		fi
	done			
elif [ $count -eq $zero ]
then 
			./wifi_sense "$ADDRESS0" "$INTERFACE0" "$CHANNEL0"  &
			echo $! > $file0
			./wifi_sense "$ADDRESS1" "$INTERFACE1" "$CHANNEL1"  &
			echo $! > $file1
			sleep 1
fi
done













