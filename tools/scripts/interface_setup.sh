#!/bin/bash

wlanconfig ath0 destroy
wlanconfig ath0 create wlandev wifi0 wlanmode monitor
ifconfig ath0 up
iwconfig ath0
iwconfig ath0 channel $1
#echo '803' > /proc/sys/net/ath0/dev_type

wlanconfig ath1 destroy
wlanconfig ath1 create wlandev wifi1 wlanmode monitor
ifconfig ath1 up
iwconfig ath1
iwconfig ath1 channel $2
#echo '803' > /proc/sys/net/ath1/dev_type

