#!/bin/bash
function die () {
	echo "$@" >&2
	exit 1
}

function start () {
	echo "/usr/local/sbin/grail3start.sh"
	nohup /usr/local/sbin/grail3start.sh &
	echo "/usr/local/sbin/grail3wifi_start_one.sh"
	nohup /usr/local/sbin/grail3wifi_start_one.sh &
}

function stop () {
	kill -9 `ps ax | grep pip_sense_layer | awk '{print $1}' | head -n 1`
	kill -9 `ps ax | grep grail3start.sh | awk '{print $1}' | head -n 1`
	kill -9 `ps ax | grep wifi_sense_layer | awk '{print $1}' | head -n 1`
	kill -9 `ps ax | grep grail3wifi_start.sh | awk '{print $1}' | head -n 1`
	#killall /usr/local/sbin/grail3start.sh
	#killall /usr/local/sbin/pip_sense_layer
}

case "$1" in
	(start)
		start
		;;
	(stop)
		stop
		;;
	(restart)
		stop
		start
		;;
	(*)
		#die "Format: $0 {stop | start | restart}"
		stop
		start	
		;;
esac
exit $?
