#!/bin/bash

file=./ath0.pid
zero=0;
ADDRESS=`ifconfig | grep ath0 | awk '{print $5}'|sed 's/-/:/g'|cut -c1-17`;
INTERFACE=ath0;
CHANNEL=6;

#./interface_setup.sh $CHANNEL $CHANNEL

while true;
do

count=`pidof wifi_sense|wc -w`
j=1
if [ $count -eq $zero ]
then 
			./wifi_sense "$ADDRESS" "$INTERFACE" "$CHANNEL"  &
			echo $! > $file
			

else
	while [ $j -le $count ];
	do		
		current=`pidof wifi_sense|awk '{print $($j)}'`;
		if [ ! -f "$file"  ]
		then
				kill $current
				./wifi_sense "$ADDRESS" "$INTERFACE" "$CHANNEL"  &
				echo $! > $file
				
		else
				i=1		
				this=`head -n $i $file|tail -n 1`
				if [ $this -eq $current ];
				then 
					echo running
				else 
					kill $current
					./wifi_sense "$ADDRESS" "$INTERFACE" "$CHANNEL"  &
					echo $! > $file
					
				fi
		fi
		
	done			
fi
done

./kill_wifi.sh

exit $?













