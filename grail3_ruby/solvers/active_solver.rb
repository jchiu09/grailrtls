################################################################################
# This solver identifies which sensors from an area are active and which
# are not currently transmitting or receiving.
################################################################################

require 'rubygems'
require 'client_world_model.rb'
require 'solver_world_model.rb'
require 'wm_data.rb'
require 'buffer_manip.rb'
require 'solver_aggregator'

require 'thread'

if (ARGV.length == 1 and
    ARGV[0] == '-?')
    puts "name: Activity Solver"
    puts "arguments: aggregator agg_solver worldmodel wm_solver wm_client" 
    puts "description: Detects what sensors are actively transmitting and which are off and logs this information in the world model." 
    puts "provides: active.bool" 
    puts "requires: sensor.*" 
    exit
end

if ARGV.length != 5
  puts "This solver requires 5 arguments:"
  puts "\t<aggregator IP> <aggregator port> <wm IP> <wm solver port> <wm client port>"
  exit
end

agg_ip   = ARGV[0]
agg_port = ARGV[1]
wm_ip       = ARGV[2]
solv_port   = ARGV[3]
client_port = ARGV[4]

def shutdown()
  puts "Exiting..."
  if (@cwm != nil and @cwm.connected)
    @cwm.close()
  end
  if (@swm != nil and @swm.connected)
    @swm.close()
  end
  #TODO FIXME The aggregator doesn't support this...
  #if (@aggregator != nil and @aggregator.connected)
  #  @aggregator.close()
  #end
  exit
end

Signal.trap("SIGTERM") {
  shutdown()
}

Signal.trap("SIGINT") {
  shutdown()
}

def getOctopusTime()
  t = Time.now
  return t.tv_sec * 1000 + t.usec/10**3
end

#TODO Record these
def recordRegions(uris)
  puts "Found regions:"
  uris.each{|uri|
    puts "\t #{uri}"
  }
end

#TODO Remember what sensors belong in which regions
#A mapping from sensor IDs to item names
@items = {}
@item_mutex = Mutex.new

#Keep track of regions and items in them
def updateItems(wmdata)
  puts "Got world data"
  if wmdata.attributes.length > 0
    sensor_id = wmdata.attributes[0].data
    phy = sensor_id.unpack('C')[0]
    id = unpackuint128(sensor_id[1, sensor_id.length-1])
    #@items[sensor_id] = wmdata.uri
    keystring = "#{phy}.#{id}"
    @item_mutex.synchronize do
      @items[keystring] = wmdata.uri
    end
    puts "Mapping #{keystring} to uri #{wmdata.uri}"
  end
end


################################################################################
#Connect to the aggregator to see which transmitters and receivers are active.
################################################################################
@aggregator = SolverAggregator.new(agg_ip, agg_port)

#Request packets from all physical layers, don't specify a transmitter ID or
#mask, and request packets every 10000 milliseconds (once every 10 seconds)
@aggregator.sendSubscription([AggrRule.new(0, [], 10000)])

################################################################################
#Connect as a client to find out which URIs have sensors associated with
#them and the names of different regions.
#We will associate different URIs with regions so that we can mark the status
#of a region as well as marking the status of the individual objects.
################################################################################
@cwm = ClientWorldModel.new(wm_ip, client_port, method(:updateItems), method(:recordRegions))

#Search for region names
@cwm.sendURISearch('region\\..*')
#Issue a request for anything with a sensor on it, updating every 10 seconds
@cwm.sendStreamRequest(".*", ['sensor.*'], 10000, 1)

################################################################################
#Now process information from the aggregator and world model and record
#what objects are active or inactive.
################################################################################

#TODO FIXME Just getting some initial data for testing, needs to be looped
cwm_thread = Thread.new do
  while (@cwm.connected)
    @cwm.handleMessage()
  end
end

#Store current item status
@active_times = {}
#Store what was last sent to the world model to see if updates
#are required
@active_status = {}

#Remember the start time so we give all of the items
#a chance to report in before reporting them inactive
@start_time = getOctopusTime()

#Connect to the world model as a solver to update active/inactive status
@swm = SolverWorldModel.new(wm_ip, solv_port, "grail/active solver\nversion 1.0")

while (@aggregator.handleMessage) do
  if (@aggregator.available_packets.length != 0) then
    #puts "Processing some packets!"
    for packet in @aggregator.available_packets do
      #TODO FIXME phy_layer should not be an array. This is a problem in the grail library
      keystring = "#{packet.phy_layer[0]}.#{packet.device_id}"
      rx_keystring = "#{packet.phy_layer[0]}.#{packet.receiver_id}"
      #puts "Got a packet from #{keystring}"
      @item_mutex.synchronize do
        if (@items.has_key?(keystring))
          item = @items[keystring]
          #Store the update time of this item
          @active_times[item] = getOctopusTime()
        end
        if (@items.has_key?(rx_keystring))
          item = @items[rx_keystring]
          #Store the update time of this item
          @active_times[item] = getOctopusTime()
        end
      end
    end
  end
  #TODO FIXME This should occur in a different thread in case
  #the aggregator blocks
  #Check the status of each item
  cur_time = getOctopusTime()
  updates = []
  @item_mutex.synchronize do
    @items.each do |key, value|
      #If this item was not active for 5 seconds mark it as missing
      if (false == @active_times.has_key?(value) and
          cur_time - @start_time > 5000)
          @active_times[value] = @start_time
      end
      if (@active_times.has_key?(value))
        isactive = (cur_time - @active_times[value]) < 5000
        if (false == @active_status.has_key?(value) or
            isactive != @active_status[value])
            @active_status[value] = isactive
            puts "Item #{value} is #{isactive ? "" : "in"}active."
            #Generate a solution for the world model here
            new_attr = WMAttribute.new('active.bool',
                                       [isactive ? 1 : 0].pack('C'),
                                       cur_time)
            updates.push(WMData.new(value, [new_attr]))
        end
      end
    end
  end
  #Send updates to the world model if any item's have changed status
  if (not updates.empty?)
    @swm.pushData(updates, false)
  end
end

