#Require rubygems for old (pre 1.9 versions of Ruby and Debian-based systems)
require 'rubygems'
require 'client_world_model.rb'
require 'solver_world_model.rb'
require 'wm_data.rb'
require 'buffer_manip.rb'

#For emailing
require 'pony'

if (ARGV.length != 2)
  puts "This program requires the email address and password of a gmail account."
  exit
end
@email=ARGV[0]
@pword=ARGV[1]

#Remember when the last brew event occured
@prev_happened = 0
#Wait for the idle latch to transition from idle to idle and back again
@latch_triggered = false

Signal.trap("SIGTERM") {
  puts "Exiting..."
  if (@cwm.connected)
    @cwm.close()
  end
  exit
}

Signal.trap("SIGINT") {
  puts "Exiting..."
  if (@cwm.connected)
    @cwm.close()
  end
  exit
}

def getOctopusTime()
  t = Time.now
  return t.tv_sec * 1000 + t.usec/10**3
end

def getHour()
  return Time.now.hour
end

TWENTY_MINUTES = 20 * 1000 * 60

#This function will check for tea time and send an email when it occurs
def checkForBrew(wmdata)
  puts "Got an idle response for #{wmdata.uri}"
  if wmdata.attributes.length > 0
    idle = wmdata.attributes[0]
    if idle.name == 'idle'
      puts "Got an idle update! Idle status is #{idle.data.unpack('C')[0]}"
      status = idle.data.unpack('C')[0]
      #Trigger the latch if the lid is open
      if (status == 0)
        @latch_triggered = true
        puts "#{wmdata.uri} latch is now open."
      elsif (@latch_triggered == true)
        @latch_triggered = false
        puts "#{wmdata.uri} latch released!"
        if (@prev_happened + TWENTY_MINUTES < getOctopusTime())
          @prev_happened = getOctopusTime()
          puts "#{wmdata.uri} is brewing coffee at time #{@prev_happened}"
          #Send out a notification email
          Pony.mail(:to        => 'bv2sn7-lt45nn@twittermail.com',
                    :subject   => 'fresh coffee',
                    :body      => "Someone is brewing fresh coffee at #{wmdata.uri}!",
                    :via => :smtp,
                    :via_options => {
                      :address   => 'smtp.gmail.com',
                      :port      => '587',
                      :enable_starttls_auto => true,
                      :domain    => 'grail.winlab',
                      :user_name => @email,
                      :password  => @pword})
        end
      end
    end
  end
end

wmip = 'grail1.winlab.rutgers.edu'
client_port = 7013
@cwm = ClientWorldModel.new(wmip, client_port, method(:checkForBrew))
#Subscribe to updates once every other second
@cwm.sendStreamRequest(".*coffee pot.*", ['idle'], 2000, 1)

while (@cwm.connected and @cwm.handleMessage() != ClientWorldModel::REQUEST_COMPLETE)
end

puts "Lost connection to world model!"

