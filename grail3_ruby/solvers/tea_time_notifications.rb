#Require rubygems for old (pre 1.9 versions of Ruby and Debian-based systems)
require 'rubygems'
require 'client_world_model.rb'
require 'solver_world_model.rb'
require 'wm_data.rb'
require 'buffer_manip.rb'

#For emailing
require 'pony'

if (ARGV.length != 2)
  puts "This program requires the email address and password of a gmail account."
  exit
end
@email=ARGV[0]
@pword=ARGV[1]

#Remember when tea time last occured
@prev_happened = 0

#Tea time occurs between 2 and 6 PM
@tea_hour_begin = 14
@tea_hour_end = 18

Signal.trap("SIGTERM") {
  puts "Exiting..."
  if (@cwm.connected)
    @cwm.close()
  end
  exit
}

Signal.trap("SIGINT") {
  puts "Exiting..."
  if (@cwm.connected)
    @cwm.close()
  end
  exit
}

def getOctopusTime()
  t = Time.now
  return t.tv_sec * 1000 + t.usec/10**3
end

HALF_DAY = 12 * 1000 * 60 * 60

#This function will check for tea time and send an email when it occurs
def checkForTea(wmdata)
  puts "Got a gathering response for #{wmdata.uri}"
  if wmdata.attributes.length > 0
    happening = wmdata.attributes[0]
    if happening.name == 'happening'
      cur_hour = Time.now.hour
      puts "Got a happening update!"
      if (happening.data.unpack('C')[0] == 1 and
        @prev_happened + HALF_DAY < getOctopusTime() and
        @tea_hour_begin <= cur_hour and
        cur_hour < @tea_hour_end)
        @prev_happened = getOctopusTime()
        puts "#{wmdata.uri} is happening at time #{@prev_happened}"

        #Send out a notification email
        Pony.mail(:to   => 'bv2sn7-lt45nn@twittermail.com', :subject => 'tea time',
                  :body => 'Tea time is happening!', :via => :smtp,
                  :via_options => { :address => 'smtp.gmail.com', :port => '587',
                    :enable_starttls_auto => true, :domain => 'grail.winlab',
                    :user_name => @email, :password => @pword})
      end
    end
  end
end

@cwm = ClientWorldModel.new('grail1.winlab.rutgers.edu', 7010, method(:checkForTea))
#Subscribe to updates once every minute
@cwm.sendStreamRequest(".*.gathering\.tea time", ['happening'], 60000, 1)

while (@cwm.connected and @cwm.handleMessage() != ClientWorldModel::REQUEST_COMPLETE)
end

puts "Lost connection to world model!"

