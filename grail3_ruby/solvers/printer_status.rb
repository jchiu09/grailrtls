#Grail 3 includes
require 'solver_distributor.rb'
require 'solution_types.rb'
require 'buffer_manip.rb'

#CUPS library
require 'cups'

if (ARGV.length != 3) then
  puts "This program requires three arguments."
  puts "The first two provide the ip address and port of a grail3 distributor."
  puts "The third argument is the region that this solver will run in."
  exit
end

puts "Connecting to #{ARGV[0]}:#{ARGV[1]} and making solutions in region #{ARGV[2]}"

#Set up a connection to the distributor
#Set up the printer type
types = [SolutionType.new('printer status')]
sd = SolverDistributor.new(ARGV[0], ARGV[1], types)
region = ARGV[2]

IDLE     = 3
PRINTING = 4
STOPPED  = 5

states = {}

#Check the printer time every second
while (1) do
  sleep 1.0
  for p in Cups.show_destinations
    #puts "Printer #{p}:"
    #puts "Options on printer #{p}:"
    opts = Cups.options_for(p)
    #puts Cups.options_for(p)
    #puts "Printer state is #{opts["printer-state"]}."
    case Integer(opts["printer-state"])
    when IDLE
      if (states[p] != "idle") then
        puts "Printer #{p} is idle."
        puts "Additional information: #{opts["printer-state-reasons"]}."
        t = Time.now
        sd.sendSolutions(region, t.usec / 10**3 + t.tv_sec*1000,
                         [ [p, 'printer status', strToUnicode("idle")] ])
      end
      states[p] = "idle"
    when PRINTING
      if (states[p] != "printing") then
        puts "Printer #{p} is printing."
        puts "Jobs on printer #{p}:"
        jobs = Cups.all_jobs(p)
        puts jobs
        puts "Additional information: #{opts["printer-state-reasons"]}."
        t = Time.now
        sd.sendSolutions(region, t.usec / 10**3 + t.tv_sec*1000,
                         [ [p, 'printer status', strToUnicode("printing")] ])
      end
      states[p] = "printing"
    when STOPPED
      if (states[p] != "stopped") then
        puts "Printer #{p} is stopped."
        puts "Additional information: #{opts["printer-state-reasons"]}."
        t = Time.now
        sd.sendSolutions(region, t.usec / 10**3 + t.tv_sec*1000,
                         [ [p, 'printer status', strToUnicode("stopped (reason #{opts["printer-state-reasons"]})")] ])
      end
      states[p] = "stopped"
    end
  end
end

