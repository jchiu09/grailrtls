require 'world_protocol.rb'
require 'world_data.rb'

ws = WorldServer.new('localhost', 7011)
uris = ws.uriSearch("*")
wd = ws.objectQuery(uris[0])
puts wd

wd2 = ws.objectDataQuery(uris[1])
puts wd2

fd = FieldData.new("test field", 0, 12334, "uint32_t", [128].pack('N'))
wdnew = WorldData.new("ruby.test field", [fd])

ws.pushData(wdnew)

puts "Searching for ruby field"
uris = ws.uriSearch("ruby.*")
if (uris.length > 0)
  puts uris
  wd3 = ws.objectDataQuery(uris[0])
  puts wd3
end
