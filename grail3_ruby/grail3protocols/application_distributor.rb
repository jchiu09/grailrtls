#This class abstracts the details of connecting to a
#GRAIL3 distributor as a client.

require 'socket'
require 'buffer_manip.rb'

class ClientDistributor
  #Message constants
  KEEP_ALIVE           = 0
  CERTIFICATE          = 1
  ACK_CERTIFICATE      = 2
  SOLUTION_PUBLICATION = 3
  SOLUTION_REQUEST     = 4
  SOLUTION_RESPONSE    = 5
  SOLUTION_SAMPLE      = 6
  HISTORIC_COMPLETE    = 7

  attr_accessor :name_to_alias, :alias_to_name, :available_solutions
  @name_to_alias
  @alias_to_name
  @available_solutions

  #Handle a message of currently unknown type
  def handleMessage()
    #Get the message length as n unsigned integer
    inlen = (@socket.recvfrom(4)[0]).unpack('N')[0]
    inbuff = @socket.recvfrom(inlen)[0]
    #Byte that indicates message type
    control = inbuff.unpack('C')[0]
    case control
    when SOLUTION_PUBLICATION
      decodeTypeSpec(inbuff[1, inbuff.length - 1])
      return SOLUTION_PUBLICATION
    when SOLUTION_SAMPLE
      decodeSolutionMessage(inbuff[1, inbuff.length - 1])
      return SOLUTION_SAMPLE
    when HISTORIC_COMPLETE
      return HISTORIC_COMPLETE
    else
      KEEP_ALIVE
    end
  end

  #Decode a type specification message
  def decodeTypeSpec(inbuff)
    num_types = inbuff.unpack('N')[0]
    rest = inbuff[4, inbuff.length - 1]
    new_sol_types = []
    for i in 1..num_types do
      type_alias = rest.unpack('N')[0]
      name, rest = splitURIFromRest(rest[4, rest.length - 1])
      new_sol_types.push(SolutionType.new(name, type_alias))
    end
    addSolutionTypes(new_sol_types)
  end

  #Decode a solution sample message
  def decodeSolutionMessage(inbuff)
    region_uri, rest = splitURIFromRest(inbuff)
    puts "Decoding solution from region #{region_uri}"
    sol_time = unpackuint64(rest)
    num_solutions = rest.unpack('x8N')[0]
    rest = rest[12, rest.length - 1]
    for i in 1..num_solutions do
      sol_alias = rest.unpack('N')[0]
      sol_target, rest = splitURIFromRest(rest[4, rest.length - 1])
      datalen = rest.unpack('N')[0]
      sol_data = rest[4, datalen]
      @available_solutions.push(SolutionData.new(region_uri, sol_time, sol_target, @alias_to_name[sol_alias], sol_data))
      rest = rest[4+datalen, rest.length - 1]
    end
  end

  def initialize(host, port)
    @connected = false
    @host = host
    @port = port
    @socket = TCPSocket.open(host, port)
    handshake = ""
    ver_string = "GRAIL client protocol"
    #The handshake is the length of the message, the protocol string, and the version (0).
    handshake << [ver_string.length].pack('N') << ver_string << "\x00\x00"
    #Receive a handshake and then send one
    @socket.recvfrom(handshake.length)
    @socket.send(handshake, 0)

    @name_to_alias = {}
    @alias_to_name = {}
    @available_solutions = []

    #Get the type specification message (should be the first message from the distributor)
    handleMessage
  end

  #Add some SolutionType objects to the known list
  def addSolutionTypes(sol_types)
    sol_types.each { |sol_type|
      @name_to_alias[sol_type.data_uri]   = sol_type.type_alias
      @alias_to_name[sol_type.type_alias] = sol_type.data_uri
    }
  end

  #Subscribe to the given solutions (referred to by name)
  def requestTypes(names, begin_time = 0, end_time = 0)
    num_aliases = 0
    aliases = ""
    for name in names do
      if (@name_to_alias[name])
        aliases += [@name_to_alias[name]].pack('N')
        num_aliases += 1
      else
        puts "Name #{name} is unknown."
      end
    end
    #Make the request message
    buff = [SOLUTION_REQUEST].pack("C") + packuint64(begin_time) +
      packuint64(end_time) + [num_aliases].pack('N') + aliases
         
    #Send the message with its length prepended to the front
    @socket.send("#{[buff.length].pack('N')}#{buff}", 0)
  end
end
