require 'application_distributor.rb'
require 'solution_types.rb'

#Making a new connection to the distributor as a client
cd = ClientDistributor.new('localhost', 7010)

#When you connect to the distributor as a client it immediately
#provides you with a list of available solution.
puts "Got #{cd.alias_to_name.length} aliases"
for name in cd.alias_to_name do
  puts "Got name #{name}"
end

#Request types by name, begin and end times (millseconds from midnight 1970)
#Differences between streaming and historic requests
cd.requestTypes(["coffee brewing"], 0, 2**60)

#Blocks until the distributor sends us a message
puts cd.handleMessage

#Any received solutions are put into the available_solutions
puts "There are #{cd.available_solutions.length} solutions"
for soln in cd.available_solutions do
  puts soln
end

#Body of your code will look like this
while (cd.handleMessage) do
  if (cd.available_solutions.length > 0) then
    #Process the solutions
  end
end


