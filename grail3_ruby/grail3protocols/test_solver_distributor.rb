require 'solver_distributor.rb'
require 'solution_types.rb'

#Make some test types and make a new solver distributor connection
types = [SolutionType.new("bork"), SolutionType.new('borkbork')]
sd = SolverDistributor.new('localhost', 7009, types)

t = Time.now
sd.sendSolutions('test region', t.usec / 10**3 + t.tv_sec*1000,
                 [ ['test1', 'bork', ['c'].pack('a')],
                   ['test2', 'borkbork', ['d'].pack('a')],
                   ['test3', 'borkborkbork', ['e'].pack('a')]])


