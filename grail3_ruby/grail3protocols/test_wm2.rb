#Require rubygems for old (pre 1.9 versions of Ruby and Debian-based systems)
require 'rubygems'
require 'client_world_connection.rb'
require 'wm_data.rb'
require 'buffer_manip.rb'

if (ARGV.length != 2)
  puts "This program needs the ip address and client port of a world model to connect to!"
  exit
end

wmip = ARGV[0]
port = ARGV[1]

#Connect as a client
cwm = ClientWorldConnection.new(wmip, port)

puts "Searching for uris"
result = cwm.URISearch('.*')
puts "Found uris #{result.get()}"

#Search for all uris and get all of their attributes
puts "Searching for all URIs and attributes"
result = cwm.snapshotRequest('.*', ['.*']).get()
puts "Got a result!"
result.each_pair {|uri, attributes|
  puts "Found uri \"#{uri}\" with attributes:"
  attributes.each {|attr|
    puts "\t#{attr.name}"
  }
}


