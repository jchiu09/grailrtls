class WorldData
  attr_accessor :object_uri, :fields

  #The URI of an object in the world server.
  @object_uri
  #Fields of this URI go into a vector of fields
  @fields
  def initialize(object_uri, fields)
    @object_uri = object_uri
    @fields = fields
  end

  #To string function
  def to_s()
    str = "#{@object_uri}: #{@fields.length} fields\n"
    @fields.each { |fd|
      str = str + "#{fd.description}: #{fd.data_type} -> #{fd.data}\n"
    }
    return str
  end
end

#Each object URI in the world server can have multiple fields
class FieldData
  attr_accessor :description, :creation_date, :expiration_date, :data_type, :data

  #The description of this field (string)
  @description
  #The creation date and the date when this field's data expires.
  #In milliseconds since Jan 1, 1970
  @creation_date
  @expiration_date
  #String description of the data type
  @data_type
  #Actual field data, in an array of binary data
  @data
  def initialize(description, creation, expiration, dtype, data)
    @description = description
    @creation_date = creation
    @expiration_date = expiration
    @data_type = dtype
    @data = data
  end
end
