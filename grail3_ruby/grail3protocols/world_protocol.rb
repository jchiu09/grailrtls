#This program will allow a ruby user to easily connect to a GRAIL3 world server.

require 'socket'
require 'world_data.rb'
require 'buffer_manip.rb'

def readWorldData(buff)
  obj_uri, rest = splitURIFromRest(buff)
  num_fields = rest.unpack('N')[0]
  rest = rest[4, rest.length - 1]
  #Create a world data object to store this world data as it is unpacked
  wd = WorldData.new(obj_uri, [])
  for i in 1..num_fields do
    description, rest = splitURIFromRest(rest)
    #There isn't a method to read 64 bit network endian values so stitch 32 bit
    #values together instead for the creation and expiration dates
    times = rest.unpack('N4')
    creation   = times[0]*(2**32) + times[1]
    expiration = times[2]*(2**32) + times[3]
    data_type, rest = splitURIFromRest(rest[16, rest.length - 1])
    data_len = rest.unpack('N')[0]
    data = rest[4, data_len]
    fd = FieldData.new(description, creation, expiration, data_type, data)
    wd.fields.push(fd);
    rest = rest[4+data_len, rest.length - 1]
  end
  return wd
end

def packWorldData(wd)
  buff = strToSizedUTF16(wd.object_uri)
  #Pack the number of fields as network byte order 32-bit integer
  buff += [wd.fields.length].pack('N')
  #Now pack each field
  wd.fields.each { |field|
    buff += strToSizedUTF16(field.description)
    buff += [field.creation_date/(2**32),field.creation_date % (2**32),
                field.expiration_date/(2**32), field.expiration_date % (2**32)].pack('NNNN')
    buff += strToSizedUTF16(field.data_type)
    buff += [field.data.length].pack('N')
    buff += field.data
  }
  return buff
end

class WorldServer

  #Message type number constants
  #A single character at the beginning of a message indicates its message type
  KEEP_ALIVE           = 0
  CERTIFICATE          = 1
  ACK_CERTIFICATE      = 2
  OBJECT_QUERY         = 3
  OBJECT_DATA_QUERY    = 4
  QUERY_RESPONSE       = 5
  URI_SEARCH           = 6
  URI_SEARCH_RESPONSE  = 7
  PUSH_DATA            = 8

  def initialize(host, port)
    @connected = false
    @host = host
    @port = port
    @socket = TCPSocket.open(host, port)
    handshake = ""
    ver_string = "GRAIL world protocol"
    #The handshake is the length of the message, the protocol string, and the version (0).
    handshake << [ver_string.length].pack('N') << ver_string << "\x00\x00"
    #Receive a handshake and then send one
    @socket.recvfrom(handshake.length)
    @socket.send(handshake, 0)
  end

  #Search for any uris that GLOB match the given uri string
  def uriSearch(uri)
    uni = strToUnicode(uri)
    #URI search is marked with a message value of 6
    buff = "#{[URI_SEARCH].pack("C")}#{[uni.length].pack("N")}#{uni}"
    @socket.send("#{[buff.length].pack('N')}#{buff}", 0)

    #Get the message length as n unsigned integer
    inlen = (@socket.recvfrom(4)[0]).unpack('N')[0]
    inbuff = @socket.recvfrom(inlen)[0]
    #Byte that indicates message type
    control = inbuff.unpack('C')
    num_uris = inbuff.unpack('x1N')[0]
    #Slice the strings out of the array
    rest = inbuff[5, inbuff.length - 1]
    uris = []
    for i in 1 .. num_uris do
      #Pull the URI out of the rest of the buffer
      str, rest = splitURIFromRest(rest)
      uris.push(str)
    end
    return uris
  end

  #Query for objects matching the string but do not return their data.
  def objectQuery(uri)
    uni = strToUnicode(uri)
    #Object query is marked with a message value of 3
    buff = "#{[OBJECT_QUERY].pack("C")}#{[uni.length].pack("N")}#{uni}"
    @socket.send("#{[buff.length].pack('N')}#{buff}", 0)

    #Unpack the message length from a network endian unsigned integer
    inlen = (@socket.recvfrom(4)[0]).unpack('N')[0]
    inbuff = @socket.recvfrom(inlen)[0]
    #Unpack the character that represents the message type
    control = inbuff.unpack('C')
    rest = inbuff[1, inbuff.length - 1]
    #Get the world data and return it
    wd = readWorldData(rest)
    return wd
  end

  #Query for objects matching the string and return their data.
  def objectDataQuery(uri)
    uni = strToUnicode(uri)
    #Object data query is marked with a message value of 4
    buff = "#{[OBJECT_DATA_QUERY].pack("C")}#{[uni.length].pack("N")}#{uni}"
    @socket.send("#{[buff.length].pack('N')}#{buff}", 0)

    #Unpack the message length from a network endian unsigned integer
    inlen = (@socket.recvfrom(4)[0]).unpack('N')[0]
    inbuff = @socket.recvfrom(inlen)[0]
    #Unpack the character that represents the message type
    control = inbuff.unpack('C')
    rest = inbuff[1, inbuff.length - 1]
    #Get the world data and return it
    wd = readWorldData(rest)
    return wd
  end
  
  #Push data to the world server
  def pushData(wd)
    #Pack the world data and put it into the message buffer
    buff = [PUSH_DATA].pack('C') + packWorldData(wd)
    #Prepend the length of the buffer to the message and send
    #it to the world server.
    @socket.send("#{[buff.length].pack('N')}#{buff}", 0)
  end

end

