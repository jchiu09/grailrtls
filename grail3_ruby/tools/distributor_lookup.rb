require 'application_distributor.rb'
require 'solution_types.rb'

if (ARGV.length < 3 or ARGV.length > 5) then
  puts "This program requires at least 3 command line arguments."
  puts "The first two are the ip address and port of a grail3 distributor."
  puts "The third argument is type of solution to request data about."
  puts "The fourth and fifth arguments (if provided) are the begin and end times of the query."
  puts "These times default to 0."
  exit
end

while (ARGV.length < 5)
  ARGV.push(0)
end

puts "Connecting to #{ARGV[0]}:#{ARGV[1]}"

#Making a new connection to the distributor as a client
cd = ClientDistributor.new(ARGV[0], ARGV[1])

#When you connect to the distributor as a client it immediately
#provides you with a list of available solution.
puts "Got #{cd.alias_to_name.length} aliases"
for name in cd.alias_to_name do
  puts "Got name #{name}"
end

#Request types by name, begin and end times (millseconds from midnight 1970)
#Differences between streaming and historic requests
puts "Requesting #{ARGV[2]} solutions from #{ARGV[3]} to #{ARGV[4]}."
cd.requestTypes([ARGV[2]], Integer(ARGV[3]), Integer(ARGV[4]))

#Blocks until the distributor sends us a message
puts cd.handleMessage

#Any received solutions are put into the available_solutions
puts "There are #{cd.available_solutions.length} solutions"
for soln in cd.available_solutions do
  puts soln
end

#Body of your code will look like this
while (cd.handleMessage) do
  if (cd.available_solutions.length > 0) then
    #Process the solutions
    soln = cd.available_solutions.slice!(0)
    puts soln
  end
end


