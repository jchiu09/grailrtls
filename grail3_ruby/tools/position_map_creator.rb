################################################################################
#This program will generate an html page that shows a map of an area with
#object in it based upon data from a world model server.
################################################################################

#Require rubygems for old (pre 1.9 versions of Ruby and Debian-based systems)
require 'rubygems'
require 'client_world_connection.rb'
require 'wm_data.rb'
require 'buffer_manip.rb'

require 'thread'

if (ARGV.length == 1 and
    ARGV[0] == '-?')
    puts "name: Position Map Creation"
    puts "arguments: worldmodel wm_client web_dir config_file" 
    puts "description: This creates a live status map (like a marauder's map)."
    puts "description: A static html page and a json representation of"
    puts "description: locations are created; AJAX is used to update the map."
    puts "The html file that is generated relies upon an icon image file and"
    puts "a map image file as well. The names are these files should appear"
    puts "in the configuration file."
    puts "creates: web_dir/location_map.html"
    puts "creates: web_dir/location_map.json"
    puts "requires: location.xoffset" 
    puts "requires: location.yoffset"
    puts "requires: location.maxx" 
    puts "requires: location.maxy"
    puts "config_file: key_value overlay A transparent image that sets the size of icons."
    puts "config_file: key_value icons A vertically tiled image of multiple icons."
    puts "config_file: key_value hover A vertically tiled image of multiple hover icons."
    puts "config_file: key_value map The map image."
    puts "config_file: key_tuple icon_size 2 The width and height of the icons."
    puts "config_file: key_value default_icon The 0-based offset of the default icon."
    puts "config_file: key_tuple* class_icon 2 The item class followed by the icon offset (0 based)."
    puts "config_file: key_tuple* class_state 3 The item class followed, state name, and icon offset (0 based)."
    exit
end

if (ARGV.length != 4)
  puts "This program requires 4 arguments: <world model ip> <client port> <output directory> <config file>"
  exit
end

wmip = ARGV[0]
client_port = ARGV[1]
@outdir = ARGV[2]
config_file=ARGV[3]

@config = {}
@icon_offsets = {}
@icon_state_offsets = {}
#Get configuration options
File.foreach(config_file) { |line|
  sides = line.chomp.split("=")
  case sides[0]
  when "overlay", "icons", "hover", "map"
    @config[sides[0]] = sides[1]
  when "icon_size"
    opts = sides[1].split(" ")
    @config["icon_width"] = opts[0].to_i
    @config["icon_height"] = opts[1].to_i
  when "default_icon"
    @config["default_icon"] = sides[1].to_i
  when "class_icon"
    opts = sides[1].split(" ")
    @icon_offsets[opts[0]] = opts[1].to_i
  when "class_state"
    opts = sides[1].split(" ")
    @icon_state_offsets[opts[0]] = [opts[1], opts[2].to_i]
  else
    puts "Unrecognized configuration option \"#{sides[0]}\""
  end
}
if (not (@config.has_key? "overlay" and @config.has_key? "icons" and
         @config.has_key? "hover" and @config.has_key? "map" and
         @config.has_key? "icon_width" and @config.has_key? "icon_height" and
         @config.has_key? "default_icon"))
  puts "Configuration is incomplete."
  exit
end

def getOctopusTime()
  t = Time.now
  return t.tv_sec * 1000 + t.usec/10**3
end

@html_a="<!DOCTYPE html>
<html>
  <head>
    <style type='text/css'>
      img.icon{
        width:#{@config["icon_width"]}px;
        height:#{@config["icon_height"]}px;
        margin-left: -#{@config["icon_width"]/2}px;
        margin-top: -#{@config["icon_height"]/2}px;
        background:url(#{@config["icons"]}) 0px -#{@config["default_icon"]*@config["icon_height"]}px;
        visibility:visible;
      }
      img.icon:hover{
        background:url(#{@config["hover"]}) 0px -#{@config["default_icon"]*@config["icon_height"]}px;
        visibility:visible;
      }"
@icon_offsets.each_pair{|name, offset|
  @html_a +="
      img.#{name}{
        background:url(#{@config["icons"]}) 0px -#{offset*@config["icon_height"]}px;
        visibility:visible;
      }
      img.#{name}:hover{
        background:url(#{@config["hover"]}) 0px -#{offset*@config["icon_height"]}px;
        visibility:visible;
      }"
}
@icon_state_offsets.each_pair{|base_name, pair|
  state = pair[0]
  offset = pair[1]
  name = "#{base_name}_#{state}"
  @html_a +="
      img.#{name}{
        background:url(#{@config["icons"]}) 0px -#{offset*@config["icon_height"]}px;
        visibility:visible;
      }
      img.#{name}:hover{
        background:url(#{@config["hover"]}) 0px -#{offset*@config["icon_height"]}px;
        visibility:visible;
      }"
}
@html_a +="
      .wrapper{
        position: relative;
        border-width: 0px;
        outline-width: 0px;
        margin-left: 0px;
        margin-top: 0px;
        padding: 0px;
        /*overflow: hidden;*/
      }
      .wrapper img {
        position: absolute;
        z-index: 2;
      }
      .map{
        position: absolute;
        z-index:1;
      }
    </style>
    <title>Status Map</title>
    <script type='text/javascript' src='jx.js'>
    </script>
    <script type='text/javascript'>
      <!--
      //Set up the div to have the correct width and height to match the map
      function setupDiv() {
        //The width must be 100% to match the map's 100% width.
        //We only need to adjust the height to match the map's height
        var map_height = document.getElementById('map').height + 'px';
        var div_style = 'padding: 0px; position: relative; width: 100%; height: '+map_height+';';
        document.getElementById('wrapper').setAttribute('style', div_style);
      }
      //Update the document with input
      function update(data) {
        //Clean up the input and then evaluate as a JSON string
        data = data.replace(/[\\n\\r]/g,' ');
        try {
            data = eval('('+data+')'); 
          //Update the time field
          document.getElementById('time_field').innerHTML = \"Last updated \" + (new Date()).toLocaleString();
          //Redraw drawables if their attributes have changed
          if (data.drawables != null) {
            for (X in data.drawables) {
              drawable = data.drawables[X]
              //Insert the image if this is a new object
              if (null == document.getElementById(drawable.name)) {
                document.getElementById('wrapper').innerHTML += drawable.img_html
              }
              else {
                document.getElementById(drawable.name).setAttribute('class', drawable.class);
              }
              //Update the position
              eval(drawable.jsstyle)
            }
          }
        }
        catch (err)
        {
          //The page was probably in the middle of refreshing. Just ignore.
        }
        //Start a timer to request updates in 1 second
        setTimeout(\"jx.load('location_map.json', update, 'text');\", 1000);
      }
      //TODO Mouseover events that display an object's status (via title)
      //Also, real-time updates
      function setPositions() {
        setupDiv();
        document.getElementById('time_field').innerHTML = \"Last updated \" + (new Date()).toLocaleString();
"
#After part A insert javascript to set the margins for each image
#based upon their size. This is to make placement based upon the
#image centers. After that close the function with part B.
#Javascript should look like this:
#        var test1_style = "left:50%;top:50%;";
#        document.getElementById('test1').setAttribute("style", test1_style);

@html_b="        //Also start a timer to request updates in 1 second
        setTimeout(\"jx.load(\'location_map.json\', update, \'text\');\", 1000);
      }
      function setStyle(class_name, vis) {
        var cssRuleCode = document.all ? 'rules' : 'cssRules'; //Account for browser differences
        for (sheet in document.styleSheets) {
          //Try to add the visibility property in case it doesn't exist
          try {
            document.styleSheets[sheet].insertRule(class_name + ' { visibility:'+vis+'; }', document.styleSheets[sheet][cssRuleCode].length);
          } catch (err) {
            try {
              document.styleSheets[sheet].addRule(class_name + ' { visibility:'+vis+'; }');
            } catch (err) {
              try {
                //If there is an existing rule
                for (rule in document.styleSheets[sheet][cssRuleCode]) {
                  if (document.styleSheets[sheet][cssRuleCode][rule].selectorText == class_name &&
                      document.styleSheets[sheet][cssRuleCode][rule].style['visibility']) {
                    document.styleSheets[sheet][cssRuleCode][rule].style['visibility'] = vis;
                  }
                }
              } catch (err) {
              }
            }
          }
        }
      }
      function hideAllOf(class_name) {
        setStyle(class_name, 'hidden');
      }
      function showAllOf(class_name) {
        setStyle(class_name, 'visible');
      }
      function setVisibility(class_name, value) {
        if (value) {
          setStyle(class_name, 'visible');
        }
        else {
          setStyle(class_name, 'hidden');
        }
      }

      //-->
    </script>
  </head>
  <body onload='setPositions()' onresize='setupDiv()' >
    <script type='text/javascript'> 
    function showChoices() { 
      var status = document.getElementById('cbChoices').checked; 
      if (document.getElementById('ScrollCB').style.display == 'block') {
        document.getElementById('ScrollCB').style.display = 'none';
      } 
      else {
        document.getElementById('ScrollCB').style.display = 'block';
      }
    } 
    </script> 
    <table border='0'><tr>
    <td>
      <p id='time_field'></p>
    </td>
    <td>
      <button id='cbChoices' onclick='showChoices()'>Select Visible Objects
    <td id='ScrollCB' style='border:1px solid blue;display:none'>"
#Set up toggles to turn different item classes visible in invisible.
@icon_offsets.each_pair{|name, offset|
  @html_b +="
    <input type='checkbox' id='#{name}_toggle' checked='checked' value='img.#{name}' onchange='setVisibility(this.value, this.checked)'>#{name}<br>"
}
@html_b +="
    </td>
    </tr></table>
    <div class='wrapper' id='wrapper'>
    <img class='map' id='map' src='#{@config["map"]}' width='100%' />
"

#Now insert the map information and an image for each object, like this:
#      <img class='icon' id='test1' alt='test1' title='Test 1' src='antenna.png' />
@html_c="    </div>
  </body>
</html>"


@map_width = 0
@map_height = 0

class Drawable
  attr_accessor :img_url, :name, :xoffset, :yoffset, :xorigin, :yorigin
  @class_name
  def initialize(name, overlay, states)
    @xorigin = ""
    @yorigin = ""
    #Default to an antenna image
    @img_url = 'antenna.png'
    #Replace the period character with underscores in the name
    @name = name.split(/[. ]/).join('_')
    @actual_name = name
    @class_name = name.split('.')[-2]
    if (@class_name == nil)
      @class_name = ""
    else
      @class_name = @class_name.split(/[ ]/).join('_')
    end
    @overlay = overlay
    if (states.has_key? @class_name)
      @alt_state = states[@class_name][0]
    else
      @alt_state = nil
    end
  end

  def make_offsets(width, height)
    percent_x = "left:#{100*xoffset / width}%;"
    percent_y = "top:#{100 - 100*yoffset / height}%;"
    return "'#{percent_x}#{percent_y}'"
  end

  #Print out the javascript that sets the width and height
  def to_javascript(width, height)
    return "        document.getElementById('#{name}').setAttribute('style', #{make_offsets(width, height)});"
  end

  def get_class(state)
    cur_class = @class_name
    if (state == nil or @alt_state == nil)
      return cur_class
    end
    #Check if a state is active
    if (state.has_key? @alt_state and 0 != state[@alt_state])
      cur_class = "#{@class_name}_#{@alt_state}"
    end
    return cur_class
  end

  #Print out the img element
  def to_image(state = nil)
    return "<img class='icon #{get_class(state)}' id='#{name}' src='#{@overlay}' alt='#{@actual_name}' />"
  end

  #Print out a json representation for live updates
  def to_json(width, height, state)
    #Store the basic parts of this drawable, along with
    #the style, which depends upon the map width and height
    return '{"name" : "'+"#{@name}"+'", "img_url" : "'+"#{@img_url}"+
      '", "class" : "'+"icon #{get_class(state)}"+
      '", "actual_name" : "'+"#{@actual_name}"+
      '", "jsstyle" : "'+to_javascript(width, height)+
      '", "img_html" : "'+to_image()+'"}'
  end
end

#Array of drawables
@items = {}
@device_status = {}

def redrawMap()
  if (@map_width != 0 and @map_height != 0)
    html_str = @html_a
    #Output the javascript to set positions
    @items.each do |key, value|
      html_str += "#{value.to_javascript(@map_width, @map_height)}\n"
    end
    html_str += @html_b
    @items.each do |key, value|
      html_str += "#{value.to_image(@device_status[key])}\n"
    end
    html_str += @html_c
    File.open("#{@outdir}/location_map.html", 'w') {|file|
      file.write(html_str)
    }
    #Also write out the updates into a single file. If the client's web
    #browser supports javascript and AJAX runs correctly then it will
    #GET updates from this file.
    #Begin a JSON array for all drawables
    json_str = '{"drawables":['+"\n"
    #Now output the drawables
    json_array = []
    @items.each do |key, value|
      json_array.push("#{value.to_json(@map_width, @map_height, @device_status[key])}")
    end
    json_str += json_array.join(",\n")
    #Close the JSON notation
    json_str += ']}'
    File.open("#{@outdir}/location_map.json", 'w') {|file|
      file.write(json_str)
    }
  end
end

#Connect as a client
@cwc = ClientWorldConnection.new(wmip, client_port)

if (not @cwc.connected)
  puts "Could not connect!"
  exit
end

#Prefer localization results behind results set by people
@m2_origin = "grail/localization_solver\nversion 1.0\nAlgorithm M2"
@cwc.setOriginPreference([ [@m2_origin,0] ])

#Update region information every 10 seconds
@region_request = @cwc.streamRequest("region.*", ['location.maxx', 'location.maxy'], 5000)

#Update locations every second
@location_request = @cwc.streamRequest(".*", ['location.xoffset', 'location.yoffset'], 2000)

#Update door closed, chair empty, and device on status every second
@status_request = @cwc.streamRequest(".*", ['(closed|empty|on)'], 1000)

status_thread = Thread.new do
  while (@cwc.connected and not @status_request.isComplete())
    status_data = @status_request.next()
    @data_mutex.synchronize do
      status_data.each_pair {|uri, attributes|
        if (not @device_status.has_key? uri)
          @device_status[uri] = {}
        end
        attributes.each {|attr|
          @device_status[uri][attr.name] = attr.data.unpack('C')[0]
        }
      }
      redrawMap()
    end
  end
end


@data_mutex = Mutex.new

region_thread = Thread.new do
  while (@cwc.connected and not @region_request.isComplete())
    region_data = @region_request.next()
    #Don't try to print any information while we are updating the region info
    @data_mutex.synchronize do
      region_data.each_pair {|uri, attributes|
        #TODO Handle multiple regions
        attributes.each {|attr|
          if attr.name == 'location.maxx'
            @map_width = attr.data.unpack('G')[0]
            #puts "maxx data is #{attr.data.unpack('C*').join(',')}"
          elsif attr.name == 'location.maxy'
            @map_height = attr.data.unpack('G')[0]
          end
        }
      }
      puts "Updated width and height #{@map_width}, #{@map_height}"
      #Now redraw the map
      redrawMap()
    end
  end
end

while (@cwc.connected and not @location_request.isComplete())
  location_data = @location_request.next()
  @data_mutex.synchronize do
    location_data.each_pair {|uri, attributes|
      #Location update
      #Make a drawable object if it doesn't exist
      if (@items.has_key?(uri) == false)
        @items[uri] = Drawable.new(uri, @config["overlay"], @icon_state_offsets)
        #TODO For now drawing everything with antenna.png
        @items[uri].img_url = 'antenna.png'
      end
      attributes.each {|attr|
        xorigin = @items[uri].xorigin
        yorigin = @items[uri].yorigin
        if attr.name == 'location.xoffset' and (xorigin == "" or xorigin == @m2_origin)
          @items[uri].xoffset = attr.data.unpack('G')[0]
          @items[uri].xorigin = attr.origin
        elsif attr.name == 'location.yoffset' and (yorigin == "" or yorigin == @m2_origin)
          @items[uri].yoffset = attr.data.unpack('G')[0]
          @items[uri].yorigin = attr.origin
        end
      }
    }
    #Now redraw the map
    redrawMap()
  end
end

