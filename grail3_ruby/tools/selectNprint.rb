#! /usr/bin/ruby
#Require rubygems for old (pre 1.9 versions of Ruby and Debian-based systems)
require 'rubygems'
require 'client_world_model.rb'
require 'solver_world_model.rb'
require 'wm_data.rb'
require 'buffer_manip.rb'
require "Map.rb"
 @image = "cleanlab"


#Make a new display room.
lab = Map.new
lab.setBackground @image
lab.write "Here's a Winlab layout !"
lab.draw
puts "Where's the file?"
@file = gets

@count


def printURIs(uris)
  puts "URI response:"
  uris.each{|uri|
    puts "\t #{uri}"
  }
  @count=uris.length
end

def printDetails(wmdata)
	b = readUnsizedUTF16(wmdata.attributes[1].data)
	a = readUnsizedUTF16(wmdata.attributes[0].data)
	name = wmdata.uri
	puts "#{name}"
	puts "#{a}"
	puts "#{b}"
	`/usr/sbin/lpadmin -p #{name} -E -v socket://#{a} -m #{b}`
	`lpr -P #{name} #{@file}`
end

def getprint(printername)
	#Now connect as a client
	cwm = ClientWorldModel.new('grail1.winlab.rutgers.edu', 7013, method(:printDetails), method(:printURIs))

	puts "Searching for URIs"
	cwm.sendURISearch(printername)

	#Handle the next message (will be a URI search response)
	cwm.handleMessage()
	i=0
	cwm.sendStreamRequest(printername,  ['ipaddress.uri','ppd.uri'], 2000, 1)
	while (i < @count)
		while (cwm.handleMessage() != ClientWorldModel::DATA_RESPONSE)
		end
	i+=1
	end
end



while event = SDL::Event.wait
     case event
       when SDL::Event::MouseButtonDown
		x = event.x      	
		y = event.y
		#puts "#{x}"
       		#puts "#{y}"
		diffx1 = (x - lab.printer1x)*(x - lab.printer1x)
		diffy1 = (y - lab.printer1y)*(y - lab.printer1y)
		diff1 = Math.sqrt(diffx1+diffy1)
		
		diffx2 = (x - lab.printer2x)*(x - lab.printer2x)
		diffy2 = (y - lab.printer2y)*(y - lab.printer2y)
		diff2 = Math.sqrt(diffx2+diffy2)
		
		if (diff1 <= 5)
			puts "Its the addon printer"
			getprint "winlab.printer.addon"
		elsif (diff2 <= 5) 
			puts "Its the common printer" 
			getprint "winlab.printer.common"
		else
			puts "Click on a real printer!"	
		end
       when SDL::Event::MouseMotion
     
      end
end

wait = $stdin.gets().chomp!().downcase
