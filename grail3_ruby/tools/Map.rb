include ObjectSpace
require 'sdl'

   

class Map
  attr_reader :printer1x, :printer1y, :printer2x, :printer2y

  @@sdlinited = false
  @@screen = nil
  

  def initialize()
    #Initialize the SDL video subsystem if it was not previously initialized
    if (false == @@sdlinited) then
      SDL.init(SDL::INIT_VIDEO)
      SDL::TTF.init()
      @@sdlinited = true
      begin
        #use a nice font from http://openfontlibrary.org/font/modernantiqua
        @@font = SDL::TTF.open('ModernAntiqua/ModernAntiqua.ttf', 18)
      rescue
        puts "Could not open font!"
      end
    end
    @items = Array.new
    @nbg = nil
    @printer1x = 110
    @printer1y = 390
    @printer2x = 300 
    @printer2y = 460
  
  end

  def setBackground(filename)

	big_endian = ([1].pack("N") == [1].pack("L"))

	if big_endian
	  rmask = 0xff000000
	  gmask = 0x00ff0000
	  bmask = 0x0000ff00
	  amask = 0x000000ff
	else
	  rmask = 0x000000ff
	  gmask = 0x0000ff00
	  bmask = 0x00ff0000
	  amask = 0xff000000
	end


    #Try to open the file and print an error if this fails.
    #Try multiple filename extensions
    [".jpg",".bmp",".png",".jpeg"].each { |ext|
      begin
        #Destroy the old background to free resources before loading a new one
        if (nil != @nbg) then @nbg.destroy() end
        @nbg = SDL::Surface.load(filename+ext)
	@bg = SDL::Surface.new(SDL::SWSURFACE, @nbg.w/2, @nbg.h/2, 32, rmask, gmask, bmask, amask);
	SDL::Surface.transformDraw(@nbg,@bg,0,0.5,0.5,0,0,0,0,SDL::Surface::TRANSFORM_SAFE)
	@@screen = SDL::Screen.open(@bg.w, @bg.h, 32, SDL::SWSURFACE)
	@bg.draw_filled_circle(@printer1x, @printer1y,5,[255,0,0])
	@bg.draw_filled_circle(@printer2x, @printer2y,5,[255,0,0])
	
        #Make the screen the right size for the background image
        
        return true
      rescue SDL::Error
      end
    }
    puts "Could not open any files named #{filename}.[jpg, bmp, png, jpeg]"
    #If everything failed return false
    return false
  end

  def addItem(filename)
    #Try to open the file and print an error if this fails.
    #Try multiple filename extensions
    [".jpg",".bmp",".png",".jpeg"].each { |ext|
      begin
        surf = SDL::Surface.load(filename+ext)
        #Enable alpha transparency in the image
        surf.displayFormatAlpha
        @items.push(surf)
        return true
      rescue SDL::Error
      end
    }
    puts "Could not open any files named #{filename}.[jpg, bmp, png, jpeg]"
    #If everything failed return false
    return false
  end

  def write(str)
    #Write out a string
    img = @@font.renderBlendedUTF8(str, 0xCF, 0xCF, 0x4F)
    if (img != nil) then
      @items.push(img)
    end
  end

  def draw()
    #Clear the background color to black
    @@screen.fillRect(0,0,@@screen.w,@@screen.h,0)

    if (nil != @@screen and nil != @bg)
      #Draw the background image
      @@screen.put(@bg,0,0)

      #Put the list of items onto the screen
      curheight = 0
      @items.each { |img|
        @@screen.put(img, 0, curheight)
        curheight = curheight + img.h + 10
      }

      #Update the screen
      @@screen.updateRect(0,0,0,0)
    end
  end

  def getInput()
    #while event = SDL::Event2.poll
    #end
    event = SDL::Event2.poll
    return event
  end
end

