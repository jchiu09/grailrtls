#Require rubygems for old (pre 1.9 versions of Ruby and Debian-based systems)
require 'rubygems'
require 'client_world_model.rb'
require 'solver_world_model.rb'
require 'wm_data.rb'
require 'buffer_manip.rb'

def getOctopusTime()
  t = Time.now
  return t.tv_sec * 1000 + t.usec/10**3
end

#Make a function to print out URIs that match a search pattern
#Print out directories with a number of the items in them
@cur_directory = ''
def printURIs(uris)
  directories = {}
  uris.each{|uri|
    if (@cur_directory != '')
      uri.match("#{@cur_directory}\.(.*)")
      name = $1
      #puts $1
    else
      name = uri
      #puts uri
    end
    if (name == nil)
      name = ''
    end
    #Check for a directory or item
    if (name.split('.').length > 1)
      directory = name.split('.')[0]
      #puts "name is #{name} in directory #{directory}"
      #Directory - remember how many items are in it
      if (directories[directory] == nil)
        directories[directory] = 0
      end
      directories[directory] += 1
    else
      #Full item
      puts name
    end
  }
  #Print out all of the directories
  for key,val in directories
    puts "#{key} (#{val})"
  end
end

def printDetails(wmdata)
  puts wmdata
end

if (ARGV.length != 4)
  puts "This program requires 4 arguments: <world model ip> <solver port> <client port> <origin ID>"
  exit
end

wmip = ARGV[0]
solver_port = ARGV[1]
client_port = ARGV[2]
origin = ARGV[3]

#The third argument is the origin name, which should be your solver or
#client's name
puts "Connecting as a solver..."
swm = SolverWorldModel.new(wmip, solver_port, origin)
#Now connect as a client
puts "Connecting as a client..."
cwm = ClientWorldModel.new(wmip, client_port, method(:printDetails), method(:printURIs))
puts "Connected."

cur_ticket = 1

print "#{@cur_directory}$"
while (line = $stdin.gets())
  #Clean up the line and then pull out the command and its arguments
  line.chomp!()
  line =~ /([a-zA-Z]+)(.*)/
  cmd = $1
  args = $2.split(' ')
  #puts "command is #{cmd} with args #{args}"
  case cmd
  when 'exit'
    exit
  when 'quit'
    exit
  when 'ls'
    cwm.sendURISearch("#{@cur_directory}.*")
    #Handle the next message (will be a URI search response)
    while (cwm.handleMessage() != ClientWorldModel::URI_RESPONSE)
    end
  when 'cd'
    if (args.length == 0)
      @cur_directory = ''
    else
      dirs_left = args.join(' ').split('.')
      dirs_left.each{|dir|
        if (dir == '\\')
          @cur_directory =~ /(.*)\..*?/
          if ($1 != nil)
            @cur_directory = $1
          else
            @cur_directory = ''
          end
        else
          if (@cur_directory != '')
            @cur_directory += '.'
          end
          @cur_directory += dir
        end
      }
    end
  when 'rm'
    target = args.join(' ')
    if (@cur_directory != '')
      swm.deleteURI("#{@cur_directory}.#{target}")
    else
      swm.deleteURI("#{target}")
    end
  when 'create'
    if (args.length == 0)
      puts "The create command requires a URI to create"
    else
      target = args.join(' ')
      #Make a new attribute
      if (@cur_directory != '')
        pre = @cur_directory + '.'
      else
        pre = ''
      end
      new_uri = pre + target
      #Send the create URI message
      swm.createURI(new_uri, getOctopusTime())
    end
  when 'detail'
    target = args.join(' ')
    if (@cur_directory != '')
      cwm.sendSnapshotRequest("#{@cur_directory}.#{target}", ['.*'], cur_ticket)
    else
      cwm.sendSnapshotRequest("#{target}", ['.*'], cur_ticket)
    end
    cur_ticket += 1
    #Handle the next three messages (the first will be a ticket, the second will be data)
    while (cwm.handleMessage() != ClientWorldModel::REQUEST_COMPLETE)
      sleep 0.01
    end
  when 'stream'
    update_interval = 5000
    target = args.join(' ')
    if (@cur_directory != '')
      cwm.sendStreamRequest("#{@cur_directory}.#{target}", ['.*'], update_interval, cur_ticket)
    else
      cwm.sendStreamRequest("#{target}", ['.*'], update_interval, cur_ticket)
    end
    cur_ticket += 1
    #Handle the next three messages (the first will be a ticket, the second will be data)
    while (cwm.handleMessage() != ClientWorldModel::REQUEST_COMPLETE)
      sleep 0.01
    end
  when 'history'
    if args.length < 3
      puts "The history command needs three arguments: the uri name, the begin time, and the end time"
    else
      end_time = Float(args[args.length-1])
      begin_time = Float(args[args.length-2])
      name_args = args[0, args.length-2]
      target = name_args.join(' ')
      if (@cur_directory != '')
        cwm.sendRangeRequest("#{@cur_directory}.#{target}", ['.*'], cur_ticket, begin_time, end_time)
      else
        cwm.sendRangeRequest("#{target}", ['.*'], cur_ticket, begin_time, end_time)
      end
      cur_ticket += 1
      #Handle the next three messages (the first will be a ticket, the second will be data)
      while (cwm.handleMessage() != ClientWorldModel::REQUEST_COMPLETE)
        sleep 0.01
      end
    end
  when 'attribute'
    if (args.length != 3)
      puts "The attribute command creates a new attribute."
      puts "This requires the item name, attribute name, and value."
    else
      #Make a new attribute
      if (@cur_directory != '')
        pre = @cur_directory + '.'
      else
        pre = ''
      end
      attrib = WMAttribute.new(args[1], [10].pack('N'), getOctopusTime())
      #Making a solution with a single attribute
      new_data = WMData.new(pre + args[0], [attrib])
      #Set the second argument to true to create the object with the given
      #URI if it does not already exist
      swm.pushData([new_data], true)
    end
  when 'help'
    puts 'Try regular shell commands like "quit", "ls", or "cd"'
    puts '"cd \\" goes up a directory, "cd" with no arguments go to the root.'
    puts 'The "create" command creates and item, "rm" can delete it.'
  else
    puts "Unknown command: #{cmd}"
  end
  print "#{@cur_directory}$"
end

