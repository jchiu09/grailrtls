################################################################################
#This program will generate an html page that shows which devices are working
#and not working in an Octopus deployment.
################################################################################

#Require rubygems for old (pre 1.9 versions of Ruby and Debian-based systems)
require 'rubygems'
require 'client_world_model.rb'
require 'wm_data.rb'
require 'buffer_manip.rb'

if (ARGV.length == 1 and
    ARGV[0] == '-?')
    puts "name: Health Page Creator"
    puts "arguments: worldmodel wm_client web_dir" 
    puts "description: Creates a web page that shows whether the sensors attached to items are active or inactive." 
    puts "provides:" 
    puts "requires: active.bool" 
    puts "creates: health.html" 
    exit
end

if (ARGV.length != 3)
  puts "This program requires 3 arguments: <world model ip> <client port> <output directory>"
  exit
end

wmip = ARGV[0]
client_port = ARGV[1]
@outfile = ARGV[2] + "/health.html"

def getOctopusTime()
  t = Time.now
  return t.tv_sec * 1000 + t.usec/10**3
end

@active_status = {}

@css="<style type=\"text/css\">\n.active{color: green;}\n.inactive{color: red;}\n</style>"


def updateActivity(wmdata)
  if wmdata.attributes.length > 0
    active = wmdata.attributes[0].data.unpack('C')[0]
    @active_status[wmdata.uri] = (active != 0)
    File.open(@outfile, 'w') {|file|
      file.write("<!DOCTYPE html>\n<html>\n<head>#{@css}\n<title>Active Status</title>\n</head>\n")
      file.write("<body>\n")
      @active_status.each do |key, value|
        file.write("#{key} is <span class=#{value ? '"active">' : '"inactive">in'}active</span><br>\n")
      end
      file.write("</body>\n</html>")
    }
  end
end

#Connect as a client
puts "Connecting as a client..."
cwm = ClientWorldModel.new(wmip, client_port, method(:updateActivity))
puts "Connected."

#Update every five seconds
cwm.sendStreamRequest(".*", ['active.bool'], 5000, 1)

while (cwm.handleMessage())
end

