#! /usr/bin/ruby

if (ARGV.length == 1 and
    ARGV[0] == '-?')
    puts "name: UI Test Template"
    puts "arguments: aggregator agg_solver worldmodel wm_solver wm_client web_dir" 
    puts "description: This is the first line of a description. Note that"
    puts "description: the ordering of description lines should be preserved"
    puts "description: when generating output or parsing it."
    puts "provides: attribute.complex.1"
    puts "provides: attribute.complex.2"
    puts "description: This a continuation of the description, and should be"
    puts "description: combined with the previous lines."
    puts "requires: attribute.basic.1" 
    puts "requires: attribute.basic.3"
    puts "creates: test.html"
    puts "creates: dir1/page1.php"
    exit
end

if (ARGV.length == 6)
  web_dir = ARGV[5]
  File.open("#{web_dir}/test.html", 'w') {|file|
    file.write("<!DOCTYPE html>\n<html>\n<head>\n<title>Test Page</title>\n</head>\n")
    file.write("<body>\n")
    file.write("<p>I am a test!</p>\n")
    file.write("</body>\n</html>")
  }
  Dir.mkdir("#{web_dir}/dir1", 0750)
  File.open("#{web_dir}/dir1/page1.php", 'w') {|file|
    file.write("<!DOCTYPE html>\n<html>\n<head>\n<title>Test Page</title>\n</head>\n")
    file.write("<body>\n")
    file.write("<?php phpinfo(); ?>\n")
    file.write("</body>\n</html>")
  }
end
