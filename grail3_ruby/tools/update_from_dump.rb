puts "Reading file #{ARGV[0]}"

if (ARGV.length != 3)
  puts "This program requires 3 arguments: <SQL dump file name> <world model ip address> <world model port>"
  exit
end

wmip = ARGV[1]
wmport = ARGV[2]

#Build commands from the beginning of an INSERT to the semicolon at the end
cmd = ""

origin_files = Hash.new
origins = []

File.open(ARGV[0], "r").each do |line|
  cmd += line
  if (cmd[cmd.length-2, 1] == ";")
    #puts "Command is #{cmd}"
    #Parse this out for the gen_wm_data solver
    if (cmd.match(/INSERT INTO "attributes" VALUES\('(.*?)','(.*?)',(.*?),(.*?),'(.*?)',X'(.*?)'\);/m))
      origin = $5
      if (origin.length != 0)
        out_line = "#{$1}\t#{$2}\t#{$3}\t#{$6}\n"
        if (origin_files.has_key?(origin) == false)
          o_name = origin.gsub(/\n/m, "\\n").gsub(/\//m, "\\")
          origin_files[origin] = File.new("#{o_name}", File::CREAT|File::TRUNC|File::RDWR)
          origins.push(origin)
          puts "Making temporary file #{o_name}"
        end
        origin_files[origin].write(out_line)
      end
    end

    #Empty the command string for the next SQL command
    cmd = ""
  end
end

files = []
origins.each do|origin|
  fname = origin_files[origin].path
  origin_files[origin].close
  puts "gen_wm_data #{wmip} #{wmport} \"#{origin}\" \"#{origin_files[origin].path}\""
  system ("gen_wm_data #{wmip} #{wmport} \"#{origin}\" \"#{origin_files[origin].path}\"")
  files.push(fname)
end

files.each do|fname|
  system("rm \"#{fname}\"")
end

