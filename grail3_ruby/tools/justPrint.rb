#Require rubygems for old (pre 1.9 versions of Ruby and Debian-based systems)
require 'rubygems'
require 'client_world_model.rb'
require 'solver_world_model.rb'
require 'wm_data.rb'
require 'buffer_manip.rb'

@count


def printURIs(uris)
  puts "URI response:"
  uris.each{|uri|
    puts "\t #{uri}"
  }
  @count=uris.length
end

def printDetails(wmdata)
#puts wmdata.attributes
#puts wmdata.attributes[0].data
#puts wmdata.attributes[1].data

b = readUnsizedUTF16(wmdata.attributes[0].data)
a = readUnsizedUTF16(wmdata.attributes[1].data)
name = wmdata.uri
puts "#{name}"
puts "#{a}"
puts "#{b}"
`/usr/sbin/lpadmin -p #{name} -E -v socket://#{a} -m #{b}`
`lpr -P #{name} ~/Documents/test_print`
end

#Now connect as a client
cwm = ClientWorldModel.new('grail1.winlab.rutgers.edu', 7013, method(:printDetails), method(:printURIs))

puts "Searching for URIs"
cwm.sendURISearch('winlab.printer.*')

#Handle the next message (will be a URI search response)
cwm.handleMessage()
i=0
cwm.sendStreamRequest('winlab.printer.*',  ['ipaddress.uri','ppd.uri'], 2000)
while (i<@count)
	while (cwm.handleMessage() != ClientWorldModel::DATA_RESPONSE)
	end
i+=1
end






