#Require rubygems for old (pre 1.9 versions of Ruby and Debian-based systems)
require 'rubygems'
require 'client_world_model.rb'
require 'solver_world_model.rb'
require 'wm_data.rb'
require 'buffer_manip.rb'

if (ARGV.length != 1)
  puts "This program requires 1 argument: <output file name>"
  exit
end

@outfile = ARGV[0]
@fractionx;@fractiony
@count
@maxx;@maxy
@ip;@ppd
@printerx;@printery
@canvas_width=800
@canvas_height=550
@printer = 1


def printURIs(uris)
  puts "URI response:"
  uris.each{|uri|
  puts "\t #{uri}"
  }
  @count=uris.length
  puts "No. of URI: #{@count}"
end


def getLocation(wmdata)
  if (wmdata.ticket == 1) 	
	@maxy = wmdata.attributes[1].data.unpack('G')[0]
	@maxx = wmdata.attributes[0].data.unpack('G')[0]
	#puts "I got the map dimensions !"
	#puts @maxx
	#puts @maxy
     File.open(@outfile, 'w') {|file|
      file.write("<!DOCTYPE html>\n<html>\n<head>\n<title>Printer Selection</title> <h1 align=\"middle\">Select and Print!</h1>\n")
      file.write("</head>\n")
      file.write("<body>\n")
     
      file.write("<div style=\"float:left; margin:0; width: 65%;\">
	
                 <canvas id=\"myCanvas\" width=\"#{@canvas_width}\" height=\"#{@canvas_height}\" style=\"position=relative; \">\n")
      file.write("<img src=\"cleanlab.jpg\" alt=\"Fallback Content\" width=\"#{@canvas_width}\" height=\"#{@canvas_height}\" />\n")
      file.write("</canvas> </div>\n")
      file.write("<script type=\"text/javascript\">\n")
      file.write("window.onload=function(){\n")	
      file.write("hide();
		  setDefaults();
		  var c=document.getElementById(\"myCanvas\");\n")
      file.write("var ctx=c.getContext(\"2d\"); 
                 c.style.border = \"black 2px solid\";\n")
       file.write("var img=new Image();\n")
      file.write("img.src=\"cleanlab.jpg\";\n")
      file.write("ctx.drawImage(img,0,0,#{@canvas_width},#{@canvas_height});\n")
      file.write("ctx.translate(0,#{@canvas_height});\n")
      file.write("ctx.scale(1,-1);\n")
      file.write("ctx.fillStyle=\"#FF0000\";\n")
      file.write("ctx.beginPath();\n")
     
      }	
  
   elsif (wmdata.ticket == 5)
      wmdata.attributes.each do |attr| 
	
      case attr.name
	when "ipaddress.uri"
	  @ip = readUnsizedUTF16(attr.data)
	when "ppd.uri"
	  @ppd = readUnsizedUTF16(attr.data)
	when "location.xoffset"
	  @printerx = attr.data.unpack('G')[0]
	when "location.yoffset"
	  @printery = attr.data.unpack('G')[0]
	end

     end

	#puts "#{@ppd}"
	#puts @printerx
	
	if (@printer==1)
	@fractionx1=@printerx/@maxx
	@fractiony1=@printery/@maxy
	@printer=2
	elsif (@printer==2)
	@fractionx2=@printerx/@maxx
	@fractiony2=@printery/@maxy
	end
	
    end

end


	#Connect as a client
	puts "Connecting as a client..."
	cwm = ClientWorldModel.new('grail1.winlab.rutgers.edu', 7010, method(:getLocation), method(:printURIs))
	puts "Connected."
	cwm.sendURISearch("winlab.printer.*")
	puts "Searching for URIs"
	cwm.handleMessage()
	cwm.sendSnapshotRequest("region.winlab.*",  ['location.maxx','location.maxy'], 1)
	
	#cwm.handleMessage()
	
	
	i=0
	cwm.sendSnapshotRequest("winlab.printer.*",  ['ipaddress.uri','ppd.uri','location.xoffset','location.yoffset'], 5)
	while (i < @count)
		while (cwm.handleMessage() != ClientWorldModel::REQUEST_COMPLETE)
		end
	i+=1
	end
	File.open(@outfile, 'a') {|file|	
	file.write("ctx.arc(#{@fractionx1*@canvas_width},#{@fractiony1*@canvas_height},6,0,Math.PI*2,true);\n")	
	file.write("ctx.arc(#{@fractionx2*@canvas_width},#{@fractiony2*@canvas_height},6,0,Math.PI*2,true);\n")	
	file.write("ctx.closePath();\n")
        file.write("ctx.fill();\n}\n")
	
	file.write("document.addEventListener(\"DOMContentLoaded\", init, false);\n")
	file.write("function init()
      {
        var canvas = document.getElementById(\"myCanvas\");
        canvas.addEventListener(\"mousedown\", getPosition, false);
      }

      function getPosition(event)
      {
        var x = new Number();
        var y = new Number();
        var canvas = document.getElementById(\"myCanvas\");

        if (event.x != undefined && event.y != undefined)
        {
          x = event.x;
          y = event.y;
        }
        else // Firefox method to get the position
        {
          x = event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
          y = event.clientY + document.body.scrollTop + document.documentElement.scrollTop;
        }

        x -= canvas.offsetLeft;
        y -= canvas.offsetTop;
	y = #{@canvas_height}-y;

   
    
		diffx1 = (x - #{@fractionx1*@canvas_width});
		diffy1 = (y - #{@fractiony1*@canvas_height});
		x1 = Math.pow(diffx1,2);
		y1 = Math.pow(diffy1,2);
		diff1 = Math.sqrt(x1+y1);
		
		
		diffx2 = (x - #{@fractionx2*@canvas_width});
		diffy2 = (y - #{@fractiony2*@canvas_height});
		x2 = Math.pow(diffx2,2);
		y2 = Math.pow(diffy2,2);
		diff2 = Math.sqrt(x2+y2);
		
		if (diff1 <= 15)
			{
			document.getElementById(\"printer\").value = \"winlab.printer.addon\";
	   		//alert(\"The file will be printed to the addon printer!\");
			document.getElementById(\"selection\").value = \"winlab.printer.addon\"
			}
		else if (diff2 <= 15 ) 
			{
			//alert(\"The file will be printed to the common printer!\");
	       		document.getElementById(\"printer\").value = \"winlab.printer.common\";
			document.getElementById(\"selection\").value = \"winlab.printer.common\"
			}
		else
			//alert(\"Click on a real printer!\");		
			document.getElementById(\"selection\").value = \"none\"
      }

function submitForm()
	{
	  document.myform.submit();
	}
function show() { document.getElementById('start').style.visibility = 'visible';
                  document.getElementById('last').style.visibility = 'visible'; }

function hide() { document.getElementById('start').style.visibility = 'hidden';
		  document.getElementById('last').style.visibility = 'hidden'; }

function setDefaults(){
    if(document.getElementById('numcopies').value==\"\"){
        document.getElementById('numcopies').value = \"1\";
    }
}

\n")

        file.write("</script>\n");
	file.write("<div style=\" float:right; margin:0; width: 35%;\"><form name= \"myform\" action=\"upload.php\" method=\"POST\" enctype=\"multipart/form-data\">
      Your file will be printed to the following printer: <br />
 
<select name=\"selection\" id=\"selection\">
  <option value=\"none\">No printer selected</option>
  <option value=\"winlab.printer.addon\">Addon Printer</option>
  <option value=\"winlab.printer.common\">Common Printer</option>
</select>
<br /> <br /> <br />
<label for=\"file\">Filename:</label>
      <input type=\"file\" name=\"file\" id=\"file\" /> 
      <input type=\"hidden\" name=\"printer\" id=\"printer\" />  </br> </br> </br>

      <input type=\"radio\" name=\"side\" value=\"single\" /> Single Sided Printing<br />
      <input type=\"radio\" name=\"side\" value=\"double\" checked/> Double Sided Printing<br /> </br>

      <input type=\"radio\" name=\"range\" value=\"all\" onclick=\"hide();\" checked /> Print all the pages<br />
      <input type=\"radio\" name=\"range\" value=\"range\" onclick=\"show();\" /> Specify Page Range </t>
      <input type=\"text\" name=\"start\" id=\"start\" size=\"5\" /></t>
      <input type=\"text\" name=\"last\" id=\"last\" size=\"5\"/><br /> </br> 


      <input type=\"checkbox\" name=\"fit\" value=\"fit\" /> Fit to page<br /> </ br>
     <br>
	 <input type=\"checkbox\" name=\"collate\" value=\"collate\" /> Collate<br /> <br /> 
	Orientation: 
	<br> 
      <input type=\"radio\" name=\"orient\" value=\"potrait\" checked /> Potrait<br />
      <input type=\"radio\" name=\"orient\" value=\"landscape\" /> Landscape </t>
	
<br /> <br />

      Number of copies: <input type=\"text\" name=\"numcopies\" id=\"numcopies\" size=\"8\" /><br />
               

      <br>
     <button onclick=\"javascript: submitForm();\"> Print my file... </button>
      </form> </div>\n")
        file.write("</body>\n</html>\n")
	}

	
	


