#! /usr/bin/ruby
#Require rubygems for old (pre 1.9 versions of Ruby and Debian-based systems)
require 'rubygems'
require 'client_world_model.rb'
require 'solver_world_model.rb'
require 'wm_data.rb'
require 'buffer_manip.rb'


if (ARGV.length != 8)
  puts "This program requires 8 arguments: <file name> <printer name> and the options !"
  exit
end

$file = ARGV[0]
$printer = ARGV[1]
$copies = ARGV[2]
$sides = ARGV[3]
$range = ARGV[4]
$option4 = ARGV[5]
$orient = ARGV[6]
$collation = ARGV[7]

def printURIs(uris)
  puts "URI response:"
  uris.each{|uri|
    puts "\t #{uri}"
  }
  @count=uris.length
end


def printDetails(wmdata)
        a = ""
        b = ""
	wmdata.attributes.each{|attr|
		if (attr.name == 'ipaddress.uri')
			a = readUnsizedUTF16(attr.data)
		elsif (attr.name == 'ppd.uri')
			b = readUnsizedUTF16(attr.data)
		end
	}
	name = wmdata.uri
	#puts "#{name}"
	#puts "#{a}"
	#puts "#{b}"
	#`/usr/sbin/lpadmin -p #{name} -E -v socket://#{a} -P #{b}`
	#`lpr -P #{name} -n 2 #{$file}`
	#`lp -d #{name} -n 2 -o sides=two-sided-long-edge -o page-ranges=1-3 #{$file}`
	#puts "#{$copies}"
	#puts "#{$sides}"
	#puts "#{$range}"
	#puts "#{$option4}"
	#puts "#{$file}"
	#puts "lp -d #{name} -n #{$copies} -o #{$sides} -o #{$range} -o #{$option4} -o #{$orient} #{$file}"
	if ($range == "all" && $option4 == "nofit")
		`lp -d #{name} -n #{$copies} -o #{$sides} -o #{$orient} -o media=Letter -o #{$collation} #{$file}`
		
	elsif ($range == "all" && $option4 == "fit-to-page")
		`lp -d #{name} -n #{$copies} -o #{$sides} -o #{$range} -o #{$option4} -o #{$orient} -o media=Letter -o #{$collation} #{$file}`
	else
		`lp -d #{name} -n #{$copies} -o #{$sides} -o #{$range} -o #{$orient} -o media=Letter -o #{$collation} #{$file}`
	end	
end



	#Now connect as a client
	cwm = ClientWorldModel.new('grail1.winlab.rutgers.edu', 7010, method(:printDetails), method(:printURIs))

	puts "Searching for URIs"
	cwm.sendURISearch($printer)

	#Handle the next message (will be a URI search response)
	cwm.handleMessage()
	i=0
	cwm.sendSnapshotRequest($printer,  ['ipaddress.uri','ppd.uri'], 5)
	while (i < @count)
		while (cwm.handleMessage() != ClientWorldModel::REQUEST_COMPLETE)
		end
	i+=1
	end



