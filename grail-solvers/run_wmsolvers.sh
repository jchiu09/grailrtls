#! /bin/bash

#Not all solvers need the world server though.
agg_ip="localhost"
agg_solv_port="7008"

wm_ip="localhost"
wm_solv_port="7009"
wm_client_port="7010"

sleep_delay=1

grail3start() {
  # Start the fingerprint solver
  nohup unbuffer ./bin/fingerprint_solver $agg_ip $agg_solv_port $wm_ip $wm_solv_port 1000 6000 >fingerprint_solver.log &
  [ "$?" -eq 0 ] && echo "Started fingerprint solver" || echo "Failed to start fingerprint solver"
  sleep $sleep_delay

  # Start the temperature solver
  nohup unbuffer ./bin/temperature_solver2 $agg_ip $agg_solv_port $wm_ip $wm_solv_port $wm_client_port >temperature_solver2.log &
  [ "$?" -eq 0 ] && echo "Started temperature solver" || echo "Failed to start temperature solver"
  sleep $sleep_delay

  # Start the mobility solver
  nohup unbuffer ./bin/mobility_solver2 $wm_ip $wm_solv_port $wm_client_port >mobility_solver2.log &
  [ "$?" -eq 0 ] && echo "Started mobility solver" || echo "Failed to start mobility solver"
  sleep $sleep_delay

  # Start the localization solver
  nohup unbuffer ./bin/localization_solver2 $wm_ip $wm_solv_port $wm_client_port >localization_solver2.log &
  [ "$?" -eq 0 ] && echo "Started localization solver" || echo "Failed to start localization solver"
  sleep $sleep_delay

  # Start the switch solver
  nohup unbuffer ./bin/switch_solver2 $agg_ip $agg_solv_port $wm_ip $wm_solv_port $wm_client_port switch_types.conf >switch_solver2.log &
  [ "$?" -eq 0 ] && echo "Started switch solver" || echo "Failed to start switch solver"
  sleep $sleep_delay

  # Start the gathering solver
  nohup unbuffer ./bin/gathering_solver $wm_ip $wm_solv_port $wm_client_port >gathering_solver.log &
  [ "$?" -eq 0 ] && echo "Started gathering solver" || echo "Failed to start gathering solver"
  sleep $sleep_delay

  # Start the location discrimination trainer
  nohup unbuffer ./bin/location_fingerprinter $wm_ip $wm_solv_port $wm_client_port >location_fingerprinter.log &
  [ "$?" -eq 0 ] && echo "Started location fingerprinting solver" || echo "Failed to start location fingerprinting solver"
  sleep $sleep_delay

  # Start the location discriminator
  nohup unbuffer ./bin/location_discriminator $wm_ip $wm_solv_port $wm_client_port >location_discriminator.log &
  [ "$?" -eq 0 ] && echo "Started location discrimination solver" || echo "Failed to start location discrimination solver"
  sleep $sleep_delay
}

grail3stop() {
  kill `ps x -ww | grep fingerprint_solver | awk '{print $1}' | head -n 1`
  sleep 1
  kill `ps x -ww | grep mobility_solver2 | awk '{print $1}' | head -n 1`
  sleep 1
  kill `ps x -ww | grep temperature_solver2 | awk '{print $1}' | head -n 1`
  sleep 1
  kill `ps x -ww | grep localization_solver2 | awk '{print $1}' | head -n 1`
  sleep 1
  kill `ps x -ww | grep switch_solver2 | awk '{print $1}' | head -n 1`
  sleep 1
  kill `ps x -ww | grep gathering_solver | awk '{print $1}' | head -n 1`
  sleep 1
  kill `ps x -ww | grep location_fingerprinter | awk '{print $1}' | head -n 1`
  sleep 1
  kill `ps x -ww | grep location_discriminator | awk '{print $1}' | head -n 1`
  sleep 1
}

case "$1" in
  start)
    grail3start
  ;;
  stop)
    grail3stop
  ;;
  restart)
    grail3stop
    grail3start
  ;;
  *)
  echo "usage $0 start|stop|restart"
esac

