#! /bin/bash

#Add solvers like this:
# <solver> [<server ip> <server port>]* <distributor ip> <distributor port> <world server ip> <world server port>
#Not all solvers need the world server though.
#agg_ip="127.0.0.1"
agg_ip="grail1.winlab.rutgers.edu"
agg_solv_port="7008"

#distrib_ip="127.0.01"
distrib_ip="grail1.winlab.rutgers.edu"
dist_solv_port="7009"
dist_client_port="7010"

#worldip="127.0.0.1"
worldip="grail1.winlab.rutgers.edu"
worldport="7011"

sleep_delay=10

grail3start() {
  # Start the mobility solver
  nohup expect_unbuffer ./bin/mobility_solver $agg_ip $agg_solv_port $distrib_ip $dist_solv_port >mobility_solver.log &
  [ "$?" -eq 0 ] && echo "Started mobility solver" || echo "Failed to start mobility solver"
  sleep $sleep_delay
  
  # Start the temperature solver
  nohup expect_unbuffer ./bin/temperature_solver $agg_ip $agg_solv_port $distrib_ip $dist_solv_port >temperature_solver.log &
  [ "$?" -eq 0 ] && echo "Started temperature solver" || echo "Failed to start temperature solver"
  sleep $sleep_delay

  # Start the coffee solver
  nohup expect_unbuffer ./bin/coffee_solver $distrib_ip $dist_solv_port $dist_client_port $worldip $worldport >coffee_solver.log &
  [ "$?" -eq 0 ] && echo "Started coffee solver" || echo "Failed to start coffee solver"
  sleep $sleep_delay

  # Start the localization solver
  nohup expect_unbuffer ./bin/localization_solver $agg_ip $agg_solv_port $distrib_ip $dist_solv_port $worldip $worldport >localization_solver.log &
  [ "$?" -eq 0 ] && echo "Started localization solver" || echo "Failed to start localization solver"
  sleep $sleep_delay

  # Start the switch solver
  nohup expect_unbuffer ./bin/switch_solver $agg_ip $agg_solv_port $distrib_ip $dist_solv_port $worldip $worldport switch_types.conf >switch_solver.log &
  [ "$?" -eq 0 ] && echo "Started switch solver" || echo "Failed to start switch solver"
  sleep $sleep_delay

  # Start the tea time solver
  nohup expect_unbuffer ./bin/tea_time_solver $distrib_ip $dist_solv_port $dist_client_port $worldip $worldport >tea_time_solver.log &
  [ "$?" -eq 0 ] && echo "Started tea time solver" || echo "Failed to start tea time solver"
  sleep $sleep_delay
}

grail3stop() {
  kill `ps x | grep mobility_solver | awk '{print $1}' | head -n 1`
  sleep 1
  kill `ps x | grep temperature_solver | awk '{print $1}' | head -n 1`
  sleep 1
  kill `ps x | grep coffee_solver | awk '{print $1}' | head -n 1`
  sleep 1
  kill `ps x | grep localization_solver | awk '{print $1}' | head -n 1`
  sleep 1
  kill `ps x | grep switch_solver | awk '{print $1}' | head -n 1`
  sleep 1
  kill `ps x | grep tea_time_solver | awk '{print $1}' | head -n 1`
  sleep 1
}

case "$1" in
  start)
    grail3start
  ;;
  stop)
    grail3stop
  ;;
  restart)
    grail3stop
    grail3start
  ;;
  *)
  echo "usage $0 start|stop|restart"
esac

