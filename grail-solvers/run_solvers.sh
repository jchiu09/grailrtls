#! /bin/bash

#Add solvers like this:
# <solver> [<server ip> <server port>]* <distributor ip> <distributor port> <world server ip> <world server port>
#Not all solvers need the world server though.
agg_ip="grail2.rutgers.edu"
agg_solv_port="7008"

distrib_ip="grail.rutgers.edu"
dist_solv_port="7009"
dist_client_port="7010"

worldip="grail.rutgers.edu"
worldport="7011"

kill `ps x | grep mobility_solver | awk '{print $1}' | head -n 1`
nohup ./bin/mobility_solver $agg_ip $agg_solv_port $distrib_ip $dist_solv_port >mobility_solver.log &
sleep 2
kill `ps x | grep temperature_solver | awk '{print $1}' | head -n 1`
nohup ./bin/temperature_solver $agg_ip $agg_solv_port $distrib_ip $dist_solv_port >temperature_solver.log &
sleep 2
kill `ps x | grep coffee_solver | awk '{print $1}' | head -n 1`
nohup ./bin/coffee_solver $distrib_ip $dist_solv_port $dist_client_port $worldip $worldport >coffee_solver.log &
sleep 2
kill `ps x | grep localization_solver | awk '{print $1}' | head -n 1`
nohup ./bin/localization_solver $agg_ip $agg_solv_port $distrib_ip $dist_solv_port $worldip $worldport >localization_solver.log &
sleep 2
kill `ps x | grep switch_solver | awk '{print $1}' | head -n 1`
nohup ./bin/switch_solver $agg_ip $agg_solv_port $distrib_ip $dist_solv_port $worldip $worldport switch_types.conf >switch_solver &
sleep 2
kill `ps x | grep tea_time_solver | awk '{print $1}' | head -n 1`
nohup ./bin/tea_time_solver $distrib_ip $dist_solv_port $dist_client_port $worldip $worldport >tea_time_solver.log &

kill `ps x | grep passive-motion-solver | awk '{print $1}' | head -n 1`
nohup java -Xms128m -Xmx128m -jar ../passive-motion-solver/target/passive-motion-solver-3.0.1-SNAPSHOT-jar-with-dependencies.jar $agg_ip $agg_solv_port $distrib_ip $dist_solv_port $worldip $worldport >../passive-motion-solver/passive_motion.log &

kill `ps x | grep RecordingSolver | awk '{print $1}' | head -n 1`
nohup java -Xms128m -Xmx128m -cp ../java-sim/target/java-sim-3.0.1-SNAPSHOT-jar-with-dependencies.jar org.grailrtls.sim.RecordingSolver $agg_ip $agg_solv_port -o ../java-sim/winlab -R 3600 >../java-sim/recording_solver.log &
