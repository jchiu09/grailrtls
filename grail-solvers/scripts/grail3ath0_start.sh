#!/bin/bash

CHANNEL=6;
INTERFACE=ath0;

#Wait for the spanning tree to be set up and for us to be online
nohup echo "Sleeping..."
sleep 20
nohup echo "Awake."
#nohup dhclient "$INTERFACE"

#Set up the wireless


ifconfig "$INTERFACE" down
wlanconfig "$INTERFACE" destroy

wlanconfig "$INTERFACE" create wlandev wifi0 wlanmode monitor
ifconfig "$INTERFACE" up
iwconfig "$INTERFACE" channel "$CHANNEL"


#MACADDRESS=`ifconfig | grep "$INTERFACE" | awk '{print $5}'`;
MACADDRESS=`ifconfig | grep "$INTERFACE" | awk '{print $5}'|sed 's/-/:/g'|cut -c1-17`;



#working=`ifconfig "$INTERFACE" | grep "192.168"`;
#if [ "$working" != "" ];
#then
  #Kill all of the eth0 daemons
#  kill `ps ax | grep "dhclient eth0" | awk '{print $1}' | head -n 1`
#  kill `ps ax | grep "/sbin/udhcpc -i eth0" | awk '{print $1}' | head -n 1`
#  ifconfig eth0 down;
#fi
  
#Optional hub announcement at startup
#nohup echo "route add -net 224.0.0.0 netmask 224.0.0.0 eth0"
#nohup echo "/usr/local/sbin/hub_discover 7717 224.0.0.1"
#nohup route add -net 224.0.0.0 netmask 224.0.0.0 eth0
#nohup /usr/local/sbin/hub_discover 7717 224.0.0.1 &

for ((;;)); do
	#Start the wifi sensing layer.
	#/usr/local/sbin/wifi_sense_layer "$ADDRESS" &
echo "invoking with:"
echo "$MACADDRESS"
echo "$INTERFACE"
echo "$CHANNEL"
echo ""

	/usr/local/sbin/wifi_sense "$MACADDRESS" "$INTERFACE" "$CHANNEL" &> /var/log/wifi_ath0.log
	#Wait one second before restarting.
	sleep 1;
done
