#!/bin/bash

### BEGIN INIT INFO
# Provides: 99grailinit.sh
# Required-Start: $remote_fs $syslog
# Required-Stop: $remote_fs $syslog
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: abc
# Description: def
### END INIT INFO


function die () {
	echo "$@" >&2
	exit 1
}

function start () {
	echo "/usr/local/sbin/grail3pip_start.sh"
	nohup /usr/local/sbin/grail3pip_start.sh &

	#echo "/usr/local/sbin/grail3wifi_start.sh"
	#nohup /usr/local/sbin/grail3wifi_start.sh &
	#echo "/usr/local/sbin/grail3wifi_start_two.sh"
	#nohup /usr/local/sbin/grail3wifi_start_two.sh &

	echo "/usr/local/sbin/grail3ath0_start.sh"
	nohup /usr/local/sbin/grail3ath0_start.sh &
	echo "/usr/local/sbin/grail3ath1_start.sh"
	nohup /usr/local/sbin/grail3ath1_start.sh &

}

function stop () {
	kill -9 `ps ax | grep pip_sense_layer | awk '{print $1}' | head -n 1`
	kill -9 `ps ax | grep grail3pip_start.sh | awk '{print $1}' | head -n 1`
	#kill -9 `ps ax | grep grail3wifi_start.sh | awk '{print $1}' | head -n 1`
	#kill -9 `ps ax | grep grail3wifi_start_one.sh | awk '{print $1}' | head -n 1`
	#kill -9 `ps ax | grep grail3wifi_start_two.sh | awk '{print $1}' | head -n 1`
	#kill -9 `ps ax | grep wifi_sense | awk '{print $1}' | head -n 1`
	#kill -9 `ps ax | grep wifi_sense_layer | awk '{print $1}' | head -n 1`

	kill -9 `ps ax | grep grail3ath0_start.sh | awk '{print $1}' | head -n 1`
	kill -9 `ps ax | grep grail3ath1_start.sh | awk '{print $1}' | head -n 1`

	for i in `ps aux | grep wifi_sense | awk '{print $2}'`;
	do
	kill "$i"
	done

	#killall /usr/local/sbin/grail3start.sh
	#killall /usr/local/sbin/pip_sense_layer
}

case "$1" in
	(start)
		start
		;;
	(stop)
		stop
		;;
	(restart)
		stop
		start
		;;
	(*)
		#die "Format: $0 {stop | start | restart}"
		stop
		start	
		;;
esac
exit $?
