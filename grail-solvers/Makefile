SRCDIR=src
BINDIR=bin
INCDIR=include
LIBDIR=lib
INSTALL_DIR=/usr/local/bin
CONFIG_DIR=/usr/share/grail3

define sources
	$(filter %.cpp, $^)
endef
define objects
	$(filter %.o, $^)
endef

#CXX=g++ -O3 -std=c++0x -I./$(INCDIR) -L./$(LIBDIR)
#Variables for implicit compilation rules
CXX=g++
#CXXFLAGS=--debug -std=c++0x -Wall -Wextra -Wno-sign-compare -I./$(INCDIR) -L./$(LIBDIR)
CXXFLAGS=-O3 -std=c++0x -Wall -Wextra -Wno-sign-compare -I./$(INCDIR) -L./$(LIBDIR)

dummy:=$(shell mkdir -p $(LIBDIR))
dummy:=$(shell mkdir -p $(BINDIR))

#Rule to make an object file from a cpp file
$(LIBDIR)/%.o : $(SRCDIR)/%.cpp $(INCDIR)/%.hpp
	$(CXX) -c $(CXXFLAGS) $< -o $@

$(LIBDIR)/%.o : $(SRCDIR)/%.cpp $(INCDIR)/%.h
	$(CXX) -c $(CXXFLAGS) $< -o $@

solvers_passive_loc: $(BINDIR)/passive_localization_solver $(BINDIR)/passive_tracking_solver $(BINDIR)/passive_training_solver $(BINDIR)/passive_offline_analysis $(BINDIR)/path_data_solver

solvers_utility: $(BINDIR)/getReleventPips $(BINDIR)/streamcollect_solver $(BINDIR)/fake_pip_sense_layer

solvers_world_model: $(BINDIR)/fingerprint_solver $(BINDIR)/mobility_solver2 $(BINDIR)/switch_solver2 $(BINDIR)/localization_solver2 $(BINDIR)/gathering_solver $(BINDIR)/location_fingerprinter $(BINDIR)/location_discriminator $(BINDIR)/solver_manager $(BINDIR)/network_performance_solver $(BINDIR)/temperature_solver2 $(BINDIR)/email_event_solver2

solvers_all: solvers_passive_loc solvers_world_model solvers_utility

world_model: $(BINDIR)/world_model_server

sense: $(BINDIR)/pip_sense_layer $(BINDIR)/pip_repeater_interpreter $(BINDIR)/wins_sense_layer $(BINDIR)/wifi_sense

all: world_model sense

install:
	cp $(BINDIR)/* $(INSTALL_DIR)
	mkdir $(CONFIG_DIR)
	cp switch_types.conf $(CONFIG_DIR)

$(LIBDIR)/world_model_protocol.o: $(SRCDIR)/world_model_protocol.cpp $(INCDIR)/world_model_protocol.hpp $(INCDIR)/netbuffer.hpp
	$(CXX) -c $(CXXFLAGS) $(sources) -o $@

$(LIBDIR)/sqlite3_world_model.o: $(SRCDIR)/sqlite3_world_model.cpp $(INCDIR)/sqlite3_world_model.hpp $(INCDIR)/world_model.hpp $(INCDIR)/sqlite_regexp_module.hpp
	$(CXX) -c $(CXXFLAGS) $(sources) -o $@

$(LIBDIR)/aggregator_solver_protocol.o: $(SRCDIR)/aggregator_solver_protocol.cpp $(INCDIR)/aggregator_solver_protocol.hpp $(INCDIR)/netbuffer.hpp $(INCDIR)/sample_data.hpp
	$(CXX) -c $(CXXFLAGS) $(sources) -o $@

$(LIBDIR)/solver_distributor_protocol.o: $(SRCDIR)/solver_distributor_protocol.cpp $(INCDIR)/solver_distributor_protocol.hpp $(INCDIR)/netbuffer.hpp
	$(CXX) -c $(CXXFLAGS) $(sources) -o $@

$(LIBDIR)/grailV3_solver_client.o: $(INCDIR)/*_protocol.hpp $(SRCDIR)/grailV3_solver_client.cpp $(INCDIR)/grailV3_solver_client.hpp
	$(CXX) -c $(CXXFLAGS) $(sources) -o $@

$(LIBDIR)/client_world_connection.o: $(INCDIR)/world_model_protocol.hpp $(SRCDIR)/client_world_connection.cpp $(INCDIR)/client_world_connection.hpp
	$(CXX) -c $(CXXFLAGS) $(sources) -o $@

$(LIBDIR)/solver_world_connection.o: $(INCDIR)/world_model_protocol.hpp $(SRCDIR)/solver_world_connection.cpp $(INCDIR)/solver_world_connection.hpp
	$(CXX) -c $(CXXFLAGS) $(sources) -o $@

$(LIBDIR)/world_server.o: $(INCDIR)/world_server_protocol.hpp $(SRCDIR)/world_server_protocol.cpp
	$(CXX) -c $(CXXFLAGS) $(sources) -o $@

$(LIBDIR)/grail_sock_server.o: $(INCDIR)/grail_sock_server.hpp $(SRCDIR)/grail_sock_server.cpp
	$(CXX) -c $(CXXFLAGS) $(sources) -o $@

$(LIBDIR)/libgrail3.a: $(LIBDIR)/sensor_aggregator_protocol.o $(LIBDIR)/aggregator_solver_protocol.o $(LIBDIR)/solver_distributor_protocol.o $(LIBDIR)/grailV3_solver_client.o $(LIBDIR)/netbuffer.o $(LIBDIR)/sample_data.o $(LIBDIR)/simple_sockets.o $(LIBDIR)/world_server.o $(LIBDIR)/grail_sock_server.o $(LIBDIR)/ThreadConnection.o $(LIBDIR)/world_model_protocol.o $(LIBDIR)/grail_types.o $(LIBDIR)/client_world_connection.o
	ar crs $@ $^

$(LIBDIR)/libtriangulate.o: $(SRCDIR)/triangulate.cpp $(INCDIR)/triangulate.hpp
	$(CXX) -c $(CXXFLAGS) $(sources) -o $@

$(LIBDIR)/libtile_localize.o: $(SRCDIR)/tile_localize.cpp $(INCDIR)/tile_localize.hpp $(LIBDIR)/libtriangulate.o
	$(CXX) -c $(CXXFLAGS) $(sources) -o $@

$(LIBDIR)/libcompass_localize.o: $(SRCDIR)/compass_localize.cpp $(INCDIR)/compass_localize.hpp
	$(CXX) -c $(CXXFLAGS) $(sources) -o $@

$(LIBDIR)/libzone_localize.o: $(SRCDIR)/zone_localize.cpp $(INCDIR)/zone_localize.hpp
	$(CXX) -c $(CXXFLAGS) $(sources) -o $@

$(BINDIR)/pip_sense_layer: $(LIBDIR)/simple_sockets.o $(LIBDIR)/libgrail3.a $(SRCDIR)/pip_sense_layer.cpp
	$(CXX) $(CXXFLAGS) $(sources) -lgrail3 -lusb -o $@

$(BINDIR)/wins_sense_layer: $(LIBDIR)/simple_sockets.o $(LIBDIR)/libgrail3.a $(SRCDIR)/wins_sense_layer.cpp
	$(CXX) $(CXXFLAGS) $(sources) -lgrail3 -lusb -o $@

$(BINDIR)/wifi_sense:$(LIBDIR)/simple_sockets.o $(LIBDIR)/libgrail3.a $(SRCDIR)/wifi_sense_layer.cpp
	$(CXX) $(CXXFLAGS) $(sources) -lgrail3 -lpcap -o $@

$(BINDIR)/pip_repeater_interpreter: $(LIBDIR)/simple_sockets.o $(LIBDIR)/libgrail3.a $(SRCDIR)/pip_repeater_interpreter.cpp
	$(CXX) $(CXXFLAGS) $(sources) -lpthread -lgrail3 -o $@

#Deprecated
$(BINDIR)/world_model_server: $(LIBDIR)/libgrail3.a $(LIBDIR)/sqlite3_world_model.o $(LIBDIR)/semaphore.o $(LIBDIR)/sqlite_regexp_module.o $(SRCDIR)/world_model_server.cpp $(LIBDIR)/standing_query.o
	$(CXX) $(CXXFLAGS) $^ -lpthread -lsqlite3 -ldl -lgrail3 -o $@

#Solvers

$(BINDIR)/email_event_solver2: $(LIBDIR)/libgrail3.a $(SRCDIR)/email_event_solver2.cpp
	$(CXX) $(CXXFLAGS) $^ -lgrail3 -lpthread -o $@

$(BINDIR)/fingerprint_solver: $(LIBDIR)/libgrail3.a $(SRCDIR)/fingerprint_solver.cpp
	$(CXX) $(CXXFLAGS) $^ -lgrail3 -lpthread -o $@

$(BINDIR)/mobility_solver2: $(LIBDIR)/libgrail3.a $(SRCDIR)/mobility_solver2.cpp
	$(CXX) $(CXXFLAGS) $^ -lgrail3 -lpthread -o $@

$(BINDIR)/switch_solver2: $(LIBDIR)/libgrail3.a $(SRCDIR)/switch_solver2.cpp
	$(CXX) $(CXXFLAGS) $^ -lgrail3 -lpthread -o $@

$(BINDIR)/gathering_solver: $(LIBDIR)/libgrail3.a $(SRCDIR)/gathering_solver.cpp
	$(CXX) $(CXXFLAGS) $^ -lgrail3 -lpthread -o $@

$(BINDIR)/location_fingerprinter: $(LIBDIR)/libgrail3.a $(SRCDIR)/location_fingerprinter.cpp
	$(CXX) $(CXXFLAGS) $^ -lgrail3 -lpthread -o $@

$(BINDIR)/location_discriminator: $(LIBDIR)/libgrail3.a $(SRCDIR)/location_discriminator.cpp
	$(CXX) $(CXXFLAGS) $^ -lgrail3 -lpthread -o $@

$(BINDIR)/temperature_solver2: $(LIBDIR)/libgrail3.a $(SRCDIR)/temperature_solver2.cpp
	$(CXX) $(CXXFLAGS) $^ -lgrail3 -lpthread -o $@

$(BINDIR)/network_performance_solver: $(LIBDIR)/libgrail3.a $(SRCDIR)/network_performance_solver.cpp
	$(CXX) $(CXXFLAGS) $^ -lgrail3 -lpthread -o $@

$(BINDIR)/get_net_stats: $(LIBDIR)/libgrail3.a $(SRCDIR)/get_net_stats.cpp
	$(CXX) $(CXXFLAGS) $^ -lgrail3 -lpthread -o $@

$(BINDIR)/fake_pip_sense_layer: $(LIBDIR)/libgrail3.a $(SRCDIR)/fake_pip_sense_layer.cpp
	$(CXX) $(CXXFLAGS) $^ -lgrail3 -lpthread -o $@

$(BINDIR)/passive_localization_solver: $(LIBDIR)/libgrail3.a $(SRCDIR)/passive_localization_solver.cpp $(SRCDIR)/passive_localization_utilities.cpp $(SRCDIR)/lda.cpp
	$(CXX) $(CXXFLAGS) $^ -lgrail3 -lpthread -o $@ \
    	-I /usr/include/opencv -L /usr/lib \
    	-lm -lcv -lhighgui -lcvaux

$(BINDIR)/passive_tracking_solver: $(LIBDIR)/libgrail3.a $(SRCDIR)/passive_tracking_solver.cpp $(SRCDIR)/passive_localization_utilities.cpp $(SRCDIR)/lda.cpp
	$(CXX) $(CXXFLAGS) $^ -lgrail3 -lpthread -o $@ \
    	-I /usr/include/opencv -L /usr/lib \
    	-lm -lcv -lhighgui -lcvaux

$(BINDIR)/passive_training_solver: $(LIBDIR)/libgrail3.a $(SRCDIR)/passive_training_solver.cpp $(SRCDIR)/passive_localization_utilities.cpp $(SRCDIR)/lda.cpp
	$(CXX) $(CXXFLAGS) $^ -lgrail3 -lpthread -o $@ \
    	-I /usr/include/opencv -L /usr/lib \
    	-lm -lcv -lhighgui -lcvaux

$(BINDIR)/passive_offline_analysis: $(LIBDIR)/libgrail3.a $(SRCDIR)/passive_offline_analysis.cpp $(SRCDIR)/passive_localization_utilities.cpp
	$(CXX) $(CXXFLAGS) $^ -lgrail3 -lpthread -o $@

$(BINDIR)/path_data_solver: $(LIBDIR)/libgrail3.a $(SRCDIR)/path_data_solver.cpp $(SRCDIR)/passive_localization_utilities.cpp 
	$(CXX) $(CXXFLAGS) $^ -lgrail3 -lpthread -o $@

$(BINDIR)/streamcollect_solver: $(LIBDIR)/libgrail3.a $(SRCDIR)/streamcollect_solver.cpp
	$(CXX) $(CXXFLAGS) $^ -lgrail3 -lpthread -o $@

$(BINDIR)/monitorcore_solver: $(LIBDIR)/libgrail3.a $(SRCDIR)/monitorcore_solver.cpp
	$(CXX) $(CXXFLAGS) $^ -lgrail3 -lpthread -o $@

$(LIBDIR)/rand.o: $(INCDIR)/rand.h $(SRCDIR)/rand.c
	$(CXX) $(CXXFLAGS) -DRAND_FILE=\"$(SRCDIR)/randfile\" -c $(SRCDIR)/rand.c -o $@

$(BINDIR)/localization_solver2: $(LIBDIR)/libgrail3.a $(LIBDIR)/multidimensional_slicing.o $(LIBDIR)/rand.o $(SRCDIR)/localization_solver2.cpp
	$(CXX) $(CXXFLAGS) $^ -lgrail3 -lpthread -o $@

$(BINDIR)/solver_manager: $(SRCDIR)/solver_manager.cpp $(LIBDIR)/libgrail3.a
	$(CXX) $(CXXFLAGS) $(sources) -ldl -lgrail3 -lpthread -o $@

$(BINDIR)/spmlocalize_solver: $(LIBDIR)/libgrail3.a $(LIBDIR)/localize_common.o $(LIBDIR)/libtriangulate.o $(LIBDIR)/libtile_localize.o $(SRCDIR)/spmlocalize_solver.cpp
	$(CXX) $(CXXFLAGS) $^ -lgrail3 -lpthread -o $@

$(BINDIR)/getReleventPips: src/getReleventPips.cpp
	g++ -O3 -std=c++0x -Wall -Wno-sign-compare -I./include -L./lib lib/libgrail3.a src/getReleventPips.cpp -lgrail3 -o bin/getReleventPips

$(BINDIR)/rssi_solver: $(LIBDIR)/libgrail3.a $(SRCDIR)/rssi_solver.cpp
	$(CXX) $(CXXFLAGS) $^ -lgrail3 -lpthread -o $@

$(BINDIR)/test_clist: $(INCDIR)/circlist.hpp $(BINDIR)/test_circ.cpp
	$(CXX) $(CXXFLAGS) $(sources) -o $@

#Program to test the M2 solver.
$(BINDIR)/M2solver: $(LIBDIR)/slice.o $(LIBDIR)/rand.o $(LIBDIR)/M2.o $(SRCDIR)/M2solver.cpp
	$(CXX) $(CXXFLAGS) $^ -o $@

#Program to test the Delaunay triangularization and visualize it in gnuplot
$(BINDIR)/dttest: $(SRCDIR)/dttest.cpp $(LIBDIR)/libtriangulate.o
	$(CXX) $(CXXFLAGS) $^ -o $@

#Program to test the tile localization algorithms
$(BINDIR)/tiletest: $(SRCDIR)/tiletest.cpp $(LIBDIR)/libtriangulate.o $(LIBDIR)/libtile_localize.o
	$(CXX) $(CXXFLAGS) $^ -o $@

#Program to test localization performance given some a data set in stdin
$(BINDIR)/testlocalize: $(SRCDIR)/testlocalize.cpp $(LIBDIR)/slice.o $(LIBDIR)/rand.o $(LIBDIR)/M2.o  $(LIBDIR)/libtriangulate.o $(LIBDIR)/libtile_localize.o $(LIBDIR)/libcompass_localize.o $(LIBDIR)/libzone_localize.o $(LIBDIR)/localize_common.o $(LIBDIR)/multidimensional_slicing.o $(LIBDIR)/celocalize.o $(LIBDIR)/cross_entropy.o $(LIBDIR)/libgrail3.a
	$(CXX) $(CXXFLAGS) $^ -o $@

#Program to test the world model data structure
$(BINDIR)/test_world_model: $(SRCDIR)/test_world_model.cpp $(LIBDIR)/sqlite3_world_model.o $(LIBDIR)/semaphore.o $(LIBDIR)/sqlite_regexp_module.o $(LIBDIR)/standing_query.o
	$(CXX) $(CXXFLAGS) $^ -lpthread -lsqlite3 -ldl -o $@

#Program to test the world model server
$(BINDIR)/test_world_model_server: $(SRCDIR)/test_world_model_server.cpp $(LIBDIR)/libgrail3.a
	$(CXX) $(CXXFLAGS) $^ -lpthread -lgrail3 -o $@

#Example program that pushes location data into the world server.
$(BINDIR)/gentable_locations: $(SRCDIR)/gentable.cpp $(LIBDIR)/libgrail3.a
	$(CXX) $(CXXFLAGS) $(sources) -lsqlite3 -ldl -lgrail3 -o $@

#Example program that pushes data to the world model from a file.
$(BINDIR)/gen_wm_data: $(SRCDIR)/gen_wm_data.cpp $(LIBDIR)/libgrail3.a
	$(CXX) $(CXXFLAGS) $(sources) -ldl -lgrail3 -lpthread -o $@

#Print out rssi values currently going through the system
bin/rssi_logger: src/rssi_logger.cpp
	g++ -O3 -std=c++0x -Wall -I./include -L./lib lib/libgrail3.a src/rssi_logger.cpp -lgrail3 -lpthread -o bin/rssi_logger


clean:
	-rm $(LIBDIR)/*.o
	-rm $(LIBDIR)/*.a
	-rm $(BINDIR)/*

.SILENT: clean
