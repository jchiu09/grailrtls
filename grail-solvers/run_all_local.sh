#! /usr/bin/bash

grail3start() {
  #Start the aggregation server. Listen for the sensing layer on ort 7007 and for
  #solvers on port 7008.
  #./bin/test_online_agg_server 7007 7008 &
  #Start the world model.. Listen for solvers on port 7009 and for clients on
  #port 7010
  # ./bin/world_model 7009 7010 &
  nohup unbuffer ./bin/world_model_server 7009 7010 > world_model.log &
  #The pip sensing layer is started like this:
  # ./bin/pip_sense_layer <aggregator ip> <aggregator port>
  #To run this locally run with the ports used above run:
  # ./bin/pip_sense_layer 127.0.0.1 7007
}

grail3stop() {
  #kill `ps x | grep test_online_agg_server | awk '{print $1}' | head -n 1`
  kill `ps x | grep world_model_server | awk '{print $1}' | head -n 1`
  #kill `ps x | grep pip_sense_layer | awk '{print $1}' | head -n 1`
}

case "$1" in
  start)
    grail3start
  ;;
  stop)
    grail3stop
  ;;
  restart)
    grail3start
    grail3stop
  ;;
  *)
  echo "usage $0 start|stop|restart"
esac
