/******************************************************************************
 * Parse packets dumped from the grail server.
 * The packets are in this format:
 * <MAC> <Timestamp (milliseconds)> <mode> <phy> <antenna> <transmitter ID>
 *    <timestamp hex?> <rss> <packet data (in hex, with ':' between bytes>
 * 
 * Many of the fields are useless to us so this program will just extract
 * what is necessary to fill in data for the grailV3 message format:
 * <Length><Message ID><FSM/TR><Physical Layer><Device ID><Receiver ID>
 * <Reception Timestamp><Observed RSSI><Sensed Data>
 *
 * Thus this program will extract the Phy number, MAC,
 * receiver ID, transmitter ID, timestamp, RSSI,
 * and the extra data appended to the end of the packet
 *
 *****************************************************************************/

#ifndef __PARSE_TRACE_HPP__
#define __PARSE_TRACE_HPP__

#include <string>
#include <vector>
#include <fstream>
#include "sample_data.hpp"

class FileSampleParser {
  private:
    std::ifstream is;
    std::string file;
  public:
    FileSampleParser(std::string filename) : is(filename.c_str()), file(filename) {
      ;
    }

    //Parse the next sample from the input stream. Returns true
    //on success and false on failure. Failure indicates that
    //unless more data is added into the file any more attempts
    //to get samples from the file will also fail.
    bool getNextSample(SampleData& sample);
};

#endif

