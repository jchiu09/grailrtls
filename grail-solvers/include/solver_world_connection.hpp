/*******************************************************************************
 * This file defines a class that simplifies connecting to the world model
 * as a solver.
 ******************************************************************************/

#include <world_model_protocol.hpp>
#include <simple_sockets.hpp>

#include <functional>
#include <map>
#include <mutex>
#include <string>
#include <thread>

/**
 * Connection to the world model from a solver. Solvers update
 * the world model adding new URIs into the world model and by
 * adding and modifying attributes of URIs in the world model.
 * This class is thread safe.
 */
class SolverWorldConnection {
  private:
    //This mutex should be locked before sending data out through the socket
    std::mutex out_mutex;
    ClientSocket s;
    std::string ip;
    uint16_t port;
    std::map<std::u16string, uint32_t> soln_to_alias;
    //Records whether different transient types are being sent to the world model
    std::map<uint32_t, bool> transient_state;
    bool _connected;
    bool interrupted;

    void receiveThread();
    std::thread rx_thread;
    //A string that identifies this solver
    std::u16string origin;

    /**
     * Specify the types that this solver can create and whether each
     * particular data type is transient in nature.
     * This must be done before sending any solution information.
     */
    void registerTypes();

  public:

    /**
     * Connect to the world model.
     * The arguments specify the origin string used to identify this solver,
     * the solution types that this solver creates, and whether those solution
     * types are transient in nature.
     */
    SolverWorldConnection(std::string ip, uint16_t port, std::u16string& origin,
        std::vector<std::pair<std::u16string, bool>>& solution_types);

    ~SolverWorldConnection();

    /**
     * Reconnect to the world model after losing or closing a connection.
     * Returns true upon connection success, false otherwise.
     */
    bool reconnect();

    /**
     * Create a solution from the given data. This is basically a convenience
     * function so that the user does not need to manually translate from
     * the solution name to its alias.
     */
    world_model::solver::SolutionData makeSolution(const std::u16string& name,
        world_model::grail_time time, const world_model::URI& target, const std::vector<uint8_t>& data);

    /**
     * Update the world model with new attribute information.
     * Transient solutions that are not requested will not be
     * sent to the world model. If the world model has
     * requested that a transient data type be sent then this function
     * will automatically begin sending data to the world model.
     */
    bool sendData(std::vector<world_model::solver::SolutionData> solutions);

    /**
     * Create a new URI in the world model.
     */
    bool createURI(world_model::URI& uri);

    /**
     * Expire a URI or URI's attribute in the world model.
     */
    bool expireURI(world_model::URI& uri, world_model::grail_time expiration);

    /**
     * Delete a URI or URI attribute in the world model.
     */
    bool deleteURI(world_model::URI& uri);

    /**
     * Returns true if this instance is connected to the world model,
     * false otherwise.
     */
    bool connected();

};

