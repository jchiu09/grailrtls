#ifndef __localize_common_hpp__
#define __localize_common_hpp__

#include <array>
#include <vector>
#include <utility>

using std::vector;
using std::pair;

//A sample from a transmitter. If the transmitter's location is unknown the
//x and y locations should be NaNs.
struct Sample {
  double x;
  double y;
  std::vector<double> rssis;
};

typedef std::vector<Sample> TrainingData;

//An array of receiver locations
typedef std::vector<std::pair<double, double>> LandmarkData;

//Sample for multidimensional data
struct MultiDSample {
  std::vector<double> coordinates;
  std::vector<double> rssis;
};

typedef std::vector<MultiDSample> MultiDTrainingData;

template<size_t dimensions>
struct MultiDLandmarkData {
  std::vector<std::array<double, dimensions>> coordinates;
};

struct Tile {
  size_t column;
  size_t row;
};

//Find the euclidean distance between two points
double euclDistance(pair<double, double> a, pair<double, double> b);

#endif

