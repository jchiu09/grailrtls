/*
tile_localize.{cpp,hpp} - Functions for SPM and ABP localization
Source version: January 11, 2011
Copyright (c) 2011 Bernhard Firner

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
  claim that you wrote the original software. If you use this software
  in a product, an acknowledgment in the product documentation would be
  appreciated but is not required.

  2. Altered source versions must be plainly marked as such, and must not be
  misrepresented as being the original software.

  3. This notice may not be removed or altered from any source
  distribution.
*/

#ifndef __tile_localize_hpp__
#define __tile_localize_hpp__

#include <vector>

#include <triangulate.hpp>
#include <localize_common.hpp>

using std::vector;

namespace tile_localize {

  typedef std::vector<Tile> Result;
  typedef vector<vector<double>> ValueMap;

  //Functions that convert from the data at a location to the parameters of
  //a distribution at that location.
  typedef function<double(Point, Point, double)> dataToParameter;
  //Functions that convert from parameters and a location to the expected
  //data value at that location.
  typedef function<double(Point, Point, double)> parameterToData;

  //Default conversion function.
  double identityFunction(Point rxer, Point sample, double value);

  //Define a type of function guesses values of points outside of
  //the complex hull formed by the given mesh. This is primarily for
  //estimating the values of the corner points.
  typedef function<vector<double>(vector<Point>&, Point, ValueMap&)> exteriorEstimator;

  //An exterior estimator function that uses deming regression for a least squares
  //fit to the data using all of the known points in the signal map.
  vector<double> demingRegression(vector<Point>& ps, Point rx_loc, ValueMap& smap);

  //An exterior estimator function that just copies the value of the nearest point
  //into the unkown point.
  vector<double> copyNearest(vector<Point>& ps, Point rx_loc, ValueMap& smap);

  /*
   * Build an interpolated signal map given triangular meshes for each receiver.
   * All of the data points in train_data must have x and y coordinates.
   * Corner values are estimated with the extEstimate function.
   */
  vector<ValueMap> buildSignalMaps(TrainingData& train_data,
                                   vector<vector<Tri>>& meshes,
                                   LandmarkData& lm_data, int columns, int rows,
                                   exteriorEstimator extEstimate = copyNearest);

  /*
   * Area Based Probability localization solver.
   * Solves for the location of any items in the training data that have
   * nan values for coordinates. The solution is in the form of the upper
   * percentile tiles where the transmitter is most likely located.
   * For instance, if percentile is .50 the returned tiles will be all
   * tiles necessary to reach a 50% confidence that the transmitter is
   * in one of the tiles.
   * The algorithm works by building an Interpolated Map Grid (IMG)
   * of signal propagation for each landmark. To determine the probability
   * of a signal coming from a particular tile the solver assumes that
   * signal strength is a guassian random variable with the given
   * variance.
   * Any missing data points should have a nan value.
   */
  std::vector<Result> ABPSolve( TrainingData train_data,
                                LandmarkData rx_locations,
                                double region_width,
                                double region_height,
                                double tile_width,
                                double tile_height,
                                double signal_variance,
                                double percentile,
                                dataToParameter fromData = identityFunction,
                                parameterToData toData   = identityFunction,
                                exteriorEstimator extEstimate = copyNearest);

  /*
   * Simple Point Matching localization solver
   * Solves for the location of any items in the training data that have
   * nan values for coordinates. The solution is in the form of the set of
   * tiles where the transmitter is located with the given confidence level.
   * The algorithm works by building an Interpolated Map Grid (IMG)
   * of signal propagation for each landmark. The SPM algorithm accepts a
   * signal as possibly coming from a tile if its RSS value is with threshold
   * dB of the tile's RSS value.
   * Any missing data points should have a nan value.
   */
  std::vector<Result> SPMSolve( TrainingData train_data,
                                LandmarkData rx_locations,
                                double region_width,
                                double region_height,
                                double tile_width,
                                double tile_height,
                                double threshold,
                                dataToParameter fromData = identityFunction,
                                parameterToData toData   = identityFunction,
                                exteriorEstimator extEstimate = copyNearest);
};

#endif

