/*******************************************************************************
 * world_server_protocol.{cpp,hpp}
 *
 * This protocol allows clients, solvers, and distributors to push and pull
 * information to and from the world server.
 ******************************************************************************/

#ifndef __WORLD_SERVER_PROTOCOL_HPP__
#define __WORLD_SERVER_PROTOCOL_HPP__

#include <vector>
#include <string>

namespace world_server {

  //Message IDs for messages to and from the world server.
  enum class MessageID : uint8_t {keep_alive          = 0, //Should connections to the world server stay alive?
                                  certificate         = 1, //If we use SSL then this message probably won't happen
                                  ack_certificate     = 2, //There is no message for certificate denial
                                  object_query        = 3,
                                  object_data_query   = 4,
                                  query_response      = 5,
                                  uri_search          = 6,
                                  uri_search_response = 7,
                                  push_data           = 8};

  //Special characters besides the period (.) are not permitted in URIs.
  typedef std::u16string URI;
  typedef std::vector<uint8_t> Buffer;

  /*
   * Objects in the world server are stored as a URI with multiple fields.
   * Fields consist of a human readable description and binary data.
   * Each field also contains a creation date and an expiration date, which
   * are stored in UNIX time (seconds since 00:00:00 UTC, January 1, 1970)
   * Some data never expires - this data is stored with an expiration date of 0.
   * As an abstract example:
   * Object URI | Field 1 Description | Creation Date | Expiration Date | Data Type | Data
   *            | Field 2 Description | Creation Date | Expiration Date | Data Type | Data
   *            | Field 3 Description | Creation Date | Expiration Date | Data Type | Data
   * 
   * For a concrete example using ``grail.location.ellipse'':
   * grail.location.ellipse | x coordinate | 1/3/2011 | never expires | grail.double | ...
   *                        | y coordinate | 1/3/2011 | never expires | grail.double | ...
   *                        | x range      | 1/3/2011 | never expires | grail.double | ...
   *                        | y range      | 1/3/2011 | never expires | grail.double | ...
   *
   * In this case the URI ``grail.location.ellipse'' has four fields that
   * describe an ellipse. The field descriptions are all doubles so
   * the string ``grail.double'' describes that to the user.
   * When a GRAIL3 distributor holds a grail.location.ellipse information
   * the data it is holding is four doubles.
   *
   * Comparing GRAIL3 URIs to web page urls we could say that the GRAIL3 URI
   * is like the url of a web page and the fields at that URI is the content
   * of the page.
   */

  //Each object URI in the world server can have multiple fields
  struct FieldData {
    //The description of one of the fields of a URI.
    std::u16string description;
    //The creation date and the date when this field's data expires.
    uint64_t creation_date;
    uint64_t expiration_date;
    std::u16string data_type;
    //Data in the field.
    Buffer data;
  };

  struct WorldData {
    //The URI of an object in the world server.
    URI object_uri;
    //Fields of this URI
    std::vector<FieldData> fields;
  };

  /*
   * Object query messages are used to get the ``fields'' of a piece of data.
   * For one example see the ``solution.location'' example above.
   * As another example, ``solution.coffee.cups poured'' has only one field with
   * description ``cups poured since last brew time'' and data ``grail.uint32''.
   *
   * This kind of data is stored as five parts:
   * Object URI | Description String | Creation Date | Expiration Date | Type String
   *
   * Thus solution URI queries return WorldData that always have string data
   * but other queries might return data that is substantial in size.
   * For example, ``Rutgers.Busch.CoRE.3F'' might have a field named
   * ``jpeg map'' that has a large amount of data.
   * To prevent sending large amounts of data to a client that does not
   * actually want that data there are two ways to query an object.
   * The Object Query message just returns the field names of a URI.
   * The Object Data Query message returns the field names and their data.
   */
  Buffer makeObjectQueryMsg(const URI& object_uri);
  URI decodeObjectQueryMsg(Buffer& buff);

  Buffer makeObjectDataQueryMsg(const URI& object_uri);
  URI decodeObjectDataQueryMsg(Buffer& buff);

  //Both message types will receive a response in the same format, but the
  //data portions of fields will be empty in the response to an object
  //query message.
  Buffer makeQueryResponse(const WorldData& world_data);
  WorldData decodeQueryResponse(Buffer& buff);

  /*
   * At times we need to search the world server for information without
   * knowing an exact URI. In these cases we can send the world server a
   * URI search message.
   *
   * For instance, if Bluetooth devices are stored in the world server
   * with URIs as follows - ``Bluetooth.ID.(sensors and other data)'' - then
   * we could search for the IDs of Bluetooth devices with the query string
   * ``Bluetooth.*'' and search for the IDs of Bluetooth devices with
   * temperature sensors with the query string
   * ``Bluetooth.*.temperature sensor''.
   *
   * Now, one might wonder if we could simply use object query messages to
   * find this information as well by querying the URI ``Bluetooth'' and
   * checking its fields - we cannot. There is a difference between a
   * part of a URI, ``Bluetooth'' then ``42'' (for a sensor with ID 42) and
   * then ``temperature sensor'' is a single URI made up of three sections,
   * but those sections are not fields.
   * Instead the URI ``Bluetooth.42.temperature sensor'' might have a field
   * with the part number for the temperature sensor and the URI
   * ``Bluetooth.42'' might have fields with the name of the Bluetooth device.
   * These two URIs are not part of each other, although their names are
   * similar. Think of URIs as web pages - the web page at www.foo.com/bar
   * and the web page at www.foo.com/bar/baz have similar URLs but that is
   * no guarantee that the pages look anything alike.
   *
   * The format of the search URI is consistent with the UNIX glob utility
   * where * will match any string (including an empty string), ? will
   * match a single character and brackets ([]) can be used to specify a
   * group of characterse to match. On a UNIX machine run the command
   * "man 7 glob" for more information.
   */
  Buffer makeURISearchMsg(const URI& search_uri);
  URI decodeURISearchMsg(Buffer& buff);

  Buffer makeURISearchResponse(const std::vector<URI>& results);
  std::vector<URI> decodeURISearchResponse(Buffer& buff);

  /*
   * It is also possible to push data into the world server.
   */
  Buffer makePushDataMsg(const WorldData& world_data);
  WorldData decodePushDataMsg(Buffer& buff);


  std::vector<unsigned char> makeHandshakeMsg();
};

#endif

