/*
triangulate.{cpp,hpp} - Functions to do Delaunay triangulation
Source version: January 11, 2011
Copyright (c) 2011 Bernhard Firner

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
  claim that you wrote the original software. If you use this software
  in a product, an acknowledgment in the product documentation would be
  appreciated but is not required.

  2. Altered source versions must be plainly marked as such, and must not be
  misrepresented as being the original software.

  3. This notice may not be removed or altered from any source
  distribution.
*/

#ifndef __triangulate_hpp__
#define __triangulate_hpp__

#include <algorithm>
#include <iostream>
#include <utility>
#include <vector>

using namespace std;

struct Point {
  double x;
  double y;
};

struct Triple {
  double a;
  double b;
  double c;
};

struct Tri {
  Point a;
  Point b;
  Point c;
};

Point toPoint(const pair<double, double>& p);

bool operator<(const Point& a, const Point& b);

bool operator==(const Point& a, const Point& b);
bool operator!=(const Point& a, const Point& b);

bool operator==(const Tri& a, const Tri& b);

ostream& operator<<(ostream& os, const Point& p);
ostream& operator<<(ostream& os, const Tri& t);

//Returns the barycentric coordinates of point p inside the given triangle.
Triple toBarycentric(Point p, Tri t);

bool insideTriangle(Point p, Tri t);

double euclDistance(Point a, Point b);

/*
 * Find the triangle that contains the given point and return a reference to it.
 * Returns tris.end() if no containing triangle is found.
 */
vector<Tri>::iterator findContainingTriangle(Point p, vector<Tri>& tris);

//Generate a triangular mesh to fill a region with the given dimension
//with triangle vertices on the given points and the four corners of the space.
vector<Tri> genTriangles(double max_x, double max_y, vector<Point> vertices);


#endif

