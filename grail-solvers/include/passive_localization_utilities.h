#ifndef __PASSIVE_LOCALIZATION_UTILITIES_H__
#define __PASSIVE_LOCALIZATION_UTILITIES_H__

#include <iostream>
#include <map>
#include <string>
#include <time.h>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <stdlib.h>
#include <math.h>
#include <cmath>
#include <ctime>

#include "sample_data.hpp"

using namespace std;

// return the mean of the vector
double getMean(vector<double>& vals);

// return the median of the vector
double getMedian(vector<double>& vals);

// return the variance of the vector 
double getVariance(vector<double>& vals, double mean);

// return the covariance of two vectors
double getCovariance(vector<double>& p1, vector<double>& p2);

// return the correlation of two vectors
double getCorrelation(vector<double>& p1, vector<double>& p2);

// return the coefficient of two vectors
double getCoefficient(vector<double>& p1, vector<double>& p2);

// return the euclidean distance of two vectors
double getDistance(vector<double>& p1, vector<double>& p2);

// return the index matching the target value
int getIndex(vector<uint128_t> vals, int target);

int getIndex(vector<uint128_t>& vals, uint128_t target);

// return the ranks from the array in descending order
vector<int> getRank(vector<double>& vals);

// return the row or column vector from a matrix (two-dimensional vector)
vector<double> getVector(vector< vector<double> > data, int id, bool col);

// return the new vector with the first n elements in origin 
vector<double> front_n(vector<double> data, int n);

// return the new vector with the last n elements in origin
vector<double> back_n(vector<double> data, int n);

// return the mean vector from the first win_size elements
vector<double> frontMean(vector< vector<double> > data, int win_size);

// return the mean vector from the last win_size elements
vector<double> backMean(vector< vector<double> > data, int win_size);

// return the vector from the first win_size elements
vector< vector<double> > frontVector(vector< vector<double> > data, int win_size);

// return the vector from the last win_size elements
vector< vector<double> > backVector(vector< vector<double> > data, int win_size);

// convert the map (int, vector) to a 2-dimensional vector
vector< vector<double> > map2vector(map<uint128_t, vector<double> > data);

// a vector left multiplication to a matrix
vector<double> leftMul(vector<double> vec, vector< vector<double> > mat);

// a matrix right multiplication to a vector
vector<double> rightMul(vector< vector<double> > mat, vector<double> vec);

// dot product of two vectors
double innerProduct(vector<double> v1, vector<double> v2);

// matrix product of two vectors
vector< vector<double> > outerProduct(vector<double> v1, vector<double> v2);

// dot multiplication of two vectors
vector<double> dotMul(vector<double> v1, vector<double> v2);
// compose a pair of tx-rx to link
uint128_t encodeLink(uint128_t tx, uint128_t rx, int rx_num);

// decompose a link into tx and rx
vector<uint128_t> decodeLink(uint128_t link, int rx_num);

// normalize the vector
void normalize(vector<double> &data);

// profile the neighbour information: mark neighbour's neighbour as the new neighbour for each callback
void neighbourProfile(vector< vector<double> > &dynamic_neighbour, vector< vector<double> > static_neighbour);

// set the cell adjacent matrix according to the neighbour information 
vector< vector<double> > setAdjCell(vector< vector<double> > neighbour);

// return the frequency table: first column is the unique value, and the second column is the normalized frequency
vector< vector<double> > intFreqTab(vector<int> data);
vector< vector<double> > doubleFreqTab(vector<double> data);

// classification accuracy between estimated label vector py and groundtruth label vector ty
double cellEst(vector<int> ty, vector<int> py);

// return the average error distance. in_err is the average error distance within the cell
double errLoc(vector<int> ty, vector<int> py, vector< vector<double> > attributes, double in_err);

// return the CDF data
vector< vector<double> > distCDF(vector<int> ty, vector<int> py, vector< vector<double> > attributes, double in_err);

void disp1Dvec(vector<double> data);

void disp2Dvec(vector <vector<double> > data);

#endif
