/*
*
* Copyright (c) 2006 Rutgers University and Konstantinos Kleisouris
* All rights reserved.
*
* Permission to use, copy, modify, and distribute this software and its
* documentation for any purpose, without fee, and without written agreement is
* hereby granted, provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the distribution.
*
* 3. Neither the name of the University nor the names of its
* contributors may be used to endorse or promote products derived from
* this software without specific prior written permission.
*
* IN NO EVENT SHALL RUTGERS UNIVERSITY BE LIABLE TO ANY PARTY FOR DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT
* OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF RUTGERS
* UNIVERSITY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* RUTGERS UNIVERSITY SPECIFICALLY DISCLAIMS ANY WARRANTIES,
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
* AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS
* ON AN "AS IS" BASIS, AND RUTGERS UNIVERSITY HAS NO OBLIGATION TO
* PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*
*
* Author: Konstantinos Kleisouris
* Version: $Id: M2.hpp,v 1.1.2.6 2010/11/12 17:18:57 bfirner Exp $
* Creation Date: 06/27/2006
* Filename: M2.c
* History:
* $Log: M2.hpp,v $
* Revision 1.1.2.6  2010/11/12 17:18:57  bfirner
* Adding test localization solver that uses probability densities of
* boxes rather than ellipses based upon average x and y values.
*
* Revision 1.1.2.5  2010/10/10 18:23:25  bfirner
* Update that get everything working.
*
* Note that CVS is marking many files as changed that were not.
* Fixed bugs in the netbuffer class.
* Fixed flipped y coordinate in the script.
* Fixed timing bug in the localization script.
* Fixed the 128 bit number.
* Added support for position messages.
*
* Revision 1.2  2009/09/23 20:05:28  aopoku
* @kkonst checking in solver that removes negative one columns from training data.
*
* Revision 1.1  2007/09/17 05:01:08  stryke3
* Initial commit.
*
* Revision 1.3  2007/04/30 21:51:07  kkonst
* Added solver code
*
* Revision 1.2  2007/03/12 22:41:13  kkonst
* Modified the solvers so that a signal strength value of -1 is used whenever
* we do not have signal measurement from a specific Access Point.
*
* Revision 1.1  2006/07/19 20:13:20  kkonst
* Added source code of fast solver under demo3
*
* Revision 1.1.1.1  2006/06/27 15:15:52  kkonst
* Imported fast solver code
*
*/

#include <vector>
#include <localize_common.hpp>

struct Result {
  double x;
  double y;
  double x_stddev;
  double y_stddev;
};

struct Rect {
  double lower_left_x;
  double lower_left_y;
  double upper_right_x;
  double upper_right_y;
};

typedef std::vector<Rect> RectResult;

enum SampleMethod { slice1     = 0,
                    slice2d    = 1,
                    met1       = 2,
                    ref2d      = 3,
                    slice1all  = 4,
                    slice2dall = 5,
                    met1all    = 6 };


//M2 solver
std::vector<Result> m2solve( int argc, char** argv,
    const TrainingData& train_data,
    const LandmarkData& ap_data,
    double DOMAIN_X,
    double DOMAIN_Y,
    int burnin,
    int additional_iter,
    SampleMethod sampling_method);

std::vector<std::vector<Tile>> m2solveBoxes( int argc, char** argv,
    const TrainingData& train_data,
    const LandmarkData& ap_data,
    double DOMAIN_X,
    double DOMAIN_Y,
    int burnin,
    int additional_iter,
    SampleMethod sampling_method,
    double tile_width,
    double tile_height,
    double percentile);

