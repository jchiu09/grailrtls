/**
 * This file contains functions and definitions for the
 * interface between solvers and the data store/distributor and between
 * the distributor and applications.
 * TODO FIXME the name of this file is no longer accurate
 */


#ifndef __SOLVER_DISTRIBUTOR_PROTOCOL_HPP__
#define __SOLVER_DISTRIBUTOR_PROTOCOL_HPP__

#include <array>
#include <string>
#include <vector>
#include <iostream>

namespace solver_distributor {

  enum MessageID {keep_alive         = 0,
                  type_specification = 1,
                  solver_data        = 2,
                  type_request       = 3};

  //A solution has a type alias and a name. When communicating with the
  //distributor to send solution data only the alias is used.
  struct SolutionType {
    //The alias used to refer to this type.
    uint32_t type_alias;
    std::u16string name;
  };

  typedef std::vector<SolutionType> TypeSpecifications;

  //Solver with data and target data. The alias specifies the data format.
  struct SolutionData {
    uint32_t type_alias;
    std::u16string target;
    std::vector<uint8_t> data;
  };

  struct SolverDataMsg {
    std::u16string region;
    //The time that when this data was current
    uint64_t time;
    std::vector<SolutionData> data;
  };

  //If begin < end then data from time begin to time end will be sent.
  //If being == end then real-time data will be sent.
  //If end < begin then data from begin to the current time will be sent, and then
  //real-time data will be sent.
  struct SolutionRequest {
    uint64_t begin;
    uint64_t end;
    std::vector<uint32_t> aliases;
  };


  /**
   * Make a handshake message.
   */
  std::vector<unsigned char> makeHandshakeMsg();

  /**
   * Make a message from the solver or distributor that specifies what types of
   * data are in possible solutions.
   */
  std::vector<unsigned char> makeTypeSpecMsg(const std::vector<SolutionType>& type_specs);

  std::vector<SolutionType> decodeTypeSpecMsg(std::vector<unsigned char>& buff, int length);

  /**
   * A request from an application for specific solutions in a time interval.
   * The type aliases of the desired solutions are sent to the distributor.
   */
  std::vector<unsigned char> makeTypeReqMsg(const SolutionRequest& request);

  SolutionRequest decodeTypeReqMsg(std::vector<unsigned char>& buff, int length);


  /**
   * Make a message to send solutions from the solver to the distributor and
   * from the distributor to the application.
   */
  std::vector<unsigned char> makeSolutionMsg(const SolverDataMsg& data);

  SolverDataMsg decodeSolutionMsg(std::vector<unsigned char>& buff, int length);

}//end namespace solver_distributor

namespace distributor_application {

  typedef solver_distributor::SolutionType SolutionType;
  typedef solver_distributor::SolverDataMsg SolverDataMsg;

  /**
   * Make a handshake message.
   */
  std::vector<unsigned char> makeHandshakeMsg();

  /**
   * Make a message from the distributor that specifies what types of
   * data are in possible solutions.
   */
  std::vector<unsigned char> makeTypeSpecMsg(const std::vector<SolutionType>& type_specs);

  /**
   * Make a message to send solutions from the distributor to the application.
   */
  std::vector<unsigned char> makeSolutionMsg(const SolverDataMsg& data);

  /**
   * Make a message to notify a client that historic streaming is complete.
   */
  std::vector<unsigned char> makeHistoricCompleteMsg();

  enum MessageID {keep_alive           = 0,
                  certificate          = 1,
                  ack_certificate      = 2, //There is no message for certificate denial
                  solution_publication = 3,
                  solution_request     = 4,
                  solution_response    = 5, //Is this required? TODO FIXME For now this will not be used.
                  solution_sample      = 6,
                  historic_complete    = 7};

}

#endif

