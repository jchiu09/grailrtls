/*******************************************************************************
 * A simple circular list class that uses a constant amount of space and
 * supports fast constant-time insertions. Insertions will overwrite the oldest
 * entries when space runs out. Deleting from the front of the list is
 * supported. Only forward iterators are supported and begin() and end()
 * functions are provided. The size of the class is equal to the space needed to
 * store the given number of elements plus the size of two integers.
 ******************************************************************************/


#ifndef __CIRCLIST_HPP__
#define __CIRCLIST_HPP__


#include <array>
#include <stdexcept>

template<typename value_type, int max_size>
class CircularList {
  private:
    //A constant size array to the list's data.
    std::array<value_type, max_size> arr;
    //The fist element in the list.
    int head;
    //The total number of elements stored.
    int total;

  public:
    friend class iterator;
    //An iterator class for the circular list
    class iterator {
      private:
        int i_offset;
        bool done;
        CircularList<value_type, max_size>* cl_p;

      public:
        //An iterator to a specific location. When the iterator reaches the position
        //specified by end through the ++ operators the iterator will become invalid.
        //Start and end can be the same initially and the iterator will be valid.
        iterator(CircularList<value_type, max_size>* cl_p, int start) {
          this->cl_p = cl_p;
          i_offset = cl_p->head;
          done = false;
        }

        //An invalid iterator representing the end of the list.
        iterator(CircularList<value_type, max_size>* cl_p) {
          this->cl_p = cl_p;
          done = true;
        }

        //Prefix ++ operator
        iterator& operator++() {
          if (done) {
            throw std::out_of_range("CircularList iterator is no longer valid.");
          }
          i_offset = (i_offset + 1) % max_size;
          //If this iterator has circled around and reached the head it is done.
          if (i_offset == (cl_p->head + cl_p->total) % max_size) {
            done = true;
          }
          return *this;
        }

        //Postfix ++ operator
        iterator operator++(int) {
          if (done) {
            throw std::out_of_range("CircularList iterator is no longer valid.");
          }
          iterator copy = *this;
          ++(*this);
          return copy;
        }

        //The dereference operator
        value_type& operator*() {
          if (done) {
            throw std::out_of_range("CircularList iterator is no longer valid.");
          }
          return cl_p->arr[i_offset];
        }

        bool operator==(const iterator& rhs) {
          //If both iterators are no longer valid return true.
          if (this->done) {
            return rhs.done;
          }
          //Otherwise these are equal if they both are valid, point to the same
          //circular list, and point to the same location.
          return (not rhs.done and
                  this->cl_p == rhs.cl_p and
                  this->i_offset == rhs.i_offset);
        }

        bool operator!=(const iterator& rhs) {
          return not this->operator==(rhs);
        }

    };

    CircularList() : arr() {
      head = 0;
      total = 0;
    }

    //Return an iterator to the current index.
    iterator begin() {
      if (total > 0) {
        return iterator(this, head);
      } else {
        return end();
      }
    }

    //Return an interator representing the end of the list.
    iterator end() {
      return iterator(this);
    }

    //Insert a new item at the tail of the list, overwritting the head if the
    //list is full.
    void insert(const value_type& item) {
      if (total < max_size) {
        //Increase the total elements stored unless the list is full.
        ++total;
      } else {
        //If space has run out then we will overwrite the previous list head.
        head = (head + 1) % max_size;
      }
      arr[(head+total-1) % max_size] = item;
    }

    //Remove the first element of the list, if it exists.
    void pop() {
      if (0 != total) {
        head = (head + 1) % max_size;
        --total;
      }
    }

    //Clear the list
    void clear() {
      total = 0;
    }

    //Return the number of elements currently accessible in the list.
    int size() {
      return total;
    }

};

#endif


