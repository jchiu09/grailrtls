#ifndef __MULTIDIMENSIONAL_SLICING_HPP__
#define __MULTIDIMENSIONAL_SLICING_HPP__

#include <localize_common.hpp>

#include <array>
#include <vector>
#include <algorithm>

using std::vector;
using std::array;

namespace md_slicing {

  struct Result {
    std::vector<double> coordinates;
    std::vector<double> stddevs;
  };

  enum SampleMethod { slice1     = 0,
    slice2d    = 1,
    met1       = 2,
    ref2d      = 3,
    slice1all  = 4,
    slice2dall = 5,
    met1all    = 6 };

  struct mc_dynamic_state { 
    int dim;
    //Position variables
    std::vector<double> q;
    std::vector<double> position_estimates;
    //Potential energy of position
    double pot_energy;

    double bprior0, bprior1, tauprior0, tauprior1;
    //Indices in the q array
    //Initial signal powers
    int first_b0i_index;
    //Propagation coefficients
    int first_b1i_index;
    int first_taui_index;
    vector<double> domains;

    mc_dynamic_state(int MAX_APS, int predictNo, int total_network_variables, vector<double> domains);
  }; 

  typedef std::vector< std::vector<double> > double_matrix;

  struct data_specifications
  {
    int N_inputs;			/* Number of "input" values */
    //TODO this can always be replaced by APS_X.size()
    //int N_targets;		/* Number of "target" values */
    int N_cases;                  /* Number of lines in the training file */

    //TODO These should just be a single vector of points
    //The coordinates of the access points (receivers)
    std::vector<std::vector<double>> AP_coordinates;
    //TODO This variable is unecessary as the APS_X.size() value could be used
    int MAX_APS;

    int predictNo;
    int observations_nopredict;

    //Distances from the training signals to the receivers
    vector<vector<double> > D;     /* Array of Arrays for Ds */
    vector<vector<double> > Dsqrd; /* Array of Arrays for D^2 */

    //The signal strengths of each AP at each transmitter
    double_matrix train_signal;

    data_specifications(int N_inputs, int MAX_APS, int N_cases,
        const std::vector<std::vector<double>>& train_coordinates,
        const double_matrix& train_signal, int predictNo, int observations_nopredict,
        const std::vector<std::vector<double>>& AP_coordinates);
  };

  void large_slice_1all(mc_dynamic_state& ds, /* Structure holding pointers to dynamical state */
      data_specifications& data_spec,
      double width, 
      int& rejections);

  std::vector<Result> multiD_m2solve( int argc, char** argv,
      const MultiDTrainingData& train_data,
      const std::vector<std::vector<double>>& ap_data,
      const std::vector<double>& domains,
      int burnin,
      int additional_iter,
      SampleMethod sampling_method);
};

/*
template<size_t dimensions>
struct multiD_mc_dynamic_state { 
  int dim;
  //Position variables
  double mean;
  double precision;
  std::vector<std::array<double, dimensions>> positions;
  std::vector<double> b0i;
  std::vector<double> b1i;
  std::vector<double> taui;
  //Gradient of potential energy with respect to position
  std::vector<std::array<double, dimensions>> grad;
  double pot_energy;	// Potential energy of position
  std::array<double, dimensions> domain;

  multiD_mc_dynamic_state(int MAX_APS, int predictNo, int total_network_variables,
      const std::array<double, dimensions>& domain);
}; 

template<size_t dimensions>
multiD_mc_dynamic_state::multiD_mc_dynamic_state(int MAX_APS, int predictNo,
    const std::array<double, dimensions>& domain) {
  dim = dimensions*predictNo;
  grad = std::vector<std::vector<double, dimensions>>(predictNo);
  this->domain = domain;

  std::vector<double>::iterator qbegin = q.begin();
  std::fill(qbegin, qbegin+last_bprior_index, 0.0);
  std::fill(qbegin+last_bprior_index, qbegin+last_taubprior_index, 0.01);
  std::fill(qbegin+first_b0i_index, qbegin+first_taui_index, 0.0);
  std::fill(qbegin+first_taui_index, qbegin+first_x_index, 0.1);

  mean = 0.0;
  precision = 0.01;
  std::vector<double> b0i(MAX_APS, 0.0);
  std::vector<double> b1i(MAX_APS, 0.0);
  std::vector<double> taui(MAX_APS, 0.1);
  std::vector<std::array<double, dimensions>> positions(predictNo);

  //Initialize positions with random locations inside the localization area
  for (auto I = positions.begin(); I != positions.end(); ++I) {
    for (size_t j = 0; j < dimensions; ++j) {
      I->at(j) = rand_uniopen() * domain[j];
    }
  }
}

template<size_t dimensions>
struct multiD_data_specifications {
  //Training locations and signal values
  std::vector<std::array<double, dimensions>> train_locations;
  double_matrix train_signal;
  int predictNo;
  int observations_nopredict;
  //AP locations
  std::vector<std::array<double, dimensions>> ap_locations;
  //Distances and square distances (euclidean) from APs to training locations
  vector<vector<double> > D;     // Array of Arrays for Ds
  vector<vector<double> > Dsqrd; // Array of Arrays for D^2

  //Constructor. This requires the number of inputs (TODO ?), cases (TODO ?),
  //all signal locations (including those with unknown origins),
  //the training signals, the total signals that need predictions and that
  //do not need predictions, and the locations of the receivers.
  multiD_data_specifications::multiD_data_specifications(
  const std::vector<std::array<double, dimensions>>& train_locations,
  const double_matrix& train_signal,
  int predictNo, int observations_nopredict,
  const std::vector<std::array<double, dimensions>>& ap_locations);
};

template<size_t dimensions>
multiD_data_specifications::multiD_data_specifications(
  const std::vector<std::array<double, dimensions>>& train_locations,
  const double_matrix& train_signal,
  int predictNo,
  int observations_nopredict,
  const std::vector<std::array<double, dimensions>>& ap_locations
)
{
  this->predictNo = predictNo;
  this->observations_nopredict = observations_nopredict;
  this->train_locations = train_locations;
  this->train_signal = train_signal;
  this->D = double_matrix(ap_locations.size(), std::vector<double>(train_locations.size(), 0.0));
  this->Dsqrd = double_matrix(ap_locations.size(), std::vector<double>(train_locations.size(), 0.0));
  this->ap_locations = ap_locations;

  double x_value, y_value, diff1, diff2, x_fixed, y_fixed;    

  //Find the distances from the receiver locations to the signal locations
  //for all signals of known origin at each receiver.
  for (auto i = 0; i < ap_locations.size(); ++i) {
    for (int j = 0; j < observations_nopredict; j++) {
      double sum_squares = 0;
      for (int dimen = 0; dimen < dimensions; ++dimen) {
        sum_squares += pow(train_locations[j][dimen] - ap_locations[i][dimen], 2.0);
      }
      this->D[i][j] = log(1+sqrt(sum_squares));
      this->Dsqrd[i][j] = D[i][j] * D[i][j];
    }
  }
}
*/

#endif //undefined __MULTIDIMENSIONAL_SLICING_HPP__

