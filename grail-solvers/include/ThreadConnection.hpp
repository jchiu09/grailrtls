/*******************************************************************************
 * Class made to simplify having multiple threaded IP connections.
 ******************************************************************************/

#ifndef __THREAD_CONNECTION_HPP__
#define __THREAD_CONNECTION_HPP__

#include <functional>
#include <list>
#include <mutex>
#include <thread>
#include <time.h>
#include <future>
#include <utility>

#include "simple_sockets.hpp"

/**
 * Threaded connection class that has functions to create new
 * threads associated with sockets.
 */
class ThreadConnection {
  private:
    //An internal list of connections
    static std::list<ThreadConnection*> connections;
    //Locked before accessing the connections list
    static std::mutex tc_mutex;
    std::future<bool> success;

    //True once the connection can be deleted
    bool finished;

    //This is private so that inherited classes must be passed to the
    //makeNewConnection function to be set up with the socket.
    ClientSocket sock;

    /**
     * Makes sure that finished is set to true when run returns.
     * Returns true if the thread exits normally and false if it exits
     * due to an unhandled exception.
     */
    bool innerRun();

    //Private copy semantics to retain control over sockets
    //(only one connection per socket)
    ThreadConnection& operator=(const ThreadConnection&) = delete;
    ThreadConnection(const ThreadConnection&) = delete;

    //Time of last socket activity.
    time_t last_activity;

  public:
    //Maximum duration of socket inactivity before timing out the thread
    //and allowing cleanFinished to clean it away.
    //Default timeout is 30 seconds.
    time_t timeout;

    ///Removes any finished connections from the connections list.
    static void cleanFinished();

    ///Intantiate and run a derived connection
    static void makeNewConnection(ClientSocket&& sock, std::function<ThreadConnection* (ClientSocket&& sock)> fun);

    ///Execute a function on each active ThreadConnection.
    static void forEach(std::function<void(ThreadConnection*)> f);

    ThreadConnection(ClientSocket&& ref_sock, time_t timeout = 30);

    virtual ~ThreadConnection(){};

    ///Virtual run function. Inheriting classes do stuff with the connection here.
    virtual void run() = 0;

    /**
     * An interrupt function that causes the thread to terminate.
     * This function must be thread safe.
     */
    virtual void interrupt() = 0;

    /*****
     * Functions to access the internal socket
     * Use these rather than going through the ClientSocket directly so
     * that socket activity can be monitored
     * ***/
    size_t receive(std::vector<unsigned char>& buff);
    void send(const std::vector<unsigned char>& buff);
    
    /**
     * Set the connection active status to avoid timing out.
     * This is automatically called when socket activity occurs.
     */
    void setActive();

    ///Return the time this thread was last active.
    time_t lastActive();

    ///Return a reference to the client socket
    ClientSocket& sockRef();
};

#endif
