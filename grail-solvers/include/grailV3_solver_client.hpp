/******************************************************************************
 * Helper library for writing the solver client interface to the grailV3
 * server.
 *****************************************************************************/

#ifndef __GRAILV3_SOLVER_CLIENT_CPP__
#define __GRAILV3_SOLVER_CLIENT_CPP__

#include <string>
#include <vector>
#include <functional>
#include <map>
#include <mutex>
#include <thread>
#include <deque>
#include <set>

#include "parse_trace.hpp"
#include "aggregator_solver_protocol.hpp"
#include "solver_distributor_protocol.hpp"
#include "simple_sockets.hpp"
#include "grail_sock_server.hpp"
#include "world_server_protocol.hpp"
#include "world_model_protocol.hpp"

//TODO In the future C++11 support for regex should be used over these POSIX
//regex c headers.
#include <sys/types.h>
#include <regex.h>

using std::u16string;

struct NetTarget {
  std::string ip;
  uint16_t port;
};

/**
 * Connection from a client to the world model.
 */
class ClientWorldModel {
  public:
    typedef std::map<world_model::URI, std::vector<world_model::Attribute>> world_state;
  private:
    uint32_t next_ticket;
    //If this client is streaming data then the getRequestResponse function is
    //disabled and data responses will only be returned through callbacks.
    bool streaming;
    bool interrupted;
    std::thread stream_thread;
    std::map<uint32_t, std::u16string> types;
    std::map<uint32_t, std::u16string> origins;
    //Send a handshake and a type declaration message.
    bool reconnect();

    ClientSocket s;
    GRAILSockServer ss;
    std::string ip;
    uint16_t port;

    //Requests that were cancelled and whose callbacks should not be called.
    std::set<uint32_t> cancelled;
    std::map<uint32_t, std::function<void (world_state&, uint32_t)>> callbacks;
    std::map<uint32_t, world_state> request_states;

    //Handle a synchronous request until the request complete message is received.
    std::tuple<world_state, uint32_t> getRequestResponse();

    void streamData(std::function<void (world_state&, uint32_t)> solutionCallback);

    bool _connected;
  public:

    ClientWorldModel(std::string ip, uint16_t port);
    ~ClientWorldModel();

    /*
     * Return the status of the connection.
     */
    bool connected();

    //Return the data synchronously
    std::tuple<world_state, uint32_t> getCurrentSnapshot(world_model::URI uri,
        std::vector<world_model::URI>& attributes, uint32_t ticket);
    std::tuple<world_state, uint32_t> getHistoricSnapshot(world_model::URI uri, std::vector<world_model::URI>& attributes,
        world_model::grail_time start, world_model::grail_time stop, uint32_t ticket);

    //Return the data synchronously
    std::tuple<world_state, uint32_t> getRange(world_model::URI uri, std::vector<world_model::URI>& attributes,
        world_model::grail_time start, world_model::grail_time stop, uint32_t ticket);

    //Return data through a provided callback in a separate thread.
    void setupAsynchronousDataStream(world_model::URI uri, std::vector<world_model::URI>& attributes,
        world_model::grail_time interval, std::function<void (world_state&, uint32_t)> solutionCallback, uint32_t ticket);

    //Sets up a data stream request but relies upon the solver calling getSynchronousStreamUpdate
    //to get data.
    void setupSynchronousDataStream(world_model::URI uri, std::vector<world_model::URI>& attributes,
        world_model::grail_time interval, uint32_t ticket);

    //Sets up a data stream request but relies upon the solver calling getSynchronousStreamUpdate
    //to get data. This version sets up a callback for this request.
    void setupSynchronousDataStream(world_model::URI uri, std::vector<world_model::URI>& attributes,
        world_model::grail_time interval, std::function<void (world_state&, uint32_t)> callback, uint32_t ticket);

    //Gets any new updates. If this is not called often enough the solver could time out from
    //the world model. If updates are received that have callbacks registered those callbacks
    //will be called and this will wait for another message.
    std::tuple<world_state, uint32_t> getSynchronousStreamUpdate();

    //Cancel request message
    void cancelRequest(uint32_t ticket);

    //Search for world_model::URIs
    std::vector<world_model::URI> searchURIs(world_model::URI expression);
};


/**
 * Connection from a solver to the world model.
 */
class SolverWorldModel {
  public:
    struct Solution {
      u16string type;
      world_model::grail_time time;
      world_model::URI target;
      std::vector<uint8_t> data;
    };
    typedef std::map<world_model::URI, std::vector<world_model::Attribute>> world_state;
  private:
    struct TransientArgs {
      u16string request;
      regex_t exp;
      bool valid;
      bool operator<(const TransientArgs& other) const {
        return request < other.request;
      }
    };
    //Send a handshake and a type declaration message.
    bool reconnect();

    //Thread and variables to monitor transient request status.
    std::mutex trans_mutex;
    std::map<uint32_t, std::multiset<TransientArgs>> transient_on;
    std::thread transient_tracker;
    bool interrupted;
    bool running;

    void trackTransients();

    std::vector<world_model::solver::AliasType> types;
    std::map<u16string, uint32_t> aliases;
    //This solver's origin string
    std::u16string origin;

    ClientSocket s;
    GRAILSockServer ss;
    std::string ip;
    uint16_t port;

    bool _connected;
  public:

    /*
     * Connect to the world model at the given ip address and port and
     * immediately announce the provided types.
     * Transient types are indicated with a true boolean value in their pairs.
     */
    SolverWorldModel(std::string ip, uint16_t port, std::vector<std::pair<u16string, bool>>& types, std::u16string origin);
    ~SolverWorldModel();

    /*
     * Return the status of the connection.
     */
    bool connected();

		/*
		 * Register new solution types.
		 */
		void addTypes(std::vector<std::pair<u16string, bool>>& new_types);

    /*
     * Send new data to the world model.
     * If create_uris is true then any URIs that are named as targets but that
     * do not exist in the world model will be created so that these
     * solutions can be pushed. If create_uris is false then those
     * solutions will not be pushed.
     */
    void sendData(std::vector<Solution>& solution, bool create_uris = true);

    /*
     * Create a new URI in the world model.
     */
    void createURI(world_model::URI uri, world_model::grail_time created);

    /*
     * Expire a world_model::URI in the world model.
     */
    void expireURI(world_model::URI uri, world_model::grail_time expires);

    /*
     * Delete a URI from the world model.
     */
    void deleteURI(world_model::URI uri);

    /*
     * Expire an attribute from the world model.
     */
    void expireURIAttribute(world_model::URI uri, std::u16string name, world_model::grail_time expires);

    /*
     * Delete an attribute from the world model.
     */
    void deleteURIAttribute(world_model::URI uri, std::u16string name);
};


/**
 * Connect to the GRAIL aggregator at the specified ip and port and
 * subscribe to any regions with a subscription in the provided
 * subscriptions vector. When new packet data is received it will
 * be passed to the packCallback.
 * This function blocks until the connection is broken.
 */
void grailAggregatorConnect(const std::vector<NetTarget>& servers,
    std::vector<aggregator_solver::Subscription> subscriptions, std::function<void (SampleData&)> packCallback);

/**
 * Maintain connections to multiple aggregators and allow the user to easily
 * update rules.
 * This is threaded - all function calls return.
 * The packCallback will be protected by a mutex and need not be thread safe.
 */
class SolverAggregator {
  private:
    std::vector<NetTarget> servers;
    std::function<void (SampleData&)> packCallback;
    std::mutex callback_mutex;
    std::vector<std::thread> server_threads;
    //Control access to the subscription list.
    std::mutex sub_mutex;
    aggregator_solver::Subscription subscription;
    //Variable to interrupt aggregator connections
    bool interrupted;
  public:
    SolverAggregator(const std::vector<NetTarget>& servers, std::function<void (SampleData&)> packCallback);
    ~SolverAggregator();
    void updateRules(aggregator_solver::Subscription subscription);
    void Disconnect(); 
};

/**
 * Connect to the distribution layer and request some solutions.
 * The solutions will be returned in the provided callback function.
 * The time_begin and time_end arguments give a time range for this
 * data query. If time_begin and time_end are equal or if time_end
 * is less than time_begin live data will be sent.
 * By default this function only requests live data.
 * Clients should use this to connect to the grail distributor.
 */
void grailDistributorConnect(std::string ip, uint16_t port,
    std::function<bool (const solver_distributor::SolutionType&)> solutionInterest,
    std::function<void (solver_distributor::SolverDataMsg&)> solutionCallback,
    time_t time_begin = 0, time_t time_end = 0);


#endif


