/*
celocalize.{cpp,hpp} - Functions for localization using cross entropy
Source version: May 6, 2011
Copyright (c) 2011 Bernhard Firner

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
  claim that you wrote the original software. If you use this software
  in a product, an acknowledgment in the product documentation would be
  appreciated but is not required.

  2. Altered source versions must be plainly marked as such, and must not be
  misrepresented as being the original software.

  3. This notice may not be removed or altered from any source
  distribution.
*/

#ifndef __celocalize_hpp__
#define __celocalize_hpp__

#include <vector>
#include <localize_common.hpp>

using std::vector;

namespace CELocalize {

  struct Result {
    double x;
    double y;
  };

  /*
   * Cross Entropy based location solver.
   * Solves for the location of any items in the training data that have
   * nan values for coordinates. Each solution is in the form of a single
   * point where the transmitter is most likely located.
   * Any missing data points should have nan values.
   */
  std::vector<Result> localize( TrainingData train_data,
                                LandmarkData rx_locations,
                                 double region_width,
                                 double region_height,
                                 double resolution);

  /*
   * Pure probability localization.
   */
  std::vector<Result> bayesLocalize( TrainingData train_data,
      LandmarkData rx_locations,
      double region_width,
      double region_height,
      double resolution);

  /*
   * Pure probability localization using radial estimates for alpha.
   */
  std::vector<Result> radialBayesLocalize( TrainingData train_data,
      LandmarkData rx_locations,
      double region_width,
      double region_height,
      double resolution,
      vector<vector<double>> antenna_gain = vector<vector<double>>());

  /*
   * Walk along areas of high probability, like M2.
   */
  std::vector<CELocalize::Result> bayesWalkLocalize( TrainingData train_data,
      LandmarkData rx_locations,
      double region_width,
      double region_height,
      double resolution,
      vector<vector<double>> antenna_gain = vector<vector<double>>());

  /*
   * Walk along areas of high probability, like M2, but use voting by receivers
   * rather than probability.
   */
  std::vector<CELocalize::Result> bayesWalkVoteLocalize( TrainingData train_data,
      LandmarkData rx_locations,
      double region_width,
      double region_height,
      double resolution,
      vector<vector<double>> antenna_gain = vector<vector<double>>());

  std::vector<CELocalize::Result> areaBasedProbability( TrainingData train_data,
      LandmarkData rx_locations,
      double region_width,
      double region_height,
      double resolution,
      vector<vector<double>> antenna_gain);

  std::vector<CELocalize::Result> clusterLocalization( TrainingData train_data,
      LandmarkData rx_locations,
      double region_width,
      double region_height,
      double resolution,
      vector<vector<double>> antenna_gain);
}
#endif


