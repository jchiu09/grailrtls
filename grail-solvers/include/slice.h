/*
*
* Copyright (c) 2006 Rutgers University and Konstantinos Kleisouris
* All rights reserved.
*
* Permission to use, copy, modify, and distribute this software and its
* documentation for any purpose, without fee, and without written agreement is
* hereby granted, provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the distribution.
*
* 3. Neither the name of the University nor the names of its
* contributors may be used to endorse or promote products derived from
* this software without specific prior written permission.
*
* IN NO EVENT SHALL RUTGERS UNIVERSITY BE LIABLE TO ANY PARTY FOR DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT
* OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF RUTGERS
* UNIVERSITY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* RUTGERS UNIVERSITY SPECIFICALLY DISCLAIMS ANY WARRANTIES,
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
* AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS
* ON AN "AS IS" BASIS, AND RUTGERS UNIVERSITY HAS NO OBLIGATION TO
* PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*
*
* Author: Konstantinos Kleisouris
* Version: $Id: slice.h,v 1.1.2.2 2010/09/07 17:33:55 bfirner Exp $
* Creation Date: 06/27/2006
* Filename: slice.h
* History:
* $Log: slice.h,v $
* Revision 1.1.2.2  2010/09/07 17:33:55  bfirner
* Updates to files. Localization solver now sends data to the test aggregator.
*
* Revision 1.1  2007/09/17 05:01:08  stryke3
* Initial commit.
*
* Revision 1.3  2007/04/30 21:51:09  kkonst
* Added solver code
*
* Revision 1.1  2006/07/19 20:13:22  kkonst
* Added source code of fast solver under demo3
*
* Revision 1.1.1.1  2006/06/27 15:15:52  kkonst
* Imported fast solver code
*
*/

#ifndef __SLICE_H__
#define __SLICE_H__

#include <vector>
using std::vector;

struct mc_dynamic_state
{ 
  int dim;
  //Position variables
  std::vector<double> q;
  //Momentum variables
  std::vector<double> p;
  //Gradient of potential energy with respect to position
  std::vector<double> grad;
  double pot_energy;	/* Potential energy of position */

  /*
  double *prob;
  int *x1;
  int *d1;
  */


  //Indices in the q array
  int last_bprior_index;
  int last_taubprior_index;
  //Initial signal powers
  int first_b0i_index;
  //Propagation coefficients
  int first_b1i_index;
  int first_taui_index;
  int first_x_index;
  int first_y_index;
  int last_y_index;
  double DOMAIN_X;
  double DOMAIN_Y;

  mc_dynamic_state(int MAX_APS, int predictNo, int total_network_variables, double DOMAIN_X, double DOMAIN_Y);
}; 

typedef std::vector< std::vector<double> > double_matrix;

struct data_specifications
{
  int N_inputs;			/* Number of "input" values */
  //TODO this can always be replaced by APS_X.size()
  //int N_targets;		/* Number of "target" values */
  int N_cases;                  /* Number of lines in the training file */

  //TODO These should just be a single vector of points
  //The x and y coordinates of the access points (receivers)
  std::vector<double> APS_X;
  std::vector<double> APS_Y;
  //TODO This variable is unecessary as the APS_X.size() value could be used
  int MAX_APS;

  int predictNo;
  int observations_nopredict;

  //Vectors of the x and y coordinates for the transmitters
  std::vector<double> train_x;
  std::vector<double> train_y;
  vector<vector<double> > D;     /* Array of Arrays for Ds */
  vector<vector<double> > Dsqrd; /* Array of Arrays for D^2 */

  //The signal strengths of each AP at each transmitter
  double_matrix train_signal;

  data_specifications(int N_inputs, int MAX_APS, int N_cases,
      const std::vector<double>& train_x, const std::vector<double>& train_y,
      const double_matrix& train_signal, int predictNo, int observations_nopredict,
      const std::vector<double>& APS_X, const std::vector<double>& APS_Y);
};


double conjugateNormalB0i_PosteriorMeanSum_1 (mc_dynamic_state& ds,
    const data_specifications& data_spec, int target_offset, double x_fixed,
    double y_fixed, double b, double precision, int& cntminus1);

void mc_app_energy(mc_dynamic_state& ds, const data_specifications& data_spec, double& energy, int k);
void mc_app_energy_2D(mc_dynamic_state& ds, data_specifications& data_spec, double& energy, int k, int grad);
void output_likelihood (mc_dynamic_state& ds, data_specifications& data_spec, int k);
void step_out(mc_dynamic_state& ds, const data_specifications& data_spec, int k, double slice_point, 
	      double curr_q, int max_steps, double& low_bnd, double& high_bnd, double width,
	      double lower, double higher, int defined);  
void dbl_out(mc_dynamic_state& ds, data_specifications& data_spec, int k, double slice_point, 
	     double curr_q, int max_int, double& low_bnd, double& high_bnd, double width,
	     double lower, double higher, int defined);
void pick_value(mc_dynamic_state& ds, data_specifications& data_spec, int k, double slice_point, double curr_q,
		int max_steps, double low_bnd, double high_bnd, int s_factor, double s_threshold, double width, int& rejections);
void mc_heatbath(mc_dynamic_state& ds, double temperature, double decay, int start_index, int end_index);
void mc_met_1(mc_dynamic_state& ds, data_specifications& data_spec, double sf, double temperature,
	      int b_accept, int k, bool use_domain, double lower, double higher, int& prop, int& rej);
void mc_slice_1(mc_dynamic_state& ds, data_specifications& data_spec, int max_steps, int s_factor,	
		double s_threshold, int k, double width, bool use_domain, int bounded, double low, double high,
		int& rejections);
void mc_slice_mult2D(mc_dynamic_state& ds, data_specifications& data_spec, std::vector<double>& save, std::vector<double>& lowb,	
		     std::vector<double>& highb, int k, int g_shrink, double width, bool use_domain, double low_x,
		     double high_x, double low_y, double high_y, int& rej);
void finite_update(mc_dynamic_state& ds, data_specifications& data_spec, int k);


#endif

