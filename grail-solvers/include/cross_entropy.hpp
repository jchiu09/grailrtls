/*
 * cross_entropy.{cpp,hpp} - Solve optimization problems with cross entropy
 * Source version: January 6, 2011
 * Copyright (c) 2011 Bernhard Firner
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *   1. The origin of this software must not be misrepresented; you must not
 *     claim that you wrote the original software. If you use this software
 *     in a product, an acknowledgment in the product documentation would be
 *     appreciated but is not required.
 *
 *   2. Altered source versions must be plainly marked as such, and must not be
 *      misrepresented as being the original software.
 *
 *   3. This notice may not be removed or altered from any source
 *      distribution.
 */


/*******************************************************************************
 * Solve an optimization problem using the cross entropy method.
 * See "A Tutorial on the Cross-Entopy Method" by
 * Boer, Kroese, Mannor, and Rubinstein for more information.
 ******************************************************************************/

#ifndef __CROSS_ENTROPY_HPP__
#define __CROSS_ENTROPY_HPP__

#include <vector>
#include <functional>

using std::vector;
using std::function;

//TODO let the user specify the stopping condition.

struct CEIteration {
  int round;
  float min_quantile_performance;
  float avg_quantile_performance;
  float avg_performance;
  vector<float> probability_vector;
};


/**
 * Run the cross entropy method with N simulations per round,
 * using the upper (1-quantile)*N results of each round to
 * form the probability vector for the next round, and using
 * initial_p_vector as the probility vector for round 0.
 * The simulation tries to reach a score of goal.
 * When successive iterations fail to improve by more than min_improve
 * the algorithm will return.
 * The function scoring_sim accepts a decision vector that
 * specifies the behavior for the monte carlo simulation. The
 * score of the simulation with that given decision vector is
 * returned.
 */
vector<CEIteration> runCE(std::size_t N, float quantile, float goal, float min_improve,
    vector<float> initial_p_vector, function<float (vector<bool>)> scoring_sim);

#endif

