/*******************************************************************************
 * grail_sock_server.{cpp,hpp} Files that simplify running a grail server.
 ******************************************************************************/

#ifndef __GRAIL_SOCK_SERVER_HPP__
#define __GRAIL_SOCK_SERVER_HPP__

#include <vector>
#include "simple_sockets.hpp"

class GRAILSockServer {
  public:
    //Hold a buffer for a unfinished piece sent in the last packet that
    //needs a new TCP packet to be processed completely.
    std::vector<unsigned char> previous_unfinished;

    ClientSocket& sock;

    GRAILSockServer(ClientSocket& s);

    /**
     * Nonblocking call that checks if a message is ready
     * to be read. If this returns true then the getNextMessage
     * function will return a new packet immediately.
     */
    bool messageAvailable(bool& interrupted);

    /**
     * Blocking call that returns the next message in a vector.
     * This always returns whole single messages regardless of how
     * packets are combined or split in transit.
     * If interrupted becomes true this will return an empty buffer.
     * This is most effective when the ClientSocket is non-blocking.
     */
    std::vector<unsigned char> getNextMessage(bool& interrupted);
};

#endif

