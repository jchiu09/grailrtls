#ifndef __LDA_H__
#define __LDA_H__

#include <map>
#include <string>
#include <vector>

using std::map;
using std::string;
using std::vector;

class lda {

	public: 
		lda(int cell_num, int link_num, int sample_num);
	
		lda(string filename, int cell_num, int link_num, int sample_num);

		// update the mean and the covariance matrix of lda algorithm
		void formulate();

		void update_one(int cell, vector<vector<double> > data);

		//update the training data for lda
		void update_all(map<int, vector<vector<double> > > data);

		// return the linear discriminant function values
		vector<double> get_delta(vector<double> data);

		// return the variance likelihood
		vector<double> get_sigma(vector<double> data);

		void readfile(string filename);

		void logfile();

		void showdata();

		void clear();
	
	private:
		int cell_num;	// denoted as K
		int link_num;	// denoted as L
		int sample_num;	// denoted as N
		map<int, vector<vector<double> > > rssi_data;	// class data matrix is L by N
		map<int, vector<double> > lda_mean;		// data matrix is K by L
		map<int, vector<double> > lda_variance;		// data matrix is K by L
		vector<vector<double> > lda_covariance;		// data matrix is L by L

};

#endif
