/*
compass_localize.{cpp,hpp} - Functions for direction-based coarse localization
Source version: January 11, 2011
Copyright (c) 2011 Bernhard Firner

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
  claim that you wrote the original software. If you use this software
  in a product, an acknowledgment in the product documentation would be
  appreciated but is not required.

  2. Altered source versions must be plainly marked as such, and must not be
  misrepresented as being the original software.

  3. This notice may not be removed or altered from any source
  distribution.
*/
#ifndef __compass_localize_hpp__
#define __compass_localize_hpp__

#include <vector>

#include <localize_common.hpp>

//Uses a compass comparison based upon pairs of receivers to find tiles
//where the unknown sample could have originated. Only use pairs of
//receivers if they are separated by at least the min_dist and
//decide that a signal comes from one side or another if it is at
//least min_diff RSS stronger on that side.
//The result returned is a set of possible tiles if the region
//was divided into tiles with the given widths and heights.
//This function is factorial with the number of receivers.
std::vector<Tile> possibleCompassTiles(const Sample& s,
                                       const LandmarkData& rxers,
                                       double max_x,
                                       double max_y,
                                       double tile_w,
                                       double tile_h,
                                       double min_dist,
                                       double min_diff,
                                       double percentile);




#endif

