#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <usb.h>
#include <errno.h>
#include <sys/time.h>

#include <fcntl.h>
#include <termios.h>
#include <sys/signal.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <time.h>

#include "simple_sockets.hpp"
#include "sensor_aggregator_protocol.hpp"

#include <iostream>
#include <string>
#include <list>
#include <map>
#include <algorithm>
#include <stdexcept>
#include <vector>
#include <fstream>
#include <sstream>

//Handle interrupt signals to exit cleanly.
#include <signal.h>

using std::string;
using std::list;
using std::map;
using std::pair;
using std::vector;

using namespace std;

#define TRAN_INTERVAL 200000/64	// in microseconds

//Global variable for the signal handler.
bool killed = false;

//Signal handler.
void handler(int signal) {
	psignal( signal, "Received signal ");
  	if (killed) {
    		std::cerr<<"Aborting.\n";
    		// This is the second time we've received the interrupt, so just exit.
    		exit(-1);
  	}
  	std::cerr<<"Shutting down...\n";
  	killed = true;
}

int main(int ac, char** arg_vector) {

	if (ac != 3) {
    		cerr<<"This program requires 2 arguments,"<<
      			" the ip address and the port number of the aggregation server to send data to.\n";
    		cerr<<"An optional third argument specifies the minimum RSS for a packet to be reported.\n";
    		return 0;
  	}
  	//Get the ip address and ports of the aggregation server
  	std::string server_ip(arg_vector[1]);
  	int server_port = atoi(arg_vector[2]);

	//Set up a socket to connect to the aggregator.
	ClientSocket agg;
	agg.domain = AF_INET;
	agg.type   = SOCK_STREAM;
	agg.protocol = 0;
	agg.port = server_port;
	agg.ip_address = server_ip;

	vector<uint128_t> tx_ids;
	vector<uint128_t> rx_ids;

	int buffer;
	string data;
	ifstream infile;

	infile.open("dfp.txt");
	if (!infile){
		cout<<"File open error!\n";
		return 0;
	}

	int file_line = 0;

	while(getline(infile, data)) {
		istringstream s(data);
		if(file_line == 0) {
			while(s>>buffer){
				tx_ids.push_back(buffer);
			}
		}
		else if (file_line == 1) {
			while(s>>buffer){
				rx_ids.push_back(buffer);
			}
		}
		file_line++;
	}

  	while (not killed) {
    		bool connected = false;
    		if (agg.setup()) {
      		std::cerr<<"Connected to the GRAIL aggregation server.\n";

      		//Try to get the handshake message
        	std::vector<unsigned char> handshake = sensor_aggregator::makeHandshakeMsg();

		//Send the handshake message
		agg.send(handshake);
		std::vector<unsigned char> raw_message(handshake.size());
		size_t length = agg.receive(raw_message);

		//Check if the handshake message failed
		if (not (length == handshake.size() and
		      std::equal(handshake.begin(), handshake.end(), raw_message.begin()) )) {
		    	//Quit on failure - what we are trying to connect to is not a proper server.
			std::cerr<<"Failure during handshake with aggregator - aborting.\n";
			    	killed = true;
			}
			else {
				connected = true;
			}
    		} 
		else {
			std::cerr<<"Failed to connect to the GRAIL aggregation server.\n";
    		}

		while (not killed) {
			for(int i = 0; i < tx_ids.size(); i++){
				for(int j = 0; j < rx_ids.size(); j++){
					SampleData sd;
					sd.physical_layer = 1;
					sd.tx_id = tx_ids[i];
					sd.rx_id = rx_ids[j];
					timeval tval;
					gettimeofday(&tval, NULL);
					sd.rx_timestamp = tval.tv_sec*1000 + tval.tv_usec/1000;
					srand (time(NULL));
					sd.rss = -(20 + rand() % 50);
					sd.sense_data.push_back(sd.rss);
					sd.valid = true;
					cout<<"Packet tx "<<sd.tx_id<<" to rx "<<sd.rx_id<<" with RSSI "<<sd.rss<<endl;
			    		agg.send(sensor_aggregator::makeSampleMsg(sd));
					//cout<<"Already sent the data packet to the aggregator"<<endl;
	    				usleep(TRAN_INTERVAL);
				}
			}
		}
	    	//Try to reconnect to the server after losing the connection.
	    	//Close the socket
        if (agg.sock_fd != (int)NULL) {
          close(agg.sock_fd);
        }

    		//Sleep a little bit, then try connecting to the server again.
    		usleep(1000);
  	}

	std::cerr<<"Exiting\n";
}
