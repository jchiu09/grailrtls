/***********************************************************************************************
this program reads wifi packages by monitoring wireless interfaces by using pcap-dev and sends
the packages out to aggregation server. for working properly, requests MadWifi driver installed
and turn on Radiotap header format.
this program doesn't take any command line arguments. for runing the program, have to configure
wifi_conf.xml file first. configuration includes: Debug on or not, aggregation server ip and
listening port, wireless device interfaces to monitor, and wifi device filters
***********************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pcap.h>
#include <sys/types.h>
#include <stdint.h>
#include <vector>
#include <string>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>
#include <typeinfo>
#include <stdexcept>
#include <iostream> 
#include <fstream>
using namespace std;


#include "sensor_aggregator_protocol.hpp"
#include "simple_sockets.hpp"
#include "sample_data.hpp"
#include "ieee80211.h"
#include "rapidxml.hpp"
using namespace rapidxml;

#define CHANNEL_TIMEOUT 1 
#define LM_MAX_PACKET_LEN 256

#if defined(__i386__) || defined(__i586__) || defined(__i686__)  || defined(__x86_64__)
#define letoh16(x) (x)
#define letoh32(x) (x)
#endif

struct ieee80211_radiotap_header{
        u_int8_t        it_version;     /* set to 0 */
        u_int8_t        it_pad;
        u_int16_t       it_len;         /* entire length */
        u_int32_t       it_present;     /* fields present */
} __attribute__((__packed__));


/* Defintions for madwifi driver radiotap header */
 struct ath_rx_radiotap_header {
        struct ieee80211_radiotap_header wr_ihdr;
        u_int64_t wr_tsft;
        u_int8_t wr_flags;
        u_int8_t wr_rate;
        u_int16_t wr_chan_freq;
        u_int16_t wr_chan_flags;
        int8_t  wr_dbm_antsignal;
        int8_t  wr_dbm_antnoise;
        u_int8_t wr_antenna;
        u_int8_t wr_antsignal;
}__attribute__((__packed__));

typedef struct mac_filter
{
        unsigned long long mac;
        unsigned long long mask;
}mac_filter_t;

typedef struct wifi_device
{
        string dev;
        unsigned long long  mac_id;
}wifi_device_t;

struct DEBUG{
        bool on;
};

template<typename T>
DEBUG& operator<<(DEBUG& dbg, T arg){
        if(dbg.on)
           cout<<arg;
        return dbg;
}

struct DEBUG debug;

/* declare internal utility functions */
//get SampleData from pcap package
int  pcap_get_sample(pcap_t* , struct SampleData*  ,   wifi_device_t );

// wifi_conf.xml file to initialize wifi device filters, channels to monitor, device interfaces to monitor, server_ip and server_port, debug on or off
bool read_conf(string,  vector<mac_filter_t>&,  vector<wifi_device_t>&, string&, string&, string&);

//convert Mac address in string type into unsigned long long type
unsigned long long MacStrToLong(string );

// check a MAC address if pass the filter list
bool check_filter(unsigned long long , const vector<mac_filter_t> );

//turn on debug output if set in wifi_conf.xml
void set_debug(string);

//check for IP packet and IP length
long calcIPlength(const u_char*, int);

//check for IP packet header length
int calcIPheader(const u_char*, int);


int main(int argc, char* argv[]){
	
	string wifi_conf;
	if(argc == 1)
		wifi_conf = "wifi_conf.xml";
	else if(argc == 2)
		wifi_conf = argv[1];
	else{
		cout<<"command line input should be: wifi_sense file_name_of_wifi_config (the second parameter is optional, default is wifi_conf.xml)\n";
		exit(1);
	}
	vector<mac_filter_t> mac_vc;
	//vector<int> channels;
	vector<wifi_device_t> inDev;
	string server_ip;
	string s_port;
	string dbg;
	read_conf(wifi_conf, mac_vc,  inDev, server_ip, s_port, dbg);
	set_debug(dbg);  // turn on debug output if set in wifi_conf.xml
	if(server_ip.size() == 0 || s_port.size() == 0)
	{
		cerr << " please check server ip or port number in wifi_conf.xml \n";
		exit(1);
	}
	
        char errorBuffer[PCAP_ERRBUF_SIZE];
        int res; 
	
	// setup socket to connect to aggregator
	int port = atoi(s_port.c_str());
	ClientSocket agg(AF_INET, SOCK_STREAM, 0, port, server_ip);
	
	// open pcap device
        vector <pcap_t*> pHandle;


	for(int i = 0; i < inDev.size(); i++){	

      	 	pcap_t* p = pcap_open_live(inDev[i].dev.c_str(), BUFSIZ, 1, 1000, errorBuffer);
		
        	if (p == NULL)
         	{
              		fprintf(stderr,"error opening device %s : %s\n",inDev[i].dev.c_str(), errorBuffer);
              		exit(1);
         	}
		pHandle.push_back(p);

	}

        //the outer while loop try to connect to aggregation server, in case server down and come back, always catch it
        while( true){
                if(!agg){
                        debug<<"error on connecting aggregation server, try again after sleep while\n";
			sleep(5); // sleep 5 second and try again 
                        continue;
                }
                cout<<"connected to aggregation server"<<endl; 

                //doing handshake 
                vector<unsigned char> handshake = sensor_aggregator::makeHandshakeMsg();
                agg.send(handshake);
                vector<unsigned char> handshake_back(handshake.size());
                size_t length = agg.receive(handshake_back);

		if( !(length == handshake.size() && equal(handshake.begin(), handshake.end(), handshake_back.begin()))){
			cerr<<" Failure during handshake with aggregator - aborting..."<<endl;
			break;
		}

	
		// get the pcap package one by one, and send the message out to aggregation server
		try{ // agg.send() throw runtime exception
	        while( 1 ){
			for(int i = 0; i < pHandle.size(); i++){
			struct SampleData lm_sample;
				res = pcap_get_sample(pHandle[i],  &lm_sample, inDev[i]  );
				if(res == 0){
					debug << "pcap_get_sample time out, try again\n";
				}
				else if(res < 0){
					cerr << "pcap_get_sample got error, reconnect device\n";
					pcap_close(pHandle[i]);	
					pHandle[i] = pcap_open_live(inDev[i].dev.c_str(), BUFSIZ, 1, 1000, errorBuffer);
					if (pHandle[i] == NULL)
       			                {
                       			        fprintf(stderr,"error opening device %s : %s\n",inDev[i].dev.c_str(), errorBuffer);
                                 		exit(1);
                        		}
				}
				else if(res >= 1 ){
				        if(check_filter(lm_sample.tx_id.lower,  mac_vc)) {
						agg.send(sensor_aggregator::makeSampleMsg(lm_sample));
					}
                        	}
			}

		}  //endof while(1)
	   } //end of try
           catch (runtime_error& re) {
      		cerr<<"wifi sensor layer error: "<<re.what()<<'\n';
    	   }
    	  catch (exception& e) {
                cerr<<"wifi sensor layer error: "<<e.what()<<'\n';
          }
    	//Try to reconnect to the server after losing the connection.
   	 //Close the socket
    //	  close(agg.sock_fd);

	} // end of while(true)
	
	cout <<"Exiting"<<endl;
	//close pcap device handle
	for(int i = 0; i < pHandle.size(); i++){
        	pcap_close(pHandle[i]);
	}
	return 0;
}


int  pcap_get_sample(pcap_t* pHandle, struct SampleData*  lm_sample,   wifi_device_t dev){

                /* variables to hold packet info from the wireless subsystem */
        struct pcap_pkthdr* pcap_header;
        const u_char *packet;           /* the pcap packet */
        struct ath_rx_radiotap_header* rdtHdr;   /* the radiotap header */
        int rssi;                 /* the received signal strength indication */
        int copy_len;             /* length of the packet we copy into the sample */
        int res;
        ieee80211_hdr_t *wlanHdr; /* pointer to the initial header */
        u_int8_t *tx_mac_p;   /* pointer to the bytes of the mac addr */
        long long tx_mac64;   /* a 64-version of the mac address */
        int wlanHdrLen;    /* length of the 802.11 header */
        ieee80211_hdr_3addr_t *wlan_3addr_hdr;   /* 802.11 3-address header */
        ieee80211_hdr_4addr_t *wlan_4addr_hdr;   /* 802.11 4-address header */
ieee80211e_hdr_3addrqos_t *wlan_3addrqos_hdr;  /* 802.11e 3-address header */
ieee80211e_hdr_4addrqos_t *wlan_4addrqos_hdr;  /* 802.11e 4-address header*/
	int radioTapHeaderLength;

        lm_sample->valid = false;

        res =  pcap_next_ex(pHandle, &pcap_header, &packet);
         if(res <0){
                cerr<< " pcap read error --return failure \n";
                return -1;
        }

        if( res == 0){
			debug << "pcap read time out - try again \n";
                        return 0;

                }

		debug << "radioTap:" << pHandle << " pcap packet length is " << pcap_header->len <<"\n";
		debug << "radioTap:"<< pHandle <<" time is " << pcap_header->ts.tv_sec << " " << pcap_header->ts.tv_usec << "\n";	


                /* cast u_char pointer to ath_rx_radiotap_header*, which make rdtHdr point to the beginning of mem address of packet */
                rdtHdr = (struct ath_rx_radiotap_header *)packet;
                /* get rssi */
                rssi = (int) rdtHdr->wr_dbm_antsignal;
		radioTapHeaderLength = letoh16(rdtHdr->wr_ihdr.it_len);
		debug << "radiotap header len = " << radioTapHeaderLength << "\n";
                /* move pointer wlanHdr pass over radiotap header, pointing to the first byte of 802.11 header */
                /* get landmark wifi card Mac address --- transmitter address */
                wlanHdr  = (ieee80211_hdr_t *) (packet + letoh16(rdtHdr->wr_ihdr.it_len));
                wlanHdrLen = ieee80211_get_hdrlen(letoh16(wlanHdr->frame_ctl));
		debug << "80211_Hdrlen = " << wlanHdrLen << "\n";

int totallength = pcap_header->len;

fprintf(stdout, "Total len: %d \n RT Hdr len: %d \n 802.11 Hdr len: %d \n LLC(presumed): %d \n", totallength, radioTapHeaderLength, wlanHdrLen, 8 );
//fprintf(stdout, "Total len: %d \n RT Hdr len: %d \n 802.11 Hdr len: %d \n", totallength, radioTapHeaderLength, wlanHdrLen );

int headerLengths = radioTapHeaderLength+wlanHdrLen;
int base = headerLengths+8;
int IPheaderLength = -1;

fprintf( stdout, "IP header parsing: \n" );


if ((base+4) >= totallength)
{
	//don't drop it - trimming at length is probably fine, who knows?
	// it is too small to even have an IP packet length field,
	// so it is definitely not IP
	fprintf(stdout,"no IP packet \n");
}
else if ((base+4)< totallength)
{
	//could be IP... oh bloody hell
	//  need to test if it is IP:

	fprintf(stdout,"Possibly an IP packet\n");
	fprintf(stdout," checking total packet length...\n");

	//  pretend it is IP...
	//  get what should be the length of the entire IP packet
	//   from where the length would be (3rd and 4th bytes)
	long computedLength = calcIPlength(packet, headerLengths);

	fprintf(stdout, " IP packet total length: %ld\n", computedLength);
	computedLength = computedLength+base+4;
	fprintf(stdout, " Decoded packet total length: %ld\n", computedLength);

	// add the 'decoded' IP packet length with the header lengths that
	// came before (plus the 4-byte 802.11 CRC that comes at the end)
	// if this sum equals the total capture length as reported by pcap,
	// then it is very, very likely an IP packet

	if (totallength == computedLength)
	{
		// if decoding the length works, then read the header length
		//  header length is 2nd short (4-bits) and denotes the number of
		//  32-bit lines in the header
		// the trim length is then the length of all the previous headers plus
		// the number of IP header lines * 32b (or 32b/8 -> 4B)

		fprintf(stdout," Decoded length matches pcap length, IP format presumed\n");
		IPheaderLength = calcIPheader(packet, headerLengths+8);

		fprintf(stdout, "  IP header length: %d\n", IPheaderLength);
		fprintf(stdout, "Total header lengths in bytes: %d\n", headerLengths+8+(IPheaderLength));
	}
	else
	{
		// don't know what to do here... drop it? ...  save it?
		fprintf(stdout, " !!!!Lengths do not match, definitely not a recognized IP format \n");
	}
}
else
{
fprintf(stdout, "Unknown packet type\n");
}


fprintf(stdout, ":.. \n\n\n");

//vector<unsigned char>(packet, packet+(radioTapHeaderLength, wlanHdrLen));
//fprintf(stdout, "partial frame dump: %X",  )

                switch(wlanHdrLen){
			case sizeof( ieee80211e_hdr_3addrqos_t): /* 3-address 802.11e frame. Guessing on TX */
                                wlan_3addrqos_hdr = (ieee80211e_hdr_3addrqos_t *) wlanHdr;
                                tx_mac_p = (u_int8_t *) wlan_3addrqos_hdr->addr2;
                                break;
                        case sizeof(ieee80211_hdr_3addr_t): /* a 3-address frame */
                                /* the transmitter address is always the 2nd address of the three */
                                wlan_3addr_hdr = (ieee80211_hdr_3addr_t *) wlanHdr;
                                tx_mac_p = (u_int8_t *) wlan_3addr_hdr->addr2;
                                break;
                        case sizeof( ieee80211e_hdr_4addrqos_t): /* 4-address 802.11e frame. Guessing on TX */
                                wlan_4addrqos_hdr = (ieee80211e_hdr_4addrqos_t *) wlanHdr;
                                tx_mac_p = (u_int8_t *) wlan_4addrqos_hdr->addr2;
                                break;
                        case  sizeof(ieee80211_hdr_4addr_t): /* 4 address frame. The TX address is always the 2nd address */
                                wlan_4addr_hdr = (ieee80211_hdr_4addr_t *) wlanHdr;
                                tx_mac_p = (u_int8_t *) wlan_4addr_hdr->addr2;

                                /* this hack for the mobilematrix tags. the source address is in the last address of a four address packet */
                                if ((wlan_4addr_hdr->addr2[0] == 0xff ) &&
                                        (wlan_4addr_hdr->addr2[1] == 0xff ) &&
                                        (wlan_4addr_hdr->addr2[2] == 0xff ) &&
                                        (wlan_4addr_hdr->addr2[3] == 0xff ) &&
                                        (wlan_4addr_hdr->addr2[4] == 0xff ) &&
                                        (wlan_4addr_hdr->addr2[5] == 0xff )) {
                                        tx_mac_p = (u_int8_t *) &(wlan_4addr_hdr->addr4);

                                }
                                break;
                        case sizeof(struct ieee80211_action):
                        case sizeof(struct ieee80211_probe_response):
                        case sizeof(struct ieee80211_assoc_request):
                        case sizeof(struct ieee80211_reassoc_request):
                                wlan_3addr_hdr = (ieee80211_hdr_3addr_t *) wlanHdr;
                                tx_mac_p = (u_int8_t *) wlan_3addr_hdr->addr2;
                                break;

                        default:
                                debug <<"unkown 802.11 frame size\n";
                                return 0;
                }


                /* timestamp in milliseconds since epoch */
                lm_sample->rx_timestamp = ( ( ((long long)(*pcap_header).ts.tv_sec )*1000) +
                ( ((long long)(*pcap_header).ts.tv_usec)/1000));
		
                 /* put the transmitter MAC into a 64-bit long long */
		char s[40]; 
                sprintf(s, "transmiter ID = %X:%X:%X:%X:%X:%X\n", tx_mac_p[0], tx_mac_p[1], tx_mac_p[2], tx_mac_p[3], tx_mac_p[4], tx_mac_p[5]);
		debug << s ;
                tx_mac64 = 0x0LL;
                tx_mac64 = ( ((long long) (tx_mac_p[0]) << 40 ) |
                ((long long) (tx_mac_p[1]) << 32 ) |
                ((long long) (tx_mac_p[2]) << 24 ) |
                ((long long) (tx_mac_p[3]) << 16 ) |
                ((long long) (tx_mac_p[4]) << 8 )  |
                ((long long) (tx_mac_p[5])));

//sprintf(tx_mac64," reclaimed?: %X:%X:%X:%X:%X:%X\n",tx_max64);

                lm_sample->rss= rssi;
        	debug << "transmitter ID in uint64_t  = " <<  tx_mac64 << "\n";
debug << "transmitter ID in uint128_t: " << uint128_t((uint64_t)tx_mac64) << "\n";
		debug << "rssi = " << rssi << "\n";

		lm_sample->tx_id = tx_mac64;
		lm_sample->rx_id = dev.mac_id;

		debug <<" receiver id in ulong = " << lm_sample->rx_id << "\n";

if ( IPheaderLength == -1 )
{
                copy_len = (pcap_header->len > LM_MAX_PACKET_LEN ) ? LM_MAX_PACKET_LEN : pcap_header->len;
}
else
{
		copy_len = headerLengths+8+IPheaderLength;
}
                lm_sample->sense_data = vector<unsigned char>(packet, packet+copy_len);
                lm_sample->physical_layer = 2; // 2 for wifi
                lm_sample->valid = true;
                return 1;
}


int calcIPheader(const u_char* packet, int headerLengths)
{
int result = -1;

long top8 = 0;
//int base = headerLengths+8;

char* pEnd;
char lengthval[] = "00";


	//decode number of 32-bit header lines
	// according to the standard, this is the 2nd short of the 1st byte

	//fprintf(stdout, "  Decoding header length:");
	//fprintf(stdout, "  reading hex byte: %X\n", packet[headerLengths]);

	sprintf(lengthval, "%X", packet[headerLengths]);
	top8 = strtol(lengthval, &pEnd, 16);

	//long headerlines = top8 & 15;
	int headerlines = top8 & 15;

	//fprintf(stdout, "  decoded number of IP header lines: %d\n", headerlines);

	result = headerlines*4;

return result;
}



long calcIPlength(const u_char* packet, int headerLengths)
{
	long result = -1;
	long bottom8;
	long top8;

	int base = headerLengths+8;

	char* pEnd;
	char lengthval[] = "00";


	sprintf(lengthval, "%X", packet[base+3]);
	bottom8 = strtol(lengthval, &pEnd, 16);

	lengthval[0] = '0';
	lengthval[1] = '0';

	sprintf(lengthval, "%X", packet[base+2]);
	top8 = strtol(lengthval, &pEnd, 16)*256;

	//long IPlength = bottom8+top8;

	result = bottom8+top8;

//fprintf(stdout, " decoded IP length: %ld\n", IPlength);

//long  computedLength = headerLengths+8+IPlength+4;

//fprintf(stdout, " total header-decoded frame length: %ld\n",computedLength);
//fprintf(stdout, " pcap-decoded frame length: %d\n", totallength);


return result;
}



		
bool read_conf(string filename, vector<mac_filter_t>& vc, 
 vector<wifi_device_t>& inDev, string& server_ip, 
string& server_port, string& dbg){
        ifstream fin(filename);
        if(fin.fail()){
                cerr << "cannot load xml file: " << filename <<"\n";
		cout<<"command line input should be: wifi_sense file_name_of_wifi_config (the second parameter is optional, default is wifi_conf.xml)\n";
		
               	exit(1);
        }
        fin.seekg(0, ios::end);
        size_t length = fin.tellg();
        fin.seekg(0, ios::beg);
        char* buffer = new char[length+1];
        fin.read(buffer, length);
        buffer[length] = '\0';
        fin.close();

        xml_document<> doc;
        doc.parse<0>(buffer);

        delete[] buffer;

        if( xml_node<>* n = doc.first_node("wifi_conf")->first_node("debug"))
                dbg = n -> value();

        if(xml_node<>* n = doc.first_node("wifi_conf")->first_node("server")->first_node("ip_address"))
                server_ip = n->value();
        else{
                cout<<"cannot not find  server IP address" << endl;
                return false;
        }

        if(xml_node<>* n = doc.first_node("wifi_conf")->first_node("server")->first_node("port"))
                server_port= n->value();
        else{
                cout<<"cannot not find server port" << endl;
                return false;
        }

      for(xml_node<>* n = doc.first_node("wifi_conf")->first_node("hub")->first_node(); n; n = n->next_sibling())
        {
                 if ( strcmp(n->name(), "antenna") == 0){
                        string dev, str_id;
                        unsigned long long mac_id;
                        if(n->first_node("device")){
                                dev = n->first_node("device")->value();
                        }
                        else{
                                cout<<"cannot find device" <<endl;
                                continue;
                        }

                      if(n->first_node("mac_id")){
                     str_id = n->first_node("mac_id")->value(); 
                               
			mac_id = MacStrToLong(str_id);
                       }
                        else{
                                cout<<"cannot find mac_id"<<endl;
                                continue;
                        } 
                        wifi_device_t w;
                        if(mac_id){
                                w.dev = dev;
                                w.mac_id = mac_id;
                        }
                        inDev.push_back(w);


                 }
              /*else if(strcmp( n->name() , "channel") == 0){
                        string str_ch = n->value();
                        int ch = atoi(str_ch.c_str());
                        channels.push_back(ch);
                }*/
                else{
                        cout<<" cannot not find antenna " << endl;
                } 

        }

        for(xml_node<>* n = doc.first_node("wifi_conf")->first_node("filter"); n ; n = n->next_sibling("filter")){
                string str_address;
                string str_mask;
                if(xml_node<>* cur = n->first_node("address")){
                        str_address = cur->value();
                }
                if(xml_node<>* cur = n->first_node("mask")){
                        str_mask = cur->value();
                }
                mac_filter_t m;
                unsigned long long mac = MacStrToLong(str_address);
                unsigned long long mask= MacStrToLong(str_mask);
                if(mac && mask){
                        m.mac = mac;
                        m.mask = mask;
                }
                else{
                        continue;
                }
                vc.push_back(m);
        }

        return true;

}

unsigned long long MacStrToLong(string str){
        unsigned long long v=0x0LL;

        for(int i=0; i<str.length(); i++){
                if(str[i] ==':' || str[i] =='-')
                        continue;
                if(str[i] >= '0' && str[i] <= '9')
                        v = (v<<4) | (str[i]-'0');
                else if(str[i] >= 'a' && str[i] <='f')
                        v = (v<<4) | (str[i]+10-'a');
                else if(str[i] >='A' && str[i] <= 'F')
                        v = (v<<4) | (str[i]+10-'A');
                else {
                        cerr<<"convert mac string to long long error\n";
                        v = 0;
                }
        }
        return v;
}

bool check_filter(unsigned long long mac, const vector<mac_filter_t> filters){
	if(filters.size() == 0) // if no filter at all, always return true.
		return true;
        for(int i=0; i < filters.size(); i++){
                if(mac == (filters[i].mac & filters[i].mask))
                        return true; //pass filters
        }

        return false;
}

void set_debug(string dbg){
	for(int i=0; i<dbg.size(); i++){
		dbg[i] = toupper(dbg[i]);
	}
	if(dbg == "ON"){
		debug.on = true;
	}
	else
		debug.on = false;
}

