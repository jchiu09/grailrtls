/*
triangulate.{cpp,hpp} - Functions to do Delaunay triangulation
Source version: January 11, 2011
Copyright (c) 2011 Bernhard Firner

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
  claim that you wrote the original software. If you use this software
  in a product, an acknowledgment in the product documentation would be
  appreciated but is not required.

  2. Altered source versions must be plainly marked as such, and must not be
  misrepresented as being the original software.

  3. This notice may not be removed or altered from any source
  distribution.
*/

#include <triangulate.hpp>

#include <algorithm>
#include <cmath>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <utility>
#include <vector>
#include <map>

using namespace std;

Point toPoint(const pair<double, double>& p) {
  return Point{p.first, p.second};
}

bool operator<(const Point& a, const Point& b) {
  return a.x < b.x or (a.x == b.x and a.y < b.y);
}

bool operator==(const Point& a, const Point& b) {
  return a.x == b.x and a.y == b.y;
}

bool operator!=(const Point& a, const Point& b) {
  return a.x != b.x or a.y != b.y;
}

bool operator==(const Tri& a, const Tri& b) {
  return a.a == b.a and a.b == b.b and a.c == b.c;
}

ostream& operator<<(ostream& os, const Point& p) {
  return os<<p.x<<','<<p.y;
}

ostream& operator<<(ostream& os, const Tri& t) {
  return os<<t.a<<';'<<t.b<<';'<<t.c;
}

Triple toBarycentric(Point p, Tri t) {
  ////The first two coordinates can be found with the determinant of the system.
  //double determinateA = (t.b.y - t.c.y)*(t.a.x - t.c.x) + (t.c.x - t.b.x)*(t.a.y - t.c.y);
  //double determinateB = (t.c.y - t.a.y)*(t.b.x - t.c.x) + (t.a.x - t.c.x)*(t.b.y - t.c.y);
  //double A = ((t.b.y - t.c.y)*(p.x - t.c.x) + (t.c.x - t.b.x)*(p.y - t.c.y)) / determinateA;
  //double B = ((t.c.y - t.a.y)*(p.x - t.c.x) + (t.a.x - t.c.x)*(p.y - t.c.y)) / determinateB;
  ////The third barycentric coordinate is what remains so that A+B+C=1
  //double C = 1.0 - A - B;
  //Take the determinate of the matrix: (x1-x3) (x2-x3)
  //                                    (y1-y3) (y2-y3)
  //Take the determinate of the matrix: (x2-x1) (x3-x1)
  //                                    (y2-y1) (y2-y1)

  double determinate =  (t.b.x - t.a.x) * (t.c.y - t.a.y) - (t.c.x - t.a.x) * (t.b.y - t.a.y);
  double A = ((t.b.x - p.x) * (t.c.y - p.y) - (t.c.x - p.x) * (t.b.y - p.y)) / determinate;
  double B = ((t.c.x - p.x) * (t.a.y - p.y) - (t.a.x - p.x) * (t.c.y - p.y)) / determinate;
  double C = ((t.a.x - p.x) * (t.b.y - p.y) - (t.b.x - p.x) * (t.a.y - p.y)) / determinate;
  //The third barycentric coordinate is what remains so that A+B+C=1
  //double C = 1.0 - A - B;
  return Triple{A, B, C};
}

bool insideTriangle(Point p, Tri t) {
  Triple bc = toBarycentric(p, t);
  return not (bc.a < 0 or bc.a > 1 or bc.b < 0 or bc.b > 1 or bc.c < 0 or bc.c > 1);
}
vector<Tri>::iterator findContainingTriangle(Point p, vector<Tri>& tris) {
  return find_if(tris.begin(), tris.end(), [&](Tri t){ return insideTriangle(p, t);});
}

void removeFromTMap(multimap<Point, Tri>& m, const Point& p, const Tri& t) {
  auto range = m.equal_range(p);
  while (range.first != range.second) {
    if (range.first->second == t) {
      m.erase(range.first);
      range = m.equal_range(p);
    } else {
      ++range.first;
    }
  }
}

void removeFromTMap(multimap<Point, Tri>& m, const Tri& t) {
  removeFromTMap(m, t.a, t);
  removeFromTMap(m, t.b, t);
  removeFromTMap(m, t.c, t);
}

void updateTMap(multimap<Point, Tri>& m, const Tri& t) {
  m.insert(make_pair(t.a, t));
  m.insert(make_pair(t.b, t));
  m.insert(make_pair(t.c, t));
}

void addTriangle(const Tri& t, vector<Tri>& tris, multimap<Point, Tri>& m) {
  //Don't save this triangle if all three points are on a line.
  if (not ( (t.a.x == t.b.x and t.a.x == t.c.x) or (t.a.y == t.b.y and t.a.y == t.c.y))) {
    tris.push_back(t);
    updateTMap(m, t);
  }
}

//Find the euclidean distance between two points
double euclDistance(Point a, Point b) {
  return sqrt( pow(a.x - b.x, 2) + pow(a.y - b.y, 2.0));
}

void addAsDelaunay(Tri& t, vector<Tri>& tris, multimap<Point, Tri>& m) {
  //In order to guarantee Delaunay triangles the circumcircles must
  //be checked to make sure they do not overlap more than three points.
  //This is done by checking two triangles that share an edge and seeing
  //if their opposite angles sum to greater than 180 degrees.

  //Find the lengths of the triangle's edges
  double edgeAB = euclDistance(t.a, t.b);
  double edgeBC = euclDistance(t.b, t.c);
  double edgeCA = euclDistance(t.c, t.a);
  //Find the longest edge to find the largest angle
  Point wide, shared1, shared2;
  double angle;
  if ( edgeAB > edgeBC and edgeAB > edgeCA) {
    wide = t.c;
    shared1 = t.a;
    shared2 = t.b;
    angle = M_PI - asin(edgeBC/edgeAB) - asin(edgeCA/edgeAB);
  }
  else if (edgeBC > edgeCA) {
    wide = t.a;
    shared1 = t.b;
    shared2 = t.c;
    angle = M_PI - asin(edgeAB/edgeBC) - asin(edgeCA/edgeBC);
  }
  else {
    wide = t.b;
    shared1 = t.a;
    shared2 = t.c;
    angle = M_PI - asin(edgeAB/edgeCA) - asin(edgeBC/edgeCA);
  }
  //Check if the angle is greater than 90 degrees (pi/2)
  //If it is then we need to flip the triangle.
  if ( M_PI/2.0 < angle) {
    //Find the opposite triangle
    auto range  = m.equal_range(shared1);
    auto range2 = m.equal_range(shared2);
    auto mirror_tri_i = find_first_of(range.first, range.second, range2.first, range2.second,
        [](pair<Point, Tri> tri1, pair<Point, Tri> tri2){ return tri1.second == tri2.second;});
    const Tri& mirror_tri = mirror_tri_i->second;
    //Make sure an opposite triangle was found.
    if (range.second != mirror_tri_i) {
      Point other;
      if (mirror_tri.a != shared1 and mirror_tri.a != shared2) { other = mirror_tri.a;}
      if (mirror_tri.b != shared1 and mirror_tri.b != shared2) { other = mirror_tri.b;}
      if (mirror_tri.c != shared1 and mirror_tri.c != shared2) { other = mirror_tri.c;}

      //Make two new triangles.
      Tri t1{wide, shared1, other};
      Tri t2{wide, shared2, other};

      //First remove the mirror triangle
      auto mirror_iter = find(tris.begin(), tris.end(), mirror_tri);
      removeFromTMap(m, mirror_tri);
      tris.erase(mirror_iter);

      //Add the new triangles
      addTriangle(t1, tris, m);
      addTriangle(t2, tris, m);
      return;
    }
  }
  //Otherwise the given triangle satisfies deleanay requirements and can be stored
  //or there was no opposite triangle to flip with.
  addTriangle(t, tris, m);
}

vector<Tri> genTriangles(double max_x, double max_y, vector<Point> vertices) {
  //Make sure that none of the samples are out of range
  for_each(vertices.begin(), vertices.end(),
      [=](const Point& p){ if (p.x > max_x or p.y > max_y) {
                                 ostringstream is;
                                 is << "In genTriangles vertex "<<p.x<<","<<p.y<<" is out of map area "<<max_x<<","<<max_y<<".";
                                 throw runtime_error(is.str());}});
  //The points (0,0), (0, max_y), (max_x, max_y), and (max_x, 0) are always
  //used so they can be removed from the vertices vector.
  vertices.erase( remove_if(vertices.begin(), vertices.end(),
                             [&](Point p) { return (0 == p.x or max_x == p.x) and
                                                   (0 == p.y or max_y == p.y);}),
                  vertices.end());
  
  //Keep track of the triangles the vertices belong to so that we can easily
  //flip triangles to satisfy delaunay triangle conditions.
  multimap<Point, Tri> intriangles;

  //Make the initial set of triangles out of the four corners.
  vector<Tri> tris{ Tri{Point{0, 0}, Point{max_x, 0}, Point{0, max_y}},
                    Tri{Point{0, max_y}, Point{max_x, max_y}, Point{max_x, 0}}};

  for_each(tris.begin(), tris.end(), [&](const Tri& t){ updateTMap(intriangles, t);});

  //Now iteratively add every single point into the list of triangles.
  //Since we need to search for the containing triangle in each case this runs
  //in n squared.
  //If we marked tiles it would depend upon the tile resolution but would
  //probably be n*log(tilespace)
  for (auto V = vertices.begin(); V != vertices.end(); ++V) {
    auto t = findContainingTriangle(*V, tris);
    if (t != tris.end()) {
      Point a = t->a;
      Point b = t->b;
      Point c = t->c;
      //Don't add a point that is already in the map
      if (a != *V and b != *V and c != *V) {
        //Three new triangles are created after adding every point so remove
        //this triangle and replace it with two triangles.
        Tri t1{a, b, *V};
        Tri t2{a, c, *V};
        Tri t3{b, c, *V};
        //Remove the old triangle
        removeFromTMap(intriangles, *t);
        tris.erase(t);

        //Add the new triangles.
        addAsDelaunay(t1, tris, intriangles);
        addAsDelaunay(t2, tris, intriangles);
        addAsDelaunay(t3, tris, intriangles);
      }
    }
    //FIXME Every point should be inside of a triangle if the four corners of
    //the map were included in the mesh. If a triangle could not be found
    //an error probably occured.
  }
  return tris;
}
