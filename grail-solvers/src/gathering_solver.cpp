#include <iostream>
#include <map>
#include <string>
#include <time.h>
#include <vector>
#include <algorithm>
#include <set>

#include "world_model_protocol.hpp"
#include "grailV3_solver_client.hpp"
#include "sample_data.hpp"
#include "netbuffer.hpp"

#include "grail_types.hpp"
#include "world_model_protocol.hpp"
#include "grailV3_solver_client.hpp"
#include "netbuffer.hpp"

using namespace world_model;

std::u16string toU16(const std::string& str) {
  return std::u16string(str.begin(), str.end());
}

std::string toString(const std::u16string& str) {
  return std::string(str.begin(), str.end());
}

struct Gathering {
  std::u16string name;
  //Where is this gathering?
  std::u16string region;
  double xmin;
  double ymin;
  double xmax;
  double ymax;
  //How many things must gather for this event to start?
  uint32_t threshold;
  //Object of interest - what type of things gather for this event?
  std::u16string ooi;
  std::set<URI> gathered;
};

bool find_xoff(Attribute& a) {
  return a.name.find(u"location.xoffset") != std::u16string::npos;
}

bool find_yoff(Attribute& a) {
  return a.name.find(u"location.yoffset") != std::u16string::npos;
}

//If the status of the given gathering has changed then send new solutions
//to the world model.
void updateGatheringStatus(Gathering& gath, SolverWorldModel& swm,
    ClientWorldModel::world_state& ws) {
  //std::cerr<<"Processing update for "<<toString(gath.name)<<'\n';
  //Check for objects of interest moving into and out of the gathering area
  std::vector<SolverWorldModel::Solution> solns;
  for (auto I = ws.begin(); I != ws.end(); ++I) {
    std::cerr<<"Checking object "<<toString(I->first)<<'\n';
    //Get the x and y offsets of the object
    auto x_attrib = std::find_if(I->second.begin(), I->second.end(), find_xoff);
    auto y_attrib = std::find_if(I->second.begin(), I->second.end(), find_yoff);
    if (x_attrib != I->second.end() and y_attrib != I->second.end()) {
      double xoffset = readPrimitive<double>(x_attrib->data);
      double yoffset = readPrimitive<double>(y_attrib->data);
      //std::cerr<<"\tX and Y offsets are "<<xoffset<<" and "<<yoffset<<'\n';
      //std::cerr<<"\tComparing to box formed by ("<<gath.xmin<<", "<<gath.ymin<<") and ("<<gath.xmax<<", "<<gath.ymax<<")\n";
      bool in_area = (xoffset >= gath.xmin and xoffset <= gath.xmax and
          yoffset >= gath.ymin and yoffset <= gath.ymax);
      //Check for a change in state.
      if (in_area and gath.gathered.count(I->first) == 0) {
        std::cerr<<toString(I->first)<<" is in area for "<<toString(gath.name)<<"\n";
        gath.gathered.insert(I->first);
        if (gath.gathered.size() == gath.threshold) {
          //Send a message indicating that this gathering has started.
          SolverWorldModel::Solution soln{u"happening", world_model::getGRAILTime(),
            gath.region+u".gathering."+gath.name, std::vector<uint8_t>{1}};
          solns.push_back(soln);
          //Also indicate that all oois in the
          //gathering are now gathered.
          for (auto OOI = gath.gathered.begin(); OOI != gath.gathered.end(); ++OOI) {
            //Send a message indicating that this gathering has ended
            SolverWorldModel::Solution ooi_soln{u"attending "+gath.name, world_model::getGRAILTime(), *OOI, std::vector<uint8_t>{1}};
            solns.push_back(ooi_soln);
          }
        }
        else if (gath.gathered.size() > gath.threshold) {
          //Indicate that this ooi has joined the gathering.
          SolverWorldModel::Solution soln{u"attending "+gath.name, world_model::getGRAILTime(), I->first, std::vector<uint8_t>{1}};
          solns.push_back(soln);
        }
      }
      if (not in_area and gath.gathered.count(I->first) == 1) {
        if (gath.gathered.size() == gath.threshold) {
          //Send a message indicating that this gathering has ended
          SolverWorldModel::Solution soln{u"happening", world_model::getGRAILTime(),
            gath.region+u".gathering."+gath.name, std::vector<uint8_t>{0}};
          solns.push_back(soln);
          //Also indicate that all oois in the gathering are no
          //longer gathered
          for (auto OOI = gath.gathered.begin(); OOI != gath.gathered.end(); ++OOI) {
            //Send a message indicating that this gathering has ended
            SolverWorldModel::Solution ooi_soln{u"attending "+gath.name, world_model::getGRAILTime(), *OOI, std::vector<uint8_t>{0}};
            solns.push_back(ooi_soln);
          }
        }
        //Indicate that this ooi is no longer gathered
        else if (gath.gathered.size() > gath.threshold) {
          //Send a message indicating that this gathering has ended
          SolverWorldModel::Solution soln{u"attending "+gath.name, world_model::getGRAILTime(), I->first, std::vector<uint8_t>{0}};
          solns.push_back(soln);
        }
        gath.gathered.erase(I->first);
      }
    }
  }
  //Send updates if there were any.
  if (not solns.empty()) {
    //Send the data to the world model
    swm.sendData(solns, false);
  }
}

int main(int ac, char** av) {
  if (4 != ac) {
    std::cerr<<"This program needs 3 arguments:\n";
    std::cerr<<"\t"<<av[0]<<" <world model ip> <solver port> <client port>\n\n";
    return 0;
  }
  std::string wm_ip(av[1]);
  int solver_port = std::stoi(std::string((av[2])));
  int client_port = std::stoi(std::string((av[3])));

  //Set up the solver world model connection;
  std::u16string origin = u"grail/gathering_solver\nversion 1.0";

  //Start with a gathering happening/not happening type.
  std::vector<std::pair<u16string, bool>> type_pairs{{u"happening", false}};
  SolverWorldModel swm(wm_ip, solver_port, type_pairs, origin);
  if (not swm.connected()) {
    std::cerr<<"Could not connect to the world model as a solver - aborting.\n";
    return 0;
  }

  ClientWorldModel cwm(wm_ip, client_port);
  if (not cwm.connected()) {
    std::cerr<<"Could not connect to the world model as a client - aborting.\n";
    return 0;
  }

  //Get URIs of type "gathering" and find out
  //any URIs with attached sensors.
  //Use average variance to detect mobility and link median values to localize
  uint32_t cur_ticket = 0;
  {
    URI gatherings = u".*\\.gathering\\..*";
    std::vector<URI> link_attributes{u"location.xmin", u"location.ymin",
      u"location.xmax", u"location.ymax", 
      u"threshold", u"objects of interest.uri"};
    //We don't expect new gathering's to be added too quickly so ask for a slow update on this.
    grail_time interval = 30000;
    cwm.setupSynchronousDataStream(gatherings, link_attributes, interval, ++cur_ticket);
  }

  //Define some functions to make finding attributes simple
  auto find_xmin = [&](Attribute& a) { return a.name.find(u"location.xmin") != std::u16string::npos;};
  auto find_xmax = [&](Attribute& a) { return a.name.find(u"location.xmax") != std::u16string::npos;};
  auto find_ymin = [&](Attribute& a) { return a.name.find(u"location.ymin") != std::u16string::npos;};
  auto find_ymax = [&](Attribute& a) { return a.name.find(u"location.ymax") != std::u16string::npos;};
  auto find_threshold = [&](Attribute& a) { return a.name.find(u"threshold") != std::u16string::npos;};
  auto find_interest = [&](Attribute& a) { return a.name.find(u"objects of interest.uri") != std::u16string::npos;};

  //Remember the gatherings here so that they can be
  //modified without having to alter the callbacks
  //given to the grailV3 interface.
  std::map<std::u16string, Gathering> known_gatherings;

  std::cerr<<"Starting loop...\n";
  while (1) {
    //Get world model updates
    ClientWorldModel::world_state ws;
    uint32_t ticket;
    std::tie(ws, ticket) = cwm.getSynchronousStreamUpdate();
    std::vector<SolverWorldModel::Solution> solns;
    for (auto I = ws.begin(); I != ws.end(); ++I) {
      //If this is a new or updated gathering type then solution types need to be created
      std::u16string::size_type gathering_index = I->first.find(u".gathering.");
      if (gathering_index != std::u16string::npos) {
        //Find out everything about this gathering
        auto xminattrib = std::find_if(I->second.begin(), I->second.end(), find_xmin);
        auto xmaxattrib = std::find_if(I->second.begin(), I->second.end(), find_xmax);
        auto yminattrib = std::find_if(I->second.begin(), I->second.end(), find_ymin);
        auto ymaxattrib = std::find_if(I->second.begin(), I->second.end(), find_ymax);
        auto threshold_attrib = std::find_if(I->second.begin(), I->second.end(), find_threshold);
        auto interest_attrib = std::find_if(I->second.begin(), I->second.end(), find_interest);
        std::u16string region = I->first.substr(0, gathering_index);
        //See if we can actually use this gathering
        if (xminattrib != I->second.end() and xmaxattrib != I->second.end() and
            yminattrib != I->second.end() and ymaxattrib != I->second.end() and
            threshold_attrib != I->second.end() and interest_attrib != I->second.end()) {
          //Parse out all of this gathering's information
          std::u16string region = I->first.substr(0, gathering_index);
          std::u16string gathering_name = I->first.substr(0 + gathering_index + std::string(".gathering.").size());
          std::cerr<<"Found a gathering named "<<toString(gathering_name)<<" in region "<<toString(region)<<'\n';
          //Read in gathering values
          double xmin = readPrimitive<double>(xminattrib->data);
          double ymin = readPrimitive<double>(yminattrib->data);
          double xmax = readPrimitive<double>(xmaxattrib->data);
          double ymax = readPrimitive<double>(ymaxattrib->data);
          uint32_t threshold = readPrimitive<uint32_t>(threshold_attrib->data);
          std::u16string ooi = readUTF16(interest_attrib->data, 0, interest_attrib->data.size());
          if (known_gatherings.end() == known_gatherings.find(gathering_name)) {
            //Make a new solution for attending this gathering
            std::vector<std::pair<u16string, bool>> ooi_pair{{u"attending "+gathering_name, false}};
            swm.addTypes(ooi_pair);
            //Make the new gathering
            Gathering& gath = known_gatherings[gathering_name] = Gathering{gathering_name, region, xmin, ymin, xmax, ymax, threshold, ooi, {}};
            //Add a new request for this object of interest
            //Request "region<anything>.<ooi>.<anything>
            URI ooi_request = region + u".*\\." + ooi + u"\\..*";
            std::vector<URI> link_attributes{u"location.xoffset", u"location.yoffset"};
            std::cerr<<"Giving request for objects of interest: "<<toString(ooi_request)<<'\n';
            //Five second updates
            grail_time interval = 5000;
            //Use references so that an update to the gathering is automatically
            //reflected in the callback.
            cwm.setupSynchronousDataStream(ooi_request, link_attributes, interval,
                [&](ClientWorldModel::world_state& ws, uint32_t ticket){updateGatheringStatus(std::ref(gath), std::ref(swm), ws);}, ++cur_ticket);
          }
          else {
            //Just updating a known gathering
            //TODO FIXME Update the gathering - should remove everything from
            //the gathering, cancel the old request, and issue a new one because
            //objects of interest may have changed
          }
        }
      }
    }
  }
  return 0;
}

