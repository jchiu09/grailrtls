#include <vector>
#include <iostream>
#include <numeric>
#include <algorithm>
#include <functional>

#include <multidimensional_slicing.hpp>

#include "rand.h"

#define INSIDE 1
#define OUTSIDE 2

using namespace md_slicing;

//TODO FIXME Get rid of these global variables
int md_rejections = 0, md_evaluate_energy = 0;
const double b0_mean = 0.0;
const double b1_mean = 0.0;
const double b0_precision = 0.000001;        // 10^-6
const double b1_precision = 0.000001;        // 10^-6

mc_dynamic_state::mc_dynamic_state(int MAX_APS, int predictNo, int total_network_variables, vector<double> domains) {
  first_b0i_index = 0;
  first_b1i_index = first_b0i_index + MAX_APS;
  first_taui_index = first_b0i_index + MAX_APS * 2;
  dim = total_network_variables;
  q    = std::vector<double>(3*MAX_APS);
  position_estimates = std::vector<double>(domains.size()*predictNo);
  this->domains = domains;

  std::vector<double>::iterator qbegin = q.begin();
  //std::fill(qbegin, qbegin+last_bprior_index, 0.0);
  //std::fill(qbegin+last_bprior_index, qbegin+last_taubprior_index, 0.01);
  bprior0 = 0.0;
  bprior1 = 0.0;
  tauprior0 = 0.01;
  tauprior1 = 0.01;
  std::fill(qbegin+first_b0i_index, qbegin+first_taui_index, 0.0);
  std::fill(qbegin+first_taui_index, q.end(), 0.1);

  //Initialize with random location inside the localization area
  for (size_t i = 0; i < domains.size(); ++i) {
    for (size_t j = 0; j < predictNo; ++j) {
      position_estimates[j + predictNo*i] = rand_uniopen() * domains[i];
    }
  }
}

data_specifications::data_specifications(
  int N_inputs,
  int MAX_APS,
  int N_cases,
  const std::vector<std::vector<double>>& train_coordinates,
  const double_matrix& train_signal,
  int predictNo,
  int observations_nopredict,
  const std::vector<std::vector<double>>& AP_coordinates
)
{
  this->predictNo = predictNo;
  this->observations_nopredict = observations_nopredict;
  this->N_inputs = N_inputs;
  this->MAX_APS = MAX_APS;
  this->N_cases = N_cases;
  this->train_signal = train_signal;
  this->D = double_matrix(MAX_APS, std::vector<double>(N_cases, 0.0));
  this->Dsqrd = double_matrix(MAX_APS, std::vector<double>(N_cases, 0.0));
  this->AP_coordinates = AP_coordinates;

  for (int i = 0; i < MAX_APS; i++) {
    for (int j = 0; j < observations_nopredict; j++) {
      //Count of the sum of the squares of the differencnes
      double sum_squares = 0.0;
      for (size_t coord = 0; coord < train_coordinates[j].size(); ++coord) {
        double val = train_coordinates[j][coord];
        double square_diff = pow(val - AP_coordinates[i][coord], 2.0);
        sum_squares += square_diff;
      }

      this->D[i][j] = log(1+sqrt(sum_squares));
      this->Dsqrd[i][j] = D[i][j] * D[i][j];
    }
  }
}

void printDS(mc_dynamic_state& ds) {
  std::cerr<<"Beta values are ";
  for (auto I = ds.q.begin(); I != ds.q.end(); ++I) {
    std::cerr<<*I<<'\t';
  }
  std::cerr<<'\n';
  std::cerr<<"Position values are ";
  for (auto I = ds.position_estimates.begin(); I != ds.position_estimates.end(); ++I) {
    std::cerr<<*I<<'\t';
  }
  std::cerr<<'\n';
}

double conjugateNormalB0i_PosteriorMeanSum_1
( mc_dynamic_state& ds, /* Structure holding pointers to dynamical state */
  const data_specifications& data_spec,
  int target_offset,
  const std::vector<double>& fixed_coords,
  double b,
  double precision,
  int& cntminus1
) 
{
  int i;
  double D=0.0, m=0.0;
  
  int N_cases = data_spec.N_cases;

  for (i = 0; i < data_spec.observations_nopredict; i++) {
    if (isnan(data_spec.train_signal[target_offset][i])) {
      cntminus1++;
      continue;
    }

    D = data_spec.D[target_offset][i];    
    m += (data_spec.train_signal[target_offset][i] - b * D);
  }

  for (i = data_spec.observations_nopredict; i < N_cases; i++) {
    if (isnan(data_spec.train_signal[target_offset][i])) {
      cntminus1++;
      continue;
    }

    double sum_squares = 0.0;
    for (size_t dimension = 0; dimension < fixed_coords.size(); ++dimension) {
      size_t position_index = i - data_spec.observations_nopredict + dimension*data_spec.predictNo;
      sum_squares += pow(ds.position_estimates[position_index] - fixed_coords[dimension], 2.0);
    }
    
    D = log(1+sqrt(sum_squares));
    m += (data_spec.train_signal[target_offset][i] - b * D);
  }

  m *= precision;

  return m;
}

/* EVALUATE POTENTIAL ENERGY */
void mc_app_energy_beta
( mc_dynamic_state& ds,	/* Current dyanamical state */
  const data_specifications& data_spec,  
  double& energy,	/* Place to store energy, null if not required */
  int k                 /* Index of coordinate being updated */
) {  
  double e = 0.0;

  if ((k >= ds.first_b0i_index) && (k < ds.first_b1i_index)) {              // This is for b0i
    int N_cases = data_spec.N_cases;
    double A, B;
    double prior_precision = ds.tauprior0;
    double prior_mean = ds.bprior0;
    int aps_index = k - ds.first_b0i_index;
    int b1_index = k + data_spec.MAX_APS;
    int tau_index = k + 2*data_spec.MAX_APS;
    int cntminus1 = 0;

    A = prior_mean*prior_precision;
    
    A += conjugateNormalB0i_PosteriorMeanSum_1(ds, data_spec, aps_index,
        data_spec.AP_coordinates[aps_index], ds.q[b1_index], ds.q[tau_index], cntminus1);
    
    B = prior_precision + (N_cases-cntminus1)* ds.q[tau_index];

    double post_mean = A / B;
    double post_variance = 1 / B;
    
    e += normal_dist(ds.q[k], post_mean, post_variance);
    energy = e;
    return;
  }
  
  fprintf(stderr, "The index (k=%d) given to mc_app_energy_beta was invalid.\n", k);
  fprintf(stdout, "The index (k=%d) given to mc_app_energy_beta was invalid.\n", k);
  return;
}

void mc_app_energy_coordinate
( mc_dynamic_state& ds,	/* Current dyanamical state */
  const data_specifications& data_spec,  
  double& energy,	/* Place to store energy, null if not required */
  int k                 /* Index of coordinate being updated */
) {  
  double e = 0.0;
  double D, m;
  int target_index;
  int i;

  //k could be the index of any coordinate but we need to know the first index.
  size_t first_dimension_idx = k % data_spec.predictNo;
  
  target_index = data_spec.observations_nopredict + first_dimension_idx;

  for (i = 0; i < data_spec.MAX_APS; i++) {
    if (isnan(data_spec.train_signal.at(i).at(target_index))) {
      continue;
    }

    const std::vector<double>& fixed_coords = data_spec.AP_coordinates[i];
    double sum_squares = 0.0;
    for (size_t dimension = 0; dimension < fixed_coords.size(); ++dimension) {
      size_t position_index = first_dimension_idx + dimension*data_spec.predictNo;
      sum_squares += pow(ds.position_estimates.at(position_index) - fixed_coords[dimension], 2.0);
    }
    
    D = log(1+sqrt(sum_squares));
    m = ds.q[i + ds.first_b0i_index] + (ds.q[i + ds.first_b1i_index] * D);
    e += normal_dist(data_spec.train_signal.at(i).at(target_index), m, 1/ds.q[i+ds.first_taui_index]); // Likelihood: This is for Si
  }

  energy = e;
}

/* FIND INTERVAL AROUND PART OF SLICE BY STEPPING OUT. */
void step_out_coordinate
( mc_dynamic_state& ds,	/* Current state */
  const data_specifications& data_spec,  
  int k,		/* Index of coordinate being updated */
  double slice_point,	/* Potential energy level that defines slice */
  double curr_q,	/* Current value for coordinate */
  int max_steps,	/* Maximum number of intervals, zero for unlimited */
  double& low_bnd,	/* Place to store low bound for interval */
  double& high_bnd,	/* Place to store high bound for interval */
  double width,          /* Estimate of the slice */
  double lower, 
  double higher,
  int bounded
)
{
  int low_steps, high_steps;

  low_steps  = max_steps==0 ? 1000000000 
             : max_steps==1 ? 0
             : rand_int(max_steps);
  high_steps = max_steps==0 ? 1000000000 
             : (max_steps-1) - low_steps;

  low_bnd  = curr_q - rand_uniopen() * width;
  high_bnd = low_bnd + width;

  if ( bounded && (low_bnd < lower) ) {
    low_bnd = lower;
  }
  else {
    while (low_steps>0) {
      ds.position_estimates[k] = low_bnd;
      mc_app_energy_coordinate(ds, data_spec, ds.pot_energy, k);    
      
      if (ds.pot_energy>slice_point) break;
      
      low_bnd -= width;
      low_steps -= 1;
      
      if (bounded && (low_bnd < lower)) {
        low_bnd = lower;
        break;
      }
    }
  }

  if (bounded && (high_bnd > higher)) {
    high_bnd = higher;
  }
  else {
    while (high_steps>0) {
      ds.position_estimates[k] = high_bnd;
      mc_app_energy_coordinate(ds, data_spec, ds.pot_energy, k);    
      
      if (ds.pot_energy>slice_point) break;
      
      high_bnd += width;
      high_steps -= 1;

      if (bounded && (high_bnd > higher)) {
        high_bnd = higher;
        break;
      }
    }
  }
}

/* FIND INTERVAL AROUND PART OF SLICE BY STEPPING OUT. */
void step_out 
( mc_dynamic_state& ds,	/* Current state */
  const data_specifications& data_spec,  
  int k,		/* Index of coordinate being updated */
  double slice_point,	/* Potential energy level that defines slice */
  double curr_q,	/* Current value for coordinate */
  int max_steps,	/* Maximum number of intervals, zero for unlimited */
  double& low_bnd,	/* Place to store low bound for interval */
  double& high_bnd,	/* Place to store high bound for interval */
  double width,          /* Estimate of the slice */
  double lower, 
  double higher,
  int bounded
)
{
  fprintf(stdout, "Stepping out with width, lower, higher, and bounded: %g, %g, %g, %i!", width, lower, higher, bounded);
  int low_steps, high_steps;

  low_steps  = max_steps==0 ? 1000000000 
             : max_steps==1 ? 0
             : rand_int(max_steps);
  high_steps = max_steps==0 ? 1000000000 
             : (max_steps-1) - low_steps;

  low_bnd  = curr_q - rand_uniopen() * width;
  high_bnd = low_bnd + width;

  if ( bounded && (low_bnd < lower) ) {
    low_bnd = lower;
  }
  else {
    while (low_steps>0) {
      ds.q[k] = low_bnd;

      if ((k >= ds.first_b0i_index) && (k < ds.first_b1i_index)) {
        mc_app_energy_beta(ds, data_spec, ds.pot_energy, k);    
      }
      else {
        mc_app_energy_coordinate(ds, data_spec, ds.pot_energy, k);    
      }
      
      if (ds.pot_energy>slice_point) break;
      
      low_bnd -= width;
      low_steps -= 1;
      
      if (bounded && (low_bnd < lower)) {
        low_bnd = lower;
        break;
      }
    }
  }

  if (bounded && (high_bnd > higher)) {
    high_bnd = higher;
  }
  else {
    while (high_steps>0) {
      ds.q[k] = high_bnd;

      if ((k >= ds.first_b0i_index) && (k < ds.first_b1i_index)) {
        mc_app_energy_beta(ds, data_spec, ds.pot_energy, k);    
      }
      else {
        mc_app_energy_coordinate(ds, data_spec, ds.pot_energy, k);    
      }
      
      if (ds.pot_energy>slice_point) break;
      
      high_bnd += width;
      high_steps -= 1;

      if (bounded && (high_bnd > higher)) {
        high_bnd = higher;
        break;
      }
    }
  }
}

/* CHECK THAT DOUBLING WOULD FIND SAME INTERVAL FROM NEW POINT.  Returns 1
   if it would, 0 if it would not. */
int dbl_ok_coordinate
( mc_dynamic_state& ds,	/* Current state */
  const data_specifications& data_spec, 
  int k,		/* Index of coordinate being updated */
  double slice_point,	/* Potential energy level that defines slice */
  double curr_q,	/* Current value for coordinate */
  double low_bnd,	/* Low bound for interval */
  double high_bnd,	/* High bound for interval */
  double width          /* Estimate of the slice */
)
{
  double mid;
  double new_q;
  int diff;

  new_q = ds.position_estimates[k];
  diff = 0;

  while (high_bnd-low_bnd>1.1*width)
  { mid = (high_bnd+low_bnd)/2;
    if (!diff) 
    { diff = (curr_q<mid) != (new_q<mid);
    }
    if (diff)
    { ds.position_estimates[k] = mid;
      mc_app_energy_coordinate(ds, data_spec, ds.pot_energy, k);    
    }
    if (new_q<mid)
    { high_bnd = mid;
      ds.position_estimates[k] = low_bnd;
    }
    else
    { low_bnd = mid;
      ds.position_estimates[k] = high_bnd;
    }
    if (diff && ds.pot_energy>slice_point)
    { 
      mc_app_energy_coordinate(ds, data_spec, ds.pot_energy, k);    
      if (ds.pot_energy>slice_point)
      { ds.position_estimates[k] = new_q;
        return 0;
      }
    }
  }

  ds.position_estimates[k] = new_q;
  return 1;
}

/* CHECK THAT DOUBLING WOULD FIND SAME INTERVAL FROM NEW POINT.  Returns 1
   if it would, 0 if it would not. */
int dbl_ok
( mc_dynamic_state& ds,	/* Current state */
  const data_specifications& data_spec, 
  int k,		/* Index of coordinate being updated */
  double slice_point,	/* Potential energy level that defines slice */
  double curr_q,	/* Current value for coordinate */
  double low_bnd,	/* Low bound for interval */
  double high_bnd,	/* High bound for interval */
  double width          /* Estimate of the slice */
)
{
  double mid;
  double new_q;
  int diff;

  new_q = ds.q[k];
  diff = 0;

  while (high_bnd-low_bnd>1.1*width)
  { mid = (high_bnd+low_bnd)/2;
    if (!diff) 
    { diff = (curr_q<mid) != (new_q<mid);
    }
    if (diff)
    { ds.q[k] = mid;
      if ((k >= ds.first_b0i_index) && (k < ds.first_b1i_index)) {
        mc_app_energy_beta(ds, data_spec, ds.pot_energy, k);    
      }
      else {
        mc_app_energy_coordinate(ds, data_spec, ds.pot_energy, k);    
      }
    }
    if (new_q<mid)
    { high_bnd = mid;
      ds.q[k] = low_bnd;
    }
    else
    { low_bnd = mid;
      ds.q[k] = high_bnd;
    }
    if (diff && ds.pot_energy>slice_point)
    { 
      if ((k >= ds.first_b0i_index) && (k < ds.first_b1i_index)) {
        mc_app_energy_beta(ds, data_spec, ds.pot_energy, k);    
      }
      else {
        mc_app_energy_coordinate(ds, data_spec, ds.pot_energy, k);    
      }
      if (ds.pot_energy>slice_point)
      { ds.q[k] = new_q;
        return 0;
      }
    }
  }

  ds.q[k] = new_q;
  return 1;
}

/* PICK VALUE FROM INTERVAL AROUND PART OF THE SLICE. */
void pick_value_coordinate
( mc_dynamic_state& ds,		/* Current state, updated with new value */
  data_specifications& data_spec,  
  int k,			/* Index of coordinate being updated */
  double slice_point,		/* Potential energy level that defines slice */
  double curr_q,		/* Current value for coordinate */
  int max_steps,		/* Negative if interval was found by doubling */
  double low_bnd,		/* Low bound for interval */
  double high_bnd,		/* High bound for interval */
  int s_factor,			/* Factor for faster shrinkage */
  double s_threshold,		/* Threshold for faster shrinkage */
  double width,                 /* Estimate of slice */
  int& rejections
)
{
  double nlow_bnd, nhigh_bnd, range;

  nlow_bnd = low_bnd;
  nhigh_bnd = high_bnd;

  for (;;) {
    ds.position_estimates[k] = nlow_bnd + rand_uniopen() * (nhigh_bnd-nlow_bnd);

    mc_app_energy_coordinate(ds, data_spec, ds.pot_energy, k);    
    
    if (ds.pot_energy<=slice_point && (max_steps>=0 
					|| dbl_ok_coordinate(ds, data_spec, k, slice_point, curr_q, low_bnd, high_bnd, width)))
    { 
      return;
    }

    rejections++;
    
    if (s_factor>=0)
    { 
      if (ds.position_estimates[k]<curr_q) 
      { nlow_bnd = ds.position_estimates[k];
      }
      else
      { nhigh_bnd = ds.position_estimates[k];
      }
    }

    if ((s_factor<-1 || s_factor>1) && ds.pot_energy>slice_point+s_threshold)
    { 
      range = (nhigh_bnd-nlow_bnd) / (s_factor>0 ? s_factor : -s_factor);
      if (range<=0) {
        std::cerr<<"Utter failure in pick_value_coordinate\n";
        abort();
      }
      while (nlow_bnd+range<curr_q) 
      { nlow_bnd += range;
      }
      while (nhigh_bnd-range>curr_q) 
      { nhigh_bnd -= range;
      }
    } 
  }
}

/* PICK VALUE FROM INTERVAL AROUND PART OF THE SLICE. */
void pick_value
( mc_dynamic_state& ds,		/* Current state, updated with new value */
  data_specifications& data_spec,  
  int k,			/* Index of coordinate being updated */
  double slice_point,		/* Potential energy level that defines slice */
  double curr_q,		/* Current value for coordinate */
  int max_steps,		/* Negative if interval was found by doubling */
  double low_bnd,		/* Low bound for interval */
  double high_bnd,		/* High bound for interval */
  int s_factor,			/* Factor for faster shrinkage */
  double s_threshold,		/* Threshold for faster shrinkage */
  double width,                 /* Estimate of slice */
  int& rejections
)
{
  double nlow_bnd, nhigh_bnd, range;

  nlow_bnd = low_bnd;
  nhigh_bnd = high_bnd;

  for (;;)
    {
    ds.q[k] = nlow_bnd + rand_uniopen() * (nhigh_bnd-nlow_bnd);

    if ((k >= ds.first_b0i_index) && (k < ds.first_b1i_index)) {
      mc_app_energy_beta(ds, data_spec, ds.pot_energy, k);    
    }
    else {
      mc_app_energy_coordinate(ds, data_spec, ds.pot_energy, k);    
    }
    
    if (ds.pot_energy<=slice_point && (max_steps>=0 
					|| dbl_ok(ds, data_spec, k, slice_point, curr_q, low_bnd, high_bnd, width)))
    { 
      return;
    }

    rejections++;
    
    if (s_factor>=0)
    { 
      if (ds.q[k]<curr_q) 
      { nlow_bnd = ds.q[k];
      }
      else
      { nhigh_bnd = ds.q[k];
      }
    }

    if ((s_factor<-1 || s_factor>1) && ds.pot_energy>slice_point+s_threshold)
    { 
      range = (nhigh_bnd-nlow_bnd) / (s_factor>0 ? s_factor : -s_factor);
      if (range<=0) {
        std::cerr<<"Utter failure in pick_value\n";
        abort();
      }
      while (nlow_bnd+range<curr_q) 
      { nlow_bnd += range;
      }
      while (nhigh_bnd-range>curr_q) 
      { nhigh_bnd -= range;
      }
    } 
  }
}

void mc_slice_1_coordinate
( mc_dynamic_state& ds, /* Structure holding pointers to dynamical state */
  data_specifications& data_spec,  
  int s_factor,		/* Factor for faster shrinkage */
  double s_threshold,	/* Threshold for faster shrinkage */  
  int k,
  double width,
  int bounded, 
  double low, 
  double high,
  int& rejections)
{
  double curr_q, slice_point, low_bnd, high_bnd;
  
  mc_app_energy_coordinate (ds, data_spec, ds.pot_energy, k);
  
  double z = rand_exp();

  slice_point = ds.pot_energy + z;
  curr_q = ds.position_estimates[k];

  double lower = low, higher = high;

  step_out_coordinate (ds, data_spec, k, slice_point, curr_q, 0, low_bnd, high_bnd, 
      width, lower, higher, bounded);
  
  pick_value_coordinate (ds, data_spec, k, slice_point, curr_q, 0, low_bnd, high_bnd, 
	      s_factor, s_threshold, width, rejections);
}

void mc_slice_1_beta
( mc_dynamic_state& ds, /* Structure holding pointers to dynamical state */
  data_specifications& data_spec,  
  int s_factor,		/* Factor for faster shrinkage */
  double s_threshold,	/* Threshold for faster shrinkage */  
  int k,
  double width,
  int bounded, 
  double low, 
  double high,
  int& rejections)
{
  int max_steps = 10;	/* Maximum number of intervals, zero for unlimited; if negative, intervals are found by doubling */
  double curr_q, slice_point, low_bnd, high_bnd;
  
  mc_app_energy_beta (ds, data_spec, ds.pot_energy, k);
  
  double z = rand_exp();

  slice_point = ds.pot_energy + z;
  curr_q = ds.q[k];

  double lower = low, higher = high;

  step_out (ds, data_spec, k, slice_point, curr_q, max_steps, low_bnd, high_bnd, 
      width, lower, higher, bounded);
  
  pick_value (ds, data_spec, k, slice_point, curr_q, max_steps, low_bnd, high_bnd, 
	      s_factor, s_threshold, width, rejections);
}

void md_slicing::large_slice_1all
( mc_dynamic_state& ds, /* Structure holding pointers to dynamical state */
  data_specifications& data_spec,
  double width, 
  int& md_rejections
  ) {
  int k;  

  //Find new locations
  for (size_t i = ds.domains.size(); i < ds.domains.size(); ++i) {
    int s_factor = 0;	/* Factor for faster shrinkage */
    double s_threshold = 0;/* Threshold for faster shrinkage */  
    for (size_t k = 0; k < data_spec.predictNo; ++k) {
      mc_slice_1_coordinate(ds, data_spec, s_factor, s_threshold, k*i, width, 1, 0.0, ds.domains[i], md_rejections); 
    }
  }

  ///////////////////////////////////////////////////////////
  //double new_value = conjugateNormalB01(ds, data_spec, k);
  //This is conjugate Normal sampler for b0 and b1
  {
    //Calculate random gaussian values for b0 and b1
    {
      double A = 0.0, B = 0.0, sum = 0.0;
      int i;

      A = b0_mean * b0_precision;
      B = b0_precision + data_spec.MAX_APS * ds.tauprior0;

      for (i = 0; i < data_spec.MAX_APS; i++) {
        sum += ds.q[ds.first_b0i_index+i];
      }

      A += sum * ds.tauprior0;

      double post_mean = A / B;
      double post_variance = 1 / B;

      ds.bprior0 = random_gaussian(post_mean, post_variance);
    }
    {
      double A = 0.0, B = 0.0, sum = 0.0;
      int i;

      A = b1_mean * b1_precision;
      B = b1_precision + data_spec.MAX_APS * ds.tauprior1;

      for (i = 0; i < data_spec.MAX_APS; i++) {
        sum += ds.q[ds.first_b1i_index+i];
      }

      A += sum * ds.tauprior1;

      double post_mean = A / B;
      double post_variance = 1 / B;

      ds.bprior1 = random_gaussian(post_mean, post_variance);
    }
  }
  ///////////////////////////////////////////////////////////

  //double new_value = conjugateGammaTauB01(ds, data_spec, k);
  ///////////////////////////////////////////////////////////
  {
    double prior_shape = 0.001;
    double prior_mean = 0.001;

    double post_shape = prior_shape + (double)data_spec.MAX_APS/2;
    double post_mean = 0.0;
    double sum = 0.0, diff = 0.0;
    int i;

    for (i = 0; i < data_spec.MAX_APS; i++) {
      diff = ds.q[ds.first_b0i_index+i] - ds.bprior0;
      sum +=  diff * diff;
    }

    post_mean = sum/2 + prior_mean;
    ds.tauprior1 = rgamma(post_shape, 1/post_mean);

    sum = 0.0;
    diff = 0.0;
    for (i = 0; i < data_spec.MAX_APS; i++) {
      diff = ds.q[ds.first_b1i_index+i] - ds.bprior1;
      sum += diff * diff;
    }

    post_mean = sum/2 + prior_mean;
    ds.tauprior1 = rgamma(post_shape, 1/post_mean);
  }
  ///////////////////////////////////////////////////////////
  
  for (k = ds.first_b0i_index; k < ds.first_b1i_index; k++) {
    int s_factor = 0;	/* Factor for faster shrinkage */
    double s_threshold = 0;/* Threshold for faster shrinkage */  
    
    mc_slice_1_beta(ds, data_spec, s_factor, s_threshold, k, 1.0, 1, -1000000.0, 0.0, md_rejections);
  }

  for (k = ds.first_b1i_index; k < ds.first_taui_index; k++) {
    ///////////////////////////////////////////////////////////
    //double new_value = conjugateNormalB01i(ds, data_spec, k);
    //This is conjugate Normal sampler for b0i and b1i, where i = 1 to Number of APs
    {
      int N_cases = data_spec.N_cases;
      double A = 0.0;
      double B = 0.0;

      if ((k >= ds.first_b0i_index) && (k < ds.first_b1i_index)) {              // This is for b0i
        double prior_precision = ds.tauprior0;
        double prior_mean = ds.bprior0;
        int aps_index = k - ds.first_b0i_index;
        int b1_index = k + data_spec.MAX_APS;
        int tau_index = k + 2*data_spec.MAX_APS;
        int cntminus1 = 0;

        A = prior_mean*prior_precision;

        A += conjugateNormalB0i_PosteriorMeanSum_1(ds, data_spec, aps_index,
            data_spec.AP_coordinates[aps_index], ds.q[b1_index], ds.q[tau_index], cntminus1);

        B = prior_precision + (N_cases-cntminus1) * ds.q[tau_index];
      }
      else if ((k >= ds.first_b1i_index) && (k < ds.first_taui_index)) {        // This is for b1i
        double prior_precision = ds.tauprior1;
        double prior_mean = ds.bprior1;
        int aps_index = k - ds.first_b1i_index;
        int b0_index = k - data_spec.MAX_APS;
        int tau_index = k + data_spec.MAX_APS;
        double A_sum = 0.0;
        double B_sum = 0.0;

        B = prior_precision;
        A = prior_mean*prior_precision;

        //conjugateNormalB1i_PosteriorMeanSum_2(ds, data_spec, aps_index, data_spec.APS_X[aps_index], data_spec.APS_Y[aps_index], 
        //    ds.q[b0_index], ds.q[tau_index], A_sum, B_sum);
        ///////////////////////////////////////////////////////////
        {
          //Variables from function call
          int target_offset = aps_index;
          double b = ds.q[b0_index];
          double precision = ds.q[tau_index];
          double& nom_sum = std::ref(A_sum);
          double& denom_sum = std::ref(B_sum);

          int i;
          double D = 0.0, D2 = 0.0;

          int N_cases = data_spec.N_cases;

          std::vector<double>& fixed_coords = data_spec.AP_coordinates[aps_index];

          for (i = 0; i < data_spec.observations_nopredict; i++) {
            if (isnan(data_spec.train_signal[target_offset][i])) {
              continue;
            }

            D = data_spec.D[target_offset][i];
            D2 = data_spec.Dsqrd[target_offset][i];

            nom_sum += D * (data_spec.train_signal[target_offset][i] - b);
            denom_sum += D2;
          }

          for (i = data_spec.observations_nopredict; i < N_cases; i++) {
            if (isnan(data_spec.train_signal[target_offset][i])) {
              continue;
            }

            double sum_squares = 0.0;
            for (size_t dimension = 0; dimension < fixed_coords.size(); ++dimension) {
              size_t position_index = i - data_spec.observations_nopredict + dimension*data_spec.predictNo;
              sum_squares += pow(ds.position_estimates[position_index] - fixed_coords[dimension], 2.0);
            }

            D = log(1+sqrt(sum_squares));

            nom_sum += D * (data_spec.train_signal[target_offset][i] - b);
            denom_sum += D * D;
          }

          nom_sum *= precision;
          denom_sum *= precision;
        }
        ///////////////////////////////////////////////////////////

        A += A_sum;
        B += B_sum;
      }  

      double post_mean = A / B;
      double post_variance = 1 / B;

      ds.q[k] = random_gaussian(post_mean, post_variance);
    }
    ///////////////////////////////////////////////////////////
  }

  for (k = ds.first_taui_index; k < ds.q.size(); k++) {
    ///////////////////////////////////////////////////////////
    //double new_value = conjugateGammaTaui(ds, data_spec, k);
    //This is conjugate Gamma sampler for tau i 
    {
      int N_cases = data_spec.N_cases;
      double prior_shape = 0.001;
      double prior_mean = 0.001;
      int aps_index = k - ds.first_taui_index;
      int b0_index = k - 2*data_spec.MAX_APS;
      int b1_index = k - data_spec.MAX_APS;

      int cntminus1 = 0;
      int i;
      for (i = 0; i < N_cases; i++) {
        if (isnan(data_spec.train_signal[aps_index][i])) {
          cntminus1++;
        }
      }

      double post_shape = prior_shape + (double)(N_cases-cntminus1)/2;
      double post_mean = 0.0;
      double sum = 0.0;


      ///////////////////////////////////////////////////////////
      //sum = conjugateGammaTaui_PosteriorMeanSum(ds, data_spec, aps_index, data_spec.APS_X[aps_index], data_spec.APS_Y[aps_index], 
      //    ds.q[b0_index], ds.q[b1_index]);
      {
        //Variables from function call
        int target_offset = aps_index;
        double b0 = ds.q[b0_index];
        double b1 = ds.q[b1_index];

        int i;
        double D=0.0, m=0.0;

        int N_cases = data_spec.N_cases;

        std::vector<double>& fixed_coords = data_spec.AP_coordinates[aps_index];

        for (i = 0; i < data_spec.observations_nopredict ; i++) {
          if (isnan(data_spec.train_signal[target_offset][i])) {
            continue;
          }

          D = data_spec.D[target_offset][i];
          m = b0 + (b1 * D);

          // Likelihood: Si
          sum += pow(data_spec.train_signal[target_offset][i] - m, 2.0);
        }

        for (i = data_spec.observations_nopredict; i < N_cases; i++) {
          if (isnan(data_spec.train_signal[target_offset][i])) {
            continue;
          }

          double sum_squares = 0.0;
          for (size_t dimension = 0; dimension < fixed_coords.size(); ++dimension) {
            size_t position_index = i - data_spec.observations_nopredict + dimension*data_spec.predictNo;
            sum_squares += pow(ds.position_estimates[position_index] - fixed_coords[dimension], 2.0);
          }

          D = log(1+sqrt(sum_squares));
          m = b0 + (b1 * D);

          // Likelihood: Si
          sum += pow(data_spec.train_signal[target_offset][i] - m, 2.0);
        }
      }
      ///////////////////////////////////////////////////////////


      post_mean = sum/2 + prior_mean;

      ds.q[k] = rgamma(post_shape, 1/post_mean);
    }
    ///////////////////////////////////////////////////////////
  }
}

void large_updateNodes_slice_1
( mc_dynamic_state& ds, /* Structure holding pointers to dynamical state */
  data_specifications& data_spec,
  double width, 
  int& rejections
  ) {
  int k;  

  for (size_t i = 0; i < ds.domains.size(); ++i) {
    int s_factor = 0;	/* Factor for faster shrinkage */
    double s_threshold = 0;/* Threshold for faster shrinkage */  
    for (size_t k = 0; k < data_spec.predictNo; ++k) {
      mc_slice_1_coordinate(ds, data_spec, s_factor, s_threshold, k + (i*data_spec.predictNo), width, 1, 0.0, ds.domains[i], rejections); 
    }
  }

  ///////////////////////////////////////////////////////////
  //double new_value = conjugateNormalB01(ds, data_spec, k);
  //This is conjugate Normal sampler for b0 and b1
  {
    //Calculate random gaussian values for b0 and b1
    {
      double A = 0.0, B = 0.0, sum = 0.0;
      int i;

      A = b0_mean * b0_precision;
      B = b0_precision + data_spec.MAX_APS * ds.tauprior0;

      for (i = 0; i < data_spec.MAX_APS; i++) {
        sum += ds.q[ds.first_b0i_index+i];
      }

      A += sum * ds.tauprior0;

      double post_mean = A / B;
      double post_variance = 1 / B;

      ds.bprior0 = random_gaussian(post_mean, post_variance);
    }
    {
      double A = 0.0, B = 0.0, sum = 0.0;
      int i;

      A = b1_mean * b1_precision;
      B = b1_precision + data_spec.MAX_APS * ds.tauprior1;

      for (i = 0; i < data_spec.MAX_APS; i++) {
        sum += ds.q[ds.first_b1i_index+i];
      }

      A += sum * ds.tauprior1;

      double post_mean = A / B;
      double post_variance = 1 / B;

      ds.bprior1 = random_gaussian(post_mean, post_variance);
    }
  }
  ///////////////////////////////////////////////////////////

  //double new_value = conjugateGammaTauB01(ds, data_spec, k);
  ///////////////////////////////////////////////////////////
  {
    double prior_shape = 0.001;
    double prior_mean = 0.001;

    double post_shape = prior_shape + (double)data_spec.MAX_APS/2;
    double post_mean = 0.0;
    double sum = 0.0, diff = 0.0;
    int i;

    for (i = 0; i < data_spec.MAX_APS; i++) {
      diff = ds.q[ds.first_b0i_index+i] - ds.bprior0;
      sum +=  diff * diff;
    }

    post_mean = sum/2 + prior_mean;
    ds.tauprior0 = rgamma(post_shape, 1/post_mean);

    sum = 0.0;
    diff = 0.0;
    for (i = 0; i < data_spec.MAX_APS; i++) {
      diff = ds.q[ds.first_b1i_index+i] - ds.bprior1;
      sum += diff * diff;
    }

    post_mean = sum/2 + prior_mean;
    ds.tauprior1 = rgamma(post_shape, 1/post_mean);
  }
  ///////////////////////////////////////////////////////////

  for (k = ds.first_b0i_index; k < ds.first_taui_index; k++) {
    ///////////////////////////////////////////////////////////
    //double new_value = conjugateNormalB01i(ds, data_spec, k);
    //This is conjugate Normal sampler for b0i and b1i, where i = 1 to Number of APs
    {
      int N_cases = data_spec.N_cases;
      double A = 0.0;
      double B = 0.0;

      if ((k >= ds.first_b0i_index) && (k < ds.first_b1i_index)) {              // This is for b0i
        double prior_precision = ds.tauprior0;
        double prior_mean = ds.bprior0;
        int aps_index = k - ds.first_b0i_index;
        int b1_index = k + data_spec.MAX_APS;
        int tau_index = k + 2*data_spec.MAX_APS;
        int cntminus1 = 0;

        A = prior_mean*prior_precision;

        A += conjugateNormalB0i_PosteriorMeanSum_1(ds, data_spec, aps_index,
            data_spec.AP_coordinates[aps_index], ds.q[b1_index], ds.q[tau_index], cntminus1);

        B = prior_precision + (N_cases-cntminus1) * ds.q[tau_index];
      }
      else if ((k >= ds.first_b1i_index) && (k < ds.first_taui_index)) {        // This is for b1i
        double prior_precision = ds.tauprior1;
        double prior_mean = ds.bprior1;
        int aps_index = k - ds.first_b1i_index;
        int b0_index = k - data_spec.MAX_APS;
        int tau_index = k + data_spec.MAX_APS;
        double A_sum = 0.0;
        double B_sum = 0.0;

        B = prior_precision;
        A = prior_mean*prior_precision;

        //conjugateNormalB1i_PosteriorMeanSum_2(ds, data_spec, aps_index, data_spec.APS_X[aps_index], data_spec.APS_Y[aps_index], 
        //    ds.q[b0_index], ds.q[tau_index], A_sum, B_sum);
        ///////////////////////////////////////////////////////////
        {
          //Variables from function call
          int target_offset = aps_index;
          double b = ds.q[b0_index];
          double precision = ds.q[tau_index];
          double& nom_sum = std::ref(A_sum);
          double& denom_sum = std::ref(B_sum);

          int i;
          double D = 0.0, D2 = 0.0;

          int N_cases = data_spec.N_cases;

          std::vector<double>& fixed_coords = data_spec.AP_coordinates[aps_index];

          for (i = 0; i < data_spec.observations_nopredict; i++) {
            if (isnan(data_spec.train_signal[target_offset][i])) {
              continue;
            }

            D = data_spec.D[target_offset][i];
            D2 = data_spec.Dsqrd[target_offset][i];

            nom_sum += D * (data_spec.train_signal[target_offset][i] - b);
            denom_sum += D2;
          }

          for (i = data_spec.observations_nopredict; i < N_cases; i++) {
            if (isnan(data_spec.train_signal[target_offset][i])) {
              continue;
            }

            double sum_squares = 0.0;
            for (size_t dimension = 0; dimension < fixed_coords.size(); ++dimension) {
              size_t position_index = i - data_spec.observations_nopredict + dimension*data_spec.predictNo;
              sum_squares += pow(ds.position_estimates[position_index] - fixed_coords[dimension], 2.0);
            }

            D = log(1+sqrt(sum_squares));

            nom_sum += D * (data_spec.train_signal[target_offset][i] - b);
            denom_sum += D * D;
          }

          nom_sum *= precision;
          denom_sum *= precision;
        }
        ///////////////////////////////////////////////////////////

        A += A_sum;
        B += B_sum;
      }  

      double post_mean = A / B;
      double post_variance = 1 / B;

      ds.q[k] = random_gaussian(post_mean, post_variance);
    }
    ///////////////////////////////////////////////////////////
  }

  for (k = ds.first_taui_index; k < ds.q.size(); k++) {
    ///////////////////////////////////////////////////////////
    //double new_value = conjugateGammaTaui(ds, data_spec, k);
    //This is conjugate Gamma sampler for tau i 
    {
      int N_cases = data_spec.N_cases;
      double prior_shape = 0.001;
      double prior_mean = 0.001;
      int aps_index = k - ds.first_taui_index;
      int b0_index = k - 2*data_spec.MAX_APS;
      int b1_index = k - data_spec.MAX_APS;

      int cntminus1 = 0;
      int i;
      for (i = 0; i < N_cases; i++) {
        if (isnan(data_spec.train_signal[aps_index][i])) {
          cntminus1++;
        }
      }

      double post_shape = prior_shape + (double)(N_cases-cntminus1)/2;
      double post_mean = 0.0;
      double sum = 0.0;


      ///////////////////////////////////////////////////////////
      //sum = conjugateGammaTaui_PosteriorMeanSum(ds, data_spec, aps_index, data_spec.APS_X[aps_index], data_spec.APS_Y[aps_index], 
      //    ds.q[b0_index], ds.q[b1_index]);
      {
        //Variables from function call
        int target_offset = aps_index;
        double b0 = ds.q[b0_index];
        double b1 = ds.q[b1_index];

        int i;
        double D=0.0, m=0.0;

        int N_cases = data_spec.N_cases;
        std::vector<double>& fixed_coords = data_spec.AP_coordinates[aps_index];

        for (i = 0; i < data_spec.observations_nopredict ; i++) {
          if (isnan(data_spec.train_signal[target_offset][i])) {
            continue;
          }

          D = data_spec.D[target_offset][i];
          m = b0 + (b1 * D);

          // Likelihood: Si
          sum += pow(data_spec.train_signal[target_offset][i] - m, 2.0);
        }

        for (i = data_spec.observations_nopredict; i < N_cases; i++) {
          if (isnan(data_spec.train_signal[target_offset][i])) {
            continue;
          }

          double sum_squares = 0.0;
          for (size_t dimension = 0; dimension < fixed_coords.size(); ++dimension) {
            size_t position_index = i - data_spec.observations_nopredict + dimension*data_spec.predictNo;
            sum_squares += pow(ds.position_estimates[position_index] - fixed_coords[dimension], 2.0);
          }

          D = log(1+sqrt(sum_squares));
          m = b0 + (b1 * D);

          // Likelihood: Si
          sum += pow(data_spec.train_signal[target_offset][i] - m, 2.0);
        }
      }
      ///////////////////////////////////////////////////////////


      post_mean = sum/2 + prior_mean;

      ds.q[k] = rgamma(post_shape, 1/post_mean);
    }
    ///////////////////////////////////////////////////////////
  }
}

//M2 solver
std::vector<md_slicing::Result> md_slicing::multiD_m2solve( int argc, char** argv,
    const MultiDTrainingData& train_data,
    const std::vector<std::vector<double>>& ap_data,
    const std::vector<double>& domains,
    int burnin,
    int additional_iter,
    SampleMethod sampling_method)
{ 
  std::vector<double> max_domain = domains;
  const float outside_drift = max_domain[0]/3.0;
  //Add the allowable drift to each dimension and constrain at the end
  std::for_each(max_domain.begin(), max_domain.end(),
      [&](double& d) { d += 2*outside_drift;});

  int c;
  int g_shrink = 0;
  double width = 1.0;   /* Estimate of the size of the slice */
  double sf = 1.0;   /* Estimate of the size of the slice */
  int b_accept = 0;
  int steps = 35, in_steps = steps;   /* Total number of steps, steps inside slice */
  int in_out_side = -1;  /* Will be either inside or outside */

  for (int i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      c = argv[i][1];
      
      switch (c) {
        case 'w':
          width = atof(argv[++i]); 
          break;	
        case 'g':
          g_shrink = 1;
          break;	
        case 'f':
          sf = atof(argv[++i]); 
          break;		
        case 'b':
          b_accept = 1;
          break;
        case 'p':
          steps = atoi(argv[++i]); 
          in_steps = steps;
          break;	
        case 'i':
          in_out_side = INSIDE; 
          break;		
        case 'o':
          in_out_side = OUTSIDE; 
          break;				
      }
    }
  }

  if ( sampling_method < slice1 or sampling_method > met1all ) {
    fprintf(stderr, "Method is not any of [slice1|slice2d|met1|ref2d|slice1all|slice2dall|met1all]\n");
    exit(1);
  }

  int N_inputs = 2;
  int total_iterations = additional_iter + burnin;
  int total_network_variables = 0;

  for (auto T = train_data.begin(); T != train_data.end(); ++T) {
    if (ap_data.size() != T->rssis.size()) {
      std::cerr<<"RSSI values in training data do not match AP data.\n";
      std::cerr<<"Aborting.\n";
      return std::vector<Result>(0);
    }
  }
  double_matrix train_signal_notclean(ap_data.size(), std::vector<double>(train_data.size()));

  std::vector<std::vector<double>> ap_locations_notclean(ap_data.size());
  std::vector<std::vector<double>> train_locations(train_data.size());

  //Add the allowable drift to each dimension and constrain at the end
  //For each dimension in the x and y locations of the input data add
  //in the outside drift value.
  //This is to compensate for some items being at the border of the domain.

  //The number of APs before pruning APs with no data.
  int MAX_APS_NOTCLEAN = ap_data.size();

  //Copy over the AP and training coordinates adjusted for the out of map expansion
  std::transform(ap_data.begin(), ap_data.end(), ap_locations_notclean.begin(),
      [=](std::vector<double> p) {
      for (auto I = p.begin(); I != p.end(); ++I) *I += outside_drift;
      return p;});

  //std::transform(train_data.begin(), train_data.end(), train_locations.begin(),
      //[=](const Sample& s) { return std::vector<double>{s.x + outside_drift, s.y+outside_drift};});
  std::transform(train_data.begin(), train_data.end(), train_locations.begin(),
      [=](MultiDSample ms) {
      std::vector<double> train_location;
      for (auto I = ms.coordinates.begin(); I != ms.coordinates.end(); ++I)
        train_location.push_back(*I + outside_drift);
      return train_location;});

  //N cases is the number of transmitters in this dataset
  int N_cases = train_data.size();

  for (int i = 0; i < MAX_APS_NOTCLEAN; ++i) {
    for (int j = 0 ; j < N_cases; ++j) {
      train_signal_notclean[i][j] = train_data[j].rssis[i];
    }
  }

  //The number of samples with unknown locations (as indicated by NAN values
  //in their coordinates.
  //int predictNo = std::accumulate(train_data.begin(), train_data.end(), 0,
      //[](int sum, const Sample& arg) { return sum + (isnan(arg.x) ? 1 : 0); });
  int predictNo = std::accumulate(train_data.begin(), train_data.end(), 0,
      [](int sum, const MultiDSample& arg) { return sum + (isnan(arg.coordinates.front()) ? 1 : 0); });
  //Replace all NaNs with 0.0.
  std::for_each(train_locations.begin(), train_locations.end(),
      [](std::vector<double>& arg) {
      std::for_each(arg.begin(), arg.end(), [](double& d) {
        if (isnan(d)) d = 0.0;});});
  
  int MAX_APS;
  std::vector<std::vector<double>> ap_locations;
  double_matrix train_signal;
  {
    std::vector<double> ap_data(MAX_APS_NOTCLEAN);
    //Check the data from each AP in all of the samples. If any AP gave only
    //values of -1 then remove its rssis from the data.
    std::vector<int> empty_columns;
    for (int column = 0; column < MAX_APS_NOTCLEAN; ++column) {
      bool empty_ap = std::accumulate(train_signal_notclean.begin(), train_signal_notclean.end(), true,
          [=](bool all_bad, const std::vector<double>& arg) {
          return all_bad and (arg[column] == -1.0 or isnan(arg[column]));});
      if (empty_ap) {
        empty_columns.push_back(column);
      }
    }


    //Remove the columns of data with indices in the empty_columns vector
    //from the training data and the ap data

    //Get the ap locations for aps with empty data sets (all -1 RSSI)
    MAX_APS = MAX_APS_NOTCLEAN - empty_columns.size();

    auto next_empty = empty_columns.begin();
    int skipped = 0;
    for (int ap = 0; ap < MAX_APS_NOTCLEAN; ++ap) {
      //Skip this entry if this AP contributes no data
      if (next_empty != empty_columns.end() and *next_empty == ap) {
        ++next_empty;
        ++skipped;
      } else {
        //Since ths row is nonempty record data from it - the x and y location of
        //the access point and the RSS values that it saw for each transmitter.
        ap_locations.push_back(ap_locations_notclean[ap]);
        train_signal.push_back(std::vector<double>(N_cases));
        //For each transmitter store data from this ap
        for (int txer = 0; txer < N_cases; ++txer) {
          //If the signal came in with a -1 that means there was no data. However,
          //this receiver was working during the last period so this receiver was
          //too far away to receive this transmitter's signal.
          //TODO FIXME Should be a way of getting better results here.
          if (train_signal_notclean[ap][txer] == -1.0 or isnan(train_signal_notclean[ap][txer])) {
            train_signal_notclean[ap][txer] = -95.0;
          }
          //Replace -1 with NaN to indicate no data
          //if (train_signal_notclean[ap][txer] == -1.0) {
            //train_signal_notclean[ap][txer] = std::numeric_limits<double>NaN();
          //}
          //Change the signal data to absolute values for the solver.
          train_signal.back()[txer] = fabs(train_signal_notclean[ap][txer]);
        }
      }
    } 
  }

  // The number of training points
  int observations_nopredict = N_cases - predictNo;

  data_specifications data_spec(N_inputs, MAX_APS, N_cases, train_locations, train_signal,
        predictNo, observations_nopredict, ap_locations);

  //TODO FIXME there should be multiple arrays, not one array with a bunch of stuff in it.
  total_network_variables = MAX_APS*3+max_domain.size()*predictNo;

  double_matrix samples_in_memory =
    double_matrix(max_domain.size()*predictNo, std::vector<double>(additional_iter));
 
  //Initialize the dynamic state struct
  //TODO FIXME declare an instance of the struct here and make this the constructor
  mc_dynamic_state ds(MAX_APS, predictNo, total_network_variables, max_domain);

  md_rejections = 0;
  md_evaluate_energy = 0;
  
  //TODO FIXME add the other sampling methods back into this piece of code
  if ( sampling_method == slice1all) {       // single variate slice sampling
    for (int i = 0; i < total_iterations; i++) {
      large_slice_1all(ds, data_spec, width, md_rejections);
      //The first <burnin> values can be ignored.
      if (i >= burnin) {
        for (int j = 0; j < max_domain.size()*predictNo; j++) {
          samples_in_memory[j][i-burnin] = ds.position_estimates[j];
        }
      }  
    }
  }
  else if ( sampling_method == slice1) {
    for (int i = 0; i < total_iterations; i++) {
      large_updateNodes_slice_1(ds, data_spec, width, md_rejections);
      //The first <burnin> values can be ignored.
      if (i >= burnin) {
        for (int j = 0; j < max_domain.size()*predictNo; j++) {
          samples_in_memory[j][i-burnin] = ds.position_estimates[j];
        }
      }  
    }
  }
  /*
   *TODO FIXME add support for this
  else if ( sampling_method == slice2d) {  // 2d variate slice sampling
    std::vector<double> save(total_network_variables);  // For X, Ys to localize
    std::vector<double> lowb(total_network_variables);  // For Low bandwidth of X, Ys
    std::vector<double> highb(total_network_variables); // For High bandwidth of X, Ys

    for (int i = 0; i < total_iterations; i++) {
      updateNodes_slice_2D(ds, data_spec, save, lowb, highb, g_shrink, width, use_domain, md_rejections);
      //The first <burnin> values can be ignored.
      if (i >= burnin) {
        for (int j = 0; j < total_network_variables; j++) {
          samples_in_memory[j][i-burnin] = ds.q[j];
        }
      }  
    }   
  }
  else if (!strcmp(sampling_method, "slice2dall")) {  // 2d variate slice sampling
    double *save = chk_alloc(total_network_variables, sizeof (double)); // For X, Ys to localize
    double *lowb = chk_alloc(total_network_variables, sizeof (double));   // For Low bandwidth of X, Ys
    double *highb = chk_alloc(total_network_variables, sizeof (double));   // For High bandwidth of X, Ys
    
    for (i = 0; i < total_iterations; i++) {
      iterationNo = i + 1;
      
      updateNodes_slice_2Dall(&ds, &data_spec, save, lowb, highb, g_shrink, width, use_domain);
      
      if ((stats_file || samples_file) && (i >= burnin)) {
	for (j = 0; j < total_network_variables; j++) {
	  samples_in_memory[j][i-burnin] = ds.q[j];
	}
      }  
    }   
  }
  else if (!strcmp(sampling_method, "met1")) {  // single variate metropolis
    for (i = 0; i < total_iterations; i++) {
      iterationNo = i + 1;
      
      updateNodes_met_1(&ds, &data_spec, sf, 1.0, b_accept, use_domain);
      
      if ((stats_file || samples_file) && (i >= burnin)) {
	for (j = 0; j < total_network_variables; j++) {
	  samples_in_memory[j][i-burnin] = ds.q[j];
	}
      }  
    }
  } 
  else if (!strcmp(sampling_method, "met1all")) {  // single variate metropolis
    for (i = 0; i < total_iterations; i++) {
      iterationNo = i + 1;
      
      updateNodes_met_1all(&ds, &data_spec, sf, 1.0, b_accept, use_domain);
      
      if ((stats_file || samples_file) && (i >= burnin)) {
	for (j = 0; j < total_network_variables; j++) {
	  samples_in_memory[j][i-burnin] = ds.q[j];
	}
      }  
    }
  } 
  else if (!strcmp(sampling_method, "ref2d")) { // 2d reflective slice sampling
    if (in_out_side == -1) {
      fprintf(stderr, "Must specify -i (inside) or -o (outside)\n");
      exit(1);
    }

    for (i = 0; i < total_iterations; i++) {
      iterationNo = i + 1;
      
      updateNodes_ref_2D(&ds, &data_spec, sf, steps, in_steps, in_out_side);
      
      if ((stats_file || samples_file) && (i >= burnin)) {
	for (j = 0; j < total_network_variables; j++) {
	  samples_in_memory[j][i-burnin] = ds.q[j];
	}
      }  
    }
  }
  */

  //Print out a bit of debugging information
  std::cout<<"Total variables, aps, transmitters, predictions, and dimensions: "<<
    total_network_variables<<", "<<MAX_APS<<", "<<observations_nopredict<<", "<<predictNo<<", "<<max_domain.size()<<'\n';
  std::vector<Result> results;


  //Correct for the outside drift allowed at the beginning
  std::for_each(max_domain.begin(), max_domain.end(),
      [&](double& d) { d -= 2*outside_drift;});

  {
    size_t dimensions = max_domain.size();
    for (int i = 0; i < predictNo; ++i) {
      size_t total = 0;
      //Running average and sum of squares for the x and y coordinates
      std::vector<double> averages(max_domain.size());
      std::vector<double> sum_squares(max_domain.size());
      std::vector<double> stddevs(max_domain.size());

      //Find the average and variance of the likely points from this transmitter
      for (int j = 0; j < additional_iter; j++) {
        ++total;
        for (size_t dimen = 0; dimen < dimensions; ++dimen) {
          //Use the current value and the previous average to keep a
          //running average and sum of squares of the difference from the mean
          double var = samples_in_memory[i+dimen*predictNo][j];
          double prev_avg = averages[dimen];

          //Correct each dimension for the drift allowed previously
          var -= outside_drift;
          if (var < 0) {
            var = 0.0;
          }
          else if (var > max_domain[dimen]) {
            var = max_domain[dimen];
          }

          averages[dimen] += (var - prev_avg)/total;
          if (total > 1) {
            sum_squares[dimen] += (var - prev_avg)*(var - averages[dimen]);
          }
        }
      }
      if ( total > 1 ) {
        std::transform(sum_squares.begin(), sum_squares.end(), stddevs.begin(),
            [&](double d) { return sqrt(d / (total - 1));});
      }
      //Save the results of this localization
      results.push_back(Result{averages, stddevs});
    }
  }
 
  return results;
}

