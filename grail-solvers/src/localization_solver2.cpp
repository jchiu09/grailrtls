#include <algorithm>
#include <array>
#include <iostream>
#include <map>
#include <string>
#include <time.h>
#include <vector>
#include <utility>
#include <limits>
#include <functional>
#include <cmath>
#include <mutex>
#include <set>
#include <thread>

#include "grail_types.hpp"
#include "world_model_protocol.hpp"
#include "grailV3_solver_client.hpp"
#include "multidimensional_slicing.hpp"
#include "netbuffer.hpp"

using namespace world_model;

using namespace grail_types;

using std::make_pair;

struct Debug {
  bool off;
};

template<typename T>
Debug& operator<<(Debug& dbg, T arg) {
  if (dbg.off) {
    std::cerr<<arg;
  }
  return dbg;
}

std::u16string toU16(const std::string& str) {
  return std::u16string(str.begin(), str.end());
}

std::string toString(const std::u16string& str) {
  return std::string(str.begin(), str.end());
}

//This function is not thread safe (but could be made so with a mutex)
struct localizeCallback {
  //World model to send solutions to
  SolverWorldModel& swm;

  int burnin;
  int additional_iter;
  md_slicing::SampleMethod s_method;
  bool domain;
  std::vector<double> domains;

  double mobility_threshold;

  //Locations of anchor transmitters and receivers in each region
  //Stored in format (region -> anchor URI -> (x, y) pair or (x, y, z) triple)
  std::map<std::u16string, std::map<URI, std::vector<double>>> anchor_locations;
  grail_time update_interval;

  std::mutex data_mutex;
  //The rssi values for an rx/tx pair.
  //Stored in format (rx URI -> tx URI -> (median RSS, time)
  std::map<URI, std::map<URI, std::pair<double, grail_time>>> rssis;
  //Variances for transmitters. These are used to trigger motion-based localization
  std::map<URI, double> variances;
  std::map<URI, grail_time> last_localized;
  //Keep track of active receivers.
  std::set<URI> is_receiver;

  Debug debug;
  
  //Constructor
  localizeCallback(SolverWorldModel& swm) : swm(swm) {
    debug.off = true;
  }

  //Tries to localize transmitters at least every update_interval seconds.
  void timedLocalize() {
    while (1) {
      //Lock the accessible variances and determine which URIs need to be localized.
      std::set<URI> to_localize;
      {
        std::unique_lock<std::mutex> lck(data_mutex);
        for (auto uri = last_localized.begin(); uri != last_localized.end(); ++uri) {
          if (variances[uri->first] > mobility_threshold) {
            to_localize.insert(uri->first);
          }
          else if (uri->second + update_interval < getGRAILTime()) {
            to_localize.insert(uri->first);
          }
        }
      }
      debug<<"Running localization round with "<<to_localize.size()<<" elements.\n";
      if (not (to_localize.empty() or domains.empty())) {
        //Localize (this also sends data to the world model and updates the last_localized map).
        //Also expire locations of URIs that need to be updated but that are not getting samples.
        this->localize(to_localize);
      }
      //Wait at least one second after every localization to get more samples.
      //Localizing takes time, so this is an extra second after the localization algorithm runs
      sleep(1);
    }
  }

  struct M2Setup {
    MultiDTrainingData t_data;
    std::vector<std::vector<double>> l_data;
    std::vector<URI> knowns;
    std::vector<URI> unknowns;
  };

  /****
   * Generate training data for the m2 solver.
   * Only use the last update_interval seconds worth of data.
   * This returns a map of regions to training data in that region.
   ****/
  std::map<u16string, M2Setup> genTrainingData(std::set<URI>& to_localize, time_t time_cutoff) {
    //Assemble data in the format needed by the M2 localization solver
    //First build a multi-layered map from region to receiver to transmitter.
    //Then generate M2 formatted data for each phy in each region.
    //Use the receiver to transmitter map to make a vector for each sample that
    //ties an x and y offset to a signal map from each receiver.
    std::map< std::u16string, std::map<URI, std::map<URI, double>>> region_rxer_txer_rss;
    {
      std::unique_lock<std::mutex> lck(data_mutex);
      //First copy over the anchor locations
      for (auto I = anchor_locations.begin(); I != anchor_locations.end(); ++I) {
        std::u16string region = I->first;
        for (auto J = I->second.begin(); J != I->second.end(); ++J) {
          std::u16string cur_rxer = J->first;
          //Populate data for any receivers that are receiving data
          if (0 != is_receiver.count(cur_rxer)) {
            //Set up an empty map for RSS values
            std::map<URI, double> rxer_map;
            //Fetch the signal values seen by this receiver
            std::map<URI, std::pair<double, grail_time>>& rx_rssis = rssis[cur_rxer];
            for (auto TX = rx_rssis.begin(); TX != rx_rssis.end(); ++TX) {
              //If this is a transmitter to localize and it has recent data at this receiver
              //add it to this receiver's signal data
              //Also add it if it is an anchor transmitter
              if ((to_localize.count(TX->first) and TX->second.second > time_cutoff) or
                  anchor_locations[region].find(TX->first) != anchor_locations[region].end()) {
                rxer_map[TX->first] = TX->second.first;
              }
              //TODO FIXME Remove stale values from the map
            }
            if (not rxer_map.empty()) {
              region_rxer_txer_rss[region][cur_rxer] = rxer_map;
            }
          }
        }
      }
    }

    //All of the data we need to do localization is now in region_rxer_txer_rss.
    //Now set up M2 training and localization data for each region.
    std::map<std::u16string, M2Setup> region_m2_data;
    for (auto rrtr = region_rxer_txer_rss.begin(); rrtr != region_rxer_txer_rss.end(); ++rrtr) {
      std::u16string region = rrtr->first;
      //Set up landmark locations.
      std::vector<std::vector<double>> l_data;
      std::map<URI, std::map<URI, double>>& rxer_txer_rss = rrtr->second;
      {
        std::unique_lock<std::mutex> lck(data_mutex);
        for (auto RX = rxer_txer_rss.begin(); RX != rxer_txer_rss.end(); ++RX) {
          l_data.push_back(anchor_locations[region][RX->first]);
        }
      }
      MultiDTrainingData t_data_unknown;
      MultiDTrainingData t_data_known;
      std::vector<URI> unknowns;
      std::vector<URI> knowns;

      //Assemble the set of all transmitters that will serve as anchors or that
      //will be localized in this region.
      std::set<URI> region_txers;
      for (auto RX = rxer_txer_rss.begin(); RX != rxer_txer_rss.end(); ++RX) {
        for (auto TX = RX->second.begin(); TX != RX->second.end(); ++TX) {
          region_txers.insert(TX->first);
        }
      }

      //A NaN value marks an unknown location.
      const float qnan = std::numeric_limits<float>::quiet_NaN();

      for (auto TX = region_txers.begin(); TX != region_txers.end(); ++TX) {
        //Record the location and all of the RSS values in this sample.
        MultiDSample sample;
        //Push back RSS values for each receiver.
        for (auto RX = rxer_txer_rss.begin(); RX != rxer_txer_rss.end(); ++RX) {
          std::map<URI, double>& txer_rss = RX->second;
          //Insert the RSS value
          if (txer_rss.end() != txer_rss.find(*TX)) {
            sample.rssis.push_back(txer_rss[*TX]);
          }
          else {
            //TODO FIXME Move to nan here rather than -1
            sample.rssis.push_back(-1);
          }
        }

        {
          std::unique_lock<std::mutex> lck(data_mutex);
          //Now put in the coordinates
          //Is this an anchor or an unknown?
          if (anchor_locations[region].find(*TX) == anchor_locations[region].end()) {
            unknowns.push_back(*TX);
            sample.coordinates = std::vector<double>(domains.size(), qnan);
            t_data_unknown.push_back(sample);
          }
          else {
            knowns.push_back(*TX);
            std::vector<double> location = anchor_locations[region][*TX];
            sample.coordinates = location;
            t_data_known.push_back(sample);
          }
        }
      }
      //Now make a single training data vector with the unknowns at the end
      MultiDTrainingData t_data(t_data_unknown.size() + t_data_known.size());
      std::copy(t_data_known.begin(), t_data_known.end(), t_data.begin());
      std::copy(t_data_unknown.begin(), t_data_unknown.end(),
          t_data.begin() + t_data_known.size());
      debug<<"Generating training data with "<<t_data_known.size()<<
        " fiduciary transmitters and solving for "<<t_data_unknown.size()<<
        " unlocalized transmitters in region "<<toString(region)<<"\n";

      region_m2_data[rrtr->first] = M2Setup{t_data, l_data, knowns, unknowns};
    }
    return region_m2_data;
  }


  //Call the m2 solver using transmitter data for transmitters in the active_tx table.
  void localize(std::set<URI>& to_localize) {
    grail_time time_cutoff = getGRAILTime() - update_interval;
    std::map<std::u16string, M2Setup> region_m2s = this->genTrainingData(to_localize, time_cutoff);

    //Create a solutions vector to put solutions into
    vector<SolverWorldModel::Solution> solns;

    //Localize in each region.
    for (auto setup = region_m2s.begin(); setup != region_m2s.end(); ++setup) {
      M2Setup& m2s = setup->second;
      //Don't localize if there are no targets of interest or anchors
      if (0 == m2s.unknowns.size() or 0 == m2s.knowns.size()) {
        debug<<"Not enough data to localize in region "<<toString(setup->first)<<"\n";
        return;
      }

      debug<<"For region "<<toString(setup->first)<<" sending solver this:\n";
      //Print out each row (transmitter)
      for (auto I = m2s.t_data.begin(); I != m2s.t_data.end(); ++I) {
        for (auto coord = I->coordinates.begin(); coord != I->coordinates.end(); ++coord) {
          debug<<*coord<<'\t';
        }
        //Print out the column value (receiver observed RSS)
        for (auto rss = I->rssis.begin(); rss != I->rssis.end(); ++rss) {
          debug<<*rss<<'\t';
        }
        debug<<'\n';
      }
      debug<<'\n';

      //TODO FIXME Sending all of the results to the solver at the same time
      //gives very poor results so each value will be solved for seperately.
      /*
      std::vector<md_slicing::Result> results;
      MultiDTrainingData known(m2s.t_data.begin(), m2s.t_data.begin() + (m2s.t_data.size() - m2s.unknowns.size()));
      for (size_t solution = 0; solution < m2s.unknowns.size(); ++solution) {
        MultiDTrainingData single_unknown = known;
        //Push back the data for just this unkown value.
        single_unknown.push_back(m2s.t_data[known.size()+solution]);
        std::vector<md_slicing::Result> single_result = multiD_m2solve(0, NULL, single_unknown, m2s.l_data,
            domains, burnin, additional_iter, s_method);
        if ( 0 < single_result.size()) {
          results.push_back(single_result[0]);
        }
      }
      */
      std::vector<md_slicing::Result> results = multiD_m2solve(0, NULL, m2s.t_data, m2s.l_data,
          domains, burnin, additional_iter, s_method);

      debug<<"Localizing in region "<<toString(setup->first)<<'\n';

      debug<<"ID\tx\ty\tx_stddev\ty_stddev\n";
      for (size_t tx_i = 0; tx_i < results.size(); ++tx_i) {

        md_slicing::Result& r = results[tx_i];
        debug<<toString(m2s.unknowns[tx_i])<<'\t';
        for (auto coord = r.coordinates.begin(); coord != r.coordinates.end(); ++coord) {
          debug<<*coord<<'\t';
        }
        for (auto stddev = r.stddevs.begin(); stddev != r.stddevs.end(); ++stddev) {
          debug<<*stddev<<'\t';
        }
        debug<<'\n';

        std::vector<u16string> offset_names{u"location.xoffset", u"location.yoffset", u"location.zoffset"};
        std::vector<u16string> deviation_names{u"location.xstddev", u"location.ystddev", u"location.zstddev"};

        grail_time soln_time = world_model::getGRAILTime();
        //Set the last localized time for this transmitter
        {
          std::unique_lock<std::mutex> lck(data_mutex);
          last_localized[m2s.unknowns[tx_i]] = soln_time;
        }

        //Push back the region of this transmitter and it location.
        SolverWorldModel::Solution region{u"location.uri", soln_time, m2s.unknowns[tx_i], vector<uint8_t>()};
        pushBackUTF16(region.data, setup->first);
        solns.push_back(region);
        for (size_t i = 0; i < r.coordinates.size(); ++i) {
          SolverWorldModel::Solution offset{offset_names[i], soln_time, m2s.unknowns[tx_i], vector<uint8_t>()};
          SolverWorldModel::Solution deviation{deviation_names[i], soln_time, m2s.unknowns[tx_i], vector<uint8_t>()};
          pushBackVal(r.coordinates[i], offset.data);
          pushBackVal(r.stddevs[i], deviation.data);
          solns.push_back(offset);
          solns.push_back(deviation);
        }
      }
    }
    //If localization occured send the new solutions to the world model.
    if (not solns.empty()) {
      //Do not create new URIs if these URIs don't exist.
      //Unless they were deleted they should appear in the world model.
      swm.sendData(solns, false);
    }
  }
};


int main(int arg_count, char** arg_vector) {
  if (4 != arg_count) {
    std::cerr<<"This program needs 3 arguments:\n";
    std::cerr<<"\t"<<arg_vector[0]<<" <world model ip> <world model client port> <world model solver port>\n";
    return 0;
  }
  std::string wm_ip(arg_vector[arg_count-3]);
  int solver_port = std::stoi(std::string((arg_vector[arg_count-2])));
  int client_port = std::stoi(std::string((arg_vector[arg_count-1])));

  //Set up the solver world model connection;
  std::string origin = "grail/localization_solver\nversion 1.0\nAlgorithm M2";
  //Provide location.x and location.y
  std::vector<std::pair<u16string, bool>> type_pairs{{u"location.xoffset", false},
    {u"location.yoffset", false},
    {u"location.zoffset", false},
    {u"location.xstddev", false},
    {u"location.ystddev", false},
    {u"location.zstddev", false},
    {u"location.uri", false} };
  SolverWorldModel swm(wm_ip, solver_port, type_pairs, u16string(origin.begin(), origin.end()));
  if (not swm.connected()) {
    std::cerr<<"Could not connect to the world model as a solver - aborting.\n";
    return 0;
  }

  //Initialize the localizer struct
  localizeCallback cb_struct(swm);
  //Set these from optional command line parameters
  cb_struct.burnin = 1000;
  cb_struct.additional_iter = 5000;
  cb_struct.s_method = md_slicing::slice1;
  cb_struct.domain = true;
  //Try to update location once every two minutes.
  //Motion will also trigger localization
  cb_struct.update_interval = 120000;
  cb_struct.mobility_threshold = 2.5;

  //Anchor location data for the cb struct comes from the world model.
  
  ClientWorldModel cwm(wm_ip, client_port);
  if (not cwm.connected()) {
    std::cerr<<"Could not connect to the world model as a client - aborting.\n";
    return 0;
  }

  enum class Ticket : uint32_t {link_median, avg_var, sensor, region, anchors, training};

  //Get attributes named "average variance" and "link median" and find
  //any URIs with attached sensors.
  //Use average variance to detect mobility and link median values to localize
  URI anything = u".*";
  vector<URI> link_attributes{u"link median"};
  vector<URI> transmitter_attributes{u"average variance"};
  vector<URI> sensor_attributes{u"sensor.*"};
  grail_time interval = 2000;
  cwm.setupSynchronousDataStream(anything, link_attributes, interval, (uint32_t)Ticket::link_median);
  cwm.setupSynchronousDataStream(anything, transmitter_attributes, interval, (uint32_t)Ticket::avg_var);
  cwm.setupSynchronousDataStream(anything, sensor_attributes, interval, (uint32_t)Ticket::sensor);
  //Also request the region size
  grail_time region_interval = 5000;
  URI region_search = u"region\\..*";
  vector<URI> domain_string{u"location\\.max."};
  cwm.setupSynchronousDataStream(region_search, domain_string, region_interval, (uint32_t)Ticket::region);
  //Let's also request anchor URIs
  grail_time anchor_interval = 5000;
  URI anchors = u".*\\.anchor\\..*";
  vector<URI> locations{u"location\\..offset", u"sensor.*"};
  cwm.setupSynchronousDataStream(anchors, locations, anchor_interval, (uint32_t)Ticket::anchors);
  //Also get static training data
  URI train_points = u".*\\.training point\\..*";
  vector<URI> train_locations{u"location\\..offset", u"receivers\\.vector<sized string>", u"fingerprint\\.vector<RSS>"};
  cwm.setupSynchronousDataStream(train_points, train_locations, anchor_interval, (uint32_t)Ticket::training);

  //Set up a timed localization thread. This allows the system to process packets
  //even while it is localizing.
  std::thread timed_localize(std::mem_fun(&localizeCallback::timedLocalize), &cb_struct);
  //Detach the thread so that it can run independently
  timed_localize.detach();

  //Define some functions to make finding attributes simple
  auto get_maxx = [&](Attribute& a) { return a.name.find(u"location.maxx") != std::u16string::npos;};
  auto get_maxy = [&](Attribute& a) { return a.name.find(u"location.maxy") != std::u16string::npos;};
  auto get_maxz = [&](Attribute& a) { return a.name.find(u"location.maxz") != std::u16string::npos;};
  auto get_xoffset = [&](Attribute& a) { return a.name.find(u"location.xoffset") != std::u16string::npos;};
  auto get_yoffset = [&](Attribute& a) { return a.name.find(u"location.yoffset") != std::u16string::npos;};
  auto get_zoffset = [&](Attribute& a) { return a.name.find(u"location.zoffset") != std::u16string::npos;};
  //Do not use the message in message backup receivers since they will not receive
  //packets regularly
  auto get_sensor = [&](Attribute& a) { return (a.name != u"sensor.mim") and (a.name.find(u"sensor") != std::u16string::npos);};
  auto get_variance = [&](Attribute& a) { return a.name == u"average variance";};
  auto get_median = [&](Attribute& a) { return a.name == u"link median";};

  //Function to unpack training data from the world model
  auto readURIVector  = [&](Buffer& data) {
    return unpackGRAILVector<URI>([&](BuffReader& br){return br.readSizedUTF16();}, data);};
  auto readDoubleVector = [&](Buffer& data) {
    return unpackGRAILVector<double>([&](BuffReader& bf){return bf.readPrimitive<double>();}, data);};
  auto get_rxer_vector = [&](Attribute& a) { return a.name == u"receivers.vector<sized string>";};
  auto get_rss_vector = [&](Attribute& a) { return a.name == u"fingerprint.vector<RSS>";};

  //Keep track of which transmitters correspond to which URIs.
  std::map<URI, URI> tx_to_uri;

  std::cerr<<"Starting loop...\n";
  while (1) {
    //Get world model updates
    ClientWorldModel::world_state ws;
    uint32_t ticket;
    std::tie(ws, ticket) = cwm.getSynchronousStreamUpdate();
    //Search the URIs to determine their types (anchor, region, mobile sensor)
    for (auto I = ws.begin(); I != ws.end(); ++I) {
      if (ticket == (uint32_t)Ticket::training) {
        //Get the region name and transmitter ID
        std::u16string::size_type tpoint_index = I->first.find(u".training point.");
        //Everything before ".training point." is the name of the region
        std::u16string region_uri = I->first.substr(0, tpoint_index);
        //Everything after ".training point." is the name of this training point
        std::u16string point_uri = I->first.substr(tpoint_index + std::u16string(u".training point.").size());

        //There should be an x and a y but there may not be a z
        auto xattrib = std::find_if(I->second.begin(), I->second.end(), get_xoffset);
        auto yattrib = std::find_if(I->second.begin(), I->second.end(), get_yoffset);
        auto zattrib = std::find_if(I->second.begin(), I->second.end(), get_zoffset);
        auto rx_attrib = std::find_if(I->second.begin(), I->second.end(), get_rxer_vector);
        auto rss_attrib = std::find_if(I->second.begin(), I->second.end(), get_rss_vector);
        //Pull out the data from the training points and update the rssis vector in the
        //localization structure.
        if (xattrib != I->second.end() and yattrib != I->second.end()) {
          double xoffset = readPrimitive<double>(xattrib->data, 0);
          double yoffset = readPrimitive<double>(yattrib->data, 0);
          std::vector<double> coordinates{xoffset, yoffset};
          if (zattrib != I->second.end()) {
            coordinates.push_back(readPrimitive<double>(zattrib->data, 0));
          }
          vector<URI> rxers = readURIVector(rx_attrib->data);
          vector<double> rss_vals = readDoubleVector(rss_attrib->data);
          //Lock the localization thread's access to this map and update it
          //but only if the data seems valid.
          if (rxers.size() == rss_vals.size()) {
            std::unique_lock<std::mutex> lck(cb_struct.data_mutex);
            for (size_t sample = 0; sample < rxers.size(); ++sample) {
              cb_struct.rssis[rxers[sample]][point_uri] = make_pair(rss_vals[sample], world_model::MAX_GRAIL_TIME);
              cb_struct.anchor_locations[region_uri][point_uri] = coordinates;
            }
          }
        }
      }
      //Check if this is a region
      std::u16string::size_type region_index = I->first.find(u"region.");
      //Also check if this is an anchor URI
      std::u16string::size_type anchor_index = I->first.find(u".anchor.");
      if (region_index == 0) {
        //Check for max values for x, y, and z domains.
        auto xattrib = std::find_if(I->second.begin(), I->second.end(), get_maxx);
        auto yattrib = std::find_if(I->second.begin(), I->second.end(), get_maxy);
        auto zattrib = std::find_if(I->second.begin(), I->second.end(), get_maxz);
        if (xattrib != I->second.end() and yattrib != I->second.end()) {
          std::u16string region_name = I->first.substr(7);
          double maxx = readPrimitive<double>(xattrib->data, 0);
          double maxy = readPrimitive<double>(yattrib->data, 0);
          cb_struct.domains = std::vector<double>{maxx, maxy};
          std::cerr<<"Region "<<toString(region_name)<<" has domain "<<maxx<<", "<<maxy;
          if (zattrib != I->second.end()) {
            double maxz = readPrimitive<double>(zattrib->data, 0);
            cb_struct.domains.push_back(maxz);
            std::cerr<<", "<<maxz;
          }
          std::cerr<<'\n';
        }
      }
      //Check if this is an anchor URI and update anchor locations.
      //Also add this to the transmitter to URI map
      else if (anchor_index != std::u16string::npos) {
        //Find the x and y offsets and the sensor ID
        auto xattrib = std::find_if(I->second.begin(), I->second.end(), get_xoffset);
        auto yattrib = std::find_if(I->second.begin(), I->second.end(), get_yoffset);
        auto zattrib = std::find_if(I->second.begin(), I->second.end(), get_zoffset);
        auto sensor_id = std::find_if(I->second.begin(), I->second.end(), get_sensor);
        std::u16string region = I->first.substr(0, anchor_index);
        if (xattrib != I->second.end() and yattrib != I->second.end() and sensor_id != I->second.end()) {
          //Read in transmitter ID and location values
          grail_types::transmitter txid = grail_types::readTransmitter(sensor_id->data);
          double xoffset = readPrimitive<double>(xattrib->data, 0);
          double yoffset = readPrimitive<double>(yattrib->data, 0);
          std::vector<double> coordinates{xoffset, yoffset};
          if (zattrib != I->second.end()) {
            coordinates.push_back(readPrimitive<double>(zattrib->data, 0));
          }
          //Add a anchor to tx_to_uri map
          std::string anchor_name = to_string(txid.phy) + "." + to_string(txid.id.lower);
          tx_to_uri[toU16(anchor_name)] = I->first;
          //Lock the anchor map and update its values
          std::unique_lock<std::mutex> lck(cb_struct.data_mutex);
          cb_struct.anchor_locations[region][I->first] = coordinates;
          std::cerr<<"Updating anchor in region "<<toString(region)<<" for txid "<<
            txid.id<<" on phy "<<(uint32_t)txid.phy<<". Location is ";
          for (auto coord = coordinates.begin(); coord != coordinates.end(); ++coord) {
            std::cerr<<*coord<<'\t';
          }
          std::cerr<<'\n';
        }
      }
      //Otherwise check the attributes for rss characteristics
      else {
        //Check if there is a transmitter attribute
        auto tx_attrib = std::find_if(I->second.begin(), I->second.end(), get_sensor);
        if (tx_attrib != I->second.end()) {
          //Transmitters are stored as one byte of physical layer and 16 bytes of ID
          grail_types::transmitter txid = grail_types::readTransmitter(tx_attrib->data);
          std::string tx_name = to_string(txid.phy) + "." + to_string(txid.id.lower);
          //Lock the lock the localization thread's access to this map and update it.
          std::unique_lock<std::mutex> lck(cb_struct.data_mutex);
          if (tx_to_uri.find(toU16(tx_name)) == tx_to_uri.end()) {
            std::cout<<"Adding new transmitter to URI: "<<tx_name<<"->"<<toString(I->first)<<'\n';
            //Add a new transmitter to the tx_to_uri map
            tx_to_uri[toU16(tx_name)] = I->first;
          }
          if ( cb_struct.last_localized.find(I->first) == cb_struct.last_localized.end()) {
            cb_struct.last_localized[I->first] = 0;
          }
        }
        //If this is variance: first determine if this corresponds to a transmitter of a URI
        auto var_attrib = std::find_if(I->second.begin(), I->second.end(), get_variance);
        if (var_attrib != I->second.end() and tx_to_uri.end() != tx_to_uri.find(I->first)) {
          //URIs for average link variances are string representations of the
          //the transmitter in the format <phy>.<id>
          URI uri = tx_to_uri[I->first];
          double variance = readPrimitive<double>(var_attrib->data, 0);
          //Track variance
          //Lock the lock the localization thread's access to this map and update it.
          std::unique_lock<std::mutex> lck(cb_struct.data_mutex);
          cb_struct.variances[uri] = variance;
        }
        auto median_attrib = std::find_if(I->second.begin(), I->second.end(), get_median);
        if (median_attrib != I->second.end()) {
          size_t phy_end = I->first.find(u".");
          size_t tx_end = I->first.find(u".", phy_end+1);
          if (phy_end != std::u16string::npos and tx_end != std::u16string::npos) {
            //URIs for link medians are string representations of the
            //the transmitter in the format <phy>.<txid>.<rxid>
            //Convert this to two URIs, one for the txer and one for the rxer
            URI raw_tx_uri = I->first.substr(0, tx_end);
            URI raw_rx_uri = I->first.substr(0, phy_end) + I->first.substr(tx_end);
            //Now determine if this corresponds to a transmitter of a URI
            if (tx_to_uri.end() != tx_to_uri.find(raw_tx_uri) and
                tx_to_uri.end() != tx_to_uri.find(raw_rx_uri)) {
              URI tx_uri = tx_to_uri[raw_tx_uri];
              URI rx_uri = tx_to_uri[raw_rx_uri];
              double median_rss = readPrimitive<double>(median_attrib->data, 0);
              //Lock the localization thread's access to this map and update it.
              std::unique_lock<std::mutex> lck(cb_struct.data_mutex);
              cb_struct.rssis[rx_uri][tx_uri] = make_pair(median_rss, median_attrib->creation_date);
              if (cb_struct.is_receiver.count(rx_uri) == 0) {
                std::cerr<<"Adding "<<toString(rx_uri)<<" as receiver.\n";
                cb_struct.is_receiver.insert(rx_uri);
              }
            }
          }
        }
      }
    }
  }
  return 0;
}

