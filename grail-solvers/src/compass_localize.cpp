/*
compass_localize.{cpp,hpp} - Functions for direction-based coarse localization
Source version: January 11, 2011
Copyright (c) 2011 Bernhard Firner

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
  claim that you wrote the original software. If you use this software
  in a product, an acknowledgment in the product documentation would be
  appreciated but is not required.

  2. Altered source versions must be plainly marked as such, and must not be
  misrepresented as being the original software.

  3. This notice may not be removed or altered from any source
  distribution.
*/

#include <algorithm>
#include <cmath>
#include <numeric>
#include <utility>
#include <vector>

#include <compass_localize.hpp>
#include <localize_common.hpp>

#include <iostream>

using namespace std;

//Convenience struct to convert a coordinate to a tile position
std::vector<Tile> possibleCompassTiles(const Sample& s,
                                       const LandmarkData& rxers,
                                       double max_x,
                                       double max_y,
                                       double tile_w,
                                       double tile_h,
                                       double min_dist,
                                       double min_diff,
                                       double percentile) {
  int columns = ceil(max_x/tile_w);
  int rows    = ceil(max_y/tile_h);

  //A record of how many times each tile has been marked as possible.
  vector<vector<int>> hits(columns, vector<int>(rows, 0));

  //Use each pair of receivers as a compass to sense a direction for the transmitter.
  for (int rx1 = 0; rx1 < rxers.size(); ++rx1) {
    for (int rx2 = rx1+1; rx2 < rxers.size(); ++rx2) {
      //Check for min distance and difference in RSS
      if ( euclDistance(rxers[rx1], rxers[rx2]) >= min_dist and
          (not (isnan(s.rssis[rx1]) or isnan(s.rssis[rx2]))) and
           abs(s.rssis[rx1] - s.rssis[rx2]) >= min_diff) {
        int closer, farther;
        if (s.rssis[rx1] > s.rssis[rx2]) {
          closer = rx1;
          farther = rx2;
        }
        else {
          closer = rx2;
          farther = rx1;
        }

        //Count the tiles on the stronger side of a line at the
        //midpoint of the two receivers.
        //TODO This should be an arc, not a line
        //TODO Use an y=mx+b equation rather than checking euclDistance repeatedly.
        for (int c = 0; c < columns; ++c) {
          for (int r = 0; r < rows; ++r) {
            double x = c * tile_w + tile_w/2.0;
            double y = r * tile_h + tile_h/2.0;
            //If this tile is closer to the stronger receiver then increment its hit count.
            //if(euclDistance(make_pair(x, y), rxers[closer]) < euclDistance(make_pair(x, y), rxers[farther]))
            //If this tile's distance to the stronger receiver is less than
            //half the distance to the weaker receiver then increment the hit count.
            double alpha = 4.0; //Alpha should not normally be worse than 4.
            double diff = abs(s.rssis[rx1] - s.rssis[rx2]);
            double ratio = pow(10, -1.0 * diff / (10.0 * alpha));
            if(euclDistance(make_pair(x, y), rxers[closer])/euclDistance(make_pair(x, y), rxers[farther]) <= ratio) {
              ++hits[c][r];
            }
          }
        }
      }
    }
  }
  int max = accumulate(hits.begin(), hits.end(), 0, [&](int cur_max, vector<int>& col) {
      int col_max = *max_element(col.begin(), col.end());
      if (col_max > cur_max) return col_max; else return cur_max;});
  int total = accumulate(hits.begin(), hits.end(), 0, [&](int cur_sum, vector<int>& col) {
      return cur_sum + accumulate(col.begin(), col.end(), 0); });
  
  //Make all of the tiles.
  vector<Tile> result;
  cerr<<"Data for sample at "<<s.x<<", "<<s.y<<'\n';
  for (int col = 0; col < columns; ++col) {
    for (int row = 0; row < rows; ++row) {
      cerr<<tile_w*col<<'\t'<<tile_h*row<<'\t'<<hits[col][row]<<'\n';
      result.push_back(Tile{col, row});
    }
  }
  //Sort the tiles by their use counts.
  sort(result.begin(), result.end(), [&](const Tile& a, const Tile& b) {
      return hits[a.column][a.row] > hits[b.column][b.row];});
  auto I = result.begin();
  double cum_prob = 0.0;
  do {
    cum_prob += (float)hits[I->column][I->row]/total;
    ++I;
  } while (cum_prob < percentile);
  //Erase the result not in the specified percentile.
  result.erase(I, result.end());
  return result;
}

