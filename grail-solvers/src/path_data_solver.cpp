#include "passive_localization_utilities.h"

#define CLICK_INTERVAL 100

using namespace std;

int main(int argc, char *argv[]){

	vector<uint128_t> tx_ids;
	vector<uint128_t> rx_ids;
	vector<uint128_t> sp_ids;
	vector<double> path_x;
	vector<double> path_y;

	int path_num;
	int path_id = 0;
	double x,y;
	int rx_id, tx_id, seq; 
	long long timestamp = 0;
	double rssi = 0;
	time_t time_seconds = 0;

	long long last_time = 0;
	long long start_time, end_time;

	int buffer;
	int file_line;
	string data;
	ifstream infile;

	file_line = 0;
	infile.open("dfp.txt");
	if (!infile){
		cout<<"DfP file open error!\n";
		return 0;
	}

	while(getline(infile, data)) {
		istringstream s(data);
		if(file_line == 0) {
			while(s>>buffer){
				tx_ids.push_back(buffer);
			}
		}
		else if (file_line == 1) {
			while(s>>buffer){
				rx_ids.push_back(buffer);
			}
		}
		else if (file_line == 2) {
			while(s>>buffer){
				sp_ids.push_back(buffer);
			}
		}
		file_line++;
	}
	infile.close();

	// read these path coordinates
	file_line = 0;
	infile.open("path_config.txt");
	if (!infile){
		cout<<"Path_config file open error!"<<endl;
		return 0;
	}

	while(infile>>x>>y){
		path_x.push_back(x);
		path_y.push_back(y);
	}

	infile.close();

	path_num = path_x.size() - 1;

	ofstream myfile;

	// start to extract the path data
	if(argc == 2){
		ifstream infile;
		infile.open(argv[1]);

		if (!infile){
			cout<<"File open error!\n";
			return 0;
		}

		while(getline(infile, data)){			
			istringstream s(data);
			rx_id = 0;
			tx_id = 0;
			timestamp = 0;
			rssi = 0;
			seq = 0;
			s>>rx_id>>timestamp>>tx_id>>seq>>rssi;

			//look for every first click packet
			if(tx_id == sp_ids[0]){
				last_time = timestamp;
				start_time = timestamp;
				vector<double> temp_x;
				vector<double> temp_y;
				vector<uint128_t> temp_rx_id;
				vector<long long> temp_time;
				vector<uint128_t> temp_tx_id;
				vector<double> temp_rssi;
/*
				time_seconds = time(NULL);
				struct tm *time_parts;
				time_parts = localtime (&time_seconds);
				stringstream out;
				out<<asctime(time_parts);
				//cout<<out.str()<<endl;
*/
				//record the data packet for the path
				while(getline(infile, data)){
					istringstream s(data);
					rx_id = 0;
					tx_id = 0;
					timestamp = 0;
					rssi = 0;
					seq = 0;
					s>>rx_id>>timestamp>>tx_id>>seq>>rssi;

					if(tx_id == sp_ids[0] and (timestamp - last_time) >= CLICK_INTERVAL){
						end_time = timestamp;
						string filename = "path_" + to_string(path_id) + ".txt";
						myfile.open(filename.c_str());

						for(int i = 0; i < temp_rx_id.size(); i++){
							temp_x.push_back(path_x[path_id] + (temp_time[i] - start_time) * 
									(path_x[path_id + 1] - path_x[path_id]) / 
									(end_time - start_time)); 
							temp_y.push_back(path_y[path_id] + (temp_time[i] - start_time) * 
									(path_y[path_id + 1] - path_y[path_id]) / 
									(end_time - start_time));
					       		myfile<<temp_rx_id[i]<<"\t"<<temp_time[i]<<"\t"
								<<temp_tx_id[i]<<"\t"<<temp_rssi[i]<<"\t"
								<<temp_x[i]<<"\t"<<temp_y[i]<<endl;
						}
						myfile.close();
						path_id++;
						break;
					}

					else if(getIndex(tx_ids, tx_id) != -1 and getIndex(rx_ids, rx_id) != -1){
						temp_rx_id.push_back(rx_id);
						temp_time.push_back(timestamp);
						temp_tx_id.push_back(tx_id);
						temp_rssi.push_back(rssi);
						//cerr<<timestamp<<endl;
					}

					if(path_id == path_num){
						break;
					}
				}						
			}
		}	

		infile.close();
		myfile.close();
	}	
}
