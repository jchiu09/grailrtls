#include <localize_common.hpp>

#include <cmath>

double euclDistance(pair<double, double> a, pair<double, double> b) {
  return sqrt( pow(a.first - b.first, 2) + pow(a.second - b.second, 2.0));
}
