/******************************************************************************
 * Helper library for writing the solver client interface to the grailV3
 * server.
 *****************************************************************************/

#include "grailV3_solver_client.hpp"

#include <iostream>
#include <iterator>
#include <unistd.h>
#include <string>
#include <functional>
#include <algorithm>
#include <mutex>
#include <thread>
#include <stdexcept>

#include "world_server_protocol.hpp"
#include "aggregator_solver_protocol.hpp"
#include "solver_distributor_protocol.hpp"
#include "sample_data.hpp"
#include "netbuffer.hpp"

#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "simple_sockets.hpp"
#include "grail_sock_server.hpp"

//TODO In the future C++11 support for regex should be used over these POSIX
//regex c headers.
#include <sys/types.h>
#include <regex.h>

#include <iostream>

using namespace aggregator_solver;
using namespace solver_distributor;
using std::map;
using std::get;

using world_server::WorldData;

using world_model::URI;
using world_model::grail_time;

struct Debug {
  bool on;
};

template<typename T>
Debug& operator<<(Debug& dbg, T arg) {
  if (dbg.on) {
    std::cout<<arg;
  }
  return dbg;
}


bool ClientWorldModel::reconnect() {
  _connected = false;
  if (s) {
    std::cout<<"Connected to the GRAIL world model.\n";
  } else {
    //Otherwise try to make a new connection
    ClientSocket s2(AF_INET, SOCK_STREAM, 0, port, ip);
    if (not s2) {
      std::cerr<<"Failed to connect to the GRAIL world model.\n";
      return false;
    }
    else {
      s = std::move(s2);
    }
  }

  //Try to get the handshake message
  {
    std::vector<unsigned char> handshake = world_model::client::makeHandshakeMsg();

    //Send the handshake message
    s.send(handshake);
    std::vector<unsigned char> raw_message(handshake.size());
    size_t length = s.receive(raw_message);

    //Check if the handshake message failed
    if (not (length == handshake.size() and
          std::equal(handshake.begin(), handshake.end(), raw_message.begin()) )) {
      std::cerr<<"Failure during client handshake with world model.\n";
      return false;
    }
  }

  _connected = true;

  ss.previous_unfinished.clear();

  return true;
}

ClientWorldModel::ClientWorldModel(std::string ip, uint16_t port) : s(AF_INET, SOCK_STREAM, 0, port, ip), ss(s) {
  //Store these values so that we can reconnect later
  this->ip = ip;
  this->port = port;

  streaming = false;
  interrupted = false;

  reconnect();
}

bool ClientWorldModel::connected() {
  return _connected;
}

std::tuple<ClientWorldModel::world_state, uint32_t> ClientWorldModel::getRequestResponse() {
  using world_model::client::MessageID;
  //Now get the snapshot response
  world_state ws;
  uint32_t ticket;
  while (not streaming) {
    world_model::Buffer in_buff = ss.getNextMessage(interrupted);
    if (in_buff.size() < 5) {
      std::cerr<<"Received broken message from client world model.\n";
      break;
    }
    MessageID message_type = (MessageID)in_buff[4];
    if (message_type == MessageID::data_response) {
      world_model::AliasedWorldData awd;
      std::tie(awd, ticket) = world_model::client::decodeDataMessage(in_buff);
      //Use the types and origins previously sent from the server to
      //decode this aliased world data to world data
      for (auto attr = awd.attributes.begin(); attr != awd.attributes.end(); ++attr) {
        if (types.end() != types.find(attr->name_alias) and
            origins.end() != origins.find(attr->origin_alias)) {
          world_model::Attribute a{types[attr->name_alias], attr->creation_date,
            attr->expiration_date, origins[attr->origin_alias], attr->data};
          ws[awd.object_uri].push_back(a);
        }
      }
    }
    else if (message_type == MessageID::keep_alive) {
      //Do nothing here, just wait for a meaningful message
    }
    else if (message_type == MessageID::attribute_alias) {
      std::vector<world_model::client::AliasType> aliases = world_model::client::decodeAttrAliasMsg(in_buff);
      for (auto at = aliases.begin(); at != aliases.end(); ++at) {
        types[at->alias] = at->type;
      }
    }
    else if (message_type == MessageID::origin_alias) {
      std::vector<world_model::client::AliasType> aliases = world_model::client::decodeOriginAliasMsg(in_buff);
      for (auto at = aliases.begin(); at != aliases.end(); ++at) {
        origins[at->alias] = at->type;
      }
    }
    else if (message_type == MessageID::request_complete) {
      break;
    }
  }
  return std::make_tuple(ws, ticket);
}

//Return the data synchronously
std::tuple<ClientWorldModel::world_state, uint32_t> ClientWorldModel::getCurrentSnapshot(URI uri, std::vector<URI>& attributes, uint32_t ticket) {
  world_model::client::Request r{uri, attributes, 0, 0};
  world_model::Buffer out_buff = makeSnapshotRequest(r, ticket);
  s.send(out_buff);
  return getRequestResponse();
}

std::tuple<ClientWorldModel::world_state, uint32_t> ClientWorldModel::getHistoricSnapshot(URI uri, std::vector<URI>& attributes, grail_time start, grail_time stop, uint32_t ticket) {
  world_model::client::Request r{uri, attributes, start, stop};
  world_model::Buffer out_buff = makeSnapshotRequest(r, ticket);
  s.send(out_buff);
  return getRequestResponse();
}

//Return the data synchronously
std::tuple<ClientWorldModel::world_state, uint32_t> ClientWorldModel::getRange(URI uri, std::vector<URI>& attributes, grail_time start, grail_time stop, uint32_t ticket) {
  world_model::client::Request r{uri, attributes, start, stop};
  world_model::Buffer out_buff = makeRangeRequest(r, ticket);
  s.send(out_buff);
  return getRequestResponse();
}

void ClientWorldModel::streamData(std::function<void (ClientWorldModel::world_state&, uint32_t)> solutionCallback) {
  using world_model::client::MessageID;
  while (not interrupted) {
    world_model::Buffer in_buff = ss.getNextMessage(interrupted);
    if (interrupted) { break;}
    if (in_buff.size() < 5) {
      std::cerr<<"Received broken message from client world model.\n";
      break;
    }
    MessageID message_type = (MessageID)in_buff[4];
    if (message_type == MessageID::data_response) {
      world_state ws;
      world_model::AliasedWorldData awd;
      uint32_t ticket;
      std::tie(awd, ticket) = world_model::client::decodeDataMessage(in_buff);
      //Use the types and origins previously sent from the server to
      //decode this aliased world data to world data
      for (auto attr = awd.attributes.begin(); attr != awd.attributes.end(); ++attr) {
        if (types.end() != types.find(attr->name_alias) and
            origins.end() != origins.find(attr->origin_alias)) {
          world_model::Attribute a{types[attr->name_alias], attr->creation_date,
            attr->expiration_date, origins[attr->origin_alias], attr->data};
          ws[awd.object_uri].push_back(a);
        }
      }
      solutionCallback(ws, ticket);
    }
    else if (message_type == MessageID::keep_alive) {
      //Do nothing here, just wait for a meaningful message
    }
    else if (message_type == MessageID::attribute_alias) {
      std::vector<world_model::client::AliasType> aliases = world_model::client::decodeAttrAliasMsg(in_buff);
      for (auto at = aliases.begin(); at != aliases.end(); ++at) {
        types[at->alias] = at->type;
      }
    }
    else if (message_type == MessageID::origin_alias) {
      std::vector<world_model::client::AliasType> aliases = world_model::client::decodeOriginAliasMsg(in_buff);
      for (auto at = aliases.begin(); at != aliases.end(); ++at) {
        origins[at->alias] = at->type;
      }
    }
    //Don't care about other messages
  }
}

//Return data through a provided callback in a separate thread.
void ClientWorldModel::setupAsynchronousDataStream(URI uri, std::vector<URI>& attributes, grail_time interval,
    std::function<void (ClientWorldModel::world_state&, uint32_t)> solutionCallback, uint32_t ticket) {
  //If we were already streaming just send another stream request.
  world_model::client::Request r{uri, attributes, 0, interval};
  world_model::Buffer out_buff = makeStreamRequest(r, ticket);
  if (streaming) {
    s.send(out_buff);
  }
  else {
    streaming = true;
    s.send(out_buff);
    stream_thread = std::thread(std::mem_fun(&ClientWorldModel::streamData), this, solutionCallback);
  }
}

//Sets up a data stream request but relies upon the solver calling getSynchronousStreamUpdate
//to get data.
void ClientWorldModel::setupSynchronousDataStream(URI uri, std::vector<URI>& attributes, grail_time interval, uint32_t ticket) {
  //Send out a stream request.
  world_model::client::Request r{uri, attributes, 0, interval};
  world_model::Buffer out_buff = makeStreamRequest(r, ticket);
  s.send(out_buff);
}

//Sets up a data stream request but relies upon the solver calling getSynchronousStreamUpdate
//to get data. This version sets up a callback for this request.
void ClientWorldModel::setupSynchronousDataStream(URI uri, std::vector<URI>& attributes, grail_time interval,
    std::function<void (ClientWorldModel::world_state&, uint32_t)> callback, uint32_t ticket) {
  //Remember that this request has a callback
  //If this was the same number as a previous request make sure it
  //isn't marked as cancelled.
  cancelled.erase(ticket);
  callbacks[ticket] = callback;
  //Send out a stream request.
  world_model::client::Request r{uri, attributes, 0, interval};
  world_model::Buffer out_buff = makeStreamRequest(r, ticket);
  s.send(out_buff);
}

//Gets any new updates. If this is not called often enough the solver could time out from
//the world model.
std::tuple<ClientWorldModel::world_state, uint32_t> ClientWorldModel::getSynchronousStreamUpdate() {
  using world_model::client::MessageID;
  while (true) {
    world_model::Buffer in_buff = ss.getNextMessage(interrupted);
    if (in_buff.size() < 5) {
      std::cerr<<"Received broken message from client world model.\n";
      break;
    }
    MessageID message_type = (MessageID)in_buff[4];
    if (message_type == MessageID::data_response) {
      world_state ws;
      world_model::AliasedWorldData awd;
      uint32_t ticket;
      std::tie(awd, ticket) = world_model::client::decodeDataMessage(in_buff);

      //Don't process this request if it was cancelled.
      if (cancelled.count(ticket) != 0) {
        //This message was sent before the cancel request reached the server.
        continue;
      }

      std::vector<world_model::Attribute>& new_attrs = ws[awd.object_uri];
      //Use the types and origins previously sent from the server to
      //decode this aliased world data to world data
      for (auto attr = awd.attributes.begin(); attr != awd.attributes.end(); ++attr) {
        if (types.end() != types.find(attr->name_alias) and
            origins.end() != origins.find(attr->origin_alias)) {
          world_model::Attribute a{types[attr->name_alias], attr->creation_date,
            attr->expiration_date, origins[attr->origin_alias], attr->data};
          new_attrs.push_back(a);
        }
      }
      //Provide complete world states to the user. The world model does not
      //resend already sent information so this result does not have all of
      //the currently requested attributes of a URI.
      world_state& prev_state = request_states[ticket];
      std::vector<world_model::Attribute>& prev_attrs = prev_state[awd.object_uri];
      //Go through the previous attributes to see if they have been updated
      for (auto attr = prev_attrs.begin(); attr != prev_attrs.end(); ++attr) {
        //See if this is an update to an already known attribute
        auto same_attribute = [&](world_model::Attribute& new_attr) {
          return (new_attr.name == attr->name) and (new_attr.origin == attr->origin);};
        auto slot = std::find_if(new_attrs.begin(), new_attrs.end(), same_attribute);
        //Update this attribute if it was changed
        if (slot != new_attrs.end()) {
          *attr = *slot;
        }
        //Otherwise return this old entry to the user since it is still the most recent
        else {
          new_attrs.push_back(*attr);
        }
      }
      //Update the previou attribute vector.
      prev_attrs = new_attrs;

      //If this data has a callback specified then send it to the callback
      //rather than returning it.
      auto cb_iterator = callbacks.find(ticket);
      if (callbacks.end() != cb_iterator) {
        cb_iterator->second(ws, ticket);
      }
      else {
        return std::make_tuple(ws, ticket);
      }
    }
    else if (message_type == MessageID::keep_alive) {
      //Do nothing here, just wait for a meaningful message
    }
    else if (message_type == MessageID::attribute_alias) {
      std::vector<world_model::client::AliasType> aliases = world_model::client::decodeAttrAliasMsg(in_buff);
      for (auto at = aliases.begin(); at != aliases.end(); ++at) {
        types[at->alias] = at->type;
      }
    }
    else if (message_type == MessageID::origin_alias) {
      std::vector<world_model::client::AliasType> aliases = world_model::client::decodeOriginAliasMsg(in_buff);
      for (auto at = aliases.begin(); at != aliases.end(); ++at) {
        origins[at->alias] = at->type;
      }
    }
    //Delete callbacks and saved states once a request completes
    else if (message_type == MessageID::request_complete) {
      uint32_t ticket = world_model::client::decodeRequestComplete(in_buff);
      auto cb_iterator = callbacks.find(ticket);
      if (callbacks.end() != cb_iterator) {
        callbacks.erase(cb_iterator);
      }
      auto rq_iterator = request_states.find(ticket);
      if (request_states.end() != rq_iterator) {
        request_states.erase(rq_iterator);
      }
    }
    //Don't care about other messages
  }
  return std::make_tuple(world_state(), 0);
}

ClientWorldModel::~ClientWorldModel() {
  //Stop the streaming thread if it is running
  if (streaming) {
    streaming = false;
    interrupted = true;
    try {
      stream_thread.join();
    }
    catch (std::runtime_error err) {
    }
  }
}

//Cancel request message
void ClientWorldModel::cancelRequest(uint32_t ticket) {
  //If this ticket request has a callback indicate that is is expired.
  cancelled.insert(ticket);
  std::cerr<<"Client remembers ticket "<<ticket<<" is cancelled.\n";
  auto cb = callbacks.find(ticket);
  if (cb != callbacks.end()) {
    callbacks.erase(cb);
  }
  s.send(world_model::client::makeCancelRequest(ticket));
}

//Search for URIs
std::vector<URI> ClientWorldModel::searchURIs(URI expression) {
  using world_model::client::MessageID;
  while (not streaming) {
    s.send(world_model::client::makeURISearch(expression));
    world_model::Buffer in_buff = ss.getNextMessage(interrupted);
    if (in_buff.size() < 5) {
      std::cerr<<"Received broken message from client world model.\n";
      std::vector<URI> nothing;
      return nothing;
    }
    MessageID message_type = (MessageID)in_buff[4];
    if (message_type == MessageID::uri_response) {
      return world_model::client::decodeURISearchResponse(in_buff);
    }
    //Otherwise ignore this message.
    //It should be a keep_alive unless we are streaming, in which case
    //execution should not have reached this point.
  }
  //If we are streaming then don't allow this message
  std::vector<URI> nothing;
  return nothing;
}

/*******************************************************************************
 * End ClientWorldModel
 ******************************************************************************/

/*******************************************************************************
 * Begin SolverWorldModel
 ******************************************************************************/

//Send a handshake and a type declaration message.
bool SolverWorldModel::reconnect() {
  //Reset connection status and set to true after the handshake.
  _connected = false;
  if (s) {
    std::cout<<"Connected to the GRAIL world model.\n";
  } else {
    //Otherwise try to make a new connection
    ClientSocket s2(AF_INET, SOCK_STREAM, 0, port, ip);
    if (not s2) {
      std::cerr<<"Failed to connect to the GRAIL world model.\n";
      return false;
    }
    else {
      s = std::move(s2);
    }
  }

  //Try to get the handshake message
  {
    std::vector<unsigned char> handshake = world_model::solver::makeHandshakeMsg();

    //Send the handshake message
    s.send(handshake);
    std::vector<unsigned char> raw_message(handshake.size());
    size_t length = s.receive(raw_message);

    //Check if the handshake message failed
    if (not (length == handshake.size() and
          std::equal(handshake.begin(), handshake.end(), raw_message.begin()) )) {
      std::cerr<<"Failure during solver handshake with world model.\n";
      return false;
    }
  }

  //Send the type announcement message
  try {
    s.send(world_model::solver::makeTypeAnnounceMsg(types, origin));
  }
  catch (std::runtime_error err) {
    std::cerr<<"Problem sending type announce message: "<<err.what()<<'\n';
    return false;
  }

  ss.previous_unfinished.clear();
  running = true;
  interrupted = false;
  //Start the transient status tracking thread
  transient_tracker = std::thread(std::mem_fun(&SolverWorldModel::trackTransients), this);
  _connected = true;
  return true;
}

static std::string toString(const std::u16string& str) {
  return std::string(str.begin(), str.end());
}

void SolverWorldModel::trackTransients() {
  using world_model::solver::MessageID;
  while (not interrupted) {
    std::vector<unsigned char> in_buff = ss.getNextMessage(interrupted);
    if (5 <= in_buff.size()) {
      MessageID message_type = (MessageID)in_buff[4];
      if (message_type == MessageID::start_transient) {
        std::vector<std::tuple<uint32_t, std::vector<std::u16string>>> trans =
          world_model::solver::decodeStartTransient(in_buff);
        std::unique_lock<std::mutex> lck(trans_mutex);
        for (auto I = trans.begin(); I != trans.end(); ++I) {
          std::cerr<<"Transient "<<std::get<0>(*I)<<" has "<<get<1>(*I).size()<<" URI requests.\n";
          for (auto request = get<1>(*I).begin(); request != get<1>(*I).end(); ++request) {
            //Store the regex pattern sent by the world model.
            std::cerr<<"Enabling transient: "<<std::get<0>(*I)<<" with string "<<toString(*request)<<'\n';

            TransientArgs ta;
            ta.request = *request;
            int err = regcomp(&ta.exp, toString(ta.request).c_str(), REG_EXTENDED);
            if (0 != err) {
              ta.valid = false;
              std::cerr<<"Error compiling regular expression "<<toString(ta.request)<<" in transient request to solver client.\n";
            }
            else {
              ta.valid = true;
            }
            transient_on[std::get<0>(*I)].insert(ta);
          }
        }
      }
      else if (message_type == MessageID::stop_transient) {
        std::vector<std::tuple<uint32_t, std::vector<std::u16string>>> trans =
          world_model::solver::decodeStopTransient(in_buff);
        std::unique_lock<std::mutex> lck(trans_mutex);
        for (auto I = trans.begin(); I != trans.end(); ++I) {
          for (auto request = get<1>(*I).begin(); request != get<1>(*I).end(); ++request) {
            //Remove the regex that was sent by the world model
            uint32_t attr_name = std::get<0>(*I);
            std::cerr<<"Disabling transient: "<<attr_name<<" with request "<<toString(*request)<<'\n';
            if (transient_on.end() != transient_on.find(attr_name)) {
              std::multiset<TransientArgs>& uri_set = transient_on[attr_name];
              auto J = std::find_if(uri_set.begin(), uri_set.end(), [&](const TransientArgs& ta) {
                  return ta.request == *request;});
              if (J != uri_set.end()) {
                TransientArgs ta = *J;
                if (ta.valid) {
                  regfree(&ta.exp);
                }
                uri_set.erase(J);
              }
            }
          }
        }
      }
    }
    else {
      std::cerr<<"Got an invalid sized message (size = "<<in_buff.size()<<'\n';
    }
  }
}

SolverWorldModel::SolverWorldModel(std::string ip, uint16_t port, std::vector<std::pair<u16string, bool>>& types, std::u16string origin) : s(AF_INET, SOCK_STREAM, 0, port, ip), ss(s) {
  this->origin = origin;
  //Store the alias types that this solver will use
  for (auto I = types.begin(); I != types.end(); ++I) {
    world_model::solver::AliasType at{(uint32_t)(this->types.size()+1), I->first, I->second};
    this->types.push_back(at);
    aliases[at.type] = at.alias;
    if (I->second) {
      if (transient_on.end() == transient_on.find(at.alias)) {
        transient_on[at.alias] = std::multiset<TransientArgs>();
      }
    }
  }
  //Store these values so that we can reconnect later
  this->ip = ip;
  this->port = port;

  reconnect();
}

SolverWorldModel::~SolverWorldModel() {
  if (running) {
    interrupted = true;
    transient_tracker.join();
  }
}

void SolverWorldModel::addTypes(std::vector<std::pair<u16string, bool>>& new_types) {
  //Store the alias types that this solver will use
	std::vector<world_model::solver::AliasType> new_aliases;
  for (auto I = new_types.begin(); I != new_types.end(); ++I) {
    world_model::solver::AliasType at{(uint32_t)(this->types.size()+1), I->first, I->second};
    this->types.push_back(at);
    aliases[at.type] = at.alias;
    if (I->second) {
      if (transient_on.end() == transient_on.find(at.alias)) {
        transient_on[at.alias] = std::multiset<TransientArgs>();
      }
    }
		new_aliases.push_back(at);
  }
  //Update the world model with a new type announcement message
  try {
    s.send(world_model::solver::makeTypeAnnounceMsg(new_aliases, origin));
  }
  catch (std::runtime_error err) {
    std::cerr<<"Problem sending type announce message: "<<err.what()<<'\n';
  }
}

bool SolverWorldModel::connected() {
  return _connected;
}

void SolverWorldModel::sendData(std::vector<Solution>& solution, bool create_uris) {
  using world_model::solver::SolutionData;
  std::vector<SolutionData> sds;
  for (auto I = solution.begin(); I != solution.end(); ++I) {
    std::unique_lock<std::mutex> lck(trans_mutex);
    if (aliases.end() != aliases.find(I->type)) {
      uint32_t alias = aliases[I->type];
      //TODO Let the user check this themselves, it should be up to them
      //whether or not to send data
      //Send if this is not a transient or it is a transient but is requested
      if (transient_on.end() == transient_on.find(alias)) {
        SolutionData sd{alias, I->time, I->target, I->data};
        sds.push_back(sd);
      }
      else {
        //Find if any patterns match this information
        if (std::any_of(transient_on[alias].begin(), transient_on[alias].end(),
              [&](const TransientArgs& ta) {
              if (not ta.valid) { return false;}
              regmatch_t pmatch;
              int match = regexec(&ta.exp, toString(I->target).c_str(), 1, &pmatch, 0);
              return (0 == match and 0 == pmatch.rm_so and I->target.size() == pmatch.rm_eo); })) {
          SolutionData sd{alias, I->time, I->target, I->data};
          sds.push_back(sd);
        }
      }
    }
  }

  //Allow sending an empty message (if all of the solutions are unrequested
  //transients solutions) to serve as a keep alive.
  s.send(world_model::solver::makeSolutionMsg(create_uris, sds));
}

void SolverWorldModel::createURI(world_model::URI uri, world_model::grail_time created) {
  s.send(world_model::solver::makeCreateURI(uri, created, origin));
}

void SolverWorldModel::expireURI(world_model::URI uri, world_model::grail_time expires) {
  s.send(world_model::solver::makeExpireURI(uri, expires, origin));
}

void SolverWorldModel::deleteURI(world_model::URI uri) {
  s.send(world_model::solver::makeDeleteURI(uri, origin));
}

void SolverWorldModel::expireURIAttribute(world_model::URI uri, std::u16string name, world_model::grail_time expires) {
  s.send(world_model::solver::makeExpireAttribute(uri, name, origin, expires));
}

void SolverWorldModel::deleteURIAttribute(world_model::URI uri, std::u16string name) {
  s.send(world_model::solver::makeDeleteAttribute(uri, name, origin));
}

/*******************************************************************************
 * End SolverWorldModel
 ******************************************************************************/

void grailAggregatorThread(uint32_t port, std::string ip, std::vector<Subscription> subscriptions,
    std::function<void (SampleData&)> packCallback, std::mutex& callback_mutex, bool& interrupted) {

  Debug debug{false};

  while (not interrupted) {
    try {
      ClientSocket cs(AF_INET, SOCK_STREAM, 0, port, ip);
      if (cs) {
        std::cerr<<"Connected to the GRAIL aggregator.\n";

        //Try to get the handshake message
        {
          std::vector<unsigned char> handshake = aggregator_solver::makeHandshakeMsg();

          //Send the handshake message
          cs.send(handshake);
          std::vector<unsigned char> raw_message(handshake.size());
          size_t length = cs.receive(raw_message);

          debug<<"Received handshake message of length "<<length<<'\n';
          debug<<"Expecting handshake of length "<<handshake.size()<<'\n';

          //Check if the handshake message failed
          if (not (length == handshake.size() and
                std::equal(handshake.begin(), handshake.end(), raw_message.begin()) )) {
            std::cerr<<"Failure during handshake with aggregator.\n";
            return;
          }
        }

        //Make a grail socket server to simplify packet handling here
        GRAILSockServer s(cs);

        //Send requests that the caller specified
        for (auto sub = subscriptions.begin(); sub != subscriptions.end(); ++sub) {
          //Send a request message
          std::vector<unsigned char> req_buff = makeSubscribeReqMsg(*sub);
          cs.send(req_buff);
        }

        while (not interrupted) {
          //Now receive incoming messages from the server.
          std::vector<unsigned char> raw_message = s.getNextMessage(interrupted);

          //If the message is long enough to actually be a message check its type
          if ( raw_message.size() > 4 ) {
            //Handle the message according to its message type.
            if ( subscription_response == raw_message[4] ) {
              debug<<"Got subscription response!\n";
              Subscription sub = decodeSubscribeMsg(raw_message, raw_message.size());
              //TODO check for changes in the subscription made by the server
            } else if ( server_sample == raw_message[4] ) {
              debug<<"Got sample!\n";
              SampleData sample = decodeSampleMsg(raw_message, raw_message.size());
              if ( sample.valid ) {
                {
                  std::unique_lock<std::mutex> lck(callback_mutex);
                  packCallback(sample);
                }
              } else {
                debug<<"But the sample wasn't valid!\n";
              }
            }
          }
        }
      }
    } catch (std::exception& err) {
      std::cerr<<"Error in grail aggregator connection: "<<err.what()<<'\n';
    }

    //Sleep for one second, then try connecting to the server again.
    sleep(1);
  }
}


/**
 * Connect to the GRAIL servers at the specified ips and ports and
 * subscribe to any regions with a subscription in the provided
 * subscriptions vector.
 */
void grailAggregatorConnect(const std::vector<NetTarget>& servers,
    std::vector<Subscription> subscriptions, std::function<void (SampleData&)> packCallback) {

  std::mutex callback_mutex;
  std::vector<std::thread> server_threads;

  bool interrupted = false;
  for (auto server = servers.begin(); server != servers.end(); ++server) {
    try {
      server_threads.push_back(std::thread(grailAggregatorThread, server->port,
            server->ip, subscriptions, packCallback, std::ref(callback_mutex), std::ref(interrupted)));
    }
    catch (std::system_error& err) {
      std::cerr<<"Error in grail aggregator connection: "<<err.code().message()<<
        " while connecting to "<<server->ip<<':'<<server->port<<'\n';
    }
    catch (std::runtime_error& err) {
      std::cerr<<"Error in grail aggregator connection: "<<err.what()<<
        " while connecting to "<<server->ip<<':'<<server->port<<'\n';
    }
  }

  while (1) {
    //TODO check to see if all connections are closed and return when they are.
    usleep(1000);
  }
}

SolverAggregator::SolverAggregator(const std::vector<NetTarget>& servers, std::function<void (SampleData&)> packCallback) {
  this->packCallback = packCallback;
  this->servers = servers;
  interrupted = false;
  //Don't establish connections until rules are provided from a call to update rules.
}

SolverAggregator::~SolverAggregator() {
  //Interrupted the aggregator connections and join the threads
  interrupted = true;
  for (auto T = server_threads.begin(); T != server_threads.end(); ++T) {
    try {
      T->join();
    }
    catch (std::runtime_error err) {
      //Thread is not joinable
    }
  }
  server_threads.clear();
}

void SolverAggregator::Disconnect() {
  //Interrupted the aggregator connections and join the threads
  interrupted = true;
  for (auto T = server_threads.begin(); T != server_threads.end(); ++T) {
    try {
      T->join();
    }
    catch (std::runtime_error err) {
      //Thread is not joinable
    }
  }
  server_threads.clear();
}

void SolverAggregator::updateRules(aggregator_solver::Subscription subscription) {
  {
    std::unique_lock<std::mutex> lck(sub_mutex);
    this->subscription = subscription;
  }
  //Reconnect to all of the aggregators

  //Interrupted the aggregator connections, join the threads, and then reconnect
  interrupted = true;
  for (auto T = server_threads.begin(); T != server_threads.end(); ++T) {
    try {
      T->join();
    }
    catch (std::runtime_error err) {
      //Thread is not joinable
    }
  }
  server_threads.clear();

  interrupted = false;
  for (auto server = servers.begin(); server != servers.end(); ++server) {
    try {
      std::vector<aggregator_solver::Subscription> subscriptions{subscription};
      server_threads.push_back(std::thread(grailAggregatorThread, server->port,
            server->ip, subscriptions, packCallback, std::ref(callback_mutex), std::ref(interrupted)));
    }
    catch (std::system_error& err) {
      std::cerr<<"Error in grail aggregator connection: "<<err.code().message()<<
        " while connecting to "<<server->ip<<':'<<server->port<<'\n';
    }
    catch (std::runtime_error& err) {
      std::cerr<<"Error in grail aggregator connection: "<<err.what()<<
        " while connecting to "<<server->ip<<':'<<server->port<<'\n';
    }
  }
}

void grailDistributorConnect(std::string ip, uint16_t port,
    std::function<bool (const SolutionType&)> solutionInterest,
    std::function<void (solver_distributor::SolverDataMsg&)> solutionCallback,
    time_t time_begin, time_t time_end) {

  Debug debug{false};

  try {

    ClientSocket s(AF_INET, SOCK_STREAM, 0, port, ip);
    if (s) {
      std::cout<<"Connected to the GRAIL distributor.\n";
    } else {
      std::cerr<<"Failed to connect to the GRAIL distributor.\n";
      return;
    }

    //Try to get the handshake message
    {
      std::vector<unsigned char> handshake = distributor_application::makeHandshakeMsg();

      //Send the handshake message
      s.send(handshake);
      std::vector<unsigned char> raw_message(handshake.size());
      size_t length = s.receive(raw_message);

      debug<<"Received handshake message of length "<<length<<'\n';
      debug<<"Expecting handshake of length "<<handshake.size()<<'\n';

      //Check if the handshake message failed
      if (not (length == handshake.size() and
               std::equal(handshake.begin(), handshake.end(), raw_message.begin()) )) {
        std::cerr<<"Failure during handshake.\n";
        return;
      }
    }

    //Keep receiving new messages until this turns false.
    bool running = true;

    //Hold a buffer for a unfinished piece sent in the last packet that
    //needs a new TCP packet to be processed completely.
    std::vector<unsigned char> previous_unfinished;

    while (running) {
      //Now receive incoming messages from the server.
      //TODO instead of 10000 this needs to be the maximum TCP message size.
      std::vector<unsigned char> raw_messages(10000);
      size_t length = s.receive(raw_messages);

      debug<<"Got message of length "<<length<<'\n';

      if ( 0 == length ) {
        debug<<"Error receiving message or the connection was closed.\n";
        running = false;
      } else {
        //If there is left over data from the last TCP message reinsert it into
        //the beginning of this message.
        if (previous_unfinished.size() > 0) {
          length += previous_unfinished.size();
          raw_messages.insert(raw_messages.begin(), previous_unfinished.begin(), previous_unfinished.end());
        }

        debug<<"Processing packet!\n";

        //TODO The message must be cut into pieces since multiple packets
        //could have been merged. Export the readPrimitive function from
        //server_solver_protocol and use that to read each length and pull
        //that length field and its corresponding packet from the buffer,
        //then feed them into this next bit of code
        //Make sure that there is room for the the size of the message (4 byte int)
        //and that there is room for the submessage
        size_t begin = 0;
        while ( begin < length - 4 ) {
          //Now make sure that there is room for this piece of message
          uint32_t piece_length = readPrimitive<uint32_t>(raw_messages, begin) + 4;
          debug<<"Processing piece of size: "<<piece_length<<
            " starting at index "<<begin<<"\n";
          if (begin + piece_length > length) {
            debug<<"Waiting for next TCP message to continue processing.\n";
            break;
          }
          //Pull out the message
          std::vector<unsigned char> raw_message(raw_messages.begin()+begin,
              raw_messages.begin()+begin+piece_length);
          //Set begin to the index of the next message
          begin += piece_length;

          //If the message is long enough to actually be a message check its type
          if ( piece_length > 4 ) {
            //Handle the message according to its message type.
            if ( distributor_application::solution_publication == raw_message[4] ) {
              debug<<"Received a solution publication message.\n";
              //TODO FIXME this message should have its own function
              raw_message[4] = type_specification;
              std::vector<SolutionType> types = decodeTypeSpecMsg(raw_message, piece_length);
              debug<<"Offered "<<types.size()<<" solution types.\n";

              //The types to request. Query the typesRequested function for each type.
              SolutionRequest types_requested;
              types_requested.begin = time_begin;
              types_requested.end = time_end;
              for (auto sol_type = types.begin(); sol_type != types.end(); ++sol_type ) {
                if (solutionInterest(*sol_type)) {
                  //Add this to the list of requests
                  types_requested.aliases.push_back(sol_type->type_alias);
                }
              }
              //Make a solution type request message and send it to the distributor
              std::vector<unsigned char> req_buff = makeTypeReqMsg(types_requested);
              s.send(req_buff);
            } else if ( distributor_application::solution_sample == raw_message[4] ) {
              debug<<"Got a solution!\n";
              //TODO FIXME There should be a dedicated function call for this message
              //rather than piggy-backing on the decodeSolutionMsg function.
              raw_message[4] = solver_data;
              SolverDataMsg data = decodeSolutionMsg(raw_message, piece_length);
              //TODO check for changes in the subscription made by the server
              solutionCallback(data);
            }
          } else {
            debug<<"Received message incorrect size field: "<<piece_length<<".\n";
            //Don't try to process any more of what was received in the buffer after
            //an incorrect message.
            begin = length;
            break;
          }
        }
        //If we are in the middle of handling a piece of data store what
        //was received so far so that processing can complete with the
        //next TCP packet.
        if (begin < length) {
          previous_unfinished = std::vector<unsigned char>(raw_messages.begin()+begin, raw_messages.end());
        } else {
          previous_unfinished = std::vector<unsigned char>(0);
        }
      }
    }
  } catch (std::exception& err) {
    std::cerr<<"Error in grail distributor connection: "<<err.what()<<'\n';
    return;
  }
}


