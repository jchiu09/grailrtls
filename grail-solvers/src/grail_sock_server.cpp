/*******************************************************************************
 * grail_sock_server.{cpp,hpp} Files that simplify running a grial server.
 ******************************************************************************/

#include "grail_sock_server.hpp"

#include <stdexcept>
#include <vector>
#include <cstring>
#include "simple_sockets.hpp"

#include "netbuffer.hpp"

GRAILSockServer::GRAILSockServer(ClientSocket& s) : sock(s) {
  previous_unfinished = std::vector<unsigned char>(0);
}

bool GRAILSockServer::messageAvailable(bool& interrupted) {
  //Try to process data from the previously unfinished buffer, but
  //if there is not enough data available receive from the network.
  //There must be at least 4 bytes to try to process a packet and
  //the size of a piece is stored in its first four bytes.
  uint32_t piece_length = 0;
  if (previous_unfinished.size() >= 4) {
    piece_length = readPrimitive<uint32_t>(previous_unfinished, 0) + 4;
  }
  if (not interrupted and 
         (previous_unfinished.size() < 4 or
          previous_unfinished.size() < piece_length)) {

    //Receive a message from the network to get more data.
    std::vector<unsigned char> raw_messages(10000);
    ssize_t length = sock.receive(raw_messages);
    if ( -1 == length ) {
      //Check for a nonblocking socket
      if (EAGAIN == errno or
          EWOULDBLOCK == errno) {
        //No message available
        return false;
      }
      else {
        //Check the error value to give a more explicit error message.
        std::string err_str(strerror(errno));
        throw std::runtime_error("Error receiving message or the connection was closed: "+err_str);
      }
    }
    else if (0 == length) {
      //0 length indicates end of file (a closed connection)
      throw std::runtime_error("Connection was closed.");
    }
    //Append the message data into the previous_unfinished buffer.
    previous_unfinished.insert(previous_unfinished.end(),
        raw_messages.begin(), raw_messages.begin()+length);

    //Read in the piece length and continue receiving packets until at least
    //that much data has been received. The length bytes themselves take 4 bytes
    piece_length = readPrimitive<uint32_t>(previous_unfinished, 0) + 4;
  }

  //Return true if there is enough data to form a packet.
  piece_length = readPrimitive<uint32_t>(previous_unfinished, 0) + 4;
  return piece_length <= previous_unfinished.size();
}

std::vector<unsigned char> GRAILSockServer::getNextMessage(bool& interrupted) {
  //Try to process data from the previously unfinished buffer, but
  //if there is not enough data available receive from the network.
  //There must be at least 4 bytes to try to process a packet and
  //the size of a piece is stored in its first four bytes.
  uint32_t piece_length = 0;
  if (previous_unfinished.size() >= 4) {
    piece_length = readPrimitive<uint32_t>(previous_unfinished, 0) + 4;
    //std::cerr<<"Getting piece with length "<<piece_length<<'\n';
  }
  //Get the next packet - keep going while the size can't be read or the
  //size was read but the buffer does not have the whole packet
  while (not interrupted and 
         (previous_unfinished.size() < 4 or
          previous_unfinished.size() < piece_length)) {

    //Receive a message from the network to get more data.
    std::vector<unsigned char> raw_messages(10000);
    ssize_t length = sock.receive(raw_messages);
    if ( -1 == length ) {
      //Check for a nonblocking socket
      if (EAGAIN == errno or
          EWOULDBLOCK == errno) {
        //Sleep for one millisecond and try again
        usleep(1000);
        continue;
      }
      else {
        //Check the error value to give a more explicit error message.
        std::string err_str(strerror(errno));
        throw std::runtime_error("Error receiving message or the connection was closed: "+err_str);
      }
    }
    else if (0 == length) {
      //0 length indicates end of file (a closed connection)
      throw std::runtime_error("Connection was closed.");
    }
    //Append the message data into the previous_unfinished buffer.
    previous_unfinished.insert(previous_unfinished.end(),
        raw_messages.begin(), raw_messages.begin()+length);

    //Read in the piece length and continue receiving packets until at least
    //that much data has been received. The length bytes themselves take 4 bytes
    if (previous_unfinished.size() >= 4) {
      piece_length = readPrimitive<uint32_t>(previous_unfinished, 0) + 4;
      //std::cerr<<"Continuing next piece with length "<<piece_length<<'\n';
    }
  }
  //If the receive function is interrupted just leave.
  if (interrupted) {
    return std::vector<unsigned char>(0);
  }

  piece_length = readPrimitive<uint32_t>(previous_unfinished, 0) + 4;
  std::vector<unsigned char> new_packet(previous_unfinished.begin(),
      previous_unfinished.begin()+piece_length);
  //Save the unused portion of the buffer. Otherwise there is no more data left over
  //so zero the buffer.
  if ( piece_length < previous_unfinished.size() ) {
    previous_unfinished = std::vector<unsigned char>(previous_unfinished.begin() + piece_length, previous_unfinished.end());
    //std::cerr<<"Saving data for next call:\n";
    //std::for_each(previous_unfinished.begin(), previous_unfinished.end(), [&](unsigned char c) {
        //std::cerr<<c;
        //});
    //std::cerr<<'\n';
  }
  else {
    previous_unfinished.clear();
  }
  return new_packet;
}

