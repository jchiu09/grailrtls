#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <time.h> //For strptime
#include <string.h> //for memset

#include "grailV3_solver_client.hpp"
#include "world_model_protocol.hpp"
#include "netbuffer.hpp"
using namespace world_model;

using namespace std;

//Replaceall example from cppreference.com, modified to continue the
//the replace even on already processed text
//Returns the modified string, but also modifies the string.
string replaceAll(string& context, const string& from, const string& to) {
  size_t foundHere;
  //While the string can still be found
  while ((foundHere = context.find(from)) != string::npos) {
    context.replace(foundHere, from.size(), to);
  }
  return context;
}

u16string tou16String (string & name)
{	return u16string(name.begin(), name.end());
}
string toString( u16string & name) {
	return string(name.begin(),name.end());
}

int main(int ac, char** av) {
  if (ac != 4) {
    return 0;
  }
  std::string wm_ip(av[1]);
  int solver_port = atoi(av[2]);
  string r(av[3]);
  u16string region(r.begin(), r.end());

  map<string, string> event_data;
  vector<string> event_order;
  vector<pair<std::u16string,bool>> event_attri;
  //Are we currently reading heading data?
  bool reading = false;
  //What is the current heading?
  string cur_heading = "";
  string line;
  //Read all lines. Break if four or more consecutive underscore characters are seen
  while (getline(cin, line)) {
    cout<<"reading\n";//Check for end condition
    size_t under_position = line.find("____");
    if (string::npos != under_position) {
      break;
    }
    size_t head_end = line.find(": ");
    //try a tab if the colon-space search fails and nothing if that fails too
    if (string::npos == head_end) {
      head_end = line.find(":\t");
    }
    if (string::npos == head_end) {
      head_end = line.find(":");
    }
    //Heading must be all upper case letters or spaces, no numbers or punctuation
    if (string::npos != head_end) {
      for (int i = 0; i < head_end; ++i) {
        if (not (isupper(line.at(i)) or isspace(line.at(i)))) {
          head_end = string::npos;
	  cout<<" heading is not in upper case or has spaces"<<endl;
          break;
        }
      }
    }
    //See if we found a ": " indicating a heading
    if (string::npos != head_end) {
      //get the heading
      cur_heading = string(line.begin(), line.begin()+head_end);
      //The first heading read must be EVENT
      if (reading or cur_heading == "EVENT") {
        reading = true;
        //Add the rest of the line to the saved event data
        string info(string(line.begin()+head_end+1, line.end()));
        //Extra white space will be cleaned up later.
        event_data[cur_heading] += info;
        event_order.push_back(cur_heading);
      }
    }
    else if (reading) {
      //Extra white space will be cleaned up later.
      event_data[cur_heading] += " " + line;
    }
  }
  
  //Now clean up the event data by converting tabs to spaces
  //and shrinking multiple spaces into single spaces
  for (vector<string>::iterator ev = event_order.begin(); ev != event_order.end(); ++ev) {
    replaceAll(event_data[*ev], "\t", " ");
    replaceAll(event_data[*ev], "  ", " ");
    event_attri.push_back(make_pair(std::u16string(ev->begin(), ev->end()), false));
    //cout <<*ev<<"\t"<< event_data[*ev]<<"\n";
  }
 string event_type = event_data["EVENT"];
 u16string event_name = tou16String(event_type);
 
 for (vector<pair<u16string,bool>>::iterator types = event_attri.begin(); types !=event_attri.end(); types++){
	pair<u16string,bool> type_pair = *types;
	cout<<string(type_pair.first.begin(),type_pair.first.end())<<" , "<<type_pair.second<<endl; 
 }
  //Need to have the time and name or we can't do anything with this.
  if (event_data.find("TIME") == event_data.end() ||
      event_data.find("EVENT") == event_data.end()) {
    return 0;
    cout<<"error";
  }
  //Use strptime to convert the time of the event to a time that grail understands
  // "DayName, Month DayNum, Year - HH:MM AM/PM"
  // "%A, %B %d, %Y - %I:%M $p"
  struct tm time_parts;
  cout<<"reached time code\n";
  //Zero the time parts to prevent random time values.
  memset(&time_parts, 0, sizeof(time_parts));
  string format(" %A, %B %d, %Y - %I:%M %p");
  strptime(event_data["TIME"].c_str(), format.c_str(), &time_parts);
  time_t actual_time = mktime(&time_parts);
  //Check to make sure we actually made this into a time_t value.
  if (-1 == actual_time) {
    return 0;
    cout<<"error in time\n";
  }


  //Set up the solver world model connection;
  std::string origin = "grail/email_event_solver\nversion 1.0";
  cout<<"ready to connect as solver\n";
  //Provide mobility as a non-transient type
  std::vector<std::pair<u16string, bool>> type_pairs{{u"event", false}};
  SolverWorldModel swm(wm_ip, solver_port, type_pairs, u16string(origin.begin(), origin.end()));
  cout<<"connected as solver\n";
  //Give up if we failed to connect to the world model.
  if (not swm.connected()) {
    return 0;
  }
 
  //Stuff all of this event's data into a buffer.
  swm.addTypes(event_attri);
  vector<uint8_t> buff;
  std::vector<SolverWorldModel::Solution> solns;
  //Store the number of event subheadings.
  //pushBackVal((uint32_t)event_data.size(), buff);
  for (vector<pair<u16string,bool>>::iterator types = event_attri.begin(); types !=event_attri.end(); types++){
  
    pair<u16string,bool> type_pair = *types;
    string type_name = toString(type_pair.first);
    cout<<type_name<<"\t"<<string(event_data[type_name].begin(), event_data[type_name].end())<<endl;
    //pushBackSizedUTF16(buff, std::u16string(type_pair.first.begin(), type_pair.first.end()));
    pushBackSizedUTF16(buff, std::u16string(event_data[type_name].begin(), event_data[type_name].end()));
    SolverWorldModel::Solution solut {type_pair.first, (uint64_t)actual_time*1000, region + u".calendar."+ event_name , buff};
  //Make a solution and send it to the world model
  solns.push_back(solut) ;
  
  }
  cout<<"ready with solution\n";
  swm.sendData(solns, true);

  return 0;
}

