#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <usb.h>
#include <errno.h>
#include <sys/time.h>

#include <fcntl.h>
#include <termios.h>
#include <sys/signal.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <time.h>

#include "simple_sockets.hpp"
#include "sensor_aggregator_protocol.hpp"
#include "sample_data.hpp"

#include <iostream>
#include <string>
#include <list>
#include <map>
#include <algorithm>
#include <stdexcept>

//Handle interrupt signals to exit cleanly.
#include <signal.h>

using std::string;
using std::list;
using std::map;
using std::pair;

#define MAX_WRITE_PKTS		0x01

#define FT_READ_MSG			0x00
#define FT_WRITE_MSG		0x01
#define FT_READ_ACK			0x02

#define FT_MSG_SIZE			0x03

//Identifies as 04d8:000a Microchip Technology, Inc.
#define WINS_VENDOR  ((unsigned short) (0x04D8))
#define WINS_PIPPROD ((unsigned char) (0x0a))

#define MAX_PACKET_SIZE_READ		(64 *1024 )
#define MAX_PACKET_SIZE_WRITE		512

typedef unsigned int frequency;
typedef unsigned char bsid;
typedef unsigned char rating;

//Global variable for the signal handler.
bool killed = false;
//Signal handler.
void handler(int signal) {
  psignal( signal, "Received signal ");
  if (killed) {
    std::cerr<<"Aborting.\n";
    // This is the second time we've received the interrupt, so just exit.
    exit(-1);
  }
  std::cerr<<"Shutting down...\n";
  killed = true;
}

//void SetData(std::string command) {
  //unsigned char dataCommand = 0x00;
  //if(command == "7F") {
    //dataCommand=0x7F;
    //m_Addr="FF.FF";
    //UpdateData(false);
  //}

  //if(command == "35") {
    //m_Addr="FF.FF";
    //dataCommand=0x35;
    //UpdateData(true);
  //}
  
  //if(command == "7A") {
    //dataCommand=0x7A;
    //UpdateData(true);
  //}

  //if(command =="42") {
    //dataCommand=0x42;
    //UpdateData(true);
  //}

  //if(command == "36") {
    //dataCommand=0x36;
    //UpdateData(true);
  //}

  //SendCmd(dataCommand);
//}

long GetCRC []={
	0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
		0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
		0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
		0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
		0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
		0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
		0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
		0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
		0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
		0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
		0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
		0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
		0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
		0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
		0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
		0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
		0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
		0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
		0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
		0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
		0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
		0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
		0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
		0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
		0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
		0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
		0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
		0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
		0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
		0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
		0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
		0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};

std::vector<char> makeCmd(unsigned char cmd) {
  //Prepare values in the output buffer
  std::vector<char> sendData(11);
	sendData[0]=0x00;
	sendData[1]=0xAA;
	sendData[2]=0xAA;
	//sendData[3]=HexToDem(m_Addr.Left(2));
	//sendData[4]=HexToDem(m_Addr.Right(2));
  //Hard coding receiver value 33.53 which is 51:83 in decimal
  sendData[3] = 51;
  sendData[4] = 83;
  if (cmd == 0x7F or cmd == 0x35) {
    sendData[3] = 0xFF;
    sendData[4] = 0xFF;
  }
	sendData[5] = 1;
	sendData[6] = cmd;
	//sendData[datalong+7]=Acc(sendData,3,datalong+6);
	char CRC16hi,CRC16lo;
	CRC16hi=0xFF;CRC16lo=0xFF;
	int  tab_s;
	char iIndex;
	for(int qw=3;qw<7;qw++) {
		iIndex=CRC16lo ^ sendData[qw];
		
		tab_s=GetCRC[iIndex];
		
		if(tab_s<0)
			tab_s=tab_s+65536;
		
		CRC16lo=CRC16hi ^ (tab_s & 255);
		CRC16hi=tab_s/256;
	}
	
	sendData[7]=CRC16lo;
	sendData[8]=CRC16hi;
	sendData[9]=0x55;
	sendData[10]=0x55;
  return sendData;
}

//int CThermometerDlg::HexToDem(CString str)
//{
//int dem=0;
	//for(int i=0;i<str.GetLength();i++)
	//{
		//dem=dem*16;
		//if((str[i]<='9')&& (str[i]>='0'))
			//dem+=str[i]-'0';
		//else if((str[i]<='F')&& (str[i]>='A'))
			//dem+=str[i]-'A'+10;
		//else if((str[i]<='f')&& (str[i]>='a'))
			//dem+=str[i]-'a'+10;
		//else
			//return -1;
	//}
	//return dem;
//}

//void CThermometerDlg::OnComm() {
  //SHORT i,KN;

  //double Vbat[20],temperature[20];

  //SHORT start_place,end_place;
  //CByteArray data;
  //CStringArray tempCardId;
  //CStringArray tempCardVbat;

  //SHORT D_LONG;
  //BYTE COMMAND_D;

  //DWORD dwBytesWrite=255; 
  //CString strr;
  //BYTE buffer[355];



  //CString cardID,param,value,dianya,time;


  //BYTE ID0[20],ID1[20],ID2[20],ID3[20],ID_A[20],ID_B[20],ID_12[20],ID_13[20];
  //int RX_YEAR[20],RX_MONTH[20],RX_DAY[20],RX_HOUR[20],RX_MINUTE[20],RX_SECOND[20];

  //COMSTAT   comStat;   
  //DWORD   dwEC;  

  //ClearCommError(hCom,   &dwEC,   &comStat);   

  //BOOL bReadStat; 

  //bReadStat= ReadFile(hCom,buffer,dwBytesWrite,&dwBytesWrite,NULL); 
  //if(!bReadStat) 
  //{ 
    //AfxMessageBox("read error"); 
  //} 

  //PurgeComm(hCom, PURGE_TXABORT|
      //PURGE_RXABORT|PURGE_TXCLEAR|PURGE_RXCLEAR);

  //if(dwBytesWrite>255 || 	dwBytesWrite<6)
  //{  
    ////return;
  //}

  //UpdateData(true);



  //CString send_temp;
  //for(i=0;i<dwBytesWrite;i++)
  //{
    //send_temp.Format("%02x ",buffer[i]);
    //m_data+=send_temp;
  //}
  //if(dwBytesWrite>0)
    //m_data+="\r\n";

  //UpdateData(false);

  ////MessageBox(m_data);

  //start_place=1;
  //end_place=dwBytesWrite;

  //for(i=1;i<end_place-2;i++)
  //{
    //if(buffer[i]==0xAA && buffer[i+1]==0xAA)
    //{
      //if(buffer[i+2]==0xAA)
      //{
        //continue;
      //}
      //else
      //{
        //start_place=i;
      //}
      //break;

    //}
    //if(i==end_place-3)
      //return;

  //} 
  //if(buffer[end_place-1]!=0x55)
  //{
    //return;
  //}



  //BYTE AddrH,AddrL;
  //AddrH=buffer[start_place+2];
  //AddrL=buffer[start_place+3];
  //m_Addr.Format("%02x.%02x",AddrH,AddrL);


  //m_reciveFrame++;

  //UpdateData(false);

  //D_LONG=buffer[start_place+4];
  //COMMAND_D=buffer[start_place+5];

  //CString datastr;



  //if(COMMAND_D==0xFF)
  //{
    //CString temp;
    //temp.Format("基站地址为：%s",m_Addr);
    //SetDlgItemText(IDC_EDIT3,m_Addr);


  //}




  //if(COMMAND_D==0xB5) {
    //KN=(buffer[start_place+4]-1)/16;
    //if(KN>19) return;
    //if(KN==0) {
      //datastr="Sorry!本次没有读取到温度传感器，请继续读取！";
      ////	SetDlgItemText(IDC_EDIT1,datastr);
      //return;
    //}
    //else {

      //BOOL flags;
      //flags=false;



      //for(int KK=1;KK<=KN;KK++) {
        //[>----CRC8校验------<]
        //BYTE CRC8_TEMP[16];
        //for(int ak=0;ak<16;ak++)

          //CRC8_TEMP[ak]=buffer[start_place+6+(KK-1)*16+ak];

        //if(CRC_8(CRC8_TEMP,16)!=0x00)
        //{
          ////MessageBox("单卡CRC8校验错误");
          //continue;
        //}

        //[>-----CRC8校验-------<]

        //ID_A[KK]=buffer[start_place+6+(KK-1)*16];
        //ID_B[KK]=buffer[start_place+7+(KK-1)*16];
        //ID0[KK]=buffer[start_place+8+(KK-1)*16];
        //ID1[KK]=buffer[start_place+9+(KK-1)*16];
        //ID2[KK]=buffer[start_place+10+(KK-1)*16];
        //ID3[KK]=buffer[start_place+11+(KK-1)*16];
        //ID_12[KK]=buffer[start_place+12+(KK-1)*16];
        //ID_13[KK]=buffer[start_place+13+(KK-1)*16];


        //Vbat[KK]=(256*1.2)/buffer[start_place+14+(KK-1)*16];


        //CTime now;
        //now=CTime::GetCurrentTime();	
        //RX_YEAR[KK]=buffer[start_place+16+(KK-1)*16]/16+2000;
        //RX_MONTH[KK]=buffer[start_place+16+(KK-1)*16]& 0x0F;
        //RX_DAY[KK]=buffer[start_place+17+(KK-1)*16];
        //RX_HOUR[KK]=buffer[start_place+18+(KK-1)*16];
        //RX_MINUTE[KK]=buffer[start_place+19+(KK-1)*16];
        //RX_SECOND[KK]=buffer[start_place+20+(KK-1)*16];




        //[>-----计算卡数-------<]

        //int CNO=ID_A[KK]*256+ID_B[KK];

        //if(!RXCARD[CNO])
        //{
          //m_CardCount++;
          //RXCARD[CNO]=true;

        //}

        //UpdateData(false);


        ////以下是数据解析
        //[>

           //传感器电压解析方式有两种方式，温度传感器电压解析方式为	Vbat[KK]=(256*1.2)/buffer[start_place+14+(KK-1)*16];

           //其它传感器电压解析方式为：Vbat[KK]=buffer[start_place+14+(KK-1)*16]/20;

           //温度传感器高位地址都小于0x80，其它传感器高位地址≥0x80

//*/
        //cardID.Format("%02x.%02x",ID_A[KK],ID_B[KK]);
        //cardID.MakeUpper();

        //CString temp,textdata;
        //if(ID0[KK]==0x05)       //温度数据
        //{


          //if(ID_A[KK]>=0x80)  //表明此温度数据是温湿度传感器测得
          //{
            //Vbat[KK]=buffer[start_place+14+(KK-1)*16]/20;
          //}
          //temperature[KK]=(ID_12[KK]*256 +ID_13[KK])*1.0/128;
          //temp.Format("温度传感器ID：%02x.%02x   电压：%0.2fV   测得温度：%0.1f℃   时间：%d-%02d-%02d %02d:%02d:%02d",ID_A[KK],ID_B[KK],Vbat[KK],temperature[KK],RX_YEAR[KK],RX_MONTH[KK],RX_DAY[KK],RX_HOUR[KK],RX_MINUTE[KK],RX_SECOND[KK]);
          //temp.MakeUpper();

          //param="温度 ℃";
          //value.Format("%0.1f℃",temperature[KK]);
          //dianya.Format("%0.2fV",Vbat[KK]);


        //}
        //else
        //{
          //Vbat[KK]=buffer[start_place+14+(KK-1)*16]/20;
          //if(ID0[KK]==0x40)   //湿度数据解析=ID_13[KK]/2
          //{


            //temp.Format("温湿度传感器ID：%02x.%02x   电压：%0.2fV   测得湿度：%d%%RH   时间：%d-%02d-%02d %02d:%02d:%02d",ID_A[KK],ID_B[KK],Vbat[KK],ID_13[KK]/2,RX_YEAR[KK],RX_MONTH[KK],RX_DAY[KK],RX_HOUR[KK],RX_MINUTE[KK],RX_SECOND[KK]);
            //temp.MakeUpper();

            //param="湿度 RH";
            //value.Format("%d%%RH",ID_13[KK]/2);
            //dianya.Format("%0.2fV",Vbat[KK]);
          //}
          //else if(ID0[KK]==0x43)
          //{


            //CString ss;
            //if(ID_13[KK]==0x00)
            //{
              //ss="干燥";
            //}
            //else if(ID_13[KK]==0x01)
            //{
              //ss="水浸";
            //}

            //temp.Format("水浸传感器ID：%02x.%02x   电压：%0.2fV   水浸状态为：%s   时间：%d-%02d-%02d %02d:%02d:%02d",ID_A[KK],ID_B[KK],Vbat[KK],ss,RX_YEAR[KK],RX_MONTH[KK],RX_DAY[KK],RX_HOUR[KK],RX_MINUTE[KK],RX_SECOND[KK]);

            //temp.MakeUpper();


            //param="水浸";
            //value.Format("%s",ss);
            //dianya.Format("%0.2fV",Vbat[KK]);

          //}
          //else if(ID0[KK]==0x45)
          //{


            //CString ss;
            //if(ID_13[KK]==0x10)
            //{
              //ss="开";
            //}
            //else if(ID_13[KK]==0x0F)
            //{
              //ss="关";
            //}

            //temp.Format("磁传感器ID：%02x.%02x   电压：%0.2fV   开关状态：%s  开关次数：%d  时间：%d-%02d-%02d %02d:%02d:%02d",ID_A[KK],ID_B[KK],Vbat[KK],ss,ID_13[KK],RX_YEAR[KK],RX_MONTH[KK],RX_DAY[KK],RX_HOUR[KK],RX_MINUTE[KK],RX_SECOND[KK]);
            //temp.MakeUpper();

            //param="磁控";
            //value.Format("%s",ss);
            //dianya.Format("%0.2fV",Vbat[KK]);


          //}
        //}

        //time.Format("%d-%02d-%02d %02d:%02d:%02d",RX_YEAR[KK],RX_MONTH[KK],RX_DAY[KK],RX_HOUR[KK],RX_MINUTE[KK],RX_SECOND[KK]);
        //ShowList(cardID,param,value,dianya,time);
      //}
    //}
  //}
//}

//PIP 3 Byte ID packet structure with variable data segment.
//3 Byte receiver ID, 21 bit transmitter id, 3 bits of parity plus up to 20 bytes of extra data.
typedef struct {
	unsigned char ex_length : 8; //Length of data in the optional data portion
	unsigned char dropped   : 8; //The number of packet that were dropped if the queue overflowed.
	unsigned int boardID    : 24;//Basestation ID
	unsigned int time       : 32;//Timestamp in quarter microseconds.
	unsigned int tagID      : 21;//Transmitter ID
	unsigned int parity     : 3; //Even parity check on the transmitter ID
	unsigned char rssi      : 8; //Received signal strength indicator
	unsigned char status    : 8; //The lower 7 bits contain the link quality indicator
	unsigned char data[20];      //The optional variable length data segment
} __attribute__((packed)) pip_packet_t;

//Map of usb devices in use, accessed by the USB device number
map<u_int8_t, bool> in_use;

void attachWINS(list<usb_dev_handle*> &wins_devs) {
  struct usb_bus *bus = NULL; 
  struct usb_device *dev = NULL;

  /* Slot numbers used to differentiate multiple PIP USB connections. */
  int slot = 0;

  /* these loops crawl the whole USB tree */
  for (bus = usb_busses; bus != NULL; bus = bus->next) {
    for (dev = bus->devices; dev != NULL; dev = dev->next) {

      int found_manu, found_serial, found_prod;
      found_manu = found_prod = found_serial = 0;

      if (dev->descriptor.idVendor ==  WINS_VENDOR)
        found_manu = 1;

      if (dev->descriptor.idProduct == WINS_PIPPROD)
        found_prod = 1; 

      //If this is a WINS device that is not already opened try opening it.
      if ( (found_manu == 1) && (found_prod == 1) && not in_use[dev->devnum] ) {
        ++slot;
        std::cerr<<"Connected to USB Tag Reader.\n";
        usb_dev_handle* new_handle = usb_open(dev);

        if (!new_handle) {
          std::cout<<"Failed to open WINS device.\n";
        }
        else {
          //Add the new device to the pip device list.
          wins_devs.push_back(new_handle);
          std::cout<<"New WINS device opened.\n";
          in_use[dev->devnum] = true;

          int retval = usb_set_configuration(wins_devs.back(), 1);
          if (retval < 0 ) { 
            printf("Setting configuration to 1 failed %d \n",retval);
            std::cerr<<"Error was "<<usb_strerror()<<'\n';
          }

          //Retry claiming the device up to two times.
          int retries = 2;

          int interface_num = 0;
          while ((retval = usb_claim_interface(wins_devs.back(), interface_num)) && retries-- > 0) {
            if (retval == -ENOMEM) {
              std::cerr<<"usb_claim_interface failed try "<<retries<<": -ENOMEM\n";
              std::cerr<<"This program is being run without permission to open usb devices - aborting.\n";
              retries = 0;
              //This failure indicates that we do not have permission to open the usb device.
              return;
            } else if (retval == -EBUSY) {
#if LIBUSB_HAS_GET_DRIVER_NP
              char drivername[256];
              if (usb_get_driver_np(new_handle, 0, drivername, sizeof(drivername))) {
                std::cerr<<"usb_get_driver_np failed\n";
                retries = 0;
              }
              else {
                std::cerr<<"kernel driver '"<<drivername<<"' is bound to interface 0\n";
#else
                std::cerr<<"kernel driver is bound to interface 0\n";
#endif /* LIBUSB_HAS_GET_DRIVER_NP */

#if LIBUSB_HAS_DETACH_KERNEL_DRIVER_NP
                if (usb_detach_kernel_driver_np(new_handle, 0)) {
                  std::cerr<<"usb_detach_kernel_driver_np failed\n";
                  retries = 0;
                }
                std::cerr<<"kernel driver successfully detached\n";

                int retval = usb_set_configuration(wins_devs.back(), 1);
                if (retval < 0 ) { 
                  printf("Setting configuration to 1 failed %d \n",retval);
                  std::cerr<<"Error was "<<usb_strerror()<<'\n';
                }
#else
                retries = 0;
#endif /* LIBUSB_HAS_DETACH_KERNEL_DRIVER_NP */
#if LIBUSB_HAS_GET_DRIVER_NP
              }
#endif /* LIBUSB_HAS_GET_DRIVER_NP */
            } else {
              std::cerr<<"usb_claim_interface failed: "<<retval<<" tries "<<retries<<"\n";
            }
          }
        }
      }
      if (!dev->config) { 
        std::cout<<"Couldn't retrieve descriptors\n"; 
      }
    }
  }
}

int main(int ac, char** arg_vector) {
  bool testing = (ac == 2 and std::string(arg_vector[1]) == "--test");
  if (not testing and ac != 3 and ac != 4) {
    std::cerr<<"This program requires 2 arguments,"<<
      " the ip address and the port number of the aggregation server to send data to.\n";
    std::cerr<<"To test the program by printing data to stdout, invoke the program like this:\n";
    std::cerr<<"\t"<<arg_vector[0]<<" --test\n";
    return 0;
  }
  std::string server_ip = "";
  int server_port = 0;
  if (not testing) {
    //Get the ip address and ports of the aggregation server
    server_ip = std::string(arg_vector[1]);
    server_port = atoi(arg_vector[2]);
  }

  //Now connect to pip devices and send their packet data to the aggregation server.
  list<usb_dev_handle*> wins_devs;
  //Set up the USB
  usb_init(); 
  usb_find_busses(); 
  usb_find_devices(); 

  //Attach new pip devices.
  attachWINS(wins_devs);

  while (not killed) {
    bool connected = false;

    //Set up a socket to connect to the aggregator.
    ClientSocket agg(AF_INET, SOCK_STREAM, 0, server_port, server_ip);
    if (not testing) {
      if (agg) {
        std::cerr<<"Connected to the GRAIL aggregation server.\n";

        //Try to get the handshake message
        {
          std::vector<unsigned char> handshake = sensor_aggregator::makeHandshakeMsg();

          //Send the handshake message
          agg.send(handshake);
          std::vector<unsigned char> raw_message(handshake.size());
          size_t length = agg.receive(raw_message);

          //Check if the handshake message failed
          if (not (length == handshake.size() and
                std::equal(handshake.begin(), handshake.end(), raw_message.begin()) )) {
            //Quit on failure - what we are trying to connect to is not a proper server.
            std::cerr<<"Failure during handshake with aggregator - aborting.\n";
            killed = true;
          }
          else {
            connected = true;
          }
        }
      } else {
        std::cerr<<"Failed to connect to the GRAIL aggregation server.\n";
      }
    }

    //A try/catch block is set up to handle exception during quitting.
    try {
      while (not killed) {
        //Check for new USB devices by checking if new devices were added or force a check every 3 seconds.
        if (0 < usb_find_busses() + usb_find_devices()) {
          attachWINS(wins_devs);
          //Remove any duplicate devices
          wins_devs.sort();
          wins_devs.unique();
        }
        if (wins_devs.size() > 0) {
          for (list<usb_dev_handle*>::iterator I = wins_devs.begin(); I != wins_devs.end(); ++I) {
            //A pip can fail up to two times in a row if this is the first time querying it.
            //If the pip fails after three retries then this pip usb_dev_handle is no longer
            //valid, probably because the pip was removed from the USB.
            int retries_left = 3;
            int retval = -1;
            //Maximum packet length is 5 bytes at the head, 255 bytes of
            //data, and 4 bytes at the tail, which is 264 bytes in total
            std::vector<unsigned char> read_buff(265);
            while (retval < 0 and retries_left > 0) {
              // Request the next packet from the sensor
              std::vector<char> cmd_buff = makeCmd(0x35);
              retval = usb_bulk_write(*I, 3, cmd_buff.data(), cmd_buff.size(), 100); 
              //std::cerr<<"First retval is "<<retval<<'\n';
              //if (retval < 0) {
                //std::cerr<<"Write Error: "<<usb_strerror()<<'\n';
              //}

              //retval = usb_bulk_read(*I, 0x83, (char*)read_buff.data(), read_buff.size(), 100);
              retval = usb_bulk_read(*I, 0x83, (char*)read_buff.data(), read_buff.size(), 10000);
              --retries_left;
              //std::cerr<<"Read retval is "<<retval<<'\n';
              //if (retval < 0) {
                //std::cerr<<"Read Error: "<<usb_strerror()<<'\n';
                //if (retval == -110) {
                  //std::cerr<<"(timed out)\n";
                //}
              //}
              //if (retval > 0) {
                //std::cerr<<"Bytes are \n";
                //for (int i = 0; i < retval; ++i) {
                  //std::cerr<<(unsigned int)read_buff[i]<<' ';
                //}
                //std::cerr<<'\n';
              //}
            }
            //std::cerr<<"Packet read with retval "<<retval<<'\n';
            //If the pip fails 3 times in a row then it was probably disconnected.
            if (retval < 0) {
              //Don't close the device if it just timed out
              if (retval != -110) {
                usb_reset(*I);
                usb_close(*I); 
                *I = NULL;
              }
            }
            else if (retval > 11) {
              if (testing) {
                std::cerr<<"Bytes are \n";
                for (int i = 0; i < retval; ++i) {
                  std::cerr<<(unsigned int)read_buff[i]<<' ';
                }
                std::cerr<<'\n';
              }
              //TODO FIXME Check CRC
              uint32_t receiver_id = (unsigned int)read_buff[3]*0xFF + (unsigned int)read_buff[4];
              if (testing) {
                std::cerr<<"Reading a packet from "<<receiver_id<<'\n';
              }
              //There were actually several packets received
              uint8_t num_bytes = read_buff[5];
              //Byte at offset 6 is the command
              uint32_t cur_byte = 6;
              //Now assemble a sample data variable for each packet received
              //and send it to the aggregator
              while (retval - cur_byte > 15) {
                SampleData sd;
                sd.rx_id = receiver_id;
                //WINS physical layer is 3
                sd.physical_layer = 3;
                sd.tx_id = ((unsigned int)read_buff[cur_byte+1])*0x100 + (unsigned int)read_buff[cur_byte+2];
                if (testing) {
                  std::cerr<<"ID bytes are "<<(unsigned int)read_buff[cur_byte+1]<<" and "<<(unsigned int)read_buff[cur_byte+2]<<'\n';
                }
                uint8_t dtype = read_buff[cur_byte+3];
                if (testing) {
                  std::cerr<<"Data type for data from sensor 0x"<<std::hex<<sd.tx_id<<" is 0x"<<std::hex<<(unsigned int)dtype<<std::dec<<'\n';
                }
                sd.sense_data.push_back(dtype);
                //Temperature
                if (dtype == 5) {
                  //temperature[KK]=(ID_12[KK]*256 +ID_13[KK])*1.0/128;
                  sd.sense_data.push_back(read_buff[cur_byte+7]);
                  sd.sense_data.push_back(read_buff[cur_byte+8]);
                  if (testing) {
                    std::cerr<<"temperature is "<<(((unsigned int)read_buff[cur_byte+7]*0x100 + (unsigned int)read_buff[cur_byte+8])/128.0)<<'\n';
                  }
                }
                //Humidity
                else if (dtype == 0x40) {
                  sd.sense_data.push_back(read_buff[cur_byte+7]);
                  sd.sense_data.push_back(read_buff[cur_byte+8]);
                }
                //Water
                else if (dtype == 0x43) {
                }
                //Magetism
                else if (dtype == 0x45) {
                }
                //Also get voltage
                sd.sense_data.push_back(read_buff[cur_byte+9]);
                cur_byte += 16;

                //Set this to the real timestamp, milliseconds since 1970
                //TODO FIXME Use the wins time
                timeval tval;
                gettimeofday(&tval, NULL);
                sd.rx_timestamp = tval.tv_sec*1000 + tval.tv_usec/1000;
                sd.rss = 0.0;
                sd.valid = true;

                if (not testing) {
                  //Send the sample data
                  agg.send(sensor_aggregator::makeSampleMsg(sd));
                }
              }
            }
          }
        }
        //Clear dead connections
        wins_devs.remove(NULL);
      }
    }
    catch (std::runtime_error& re) {
      std::cerr<<"USB sensor layer error: "<<re.what()<<'\n';
    }
    catch (std::exception& e) {
      std::cerr<<"USB sensor layer error: "<<e.what()<<'\n';
    }
    //Try to reconnect to the server after losing the connection.
    //Sleep a little bit, then try connecting to the server again.
    usleep(1000);
  }
  std::cerr<<"Exiting\n";
  //Clean up the usb connections before exiting.
  for (list<usb_dev_handle*>::iterator I = wins_devs.begin(); I != wins_devs.end(); ++I) {
    usb_reset(*I);
    usb_close (*I); 
  }
}


