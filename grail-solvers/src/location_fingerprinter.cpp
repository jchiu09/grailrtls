/*******************************************************************************
 * This program will fingerprint locations based upon RSS values from the
 * fingerprint_solver.
 * Fingerprinting is triggered by a URI in the world model
 * having a calibration.sensor attribute and another attribute named
 * "fingerprinting on" set to true (0x01 in hex).
 * This solver will request RSS values from the sensor (calibration.sensor)
 * and will generate entries for fingerprint.<statistic>.vector<double>
 * Those entry attributes will be placed in the world model when
 * "fingerprinting on" turns to false (0x00 in hex).
 * A location discrimination solver can use these values for location
 * discrimination.
 ******************************************************************************/

#include <algorithm>
#include <cmath>
#include <deque>
#include <functional>
#include <map>
#include <mutex>
#include <queue>
#include <string>
#include <thread>
#include <tuple>
#include <utility>
#include <vector>

#include <grailV3_solver_client.hpp>
#include <netbuffer.hpp>
#include <sample_data.hpp>
#include <world_model_protocol.hpp>
#include <grail_types.hpp>

using std::deque;
using std::map;
using std::pair;
using std::tuple;
using std::u16string;
using std::vector;

using world_model::Attribute;
using world_model::grail_time;
using world_model::URI;

typedef pair<unsigned char, TransmitterID> UniqueTxer;

std::string toString(const std::u16string& str) {
  return std::string(str.begin(), str.end());
}

grail_types::transmitter strToSensor(const std::string& str) {
  size_t phy_end = str.find(".");
  grail_types::transmitter tx;
  tx.phy = std::stoul(str.substr(0, phy_end));
  tx.id.upper = 0;
  tx.id.lower = std::stoul(str.substr(phy_end+1));
  return tx;
}

std::u16string rxerFromLink(const std::u16string& link_string) {
  size_t phy_end = link_string.find(u".");
  size_t tx_end = link_string.find(u".", phy_end+1);
  if (phy_end != std::u16string::npos and tx_end != std::u16string::npos) {
    //URIs for link medians are string representations of the
    //the transmitter in the format <phy>.<txid>.<rxid>
    URI raw_tx_uri = link_string.substr(0, tx_end);
    return link_string.substr(0, phy_end) + link_string.substr(tx_end);
  }
  else {
    return u"";
  }
}

typedef std::map<std::u16string, std::priority_queue<double>> PlaceStats;

std::mutex handling_mutex;

//Use the statistics of transmitter that the world model provides to
//model the fingerprint of the given location.
//The main thread will create a lambda closure around this function for
//each place and will cancel the request when the fingerprint is complete.
void updatePlaceStats(PlaceStats& medians, ClientWorldModel::world_state& ws) {
  std::unique_lock<std::mutex> lck(handling_mutex);
  for (auto I = ws.begin(); I != ws.end(); ++I) {
    if (not I->second.empty()) {
      std::cerr<<"Getting median for "<<toString(I->first)<<'\n';
      std::u16string rxer = rxerFromLink(I->first);
      double median_rss = readPrimitive<double>(I->second.at(0).data, 0);
      medians[rxer].push(median_rss);
    }
  }
}

int main(int ac, char** av) {
  if (4 != ac) {
    std::cerr<<"This program needs 3 arguments:\n";
    std::cerr<<"\t"<<av[0]<<" <world model ip> <solver port> <client port>\n";
    std::cerr<<"The solution type \"average variance\" will be used for fingerprinting.\n";
    return 0;
  }

  //World model IP and ports
  std::string wm_ip(av[1]);
  int solver_port = std::stoi(std::string((av[2])));
  int client_port = std::stoi(std::string((av[3])));

  //Set up the solver world model connection;
  std::u16string origin = u"grail/location fingerprinter\nversion 1.0";
  //Solutions are fingerprint characteristics
  std::vector<std::pair<u16string, bool>> type_pairs{{u"fingerprint.receivers.vector<sensor.receiver>", false},
    {u"fingerprint.min signal.vector<RSS>", false},
    {u"fingerprint.low quarter.vector<RSS>", false},
    {u"fingerprint.median.vector<RSS>", false},
    {u"fingerprint.high quarter.vector<RSS>", false},
    {u"fingerprint.max signal.vector<RSS>", false}};
  SolverWorldModel swm(wm_ip, solver_port, type_pairs, origin);
  if (not swm.connected()) {
    std::cerr<<"Could not connect to the world model as a solver - aborting.\n";
    return 0;
  }
  
  ClientWorldModel cwm(wm_ip, client_port);
  if (not cwm.connected()) {
    std::cerr<<"Could not connect to the world model as a client - aborting.\n";
    return 0;
  }

  //We need to keep track of the current ticket for tag statistics requests.
  const uint32_t calibration_ticket = 1;
  uint32_t cur_ticket = 1;

  //Search for URIs with fingerprinting attributes.
  //Get attributes named "average variance"
  //If that parameter goes above threshold declare that item moving.
  URI any_sensor = u".*";
  vector<URI> fp_attributes{u"calibration.sensor", u"fingerprinting on"};
  grail_time interval = 1000;
  cwm.setupSynchronousDataStream(any_sensor, fp_attributes, interval, calibration_ticket);

  //Remember areas where statistics are being gathered and the
  //ticket for the world model request that is gathering data there.
  std::map<URI, std::pair<uint32_t, PlaceStats>> running_statistics;
  //Requests that need to be assigned tickets.
  std::deque<URI> pending_tickets;

  auto is_fper = [&](Attribute& a) { return a.name == (u"calibration.sensor");};
  auto is_fon = [&](Attribute& a) { return a.name == u"fingerprinting on";};

  std::cerr<<"Starting loop...\n";
  while (1) {
    //Get updates in fingerprinting
    ClientWorldModel::world_state ws;
    uint32_t ticket;
    std::tie(ws, ticket) = cwm.getSynchronousStreamUpdate();
    //Check for a change in fingerprinting state.
    for (auto I = ws.begin(); I != ws.end(); ++I) {
      std::cerr<<"Got world state for "<<toString(I->first)<<'\n';
      auto fper = std::find_if(I->second.begin(), I->second.end(), is_fper);
      auto fon = std::find_if(I->second.begin(), I->second.end(), is_fon);
      if (fper != I->second.end() and fon != I->second.end()) {
        bool print_on = readPrimitive<bool>(fon->data, 0);
        auto stat_i = running_statistics.find(I->first);
        //Start collecting statistics if this just turned on
        if (print_on and running_statistics.end() == running_statistics.find(I->first)) {
          //Get the fingerprinting ID
          grail_types::transmitter txid = grail_types::readTransmitter(fper->data);
          std::string tx_string = std::to_string(txid.phy) + "\\." + std::to_string(txid.id.lower) + "\\..*";
          //Request the attribute median RSS for this transmitter.
          URI this_sensor = std::u16string(tx_string.begin(), tx_string.end());
          vector<URI> median{u"link median"};
          grail_time interval = 1000;
          running_statistics[I->first] = std::make_pair(++cur_ticket, PlaceStats());
          PlaceStats& ps = running_statistics[I->first].second;
          std::cerr<<"Requesting median for URI pattern "<<tx_string<<'\n';
          cwm.setupSynchronousDataStream(this_sensor, median, interval,
              [&](ClientWorldModel::world_state& ws, uint32_t ticket){
              updatePlaceStats(std::ref(ps), ws);}, cur_ticket);
        }
        //Fingerprinting is finished.
        else if (not print_on and running_statistics.end() != stat_i) {
          std::cerr<<"Finishing data collection for location "<<toString(I->first)<<'\n';
          //Cancel this streaming request.
          std::cerr<<"Cancelling ticket "<<stat_i->second.first<<'\n';
          cwm.cancelRequest(stat_i->second.first);

          //Generate the required statistics and give them to the world model.
          PlaceStats& ps = stat_i->second.second;
          grail_time sol_time = world_model::getGRAILTime();
          SolverWorldModel::Solution rx_soln{u"fingerprint.receivers.vector<sensor.receiver>", sol_time, I->first, vector<uint8_t>()};
          SolverWorldModel::Solution min_soln{u"fingerprint.min signal.vector<RSS>", sol_time, I->first, vector<uint8_t>()};
          SolverWorldModel::Solution low25_soln{u"fingerprint.low quarter.vector<RSS>", sol_time, I->first, vector<uint8_t>()};
          SolverWorldModel::Solution mid_soln{u"fingerprint.median.vector<RSS>", sol_time, I->first, vector<uint8_t>()};
          SolverWorldModel::Solution high25_soln{u"fingerprint.high quarter.vector<RSS>", sol_time, I->first, vector<uint8_t>()};
          SolverWorldModel::Solution max_soln{u"fingerprint.max signal.vector<RSS>", sol_time, I->first, vector<uint8_t>()};
          //Push back the number of items in each of the vectors
          pushBackVal<uint32_t>(ps.size(), rx_soln.data);
          pushBackVal<uint32_t>(ps.size(), min_soln.data);
          pushBackVal<uint32_t>(ps.size(), low25_soln.data);
          pushBackVal<uint32_t>(ps.size(), mid_soln.data);
          pushBackVal<uint32_t>(ps.size(), high25_soln.data);
          pushBackVal<uint32_t>(ps.size(), max_soln.data);
          for (auto RX = ps.begin(); RX != ps.end(); ++RX) {
            std::cerr<<"About to pull data from rxer "<<toString(RX->first)<<'\n';
            std::priority_queue<double>& pq = RX->second;
            //Write out this receiver's fingerprint information
            grail_types::writeTransmitter(strToSensor(toString(RX->first)), rx_soln.data);
            pushBackVal<double>(pq.top(), max_soln.data);
            //Pop one quarter of the solutions to find the upper 25%, middle,
            //and lower 25% values.
            uint32_t quarter = pq.size() / 4;
            for (uint32_t i = 0; i < quarter; ++i) { pq.pop(); }
            pushBackVal<double>(pq.top(), high25_soln.data);
            for (uint32_t i = 0; i < quarter; ++i) { pq.pop(); }
            pushBackVal<double>(pq.top(), mid_soln.data);
            for (uint32_t i = 0; i < quarter; ++i) { pq.pop(); }
            pushBackVal<double>(pq.top(), low25_soln.data);
            while (pq.size() > 1) { pq.pop();}
            pushBackVal<double>(pq.top(), min_soln.data);
          }
          vector<SolverWorldModel::Solution> solns{rx_soln, min_soln, low25_soln, mid_soln, high25_soln, max_soln};
          swm.sendData(solns, false);
          //Lock before deleting the stat data.
          std::unique_lock<std::mutex> lck(handling_mutex);
          std::cerr<<"Deleting stats.\n";
          running_statistics.erase(stat_i);
          std::cerr<<"Done.\n";
        }
      }
      else {
        std::cerr<<"But it wasn't interesting.\n";
      }
    }
  }
  return 0;
}

