#include <algorithm>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <time.h>
#include <vector>
#include <cmath>
#include <thread>

#include "aggregator_solver_protocol.hpp"
#include "solver_distributor_protocol.hpp"
#include "grailV3_solver_client.hpp"
#include "sample_data.hpp"
#include "netbuffer.hpp"

using namespace aggregator_solver;

using namespace solver_distributor;

bool logging = false;

void packetCallback(SampleData& sample) {
  //Only process valid samples.
  if (not sample.valid) {
    return;
  }

  if (logging) {
    std::cout<<sample.tx_id.lower<<'\t'<<sample.rx_id<<'\t'<<sample.rss<<'\n';
  }
}

void buttonListener() {
  using namespace std;
  std::string in;
  /*
  std::vector<std::pair<float, float>> locations{ {37, 39}, {63, 30.8}, {73, 44.5}, {81, 57}, {81, 76},
                                                  {101, 46.5}, {93, 40.5}, {85, 34}, {60.5, 14}, {83,7},
                                                  {90, 14.5}, {106.5, 24}, {132, 14.5}, {141.5, 5}, {131.5, 44},
                                                  {124, 51.5}, {132, 68}, {120, 63}, {156, 76.5}, {145, 99},
                                                  {124.5, 89}, {116, 100}, {111, 80}, {106.5, 61}, {108, 42.5}};
                                                  */
  /*
  std::vector<std::pair<float, float>> locations{ {4, 48}, {4, 52}, {4, 56},
                                                  {14, 55.5}, {18, 55.5}, {22, 55.5}, {26, 55.5}, {30, 55.5}, {34, 55.5}, {38, 55.5}, {42, 55.5},
                                                  {14, 66}, {18, 66}, {22, 66}, {26, 66}, {30, 66}, {34, 66}, {38, 66}, {42, 66},
                                                  {42, 70}, {42, 74}, {42, 78}, {42, 82},
                                                  {4, 89}, {8, 89} };
                                                  */
  std::vector<std::pair<float, float>> locations{ {36.2, 64}, //Near transmitter 128
                                                  {25.6, 87}, //Near transmitter 115
                                                  {13.6, 54.5}}; //Near transmitter 46
  auto cur = locations.begin();
  cerr<<"Press enter to log information from location "<<cur->first<<'\t'<<cur->second<<'\n';
  while (getline(cin, in)) {
    cout<<"Sample "<<cur->first<<'\t'<<cur->second<<'\n';
    logging = true;
    //Sleep for 10 seconds
    usleep(10000000);
    logging = false;
    if (++cur == locations.end()) cur = locations.begin();
    cerr<<"\nPress enter to log information from location "<<cur->first<<'\t'<<cur->second<<'\n';
  }
  exit(0);
}


int main(int arg_count, char** arg_vector) {
  if (3 > arg_count) {
    std::cerr<<"This program needs at least 2 arguments:\n";
    std::cerr<<"\tclient [<aggregator ip> <aggregator port>]+\n";
    return 0;
  }

  //Grab the ip and ports for the servers and aggregator
  std::vector<NetTarget> servers;
  for (int s_num = 1; s_num < arg_count; s_num += 2) {
    std::string server_ip(arg_vector[s_num]);
    uint16_t server_port = atoi(arg_vector[s_num + 1]);
    servers.push_back(NetTarget{server_ip, server_port});
  }

  Rule winlab_rule;
  winlab_rule.physical_layer  = 1;

  winlab_rule.update_interval = 2000;

  Subscription winlab_sub{winlab_rule};
  std::vector<Subscription> subs{winlab_sub};

  std::thread t = std::thread(buttonListener);

  //Connect to the grail server with our subscription lists.
  grailAggregatorConnect(servers, subs, packetCallback);

  return 0;
}

