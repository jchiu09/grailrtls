/*
 * Copyright (c) 2011 Bernhard Firner and Rutgers University
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or visit http://www.gnu.org/licenses/gpl-2.0.html
 */

#include <algorithm>
#include <array>
#include <iostream>
#include <map>
#include <string>
#include <utility>
#include <vector>

#include <grailV3_solver_client.hpp>
#include <aggregator_solver_protocol.hpp>
#include <netbuffer.hpp>
#include <sample_data.hpp>
#include <world_model_protocol.hpp>
#include <grail_types.hpp>

using namespace aggregator_solver;

using std::pair;

using world_model::Attribute;
using world_model::grail_time;
using world_model::URI;

struct Debug {
  bool on;
};

template<typename T>
Debug& operator<<(Debug& dbg, T arg) {
  if (dbg.on) {
    std::cout<<arg;
  }
  return dbg;
}

std::u16string toU16(const std::string& str) {
  return std::u16string(str.begin(), str.end());
}

std::string toString(const std::u16string& str) {
  return std::string(str.begin(), str.end());
}

int main(int arg_count, char** arg_vector) {
  if (6 > arg_count or (arg_count % 2) == 1) {
    std::cerr<<"This program needs 6 or more arguments:\n";
    std::cerr<<"\t"<<arg_vector[0]<<" [<aggregator ip> <aggregator port>]+ <world model ip> <solver port> <client port>\n\n";
    std::cerr<<"Any number of aggregator ip/port pairs may be provided to connect to multiple aggregators.\n";
    return 0;
  }

  //Grab the ip and ports for the aggregators and world model
  std::vector<NetTarget> servers;
  for (int s_num = 1; s_num < arg_count - 3; s_num += 2) {
    std::string server_ip(arg_vector[s_num]);
    uint16_t server_port = std::stoi(std::string(arg_vector[s_num + 1]));
    servers.push_back(NetTarget{server_ip, server_port});
  }

  //World model IP and ports
  std::string wm_ip(arg_vector[arg_count - 3]);
  int solver_port = std::stoi(std::string((arg_vector[arg_count - 2])));
  int client_port = std::stoi(std::string((arg_vector[arg_count - 1])));

  //Set up the solver world model connection;
  std::string origin = "grail/temperature solver\nversion 1.0";

  //Provide temperature data
  std::vector<std::pair<u16string, bool>> type_pairs{{u"temperature", false}};
  SolverWorldModel swm(wm_ip, solver_port, type_pairs, u16string(origin.begin(), origin.end()));
  if (not swm.connected()) {
    std::cerr<<"Could not connect to the world model as a solver - aborting.\n";
    return 0;
  }

  ClientWorldModel cwm(wm_ip, client_port);
  if (not cwm.connected()) {
    std::cerr<<"Could not connect to the world model as a client - aborting.\n";
    return 0;
  }

  //Search for any URIs with attributes sensors.temperature
  URI desired_uris = u".*";
  std::vector<URI> attributes{u"sensor.temperature.*"};
  world_model::grail_time interval = 5000;
  cwm.setupSynchronousDataStream(desired_uris, attributes, interval, 1);

  //Objects with temperature sensors
  std::set<URI> temperature_sensors;
  //A mapping between sensors and the objects they are attached to.
  std::map<pair<uint8_t, uint128_t>, URI> tx_to_uri;
  //Aggregator callback is threaded so we need a mutex
  std::mutex tx_to_uri_mutex;
  std::map<URI, double> prev_temp;

  //Define a callback to handle new temperature data.
  auto packet_callback = [&](SampleData& s) {
    if (s.valid and 2 == s.sense_data.size()) {
      bool temp_sense = false;
      std::pair<uint8_t, uint128_t> txer = std::make_pair(s.physical_layer, s.tx_id);
      {
        std::unique_lock<std::mutex> lck(tx_to_uri_mutex);
        temp_sense = tx_to_uri.find(txer) != tx_to_uri.end();
      }
      if (temp_sense) {
        //Get the temperature but perform a quick vote with previous value
        //to screen out anomalous readings
        //Temperature is in celcius for pips
        if (s.physical_layer == 1) {
          double temp = ((uint32_t)s.sense_data[0] << 8) + s.sense_data[1];
          URI tx_uri = tx_to_uri[txer];
          if (prev_temp.find(tx_uri) == prev_temp.end()) {
            prev_temp[tx_uri] = temp;
            //Send this data to the solver
            SolverWorldModel::Solution soln{u"temperature", world_model::getGRAILTime(), tx_uri, std::vector<uint8_t>()};
            pushBackVal<double>(temp, soln.data);
            std::vector<SolverWorldModel::Solution> solns{soln};
            swm.sendData(solns, false);
            std::cerr<<toString(tx_uri)<<" has temperature "<<temp<<'\n';
          }
          //TODO This two step changer isn't the best - should really wait
          //for two equal values.
          else if (prev_temp[tx_uri] != temp) {
            //Clear the temperature value. This forces two consecutive
            //temperature values to change the reported temperature.
            prev_temp.erase(tx_uri);
          }
        }
      }
    }
  };
  SolverAggregator aggregator(servers, packet_callback);

  //Lambda function to find temperature attributes
  auto is_temp = [&](Attribute& a) { return a.name == u"sensor.temperature";};

  //Rules for the aggregator
  std::map<uint8_t, Rule> phy_to_rule;

  std::cerr<<"Starting loop...\n";
  while (1) {
    //Remember if we need to ask the aggregator for more information.
    bool new_transmitters = false;
    //Get world model updates
    ClientWorldModel::world_state ws;
    uint32_t ticket;
    std::tie(ws, ticket) = cwm.getSynchronousStreamUpdate();
    //Make a list of new solutions
    //Check each URI to see if it has begun to move or has stopped moving
    for (auto I = ws.begin(); I != ws.end(); ++I) {
      //Check if there is a transmitter attribute
      auto tx_attrib = std::find_if(I->second.begin(), I->second.end(), is_temp);
      if (tx_attrib != I->second.end()) {
        //Transmitters are stored as one byte of physical layer and 16 bytes of ID
        grail_types::transmitter tx_temp = grail_types::readTransmitter(tx_attrib->data);
        if (phy_to_rule.find(tx_temp.phy) == phy_to_rule.end()) {
          phy_to_rule[tx_temp.phy].physical_layer  = tx_temp.phy;
          phy_to_rule[tx_temp.phy].update_interval = 1000;
        }

        //See if this is a new transmitter
        auto tx_present = [&](const Transmitter& tx) {return tx.base_id == tx_temp.id;};

        //Only accept data from sensors that we care about
        std::vector<Transmitter>& phy_txers = phy_to_rule[tx_temp.phy].txers;
        if (phy_txers.end() == std::find_if(phy_txers.begin(), phy_txers.end(), tx_present)) {
          //Make a new aggregator request for this.
          Transmitter sensor_id;
          sensor_id.base_id = tx_temp.id;
          sensor_id.mask.upper = 0xFFFFFFFFFFFFFFFF;
          sensor_id.mask.lower = 0xFFFFFFFFFFFFFFFF;
          phy_txers.push_back(sensor_id);
          //Mark that we are making a change to the aggregator rules.
          new_transmitters = true;
          //Remember the URI that this transmitter corresponds to
          std::unique_lock<std::mutex> lck(tx_to_uri_mutex);
          tx_to_uri[std::make_pair(tx_temp.phy, tx_temp.id)] = I->first;
        }
      }
    }
    //Make new subscriptions to the aggregator if there are new transmitters
    if (new_transmitters) {
      Subscription sub;
      for (auto I = phy_to_rule.begin(); I != phy_to_rule.end(); ++I) {
        sub.push_back(I->second);
      }
      aggregator.updateRules(sub);
    }
  }
}

