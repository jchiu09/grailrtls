#include <array>
#include <string>
#include <vector>

#include "solver_distributor_protocol.hpp"
#include "netbuffer.hpp"

using namespace solver_distributor;

std::vector<unsigned char> solver_distributor::makeHandshakeMsg() {
  std::vector<unsigned char> buff(4);
  std::string protocol_string = "GRAIL distributor protocol";
  buff.insert(buff.end(), protocol_string.begin(), protocol_string.end());
  //Version number and extension are both currently zero
  buff.push_back(0);
  buff.push_back(0);
  //Insert the length of the protocol string into the buffer
  pushBackVal<uint32_t>(protocol_string.length(), buff, 0);
  return buff;
}

/**
 * Make a message from the solver that specifies what types of data are
 * in possible solutions.
 */
std::vector<unsigned char> solver_distributor::makeTypeSpecMsg(const std::vector<SolutionType>& type_specs) {
  std::vector<unsigned char> buff;

  //Keep track of the total length of the message (in bytes)
  uint32_t total_length = 0;
  //Push back space in the buffer for the total length.
  pushBackVal(total_length, buff);
  //Push back the message type.
  total_length += pushBackVal((uint8_t)type_specification, buff);

  //Push back the total number of type specifications in this message.
  total_length += pushBackVal((uint32_t)type_specs.size(), buff);

  for (auto spec = type_specs.begin(); spec != type_specs.end(); ++spec) {

    total_length += pushBackVal(spec->type_alias, buff);
    //Push back the length of the name of this data and the name of the data.
    total_length += pushBackSizedUTF16(buff, spec->name);
    //total_length += pushBackVal(spec->data.data_len, buff);
  }

  //Push back the actual length into the buffer.
  pushBackVal(total_length, buff, 0);
  return buff;
}

std::vector<SolutionType> solver_distributor::decodeTypeSpecMsg(std::vector<unsigned char>& buff, int length) {
  BuffReader reader(buff);

  std::vector<SolutionType> msg;
  uint32_t total_length = reader.readPrimitive<uint32_t>();
  uint8_t msg_type = reader.readPrimitive<uint8_t>();

  //Check to make sure this is a valid type specification message.
  if ( length == (total_length + 4) and
       msg_type == solver_distributor::type_specification) {
    uint32_t total_types = reader.readPrimitive<uint32_t>();

    for (size_t i = 0; i < total_types; ++i) {
      SolutionType sol;
      sol.type_alias = reader.readPrimitive<uint32_t>();
      uint32_t data_name_len = reader.readPrimitive<uint32_t>();
      sol.name = reader.readUTF16(data_name_len / sizeof(char16_t));
      msg.push_back(sol);
    }
  }

  return msg;
}

std::vector<unsigned char> solver_distributor::makeTypeReqMsg(const SolutionRequest& request) {
  std::vector<unsigned char> buff;

  //Keep track of the total length of the message (in bytes)
  uint32_t total_length = 0;
  //Push back space in the buffer for the total length.
  pushBackVal(total_length, buff);
  //Push back the message type.
  total_length += pushBackVal((uint8_t)distributor_application::solution_request, buff);

  //Push back the begin and end times
  total_length += pushBackVal(request.begin, buff);
  total_length += pushBackVal(request.end, buff);


  //Push back the total number of type specifications in this message.
  total_length += pushBackVal((uint32_t)request.aliases.size(), buff);

  //Push back the type alias for each desired type
  for (auto alias = request.aliases.begin(); alias != request.aliases.end(); ++alias) {
    total_length += pushBackVal(*alias, buff);
  }

  //Push back the actual length into the buffer.
  pushBackVal(total_length, buff, 0);
  return buff;
}

SolutionRequest solver_distributor::decodeTypeReqMsg(std::vector<unsigned char>& buff, int length) {
  BuffReader reader(buff);
  SolutionRequest request;

  uint32_t total_length = reader.readPrimitive<uint32_t>();
  uint8_t msg_type = reader.readPrimitive<uint8_t>();

  //Check to make sure this is a valid type specification message.
  if ( length == (total_length + 4) and
       msg_type == distributor_application::solution_request) {

    reader.convertPrimitive(request.begin);
    reader.convertPrimitive(request.end);
    uint32_t total_types = reader.readPrimitive<uint32_t>();

    for (size_t i = 0; i < total_types; ++i) {
      request.aliases.push_back(reader.readPrimitive<uint32_t>());
    }
  }

  return request;
}

/**
 * Make a message from the solver to send solutions to the aggregator.
 */
std::vector<unsigned char> solver_distributor::makeSolutionMsg(const SolverDataMsg& solutions) {
  std::vector<unsigned char> buff;

  uint32_t total_length = 0;
  //Push back space in the buffer for the total length.
  pushBackVal(total_length, buff);
  //Push back the message type.
  total_length += pushBackVal((uint8_t)solver_data, buff);

  //Push back the region URI for these solutions
  //and the total number of solutions in this message.
  total_length += pushBackSizedUTF16(buff, solutions.region);
  total_length += pushBackVal(solutions.time, buff);
  total_length += pushBackVal<uint32_t>(solutions.data.size(), buff);

  //Push back the alias, target name, and data for each solution.
  for (auto soln = solutions.data.begin(); soln != solutions.data.end(); ++soln) {
    total_length += pushBackVal(soln->type_alias, buff);
    total_length += pushBackSizedUTF16(buff, soln->target);
    total_length += pushBackVal((uint32_t)soln->data.size(), buff);
    for (auto val = soln->data.begin(); val != soln->data.end(); ++val) {
      total_length += pushBackVal(*val, buff);
    }
  }

  //Push back the actual length into the buffer.
  pushBackVal(total_length, buff, 0);
  return buff;
}

SolverDataMsg solver_distributor::decodeSolutionMsg(std::vector<unsigned char>& buff, int length) {
  BuffReader reader(buff);

  SolverDataMsg msg;

  uint32_t total_length = reader.readPrimitive<uint32_t>();
  uint8_t msg_type = reader.readPrimitive<uint8_t>();

  if ( length == (total_length + 4) and
       msg_type == solver_distributor::solver_data) {
    //Read in the region URI, the timestamp, and the total
    //number of solutions in this message.
    msg.region = reader.readSizedUTF16();
    msg.time = reader.readPrimitive<uint64_t>();
    uint32_t num_solutions = reader.readPrimitive<uint32_t>();

    //Read in the data set (alias, target, and data) for each solution
    for (size_t i = 0; i < num_solutions; ++i) {
      SolutionData d;
      reader.convertPrimitive(d.type_alias);
      d.target = reader.readSizedUTF16();
      d.data = reader.readSizedVector<uint8_t>();

      msg.data.push_back(d);
    }
  }

  return msg;
}


std::vector<unsigned char> distributor_application::makeHandshakeMsg() {
  std::vector<unsigned char> buff(4);
  std::string protocol_string = "GRAIL client protocol";
  buff.insert(buff.end(), protocol_string.begin(), protocol_string.end());
  //Version number and extension are both currently zero
  buff.push_back(0);
  buff.push_back(0);
  //Insert the length of the protocol string into the buffer
  pushBackVal<uint32_t>(protocol_string.length(), buff, 0);
  return buff;
}

std::vector<unsigned char> distributor_application::makeTypeSpecMsg(const std::vector<SolutionType>& type_specs) {
  std::vector<unsigned char> buff = solver_distributor::makeTypeSpecMsg(type_specs);
  buff[4] = distributor_application::solution_publication;
  return buff;
}

std::vector<unsigned char> distributor_application::makeSolutionMsg(const SolverDataMsg& data) {
  std::vector<uint8_t> buff = solver_distributor::makeSolutionMsg(data);
  //Change the message type - otherwise these two messages are actually the same.
  buff[4] = distributor_application::solution_sample;
  return buff;
}

std::vector<unsigned char> distributor_application::makeHistoricCompleteMsg() {
  std::vector<unsigned char> buff;
  pushBackVal<uint32_t>(1, buff);
  pushBackVal<uint8_t>((uint8_t)historic_complete, buff);
  return buff;
}


