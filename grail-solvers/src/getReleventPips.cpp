#include <algorithm>
#include <array>
#include <iostream>
#include <map>
#include <string>
#include <time.h>
#include <vector>
#include <sstream>

#include "world_server_protocol.hpp"
#include "aggregator_solver_protocol.hpp"
#include "grailV3_solver_client.hpp"
#include "sample_data.hpp"
#include "netbuffer.hpp"

using namespace std;
using namespace solver_distributor;

int main(int ac, char** arg_vector) {
  if (ac != 7) {
    std::cerr<<"You didn't give the right number of arguments.\n";
    return 0;
  }

  vector<u16string> good_rxers;
  vector<u16string> good_txers;
  vector<uint64_t> rxers;
  vector<uint64_t> txers;
  vector<u16string> good_pips;

  //vector<uint64_t> good_rxers;
  //vector<uint64_t> good_txers;
  //vector<u16string> good_pips;

  std::string ws_ip(arg_vector[1]);
  int ws_port = atoi(arg_vector[2]);
  double xmin = stod(string(arg_vector[3]));
  double ymin = stod(string(arg_vector[4]));
  double xmax = stod(string(arg_vector[5]));
  double ymax = stod(string(arg_vector[6]));
  cout<<"The rooms as at ("<<xmin<<", "<<ymin<<") ("<<xmax<<", "<<ymax<<")\n";
  {
    //Make a world server object and connect to it
    WorldServer ws;
    if (not ws.connect(ws_ip, ws_port)) {
      std::cerr<<"Error connecting to the world server! Aborting!\n";
      return 0;
    }

    //std::map<world_server::URI, std::map<std::u16string, world_server::FieldData>> wds = ws.getDataObjects(u"pipsqueak.*");
    std::map<world_server::URI, std::map<std::u16string, world_server::FieldData>> wds_rx = ws.getDataObjects(u"pipsqueak.receiver.*");
    std::map<world_server::URI, std::map<std::u16string, world_server::FieldData>> wds_tx = ws.getDataObjects(u"pipsqueak.transmitter.*");

    if (wds_rx.empty()) {
      std::cerr<<"There are pipsqueak receiver! Aborting!\n";
      return 0;
    }
    //Pull data from the first pot
    for (auto object = wds_rx.begin(); object != wds_rx.end(); ++object) {
      std::cout<<"Checking object "<<std::string(object->first.begin(), object->first.end())<<'\n';
      //Check the attributes
      std::map<std::u16string, world_server::FieldData> atts = object->second;
      if (atts.end() == atts.find(u"location.x") or atts.end() == atts.find(u"location.y")) {
        std::cout<<"This pipsqueak doesn't have a location.\n";
      }
      else {
        //The IDs are stored as 64 bit integers
        double locx = readPrimitive<double>(atts[u"location.x"].data, 0);
        double locy = readPrimitive<double>(atts[u"location.y"].data, 0);
	  uint64_t id = readPrimitive<uint64_t>(atts[u"id"].data, 0);
        std::cout<<"("<<locx<<", "<<locy<<")\n";
        if (locx >= xmin and locx <= xmax and locy >= ymin and locy <= ymax) {
          good_rxers.push_back(object->first);
	    rxers.push_back(id);
        }
      }
    }

    if (wds_tx.empty()) {
      std::cerr<<"There are pipsqueaks! Aborting!\n";
      return 0;
    }
    //Pull data from the first pot
    for (auto object = wds_tx.begin(); object != wds_tx.end(); ++object) {
      std::cout<<"Checking object "<<std::string(object->first.begin(), object->first.end())<<'\n';
      //Check the attributes
      std::map<std::u16string, world_server::FieldData> atts = object->second;
      if (atts.end() == atts.find(u"location.x") or atts.end() == atts.find(u"location.y")) {
        std::cout<<"This pipsqueak doesn't have a location.\n";
      }
      else {
        //The IDs are stored as 64 bit integers
        double locx = readPrimitive<double>(atts[u"location.x"].data, 0);
        double locy = readPrimitive<double>(atts[u"location.y"].data, 0);
	  uint64_t id = readPrimitive<uint64_t>(atts[u"id"].data, 0);
        std::cout<<"("<<locx<<", "<<locy<<")\n";
        if (locx >= xmin and locx <= xmax and locy >= ymin and locy <= ymax) {
          good_txers.push_back(object->first);
	    txers.push_back(id);
        }
      }
    }
  }
/*
  cout<<"These are all of the pips in the room:\n";
  for (auto name = good_pips.begin(); name != good_pips.end(); ++name) {
    cout<<string(name->begin(), name->end())<<'\n';
  }
*/

  cout<<"These are all of pips in the room:\n";
  //cout<<"These are all of the receivers in the room:\n";
  for (auto name = good_rxers.begin(); name != good_rxers.end(); ++name) {
    cout<<string(name->begin(), name->end())<<'\n';
  }

  //cout<<"These are all of the transmitters in the room:\n";
  for (auto name = good_txers.begin(); name != good_txers.end(); ++name) {
    cout<<string(name->begin(), name->end())<<'\n';
  }

  cout<<"These are all of the pips in the room:\n";
  for (auto name = good_pips.begin(); name != good_pips.end(); ++name) {
    cout<<"\t"<<string(name->begin(), name->end())<<'\n';
  }

  return 0;
}
