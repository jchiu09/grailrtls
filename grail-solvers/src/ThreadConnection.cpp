#include "ThreadConnection.hpp"

#include <algorithm>
#include <iostream>
#include <chrono>

using namespace std::chrono;

//An internal list of connections
std::list<ThreadConnection*> ThreadConnection::connections;
//Locked before accessing the connections list
std::mutex ThreadConnection::tc_mutex;

//Removes any finished connections from the connections list.
void ThreadConnection::cleanFinished() {
  //Lock the connection mutex and sweep through the list, removing any finished connections.
  std::unique_lock<std::mutex> lck(tc_mutex);
  auto I = connections.begin();
  while (I != connections.end()) {
    //First time out the connection if it is stale.
    if (time(NULL) - (*I)->last_activity > (*I)->timeout) {
      std::cerr<<"Timing out connection to "<<(*I)->sock.ip_address()<<'\n';
      //Interrupt the thread and remove this instance from the list
      (*I)->interrupt();
      (*I)->finished = true;
    }

    if ((*I)->finished and
        (*I)->success.valid()) {
      //try {
        //std::cerr<<"Removing thread.\n";
        //milliseconds t_before = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
        //(*I)->this_thread.join();
        //milliseconds t_after = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
        //std::cerr<<"Waited "<<(t_after - t_before).count()<<" milliseconds for thread to join.\n";
      //}
      //catch (std::system_error e) {
        ////Thread is not joinable
        //std::cerr<<"Error joining thread before closing connection: "<<e.what()<<'\n';
      //}
      bool succ = (*I)->success.get();
      if (succ) {
        std::cerr<<"Successfully closing connection from "<<(*I)->sock.ip_address()<<'\n';
      }
      else {
        std::cerr<<"Closing connection from "<<(*I)->sock.ip_address()<<" after an unrecoverable error.\n";;
      }
      I = connections.erase(I);
    }
    else {
      ++I;
    }
  }
}

void ThreadConnection::forEach(std::function<void(ThreadConnection*)> f) {
  std::unique_lock<std::mutex> lck(tc_mutex);
  std::for_each(connections.begin(), connections.end(), f);
}

bool ThreadConnection::innerRun() {
  std::cerr<<"Running connection from "<<this->sock.ip_address()<<':'<<this->sock.port()<<'\n';
  try {
    this->run();
    finished = true;
    return true;
  }
  catch (std::system_error& err) {
    std::cerr<<"Thread connection dying with runtime error: "<<err.code().message()<<
      " in connection to "<<this->sock.ip_address()<<':'<<this->sock.port()<<'\n';
  }
  catch (std::runtime_error& err) {
    std::cerr<<"Thread connection dying with system error: "<<err.what()<<
      " in connection to "<<this->sock.ip_address()<<':'<<this->sock.port()<<'\n';
  }
  return false;
}

///Default to 30 seconds for the timeout
ThreadConnection::ThreadConnection(ClientSocket&& ref_sock, time_t timeout) : sock(std::forward<ClientSocket>(ref_sock)), timeout(timeout){
  last_activity = time(NULL);
  finished = false;
};

void ThreadConnection::setActive() {
  last_activity = time(NULL);
}

time_t ThreadConnection::lastActive() {
  return last_activity;
}

size_t ThreadConnection::receive(std::vector<unsigned char>& buff) {
  size_t size = sock.receive(buff);
  last_activity = time(NULL);
  return size;
}
void ThreadConnection::send(const std::vector<unsigned char>& buff) {
  sock.send(buff);
  last_activity = time(NULL);
}
ClientSocket& ThreadConnection::sockRef() {
  return sock;
}

void ThreadConnection::makeNewConnection(ClientSocket&& sock, std::function<ThreadConnection* (ClientSocket&& sock)> fun) {
  if (sock.ip_address() != "") {
    std::cerr<<"Got a connection from "<<sock.ip_address()<<".\n";
  }
  if (sock) {
    std::unique_lock<std::mutex> lck(tc_mutex);
    //Add this new connection to the solver connection list and then give it the socket.
    connections.push_front(fun(std::forward<ClientSocket>(sock)));
    //TODO Would std::thread be better here so we could detach it?
    std::cerr<<"Starting connection.\n";
    connections.front()->success = std::async(std::launch::async, std::mem_fun(&ThreadConnection::innerRun), connections.front());
  }
}

