#include <iostream>
#include <limits>
#include <string>
#include <regex>
#include <sstream>
#include <fstream>
#include <map>
#include <numeric>
#include <functional>

#include <M2.hpp>
#include <tile_localize.hpp>
#include <compass_localize.hpp>
#include <celocalize.hpp>
#include <multidimensional_slicing.hpp>

#include "grailV3_solver_client.hpp"
#include "netbuffer.hpp"

using namespace std;

/*
typedef int ReceiverID;
typedef int TransmitterID;
*/

vector<double> pip_whip_gain{-5.0};

vector<double> pip_parabola_gain{-1.2, -2, -4.33, -5.0, -7.0, -9.0, -10.0, -9.0, -9.0,
                                 -10.0, -9.0, -9.0, -10.0, -9.0, -7.0, -5.0, -4.33, -2};

//vector<double> small_hood_gain{-10, -12, -17.5, -21, -22.0, -21, -17.5, -12.0};
//vector<double> small_hood_gain{-5, -7, -12.5, -16, -17.0, -16, -12.5, -7.0};
//vector<double> small_hood_gain{-1, -3, -8.5, -12, -13.0, -12, -8.5, -3.0};
vector<double> small_hood_gain{-3, -5, -10.5, -14, -15.0, -14, -10.5, -5.0};

//A sample with all of the RSS values from a transmitter
struct RawSample {
  double x;
  double y;
  map<ReceiverID, vector<double>> rssis;
};

Sample rawToSample(RawSample& rs, vector<ReceiverID> rxorder) {
  Sample s{rs.x, rs.y, {}};
  //for (ReceiverID id : rxorder) {
  for (auto id = rxorder.begin(); id != rxorder.end(); ++id) {
    //Use the median RSS at ths receiver
    vector<double> rssis = rs.rssis[*id];
    if (0 < rssis.size()) {
      sort(rssis.begin(), rssis.end());
      s.rssis.push_back(rssis[rssis.size()/2]);
    } else {
      //Populate values of -1 if there is no data at this location.
      s.rssis.push_back(-1);
    }
  }
  return s;
}

vector<Sample> rawToTimeSeries(RawSample& rs, vector<ReceiverID> rxorder) {
  vector<Sample> samples;
  //Keep pulling out time slices until we run out of time periods.
  for (size_t time_period = 0; true; ++time_period) {
    Sample s{rs.x, rs.y, {}};
    for (auto id = rxorder.begin(); id != rxorder.end(); ++id) {
      //Quit if there are no more sample points here
      vector<double> rssis = rs.rssis[*id];
      if (rssis.size() > 0 and rssis.size() <= time_period) {
        return samples;
      }
      else {
        if (rssis.size() == 0) {
          //Populate values of -1 if there is no data at this location.
          s.rssis.push_back(-1);
        }
        else {
          s.rssis.push_back(rssis[time_period]);
        }
      }
    }
    samples.push_back(s);
  }
}

//Compute the sample standard deviation of these RSS values at each receiver
Sample rawToStddevSample(RawSample& rs, vector<ReceiverID> rxorder) {
  const float qnan = std::numeric_limits<float>::quiet_NaN();
  Sample s{rs.x, rs.y, {}};
  //for (ReceiverID id : rxorder) {
  for (auto id = rxorder.begin(); id != rxorder.end(); ++id) {
    //Calculate the standard deviation using signal values at this receiver
    vector<double> rssis = rs.rssis[*id];
    if (1 < rssis.size()) {

      double avg = accumulate(rssis.begin(), rssis.end(), 0.0,
          [&](double a, double b){return a + (b/rssis.size());});
      double ssquares = accumulate(rssis.begin(), rssis.end(), 0.0,
          [&](double a, double b){return a + pow(avg - b, 2.0);});
      s.rssis.push_back(sqrt(ssquares / (rssis.size() - 1)));
    } else {
      //Populate values of qnan if there is no data at this location.
      s.rssis.push_back(qnan);
    }
  }
  return s;
}

pair<double, double> getCentroid(vector<Tile>& tiles, double tile_w, double tile_h) {
  if (0 == tiles.size()) {
    return make_pair(numeric_limits<double>::quiet_NaN(), numeric_limits<double>::quiet_NaN());
  }
  double total_x = 0.0;
  double total_y = 0.0;
  for (auto T = tiles.begin(); T != tiles.end(); ++T) {
    total_x += T->column * tile_w + tile_w/2.0;
    total_y += T->row * tile_h + tile_h/2.0;
  }
  return make_pair(total_x / tiles.size(), total_y / tiles.size());
}

ostream& operator<<(ostream& os, const vector<Tile>& tiles) {
  if (0 == tiles.size()) { return os; }
  os<<tiles[0].column<<","<<tiles[0].row;
  for (auto T = tiles.begin()+1; T != tiles.end(); ++T) {
    os<<'\t'<<T->column<<","<<T->row;
  }
  return os;
}

bool operator==(const Tile& t1, const Tile& t2) {
  return t1.column == t2.column and t1.row == t2.row;
}

bool operator<(const Tile& t1, const Tile& t2) {
  return t1.row < t2.row or (t1.row == t2.row and t1.column < t2.column);
}

//Returns the error distance of this data and solver
double evaluateData(int target_id, map<TransmitterID, RawSample> raw_samples,
                    map<ReceiverID, pair<double, double>>& rx_locations,
                    map<ReceiverID, vector<double>>& antenna_gains,
                    string solver, double maxx, double maxy) {
  const float qnan = std::numeric_limits<float>::quiet_NaN();
  if (raw_samples.find(target_id) == raw_samples.end()) {
    cerr<<"No data for target "<<target_id<<'\n';
    return qnan;
  }
  //Set up the landmark data for the solver.
  vector<ReceiverID> rxorder;
  LandmarkData lm_data;
  for (auto p = rx_locations.begin(); p != rx_locations.end(); ++p) {
    rxorder.push_back(p->first);
    lm_data.push_back(p->second);
  }
  double real_x = raw_samples[target_id].x;
  double real_y = raw_samples[target_id].y;
  raw_samples[target_id].x = qnan;
  raw_samples[target_id].y = qnan;
  Sample tar_sample = rawToSample(raw_samples[target_id], rxorder);
  vector<Sample> tar_series = rawToTimeSeries(raw_samples[target_id], rxorder);
  Sample tar_stddev = rawToStddevSample(raw_samples[target_id], rxorder);
  //Remove the target from the raw samples so that it cannot be used
  //to localize itself.
  raw_samples.erase(raw_samples.find(target_id));

  TrainingData td;
  for (auto rs = raw_samples.begin(); rs != raw_samples.end(); ++rs) {
    td.push_back( rawToSample(rs->second, rxorder));
  }

  vector<TrainingData> td_series;
  for (auto S = tar_series.begin(); S != tar_series.end(); ++S) {
    TrainingData step_td = td;
    step_td.push_back(*S);
    td_series.push_back(step_td);
  }

  //Now push back the target sample using the median RSS value into the training data.
  td.push_back(tar_sample);

  //TODO FIXME This is debugging stuff to evaluate stddev to distance correlation
  for (auto rxid = rxorder.begin(); rxid != rxorder.end(); ++rxid) {
    double xdiff = rx_locations[*rxid].first - real_x;
    double ydiff = rx_locations[*rxid].second - real_y;
    double distance = sqrt(pow(xdiff, 2.0) + pow(ydiff, 2.0));
    std::cout<<"Stddev: "<<distance<<"\t"<<tar_stddev.rssis[std::distance(rxorder.begin(), rxid)]<<'\n';
  }

  /*
  //Change all unknown values to RSS of -100
  for_each(td.begin(), td.end(),
      [=](Sample& s) { transform(s.rssis.begin(), s.rssis.end(), s.rssis.begin(),
        [=](double d){ if (isnan(d) or -1 == d) return -100.0; else return d;;});});
        */
  //Change all unknown values to NaNs
  for_each(td.begin(), td.end(),
      [=](Sample& s) { transform(s.rssis.begin(), s.rssis.end(), s.rssis.begin(),
        [=](double d){ if (isnan(d) or -1 == d) return numeric_limits<double>::quiet_NaN(); else return d;});});

  //Format the data differently for many-dimensional solvers
  MultiDTrainingData tdata;
  for (auto I = td.begin(); I != td.end(); ++I) {
    MultiDSample ms;
    ms.rssis = I->rssis;
    ms.coordinates = std::vector<double>{I->x, I->y};
    tdata.push_back(ms);
  }
  std::vector<std::vector<double>> multid_lmdata(lm_data.size());
  std::transform(lm_data.begin(), lm_data.end(), multid_lmdata.begin(),
      [](std::pair<double, double>& p) { return std::vector<double>{p.first, p.second};});
  std::vector<double> domains{maxx, maxy};

  int burnin = 1000;
  int additional_iter = 5000;
  SampleMethod s_method = slice1;

  //The power at a distance of 0 from the receiver
  double initial_loss = -5.0;
  tile_localize::dataToParameter toAlpha = [=](Point rxer, Point sample, double rss) {
    double distance = euclDistance(rxer, sample);
    //Determine alpha from the equation: Loss = 10*alpha*log10(distance) + initial_loss
    double alpha = (rss - initial_loss) / ( 10 * log10(distance));
    return abs(alpha);
  };
  //TODO FIXME HERE If any data points have a value of -1 they should not be used as vertices in
  //the initial map used for interpolation.
  //TODO Need a way to remove a vertex from the delaunay triangle mesh or I need to
  //rebuild the mesh for every receiver.

  tile_localize::parameterToData toRSS = [=](Point rxer, Point sample, double alpha)->double {
    double distance = euclDistance(rxer, sample);
    if (0.0 == distance) { return initial_loss;}
    //Determine the rss from the equation: Loss = 10*alpha*log10(distance) + initial_loss
    double rss = -1 * alpha * (10 * log10(distance)) + initial_loss;
    return rss;
  };


  if (solver == "multiM2") {
    md_slicing::SampleMethod md_method = md_slicing::slice1;
    //md_slicing::SampleMethod md_method = md_slicing::slice1all;
    //Change all unknown values to RSS of -100
    for_each(td.begin(), td.end(),
        [=](Sample& s) { transform(s.rssis.begin(), s.rssis.end(), s.rssis.begin(),
          [=](double d){ if (isnan(d) or -1 == d) return -100.0; else return d;;});});
    vector<md_slicing::Result> results = md_slicing::multiD_m2solve(0, NULL, tdata, multid_lmdata, domains, burnin, additional_iter, md_method);
    if (results.size() > 0) {
      double error = sqrt(powf(results[0].coordinates[0] - real_x, 2.0) + powf(results[0].coordinates[1] - real_y, 2.0));
      cout<<"Got "<<results.rbegin()->coordinates[0]<<", "<<results.rbegin()->coordinates[1]<<" and ground truth is "<<
        real_x<<", "<<real_y<<"\tError "<<error<<'\n';
      return error;
    }
    else {
      cout<<"Got no solution for ground truth "<<real_x<<", "<<real_y<<'\n';
      return qnan;
    }
  }
  //Many dimensional M2 with a simulated third dimension
  else if (solver == "multiM2_3") {
    //Add in a third dimension with all coordinates of 0.0
    for (auto I = tdata.begin(); I != tdata.end(); ++I) {
      I->coordinates.push_back(0.0);
    }
    for (auto I = multid_lmdata.begin(); I != multid_lmdata.end(); ++I) {
      I->push_back(0.0);
    }
    md_slicing::SampleMethod md_method = md_slicing::slice1;
    //md_slicing::SampleMethod md_method = md_slicing::slice1all;
    //Change all unknown values to RSS of -100
    for_each(td.begin(), td.end(),
        [=](Sample& s) { transform(s.rssis.begin(), s.rssis.end(), s.rssis.begin(),
          [=](double d){ if (isnan(d) or -1 == d) return -100.0; else return d;;});});
    domains.push_back(0.0);
    vector<md_slicing::Result> results = md_slicing::multiD_m2solve(0, NULL, tdata, multid_lmdata, domains, burnin, additional_iter, md_method);
    if (results.size() > 0) {
      double error = sqrt(powf(results[0].coordinates[0] - real_x, 2.0) + powf(results[0].coordinates[1] - real_y, 2.0));
      cout<<"Got "<<results.rbegin()->coordinates[0]<<", "<<results.rbegin()->coordinates[1]<<" and ground truth is "<<
        real_x<<", "<<real_y<<"\tError "<<error<<'\n';
      return error;
    }
    else {
      cout<<"Got no solution for ground truth "<<real_x<<", "<<real_y<<'\n';
      return qnan;
    }
  }
  else if (solver == "M2") {
    //Change all unknown values to RSS of -100
    for_each(td.begin(), td.end(),
        [=](Sample& s) { transform(s.rssis.begin(), s.rssis.end(), s.rssis.begin(),
          [=](double d){ if (isnan(d) or -1 == d) return -100.0; else return d;;});});
    vector<Result> results = m2solve(0, NULL, td, lm_data, maxx, maxy, burnin, additional_iter, s_method);
    if (results.size() > 0) {
      double error = sqrt(powf(results[0].x - real_x, 2.0) + powf(results[0].y - real_y, 2.0));
      cout<<"Got "<<results.rbegin()->x<<", "<<results.rbegin()->y<<" and ground truth is "<<
        real_x<<", "<<real_y<<"\tError "<<error<<'\n';
      return error;
    }
    else {
      cout<<"Got no solution for ground truth "<<real_x<<", "<<real_y<<'\n';
      return qnan;
    }
  }
  else if (solver == "M2tile") {
    double tile_w = 8;
    double tile_h = 8;
    double percentile = 0.50;
    vector<vector<Tile>> result = m2solveBoxes(0, NULL, td, lm_data, maxx, maxy,
        burnin, additional_iter, s_method, tile_w, tile_h, percentile);
    //Count the error from the centroid - first find the center
    if (0 == result.size()) {
      cout<<"Got no solution for ground truth "<<real_x<<", "<<real_y<<'\n';
      return qnan;
    }
    pair<double, double> center = getCentroid(result[0], tile_w, tile_h);
    double error = sqrt(powf(center.first - real_x, 2.0) + powf(center.second - real_y, 2.0));

    cout<<"Tiles: "<<result[0]<<'\n';
    cout<<"Center is "<<center.first<<", "<<center.second<<" and ground truth is "<<
      real_x<<", "<<real_y<<"\tError "<<error<<'\n';
    return error;
  }
  else if (solver == "CE") {
    double resolution = 8.0;
    vector<CELocalize::Result> result;
    result = CELocalize::localize(td, lm_data, maxx, maxy, resolution);

    //Count the error from the given point
    double error = sqrt(powf(result[0].x - real_x, 2.0) + powf(result[0].y - real_y, 2.0));

    cout<<"Location is "<<result[0].x<<", "<<result[0].y<<" and ground truth is "<<
      real_x<<", "<<real_y<<"\tError "<<error<<'\n';
    return error;
  }
  else if (solver == "BL") {
    double resolution = 8.0;
    vector<CELocalize::Result> result;
    result = CELocalize::bayesLocalize(td, lm_data, maxx, maxy, resolution);

    //Count the error from the given point
    double error = sqrt(powf(result[0].x - real_x, 2.0) + powf(result[0].y - real_y, 2.0));

    cout<<"Location is "<<result[0].x<<", "<<result[0].y<<" and ground truth is "<<
      real_x<<", "<<real_y<<"\tError "<<error<<'\n';
    return error;
  }
  else if (solver == "radialBL") {
    double resolution = 8.0;
    vector<CELocalize::Result> result;
    //Set the gain pattern of each receiver
    vector<vector<double>> gains(lm_data.size(), pip_whip_gain);
    //for_each(gains.begin(), gains.end(), [&](vector<double>& v ) { v = pip_whip_gain;});
    for (size_t rxer = 0; rxer < lm_data.size(); ++rxer) {
      std::cerr<<"Rxer "<<rxer<<" is in entry number "<<rxorder[rxer]<<'\n';
      if (antenna_gains.end() == antenna_gains.find(rxorder[rxer])) {
        gains[rxer] = pip_whip_gain;
        std::cerr<<"Using default whip gain with "<<gains[rxer].size()<<" entries\n";
      }
      else {
        gains[rxer] = antenna_gains[rxorder[rxer]];
        std::cerr<<"Using given gain with "<<gains[rxer].size()<<" entries\n";
      }
    }
    std::cerr<<"First entry in first gains is "<<gains[0].size()<<'\n';

    result = CELocalize::radialBayesLocalize(td, lm_data, maxx, maxy, resolution, gains);

    //Count the error from the given point
    double error = sqrt(powf(result[0].x - real_x, 2.0) + powf(result[0].y - real_y, 2.0));

    cout<<"Location is "<<result[0].x<<", "<<result[0].y<<" and ground truth is "<<
      real_x<<", "<<real_y<<"\tError "<<error<<'\n';
    return error;
  }
  else if (solver == "radialBLtime") {
    double resolution = 8.0;
    //Set the gain pattern of each receiver
    vector<vector<double>> gains(lm_data.size(), pip_whip_gain);
    //for_each(gains.begin(), gains.end(), [&](vector<double>& v ) { v = pip_whip_gain;});
    for (size_t rxer = 0; rxer < lm_data.size(); ++rxer) {
      if (antenna_gains.end() == antenna_gains.find(rxorder[rxer])) {
        gains[rxer] = pip_whip_gain;
      }
      else {
        gains[rxer] = antenna_gains[rxorder[rxer]];
      }
    }

    vector<CELocalize::Result> time_results;
    for (auto S = td_series.begin(); S != td_series.end(); ++S) {
      vector<CELocalize::Result> result;
      result = CELocalize::radialBayesLocalize(*S, lm_data, maxx, maxy, resolution, gains);
      time_results.push_back(result[0]);
    }
    //Find the average x and y locations.
    double avg_x = 0.0;
    double avg_y = 0.0;
    for (auto R = time_results.begin(); R != time_results.end(); ++R) {
      avg_x += R->x / time_results.size();
      avg_y += R->y / time_results.size();
    }

    //Count the error from the given point
    double error = sqrt(powf(avg_x - real_x, 2.0) + powf(avg_y - real_y, 2.0));

    cout<<"Location is "<<avg_x<<", "<<avg_y<<" and ground truth is "<<
      real_x<<", "<<real_y<<"\tError "<<error<<'\n';
    return error;
  }
  else if (solver == "bayeswalk") {
    double resolution = 8.0;
    vector<CELocalize::Result> result;
    //Set the gain pattern of each receiver
    vector<vector<double>> gains(lm_data.size(), pip_whip_gain);
    //for_each(gains.begin(), gains.end(), [&](vector<double>& v ) { v = pip_whip_gain;});
    for (size_t rxer = 0; rxer < lm_data.size(); ++rxer) {
      std::cerr<<"Rxer "<<rxer<<" is in entry number "<<rxorder[rxer]<<'\n';
      if (antenna_gains.end() == antenna_gains.find(rxorder[rxer])) {
        gains[rxer] = pip_whip_gain;
        std::cerr<<"Using default whip gain with "<<gains[rxer].size()<<" entries\n";
      }
      else {
        gains[rxer] = antenna_gains[rxorder[rxer]];
        std::cerr<<"Using given gain with "<<gains[rxer].size()<<" entries\n";
      }
    }
    std::cerr<<"First entry in first gains is "<<gains[0].size()<<'\n';

    result = CELocalize::bayesWalkLocalize(td, lm_data, maxx, maxy, resolution, gains);

    //Count the error from the given point
    double error = sqrt(powf(result[0].x - real_x, 2.0) + powf(result[0].y - real_y, 2.0));

    cout<<"Location is "<<result[0].x<<", "<<result[0].y<<" and ground truth is "<<
      real_x<<", "<<real_y<<"\tError "<<error<<'\n';
    return error;
  }
  else if (solver == "bayeswalkvote") {
    double resolution = 8.0;
    vector<CELocalize::Result> result;
    //Set the gain pattern of each receiver
    vector<vector<double>> gains(lm_data.size(), pip_whip_gain);
    //for_each(gains.begin(), gains.end(), [&](vector<double>& v ) { v = pip_whip_gain;});
    for (size_t rxer = 0; rxer < lm_data.size(); ++rxer) {
      std::cerr<<"Rxer "<<rxer<<" is in entry number "<<rxorder[rxer]<<'\n';
      if (antenna_gains.end() == antenna_gains.find(rxorder[rxer])) {
        gains[rxer] = pip_whip_gain;
        std::cerr<<"Using default whip gain with "<<gains[rxer].size()<<" entries\n";
      }
      else {
        gains[rxer] = antenna_gains[rxorder[rxer]];
        std::cerr<<"Using given gain with "<<gains[rxer].size()<<" entries\n";
      }
    }
    std::cerr<<"First entry in first gains is "<<gains[0].size()<<'\n';

    result = CELocalize::bayesWalkVoteLocalize(td, lm_data, maxx, maxy, resolution, gains);

    //Count the error from the given point
    double error = sqrt(powf(result[0].x - real_x, 2.0) + powf(result[0].y - real_y, 2.0));

    cout<<"Location is "<<result[0].x<<", "<<result[0].y<<" and ground truth is "<<
      real_x<<", "<<real_y<<"\tError "<<error<<'\n';
    return error;
  }
  else if (solver == "normalizedABP") {
    double resolution = 1.0;
    vector<CELocalize::Result> result;
    //Set the gain pattern of each receiver
    vector<vector<double>> gains(lm_data.size(), pip_whip_gain);
    //for_each(gains.begin(), gains.end(), [&](vector<double>& v ) { v = pip_whip_gain;});
    for (size_t rxer = 0; rxer < lm_data.size(); ++rxer) {
      std::cerr<<"Rxer "<<rxer<<" is in entry number "<<rxorder[rxer]<<'\n';
      if (antenna_gains.end() == antenna_gains.find(rxorder[rxer])) {
        gains[rxer] = pip_whip_gain;
        std::cerr<<"Using default whip gain with "<<gains[rxer].size()<<" entries\n";
      }
      else {
        gains[rxer] = antenna_gains[rxorder[rxer]];
        std::cerr<<"Using given gain with "<<gains[rxer].size()<<" entries\n";
      }
    }
    std::cerr<<"First entry in first gains is "<<gains[0].size()<<'\n';

    result = CELocalize::areaBasedProbability(td, lm_data, maxx, maxy, resolution, gains);

    //Count the error from the given point
    double error = sqrt(powf(result[0].x - real_x, 2.0) + powf(result[0].y - real_y, 2.0));

    cout<<"Location is "<<result[0].x<<", "<<result[0].y<<" and ground truth is "<<
      real_x<<", "<<real_y<<"\tError "<<error<<'\n';
    return error;
  }
  else if (solver == "abpcluster") {
    double resolution = 1.0;
    vector<CELocalize::Result> result;
    //Set the gain pattern of each receiver
    vector<vector<double>> gains(lm_data.size(), pip_whip_gain);
    //for_each(gains.begin(), gains.end(), [&](vector<double>& v ) { v = pip_whip_gain;});
    for (size_t rxer = 0; rxer < lm_data.size(); ++rxer) {
      std::cerr<<"Rxer "<<rxer<<" is in entry number "<<rxorder[rxer]<<'\n';
      if (antenna_gains.end() == antenna_gains.find(rxorder[rxer])) {
        gains[rxer] = pip_whip_gain;
        std::cerr<<"Using default whip gain with "<<gains[rxer].size()<<" entries\n";
      }
      else {
        gains[rxer] = antenna_gains[rxorder[rxer]];
        std::cerr<<"Using given gain with "<<gains[rxer].size()<<" entries\n";
      }
    }
    std::cerr<<"First entry in first gains is "<<gains[0].size()<<'\n';

    result = CELocalize::clusterLocalization(td, lm_data, maxx, maxy, resolution, gains);

    //Count the error from the given point
    double error = sqrt(powf(result[0].x - real_x, 2.0) + powf(result[0].y - real_y, 2.0));

    cout<<"Location is "<<result[0].x<<", "<<result[0].y<<" and ground truth is "<<
      real_x<<", "<<real_y<<"\tError "<<error<<'\n';
    return error;
  }
  else if (solver == "normalizedABPtime") {
    double resolution = 1.0;
    //Set the gain pattern of each receiver
    vector<vector<double>> gains(lm_data.size(), pip_whip_gain);
    //for_each(gains.begin(), gains.end(), [&](vector<double>& v ) { v = pip_whip_gain;});
    for (size_t rxer = 0; rxer < lm_data.size(); ++rxer) {
      if (antenna_gains.end() == antenna_gains.find(rxorder[rxer])) {
        gains[rxer] = pip_whip_gain;
      }
      else {
        gains[rxer] = antenna_gains[rxorder[rxer]];
      }
    }

    vector<CELocalize::Result> time_results;
    for (auto S = td_series.begin(); S != td_series.end(); ++S) {
      vector<CELocalize::Result> result;
      result = CELocalize::areaBasedProbability(*S, lm_data, maxx, maxy, resolution, gains);
      time_results.push_back(result[0]);
    }
    //Find the average x and y locations.
    double avg_x = 0.0;
    double avg_y = 0.0;
    for (auto R = time_results.begin(); R != time_results.end(); ++R) {
      avg_x += R->x / time_results.size();
      avg_y += R->y / time_results.size();
    }


    //Count the error from the given point
    double error = sqrt(powf(avg_x - real_x, 2.0) + powf(avg_y - real_y, 2.0));

    cout<<"Location is "<<avg_x<<", "<<avg_y<<" and ground truth is "<<
      real_x<<", "<<real_y<<"\tError "<<error<<'\n';
    return error;
  }
  else if (solver == "SPM") {
    double tile_w = 8;
    double tile_h = 8;
    //TODO loop over increasing thresholds until a match is found - this is because some stuff keeps giving NaN results.
    //ABP shouldn't have this problem
    double threshold = 5;
    double error = qnan;
    pair<double, double> center;
    vector<tile_localize::Result> result;
    while ( isnan(error)) {
      using namespace tile_localize;
      result = tile_localize::SPMSolve(td, lm_data, maxx, maxy, tile_w, tile_h, threshold,
                                       identityFunction, identityFunction, demingRegression);

      //Count the error from the centroid - first find the center
      if (0 == result.size()) {
        cout<<"Got no solution for ground truth "<<real_x<<", "<<real_y<<'\n';
        return qnan;
      }
      center = getCentroid(result[0], tile_w, tile_h);
      error = sqrt(powf(center.first - real_x, 2.0) + powf(center.second - real_y, 2.0));
      threshold += 1.0;
    }
    cout<<"Tiles: "<<result[0]<<'\n';
    cout<<"Center is "<<center.first<<", "<<center.second<<" and ground truth is "<<
      real_x<<", "<<real_y<<"\tError "<<error<<'\n';
    return error;
  }
  else if (solver == "alphaSPM") {
    double tile_w = 8;
    double tile_h = 8;
    double threshold = 5;
    double error = qnan;
    pair<double, double> center;

    vector<tile_localize::Result> result;
    //Loop over increasing thresholds until a match is found
    while ( isnan(error)) {
      result = tile_localize::SPMSolve(td, lm_data, maxx, maxy, tile_w, tile_h, threshold, toAlpha, toRSS);

      //Count the error from the centroid - first find the center
      if (0 == result.size()) {
        cout<<"Got no solution for ground truth "<<real_x<<", "<<real_y<<'\n';
        return qnan;
      }
      center = getCentroid(result[0], tile_w, tile_h);
      error = sqrt(powf(center.first - real_x, 2.0) + powf(center.second - real_y, 2.0));
      threshold += 1.0;
    }
    cout<<"Tiles: "<<result[0]<<'\n';
    cout<<"Center is "<<center.first<<", "<<center.second<<" and ground truth is "<<
      real_x<<", "<<real_y<<"\tError "<<error<<'\n';
    return error;
  }
  else if (solver == "ABP") {
    double tile_w = 15;
    double tile_h = 15;
    double percentile = 0.50;
    double variance = 14;
    vector<tile_localize::Result> result;
    result = tile_localize::ABPSolve(td, lm_data, maxx, maxy, tile_w, tile_h, variance, percentile);

    //Count the error from the centroid - first find the center
    if (0 == result.size()) {
      cout<<"Got no solution for ground truth "<<real_x<<", "<<real_y<<'\n';
      return qnan;
    }
    pair<double, double> center = getCentroid(result[0], tile_w, tile_h);
    double error = sqrt(powf(center.first - real_x, 2.0) + powf(center.second - real_y, 2.0));

    cout<<"Tiles: "<<result[0]<<'\n';
    cout<<"Center is "<<center.first<<", "<<center.second<<" and ground truth is "<<
      real_x<<", "<<real_y<<"\tError "<<error<<'\n';
    return error;
  }
  else if (solver == "alphaABP") {
    double tile_w = 15;
    double tile_h = 15;
    double percentile = 0.50;
    double variance = 14;
    vector<tile_localize::Result> result;
    result = tile_localize::ABPSolve(td, lm_data, maxx, maxy, tile_w, tile_h, variance, percentile, toAlpha, toRSS);

    //Count the error from the centroid - first find the center
    if (0 == result.size()) {
      cout<<"Got no solution for ground truth "<<real_x<<", "<<real_y<<'\n';
      return qnan;
    }
    pair<double, double> center = getCentroid(result[0], tile_w, tile_h);
    double error = sqrt(powf(center.first - real_x, 2.0) + powf(center.second - real_y, 2.0));

    cout<<"Tiles: "<<result[0]<<'\n';
    cout<<"Center is "<<center.first<<", "<<center.second<<" and ground truth is "<<
      real_x<<", "<<real_y<<"\tError "<<error<<'\n';
    return error;
  }
  else if (solver == "compass") {
    Sample s = *td.rbegin();
    TrainingData trace(td.begin(), td.begin() + (td.size() - 1));
    double tile_w = 8;
    double tile_h = 8;
    double min_dist = 8;
    double min_diff = 4;
    double percentile = 0.05;

    vector<Tile> result = possibleCompassTiles(s, lm_data, maxx, maxy, tile_w, tile_h, min_dist, min_diff, percentile);
    pair<double, double> center = getCentroid(result, tile_w, tile_h);
    double error = sqrt(powf(center.first - real_x, 2.0) + powf(center.second - real_y, 2.0));

    cout<<"Tiles: "<<result<<'\n';
    cout<<"Center is "<<center.first<<", "<<center.second<<" and ground truth is "<<
      real_x<<", "<<real_y<<"\tError "<<error<<'\n';
    return error;
  }
  else if (solver == "M2tilecompass") {
    double tile_w = 8;
    double tile_h = 8;
    double percentile = 0.75;
    double min_dist = 8;
    double min_diff = 4;
    Sample s = *td.rbegin();
    vector<Tile> possibles = possibleCompassTiles(s, lm_data, maxx, maxy, tile_w, tile_h, min_dist, min_diff, percentile);
    vector<vector<Tile>> m2possibles = m2solveBoxes(0, NULL, td, lm_data, maxx, maxy,
        burnin, additional_iter, s_method, tile_w, tile_h, percentile);
    vector<Tile> result;
    //Only accept tiles that the compass also accepted
    for_each(m2possibles[0].begin(), m2possibles[0].end(), [&](Tile& t) {
        if (possibles.end() != find(possibles.begin(), possibles.end(), t)) result.push_back(t); });
    //Count the error from the centroid - first find the center
    if (0 == result.size()) {
      cout<<"Got no solution for ground truth "<<real_x<<", "<<real_y<<'\n';
      return qnan;
    }
    pair<double, double> center = getCentroid(result, tile_w, tile_h);
    double error = sqrt(powf(center.first - real_x, 2.0) + powf(center.second - real_y, 2.0));

    cout<<"Tiles: "<<result<<'\n';
    cout<<"Center is "<<center.first<<", "<<center.second<<" and ground truth is "<<
      real_x<<", "<<real_y<<"\tError "<<error<<'\n';
    return error;
  }
  else if (solver == "amalgam") {
    //Get the centers of the M2, alphaSPM and alphaABP solvers and center them.
    pair<double, double> m2center, abp_center, spm_center, compass_center;
    {
      vector<Result> results = m2solve(0, NULL, td, lm_data, maxx, maxy, burnin, additional_iter, s_method);
      if (results.size() > 0) {
        m2center = make_pair(results[0].x, results[0].y);
      }
      else {
        cout<<"Got no solution for ground truth "<<real_x<<", "<<real_y<<'\n';
        return qnan;
      }
    }
    {
      double tile_w = 15;
      double tile_h = 15;
      double percentile = 0.50;
      double variance = 14;
      vector<tile_localize::Result> result;
      result = tile_localize::ABPSolve(td, lm_data, maxx, maxy, tile_w, tile_h, variance, percentile, toAlpha, toRSS);

      //Count the error from the centroid - first find the center
      if (0 == result.size()) {
        cout<<"Got no solution for ground truth "<<real_x<<", "<<real_y<<'\n';
        return qnan;
      }
      abp_center = getCentroid(result[0], tile_w, tile_h);
    }
    {
      double tile_w = 8;
      double tile_h = 8;
      double threshold = 5;
      double error = qnan;

      vector<tile_localize::Result> result;
      //Loop over increasing thresholds until a match is found
      while ( isnan(error)) {
        result = tile_localize::SPMSolve(td, lm_data, maxx, maxy, tile_w, tile_h, threshold, toAlpha, toRSS);

        //Count the error from the centroid - first find the center
        if (0 == result.size()) {
          cout<<"Got no solution for ground truth "<<real_x<<", "<<real_y<<'\n';
          return qnan;
        }
        spm_center = getCentroid(result[0], tile_w, tile_h);
        error = sqrt(powf(spm_center.first - real_x, 2.0) + powf(spm_center.second - real_y, 2.0));
        threshold += 1.0;
      }
    }
    {
      Sample s = *td.rbegin();
      double tile_w = 8;
      double tile_h = 8;
      double min_dist = 8;
      double min_diff = 4;
      double percentile = 0.2;

      vector<Tile> result = possibleCompassTiles(s, lm_data, maxx, maxy, tile_w, tile_h, min_dist, min_diff, percentile);
      compass_center = getCentroid(result, tile_w, tile_h);
    }
    cout<<"Centers are "<<toPoint(m2center)<<"\t"<<toPoint(abp_center)<<"\t"<<toPoint(spm_center)<<"\t"<<toPoint(compass_center)<<endl;
    double x_center = (m2center.first + abp_center.first + spm_center.first + compass_center.first) / 4.0;
    double y_center = (m2center.second + abp_center.second + spm_center.second + compass_center.second) / 4.0;
    double error = sqrt(powf(x_center - real_x, 2.0) + powf(y_center - real_y, 2.0));
    cout<<"Center is "<<x_center<<", "<<y_center<<" and ground truth is "<<
      real_x<<", "<<real_y<<"\tError "<<error<<"\n\n";
    return error;
  }
  else if (solver == "amalgamtile") {
    double tile_w = 8;
    double tile_h = 8;
    vector<Tile> M2tiles, ABPtiles, compass_tiles;
    {
      double percentile = 0.50;
      vector<vector<Tile>> results = m2solveBoxes(0, NULL, td, lm_data, maxx, maxy,
          burnin, additional_iter, s_method, tile_w, tile_h, percentile);
      M2tiles = results[0];
    }
    {
      double percentile = 0.50;
      double variance = 10;
      vector<tile_localize::Result> results;
      results = tile_localize::ABPSolve(td, lm_data, maxx, maxy, tile_w, tile_h, variance, percentile, toAlpha, toRSS);
      ABPtiles = results[0];
    }
    {
      Sample s = *td.rbegin();
      double min_dist = 8;
      double min_diff = 4;
      double percentile = 0.2;

      compass_tiles = possibleCompassTiles(s, lm_data, maxx, maxy, tile_w, tile_h, min_dist, min_diff, percentile);
    }
    //Record every possible tile
    vector<Tile> possibles;
    for_each(M2tiles.begin(), M2tiles.end(), [&](Tile& t) {
        if (possibles.end() == find(possibles.begin(), possibles.end(), t)) possibles.push_back(t);});
    for_each(ABPtiles.begin(), ABPtiles.end(), [&](Tile& t) {
        if (possibles.end() == find(possibles.begin(), possibles.end(), t)) possibles.push_back(t);});
    for_each(compass_tiles.begin(), compass_tiles.end(), [&](Tile& t) {
        if (possibles.end() == find(possibles.begin(), possibles.end(), t)) possibles.push_back(t);});
    //Rank the tiles by their appearance in the scoring vectors.
    //First calculate the scores (lower is better)
    map<Tile, int> scores;
    for_each(possibles.begin(), possibles.end(), [&](Tile& t) {
        int score = 0;
        auto I = find(M2tiles.begin(), M2tiles.end(), t);
        if (I == M2tiles.end()) { score += M2tiles.size() + 1;}
        else score += distance(M2tiles.begin(), I);
        I = find(ABPtiles.begin(), ABPtiles.end(), t);
        if (I == ABPtiles.end()) { score += ABPtiles.size() + 1;}
        else score += distance(ABPtiles.begin(), I);
        I = find(compass_tiles.begin(), compass_tiles.end(), t);
        if (I == compass_tiles.end()) { score += compass_tiles.size() + 1;}
        else score += distance(compass_tiles.begin(), I);
        scores[t] = score;
        });
    sort(possibles.begin(), possibles.end(), [&](const Tile& a, const Tile& b) {
        return scores[a] < scores[b];});
    //Only accept tiles within 10% of the best score.
    int best_score = scores[possibles[0]];
    auto I = possibles.begin();
    while (scores[*I] < best_score * 1.1) {
      ++I;
    }
    //Erase the result not in the specified percentile.
    possibles.erase(I, possibles.end());
    pair<double, double> center = getCentroid(possibles, tile_w, tile_h);
    double error = sqrt(powf(center.first - real_x, 2.0) + powf(center.second - real_y, 2.0));

    cout<<"Tiles: "<<possibles<<'\n';
    cout<<"Center is "<<center.first<<", "<<center.second<<" and ground truth is "<<
      real_x<<", "<<real_y<<"\tError "<<error<<'\n';
    return error;
  }
  //TODO FIXME HERE Try checking for overlapping tiles in increasingly large percentiles to find an amalgamated solution.
  throw runtime_error("Solver " + solver + " is unknown.");
  return 0;
}

int main(int ac, char** av) {

  //The size of the entire winlab region. This can be overridden in the data input
  //with a line of the format:
  //REGION <xmin> <xmax> <ymin> <ymax>
  //Coordinates will then be translated into that system, with 0,0 at xmin,ymin
  double minx = 0;
  double miny = 0;
  double maxx = 218;
  double maxy = 169;
  double translatex = 0;
  double translatey = 0;

  //The locations of immobile transmitters and receivers
  map<ReceiverID, std::pair<double, double>> rx_locations;
  map<TransmitterID, std::pair<double, double>> tx_locations;
  //The gains of receivers
  map<ReceiverID, vector<double>> gains;

  rx_locations[196] = make_pair(106.2, 23);
  rx_locations[197] = make_pair(106.7, 28);
  rx_locations[635] = make_pair(108.7, 73.6);
  rx_locations[637] = make_pair(109.2, 67.3);
  rx_locations[639] = make_pair(138, 61);
  rx_locations[640] = make_pair(68.4, 49.2);
  rx_locations[641] = make_pair(77.3, 52);
  rx_locations[642] = make_pair(122.5, 24); //
  rx_locations[643] = make_pair(122.5, 30); //
  rx_locations[644] = make_pair(122.5, 37); //
  rx_locations[646] = make_pair(137, 69);
  rx_locations[647] = make_pair(130, 103);
  rx_locations[648] = make_pair(90.2, 112); //
  rx_locations[649] = make_pair(28.9, 75.5);
  rx_locations[650] = make_pair(130, 86);
  rx_locations[651] = make_pair(60.5, 23.5);
  rx_locations[652] = make_pair(71.9, 28);
  rx_locations[667] = make_pair(23.4, 71.9);

  //Stationary transmitters
  tx_locations[29] = make_pair(123, 37);
  tx_locations[33] = make_pair(99, 19);
  tx_locations[35] = make_pair(142, 24);
  tx_locations[36] = make_pair(47, 43);
  tx_locations[37] = make_pair(143, 64);
  tx_locations[38] = make_pair(72, 34);
  tx_locations[39] = make_pair(123, 3);
  tx_locations[40] = make_pair(123, 20);
  tx_locations[41] = make_pair(100, 34);
  tx_locations[42] = make_pair(77, 87);
  tx_locations[43] = make_pair(104, 79);
  tx_locations[44] = make_pair(97, 43);
  tx_locations[49] = make_pair(95, 55);
  tx_locations[50] = make_pair(115, 43);
  tx_locations[54] = make_pair(77, 24);
  tx_locations[64] = make_pair(43, 28);
  tx_locations[66] = make_pair(74, 49);
  tx_locations[73] = make_pair(55, 6);
  tx_locations[78] = make_pair(76, 5);
  tx_locations[82] = make_pair(65, 24);
  tx_locations[85] = make_pair(124, 75);
  tx_locations[86] = make_pair(104, 56);
  tx_locations[87] = make_pair(131, 75);

  if (ac == 4 or ac == 5) {
    tx_locations.clear();
    rx_locations.clear();
    //Read in transmitter and receiver locations from the world server if the world server address is given
    //Get the IDs and locations of stationary transmitters and receivers from
    //the world server.
    std::string ws_ip(av[ac-2]);
    int ws_port = atoi(av[ac-1]);
    //Reset the argument count for logic in later parts of the program
    ac -= 2;
    {
      WorldServer ws;
      if (not ws.connect(ws_ip, ws_port)) {
        std::cerr<<"Error connecting to the world server! Aborting!\n";
        return 0;
      }
      //First find fiduciary transmitters
      std::map<world_server::URI, std::map<std::u16string, world_server::FieldData>> wds = ws.getDataObjects(u"pipsqueak.transmitter.*");
      //Check each object to see if it has a location
      for (auto WD = wds.begin(); WD != wds.end(); ++WD) {
        std::map<std::u16string, world_server::FieldData> pip = WD->second;
        //Make sure the location and id fields exist and then read in their data.
        if (pip.end() != pip.find(u"location.x") and
            pip.end() != pip.find(u"location.y") and
            pip.end() != pip.find(u"id")) {
          uint64_t pip_id = readPrimitive<uint64_t>(pip[u"id"].data, 0);
          double pip_x = readPrimitive<double>(pip[u"location.x"].data, 0);
          double pip_y = readPrimitive<double>(pip[u"location.y"].data, 0);
          std::cerr<<"Transmitter id "<<pip_id<<" is at "<<pip_x<<", "<<pip_y<<'\n';
          tx_locations[pip_id] = make_pair(pip_x, pip_y);
        }
      }
      //Now repeat for the receivers
      wds = ws.getDataObjects(u"pipsqueak.receiver.*");
      for (auto WD = wds.begin(); WD != wds.end(); ++WD) {
        std::map<std::u16string, world_server::FieldData> pip = WD->second;
        //Make sure the location and id fields exist and then read in their data.
        if (pip.end() != pip.find(u"location.x") and
            pip.end() != pip.find(u"location.y") and
            pip.end() != pip.find(u"id")) {
          uint64_t pip_id = readPrimitive<uint64_t>(pip[u"id"].data, 0);
          double pip_x = readPrimitive<double>(pip[u"location.x"].data, 0);
          double pip_y = readPrimitive<double>(pip[u"location.y"].data, 0);
          std::cerr<<"Receiver id "<<pip_id<<" is at "<<pip_x<<", "<<pip_y<<'\n';
          rx_locations[pip_id] = make_pair(pip_x, pip_y);
          if (pip.end() != pip.find(u"antenna.pattern") and
              pip.end() != pip.find(u"antenna.rotation.degrees")) {
            vector<double> gain = pip_whip_gain;
            std::u16string gain_name = readUTF16(pip[u"antenna.pattern"].data, 0, pip[u"antenna.pattern"].data.size());
            std::cerr<<"Got antenna name "<<std::string(gain_name.begin(), gain_name.end())<<'\n';
            if (gain_name == u"whip") {
              gain = pip_whip_gain;
            }
            else if (gain_name == u"hood") {
              gain = small_hood_gain;
            }
            else {
              gain = pip_whip_gain;
            }
            double rotation = readPrimitive<double>(pip[u"antenna.rotation.degrees"].data, 0);
            std::cerr<<"Got antenna rotation "<<rotation<<'\n';
            double degrees_per_slice = 360.0 / gain.size();
            double slice_rotate = rint(rotation / degrees_per_slice);
            if (slice_rotate != 0 and slice_rotate < gain.size()) {
              std::cerr<<"Rotating by "<<slice_rotate<<" slices\n";
              std::rotate(gain.begin(), gain.begin() + (gain.size() - slice_rotate), gain.end());
            }
            gains[pip_id] = gain;
            std::cerr<<"Gain has "<<gains[pip_id].size()<<" entries\n";
            std::for_each(gain.begin(), gain.end(), [](double g) { std::cerr<<'\t'<<g;});
            std::cerr<<'\n';
          }
          else {
            gains[pip_id] = pip_whip_gain;
          }
        }
      }
      if (rx_locations.empty() or tx_locations.empty()) {
        std::cerr<<"Insufficient information on transmitter and receiver locations in the world server! Aborting!\n";
        return 0;
      }
    }
  }
  if (ac != 3 and ac != 2) {
    cerr<<"This program needs either one or two arguments.\n";
    cerr<<av[0]<<" <solver>\n";
    cerr<<"\tWith a single argument you specify the name of a solver to run.\n"<<
      "\tEach transmitter with a known location will be localized using all other fiduciary transmitters.\n";
    cerr<<av[0]<<" <target id> <solver>\n";
    cerr<<"\tLocalize for the target. Locations of the target are specifed by lines of type \"Sample x y\" in the input.\n";
    return 0;
  }
  int target_id = -1;
  string solver_name = "";
  if (ac == 2) {
    solver_name = string(av[1]);
  }
  else if (ac == 3) {
    target_id = stoi(string(av[1]));
    solver_name = string(av[2]);
  }

  //Raw samples
  map<TransmitterID, RawSample> raw_samples;
  const float qnan = std::numeric_limits<float>::quiet_NaN();
  string in;
  double curx = 0;
  double cury = 0;
  map<int, vector<float>> samples;
  int trials = 0;
  double sum_square_error = 0;
  double cum_error = 0.0;
  while (getline(cin, in)) {
    if ("" != in) {
      istringstream is(in);
      string word;
      if (is >> word) {
        //New time period begins.
        if ("Sample" == word) {
          //Evaluate data from the previous period.
          if (not raw_samples.empty()) {
            cerr<<"Evaluating data for position "<<curx<<", "<<cury<<'\n';
            double err = evaluateData(target_id, raw_samples, rx_locations, gains, solver_name, maxx, maxy);
            if (not isnan(err)) {
              ++trials;
              sum_square_error += powf(err, 2.0);
              cum_error += err;
            }
          }
          is >> curx >> cury;
          raw_samples.clear();
        } else if ("REGION" == word) {
          //Syntax is:
          //REGION <xmin> <xmax> <ymin> <ymax>
          double new_minx, new_maxx, new_miny, new_maxy;
          is >> new_minx >> new_maxx >> new_miny >> new_maxy;
          //Translate existing coordinates so that 0,0 is at xmin,ymin
          translatex = minx - new_minx;
          translatey = miny - new_miny;
          //Translate known locations
          for (auto RX = rx_locations.begin(); RX != rx_locations.end(); ++RX) {
            RX->second.first += translatex;
            RX->second.second += translatey;
          }
          for (auto TX = tx_locations.begin(); TX != tx_locations.end(); ++TX) {
            TX->second.first += translatex;
            TX->second.second += translatey;
          }
          //This command should appear before any data
          minx = new_minx;
          miny = new_miny;
          maxx = new_maxx + translatex;
          maxy = new_maxy + translatey;
          std::cerr<<"Translating by "<<translatex<<", "<<translatey<<'\n';
          std::cerr<<"Using maxx and maxy "<<maxx<<", "<<maxy<<'\n';
        } else {
          //Get packet data and store it.
          istringstream is2(word);
          int txid;
          int rxid;
          double rss;
          if ( is2 >> txid and is >> rxid >> rss) {
            //Check if this is the target transmitter or we know this transmitter's location
            if (target_id == txid or tx_locations.end() != tx_locations.find(txid)) {
              //Check if this is the first packet from a transmitter.
              if (raw_samples.end() == raw_samples.find(txid)) {
                raw_samples[txid] = RawSample{qnan, qnan, {}};
                //This transmitter has a known location.
                if (tx_locations.end() != tx_locations.find(txid)) {
                  raw_samples[txid].x = tx_locations[txid].first;
                  raw_samples[txid].y = tx_locations[txid].second;
                }
                if (txid == target_id) {
                  raw_samples[txid].x = curx + translatex;
                  raw_samples[txid].y = cury + translatey;
                }
              }
              //Enter the RSS data from txid to rxid
              raw_samples[txid].rssis[rxid].push_back(rss);
            }
          }
        }
      }
    }
  }
  //Evaluate the last data set
  if (not raw_samples.empty()) {
    //Check if a target was provided
    if (0 <= target_id) {
      cerr<<"Evaluating data for position "<<curx+translatex<<", "<<cury+translatey<<'\n';
      double err = evaluateData(target_id, raw_samples, rx_locations, gains, solver_name, maxx, maxy);
      if (not isnan(err)) {
        ++trials;
        sum_square_error += powf(err, 2.0);
        cum_error += err;
      }
    }
    //Otherwise localize each transmitter with a known location
    else {
      for (auto target = tx_locations.begin(); target != tx_locations.end(); ++target) {
        cerr<<"Localizaing transmitter "<<target->first<<" with solver "<<solver_name<<'\n';
        cerr<<"Evaluating data for position "<<target->second.first+translatex<<", "<<target->second.second+translatey<<'\n';
        double err = evaluateData(target->first.lower, raw_samples, rx_locations, gains, solver_name, maxx, maxy);
        std::cerr<<"Err is "<<err<<'\n';
        if (not isnan(err)) {
          ++trials;
          sum_square_error += powf(err, 2.0);
          cum_error += err;
          std::cerr<<"SSError and cumulative error are "<<sum_square_error<<" and "<<cum_error<<'\n';
        }
      }
    }
  }
  double rmse = sqrt(sum_square_error/trials);
  double avge = cum_error / trials;
  cout<<"RMSE is "<<rmse<<" with average error "<<avge<<'\n';
  return 0;
}
