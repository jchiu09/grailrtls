/*******************************************************************************
 * This file defines a class that simplifies connecting to the world model
 * as a solver.
 ******************************************************************************/

#include <solver_world_connection.hpp>
#include <grail_sock_server.hpp>
#include <netbuffer.hpp>
#include <simple_sockets.hpp>
#include <world_model_protocol.hpp>

#include <algorithm>
#include <functional>
#include <map>
#include <string>
#include <thread>

#include <iostream>

//Networking headers.
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

using namespace world_model;
using namespace std;

void SolverWorldConnection::registerTypes() {
  vector<solver::AliasType> aliases;
  for (auto soln = soln_to_alias.begin(); soln != soln_to_alias.end(); ++soln) {
    //Make an alias type for this solution type, then check if this is a
    //transient solution type.
    solver::AliasType at{soln->second, soln->first, false};
    if (transient_state.end() != transient_state.find(soln->second)) {
      at.transient = true;
    }
    aliases.push_back(at);
  }
  //Make and send a type announce message.
  s.send(makeTypeAnnounceMsg(aliases, origin));
}

/**
 * Reconnect to the world model after losing or closing a connection.
 * Returns true upon connection success, false otherwise.
 */
bool SolverWorldConnection::reconnect() {
  if (s.setup()) {
    std::cout<<"Connected to the GRAIL world model.\n";
  } else {
    std::cerr<<"Failed to connect to the GRAIL world model.\n";
    return false;
  }

  //Try to get the handshake message
  {
    std::vector<unsigned char> handshake = world_model::solver::makeHandshakeMsg();

    //Send the handshake message
    s.send(handshake);
    std::vector<unsigned char> raw_message(handshake.size());
    size_t length = s.receive(raw_message);

    //Check if the handshake message failed
    if (not (length == handshake.size() and
          std::equal(handshake.begin(), handshake.end(), raw_message.begin()) )) {
      std::cerr<<"Failure during handshake with world model.\n";
      return false;
    }
  }
  //Connection set up successfully
  _connected = true;

  //Now update the solution types with a type announce message
  registerTypes();

  return true;
}

void SolverWorldConnection::receiveThread() {
  using namespace world_model::solver;
  GRAILSockServer in_sock(s);
  while (not interrupted) {
    std::vector<unsigned char> raw_message = in_sock.getNextMessage(interrupted);
    //size_t piece_length = readPrimitive<uint32_t>(raw_message, 0);

    //Process this packet if it is valid
    if ( raw_message.size() >= 5 ) {
      //Handle the message according to its message type.
      solver::MessageID message_type = (solver::MessageID)raw_message[4];
      std::cerr<<"Getting a message.\n";

      if ( MessageID::start_transient == message_type ) {
        std::vector<uint32_t> transients = solver::decodeStartTransient(raw_message);
        for (auto T = transients.begin(); T != transients.end(); ++T) {
          if (transient_state.end() != transient_state.find(*T)) {
            transient_state[*T] = true;
          }
        }
      }
      else if ( MessageID::stop_transient == message_type ) {
        std::vector<uint32_t> transients = solver::decodeStopTransient(raw_message);
        for (auto T = transients.begin(); T != transients.end(); ++T) {
          if (transient_state.end() != transient_state.find(*T)) {
            transient_state[*T] = false;
          }
        }
      }
    }
  }
}

SolverWorldConnection::SolverWorldConnection(std::string ip, uint16_t port,
    std::u16string& origin, std::vector<std::pair<std::u16string, bool>>& solution_types) {
  _connected = false;
  interrupted = false;
  this->origin = origin;
  //Add the solution types into the soln_to_alias and transient state maps
  uint32_t cur_alias = 0;
  for (auto s_type = solution_types.begin(); s_type != solution_types.end(); ++s_type) {
    soln_to_alias[s_type->first] = cur_alias;
    //Put the transient types into the state map and set them to off (not streaming).
    if (s_type->second) {
      transient_state[cur_alias] = false;
    }
    ++cur_alias;
  }

  //Store these values so that we can reconnect later
  this->ip = ip;
  this->port = port;
  s.domain = AF_INET;
  s.type   = SOCK_STREAM;
  s.protocol = 0;
  s.port = port;
  s.ip_address = ip;
  reconnect();
  //Start a listening thread.
  rx_thread = std::thread(&SolverWorldConnection::receiveThread, this);
}

SolverWorldConnection::~SolverWorldConnection() {
  interrupted = true;
  //TODO FIXME Having something that could possible throw an exception in a destructor is bad.
  rx_thread.join();
}

solver::SolutionData SolverWorldConnection::makeSolution(const std::u16string& name,
    grail_time time, const URI& target, const std::vector<uint8_t>& data) {
  if (soln_to_alias.end() == soln_to_alias.find(name)) {
    throw std::runtime_error("Unknown solution type: " + std::string(name.begin(), name.end()));
  }
  return solver::SolutionData{soln_to_alias[name], time, target, data};
}

bool SolverWorldConnection::sendData(std::vector<solver::SolutionData> solutions) {
  if (not _connected and not reconnect()) {
    return false;
  }
  //Go through the solution list and remove anything that is transient but
  //not requested by the world model.
  for (auto I = solutions.begin(); I != solutions.end();) {
    //Remove unknown solutions or solutions that are transient and unrequested
    if (transient_state.end() == transient_state.find(I->type_alias) or
        transient_state[I->type_alias] == false) {
      I = solutions.erase(I);
    }
    else {
      ++I;
    }
  }
  //Send the provided solutions to the world model if there are any that need
  //to be sent
  if (solutions.size() != 0) {
    s.send(solver::makeSolutionMsg(solutions));
  }
  return true;
}

bool SolverWorldConnection::createURI(URI& uri) {
  if (not _connected and not reconnect()) {
    return false;
  }
  //Send this solver's origin name with the new uri
  s.send(solver::makeCreateURI(uri, origin));
  return true;
}

bool SolverWorldConnection::expireURI(URI& uri, grail_time expiration) {
  if (not _connected and not reconnect()) {
    return false;
  }
  //Send this solver's origin name with the new uri to expire
  s.send(solver::makeExpireURI(uri, origin, expiration));
  return true;
}

bool SolverWorldConnection::deleteURI(URI& uri) {
  if (not _connected and not reconnect()) {
    return false;
  }
  //Make a uri delete message
  s.send(solver::makeDeleteURI(uri, origin));
  return true;
}



/**
 * Returns true if this instance is connected to the world model,
 * false otherwise.
 */
bool SolverWorldConnection::connected() {
  return _connected;
}


