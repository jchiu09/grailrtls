#include <grailV3_solver_client.hpp>
#include "aggregator_solver_protocol.hpp"
#include <netbuffer.hpp>
#include <sample_data.hpp>
#include <world_model_protocol.hpp>
#include <grail_types.hpp>

#include "lda.h"
#include "passive_localization_utilities.h"

#define CHECK_INTERVAL 3
#define BACK_WINDOW 3

using namespace aggregator_solver;
using world_model::Attribute;
using world_model::grail_time;
using world_model::URI;

using namespace std;

vector<uint128_t> tx_ids;						// list of the transmitter IDs for passive localization
vector<uint128_t> rx_ids;						// list of the receiver IDs for passive localization
vector<uint128_t> sp_ids;						// list of the transmitter IDs for special purpose, e.g. click
vector<uint128_t> active_tx_ids;		// list of the transmitter IDs for active localization
vector< vector<double> > location;	// list of x and y coordinates of the each cell's center
vector< vector<double> > neighbour;	// list of the neighbour cell IDs for each cell

int cell_num;												// number of the cells
int link_num;												// number of distinguished pair of transmitter-receiver
int sample_num;											// number of the sample data vector for each cell
int attr_num;												// number of the attibutes for each cell, e.g. x and y coordinates
int target_num;											// number of the occupied cells to be estimated

struct Debug {
	bool on;
};

template<typename T>
Debug& operator<<(Debug& dbg, T arg) {
	if (dbg.on) {
		cout<<arg;
 	}
	return dbg;
}

// check if it's a "help" packet indicating the cell number
int help_packet(uint128_t tx_id){
	// 1: separate the data stream into training data and pass by, 2,3,4 TBD 
	int help_type = 0;
	for(int i = 0; i < sp_ids.size(); i++){
		if(tx_id == sp_ids[i]){
			help_type = i+1;
		}
	}
	return help_type;
}

// check the tx_id and rx_id of the sample data is valid 
bool data_packet(uint128_t tx_id, uint128_t rx_id) {
	bool tx_find = false;
	bool rx_find = false;

	for(int i = 0; i < tx_ids.size(); i++){
		if(tx_id == tx_ids[i]){
			tx_find = true;
		}
	}

	for(int i = 0; i < rx_ids.size(); i++){
		if(rx_id == rx_ids[i]){
			rx_find = true;
		}
	}
	return (tx_find * rx_find);
}

int main(int argc, char** argv) {

	if (argc < 6 or (argc % 2) == 0) {
		cerr<<"This program needs 5 or more arguments:\n";
		cerr<<"<dfp_config file> + <cell_config file> + <environmental RSS information> + <training data>"<<endl;
		cerr<<" + [<aggregator ip> <aggregator port>] + <world model ip> <solver port>\n";
		cerr<<"Any number of aggregator ip/port pairs may be provided to connect to multiple aggregators.\n";
		return 0;
	}

  //Grab the ip and ports for the aggregators and world model
  std::vector<NetTarget> aggregators;
  for (int s_num = 5; s_num < argc - 2; s_num += 2) {
    std::string aggr_ip(argv[s_num]);
    uint16_t aggr_port = std::stoi(std::string(argv[s_num + 1]));
    aggregators.push_back(NetTarget{aggr_ip, aggr_port});
  }

  //World model IP and solver port
  std::string wm_ip(argv[argc - 2]);
  int solver_port = std::stoi(std::string((argv[argc - 1])));

  //Set up the solver world model connection;
  std::string origin = "grail/passive localization solver\nversion 1.0";

  //Provide passive localization x-y solutions
  std::vector<std::pair<u16string, bool>> type_pairs{{u"location.xoffset.vector<double>", false}, 								
																										 {u"location.yoffset.vector<double>", false}};

  SolverWorldModel swm(wm_ip, solver_port, type_pairs, u16string(origin.begin(), origin.end()));
  if (not swm.connected()) {
    std::cerr<<"Could not connect to the world model as a solver - aborting.\n";
    return 0;
  }
	Rule winlab_rule;
	winlab_rule.physical_layer  = 1;
	winlab_rule.update_interval = 225;

	Subscription winlab_sub{winlab_rule};
	vector<Subscription> subs{winlab_sub};

	int buffer;
	int file_line = 0;
	string data;
	ifstream infile;

	infile.open(argv[1]);

	if (!infile){
		cout<<"dfp_config file open error!\n";
		return 0;
	}

	while (getline(infile, data)) {
		istringstream s(data);
		if(file_line == 0) {
			while(s>>buffer){
				tx_ids.push_back(buffer);
			}
		}
		else if (file_line == 1) {
			while(s>>buffer){
				rx_ids.push_back(buffer);
			}
		}
		else if (file_line == 2) {
			while(s>>buffer){
				sp_ids.push_back(buffer);
			}
		}
		else if (file_line == 3) {
			while(s>>buffer){
				active_tx_ids.push_back(buffer);
			}
		}
		else if (file_line == 4) {
			s>>cell_num>>sample_num>>attr_num>>target_num;
			for(int i = 0; i < cell_num; i++) {
				neighbour.push_back(vector<double>(cell_num, 0));
			}
			for(int i = 0; i < cell_num; i++) {
				location.push_back(vector<double>());
			}
		}
		else if (file_line > 4)	{
			int cell;
			s>>cell;
			neighbour[cell - 1][cell - 1] = 1;
			while(s>>buffer){
				neighbour[cell - 1][buffer - 1] = 1;	
			}
		}
		file_line++;
	}
	infile.close();

	infile.open(argv[2]);
	if (!infile){
		cout<<"dfp_cell_config file open error!\n";
		return 0;
	}

	while (getline(infile, data)) {
		istringstream s(data);
		int cell;
		s>>cell;
		double buffer;
		while(s>>buffer){
			location[cell-1].push_back(buffer);
		}
	}

	infile.close();

	link_num = tx_ids.size() * rx_ids.size();

	vector<double> static_median (link_num, 0);
	vector<double> static_var (link_num, 0);
	vector<double> static_num (link_num, 0);

	//read the environmental RSSI for correction
	infile.open(argv[3]);
	// no environmental information
	if (!infile){
		cerr<<"No environmental RSS information, set to 0 as default.\n";
	}
	else {
		int cell = 0;
		//get the environmental info line by line
		while(getline(infile, data)){
			istringstream s(data);
			double median = 0;
			double var = 0;
			double num = 0;

			s>>median>>var>>num;
			static_median[cell] = median;
			static_var[cell] = var;
			static_num[cell] = num;
			cell++;
		}
	}

	infile.close();

	static map<int, vector<vector<double> > > rssi_record;
	static map<int, vector<vector<double> > > rssi_buffer;
	static map<uint128_t, vector<double> > rssi_report;
	static vector<double> rssi_mean;
	static vector<double> posts;
	static vector<int> rank;
	static vector<double> prior(cell_num, 1); 	// the prior probability for each cell	
	static Debug debug{false};
	static time_t last_time = 0;
	static lda mylda(cell_num, link_num, sample_num);
	mylda.readfile(argv[4]);
	mylda.formulate();

	//Collect passive RSSI data
	//This function is not thread safe (but could be made so with a mutex)
	auto packetCallback = [&](SampleData& sample) {
		// Read the packets from all the links as a RSSI vector; drop the whole vector if any element misses
		if (data_packet(sample.tx_id, sample.rx_id)) {
			//Determine the calendar time from the milliseconds since the Jan 1, 1970 UTC.
			time_t time_seconds = time(NULL); 			// return the current time
			struct tm *time_parts;
			time_parts = localtime (&time_seconds);
			//Set the last time variable if it is unset
			if (last_time == 0) {
				last_time = time_seconds;
			}
			debug<<time_seconds<<"\t"<<last_time<<"\n";		
			//Log data and clear the transmitter counter every five second
			if (time_seconds - last_time == CHECK_INTERVAL) {
				debug<<"Time is checked\n";
				for(auto link_id = rssi_report.begin(); link_id != rssi_report.end(); link_id++){
					uint128_t id = link_id->first;
					if(rssi_report[id].size() == 0){
						cout<<"Not enough data for passive localization, wait for next period\n";
						last_time = time_seconds;
						rssi_report.clear();
						rssi_mean.clear();
						break;
					}	else {
						vector<double> temp = back_n(rssi_report[id], BACK_WINDOW);
						rssi_mean.push_back(getMean(temp)); 
					}
				}

				for (int i = 0; i < rssi_mean.size(); i++) {
					rssi_mean[i] = rssi_mean[i] - static_median[i];
				}
				posts = mylda.get_delta(rssi_mean);
				rank = getRank(posts);

				stringstream out;
				out<<asctime(time_parts);
				cout<<" The occupied cells at "<<out.str()<<endl;
				for(int i = 0; i < 32; i++)
					cout<<rank[i]<<"\t";
				cout<<endl;

				SolverWorldModel::Solution soln_x{u"location.xoffset.vector<double>", world_model::getGRAILTime(), 
																					u"winlab.passive entity", std::vector<uint8_t>()};
				SolverWorldModel::Solution soln_y{u"location.yoffset.vector<double>", world_model::getGRAILTime(), 
																					u"winlab.passive entity", std::vector<uint8_t>()};
		    pushBackVal<uint32_t>(target_num, soln_x.data);
		    pushBackVal<uint32_t>(target_num, soln_y.data);
				for (int i = 0; i < target_num; i++) {
		    	pushBackVal<double>(location[rank[i]-1][0], soln_x.data);
		    	pushBackVal<double>(location[rank[i]-1][1], soln_y.data);
				}
				std::vector<SolverWorldModel::Solution> solns{soln_x, soln_y};
		    swm.sendData(solns, true);
				last_time = time_seconds;
				rssi_report.clear();
				rssi_mean.clear();
				posts.clear();
				rank.clear();
			}

			debug<<"Got rx_id, tx_id, and RSS:\n\t";
			debug<<sample.rx_id<<'\t'<<sample.tx_id<<'\t'<<sample.rss<<'\n';

			//Record that this sensor reported rssi in
			int tx_sub = getIndex(tx_ids, sample.tx_id.lower);
			int rx_sub = getIndex(rx_ids, sample.rx_id.lower);

			uint128_t link_id = tx_sub * rx_ids.size() + rx_sub;
			rssi_report[link_id].push_back(sample.rss);
			debug<<"Receive packet from "<<sample.tx_id.lower<<" to "<<sample.rx_id.lower<<"\n";
		}
	};

	grailAggregatorConnect(aggregators, subs, packetCallback);

	while (1) {
		sleep(1);
	}
	return 0;
}
