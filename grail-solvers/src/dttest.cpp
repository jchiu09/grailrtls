/*
dttest.cpp - Sample program to test the triangulate functions.
Source version: January 11, 2011
Copyright (c) 2011 Bernhard Firner

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
  claim that you wrote the original software. If you use this software
  in a product, an acknowledgment in the product documentation would be
  appreciated but is not required.

  2. Altered source versions must be plainly marked as such, and must not be
  misrepresented as being the original software.

  3. This notice may not be removed or altered from any source
  distribution.
*/

#include <iostream>

#include <triangulate.hpp>

using namespace std;

int main() {
  double max_x = 10.0;
  double max_y = 10.0;
  vector<Point> vertices{Point{0, 0}, Point{0, max_y}, Point{max_x, 0}, Point{max_x, max_y}};
  vertices.push_back(Point{2,5});
  vertices.push_back(Point{8,6});
  vertices.push_back(Point{1,1});

  vector<Tri> result = genTriangles(max_x, max_y, vertices);

  cout<<"set xrange[-0.5:10.5]\nset yrange[-0.5:10.5]\n";

  for (auto T = result.begin(); T != result.end(); ++T) {
    cout<<"set arrow from "<<T->a<<" to "<<T->b<<" nohead\n";
    cout<<"set arrow from "<<T->b<<" to "<<T->c<<" nohead\n";
    cout<<"set arrow from "<<T->c<<" to "<<T->a<<" nohead\n";
    //cout<<*T<<'\n';
  }

  cout<<"set terminal png\n";
  cout<<"set output 'out.png'\n";
  cout<<"plot -2 notitle\n";

  return 0;
}
