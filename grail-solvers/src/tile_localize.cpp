/*
tile_localize.{cpp,hpp} - Functions for SPM and ABP localization
Source version: January 11, 2011
Copyright (c) 2011 Bernhard Firner

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
  claim that you wrote the original software. If you use this software
  in a product, an acknowledgment in the product documentation would be
  appreciated but is not required.

  2. Altered source versions must be plainly marked as such, and must not be
  misrepresented as being the original software.

  3. This notice may not be removed or altered from any source
  distribution.
*/

#include <cmath>
#include <stdexcept>
#include <iostream>
#include <limits>
#include <numeric>
#include <functional>
#include <sstream>
#include <utility>
#include <vector>

#include <tile_localize.hpp>
#include <triangulate.hpp>

using namespace tile_localize;
using namespace std;

struct SamplePoint {
  double x;
  double y;
  double rss;
};

/**
 * Computes the rss of a point at x and y using bilinear interpolation.
 * The tri argument contains three points who's signal values are
 * available in the given signal map.
 * This assumes that x and y lie inside the triangle formed by the three
 * given points.
 */
double bilinearInterpolate(Point p, const Tri& t, vector<vector<double>>& smap) {
  //Find the barycentric coordinates of the point, A, B, and C.
  //These will determine the contribution of RSS from each point.
  Triple bc = toBarycentric(p, t);
  return bc.a*smap[t.a.x][t.a.y] + bc.b*smap[t.b.x][t.b.y] + bc.c*smap[t.c.x][t.c.y];
}

//Returns the expected signal value at this point
double interpolate(Point p, vector<vector<double>>& smap, vector<Tri>& mesh) {
  auto t_i = findContainingTriangle(p, mesh);
  if ( mesh.end() != t_i) {
    return bilinearInterpolate(p, *t_i, smap);
  }
  //cerr<<"Couldn't find triangle for point "<<p<<endl;
  return numeric_limits<float>::quiet_NaN();
}

//Convenience struct to convert a coordinate to a tile position
struct Converter {
  double tile_size;
  int operator()(double location){
    //Truncate the result instead of rounding so we return the tile which
    //this coordinate lies inside. eg. [0,1] is tile 0, [1,2] is tile 1.
    if (location == 0.0) { return 0;}
    //Start counting from zero so subtract 1
    return (location-1.0)/tile_size;
  }
};

vector<double> tile_localize::demingRegression(vector<Point>& ps, Point rx_loc, ValueMap& smap) {
  //Assume that the values in smap follow a y=b0+b1x type of equation,
  //where x is the distance from a point in smap to rx_loc (the receiver loation),
  //y is the value of that point in smap, b0 is the y-value at distance 0, and
  //b1 is the slope.
  //Each point has some error in it though, so we'll do a least squares fit.
  //We could do this with a SVD but for a 2D dataset the Deming regression is simpler.
  //An explanation can be found at http://en.wikipedia.org/wiki/Deming_regression
  //A vector of distance-value pairs.
  vector <pair<double, double>> dist_val;
  for (size_t x = 0; x < smap.size(); ++x) {
    //Assume that smap is non-empty
    for (size_t y = 0; y < smap.at(0).size(); ++y) {
      if (not isnan(smap[x][y])) {
        dist_val.push_back(make_pair( euclDistance(rx_loc, Point{(double)x, (double)y}), smap[x][y]));
      }
    }
  }
  int total = dist_val.size();
  double xmean = accumulate(dist_val.begin(), dist_val.end(), 0.0,
      [&](double m, pair<double, double>& dv) { return m + dv.first/total; });
  double ymean = accumulate(dist_val.begin(), dist_val.end(), 0.0,
      [&](double m, pair<double, double>& dv) { return m + dv.second/total; });
  double xvar = accumulate(dist_val.begin(), dist_val.end(), 0.0,
      [&](double m, pair<double, double>& dv) { return m + pow(dv.first-xmean, 2.0)/total; });
  double yvar = accumulate(dist_val.begin(), dist_val.end(), 0.0,
      [&](double m, pair<double, double>& dv) { return m + pow(dv.second-ymean, 2.0)/total; });
  double covar = accumulate(dist_val.begin(), dist_val.end(), 0.0,
      [&](double m, pair<double, double>& dv) { return m + (dv.first-xmean)*(dv.second-ymean)/total; });

  //Assume that the ratio of the variance of y errors to the variance of x errors is the same.
  //TODO The y variance is probably greater than the x variance.
  double ratio = 1.0;
  //Estimate the slope
  double b1 = (yvar - ratio*xvar + sqrt(pow((yvar - ratio*xvar), 2.0) + 4*ratio*covar))/(2*covar);
  //Determine the initial value from the slope and x and y means.
  double b0 = ymean - b1*xmean;
  vector<double> results;
  for_each(ps.begin(), ps.end(), [&](Point& p) {
      if (isnan(smap[p.x][p.y])) results.push_back(b0 + b1 * euclDistance(rx_loc, p));
      else results.push_back(smap[p.x][p.y]); });
  return results;
}

vector<double> tile_localize::copyNearest(vector<Point>& ps, Point rx_loc, ValueMap& smap) {
  vector<double> results;
  for (auto P = ps.begin(); P != ps.end(); ++P) {
    if (not isnan(smap[P->x][P->y])) {
      results.push_back(smap[P->x][P->y]);
    }
    else {
      //Direction to search (FIXME assuming this point is at a corner)
      double x_pos = P->x == 0 ? 1.0 : -1.0;
      double y_pos = P->y == 0 ? 1.0 : -1.0;
      double found = false;
      for (int dist = 1; dist <= (smap.size()-1) + (smap[0].size()-1) and not found; ++dist) {
        for (int x_i = 0; x_i < smap.size() and x_i <= dist and not found; ++x_i) {
          //Adjust as an offset from the given point p
          int x = P->x + x_i*x_pos;
          int y = P->y + (dist - x_i)*y_pos;
          //Check if the y value is in range and that the map has a value for this point
          if ( y < smap[0].size() and not isnan(smap[x][y])) {
            results.push_back(smap[x][y]);
            found = true;
          }
        }
      }
      //Push back NaN if no closest point was found.
      if (not found) results.push_back(numeric_limits<double>::quiet_NaN());
    }
  }
  return results;
}

void giveCornerValue(Point p, pair<double,double> rx_loc, vector<vector<double>>& smap, vector<Tri>& mesh) {
  Point r{rx_loc.first, rx_loc.second};
  double x_pos = p.x == 0 ? 1.0 : -1.0;
  double y_pos = p.y == 0 ? 1.0 : -1.0;
  //Leave the point alone if it already has a value.
  if(isnan(smap[p.x][p.y])) {
    //Just finding the closest point with data to the corner, finding a linear signal to
    //distance function, and assigning the corner's value from that.
    double dist1 = 0.0;
    double sig1  = 0.0;
    for (int dist = 1; dist <= (smap.size()-1) + (smap[0].size()-1); ++dist) {
      for (int x_i = 0; x_i < smap.size() and x_i <= dist; ++x_i) {
        //Adjust as an offset from the given point p
        int x = p.x + x_i*x_pos;
        int y = p.y + (dist - x_i)*y_pos;
        //Make sure that the y value is not out of range (x cannot be out of range here),
        //that the map has a value for this point, and that this is not a corner point.
        if ( y < smap[0].size() and not isnan(smap[x][y]) and not (x == smap.size()-1 and y == smap[0].size()-1)) {
          //Check if this is the first point
          if (dist1 == 0.0) {
            dist1 = euclDistance(r, Point{(double)x, (double)y});
            sig1 = smap[x][y];
          } else {
            //Use simple signal to distance function with the two points.
            double dist2 = euclDistance(r, Point{(double)x, (double)y});
            double sig2  = smap[x][y];
            //Find the rate of power loss per unit distance.
            double slope = (sig2-sig1)/(dist2-dist1);
            //Guess at the reception power at distance 0
            double initial = ((sig2 - dist2*slope) + (sig1 - dist1*slope))/2.0;
            smap[p.x][p.y] = initial + slope * euclDistance(r, p);
            return;
          }
        }
      }
    }
    /*
    auto t = findContainingTriangle(p, mesh);
    if (t != mesh.end()) {
      vector<pair<Point, double>> good_values;
      //Warning: if all three points have no data we assign the corner a zero value.
      if (not isnan(smap[t->a.x][t->a.y])) {
        good_values.push_back(make_pair(t->a, smap[t->a.x][t->a.y]));
      }
      if (not isnan(smap[t->b.x][t->b.y])) {
        good_values.push_back(make_pair(t->b, smap[t->b.x][t->b.y]));
      }
      if (not isnan(smap[t->c.x][t->c.y])) {
        good_values.push_back(make_pair(t->c, smap[t->c.x][t->c.y]));
      }
      double total_distance = accumulate(good_values.begin(), good_values.end(), 0.0,
          [=](double x, pair<Point, double>& y) { return x + euclDistance(y.first, r);});
      double corner_val = accumulate(good_values.begin(), good_values.end(), 0.0,
          [=](double x, pair<Point, double>& y) { return x + (1.0 - euclDistance(y.first, r)/total_distance)*y.second;});
      smap[p.x][p.y] = corner_val;
    }
    */
  }
}

//Build a signal map for each receiver using the given training data and
//triangular mesh.
vector< vector<vector<double>>> tile_localize::buildSignalMaps(TrainingData& train_data,
                                                               vector<vector<Tri>>& meshes,
                                                               LandmarkData& lm_data, int columns, int rows,
                                                               exteriorEstimator extEstimate) {
  size_t num_rx = lm_data.size();
  //Now interpolate to build the signal maps.
  const float qnan = std::numeric_limits<float>::quiet_NaN();
  vector< vector<vector<double>>> signal_maps(num_rx, vector<vector<double>>(columns, vector<double>(rows, qnan)));
  //Make sure that none of the samples are out of range
  for_each(train_data.begin(), train_data.end(),
      [=](const Sample& s){ if (s.x >= columns or s.y >= rows) {
                                 ostringstream is;
                                 is << "Training data "<<s.x<<","<<s.y<<" is out of map area.";
                                 throw runtime_error(is.str());}});
  for (size_t rxer = 0; rxer < num_rx; ++rxer) {
    //Make a shortcut to the signal map for this receiver
    vector<vector<double>>& smap = signal_maps[rxer];
    //Copy the known rssi values into the signal map.
    for_each(train_data.begin(), train_data.end(),
        [&](const Sample& s) { smap[s.x][s.y] = s.rssis[rxer];});

    //The corner values will be used in the triangular mesh but might not
    //have sample values so we need to fill them in. Subtract one from the
    //row and column counts since we start counting tiles from 0.
    double d_rows = rows - 1.0;
    double d_cols = columns - 1.0;

    //TODO First interpolate the value at the receiver and use that in the giveCornerValue function.
    //Notice that this can use one "guessed" corner value to guess another one
    //if two corners are in the same triangle.
    vector<Point> corners{Point{0, 0}, Point{0, d_rows}, Point{d_cols, d_rows}, Point{d_cols, 0}};
    vector<double> corner_vals = extEstimate(corners, toPoint(lm_data[rxer]), smap);
    for (int i = 0; i < corners.size(); ++i) {
      smap[corners[i].x][corners[i].y] = corner_vals[i];
    }

    //Now interpolate to give each tile a value if it does not currently have one.
    for (size_t x = 0; x < columns; ++x) {
      for (size_t y = 0; y < rows; ++y) {
        if (isnan(smap[x][y])) {
          smap[x][y] = interpolate(Point{(double)x,(double)y}, smap, meshes[rxer]);
        }
      }
    }
  }
  return signal_maps;
}

//Returns the probability of seeing the given signal if the distribution
//of signal strengths has the given mean and variance.
double probSignalFromMean(double signal, double mean, double variance) {
  //TODO We assume signal readings have an error of 0.5 here but that should
  //be another parameter.
  //The probabability of a signal of the given value is
  //cdf(signal+error)-cdf(signal-error) where error is the resolution
  //of the measurement.
  //Assume a normal distribution
  double result = 0.5 * ( ( 1 + erf( (signal + 0.5 - mean)/variance)) - ( 1 + erf( (signal - 0.5 - mean)/variance)));
  if (isnan(result)) { return 0.0;}
  else { return result;}
  //TODO The signal should be converted into power before this.
}

/*
 * Solves for the location of any items in the training data that have
 * nan values for coordinates. The solution is in the form of the set of
 * tiles where the transmitter is located with the given confidence level.
 * The algorithm works by building an Interpolated Map Grid (IMG)
 * of signal propagation for each landmark. The determine the probability
 * of a signal coming from a particular tile the solver assumes that
 * signal strength is a guassian random variable with the given
 * variance.
 */
vector<Result> tile_localize::ABPSolve( TrainingData train_data,
                                        LandmarkData rx_coord,
                                        double region_width,
                                        double region_height,
                                        double tile_width,
                                        double tile_height,
                                        double signal_variance,
                                        double percentile,
                                        dataToParameter fromData,
                                        parameterToData toData,
                                        exteriorEstimator extEstimate) {
  TrainingData unknowns;
  //Move unknown entries from train_data to the unknowns vector.
  auto I = train_data.begin();
  while (I != train_data.end()) {
    if ( isnan(I->x)) {
      unknowns.push_back(*I);
      I = train_data.erase(I);
    }
    else {
      ++I;
    }
  }

  int columns = ceil(region_width/tile_width);
  int rows    = ceil(region_height/tile_height);
  Converter toCol{tile_width};
  Converter toRow{tile_height};
  //Convert all coordinates from absolute values into tile values.
  for_each(rx_coord.begin(), rx_coord.end(),
      [&](pair<double, double>& p){p.first  = toCol(p.first);
                                   p.second = toRow(p.second);});
  for_each(train_data.begin(), train_data.end(),
      [&](Sample& s){s.x = toCol(s.x);
                     s.y = toRow(s.y);});

  //First build the Interpolated Map Grid (IMG) for each receiver.
  vector<vector<Tri>> meshes(rx_coord.size());
  for (int rxer = 0; rxer < rx_coord.size(); ++rxer) {
    //We need to build the delaunay triangle mesh for use in interpolation.
    //Only use vertices with values in the mesh
    vector<Point> vertices;
    for_each(train_data.begin(), train_data.end(), [&](Sample& s) {
       if (not isnan(s.rssis[rxer])) vertices.push_back(Point{s.x, s.y}); });
    //Create a mesh columns wids and rows high.
    meshes[rxer] = genTriangles(columns - 1, rows - 1, vertices);
  }

  //Convert data to the parameters that should be interpolated using the given
  //fromData function.
  TrainingData converted = train_data;
  for_each(converted.begin(), converted.end(), [&](Sample& s) {
      for (int rxer = 0; rxer < rx_coord.size(); ++rxer) {
        Point rx = Point{rx_coord[rxer].first*tile_width + tile_width/2.0, rx_coord[rxer].second*tile_height + tile_height/2.0};
        Point tx = Point{s.x*tile_width+tile_width/2.0, s.y*tile_height+tile_height/2.0};
        s.rssis[rxer] = fromData(rx, tx, s.rssis[rxer]); }});
                      
  //Build the signal maps with this data and give the corners values using
  //the user specified extEstimate function.
  vector< vector<vector<double>>> signal_maps =
    buildSignalMaps(converted, meshes, rx_coord, columns, rows, extEstimate);

  //Convert values from parameters back to data for tile searching.
  for (size_t rxer = 0; rxer < signal_maps.size(); ++rxer) {
    for (size_t col = 0; col < columns; ++col) {
      for (size_t row = 0; row < rows; ++row) {
        double y = row * tile_height + tile_height/2.0;
        double x = col * tile_width + tile_width/2.0;
        Point rx = Point{rx_coord[rxer].first*tile_width + tile_width/2.0, rx_coord[rxer].second*tile_height + tile_height/2.0};
        signal_maps[rxer][col][row] = toData(rx, Point{(double)x, (double)y}, signal_maps[rxer][col][row]);
      }
    }
  }

  vector<Result> results;
  //Generate a result for each unknown
  for (auto I = unknowns.begin(); I != unknowns.end(); ++I) {
    //We can to solve for P(tile|signal) which is
    //P(signal|tile)*P(tile)/P(signal)
    //With multiple signal maps this becomes
    //P(tile|s1,s2,...) = P(s1,s2,...|tile)*P(tile)/P(s1,s2,...)
    //First solve for P(s1,s2,...)*P(T) which is
    //the sum over all tiles of the product across all receivers, P(s1|T)P(s2|T)*...*P(T)
    //However, since P(T) is the same for each tile we can drop it from these equations.
    double p_signals = 0.0;
    int total_tiles = rows*columns;
    for (size_t tile_i = 0; tile_i < total_tiles; ++tile_i) {
      double tile_product = 1.0;
      //Find the probability of this signal at this tile across all receivers.
      int cur_x = tile_i / columns;
      int cur_y = tile_i % columns;
      for (size_t rxer = 0; rxer < rx_coord.size(); ++rxer) {
        double tile_mean = signal_maps[rxer][cur_x][cur_y];
        tile_product *= probSignalFromMean(I->rssis[rxer], tile_mean, signal_variance);
      }
      p_signals += tile_product;
    }
    cerr<<"p_signals is "<<p_signals<<'\n';
    //Now solve for the P(s1,s2,...|T) values
    vector<pair<double, int>> tile_probs(total_tiles);
    for (size_t tile_i = 0; tile_i < total_tiles; ++tile_i) {
      double p_sig_tiles = 1.0;
      //Find the probability of this signal at this tile across all receivers.
      int cur_x = tile_i / columns;
      int cur_y = tile_i % columns;
      for (size_t rxer = 0; rxer < rx_coord.size(); ++rxer) {
        double tile_mean = signal_maps[rxer][cur_x][cur_y];
        p_sig_tiles *= probSignalFromMean(I->rssis[rxer], tile_mean, signal_variance);
      }
      //Take the P(s1,s2...|T) value and use the equation
      //P(T|s1,s2...) = P(s1,s2...)P(T)/T(s1,s2...) to solve for P(T|s1,s2...)
      tile_probs[tile_i] = make_pair(p_sig_tiles/p_signals, tile_i);
    }

    sort(tile_probs.begin(), tile_probs.end(),
        [](pair<double, int> a, pair<double, int> b) { return a.first > b.first;});

    //Keep putting tiles into the result list until we hit the
    //desired probability percentile
    double cum_prob = 0.0;
    Result r;
    for (auto I = tile_probs.begin(); I != tile_probs.end() and cum_prob < percentile; ++I) {
      r.push_back(Tile{I->second / columns, I->second % columns});
      cum_prob += I->first;
    }
    results.push_back(r);
  }
  return results;
}

double tile_localize::identityFunction(Point rxer, Point sample, double value) {
  return value;
}

/*
 * Solves for the location of any items in the training data that have
 * nan values for coordinates. The solution is in the form of the set of
 * tiles where the transmitter is located with the given confidence level.
 * The algorithm works by building an Interpolated Map Grid (IMG)
 * of signal propagation for each landmark. The SPM algorithm accepts a
 * signal as possibly coming from a tile if its RSS value is with threshold
 * dB of the tile's RSS value.
 */
vector<Result> tile_localize::SPMSolve( TrainingData train_data,
                                        LandmarkData rx_coord,
                                        double region_width,
                                        double region_height,
                                        double tile_width,
                                        double tile_height,
                                        double threshold,
                                        dataToParameter fromData,
                                        parameterToData toData,
                                        exteriorEstimator extEstimate) {
  TrainingData unknowns;
  //Move unknown entries from train_data to the unknowns vector.
  auto I = train_data.begin();
  while (I != train_data.end()) {
    if ( isnan(I->x)) {
      unknowns.push_back(*I);
      I = train_data.erase(I);
    }
    else {
      ++I;
    }
  }

  int columns = ceil(region_width/tile_width);
  int rows    = ceil(region_height/tile_height);
  Converter toCol{tile_width};
  Converter toRow{tile_height};
  //Convert all coordinates from absolute values into tile values.
  for_each(rx_coord.begin(), rx_coord.end(),
      [&](pair<double, double>& p){p.first  = toCol(p.first);
                                   p.second = toRow(p.second);});
  for_each(train_data.begin(), train_data.end(),
      [&](Sample& s){s.x = toCol(s.x);
                     s.y = toRow(s.y);});

  //First build the Interpolated Map Grid (IMG) for each receiver.
  vector<vector<Tri>> meshes(rx_coord.size());
  for (int rxer = 0; rxer < rx_coord.size(); ++rxer) {
    //We need to build the delaunay triangle mesh for use in interpolation.
    //Only use vertices with values in the mesh
    vector<Point> vertices;
    for_each(train_data.begin(), train_data.end(), [&](Sample& s) {
       if (not isnan(s.rssis[rxer])) vertices.push_back(Point{s.x, s.y}); });
    //Create a mesh columns wids and rows high.
    meshes[rxer] = genTriangles(columns - 1, rows - 1, vertices);
  }

  //Convert data to the parameters that should be interpolated using the given
  //fromData function.
  TrainingData converted = train_data;
  for_each(converted.begin(), converted.end(), [&](Sample& s) {
      for (int rxer = 0; rxer < rx_coord.size(); ++rxer) {
        Point rx = Point{rx_coord[rxer].first*tile_width + tile_width/2.0, rx_coord[rxer].second*tile_height + tile_height/2.0};
        Point tx = Point{s.x*tile_width+tile_width/2.0, s.y*tile_height+tile_height/2.0};
        s.rssis[rxer] = fromData(rx, tx, s.rssis[rxer]); }});
                      
  //Build the signal maps with this data and give the corners values using
  //the user specified extEstimate function.
  vector< vector<vector<double>>> signal_maps =
    buildSignalMaps(converted, meshes, rx_coord, columns, rows, extEstimate);

  //Convert values from parameters back to data for tile searching.
  for (size_t rxer = 0; rxer < signal_maps.size(); ++rxer) {
    cerr<<"Map for rxer "<<rxer<<" located at "<<toPoint(rx_coord[rxer])<<endl;
    cerr.precision(3);
    //TODO reverse these two after finishing printing stuff
    for (size_t row = 0; row < rows; ++row) {
        for (size_t col = 0; col < columns; ++col) {
        double y = row * tile_height + tile_height/2.0;
        double x = col * tile_width + tile_width/2.0;
        Point rx = Point{rx_coord[rxer].first*tile_width + tile_width/2.0, rx_coord[rxer].second*tile_height + tile_height/2.0};
        signal_maps[rxer][col][row] = toData(rx, Point{(double)x, (double)y}, signal_maps[rxer][col][row]);
        cerr<<signal_maps[rxer][col][row]<<' ';
      }
      cerr<<endl;
    }
    cerr<<endl;
  }
  /*
  for (size_t rxer = 0; rxer < signal_maps.size(); ++rxer) {
    for (size_t col = 0; col < columns; ++col) {
      for (size_t row = 0; row < rows; ++row) {
        double y = row * tile_height + tile_height/2.0;
        double x = col * tile_width + tile_width/2.0;
        Point rx = Point{rx_coord[rxer].first*tile_width + tile_width/2.0, rx_coord[rxer].second*tile_height + tile_height/2.0};
        signal_maps[rxer][col][row] = toData(rx, Point{(double)x, (double)y}, signal_maps[rxer][col][row]);
      }
    }
  }
  */

  //Now make a result for each sample with unknown locations.
  vector<Result> results;
  for (auto I = unknowns.begin(); I != unknowns.end(); ++I) {
    //Keep increasing the threshold until we get some results.
    Result r;
    float threshold_increase = 0.0;
    while (r.empty()) {
      //Assume all tiles are a possible position for the unknown at first
      vector<vector<bool>> possible_tiles(columns, vector<bool>(rows, true));
      for (size_t rxer = 0; rxer < signal_maps.size(); ++rxer) {
        for (size_t x = 0; x < columns; ++x) {
          for (size_t y = 0; y < rows; ++y) {
            //Eliminate tiles from the possible list if their rss values
            //differ from the sample's rss value by more than the threshold.
            if ( abs(I->rssis[rxer] - signal_maps[rxer][x][y]) > threshold + threshold_increase) {
              possible_tiles[x][y] = false;
            }
          }
        }
      }
      for (size_t x = 0; x < columns; ++x) {
        for (size_t y = 0; y < rows; ++y) {
          if (possible_tiles[x][y]) { r.push_back(Tile{x, y});}
        }
      }
      threshold_increase += 1.0;
    }
    results.push_back(r);
  }
  return results;
}



