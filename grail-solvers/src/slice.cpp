/*
*
* Copyright (c) 2006 Rutgers University and Konstantinos Kleisouris
* All rights reserved.
*
* Permission to use, copy, modify, and distribute this software and its
* documentation for any purpose, without fee, and without written agreement is
* hereby granted, provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the distribution.
*
* 3. Neither the name of the University nor the names of its
* contributors may be used to endorse or promote products derived from
* this software without specific prior written permission.
*
* IN NO EVENT SHALL RUTGERS UNIVERSITY BE LIABLE TO ANY PARTY FOR DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT
* OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF RUTGERS
* UNIVERSITY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* RUTGERS UNIVERSITY SPECIFICALLY DISCLAIMS ANY WARRANTIES,
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
* AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS
* ON AN "AS IS" BASIS, AND RUTGERS UNIVERSITY HAS NO OBLIGATION TO
* PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*
*
* Author: Konstantinos Kleisouris
* Version: $Id: slice.cpp,v 1.1.2.2 2010/09/07 17:33:55 bfirner Exp $
* Creation Date: 06/27/2006
* Filename: slice.c
* History:
* $Log: slice.cpp,v $
* Revision 1.1.2.2  2010/09/07 17:33:55  bfirner
* Updates to files. Localization solver now sends data to the test aggregator.
*
* Revision 1.1  2007/09/17 05:01:08  stryke3
* Initial commit.
*
* Revision 1.3  2007/04/30 21:51:08  kkonst
* Added solver code
*
* Revision 1.1  2006/07/19 20:13:22  kkonst
* Added source code of fast solver under demo3
*
* Revision 1.1.1.1  2006/06/27 15:15:52  kkonst
* Imported fast solver code
*
*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <algorithm>
#include <vector>

#include "rand.h"
#include "slice.h"

#include <iostream>

extern int likelihood_file, iterationNo;     

void printDS2(mc_dynamic_state& ds) {
  std::cerr<<"Beta values are ";
  for (auto I = ds.q.begin()+ds.first_b0i_index; I != ds.q.begin()+ds.first_x_index; ++I) {
    std::cerr<<*I<<'\t';
  }
  std::cerr<<'\n';
  std::cerr<<"Position values are ";
  for (auto I = ds.q.begin() + ds.first_x_index; I != ds.q.begin()+ds.last_y_index; ++I) {
    std::cerr<<*I<<'\t';
  }
  std::cerr<<'\n';
}

mc_dynamic_state::mc_dynamic_state(int MAX_APS, int predictNo, int total_network_variables, double DOMAIN_X, double DOMAIN_Y) {
  last_bprior_index = 2;
  last_taubprior_index = 4;
  first_b0i_index = last_taubprior_index;
  first_b1i_index = first_b0i_index + MAX_APS;
  first_taui_index = first_b0i_index + MAX_APS * 2;
  first_x_index = first_b0i_index + MAX_APS * 3;
  first_y_index = first_x_index + predictNo;
  last_y_index = first_y_index + predictNo; 
  dim = total_network_variables;
  q    = std::vector<double>(dim);
  p    = std::vector<double>(dim);
  grad = std::vector<double>(dim);
  this->DOMAIN_X = DOMAIN_X;
  this->DOMAIN_Y = DOMAIN_Y;

  std::vector<double>::iterator qbegin = q.begin();
  std::fill(qbegin, qbegin+last_bprior_index, 0.0);
  std::fill(qbegin+last_bprior_index, qbegin+last_taubprior_index, 0.01);
  std::fill(qbegin+first_b0i_index, qbegin+first_taui_index, 0.0);
  std::fill(qbegin+first_taui_index, qbegin+first_x_index, 0.1);

  //Initialize with random location inside the localization area
  for (int i = first_x_index; i < first_y_index; i++) {        
    q[i] = rand_uniopen() * DOMAIN_X;      // Initialization for Xs
  }
  for (int i = first_y_index; i < last_y_index; i++) {        
    q[i] = rand_uniopen() * DOMAIN_Y;      // Initialization for Ys
  }
}

data_specifications::data_specifications(
  int N_inputs,
  int MAX_APS,
  int N_cases,
  const std::vector<double>& train_x,
  const std::vector<double>& train_y,
  const double_matrix& train_signal,
  int predictNo,
  int observations_nopredict,
  const std::vector<double>& APS_X,
  const std::vector<double>& APS_Y
)
{
  this->predictNo = predictNo;
  this->observations_nopredict = observations_nopredict;
  this->N_inputs = N_inputs;
  this->MAX_APS = MAX_APS;
  this->N_cases = N_cases;
  this->train_x = train_x;
  this->train_y = train_y;
  this->train_signal = train_signal;
  this->D = double_matrix(MAX_APS, std::vector<double>(N_cases, 0.0));
  this->Dsqrd = double_matrix(MAX_APS, std::vector<double>(N_cases, 0.0));
  this->APS_X = APS_X;
  this->APS_Y = APS_Y;

  double x_value, y_value, diff1, diff2, x_fixed, y_fixed;    

  for (int i = 0; i < MAX_APS; i++) {
    x_fixed = APS_X[i];
    y_fixed = APS_Y[i];

    for (int j = 0; j < observations_nopredict; j++) {
      x_value = train_x[j];
      y_value = train_y[j];

      diff1 = x_value-x_fixed;
      diff2 = y_value-y_fixed;

      this->D[i][j] = log(1+sqrt(diff1*diff1 + diff2*diff2));
      this->Dsqrd[i][j] = D[i][j] * D[i][j];
    }
  }
}



double conjugateNormalB0i_PosteriorMeanSum_1
( mc_dynamic_state& ds, /* Structure holding pointers to dynamical state */
  const data_specifications& data_spec,
  int target_offset,
  double x_fixed,
  double y_fixed,
  double b,
  double precision,
  int& cntminus1
) 
{
  int i;
  double D=0.0, m=0.0;
  double diff1 = 0.0, diff2 = 0.0;
  
  int N_cases = data_spec.N_cases;
  double x_value, y_value;

  for (i = 0; i < data_spec.observations_nopredict; i++) {
    if (data_spec.train_signal[target_offset][i] == -1) {
      cntminus1++;
      continue;
    }

    D = data_spec.D[target_offset][i];    
    m += (data_spec.train_signal[target_offset][i] - b * D);
  }

  for (i = data_spec.observations_nopredict; i < N_cases; i++) {
    if (data_spec.train_signal[target_offset][i] == -1) {
      cntminus1++;
      continue;
    }

    x_value = ds.q[ds.first_x_index+i-data_spec.observations_nopredict];
    y_value = ds.q[ds.first_x_index+i+data_spec.predictNo-data_spec.observations_nopredict];
    
    diff1 = x_value-x_fixed;
    diff2 = y_value-y_fixed;
    
    D = log(1+sqrt(diff1*diff1 + diff2*diff2));
    m += (data_spec.train_signal[target_offset][i] - b * D);
  }

  m *= precision;

  return m;
}


/* EVALUATE POTENTIAL ENERGY */
void mc_app_energy
( mc_dynamic_state& ds,	/* Current dyanamical state */
  const data_specifications& data_spec,  
  double& energy,	/* Place to store energy, null if not required */
  int k                 /* Index of coordinate being updated */
) {  
  double e = 0.0;
  double D, m;
  int target_index;
  double diff1 = 0.0, diff2 = 0.0;
  int x_index = -1;
  int y_index = -1;
  int i;

  if ((k >= ds.first_b0i_index) && (k < ds.first_b1i_index)) {              // This is for b0i
    int N_cases = data_spec.N_cases;
    double A, B;
    double prior_precision = ds.q[2];
    double prior_mean = ds.q[0];
    int aps_index = k - ds.first_b0i_index;
    int b1_index = k + data_spec.MAX_APS;
    int tau_index = k + 2*data_spec.MAX_APS;
    int cntminus1 = 0;

    A = prior_mean*prior_precision;
    
    A += conjugateNormalB0i_PosteriorMeanSum_1(ds, data_spec, aps_index, data_spec.APS_X[aps_index], data_spec.APS_Y[aps_index], 
    				       ds.q[b1_index], ds.q[tau_index], cntminus1);
    
    B = prior_precision + (N_cases-cntminus1)* ds.q[tau_index];

    double post_mean = A / B;
    double post_variance = 1 / B;
    
    e += normal_dist(ds.q[k], post_mean, post_variance);
    energy = e;
    return;
  }
  
  //X or Y
  if ( (k >= ds.first_x_index) && (k < ds.first_y_index) ) {
    x_index = k;
    y_index = k+data_spec.predictNo;
  }
  else if ( (k >= ds.first_y_index) && (k < ds.last_y_index) ) {
    x_index = k-data_spec.predictNo;
    y_index = k;
  }

  if (x_index < 0 or y_index < 0) {
    fprintf(stderr, "The index (k=%d) given to mc_app_energy was invalid.\n", k);
    return;
  }
  
  target_index = data_spec.observations_nopredict + x_index - ds.first_x_index;

  for (i = 0; i < data_spec.MAX_APS; i++) {
    if (data_spec.train_signal[i][target_index] == -1) {
      continue;
    }

    diff1 = ds.q[x_index]-data_spec.APS_X[i];
    diff2 = ds.q[y_index]-data_spec.APS_Y[i];   
    D = log(1+sqrt(diff1*diff1 + diff2*diff2));
    m = ds.q[i + ds.first_b0i_index] + (ds.q[i + ds.first_b1i_index] * D);
    e += normal_dist(data_spec.train_signal[i][target_index], m, 1/ds.q[i+ds.first_taui_index]); // Likelihood: This is for Si
  }

  energy = e;
}

/* PERFORM MULTIVARIATE SLICE SAMPLING WITH HYPERRECTANGLES. */
void mc_slice_mult2D
( mc_dynamic_state& ds,	/* State to update */
  data_specifications& data_spec,  
  std::vector<double>& save,	/* Place to save current state */
  std::vector<double>& lowb,	/* Storage for low bounds of hyperrectangle */
  std::vector<double>& highb,	/* Storage for high bounds of hyperrectangle */
  int k,                 /* index for x in ds.q */
  int g_shrink,		/* Shrink based on gradient? */
  double width,
  bool use_domain,
  double low_x,
  double high_x,
  double low_y,
  double high_y,
  int& rejections
)
{
  double slice_point, maxp, pr;
  //FIXME This value might not be set before being used as an index int a vector
  int maxk = 0;

  if (use_domain) {
    lowb[k]  = low_x; 
    highb[k] = high_x;
    lowb[k+data_spec.predictNo]  = low_y; 
    highb[k+data_spec.predictNo] = high_y;
  }
  else {
    lowb[k]  = ds.q[k] - rand_uniopen() * width;  
    highb[k] = lowb[k] + width;
    lowb[k+data_spec.predictNo]  = ds.q[k+data_spec.predictNo] - rand_uniopen() * width; 
    highb[k+data_spec.predictNo] = lowb[k+data_spec.predictNo] + width;

    if (lowb[k] < low_x) lowb[k] = low_x;
    if (highb[k] > high_x) highb[k] = high_x;
    if (lowb[k+data_spec.predictNo] < low_y) lowb[k+data_spec.predictNo] = low_y;
    if (highb[k+data_spec.predictNo] > high_y) highb[k+data_spec.predictNo] = high_y;
  }
  
  mc_app_energy_2D (ds, data_spec, ds.pot_energy, k, 0);
  slice_point = ds.pot_energy + rand_exp();

  save[k] = ds.q[k];
  save[k+data_spec.predictNo] = ds.q[k+data_spec.predictNo];
 
  for (;;)
  { 
    ds.q[k]  = lowb[k] + rand_uniopen() * (highb[k]-lowb[k]);
    ds.q[k+data_spec.predictNo]  = lowb[k+data_spec.predictNo] + rand_uniopen() * (highb[k+data_spec.predictNo]-lowb[k+data_spec.predictNo]);

    mc_app_energy_2D(ds, data_spec, ds.pot_energy, k, g_shrink);

    if (ds.pot_energy<=slice_point) { 
      return;
    }
    
    rejections++;

    if (g_shrink!=0) {  // Use gradient
      maxp = -1;

      /* This is for X */
      pr = ds.grad[k] * (highb[k]-lowb[k]);
      if (pr<0) pr = -pr;

      if (pr>maxp) { 
        maxp = pr;
        maxk = k;
      }

      /* This is for Y */
      pr = ds.grad[k+data_spec.predictNo] * (highb[k+data_spec.predictNo]-lowb[k+data_spec.predictNo]);
      if (pr<0) pr = -pr;

      if (pr>maxp) { 
        maxp = pr;
        maxk = k+data_spec.predictNo;
      }

      if (ds.q[maxk]>save[maxk]) {      
        highb[maxk] = ds.q[maxk];
      }
      else { 
        lowb[maxk] = ds.q[maxk];
      }      
    }
    else {    // Not Use gradient
      if (ds.q[k]>save[k]) {        // This is for X
        highb[k] = ds.q[k];
      }
      else { 
        lowb[k] = ds.q[k];
      }      

      if (ds.q[k+data_spec.predictNo]>save[k+data_spec.predictNo]) {   // This is for Y
        highb[k+data_spec.predictNo] = ds.q[k+data_spec.predictNo];
      }
      else { 
	lowb[k+data_spec.predictNo] = ds.q[k+data_spec.predictNo];
      }    
    }
  }
}

/* FIND INTERVAL AROUND PART OF SLICE BY STEPPING OUT. */
void step_out 
( mc_dynamic_state& ds,	/* Current state */
  const data_specifications& data_spec,  
  int k,		/* Index of coordinate being updated */
  double slice_point,	/* Potential energy level that defines slice */
  double curr_q,	/* Current value for coordinate */
  int max_steps,	/* Maximum number of intervals, zero for unlimited */
  double& low_bnd,	/* Place to store low bound for interval */
  double& high_bnd,	/* Place to store high bound for interval */
  double width,          /* Estimate of the slice */
  double lower, 
  double higher,
  int bounded
)
{
  int low_steps, high_steps;

  low_steps  = max_steps==0 ? 1000000000 
             : max_steps==1 ? 0
             : rand_int(max_steps);
  high_steps = max_steps==0 ? 1000000000 
             : (max_steps-1) - low_steps;

  low_bnd  = curr_q - rand_uniopen() * width;
  high_bnd = low_bnd + width;

  if ( bounded && (low_bnd < lower) ) {
    low_bnd = lower;
  }
  else {
    while (low_steps>0) {
      ds.q[k] = low_bnd;
      mc_app_energy (ds, data_spec, ds.pot_energy, k);    
      
      if (ds.pot_energy>slice_point) break;
      
      low_bnd -= width;
      low_steps -= 1;
      
      if (bounded && (low_bnd < lower)) {
        low_bnd = lower;
        break;
      }
    }
  }

  if (bounded && (high_bnd > higher)) {
    high_bnd = higher;
  }
  else {
    while (high_steps>0) {
      ds.q[k] = high_bnd;
      mc_app_energy (ds, data_spec, ds.pot_energy, k);    
      
      if (ds.pot_energy>slice_point) break;
      
      high_bnd += width;
      high_steps -= 1;

      if (bounded && (high_bnd > higher)) {
        high_bnd = higher;
        break;
      }
    }
  }
}


/* FIND INTERVAL AROUND PART OF SLICE BY DOUBLING. */
void dbl_out
( mc_dynamic_state& ds,	/* Current state */
  data_specifications& data_spec,  
  int k,		/* Index of coordinate being updated */
  double slice_point,	/* Potential energy level that defines slice */
  double curr_q,	/* Current value for coordinate */
  int max_int,		/* Maximum number of intervals to go through: 2^max_int*width */
  double& low_bnd,	/* Place to store low bound for interval */
  double& high_bnd,	/* Place to store high bound for interval */
  double width,          /* Estimate of the slice */
  double lower, 
  double higher,
  int bounded
)
{
  int low_out, high_out;

  low_bnd  = curr_q - rand_uniopen() * width;
  high_bnd = low_bnd + width;
  
  if ( bounded && (low_bnd < lower) ) {
    low_bnd = lower;
    low_out = 1;
  }
  else {
    ds.q[k] = low_bnd;
    mc_app_energy (ds, data_spec, ds.pot_energy, k);    
    low_out = ds.pot_energy>slice_point;
  }
  
  if (bounded && (high_bnd > higher)) {
    high_bnd = higher;
    high_out = 1;
  }
  else {
    ds.q[k] = high_bnd;
    mc_app_energy (ds, data_spec, ds.pot_energy, k);    
    high_out = ds.pot_energy>slice_point;
  }

  while (max_int>1 && (!low_out || !high_out))
  { 
    if (rand_int(2))
    { low_bnd -= (high_bnd - low_bnd);
      ds.q[k] = low_bnd;
      mc_app_energy (ds, data_spec, ds.pot_energy, k);    
      low_out = ds.pot_energy>slice_point;

      if (bounded && (low_bnd < lower)) {
        low_bnd = lower;
        low_out = 1;
      }
    }
    else
    { high_bnd += (high_bnd - low_bnd);
      ds.q[k] = high_bnd;
      mc_app_energy (ds, data_spec, ds.pot_energy, k);    
      high_out = ds.pot_energy>slice_point;

      if (bounded && (high_bnd > higher)) {
        high_bnd = higher;
        high_out = 1;
      }
    }

    max_int -= 1;
  }
}


/* CHECK THAT DOUBLING WOULD FIND SAME INTERVAL FROM NEW POINT.  Returns 1
   if it would, 0 if it would not. */
int dbl_ok
( mc_dynamic_state& ds,	/* Current state */
  const data_specifications& data_spec, 
  int k,		/* Index of coordinate being updated */
  double slice_point,	/* Potential energy level that defines slice */
  double curr_q,	/* Current value for coordinate */
  double low_bnd,	/* Low bound for interval */
  double high_bnd,	/* High bound for interval */
  double width          /* Estimate of the slice */
)
{
  double mid;
  double new_q;
  int diff;

  new_q = ds.q[k];
  diff = 0;

  while (high_bnd-low_bnd>1.1*width)
  { mid = (high_bnd+low_bnd)/2;
    if (!diff) 
    { diff = (curr_q<mid) != (new_q<mid);
    }
    if (diff)
    { ds.q[k] = mid;
      mc_app_energy (ds, data_spec, ds.pot_energy, k);    
    }
    if (new_q<mid)
    { high_bnd = mid;
      ds.q[k] = low_bnd;
    }
    else
    { low_bnd = mid;
      ds.q[k] = high_bnd;
    }
    if (diff && ds.pot_energy>slice_point)
    { mc_app_energy (ds, data_spec, ds.pot_energy, k);    
      if (ds.pot_energy>slice_point)
      { ds.q[k] = new_q;
        return 0;
      }
    }
  }

  ds.q[k] = new_q;
  return 1;
}


/* PICK VALUE FROM INTERVAL AROUND PART OF THE SLICE. */
void pick_value
( mc_dynamic_state& ds,		/* Current state, updated with new value */
  data_specifications& data_spec,  
  int k,			/* Index of coordinate being updated */
  double slice_point,		/* Potential energy level that defines slice */
  double curr_q,		/* Current value for coordinate */
  int max_steps,		/* Negative if interval was found by doubling */
  double low_bnd,		/* Low bound for interval */
  double high_bnd,		/* High bound for interval */
  int s_factor,			/* Factor for faster shrinkage */
  double s_threshold,		/* Threshold for faster shrinkage */
  double width,                 /* Estimate of slice */
  int& rejections
)
{
  double nlow_bnd, nhigh_bnd, range;

  nlow_bnd = low_bnd;
  nhigh_bnd = high_bnd;

  for (;;)
    {
    ds.q[k] = nlow_bnd + rand_uniopen() * (nhigh_bnd-nlow_bnd);

    mc_app_energy (ds, data_spec, ds.pot_energy, k);    
    
    if (ds.pot_energy<=slice_point && (max_steps>=0 
					|| dbl_ok(ds, data_spec, k, slice_point, curr_q, low_bnd, high_bnd, width)))
    { 
      return;
    }

    rejections++;
    
    if (s_factor>=0)
    { 
      if (ds.q[k]<curr_q) 
      { nlow_bnd = ds.q[k];
      }
      else
      { nhigh_bnd = ds.q[k];
      }
    }

    if ((s_factor<-1 || s_factor>1) && ds.pot_energy>slice_point+s_threshold)
    { 
      range = (nhigh_bnd-nlow_bnd) / (s_factor>0 ? s_factor : -s_factor);
      if (range<=0) abort();
      while (nlow_bnd+range<curr_q) 
      { nlow_bnd += range;
      }
      while (nhigh_bnd-range>curr_q) 
      { nhigh_bnd -= range;
      }
    } 
  }
}


/* PERFORM ONE-VARIABLE SLICE SAMPLING UPDATES FOR COORDINATES IN SOME RANGE. */
void mc_slice_1
( mc_dynamic_state& ds, /* Structure holding pointers to dynamical state */
  data_specifications& data_spec,  
  int max_steps,	/* Maximum number of intervals, zero for unlimited; if negative, intervals are found by doubling */
  int s_factor,		/* Factor for faster shrinkage */
  double s_threshold,	/* Threshold for faster shrinkage */  
  int k,
  double width,
  bool use_domain,
  int bounded, 
  double low, 
  double high,
  int& rejections)
{
  double curr_q, slice_point, low_bnd, high_bnd;
  
  mc_app_energy (ds, data_spec, ds.pot_energy, k);
  
  double z = rand_exp();

  slice_point = ds.pot_energy + z;
  curr_q = ds.q[k];
  
  if (use_domain) {
    low_bnd = low;
    high_bnd = high;
  }
  else {
    double lower = low, higher = high;

    if (max_steps>=0)
      { step_out (ds, data_spec, k, slice_point, curr_q, max_steps, low_bnd, high_bnd, 
		  width, lower, higher, bounded);
      }
    else
      { dbl_out (ds, data_spec, k, slice_point, curr_q, -max_steps, low_bnd, high_bnd, 
		 width, lower, higher, bounded);
      }
  }
  
  pick_value (ds, data_spec, k, slice_point, curr_q, max_steps, low_bnd, high_bnd, 
	      s_factor, s_threshold, width, rejections);
}


/**********************************************************************************/

/* PERFORM METROPOLIS UPDATE ON ONE COMPONENT AT A TIME. */
void mc_met_1
( mc_dynamic_state& ds,	/* State to update */
  data_specifications& data_spec,  
  double sf,	
  double temperature,
  int b_accept,		/* Use Barker/Boltzmann acceptance probability? */
  int k,
  bool use_domain,
  double lower,
  double higher, 
  int& prop,
  int& rej
)
{
  double old_energy, qsave, U, a, delta;
  
  mc_app_energy (ds, data_spec, ds.pot_energy, k);
  
  old_energy = ds.pot_energy;  
  qsave = ds.q[k];  

  if (use_domain) {  // Uniform distribution
    ds.q[k] = rand_uniopen() * higher;
  }
  else {
    ds.q[k] += sf * rand_gaussian();

    if ((ds.q[k] < lower) || (ds.q[k] > higher)) {
      prop += 1;
      ds.pot_energy = old_energy;    
      ds.q[k] = qsave;
      rej += 1;  
      return;
    }     
  }

  mc_app_energy (ds, data_spec, ds.pot_energy, k);
  
  prop += 1;
  delta = ds.pot_energy - old_energy;
  
  U = rand_uniform(); /* Do every time to keep in sync for coupling purposes*/
  
  a = exp(-delta/temperature);
  if (b_accept) { 
    a = 1/(1+1/a);
  }

  if (U >= a) {   // rejection
    rej += 1;   
    ds.pot_energy = old_energy;    
    ds.q[k] = qsave;
  }
}


/* PERFORM HEATBATH UPDATE OF MOMENTUM. */
//void mc_heatbath
//( mc_dynamic_state& ds,	[> State to update <]
  //double temperature,	[> Temperature of heat bath <]
  //double decay,		[> Decay factor for existing momentum <]
  //int start_index,
  //int end_index
//)
//{ 
  //double std_dev;
  //int j;

  //if (decay != 0) {
    //fprintf(stderr, "Decay should be zero %f\n", decay);
    //exit(1);
  //}

  //std_dev = sqrt(temperature * (1-decay*decay));
  
  ////printf("%f\t%f\n", temperature, decay);
  
  ////  if (decay==0)
  ////{ 
  //for (j = start_index; j < end_index; j++) { 
    //ds.p[j] = std_dev * rand_gaussian();      // mu + sqrt(variance) * rand_gaussian();
  //}
  //// }
  //[> else
     //{ for (j = start_index; j < end_index; j++)
     //{ ds.p[j] = decay * ds.p[j] + std_dev * rand_gaussian();
     //}
     //}*/
//}


/**********************************************************************************/
/**********************************************************************************/

/*void finite_update */
/*( mc_dynamic_state& ds, [> Structure holding pointers to dynamical state <]*/
  /*data_specifications& data_spec, */
  /*int k*/
/*)*/
/*{  */
  /*long lower = data_spec.lower;*/
  /*long size = data_spec.upper - data_spec.lower + 1;*/
  /*long i;*/
  /*for (i = 0; i < size; i++) {*/
    /*double ivalue = lower + i;*/
    /*ds.q[k] = ivalue;      */
    /*mc_app_energy (ds, data_spec, ds.pot_energy, k);*/
    /*data_spec.loglik[i] = ds.pot_energy;*/
  /*}*/
 
  /*[> Exponentiate to get values proportional to probabilities <]*/
  /*double llsum = 0.0;*/
  /*for (i = 0; i < size; i++) {*/
    /*data_spec.loglik[i] = exp(-data_spec.loglik[i]);*/
    /*llsum += data_spec.loglik[i];*/
  /*}*/
  
  /*[> Sample <]*/
  /*double urand = rand_uniform() * llsum; // runif(0.0, llsum);*/
  /*llsum = 0.0;*/
  /*for (i = 0; i < size - 1; i++) {*/
    /*llsum += data_spec.loglik[i];*/
    /*if (llsum > urand) {*/
      /*break;*/
    /*}*/
  /*}*/
  
  /*double ivalue = lower + i;*/
  /*ds.q[k] = ivalue;                    // new value*/
/*}*/



