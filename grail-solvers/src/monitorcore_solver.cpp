/* 
  Author: Jessy Sheng
  Time: 2/14/2012
  this program for monitoring wifi interfaces are up and running  to gather wifi single.
  this program read in a antennas_conf file which specify mac addresses of wifi interfaces
  and IP address of the hub. this program scan 10 seconds wifi singles from aggregator, compare
  the recever mac address with the read in wifi interfaces  mac address.
  if missing in receiver list, send out email to somebody care
*/

#include <algorithm>
#include <array>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <time.h>
#include <utility>
#include <vector>
#include <sstream>
#include <string.h>

#include <grailV3_solver_client.hpp>
#include "aggregator_solver_protocol.hpp"
#include <netbuffer.hpp>
#include <sample_data.hpp>
#include <world_model_protocol.hpp>
#include <grail_types.hpp>
#include "rapidxml.hpp"

using namespace aggregator_solver;
using namespace rapidxml;

using std::pair;

using world_model::Attribute;
using world_model::grail_time;
using world_model::URI;
//using namespace std;
struct Debug {
  bool on;
};

template<typename T>
Debug& operator<<(Debug& dbg, T arg) {
  if (dbg.on) {
    std::cout<<arg;
  }
  return dbg;
}

std::u16string toU16(const std::string& str) {
  return std::u16string(str.begin(), str.end());
}


unsigned long long MacStrToLong(const std::string str);

void readAllAntennas(std::set<pair<ReceiverID, std::string>>&, const char* );
void CheckAntennas(const std::set<ReceiverID> &revs, const std::set<pair<ReceiverID, std::string>> &ants);


struct Debug debug;
int main(int arg_count, char** arg_vector) {
  	if (3 > arg_count ) {
    		std::cerr<<"the command line input with world model:\n";
    		std::cerr<<"\t"<<arg_vector[0]<<" [<aggregator ip> <aggregator port>]+ <world model ip> <solver port> <client port>\n\n";
    		std::cerr<<"the command line input without world model:\n";
    		std::cerr<<"\t"<<arg_vector[0]<<" [<aggregator ip> <aggregator port>]\n\n";
    		return 0;
  	}

	debug.on = false;
  	//Grab the ip and ports for the aggregators and world model
  	std::vector<NetTarget> servers;
    	std::string server_ip(arg_vector[1]);
    	uint16_t server_port = std::stoi(std::string(arg_vector[2]));
    	servers.push_back(NetTarget{server_ip, server_port});
	if(arg_count > 3) {
  	//World model IP and ports
  	std::string wm_ip(arg_vector[3]);
  	int solver_port = std::stoi(std::string((arg_vector[4])));
  	int client_port = std::stoi(std::string((arg_vector[5])));

  	ClientWorldModel cwm(wm_ip, client_port);
  	if (not cwm.connected()) {
		//send email out to notice world model is down.
    		std::cerr<<"Could not connect to the world model as a client - aborting.\n";
  		}
	} //end of arg_count >3

	//read in monitering antennas' mac and ip address
	std::set<pair<ReceiverID, std::string>> antennas;
	const char* filename="./wifi_antenna.xml";
	readAllAntennas(antennas, filename);
   	//callback for the GRAIL aggregator
  	//Aggregator callback is threaded so we need a mutex
  	std::mutex receivers_mutex;
  	std::set<ReceiverID> receivers;

  	//Connect to the aggregator and update it with new rules as the world model
  	//provided transmitters of interest
  	auto packet_callback = [&](SampleData& s) {
    	if (s.valid ) {
     		if(s.physical_layer == 2){ 
        	std::unique_lock<std::mutex> lck(receivers_mutex);

		//std::cout <<"receiver_id = " << to_string(s.rx_id)<<std::endl;
        	receivers.insert(s.rx_id);
		}
     	}
  };

  	SolverAggregator aggregator(servers, packet_callback);

  	while (1) {
      		Subscription sub;
      		Rule r;
      		r.physical_layer = 2;
      		r. update_interval = 2000; 
      		sub.push_back(r);

      		aggregator.updateRules(sub);
		sleep(10);
		aggregator.Disconnect();
		std::set<ReceiverID>::iterator it;
		for(it = receivers.begin(); it != receivers.end(); it++)
			debug<<*(it)<<"\n";
		CheckAntennas( receivers, antennas);
		receivers.clear();
		//every 10 hours scan once
		sleep(36000);
		debug<<"reconnecting ..........\n";

      }
   	return 0;
}


unsigned long long MacStrToLong(const std::string str){
        unsigned long long v=0x0LL;

        for(int i=0; i<str.length(); i++){
                if(str[i] ==':' || str[i] =='-')
                        continue;
                if(str[i] >= '0' && str[i] <= '9')
                        v = (v<<4) | (str[i]-'0');
                else if(str[i] >= 'a' && str[i] <='f')
                        v = (v<<4) | (str[i]+10-'a');
                else if(str[i] >='A' && str[i] <= 'F')
                        v = (v<<4) | (str[i]+10-'A');
                else {
                        std::cerr<<"convert mac string to long long error\n";
                        v = 0;
                }
        }
        return v;
}

void readAllAntennas(std::set<pair<ReceiverID, std::string>> &ants, const char* filename){
	
        std::ifstream fin(filename);
        if(fin.fail()){
                std::cerr << "cannot load xml file" << filename <<"\n";
                return;
        }
        fin.seekg(0, std::ios::end);
        size_t length = fin.tellg();
        fin.seekg(0, std::ios::beg);
        char* buffer = new char[length+1];
        fin.read(buffer, length);
        buffer[length] = '\0';
        fin.close();

        xml_document<> doc;
        doc.parse<0>(buffer);

        delete[] buffer;
	
	for(xml_node<>* n = doc.first_node("antenna"); n; n = n->next_sibling("antenna")){
		uint64_t mac  ;
		ReceiverID mac_id;
		std::string ip;
		pair<ReceiverID, std::string> pp;
		if(xml_node<>* cur = n->first_node("mac")){
			mac = MacStrToLong(cur->value());
		//	smi.mac = mac;
		}
		if(xml_node<>* cur = n->first_node("ip")){
			ip = cur->value();
		//	smi.ip = ip;
		}

		mac_id = mac;
		pp.first=mac_id;
		pp.second = ip;
		ants.insert(pp);
		debug<<"mac uint64="<< mac << "\n";
		debug<<"mac ReceiverID=" << pp.first << "\n";
	}
}

void CheckAntennas(const std::set<ReceiverID> &revs, const std::set<pair<ReceiverID, std::string>> &ants){
	std::set<pair<ReceiverID, std::string>> ::iterator it;
	bool allworking = true; 
	std::string cmd = "echo \" the following wifi interface are not working:\n ";
	for(it = ants.begin(); it!= ants.end(); it++)
	{
		if(revs.find(it->first) == revs.end()){
			allworking = false;
			std::cout<<"antenna is not working: "<<it->first<<"   ;ip ="<<it->second<<std::endl;
			cmd += "mac = "; 
			cmd += to_string(it->first);
			cmd += ": ip = ";
			cmd += it->second;
			cmd += "\n";
		}		
	}
	if(!allworking) {
		cmd += "\" | mutt -s \"core2/3 Alix monitor\" youremail@cs.rutgers.edu";
		system(cmd.c_str());
		//std::cout << cmd << std::endl;
	}	
}
