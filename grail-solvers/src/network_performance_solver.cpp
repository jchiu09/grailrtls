/*
 * Copyright (c) 2011 Bernhard Firner and Rutgers University
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or visit http://www.gnu.org/licenses/gpl-2.0.html
 */

/*******************************************************************************
 * This solver measures the network performance of packets from from a group
 * of aggregators. Metrics are:
 *  Number of transmitters and receivers
 *  Packet Loss %
 *  Throughput
 *  Total number of packets
 *  Average number of receptions per packet
 ******************************************************************************/

#include <algorithm>
#include <array>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include <unistd.h>

#include <grail_types.hpp>
#include <grailV3_solver_client.hpp>
#include "aggregator_solver_protocol.hpp"
#include <netbuffer.hpp>
#include <sample_data.hpp>
#include <world_model_protocol.hpp>

using std::map;
using std::pair;
using std::set;

using namespace aggregator_solver;

int main(int arg_count, char** arg_vector) {
  if (7 > arg_count or (arg_count % 2) == 0) {
    std::cerr<<"This program needs 7 or more arguments:\n";
    std::cerr<<"\t"<<arg_vector[0]<<" [<aggregator ip> <aggregator port>]+ <world model ip> <solver port> <client port> <region>\n\n";
    std::cerr<<"Any number of aggregator ip/port pairs may be provided to connect to multiple aggregators.\n";
    return 0;
  }

  //Grab the ip and ports for the aggregators and world model
  std::vector<NetTarget> servers;
  for (int s_num = 1; s_num < arg_count - 4; s_num += 2) {
    std::string server_ip(arg_vector[s_num]);
    uint16_t server_port = std::stoi(std::string(arg_vector[s_num + 1]));
    servers.push_back(NetTarget{server_ip, server_port});
  }

  //World model IP and ports
  std::string wm_ip(arg_vector[arg_count - 4]);
  int solver_port = std::stoi(std::string((arg_vector[arg_count - 3])));
  int client_port = std::stoi(std::string((arg_vector[arg_count - 2])));

  //Read in the region name
  std::u16string region_name;
  {
    std::string region(arg_vector[arg_count - 1]);
    region_name = std::u16string(region.begin(), region.end());
  }

  //We will sample statistics and then sleep so define those intervals here.
  size_t sample_time = 20;
  size_t sleep_interval = 40;

  //Set up the solver world model connection;
  std::u16string origin = u"grail/performance solver\nversion 1.0";

  //Provide various network statistics
  std::vector<std::pair<u16string, bool>> type_pairs{{u"transmitters.int64", false},
                                                     {u"receivers.int64", false},
                                                     {u"packet loss percent.double", false},
                                                     {u"no mim packet loss percent.double", false},
                                                     {u"throughput.bytes.double", false},
                                                     {u"throughput.packets.double", false},
                                                     {u"receptions per packet.double", false},
                                                     {u"percent packets received.double", false},
                                                     {u"active.bool", false}};

  SolverWorldModel swm(wm_ip, solver_port, type_pairs, origin);
  if (not swm.connected()) {
    std::cerr<<"Could not connect to the world model as a solver - aborting.\n";
    return 0;
  }

  //Ask the world model which receivers are message in message receivers
  ClientWorldModel cwm(wm_ip, client_port);
  if (not cwm.connected()) {
    std::cerr<<"Could not connect to the world model as a client - aborting.\n";
    return 0;
  }
  std::mutex wm_mutex;
  std::map<grail_types::transmitter, std::u16string> txnames;
  std::set<grail_types::transmitter> mim_rxers;
  std::set<grail_types::transmitter> repeaters;
  std::function<void (ClientWorldModel::world_state&, uint32_t)> wm_callback =
    [&](ClientWorldModel::world_state& ws, uint32_t ticket) {
      std::unique_lock<std::mutex> lck(wm_mutex);
      for (auto I = ws.begin(); I != ws.end(); ++I) {
        if (not I->second.empty()) {
          for (auto attr = I->second.begin(); attr != I->second.end(); ++attr) {
            if (attr->data.size() == sizeof(grail_types::transmitter)) {
              grail_types::transmitter tx = grail_types::readTransmitter(attr->data);
              //MiM request
              if (ticket == 1) {
                //See if this is an expiration notice
                if (attr->expiration_date != 0) {
                  std::cerr<<"Expiring MiM receiver "<<tx<<'\n';
                  mim_rxers.erase(tx);
                }
                else {
                  std::cerr<<"Remembering new MiM receiver "<<tx<<'\n';
                  mim_rxers.insert(tx);
                }
              }
              //Sensor name request
              else if (ticket == 2) {
                //See if this is an expiration notice
                if (attr->expiration_date != 0) {
                  std::cerr<<"Expiring name for "<<tx<<'\n';
                  txnames.erase(tx);
                }
                else {
                  std::cerr<<"Remembering new tx name for "<<tx<<'\n';
                  txnames[tx] = I->first;
                }
              }
              //Repeater
              else if (ticket == 3) {
                //See if this is an expiration notice
                if (attr->expiration_date != 0) {
                  std::cerr<<"Expiring repeater "<<tx<<'\n';
                  repeaters.erase(tx);
                }
                else {
                  std::cerr<<"Remembering new repeater "<<tx<<'\n';
                  repeaters.insert(tx);
                }
              }
            }
          }
        }
      }
    };
  //Ticket 1 requests all MiM sensors, ticket 2 just gets all sensors
  //so we can record their associated URI names.
  world_model::grail_time interval = 2000;
  std::vector<world_model::URI> mim_names{u"sensor.mim"};
  cwm.setupAsynchronousDataStream(u".*", mim_names, interval, wm_callback, 1);

  std::vector<world_model::URI> all_names{u"sensor.*"};
  cwm.setupAsynchronousDataStream(u".*", all_names, interval, wm_callback, 2);

  std::vector<world_model::URI> repeater_names{u"sensor.repeater"};
  cwm.setupAsynchronousDataStream(u".*", mim_names, interval, wm_callback, 3);

  //Remember the packet loss rates of individual transmitters, but don't update
  //them in the world model each sampling period. Instead update these values
  //after every 1000 packets to give enough time for meaningful statistics
  //to be gathered.
  map<grail_types::transmitter, std::pair<uint32_t, uint32_t>> rxed_expected;

  while (true) {
    /****************************************************************************/
    //Remember various statistics about each physical layer.
    //Remember what transmitters and receivers are present.
    map<unsigned char, set<TransmitterID>> transmitters;
    map<unsigned char, set<ReceiverID>> receivers;

    //Record the total number of packets and total amount of information
    map<unsigned char, int64_t> num_packets;
    map<unsigned char, int64_t> bytes_data;

    //Keep track of packet intervals to detect missing packets
    map<pair<unsigned char, TransmitterID>, int64_t> last_received;
    map<unsigned char, map<TransmitterID, map<int64_t, set<ReceiverID>>>> last_received_by;
    //Use the total received packets with the expected number
    //(from the intervals variable) to estimate the average numbers
    //of packets per transmission.
    map<unsigned char, map<TransmitterID, int64_t>> total_received;
    //The time spent collecting packets, in milliseconds.
    int64_t collect_start;
    int64_t collect_time;
    /****************************************************************************/

    //Connect to the aggregator and request everything.
    //Collect sample_time seconds of data every minute
    auto packet_callback = [&](SampleData& s) {
      if (s.valid) {
        if (0 == repeaters.count(grail_types::transmitter{s.physical_layer, s.tx_id}) and
            0 == repeaters.count(grail_types::transmitter{s.physical_layer, s.rx_id})) {
          //Remember that this transmitter and receiver exist on this phy
          transmitters[s.physical_layer].insert(s.tx_id);
          ++total_received[s.physical_layer][s.tx_id];
          receivers[s.physical_layer].insert(s.rx_id);
          //Pipsqueak tags have 3 bytes of ID
          if (s.physical_layer == 1) {
            bytes_data[s.physical_layer] += 3 + s.sense_data.size();
          }
          //TODO Other physical layers

          //Increment the total number of packets
          num_packets[s.physical_layer]++;

          //We can only keep loss statistics for packets with regular intervals.
          if (s.physical_layer == 1) {
            //TODO FIXME using local time on this computer which ignores any delays
            //in data transmission and such. We might overestimate packet loss.
            int64_t now = world_model::getGRAILTime();
            auto phy_tx = std::make_pair(s.physical_layer, s.tx_id);
            //Initialize if this is the first time this transmitter is seen
            if (last_received.find(phy_tx) == last_received.end()) {
              last_received[phy_tx] = now;
            }
            //Assume we are in the last interval
            int64_t last = last_received[phy_tx];
            //If at least 225 milliseconds has passed then we might
            //be in a new interval
            if (now > last + 225) {
              last = now;
            }
            //Update the interval time
            last_received[phy_tx] = last;
            //Remember that this receiver saw this packet transmission in this interval
            last_received_by[s.physical_layer][s.tx_id][last].insert(s.rx_id);
            //std::cerr<<(unsigned int)s.physical_layer<<"."<<s.tx_id<<" from rx "<<s.rx_id<<" at time "<<now<<'\n';
          }
        }
      }
    };

    {
      //Connect to the aggregators and get sample_time seconds of data.
      SolverAggregator aggregator(servers, packet_callback);
      //Subscribe to all physical layers, update every 50 ms
      Rule r;
      r.physical_layer = 0;
      r.update_interval = 50;
      Subscription sub{r};
      aggregator.updateRules(sub);
      collect_start = world_model::getGRAILTime();
      sleep(sample_time);
    }
    //Now compile the statistics taken over the last sample_time seconds
    int64_t now = world_model::getGRAILTime();
    collect_time = now - collect_start;
    double collect_seconds = collect_time/1000.0;

    std::vector<SolverWorldModel::Solution> solns;
    //Send transmitter and receiver counts

    //Count transmitters that were seen at least twice.
    for (auto txers = transmitters.begin(); txers != transmitters.end(); ++txers) {
      //If this physical layer beacons then use that as a filter for real IDs
      //Otherwise just use the observed IDs
      std::string phy = std::to_string((unsigned int)txers->first);
      std::u16string uri = region_name + u".physical layer." + u16string(phy.begin(), phy.end());
      SolverWorldModel::Solution soln{u"transmitters.int64", now, uri, std::vector<uint8_t>()};
      if (not last_received_by[txers->first].empty()) {
        int64_t count = 0;
        //Loop over every device in this physical layer to count the number of transmitters
        for (auto I = last_received_by[txers->first].begin(); I != last_received_by[txers->first].end(); ++I) {
          //For physical layer 1 make sure this device has been declared before
          if (I->second.size() > 1 and
              (1 != txers->first or txnames.end() != txnames.find(grail_types::transmitter{txers->first, I->first}))) {
            ++count;
          }
        }
        std::cerr<<"Total transmitters on phy "<<phy<<" is "<<count<<'\n';
        pushBackVal<int64_t>(count, soln.data);
        solns.push_back(soln);
      }
      else {
        std::cerr<<"Total transmitters on phy "<<phy<<" is "<<txers->second.size()<<'\n';
        pushBackVal<int64_t>(txers->second.size(), soln.data);
        solns.push_back(soln);
      }
    }

    //Count the number of receivers
    for (auto I = receivers.begin(); I != receivers.end(); ++I) {
      std::string phy = std::to_string((unsigned int)I->first);
      std::u16string uri = region_name + u".physical layer." + u16string(phy.begin(), phy.end());
      SolverWorldModel::Solution soln{u"receivers.int64", now, uri, std::vector<uint8_t>()};
      std::cerr<<"Total receivers on phy "<<phy<<" is "<<I->second.size()<<'\n';
      pushBackVal<int64_t>(I->second.size(), soln.data);
      solns.push_back(soln);
    }

    //Send numbers of packets and bytes transferred. These need to be divided
    //by the time in seconds to convert to throughput
    for (auto I = num_packets.begin(); I != num_packets.end(); ++I) {
      std::string phy = std::to_string((unsigned int)I->first);
      std::u16string uri = region_name + u".physical layer." + u16string(phy.begin(), phy.end());
      SolverWorldModel::Solution soln{u"throughput.packets.double", now, uri, std::vector<uint8_t>()};
      std::cerr<<"Packet throughput on phy "<<phy<<" is "<<I->second/collect_seconds<<'\n';
      //Divide the total number of packets by the collection time in seconds
      pushBackVal<double>(I->second / collect_seconds, soln.data);
      solns.push_back(soln);
    }
    for (auto I = bytes_data.begin(); I != bytes_data.end(); ++I) {
      std::string phy = std::to_string((unsigned int)I->first);
      std::u16string uri = region_name + u".physical layer." + u16string(phy.begin(), phy.end());
      SolverWorldModel::Solution soln{u"throughput.bytes.double", now, uri, std::vector<uint8_t>()};
      std::cerr<<"Data throughput on phy "<<phy<<" is "<<I->second/collect_seconds<<'\n';
      //Divide the total number of packets by the collection time in seconds
      pushBackVal<double>(I->second / collect_seconds, soln.data);
      solns.push_back(soln);
    }

    //Remember which transmitters and receivers are active
    std::map<grail_types::transmitter, bool> device_active;
    //Now update the packet loss percent and receptions per packet values
    for (auto I = last_received_by.begin(); I != last_received_by.end(); ++I) {
      //Also count the miss rate of each receiver
      std::map<grail_types::transmitter, int64_t> rx_receptions;

      std::string phy = std::to_string((unsigned int)I->first);
      uint8_t phy_num = std::stoi(phy);

      std::u16string uri = region_name + u".physical layer." + u16string(phy.begin(), phy.end());
      int64_t received = 0;
      int64_t non_mim_received = 0;
      int64_t total_expected = 0;
      int64_t total_multiples = 0;
      //Examine packets and determine a packet interval
      for (auto J = I->second.begin(); J != I->second.end(); ++J) {
        //Skip transmitters that are not known as they may be spurious
        if (1 == I->first and txnames.end() == txnames.find(grail_types::transmitter{I->first, J->first})) {
          continue;
        }
        //Try to screen out spurious packets by only looking at packets that
        //come from known sources
        if (J->second.size() > 1) {
          //Use the lower quartile's interval to estimate the actual
          //interval and use that to estimate the expected number of
          //packets.
          std::vector<int64_t> intervals;
          {
            auto tx_time = J->second.begin();
            int64_t last_time = tx_time->first;
            for (; tx_time != J->second.end(); ++tx_time) {
              intervals.push_back(tx_time->first - last_time);
              last_time = tx_time->first;
            }
          }
          std::sort(intervals.begin(), intervals.end());
          //Pick the lower quartile interval as a reasonable estimate.
          int64_t interval = intervals[intervals.size()/4];
          if (interval == 0) {
            continue;
          }

          int64_t rxed = 0;         //Total number of transmissions received (not counting duplicates)
          int64_t non_mim_rxed = 0; //Total number of transmissions received, not counting MiM
          int64_t missed = 0;       //Missed transmissions
          int64_t multiples = 0;    //Number of receivers that saw a packet for each transmission
          auto not_mim = [&](const ReceiverID& rid) {
            std::unique_lock<std::mutex> lck(wm_mutex);
            return 0 == mim_rxers.count(grail_types::transmitter{1, rid});
          };
          auto log_interval = [&](pair<int64_t, set<ReceiverID>>& group) {
            //std::cerr<<"Logging group for txer "<<J->first<<" at time "<<group.first<<" from rxers ";
            //for (auto R = group.second.begin(); R != group.second.end(); ++R) { std::cerr<<"\t"<<*R;}
            //std::cerr<<'\n';

            //Remember how many packets each receiver successfuly heard
            std::for_each(group.second.begin(), group.second.end(), [&] (const uint128_t& r) {
                rx_receptions[grail_types::transmitter{phy_num, r}]++; });

            if (std::any_of(group.second.begin(), group.second.end(), not_mim)) {
              ++non_mim_rxed;
            }
            ++rxed;
            multiples += group.second.size();
          };

          //Loop over the received values but merge groups that are in the same interval
          auto rx_group = J->second.begin();
          pair<int64_t, set<ReceiverID>> last_group = *rx_group;
          for (rx_group++; rx_group != J->second.end(); ++rx_group) {
            //Merge if these are in the same interval
            if (rx_group->first < last_group.first + 0.5*interval) {
              std::for_each(rx_group->second.begin(), rx_group->second.end(),
                  [&](ReceiverID rxid) {last_group.second.insert(rxid);});
              //std::cerr<<"Merging group at time "<<last_group.first<<" with ";
              //for (auto R = last_group.second.begin(); R != last_group.second.end(); ++R) { std::cerr<<"\t"<<*R;}
              //std::cerr<<'\n';
              //std::cerr<<"\twith group at time "<<rx_group->first<<" with ";
              //for (auto R = rx_group->second.begin(); R != rx_group->second.end(); ++R) { std::cerr<<"\t"<<*R;}
              //std::cerr<<'\n';
            }
            else {
              //Log the last group
              log_interval(last_group);
              int64_t cur_interval = rx_group->first - last_group.first;
              //Packets were missed
              if (2 <= cur_interval / interval) {
                //std::cerr<<"MISS: "<<cur_interval<<"/"<<interval<<"\n";
                missed += (cur_interval / interval) - 1;
              }
              last_group = *rx_group;
            }
          }
          //Log the very last group
          log_interval(last_group);

          //Log the statistics of this individual transmitter
          grail_types::transmitter tx{I->first, J->first};
          auto tx_stats = rxed_expected.find(tx);
          if (rxed_expected.end() == tx_stats) {
            rxed_expected[tx] = std::make_pair(0, 0);
            tx_stats = rxed_expected.find(tx);
          }
          tx_stats->second.first += rxed;
          tx_stats->second.second += missed + rxed;
          //Log this transmitter as active
          device_active[tx] = true;

          //Now record the values
          total_multiples += multiples;
          received += rxed;
          non_mim_received += non_mim_rxed;
          //Slightly biased since at least 1 packet must have been received
          total_expected += missed + rxed;
          //Debugging
          //std::cerr<<"Estimated interval was "<<interval<<" with "<<rxed<<" received and "<<missed<<" missed in interval of length "<<collect_time<<".\n";
        }
      }

      if (total_expected > 0) {
        std::cerr<<"On channel "<<(unsigned int)I->first<<" expected "<<
          total_expected<<" total packets and got "<<received<<" with "<<
          total_multiples<<" packets counting multiples.\n";
        SolverWorldModel::Solution loss_soln{u"packet loss percent.double", now, uri, std::vector<uint8_t>()};
        pushBackVal<double>(1.0 - (double)received / total_expected, loss_soln.data);
        solns.push_back(loss_soln);
        SolverWorldModel::Solution no_mim_loss_soln{u"no mim packet loss percent.double", now, uri, std::vector<uint8_t>()};
        pushBackVal<double>(1.0 - (double)non_mim_received / total_expected, no_mim_loss_soln.data);
        solns.push_back(no_mim_loss_soln);
        SolverWorldModel::Solution receptions_soln{u"receptions per packet.double", now, uri, std::vector<uint8_t>()};
        pushBackVal<double>((double)total_multiples / total_expected, receptions_soln.data);
        solns.push_back(receptions_soln);
        std::cerr<<"Phy "<<(unsigned int)I->first<<
          " has loss rate "<<(1.0 - (double)received / total_expected)<<'\n';
        std::cerr<<"Phy "<<(unsigned int)I->first<<
          " has non MiM loss rate "<<(1.0 - (double)non_mim_received / total_expected)<<'\n';
        std::cerr<<"Phy "<<(unsigned int)I->first<<
          " saw "<<((double)total_multiples / total_expected)<<" packets per transmission\n";

        //Update individual receiver statistics
        for (auto rxer = rx_receptions.begin(); rxer != rx_receptions.end(); ++rxer) {
          auto uri_name = txnames.find(rxer->first);
          if (uri_name != txnames.end()) {
            device_active[rxer->first] = true;
            //Find the object URI that this receiver corresponds to
            std::u16string rx_uri = uri_name->second;
            std::cerr<<"Receiver "<<rxer->first<<
              " has reception rate "<<((double)rxer->second / total_expected)<<'\n';

            SolverWorldModel::Solution rx_reception{u"percent packets received.double", now, rx_uri, std::vector<uint8_t>()};
            pushBackVal<double>(((double)rxer->second / total_expected), rx_reception.data);
            solns.push_back(rx_reception);
          }
        }
      }
    }

    //Now add active/inactive information
    for (auto I = txnames.begin(); I != txnames.end(); ++I) {
      SolverWorldModel::Solution activity{u"active.bool", now, I->second, std::vector<uint8_t>()};
      pushBackVal<bool>(device_active[I->first], activity.data);
      std::cerr<<"Device "<<std::string(I->second.begin(), I->second.end())<<
        " is "<<(device_active[I->first] ? "active" : "inactive")<<'\n';
      solns.push_back(activity);
    }

    //Create new entries for these URIs if they don't exist yet.
    if (solns.size() > 0) {
      swm.sendData(solns, true);
    }

    //Update individual transmitter statistics once every 1000 packets
    std::vector<SolverWorldModel::Solution> individual_solns;
    for (auto txer = rxed_expected.begin(); txer != rxed_expected.end(); ++txer) {
      if (txer->second.second >= 1000) {
        auto uri_name = txnames.find(txer->first);
        if (uri_name != txnames.end()) {
          //Find the object URI that this transmitter corresponds to
          std::u16string tx_uri = uri_name->second;
          SolverWorldModel::Solution loss_soln{u"packet loss percent.double", now, tx_uri, std::vector<uint8_t>()};
          pushBackVal<double>(1.0 - (double)txer->second.first / txer->second.second, loss_soln.data);
          individual_solns.push_back(loss_soln);
          std::cerr<<"Transmitter "<<txer->first<<
            " has loss rate "<<(1.0 - (double)txer->second.first / txer->second.second)<<'\n';
        }
        txer->second = std::make_pair(0, 0);
      }
    }

    //Do not create entries for these URIs if they don't exist (they may be
    //deleted while these calculations are taking place)
    if (individual_solns.size() > 0) {
      swm.sendData(individual_solns, false);
    }

    //Sample again after the sleep interval
    sleep(sleep_interval);
  }

  return 0;
}
