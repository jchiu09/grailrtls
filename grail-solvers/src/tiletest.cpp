/*
tiletest.cpp - Sample program to test the tile localization algorithms.
Source version: January 11, 2011
Copyright (c) 2011 Bernhard Firner

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
  claim that you wrote the original software. If you use this software
  in a product, an acknowledgment in the product documentation would be
  appreciated but is not required.

  2. Altered source versions must be plainly marked as such, and must not be
  misrepresented as being the original software.

  3. This notice may not be removed or altered from any source
  distribution.
*/

#include <limits>
#include <iostream>

#include <tile_localize.hpp>
#include <triangulate.hpp>

using namespace std;
using namespace tile_localize;

//Conevenience struct to convert a coordinate to a tile position
struct Converter {
  double tile_size;
  int operator()(double location){
    //Truncate the result instead of rounding so we return the tile which
    //this coordinate lies inside. eg. [0,1] is tile 0, [1,2] is tile 1.
    if (location == 0.0) { return 0;}
    //Start counting from zero so subtract 1
    return (location-1.0)/tile_size;
  }
};

int main() {
  double max_x = 10.0;
  double max_y = 10.0;
  double tile_w = 2.0;
  double tile_h = 2.0;
  double threshold = 7;

  const float qnan = std::numeric_limits<float>::quiet_NaN();
  //These are the ``real'' made up locations. Set one of them to nan for testing.
  //vector<Sample> train_data{Sample{0, 0, vector<double>{-10, -24}},
                            //Sample{10, 0, vector<double>{-20, -20}},
                            //Sample{0, 10, vector<double>{-20, -20}},
                            //Sample{3, 3, vector<double>{-10, -20}},
                            //Sample{8, 10, vector<double>{-22, -12}},
                            //Sample{7, 7, vector<double>{-20, -10}}};
  vector<Sample> train_data{Sample{0, 0, vector<double>{-10, -24}},
                            Sample{10, 0, vector<double>{-20, -20}},
                            Sample{0, 10, vector<double>{-20, -20}},
                            Sample{3, 3, vector<double>{-10, -20}},
                            Sample{8, 10, vector<double>{-22, -12}},
                            Sample{qnan, qnan, vector<double>{-20, -10}}};
  LandmarkData rx_locations{ pair<double, double>{0, 0}, pair<double, double>{10, 10}};

  //Print out the interpolated map grid (IMG)
  //Create a mesh columns wids and rows high.
  int columns = ceil((max_x / tile_w));
  int rows = ceil((max_x / tile_h));
  Converter toCol{tile_w};
  Converter toRow{tile_h};
  {
    vector<Sample> train_data2 = train_data;
    train_data2.erase(train_data2.begin() + 5);
    for_each(train_data2.begin(), train_data2.end(),
        [&](Sample& s){s.x = toCol(s.x);
                       s.y = toRow(s.y);});
    vector<Point> vertices(train_data.size()-1);
    for (int i = 0; i < vertices.size(); ++i) {
      vertices[i] = Point{(double)toCol(train_data[i].x), (double)toRow(train_data[i].y)};
    }
    vector<Tri> mesh = genTriangles(columns - 1, rows - 1, vertices);
    vector< vector<vector<double>>> signal_maps =
      buildSignalMaps(train_data2, mesh, rx_locations, columns, rows);
    for (int i = 0; i < rx_locations.size(); ++i) {
      cerr<<"IMG "<<i;
      for (int y = 0; y < rows; ++y) {
        cerr<<'\n';
        for (int x = 0; x < columns; ++x) {
          cerr.precision(3);
          cerr<<signal_maps[i][x][y]<<'\t';
        }
      }
      cerr<<"\n\n";
    }
  }



  //vector<Tri> result = genTriangles(max_x, max_y, vertices);
  vector<Result> result = SPMSolve(train_data, rx_locations, max_x, max_y, tile_w, tile_h, threshold);

  for (auto R = result.begin(); R != result.end(); ++R) {
    cout<<"Tiles: ";
    for (auto T = R->begin(); T != R->end(); ++T) {
      cout<<'\t'<<T->column<<","<<T->row;
    }
    cout<<'\n';
  }
  /*
  cout<<"set xrange[-0.5:10.5]\nset yrange[-0.5:10.5]\n";

  for (auto T = result.begin(); T != result.end(); ++T) {
    cout<<"set arrow from "<<T->a<<" to "<<T->b<<" nohead\n";
    cout<<"set arrow from "<<T->b<<" to "<<T->c<<" nohead\n";
    cout<<"set arrow from "<<T->c<<" to "<<T->a<<" nohead\n";
    //cout<<*T<<'\n';
  }

  cout<<"set terminal png\n";
  cout<<"set output 'out.png'\n";
  cout<<"plot -2 notitle\n";
  */

  return 0;
}
