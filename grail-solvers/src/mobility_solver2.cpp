/*******************************************************************************
 * Use variance information from the world model to provide mobility information
 * to the world model.
 ******************************************************************************/

#include <algorithm>
#include <cmath>
#include <deque>
#include <functional>
#include <map>
#include <mutex>
#include <string>
#include <thread>
#include <tuple>
#include <utility>
#include <vector>

//TODO Remove this (used for usleep) and replace with C++0x sleep mechanism
#include <unistd.h>

#include <grailV3_solver_client.hpp>
#include <netbuffer.hpp>
#include <sample_data.hpp>
#include <world_model_protocol.hpp>
#include <grail_types.hpp>

using std::deque;
using std::map;
using std::pair;
using std::tuple;
using std::u16string;
using std::vector;

using world_model::Attribute;
using world_model::grail_time;
using world_model::URI;

typedef pair<unsigned char, TransmitterID> UniqueTxer;

std::u16string txerToUString(UniqueTxer& ut) {
  std::string str = std::to_string(ut.first) + "." + std::to_string(ut.second.lower);
  return u16string(str.begin(), str.end());
}

int main(int ac, char** av) {
  if (4 != ac) {
    std::cerr<<"This program needs 3 arguments:\n";
    std::cerr<<"\t"<<av[0]<<" <world model ip> <solver port> <client port>\n";
    std::cerr<<"The solution type \"average variance\" will be used for mobility detection.\n";
    return 0;
  }

  //World model IP and ports
  std::string wm_ip(av[1]);
  int solver_port = std::stoi(std::string((av[2])));
  int client_port = std::stoi(std::string((av[3])));

  double threshold = 3.68;

  //Set up the solver world model connection;
  std::string origin = "grail/mobility_solver\nversion 1.0\nthreshold "+std::to_string(threshold);
  //Provide mobility as a non-transient type
  std::vector<std::pair<u16string, bool>> type_pairs{{u"mobility", false}};
  SolverWorldModel swm(wm_ip, solver_port, type_pairs, u16string(origin.begin(), origin.end()));
  if (not swm.connected()) {
    std::cerr<<"Could not connect to the world model as a solver - aborting.\n";
    return 0;
  }
  
  ClientWorldModel cwm(wm_ip, client_port);
  if (not cwm.connected()) {
    std::cerr<<"Could not connect to the world model as a client - aborting.\n";
    return 0;
  }

  enum class Ticket : uint32_t {sensors, variance};

  //Get attributes named "average variance"
  //If that parameter goes above threshold declare that item moving.
  URI any_sensor = u".*";
  vector<URI> sensor_attributes{u"sensor.*"};
  grail_time interval = 1000;
  cwm.setupSynchronousDataStream(any_sensor, sensor_attributes, interval, (uint32_t)Ticket::sensors);
  URI any_link = u".*";
  vector<URI> link_attributes{u"average variance"};
  cwm.setupSynchronousDataStream(any_link, link_attributes, interval, (uint32_t)Ticket::variance);

  //Keep track of the current status we've reported
  map<URI, bool> moving;
  map<u16string, URI> transmitter_to_uri;

  //Define some functions to make finding attributes simple
  auto is_transmitter = [&](Attribute& a) { return a.name.find(u"sensor") != std::u16string::npos;};
  auto is_variance = [&](Attribute& a) { return a.name == u"average variance";};

  std::cerr<<"Starting loop...\n";
  while (1) {
    //Get world model updates
    ClientWorldModel::world_state ws;
    uint32_t ticket;
    std::tie(ws, ticket) = cwm.getSynchronousStreamUpdate();
    //Make a list of new solutions
    vector<SolverWorldModel::Solution> solns;
    //Check each URI to see if it has begun to move or has stopped moving
    for (auto I = ws.begin(); I != ws.end(); ++I) {
      //Check if there is a transmitter attribute
      auto tx_attrib = std::find_if(I->second.begin(), I->second.end(), is_transmitter);
      if (tx_attrib != I->second.end()) {
        //Transmitters are stored as one byte of physical layer and 16 bytes of ID
        grail_types::transmitter txid = grail_types::readTransmitter(tx_attrib->data);
        std::string tx_name = to_string(txid.phy) + "." + to_string(txid.id.lower);
        if (transmitter_to_uri.find(u16string(tx_name.begin(), tx_name.end())) == transmitter_to_uri.end()) {
          std::cout<<"Adding new transmitter to URI: "<<tx_name<<"->"<<std::string(I->first.begin(), I->first.end())<<'\n';
        }
        transmitter_to_uri[u16string(tx_name.begin(), tx_name.end())] = I->first;
      }
      auto var_attrib = std::find_if(I->second.begin(), I->second.end(), is_variance);
      //If this is variance: first determine if this corresponds to a transmitter of a URI
      if (var_attrib != I->second.end() and transmitter_to_uri.end() != transmitter_to_uri.find(I->first)) {
        URI uri = transmitter_to_uri[I->first];
        double variance = readPrimitive<double>(var_attrib->data, 0);
        //Threshold is for standard deviation.
        bool is_moving = sqrt(variance) >= threshold;
        if (moving.find(uri) == moving.end() or is_moving != moving[uri]) {
          //Make the transition elastic
          if ((not moving[uri] and sqrt(variance) >= 1.05*threshold) or
              (moving[uri] and sqrt(variance) <= 0.95 * threshold)) {
            moving[uri] = is_moving;
            SolverWorldModel::Solution soln{u"mobility", var_attrib->creation_date, uri, vector<uint8_t>()};
            pushBackVal<uint8_t>(is_moving ? 1 : 0, soln.data);
            std::cerr<<"Sending mobility solution for "<<std::string(uri.begin(), uri.end())<<": "<<(is_moving ? "moving\n" : "not moving\n");
            solns.push_back(soln);
          }
        }
      }
    }
    if (not solns.empty()) {
      //Do not create URIs for these entries, just send the data
      swm.sendData(solns, false);
    }
  }

  return 0;
}

