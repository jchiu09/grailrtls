/*
zone_localize.{cpp,hpp} - Localize by marking regions as possible or impossible
                          locations for signal origin.
Source version: January 25, 2011
Copyright (c) 2011 Bernhard Firner

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
  claim that you wrote the original software. If you use this software
  in a product, an acknowledgment in the product documentation would be
  appreciated but is not required.

  2. Altered source versions must be plainly marked as such, and must not be
  misrepresented as being the original software.

  3. This notice may not be removed or altered from any source
  distribution.
*/

#include <algorithm>
#include <climits>
#include <cmath>
#include <numeric>
#include <utility>
#include <vector>

#include <zone_localize.hpp>
#include <localize_common.hpp>

#include <iostream>

using namespace std;

std::vector<Tile> zoneTileLocalize(const Sample& s,
                                   const LandmarkData& rxers,
                                   double max_x,
                                   double max_y,
                                   double tile_w,
                                   double tile_h,
                                   double min_dist,
                                   double min_diff,
                                   int num_levels) {
  int columns = ceil(max_x/tile_w);
  int rows    = ceil(max_y/tile_h);

  //A record of how many times each tile has been marked as probable or
  //improbable.
  //+1 for each time a tile is marked as probable and -1 for each time a
  //tile is marked as not probable.
  vector<vector<int>> hits(columns, vector<int>(rows, 0));

  //TODO remove this - it is for debugging
  //for (int r = 0; r < rxers.size(); ++r) {
    //double x = rxers[r].first;
    //double y = rxers[r].second;
    //double z = s.rssis[r];
    //cerr<<"set arrow from "<<x<<", "<<y<<", 0 to "<<x<<", "<<y<<", "<<z<<"\n";
  //}

  //Use each pair of receivers as a landmark to guess a zone where the
  //signal may have originated.
  for (int rx1 = 0; rx1 < rxers.size(); ++rx1) {
    for (int rx2 = rx1+1; rx2 < rxers.size(); ++rx2) {
      //TODO FIXME Trying out a different method here.
      if (true) {
        double diff = s.rssis[rx2] - s.rssis[rx1];
        for (int c = 0; c < columns; ++c) {
          for (int r = 0; r < rows; ++r) {
            for (double alph_mod = 2; alph_mod <= 4; alph_mod += 2) {
              for (double alph_mod2 = 2; alph_mod2 <= 4; alph_mod2 += 2) {
                pair<double, double> p{(double)c, (double)r};
                double l1 = euclDistance( rxers[rx1], p);
                double l2 = euclDistance( rxers[rx2], p);
                double comp = 10 * (alph_mod * log10(l1) - alph_mod2 * log10(l2));
                if (comp < diff) {
                  ++hits[c][r];
                }
              }
            }
          }
        }
      }
      //Check for min distance and difference in RSS and for real RSS values
      else if ( euclDistance(rxers[rx1], rxers[rx2]) >= min_dist and
           not (isnan(s.rssis[rx1]) or isnan(s.rssis[rx2]))) {

        //Check for 3 cases:
        // Close: transmitter is close to one of the receivers
        // Equal near: transmitter is in between the receivers
        // Equal far: transmitter is far from the receivers.
        //Close case
        if ( abs(s.rssis[rx1] - s.rssis[rx2]) >= min_diff) {
          //Identify closer and farther receivers
          int closer = rx2;
          int farther = rx1;
          if (s.rssis[rx1] > s.rssis[rx2]) {
            closer = rx1;
            farther = rx2;
          }

          //Count the tiles that would get this much stronger a signal
          //around the closer receiver.
          for (int c = 0; c < columns; ++c) {
            for (int r = 0; r < rows; ++r) {
              double x = c * tile_w + tile_w/2.0;
              double y = r * tile_h + tile_h/2.0;
              //If this tile is closer to the stronger receiver then increment
              //its hit count, otherwise decrement it.
              double alpha = 4.0; //Alpha should not normally be worse than 4.
              double diff = abs(s.rssis[rx1] - s.rssis[rx2]);
              double ratio = pow(10, -1.0 * diff / (10.0 * alpha));
              if(euclDistance(make_pair(x, y), rxers[closer])/euclDistance(make_pair(x, y), rxers[farther]) <= ratio) {
                ++hits[c][r];
              }
              /*
              else {
                --hits[c][r];
              }
              */
            }
          }
        }
        //Otherwise the RSS values are very close. Let's see if this is
        //near or far by checking if the source could have been from
        //the center point between the two receivers
        else {
          double center_x = (rxers[rx1].first + rxers[rx2].first) / 2.0;
          double center_y = (rxers[rx1].second + rxers[rx2].second) / 2.0;
          //TODO FIXME Just using a threshold here for testing, but this is not the way.
          if (s.rssis[rx1] > -60.0 and s.rssis[rx2] > -60.0) {
            double x_dist = abs(rxers[rx1].first - rxers[rx2].first);
            double y_dist = abs(rxers[rx1].second - rxers[rx2].second);
            double x_dev = x_dist * 0.6;
            double y_dev = y_dist * 0.6;
            //Increment spots in a square area around the two receivers where
            //the observed signal might be the same.
            //TODO FIXME It is so inefficient to all of the tiles when we only
            //care about a small number of them
            bool printed = false;
            for (int c = 0; c < columns; ++c) {
              for (int r = 0; r < rows; ++r) {
                double x = c * tile_w + tile_w/2.0;
                double y = r * tile_h + tile_h/2.0;
                double alpha = 2.0; //At best alpha might be 2.
                double diff = s.rssis[rx2] - s.rssis[rx1];
                double ratio = euclDistance(make_pair(x, y), rxers[rx1])/euclDistance(make_pair(x, y), rxers[rx2]);
                if(10.0 * alpha * log10(ratio) < diff) {
                  if (not printed) cerr<<"Between rxers at "<<rxers[rx1].first<<","<<rxers[rx1].second<<
                    " and "<<rxers[rx2].first<<","<<rxers[rx2].second<<'\n';
                  printed = true;
                  ++hits[c][r];
                }
              }
            }
          }
        }
      }
    }
  }
  int max = accumulate(hits.begin(), hits.end(), 0, [&](int cur_max, vector<int>& col) {
      int col_max = *max_element(col.begin(), col.end());
      if (col_max > cur_max) return col_max; else return cur_max;});
  int total = accumulate(hits.begin(), hits.end(), 0, [&](int cur_sum, vector<int>& col) {
      return cur_sum + accumulate(col.begin(), col.end(), 0); });
  
  //Make all of the tiles.
  vector<Tile> result;
  cerr<<"Data for sample at "<<s.x<<", "<<s.y<<'\n';
  for (int col = 0; col < columns; ++col) {
    for (int row = 0; row < rows; ++row) {
      cerr<<tile_w*col<<'\t'<<tile_h*row<<'\t'<<hits[col][row]<<'\n';
      result.push_back(Tile{col, row});
    }
  }
  //Sort the tiles by their use counts.
  sort(result.begin(), result.end(), [&](const Tile& a, const Tile& b) {
      return hits[a.column][a.row] > hits[b.column][b.row];});
  auto I = result.begin();
  int last_count = INT_MIN;
  int level_count = 0;
  do {
    if (hits[I->column][I->row] != last_count) {
      ++level_count;
      last_count = hits[I->column][I->row];
    }
    ++I;
  } while (I != result.end() and (level_count < num_levels or hits[I->column][I->row] == last_count));
  //Erase the result not in the specified percentile.
  result.erase(I, result.end());
  return result;
}

