#include <algorithm>
#include <string>

#include "world_server_protocol.hpp"
#include "netbuffer.hpp"

using namespace world_server;

std::vector<unsigned char> world_server::makeHandshakeMsg() {
  std::vector<unsigned char> buff(4);
  std::string protocol_string = "GRAIL world protocol";
  buff.insert(buff.end(), protocol_string.begin(), protocol_string.end());
  //Version number and extension are both currently zero
  buff.push_back(0);
  buff.push_back(0);
  //Insert the length of the protocol string into the buffer
  pushBackVal<uint32_t>(protocol_string.length(), buff, 0);
  return buff;
}

int encodeWorldData(const WorldData& wd, Buffer& buff) {
  int bytes = 0;
  bytes += pushBackSizedUTF16(buff, wd.object_uri);
  bytes += pushBackVal((uint32_t)wd.fields.size(), buff);
  //for (FieldData fd : wd.fields)
  for (auto fd = wd.fields.begin(); fd != wd.fields.end(); ++fd) {
    bytes += pushBackSizedUTF16(buff, fd->description);
    bytes += pushBackVal(fd->creation_date, buff);
    bytes += pushBackVal(fd->expiration_date, buff);
    bytes += pushBackSizedUTF16(buff, fd->data_type);
    //Push back the fd->data.size() value
    bytes += pushBackVal((uint32_t)fd->data.size(), buff);
    //for (uint8_t b : fd.data)
    for (auto b = fd->data.begin(); b != fd->data.end(); ++b) {
      bytes += pushBackVal(*b, buff);
    }
  }
  return bytes;
}

WorldData decodeWorldData(BuffReader& reader) {
  WorldData wd;
  wd.object_uri = reader.readSizedUTF16();
  int num_fields = reader.readPrimitive<uint32_t>();
  for (int i = 0; i < num_fields; ++i) {
    FieldData fd;
    fd.description     = reader.readSizedUTF16();
    fd.creation_date   = reader.readPrimitive<uint64_t>();
    fd.expiration_date = reader.readPrimitive<uint64_t>();
    fd.data_type       = reader.readSizedUTF16();
    int data_len = reader.readPrimitive<uint32_t>();
    fd.data = Buffer(data_len);
    reader.readPrimitiveContainer(fd.data);
    wd.fields.push_back(fd);
  }

  return wd;
}


Buffer world_server::makeObjectQueryMsg(const URI& object_uri) {
  Buffer buff;

  //Keep track of the total length of the message (in bytes)
  uint32_t total_length = 0;
  //Push back space in the buffer for the total length.
  pushBackVal(total_length, buff);

  //Store the message type and data
  total_length += pushBackVal(MessageID::object_query, buff);
  total_length += pushBackSizedUTF16(buff, object_uri);

  //Store the size of the message.
  pushBackVal(total_length, buff, 0);

  return buff;
}

URI world_server::decodeObjectQueryMsg(Buffer& buff) {
  BuffReader reader(buff);

  uint32_t total_length = reader.readPrimitive<uint32_t>();
  //Verify that the message can be reconstructed.
  if (total_length+sizeof(total_length) < buff.size()) { return u"";}
  MessageID msg_type = MessageID(reader.readPrimitive<uint8_t>());

  std::u16string uri;

  //Verify the message type before decoding.
  if ( MessageID::object_query == msg_type) {
    uri = reader.readSizedUTF16();
  }
  return uri;
}

Buffer world_server::makeObjectDataQueryMsg(const URI& object_uri) {
  Buffer buff;

  //Keep track of the total length of the message (in bytes)
  uint32_t total_length = 0;
  //Push back space in the buffer for the total length.
  pushBackVal(total_length, buff);

  //Store the message type and data
  total_length += pushBackVal(MessageID::object_data_query, buff);
  total_length += pushBackSizedUTF16(buff, object_uri);

  //Store the size of the message.
  pushBackVal(total_length, buff, 0);

  return buff;
}

URI world_server::decodeObjectDataQueryMsg(Buffer& buff) {
  BuffReader reader(buff);

  uint32_t total_length = reader.readPrimitive<uint32_t>();
  //Verify that the message can be reconstructed.
  if (total_length+sizeof(total_length) < buff.size()) { return u"";}
  MessageID msg_type = MessageID(reader.readPrimitive<uint8_t>());

  std::u16string uri;

  //Verify the message type before decoding.
  if ( MessageID::object_data_query == msg_type) {
    uri = reader.readSizedUTF16();
  }
  return uri;
}

Buffer world_server::makeQueryResponse(const WorldData& world_data) {
  Buffer buff;

  //Keep track of the total length of the message (in bytes)
  uint32_t total_length = 0;
  //Push back space in the buffer for the total length.
  pushBackVal(total_length, buff);

  //Store the message type and data
  total_length += pushBackVal(MessageID::query_response, buff);
  total_length += encodeWorldData(world_data, buff);
  //Store the size of the message.
  pushBackVal(total_length, buff, 0);

  return buff;
}

WorldData world_server::decodeQueryResponse(Buffer& buff) {
  BuffReader reader(buff);

  WorldData wd;

  uint32_t total_length = reader.readPrimitive<uint32_t>();
  //Verify that the message can be reconstructed.
  if (total_length+sizeof(total_length) < buff.size()) { return wd;}
  MessageID msg_type = MessageID(reader.readPrimitive<uint8_t>());

  //Verify the message type before decoding.
  if ( MessageID::query_response == msg_type) {
    //Decode the world data
    wd = decodeWorldData(reader);
  }
  return wd;
}

Buffer world_server::makeURISearchMsg(const URI& search_uri) {
  Buffer buff;

  //Keep track of the total length of the message (in bytes)
  uint32_t total_length = 0;
  //Push back space in the buffer for the total length.
  pushBackVal(total_length, buff);

  //Store the message type and data
  total_length += pushBackVal(MessageID::uri_search, buff);
  total_length += pushBackSizedUTF16(buff, search_uri);

  //Store the size of the message.
  pushBackVal(total_length, buff, 0);

  return buff;
}

URI world_server::decodeURISearchMsg(Buffer& buff) {
  BuffReader reader(buff);

  uint32_t total_length = reader.readPrimitive<uint32_t>();
  //Verify that the message can be reconstructed.
  if (total_length+sizeof(total_length) < buff.size()) { return u"";}
  MessageID msg_type = MessageID(reader.readPrimitive<uint8_t>());

  std::u16string uri;

  //Verify the message type before decoding.
  if ( MessageID::uri_search == msg_type) {
    uri = reader.readSizedUTF16();
  }
  return uri;
}

Buffer world_server::makeURISearchResponse(const std::vector<URI>& results) {
  Buffer buff;

  //Keep track of the total length of the message (in bytes)
  uint32_t total_length = 0;
  //Push back space in the buffer for the total length.
  pushBackVal(total_length, buff);

  //Store the message type and data
  total_length += pushBackVal(MessageID::uri_search_response, buff);
  //Store the number of URIs
  total_length += pushBackVal<uint32_t>(results.size(), buff);
  std::for_each(results.begin(), results.end(), [&](const URI& uri) {
      total_length += pushBackSizedUTF16(buff, uri);} );

  //Store the size of the message.
  pushBackVal(total_length, buff, 0);

  return buff;
}

std::vector<URI> world_server::decodeURISearchResponse(Buffer& buff) {
  BuffReader reader(buff);

  std::vector<URI> uris;

  uint32_t total_length = reader.readPrimitive<uint32_t>();
  //Verify that the message can be reconstructed.
  if (total_length+sizeof(total_length) < buff.size()) { return uris;}
  MessageID msg_type = MessageID(reader.readPrimitive<uint8_t>());

  //Verify the message type before decoding.
  if ( MessageID::uri_search_response == msg_type) {
    //Read in the number of URIs and then read in all of the URIs
    for (uint32_t number = reader.readPrimitive<uint32_t>(); number > 0; --number) {
      uris.push_back(reader.readSizedUTF16());
    }
  }
  return uris;
}

Buffer world_server::makePushDataMsg(const WorldData& world_data) {
  Buffer buff;

  //Keep track of the total length of the message (in bytes)
  uint32_t total_length = 0;
  //Push back space in the buffer for the total length.
  pushBackVal(total_length, buff);

  //Store the message type and data
  total_length += pushBackVal(MessageID::push_data, buff);
  total_length += encodeWorldData(world_data, buff);
  //Store the size of the message.
  pushBackVal(total_length, buff, 0);

  return buff;
}

WorldData world_server::decodePushDataMsg(Buffer& buff) {
  BuffReader reader(buff);

  WorldData wd;

  uint32_t total_length = reader.readPrimitive<uint32_t>();
  //Verify that the message can be reconstructed.
  if (total_length+sizeof(total_length) < buff.size()) { return wd;}
  MessageID msg_type = MessageID(reader.readPrimitive<uint8_t>());

  //Verify the message type before decoding.
  if ( MessageID::push_data == msg_type) {
    //Decode the world data
    wd = decodeWorldData(reader);
  }
  return wd;
}

