#include "passive_localization_utilities.h"

using namespace std;

// return the mean of the vector
double getMean(vector<double>& vals){

	double sum = 0;

	for(vector<double>::iterator I = vals.begin(); I != vals.end(); ++I){
		sum += *I;
	}

	return sum/vals.size();
}

// return the median of the vector
double getMedian(vector<double>& vals){
	
	sort(vals.begin(), vals.end());
	
	double median = 0;

	if(vals.size() % 2 == 1) {
		median = vals[vals.size() / 2];
	}
	else {
		median = (vals[vals.size() / 2 - 1] + vals[vals.size() / 2]) / 2;
	}
	return median;
}

// return the variance of the vector 
double getVariance(vector<double>& vals, double mean){

	double ssquares = 0;

	for(vector<double>::iterator I = vals.begin(); I != vals.end(); ++I){
		ssquares += (*I - mean) * (*I - mean);
	}

	return ssquares/vals.size();
}

// return the covariance of two vectors
double getCovariance(vector<double>& p1, vector<double>& p2){

	double p1_mean = getMean(p1);
	double p2_mean = getMean(p2);

	double sum = 0;
	
	for(int i = 0; i < p1.size(); i++){
		sum += (p1[i] - p1_mean) * (p2[i] - p2_mean);
	}

	return sum/p1.size();
}

// return the correlation of two vectors
double getCorrelation(vector<double>& p1, vector<double>& p2){
	double sum = 0;
	for(int i = 0; i < p1.size(); i++){
		sum += p1[i] * p2[i];
	}

	return sum/p1.size();
}

// return the coefficient of two vectors
double getCoefficient(vector<double>& p1, vector<double>& p2){

	double cov = getCovariance(p1, p2);

	double p1_stdev = sqrt(getVariance(p1, getMean(p1)));
	double p2_stdev = sqrt(getVariance(p2, getMean(p2)));
	
	double coef = cov / (p1_stdev * p2_stdev);

	if(cov == 0 || p1_stdev * p2_stdev == 0) {
		coef = 0;
	}

	return coef;
}

// return the euclidean distance of two vectors
double getDistance(vector<double>& p1, vector<double>& p2){

	double distance = 0;

	for(int i = 0; i < p1.size(); i++){
		distance += (p1[i] - p2[i]) * (p1[i] - p2[i]);
	}

	return distance;
}

// return the index by descending order of the original vector 
vector<int> getRank(vector<double>& vals){
	vector<int> rank;

	for(int i = 0; i < vals.size(); i++){
		rank.push_back(i+1);
	}

	double temp_val;
	int temp_rank;

	for(int i = 0; i < vals.size() - 1; i++){
		for(int j = 0; j < vals.size() - 1; j++){
	  	if(vals[j] < vals[j+1]) {
				temp_val = vals[j+1];
				vals[j+1] = vals[j];
				vals[j] = temp_val;

				temp_rank = rank[j+1];
				rank[j+1] = rank[j];
				rank[j] = temp_rank;
	    }
		}
	}
	return rank;
}

// extract the column or row vector from the matrix
vector<double> getVector(vector<vector<double> > data, int id, bool col){
	vector<double> ret_vector;
	if(col == true){
		for(int i = 0; i < data.size(); i++){
			ret_vector.push_back(data[i][id]);
		}
	}

	else if(col == false){
		for(int i = 0; i < data[0].size(); i++){
			ret_vector.push_back(data[id][i]);
		}
	}

	else{
		cerr<<"Third parameter should be 0 (row) or 1 (col)"<<endl;
	}

	return ret_vector;
}

int getIndex(vector<uint128_t> vals, int target) {
	for(int i = 0; i < vals.size(); i++){
		if(vals[i] == target){
			return i;
		}
	}
	return -1; 
}

int getIndex(vector<uint128_t>& vals, uint128_t target) {
	for(int i = 0; i < vals.size(); i++){
		if(vals[i] == target){
			return i;
		}
	}
	return -1;
}

double getIndex(vector<double>& vals, double target) {
	for(int i = 0; i < vals.size(); i++){
		if(vals[i] == target){
			return i;
		}
	}
	return -1;
}

// return the new vector with the first n elements in origin 
vector<double> front_n(vector<double> data, int n){
	vector<double> temp(n, 0);
	if(n > data.size()){
		cerr<<"n is larger than the data size"<<endl;
		temp.resize(1);
	} else{
		for(int i = 0; i < n; i++){
			temp[i] = data[i];
		}
	}
	return temp;
}

// return the new vector with the last n elements in origin
vector<double> back_n(vector<double> data, int n){
	vector<double> temp(n, 0);
	if(n > data.size()){
		cerr<<"n is larger than the data size"<<endl;
		temp.resize(1);
	} else{
		for(int i = data.size() - 1; i > data.size() - n - 1; i--){
			temp[data.size()-i-1] = data[i];
		}
	}
	return temp;
}

vector<double> frontMean(vector< vector<double> > data, int win_size){
	int min_size = data[0].size();
	for(int i = 0; i < data.size(); i++){
		if(min_size < data[i].size()){
			min_size = data[i].size();
		}
	}
	
	vector<double> front_mean(data.size(), 0);

	if(min_size < win_size){
		cerr<<"Data size is not enough for the window size"<<endl;
		front_mean.resize(1);
	} else{
		for(int i = 0; i < data.size(); i++){
			vector<double> temp = front_n(data[i], win_size);
			front_mean[i] = getMean(temp);
		}
	}
	return front_mean;
}

vector< vector<double> > frontVector(vector< vector<double> > data, int win_size){
	int min_size = data[0].size();
	for(int i = 0; i < data.size(); i++){
		if(min_size < data[i].size()){
			min_size = data[i].size();
		}
	}
	
	vector< vector<double> > front_vector;

	if(min_size < win_size){
		cerr<<"Data size is not enough for the window size"<<endl;
		front_vector.resize(1);
	} else{
		for(int i = 0; i < data.size(); i++){
			vector<double> temp = front_n(data[i], win_size);
			front_vector.push_back(temp);
		}
	}
	return front_vector;
}

vector<double> backMean(vector< vector<double> > data, int win_size){
	int min_size = data[0].size();
	for(int i = 0; i < data.size(); i++){
		if(min_size < data[i].size()){
			min_size = data[i].size();
		}
	}
	vector<double> back_mean(data.size(), 0);

	if(min_size < win_size){
		cerr<<"Data size is not enough for the window size"<<endl;
		back_mean.resize(1);
	} else{
		for(int i = 0; i < data.size(); i++){
			vector<double> temp = back_n(data[i], win_size);
			back_mean[i] = getMean(temp);
		}
	}
	return back_mean;
}

vector< vector<double> > backVector(vector< vector<double> > data, int win_size){
	int min_size = data[0].size();
	for(int i = 0; i < data.size(); i++){
		if(min_size < data[i].size()){
			min_size = data[i].size();
		}
	}
	cout<<"Minimum size is "<<min_size<<endl;
	vector< vector<double> > back_vector;

	if(min_size < win_size){
		cerr<<"Data size is not enough for the window size"<<endl;
		back_vector.resize(1);
	} 
	else{
		for(int i = 0; i < data.size(); i++){
			vector<double> temp = back_n(data[i], win_size);
			back_vector.push_back(temp);
		}
	}

	return back_vector;
}

vector< vector<double> > map2vector(map<uint128_t, vector<double> > data){
	vector< vector<double> > ret;
	for(auto link_id = data.begin(); link_id != data.end(); link_id++){
		ret.push_back(link_id->second);
	}
	return ret;
}

vector<double> leftMul(vector<double> vec, vector< vector<double> > mat){
	vector<double> result;
	for(int i = 0; i < mat[0].size(); i++){
		double temp_sum = 0;
		for(int j = 0; j < vec.size(); j++){
			temp_sum = temp_sum + vec[j] * mat[j][i];
		}
		result.push_back(temp_sum);
	}
	return result;
}

vector<double> rightMul(vector< vector<double> > mat, vector<double> vec){
	vector<double> result;
	for(int i = 0; i < mat.size(); i++){
		double temp_sum = 0;
		for(int j = 0; j < vec.size(); j++){
			temp_sum = temp_sum + mat[i][j] * vec[j];
		}
		result.push_back(temp_sum);
	}
	return result;
}

double innerProduct(vector<double> v1, vector<double> v2){
	double temp_sum = 0;
	for(int i = 0; i < v1.size(); i++){
		temp_sum += v1[i] * v2[i];
	}
	return temp_sum;
}

vector< vector<double> > outerProduct(vector<double> v1, vector<double> v2){
	vector< vector<double> > retmat(v1.size(), vector<double>(v2.size(), 0));
	for(int i = 0; i < v1.size(); i++){
		for(int j = 0; j < v2.size(); j++){
			retmat[i][j] = v1[i] * v2[j];
		}
	}
	return retmat;
}

vector<double> dotMul(vector<double> v1, vector<double> v2){
	vector<double> retvec;
	for(int i = 0; i < v1.size(); i++){
		retvec.push_back(v1[i]*v2[i]);
	}
	return retvec;
}

uint128_t encodeLink(uint128_t tx, uint128_t rx, int rx_num) {
	uint128_t link = tx.lower * rx_num + rx.lower;
	return link;
}

vector<uint128_t> decodeLink(uint128_t link, int rx_num) {
	vector<uint128_t> device;
	device.push_back(floor((link.lower - 1) / rx_num) + 1);
	device.push_back((link.lower - 1) % rx_num + 1);
	return device;
}

void normalize(vector<double> &data) {
	double min = data[0];
	for(int i = 1; i < data.size(); i++){
		if(data[i] < min) {
			min = data[i];
		}
	}
	for(int i = 0; i < data.size(); i++) {
		data[i] -= min;
	}
	double sum = 0;
	for(int i = 0; i < data.size(); i++) {
		sum += data[i];
	}
	for(int i = 0; i < data.size(); i++) {
		data[i] /= sum;
	}
	return;
}	

void neighbourProfile(vector< vector<double> > &dynamic_neighbour, vector< vector<double> > static_neighbour) {
	vector< vector<double> > temp_dynamic_neighbour = dynamic_neighbour;
	for(int i = 0; i < static_neighbour.size(); i++) {
		for(int j = 0; j < temp_dynamic_neighbour[i].size(); j++) {
			for(int k = 0; k < static_neighbour[temp_dynamic_neighbour[i][j]-1].size(); k++) {
				if(getIndex(dynamic_neighbour[i], static_neighbour[temp_dynamic_neighbour[i][j]-1][k]) == -1) {
					dynamic_neighbour[i].push_back(static_neighbour[temp_dynamic_neighbour[i][j]-1][k]);
				}
			}
		}
	}
	return;
}

vector< vector<double> > setAdjCell(vector< vector<double> > neighbour) {
	vector< vector<double> > cell_adj;
	for(int i = 0; i < neighbour.size(); i++) {
		cell_adj.push_back(vector<double>(neighbour.size(), 0));
	}
	for(int i = 0; i < neighbour.size(); i++) {
		for(int j = 0; j < neighbour[i].size(); j++) {
			cell_adj[i][neighbour[i][j]-1] = 1;
		}
		cell_adj[i][i] = 1;
	}
	return cell_adj;
}

vector< vector<double> > intFreqTab(vector<int> data){
	sort(data.begin(), data.end());
	vector<double> uni;
	vector<	vector<double> > freq;
	double last = -1;
	for(int i = 0; i < data.size(); i++) {
		if(data[i] != last) {
			uni.push_back(data[i]);
			last = data[i];
			freq.push_back(vector<double>());
		}
	}
	for(int i = 0; i < uni.size(); i++) {
		freq[i].push_back(uni[i]);
		freq[i].push_back(0);
		for(int j = 0; j < data.size(); j++) {
			if(data[j] == freq[i][0]) {
				freq[i][1]++;
			}
		}
		freq[i][1] = freq[i][1] / data.size();
	}
	return freq;
}

vector< vector<double> > doubleFreqTab(vector<double> data){
	sort(data.begin(), data.end());
	vector<double> uni;
	vector<	vector<double> > freq;
	double last = -1;
	for(int i = 0; i < data.size(); i++) {
		if(data[i] != last) {
			uni.push_back(data[i]);
			last = data[i];
			freq.push_back(vector<double>());
		}
	}
	for(int i = 0; i < uni.size(); i++) {
		freq[i].push_back(uni[i]);
		freq[i].push_back(0);
		for(int j = 0; j < data.size(); j++) {
			if(data[j] == freq[i][0]) {
				freq[i][1]++;
			}
		}
		freq[i][1] = freq[i][1] / data.size();
	}
	return freq;
}

double cellEst(vector<int> ty, vector<int> py){
	double ret = 0;
	for(int i = 0; i < ty.size(); i++) {
		if (py[i] == ty[i]) {
			ret++;
		}
	}
	ret = ret / ty.size();
	return ret;
}

double errLoc(vector<int> ty, vector<int> py, vector< vector<double> > attributes, double in_err) {
	vector<double> dists;
	for(int i = 0; i < ty.size(); i++) {
		double dist;
		if (ty[i] == py[i]) {
			dist = in_err;
		} else {
			dist = sqrt(pow((attributes[(int)py[i]-1][0] - attributes[(int)ty[i]-1][0]), 2) + pow((attributes[py[i]-1][1] - attributes[ty[i]-1][1]), 2));
		}
		dists.push_back(dist);
	}
	return getMean(dists);
}

vector< vector<double> > distCDF(vector<int> ty, vector<int> py, vector< vector<double> > attributes, double in_err) {
	vector<double> dists;
	for(int i = 0; i < ty.size(); i++) {
		double dist;
		if (ty[i] == py[i]) {
			dist = in_err;
		} else {
			dist = sqrt(pow((attributes[py[i]-1][0] - attributes[ty[i]-1][0]), 2) + pow((attributes[py[i]-1][1] - attributes[ty[i]-1][1]), 2));
		}
		dists.push_back(dist);
	}
	vector< vector<double> > dists_freq = doubleFreqTab(dists);
	vector< vector<double> > dists_cdf = dists_freq;
	for(int i = 0; i < dists_freq.size(); i++){
		for(int j = 0; j < i; j++) {
			dists_cdf[i][1] = dists_cdf[i][1] + dists_freq[j][1];
		}
	}
	return dists_cdf;
}

void disp1Dvec(vector<double> data) {
	for(int i = 0; i < data.size(); i++) {
		cout<<data[i]<<"\t";
	}
	cout<<endl;
	return;
}

void disp2Dvec(vector <vector<double> > data) {
	for(int i = 0; i < data.size(); i++) {
		for(int j = 0; j < data[i].size(); j++) {
			cout<<data[i][j]<<"\t";
		}
		cout<<endl;
	}
	return;
}
