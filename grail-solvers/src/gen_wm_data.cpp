/*******************************************************************************
 * This program converts data in files into data for the world model.
 * Data should be in four columns separated with tabs. The columns are
 * as follows:
 * 1) URI Name
 * 2) Attribute Name
 * 3) Creation Time (milliseconds since the epoch 1970).
 * 4) Binary data represented by a hexedecimal string. This is not preceded by
 *    the "0x" characters.
 *
 * Example:
 * winlab.anchor.receiver.639	location.xoffset	1302048368	4061400000000000
 * The URI is winlab.anchor.pipsqueak.receiver.639
 * The attribute name is location.xoffset (which happens to be a double).
 * The creation time was 1302048368 and the raw binary bits of the x offset
 * are 4061400000000000.
 *
 * This program is not smart enough to check to make sure your binary data
 * makes sense.
 * 
 ******************************************************************************/

#include <algorithm>
#include <iostream>
#include <set>
#include <string>
#include <vector>
#include <stdexcept>

#include <netbuffer.hpp>
#include <world_model_protocol.hpp>
#include <grailV3_solver_client.hpp>

std::u16string toU16(const std::string& str) {
  return std::u16string(str.begin(), str.end());
}

int main(int ac, char** av) {
  if (ac != 5) {
    std::cerr<<"Improper arguments. Correct usage is\n";
    std::cerr<<"\t"<<av[0]<<" <world model ip> <solver port> <origin name> <data file>\n";
    return 0;
  }
  //World model IP and port
  std::string wm_ip(av[1]);
  int wm_port = std::stoi(std::string((av[2])));
  std::string origin(av[3]);

  //Make solutions from each line of the file.
  std::vector<SolverWorldModel::Solution> solns;
  std::set<u16string> types;

  std::cerr<<"Reading file...\n";
  //Go through the file to get all of the solutions and their types.
  std::ifstream sol_file(av[4]);
  if (not sol_file.is_open()) {
    std::cerr<<"Error opening solution file \""<<av[4]<<"\"\n";
    return 1;
  }
  std::string line;
  //Read in each type and its corresponding name
  while (std::getline(sol_file, line)) {
    std::vector<std::string> parts;
    //Split the line at the tabs
    std::string::size_type begin = 0;
    std::string::size_type end = 0;
    while (end != std::string::npos) {
      end = line.find('\t', begin);
      parts.push_back(line.substr(begin, end - begin));
      begin = end+1;
    }
    //Only process if there were four fields to parse
    //and there is information for the URI, attribute name, and creation time
    if (parts.size() != 4) {
      std::cerr<<"Got bad line: "<<line<<'\n';
    }
    else if (parts[0].size() != 0 and
        parts[1].size() != 0 and
        parts[2].size() != 0) {
      try {
        std::u16string uri = toU16(std::string(parts[0]));
        std::u16string attr_name = toU16(std::string(parts[1]));
        uint64_t creation = std::stoll(parts[2]);
        std::vector<uint8_t> buffer;
        //Base 16 conversion, 1 byte at a time
        for (int idx = 0; idx < parts[3].size(); idx +=2) {
          std::string byte(parts[3].begin()+idx, parts[3].begin()+idx+2);
          buffer.push_back(std::stoul(byte, NULL, 16));
        }
        SolverWorldModel::Solution soln{attr_name, creation, uri, buffer};
        types.insert(attr_name);
        solns.push_back(soln);
      }
      catch (std::invalid_argument& err) {
        std::cerr<<"Invalid date in line: "<<line<<'\n';
      }
    }
  }
  sol_file.close();

  std::vector<std::pair<u16string, bool>> type_pairs;
  std::for_each(types.begin(), types.end(), [&](const std::u16string& s) {
      type_pairs.push_back(std::make_pair(s, false));});

  std::for_each(types.begin(), types.end(), [&](const std::u16string& s) {
      std::cerr<<"Type is "<<std::string(s.begin(), s.end())<<'\n';});

  //Connect to the solver world model and send the data.
  std::cerr<<"Sending data...\n";
  SolverWorldModel swm(wm_ip, wm_port, type_pairs, u16string(origin.begin(), origin.end()));
  if (not swm.connected()) {
    std::cerr<<"Could not connect to the world model as a solver - aborting.\n";
    return 0;
  }
  if (not solns.empty()) {
    //If this solution message is too large then it could pose problems
    //either for networking or for memory in the world model.
    //Split up the solutions if there are too many.
    if (solns.size() < 10000) {
      swm.sendData(solns, true);
    }
    else {
      auto begin = solns.begin();
      int chunk_num = 0;
      int total_chunks = ceil(solns.size() / 10000.0);
      while (begin != solns.end()) {
        ptrdiff_t to_end = std::distance(begin, solns.end());
        to_end = std::min(to_end, (ptrdiff_t)10000);
        std::vector<SolverWorldModel::Solution> chunk(begin, begin+to_end);
        swm.sendData(chunk, true);
        begin += to_end;
        std::cerr<<"Sending chunk "<<++chunk_num<<" of "<<total_chunks<<'\n';
        //Sleep for a small amount of time to avoid overwhelming the network
        usleep(100000);
      }
    }
    std::cerr<<"Data was sent.\n";
  }

  return 0;
}
