#include <algorithm>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <time.h>
#include <vector>
#include <cmath>

#include "aggregator_solver_protocol.hpp"
#include "solver_distributor_protocol.hpp"
#include "grailV3_solver_client.hpp"
#include "sample_data.hpp"
#include "netbuffer.hpp"

using namespace aggregator_solver;

using namespace solver_distributor;

//The distributor where mobility results should be sent.
Distributor grail_dist;

//Print out motion events.
//This function is not thread safe (but could be made so with a mutex)
void packetCallback(SampleData& sample) {
  //Only process valid samples.
  if (not sample.valid) {
    return;
  }

  //Get the target name in string format.
  std::u16string target;
  {
    std::ostringstream os;
    os << sample.tx_id.lower;
    std::string tag_name = os.str();
    target = std::u16string(tag_name.begin(), tag_name.end());
  }

  //Create a new solver data message with this mobility information
  SolverDataMsg result{u"Winlab", msecTime()};
  SolutionData d{1, target};
  pushBackVal(sample.rx_id.upper, d.data);
  pushBackVal(sample.rx_id.lower, d.data);
  pushBackVal(sample.rss, d.data);
  result.data.push_back(d);
  grail_dist.send(result);

}


int main(int arg_count, char** arg_vector) {
  if (5 > arg_count) {
    std::cerr<<"This program needs at least 4 arguments:\n";
    std::cerr<<"\tclient [<server ip> <server port>]+ <aggregator ip> <aggregator port>\n";
    return 0;
  }

  //Grab the ip and ports for the servers and aggregator
  std::vector<NetTarget> servers;
  for (int s_num = 1; s_num < arg_count - 2; s_num += 2) {
    std::string server_ip(arg_vector[s_num]);
    uint16_t server_port = atoi(arg_vector[s_num + 1]);
    servers.push_back(NetTarget{server_ip, server_port});
  }
  std::string aggr_ip(arg_vector[arg_count-2]);
  int aggr_port = atoi(arg_vector[arg_count-1]);

  Rule winlab_rule;
  winlab_rule.physical_layer  = 1;

  winlab_rule.update_interval = 4000;
  //Accept all transmitters
  Transmitter all_sensors;
  winlab_rule.txers.push_back(all_sensors);

  Subscription winlab_sub{winlab_rule};
  std::vector<Subscription> subs{winlab_sub};

  //Set up the types for the GRAIL distributor
  std::vector<SolutionType> types;
  //Mobility will just be indicated with a single byte
  //with value 0 for no mobility and 1 for mobility.
  SolutionType s_t{1, std::u16string(u"signal strength")};
  types.push_back(s_t);

  //Connect to the distributor first, then the aggregator.
  if (grail_dist.connect(aggr_ip, aggr_port, types)) {
    //Connect to the grail aggregator with our subscription lists.
    grailAggregatorConnect(servers, subs, packetCallback);
  } else {
    std::cerr<<"Error connecting to the GRAIL aggregator.\n";
  }

  return 0;

}

