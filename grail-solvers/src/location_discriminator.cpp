/*
 * Copyright (c) 2012 Bernhard Firner and Rutgers University
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or visit http://www.gnu.org/licenses/gpl-2.0.html
 */

#include <algorithm>
#include <array>
#include <iostream>
#include <map>
#include <string>
#include <time.h>
#include <vector>
#include <utility>
#include <limits>
#include <functional>
#include <cmath>
#include <mutex>
#include <set>
#include <stdexcept>
#include <thread>

#include "grail_types.hpp"
#include "world_model_protocol.hpp"
#include "grailV3_solver_client.hpp"
#include "netbuffer.hpp"

using namespace world_model;

using std::make_pair;
using std::map;
using std::pair;

std::u16string toU16(const std::string& str) {
  return std::u16string(str.begin(), str.end());
}

std::string toString(const std::u16string& str) {
  return std::string(str.begin(), str.end());
}

//Statistics of a location
struct Location {
  double min;
  double low_quarter;
  double median;
  double high_quarter;
  double max;
};

//A map from location names to the statistics of that location.
//Each location has a group of receivers and their "view" of that location.
map<URI, map<URI, Location>> location_info;

//Keep track of which transmitters correspond to which URIs.
map<URI, URI> tx_to_uri;

//The median RSS of an object at each receiver
map<URI, map<URI, double>> uri_medians;

//The current location of each object and its score there.
map<URI, pair<u16string, double>> cur_locations;

std::vector<Attribute>::iterator getAttr(std::vector<Attribute>& attrs, const std::u16string& attr) {
  return std::find_if(attrs.begin(), attrs.end(), [&](const Attribute& a) {return a.name == attr;});
}

std::pair<std::u16string, std::u16string> sensorsFromLink(const std::u16string& link_string) {
  size_t phy_end = link_string.find(u".");
  size_t tx_end = link_string.find(u".", phy_end+1);
  if (phy_end != std::u16string::npos and tx_end != std::u16string::npos) {
    //URIs for link medians are string representations of the
    //the transmitter in the format <phy>.<txid>.<rxid>
    URI raw_tx_uri = link_string.substr(0, tx_end);
    URI raw_rx_uri = link_string.substr(0, phy_end) + link_string.substr(tx_end);
    return make_pair(raw_tx_uri, raw_rx_uri);
  }
  else {
    return make_pair(u"", u"");
  }
}

//Get the average of the doubles in a vector
double getAverage(std::vector<double> v) {
  return std::accumulate(v.begin(), v.end(), 0.0, [&](double a, double b){return a + (b/v.size());});
}

bool compareSignal(const pair<URI, double>& a, const pair<URI, double>& b) {
  return a.second > b.second;
}

typedef std::function<bool(pair<URI, map<URI, Location>>&)> filter_fun;

URI selectLocation(map<URI, double>& medians) {
  //First sort the median values from strongest to weakest
  std::vector<pair<URI, double>> pq(medians.begin(), medians.end());
  std::sort(pq.begin(), pq.end(), compareSignal);
  //For future passes
  auto pq2 = pq;
  /*
  std::cerr<<"Strongest signal is "<<pq.front().second<<'\n';
  std::cerr<<"Signals are:\n";
  std::for_each(pq.begin(), pq.end(), [&](pair<URI, double>& p) {
      std::cerr<<"\t"<<toString(p.first)<<"\t"<<p.second<<'\n';
      });
      */
  std::vector<pair<URI, map<URI, Location>>> possible(location_info.begin(), location_info.end());
  std::vector<filter_fun> filters {
    [&](pair<URI, map<URI, Location>>& p) {
      if (p.second.find(pq.front().first) == p.second.end()) { return false;}
      Location& l = p.second[pq.front().first];
      //std::cerr<<"comparing max "<<l.max<<" to "<<pq.front().second<<'\n';
      return l.max < pq.front().second;},
    [&](pair<URI, map<URI, Location>>& p) {
      if (p.second.find(pq.front().first) == p.second.end()) { return false;}
      Location& l = p.second[pq.front().first];
      //std::cerr<<"comparing min "<<l.min<<" to "<<pq.front().second<<'\n';
      return l.min > pq.front().second;},
    [&](pair<URI, map<URI, Location>>& p) {
      if (p.second.find(pq.front().first) == p.second.end()) { return false;}
      Location& l = p.second[pq.front().first];
      //std::cerr<<"comparing hq "<<l.high_quarter<<" to "<<pq.front().second<<'\n';
      return l.high_quarter < pq.front().second;},
    [&](pair<URI, map<URI, Location>>& p) {
      if (p.second.find(pq.front().first) == p.second.end()) { return false;}
      Location& l = p.second[pq.front().first];
      //std::cerr<<"comparing lq "<<l.low_quarter<<" to "<<pq.front().second<<'\n';
      return l.low_quarter > pq.front().second;}};
  auto I = filters.begin();
  auto prev_possible = possible;
  while (possible.size() > 1 and I != filters.end()) {
    //Go through each median from strongest to weakest
    prev_possible = possible;
    while (not pq.empty() and possible.size() > 1) {
      possible.erase(std::remove_if(possible.begin(), possible.end(), *I), possible.end());
      pq.erase(pq.begin());
      if (possible.empty()) {
        possible = prev_possible;
      }
      else {
        prev_possible = possible;
      }
    }
    pq = pq2;
    ++I;
  }
  //std::cerr<<"ended up with "<<possible.size()<<" possible entries.\n";
  if (possible.size() == 1) {
    return possible[0].first;
  }
  else {
    //std::cerr<<"Could not resolve between "<<prev_possible.size()<<" locations.\n";
    return u"unknown";
  }

}

double scoreFingerprint(map<URI, double>& medians, map<URI, Location>& fingerprint) {
  std::vector<double> scores;
  for (auto I = medians.begin(); I != medians.end(); ++I) {
    //Assign a score from 0 to 1 for this median value to rate its similarity
    //to observed values at this location.
    if (fingerprint.find(I->first) != fingerprint.end()) {
      Location& loc = fingerprint[I->first];
      double score = 0.0;
      if (I->second < loc.min - (loc.median - loc.min)) {
        score = -1.0;
      }
      else if (I->second < loc.min) {
        score = -0.5;
      }
      else if (loc.min <= I->second and I->second < loc.low_quarter) {
        score = 0.5 * (I->second - loc.min) / (loc.low_quarter - loc.min);
      }
      else if (loc.low_quarter <= I->second and I->second < loc.median) {
        score = 0.5 + 0.5 * (I->second - loc.low_quarter) / (loc.median - loc.low_quarter);
      }
      else if (loc.median <= I->second and I->second < loc.high_quarter) {
        score = 0.5 + 0.5 * (I->second - loc.median) / (loc.high_quarter - loc.median);
      }
      else if (loc.high_quarter <= I->second and I->second < loc.max) {
        score = 0.5 * (I->second - loc.high_quarter) / (loc.max - loc.high_quarter);
      }
      else if (I->second > loc.max + (loc.max - loc.median)) {
        score = -1.0;
      }
      else if (I->second > loc.max) {
        score = -0.5;
      }
      scores.push_back(score);
    }
  }
  //Require at least half of the receivers used in fingerprinting to work
  if (scores.size() < fingerprint.size() / 2.0) {
    return 0.0;
  }
  return getAverage(scores);
}

double threshold = 0.10;

//Check if an object is at any of the locations and notify the
//world model if any location changes occur.
void updateRSS(SolverWorldModel& swm, ClientWorldModel::world_state& ws) {
  std::set<URI> updated_uris;
  //Check each URI for new sensors
  for (auto I = ws.begin(); I != ws.end(); ++I) {
    pair<std::u16string, std::u16string> tx_rx = sensorsFromLink(I->first);
    //Find the transmitter and receiver in this link
    if (tx_to_uri.find(tx_rx.first) != tx_to_uri.end()) {
      URI uri = tx_to_uri[tx_rx.first];
      //Make this URI as being updated
      updated_uris.insert(uri);
      auto median_i = getAttr(I->second, u"link median");
      double median = readPrimitive<double>(median_i->data);
      uri_medians[uri][tx_rx.second] = median;
    }
  }
  //Now check if any item location statuses have changed at each location
  std::vector<SolverWorldModel::Solution> solns;
  //TODO Remove repetitions from this
  for (auto I = updated_uris.begin(); I != updated_uris.end(); ++I) {
    //Check for a state change
    auto cur_loc_i = cur_locations.find(*I);
    URI new_location = selectLocation(uri_medians[*I]);
    if (cur_loc_i == cur_locations.end() or
        new_location != cur_loc_i->first) {
      SolverWorldModel::Solution s{u"location.uri", getGRAILTime(), *I, std::vector<uint8_t>()};
      pushBackUTF16(s.data, new_location);
      solns.push_back(s);
      cur_locations[*I] = make_pair(new_location, 1.0);
      std::cerr<<"Marking "<<toString(*I)<<" in location "<<toString(new_location)<<'\n';
    }
  }

  /*
  for (auto L = location_info.begin(); L != location_info.end(); ++L) {
    for (auto I = updated_uris.begin(); I != updated_uris.end(); ++I) {
      double location_score = scoreFingerprint(uri_medians[*I], L->second);
      //Check for a state change
      auto cur_loc_i = cur_locations.find(*I);
      //TODO support being in multiple locations at once?
      SolverWorldModel::Solution s{u"location.uri", getGRAILTime(), *I, std::vector<uint8_t>()};
      SolverWorldModel::Solution s2{u"score.double", getGRAILTime(), *I, std::vector<uint8_t>()};
      pushBackVal<double>(location_score, s2.data);
      //Greater than threshold and this is the first time locating this
      //URI or this score is better than the previous location.
      //std::cerr<<"URI "<<toString(*I)<<" has score "<<location_score<<" at location "<<toString(L->first)<<'\n';
      if (location_score >= threshold) {
       if (cur_loc_i == cur_locations.end() or
           (location_score > cur_loc_i->second.second and
            L->first != cur_loc_i->second.first)) {
         //Now in this location
         pushBackUTF16(s.data, L->first);
         cur_locations[*I] = make_pair(L->first, location_score);
         solns.push_back(s);
         solns.push_back(s2);
         std::cerr<<"Marking "<<toString(*I)<<" in location "<<toString(L->first)<<'\n';
       }
      }
      else if (location_score < threshold and
          (cur_loc_i != cur_locations.end() and L->first == cur_loc_i->second.first)) {
        //Now not in this location
        //TODO FIXME Expire the previous solution
        pushBackUTF16(s.data, u"unknown");
        cur_locations[*I] = make_pair(L->first, location_score);
        solns.push_back(s);
        std::cerr<<"Marking "<<toString(*I)<<" in location unknown\n";
        cur_locations[*I] = make_pair(u"unknown", 0);
      }
      //Update score if this is the current location
      if (cur_loc_i != cur_locations.end() and
          L->first == cur_loc_i->second.first) {
        cur_locations[*I] = make_pair(L->first, location_score);
      }
    }
  }
  */
  //Update solutions if any locations changed.
  if (not solns.empty()) {
    swm.sendData(solns, false);
  }
}

//Update sensor information.
void updateSensor(ClientWorldModel::world_state& ws, uint32_t ticket) {
  auto get_sensor = [&](Attribute& a) { return a.name.find(u"sensor") != std::u16string::npos;};
  //Check each URI for new sensors
  for (auto I = ws.begin(); I != ws.end(); ++I) {
    auto sensor_id = std::find_if(I->second.begin(), I->second.end(), get_sensor);
    if (sensor_id != I->second.end()) {
      //Read in transmitter ID and map it to this URI
      grail_types::transmitter txid = grail_types::readTransmitter(sensor_id->data);
      std::string tx_name = to_string(txid.phy) + "." + to_string(txid.id.lower);
      tx_to_uri[toU16(tx_name)] = I->first;
    }
  }
}

void updateLocationInfo(ClientWorldModel::world_state& ws, uint32_t ticket) {
  using namespace grail_types;
  for (auto I = ws.begin(); I != ws.end(); ++I) {
    std::cerr<<"Updating location info for "<<toString(I->first)<<'\n';
    auto rxers_i = getAttr(I->second, u"fingerprint.receivers.vector<sensor.receiver>");
    auto mins_i = getAttr(I->second, u"fingerprint.min signal.vector<RSS>");
    auto low_quarter_i = getAttr(I->second, u"fingerprint.low quarter.vector<RSS>");
    auto medians_i = getAttr(I->second, u"fingerprint.median.vector<RSS>");
    auto high_quarter_i = getAttr(I->second, u"fingerprint.high quarter.vector<RSS>");
    auto maxs_i = getAttr(I->second, u"fingerprint.max signal.vector<RSS>");
    std::vector<grail_types::transmitter> rxids =
      unpackGRAILVector<grail_types::transmitter>(readTransmitterFromBuffer, rxers_i->data);
    auto double_unpacker = [&](BuffReader& br){return br.readPrimitive<double>();};
    std::vector<double> mins = unpackGRAILVector<double>( double_unpacker, mins_i->data);
    std::vector<double> low_quarter = unpackGRAILVector<double>( double_unpacker, low_quarter_i->data);
    std::vector<double> medians = unpackGRAILVector<double>( double_unpacker, medians_i->data);
    std::vector<double> high_quarter = unpackGRAILVector<double>( double_unpacker, high_quarter_i->data);
    std::vector<double> maxs = unpackGRAILVector<double>( double_unpacker, maxs_i->data);
    //For each receiver put its attributes into the location info structure.
    map<URI, Location>& locations = location_info[I->first];
    std::cerr<<"Updating with "<<rxids.size()<<" entries.\n";
    try {
      for (size_t i = 0; i < rxids.size(); ++i) {
        grail_types::transmitter rxid = rxids.at(i);
        std::string rx_name = std::to_string(rxid.phy) + "." + std::to_string(rxid.id.lower);
        Location& loc = locations[std::u16string(rx_name.begin(), rx_name.end())];
        loc.min = mins.at(i);
        loc.low_quarter = low_quarter.at(i);
        loc.median = medians.at(i);
        loc.high_quarter = high_quarter.at(i);
        loc.max = maxs.at(i);
      }
      std::cerr<<"Location info updated for "<<toString(I->first)<<" with "<<rxids.size()<<" entries.\n";
    } catch (std::runtime_error err) {
      std::cerr<<"Badly sized data in fingerprint at uri "<<toString(I->first)<<'\n';
    }
  }
}

int main(int arg_count, char** arg_vector) {
  if (4 != arg_count) {
    std::cerr<<"This program needs 3 arguments:\n";
    std::cerr<<"\tclient <world model ip> <world model solver port> <world model solver port>\n";
    return 0;
  }
  std::string wm_ip(arg_vector[arg_count-3]);
  int solver_port = std::stoi(std::string((arg_vector[arg_count-2])));
  int client_port = std::stoi(std::string((arg_vector[arg_count-1])));

  //Set up the solver world model connection;
  std::string origin = "grail/discriminator solver\nversion 1.0";
  //Provide location.uri
  std::vector<std::pair<u16string, bool>> type_pairs{{u"location.uri", false}, {u"score.double", false}};
  SolverWorldModel swm(wm_ip, solver_port, type_pairs, u16string(origin.begin(), origin.end()));
  if (not swm.connected()) {
    std::cerr<<"Could not connect to the world model as a solver - aborting.\n";
    return 0;
  }

  ClientWorldModel cwm(wm_ip, client_port);
  if (not cwm.connected()) {
    std::cerr<<"Could not connect to the world model as a client - aborting.\n";
    return 0;
  }

  enum class Ticket : uint32_t {median, sensors, fingerprints};

  //Get attributes "link median" and find
  //any URIs with attached sensors.
  URI anything = u".*";
  std::vector<URI> link_attributes{u"link median"};
  std::vector<URI> sensor_attributes{u"sensor.*"};
  grail_time interval = 2000;
  cwm.setupSynchronousDataStream(anything, link_attributes, interval, 
      [&](ClientWorldModel::world_state& ws, uint32_t ticket){
      updateRSS(std::ref(swm), ws);}, (uint32_t)Ticket::median);
  cwm.setupSynchronousDataStream(anything, sensor_attributes, interval, updateSensor, (uint32_t)Ticket::sensors);

  //Get fingerprinting statistics for locations
  std::vector<URI> finger_stats{u"fingerprint.receivers.vector<sensor.receiver>",
    u"fingerprint.min signal.vector<RSS>",
    u"fingerprint.low quarter.vector<RSS>",
    u"fingerprint.median.vector<RSS>",
    u"fingerprint.high quarter.vector<RSS>",
    u"fingerprint.max signal.vector<RSS>"};
  cwm.setupSynchronousDataStream(anything, finger_stats, interval, updateLocationInfo, (uint32_t)Ticket::fingerprints);

  std::cerr<<"Starting loop...\n";
  while (1) {
    //Each of the synchronous requests handles its data in a callback
    //so this loop does not need to handle any data.
    ClientWorldModel::world_state ws;
    uint32_t ticket;
    std::tie(ws, ticket) = cwm.getSynchronousStreamUpdate();
  }
  return 0;
}

