/*
 * Copyright (c) 2012 Bernhard Firner and Rutgers University
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or visit http://www.gnu.org/licenses/gpl-2.0.html
 */

#include <world_model_protocol.hpp>
#include <client_world_connection.hpp>
#include <netbuffer.hpp>

#include <iostream>
#include <string>
#include <vector>

//Handle interrupt signals to exit cleanly.
#include <signal.h>

using namespace std;

//Convenience function to convert UTF16 strings used in GRAIL to printable UTF8
string toString(const u16string& ustr) {
  return string(ustr.begin(), ustr.end());
}

//Global variable for the signal handler.
bool killed = false;
//Signal handler.
void handler(int signal) {
  psignal( signal, "Received signal ");
  if (killed) {
    std::cerr<<"Aborting.\n";
    // This is the second time we've received the interrupt, so just exit.
    exit(-1);
  }
  std::cerr<<"Shutting down...\n";
  killed = true;
}

int main(int argc, char** argv) {
  if (argc != 3) {
    cerr<<"This program expects two arguments, the world model server's ip address and client port.\n";
    return 0;
  }

  std::string ip(argv[1]);
  uint16_t port = stoi(argv[2]);

  //Connect to the world model as a client
  ClientWorldConnection cwc(ip, port);

  //Asking for everything
  u16string any_object(u".*");
  std::vector<u16string> every_attribute{u".*"};

  //Example of using a simple response
  {
    //Getting a current snapshot and block until results are returned.
    world_model::WorldState ws = cwc.currentSnapshotRequest(any_object, every_attribute).get();
    //Shorter than this version:
    //Response wsf = cwc.currentSnapshotRequest(any_object, every_attribute);
    //world_model::WorldState ws = wsf.get();

    //Print out the world model's information
    for (auto object = ws.begin(); object != ws.end(); ++object) {
      cout<<"Found object "<<toString(object->first)<<'\n';
      cout<<"Attributes are:\n";
      for (auto attr = object->second.begin(); attr != object->second.end(); ++attr) {
        cout<<"\t"<<toString(attr->name)<<'\n';
      }
    }
  }

  /*
  //Now sign up for locations for anything named mug and get updates every 10 seconds
  u16string mug_objects(u".*\\.mug\\..*");
  std::vector<u16string> location_attributes{u"location\\..offset"};
  StepResponse sr = cwc.streamRequest(mug_objects, location_attributes, 10000);
  while (sr.hasNext() and not killed) {
    world_model::WorldState ws = sr.next();
    //Print out the world model's information
    for (auto object = ws.begin(); object != ws.end(); ++object) {
      cout<<"Found mug "<<toString(object->first)<<'\n';
      cout<<"Location updates are:\n";
      for (auto attr = object->second.begin(); attr != object->second.end(); ++attr) {
        cout<<"\t"<<toString(attr->name)<<" is "<<readPrimitive<double>(attr->data)<<'\n';
      }
    }
  }
  */

  return 0;
}

