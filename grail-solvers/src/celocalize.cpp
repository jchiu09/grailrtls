/*
celocalize.{cpp,hpp} - Functions for localization using cross entropy
Source version: May 6, 2011
Copyright (c) 2011 Bernhard Firner

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
  claim that you wrote the original software. If you use this software
  in a product, an acknowledgment in the product documentation would be
  appreciated but is not required.

  2. Altered source versions must be plainly marked as such, and must not be
  misrepresented as being the original software.

  3. This notice may not be removed or altered from any source
  distribution.
*/

#include "celocalize.hpp"
#include "cross_entropy.hpp"
#include <algorithm>
#include <localize_common.hpp>
#include <utility>
#include <vector>
#include <map>
#include <climits>
#include <ctime>
#include <cmath>
#include <bitset>
#include <random>

//TODO remove me
#include <iostream>

using std::vector;
using std::pair;
using std::make_pair;
using std::map;

struct AlphaEstimate {
  double mean;
  double variance;
};

//Get the average of the numbers in a vector
double getAverage(vector<double>& v) {
  return accumulate(v.begin(), v.end(), 0.0, [&](double a, double b){return a + (b/v.size());});
}

//Get the variance of the numbers in a vector without recomputing the average
double getVariance(vector<double>& v, double avg) {
  double ssquares = accumulate(v.begin(), v.end(), 0.0,
      [&](double a, double b){return a + pow(avg - b, 2.0);});
  return ssquares / (v.size() - 1);
}

//Get the variance of the numbers in a vector
double getVariance(vector<double>& v) {
  double avg = getAverage(v);
  return getVariance(v, avg);
}

//The expected distance of a transmitter with given signal strength and
//attenutation coefficient, alpha
double expectedDistance(double signal, double alpha, double gain = -3) {
  return pow(10, (signal - gain)/ (-10 * alpha));
}

//Estimate the alpha values for signal propagation
vector<AlphaEstimate> estimateAlphas(TrainingData& td, LandmarkData& rxers) {
  vector<AlphaEstimate> alphas;
  const double initial_loss = -5.0;
  for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
    //Go through each pair of RSS values to estimate the alpha value
    vector<double> singles;
    for (size_t txer = 0; txer < td.size(); ++txer) {
      double rss = td[txer].rssis[rxer];
      double dist = sqrt(pow(rxers[rxer].first - td[txer].x, 2) + pow(rxers[rxer].second - td[txer].y, 2));
      double alpha = (rss - initial_loss) / (-10 * log10(dist));
      if (not (isnan(alpha))) {
        singles.push_back(alpha);
      }
    }
    if (1 < singles.size()) {
      double avg = getAverage(singles);
      double variance = getVariance(singles, avg);
      //Refuse to accept extremely low variances
      if (variance < 0.1) { variance = 0.1; }
      alphas.push_back(AlphaEstimate{avg, variance});
      std::cerr<<"Alpha avg/variance for receiver "<<rxer<<" is "<<avg<<"/"<<variance<<'\n';
    }
    else {
      double qnan = std::numeric_limits<double>::quiet_NaN();
      alphas.push_back(AlphaEstimate{qnan, qnan});
    }
  }
  return alphas;
}

//Interpolate the expected value at the given angle
//(in radians from -pi to +pi) with the slice estimates in slice_vals.
//Used for both sliced gain and alpha estimates.
AlphaEstimate interpolateSlices(double angle, vector<AlphaEstimate> slice_vals) {
  //First find the size of the slices in radians
  double slice_size = 2*M_PI / slice_vals.size();
  //The first slice is centered at 0 degrees so to calculate which two slices
  //the given angle is between ignore the centering here:
  //change the range from 0 to 2PI so that rounding away from 0 is always correct.
  if (angle < 0) { angle = 2*M_PI + angle;}
  double tween_slice = angle/slice_size; //Somewhere between 0 and the number of slices
  int slice_one = trunc(tween_slice);
  int slice_two = slice_one + 1;
  if (slice_one >= slice_vals.size()) { slice_one -= slice_vals.size();}
  if (slice_two >= slice_vals.size()) { slice_two -= slice_vals.size();}
  double percent = remainder(angle, slice_size) / slice_size;
  //TODO Testing if biasing towards the closer value is better
  //This still gives a smooth transition from one slice to another
  //if (percent < 0.5) { percent *= percent; }
  //else if (percent > 0.5) { percent = sqrt(percent);}
  double first = 1.0 - percent;
  double second = percent;
  double mean = slice_vals[slice_one].mean*first + slice_vals[slice_two].mean*second;
  if (first > second) {
    first += second/2.0;
    second /= 2.0;
  }
  else {
    second += first/2.0;
    first /= 2.0;
  }
  double variance = slice_vals[slice_one].variance*first + slice_vals[slice_two].variance*second;
  return AlphaEstimate{mean, variance};
}

//Interpolate the expected value at the given angle
//(in radians from -pi to +pi) with the slice estimates in slice_vals.
//Used for both sliced gain and alpha estimates.
double interpolateSlices(double angle, vector<double> slice_vals) {
  //First find the size of the slices in radians
  double slice_size = 2*M_PI / slice_vals.size();
  //The first slice is centered at 0 degrees so to calculate which two slices
  //the given angle is between ignore the centering here:
  //change the range from 0 to 2PI so that rounding away from 0 is always correct.
  if (angle < 0) { angle = 2*M_PI + angle;}
  double tween_slice = angle/slice_size; //Somewhere between 0 and the number of slices
  int slice_one = trunc(tween_slice);
  int slice_two = slice_one + 1;
  if (slice_one >= slice_vals.size()) { slice_one -= slice_vals.size();}
  if (slice_two >= slice_vals.size()) { slice_two -= slice_vals.size();}
  double percent = remainder(angle, slice_size) / slice_size;
  //TODO Testing if biasing towards the closer value is better
  //This still gives a smooth transition from one slice to another
  if (percent < 0.5) { percent *= percent; }
  else if (percent > 0.5) { percent = sqrt(percent);}
  return slice_vals[slice_one]*(1.0 - percent) + slice_vals[slice_two]*(percent);
}

//Return the probability of seeing a signal at a certain distance from a receiver if
//the propagation attenuation cooefficient is the given alpha estimate
//The var_coeff modifies the variance. (used variance = var_coeff * ae.variance)
long double probSignalFromAlphas(double signal, double dist, AlphaEstimate& ae, double var_coeff = 1.0, double antenna_gain = -5.0) {
  //TODO We assume signal readings have an error of 0.5 here but that should
  //be another parameter.
  //double measure_error = 0.5;
  //The probabability of a signal of the given value is
  //cdf(signal+error)-cdf(signal-error) where error is the resolution
  //of the measurement.
  signal -= antenna_gain;
  long double mean = -10 * log10(dist) * ae.mean;
  long double variance = 10 * log10(dist) * ae.variance * var_coeff;
  //The range above and below the signal value that we might expect to see (assuming that the signal really came from here)
  //75% confidence
  //double bound_range = 2 * sqrt(variance) + measure_error;
  //Using 128 bit precision here
  //Assume a normal distribution
  long double result = 0.5 * ( ( 1 + erfl( (signal + 0.5 - mean)/variance)) - ( 1 + erfl( (signal - 0.5 - mean)/variance)));
  //double result = 0.5 * ( ( 1 + erf( (signal + bound_range - mean)/variance)) - ( 1 + erf( (signal - bound_range - mean)/variance)));
  //std::cerr<<"\tresult is "<<result<<" from mean/variance "<<mean<<"/"<<variance<<" for signal "<<signal<<'\n';
  if (isnan(result)) { return 0.0;}
  return result;
}

double probDensityFromAlpha(double rss, double dist, AlphaEstimate& ae) {
  double mean = -10 * log10(dist) * ae.mean;
  double variance = 10 * log10(dist) * ae.variance;
  return (1.0 / sqrt(2*M_PI*variance)) * exp(-1.0 * pow(rss - mean, 2.0) / (2 * variance));
}

//Find the mean and variance of alpha in four quadrants that maximizes the probability
//of observing the given signal values
vector<vector<AlphaEstimate>> estimateQuadAlphas(TrainingData& td, LandmarkData& rxers,
    vector<vector<double>> antenna_gain = vector<vector<double>>()) {
  //First get the an overall estimation of alpha values to use as default
  //values, for instance when a quadrant has no training points.
  vector<AlphaEstimate> default_alphas = estimateAlphas(td, rxers);
  //Build new alpha estimates for all of the receivers.
  vector<vector<AlphaEstimate>> alphas(rxers.size());
  //The default initial loss
  const double initial_gain = -5.0;

  //Estimate the alpha values for each quadrant
  for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
    std::cerr<<"Generating alphas for rxer "<<rxer<<'\n';
    //If the antenna gain vector has some entries we will use them instead of the default
    bool use_gain = false;
    if (antenna_gain.size() == rxers.size() and antenna_gain[rxer].size() != 0) {
      use_gain = true;
    }
    //A vector with four subvectors to hold the distance,rss pairs of transmitters
    //in each quadrant.
    vector<vector<pair<double, double>>> quad_samples(4, vector<pair<double, double>>());
    //Put two initial values into each quadrant
    for (size_t q = 0; q < 4; ++q) {
      AlphaEstimate def = default_alphas[rxer];
      std::cerr<<"Alpha estimate has mean and variance "<<def.mean<<", "<<def.variance<<'\n';
      quad_samples[q].push_back(make_pair(expectedDistance(-40, def.mean*0.95, 0), -40));
      std::cerr<<"Inserting artificial distance "<<quad_samples[q].back().first<<'\n';
      quad_samples[q].push_back(make_pair(expectedDistance(-40, def.mean*1.05, 0), -40));
      std::cerr<<"Inserting artificial distance "<<quad_samples[q].back().first<<'\n';
    }
    for (auto TX = td.begin(); TX != td.end(); ++TX) {
      //Distance along x and y axes to get to the transmitter from the receiver
      //(positive y is down, negative y is up, positive x is right, negative x is left
      double xdiff = TX->x - rxers[rxer].first;
      double ydiff = TX->y - rxers[rxer].second;
      double rss = TX->rssis[rxer];
      double dist = sqrt(pow(xdiff, 2) + pow(ydiff, 2));

      //Interpolate over the gain slices provided to find the gain in this direction
      //if available, otherwise use the default gain value
      double ant_gain = initial_gain;
      if (use_gain) {
        ant_gain = interpolateSlices(atan2(ydiff, xdiff) , antenna_gain[rxer]);
      }
      //Find the angle from the receiver to the transmitter
      double tx_angle = atan2( ydiff, xdiff);
      //Switch to the interval (-PI/8,7PI/8) rather than (-PI,PI)
      //This makes it each to check which quadrant we are in
      if (tx_angle < -M_PI/8.0) {
        tx_angle = 2*M_PI + tx_angle;
      }
      //if (not isnan(rss)) {
        for (size_t quad = 0; quad < 4; ++quad) {
          if ( tx_angle <= (2*quad + 1)*(M_PI/8.0)) {
            quad_samples[quad].push_back(make_pair(dist, rss - ant_gain));
            break;
          }
        }
      //}
    }
    std::cerr<<"There are "<<quad_samples.size()<<" quadrants.\n";
    //Now compute statistics for each quadrant
    for (auto quad = quad_samples.begin(); quad != quad_samples.end(); ++quad) {
      auto next = quad+1;
      while (quad->empty()) {
        std::cerr<<"Rotating for data...\n";
        if (next == quad_samples.end()) {
          next = quad_samples.begin();
        }
        if (quad == next) {
          std::cerr<<"No data to estimate alpha values with!\n";
        }
        *quad = *next;
        ++next;
      }
      if (quad->empty()) {
        //Default values
        std::cerr<<"Using default alpha values!\n";
        alphas[rxer].push_back(default_alphas[rxer]);
      }
      //Otherwise estimate the mean and variance of the alpha value
      //by seeing what values give the highest probability
      //Brute force this.
      else {
        double best_mean = 0.0;
        double best_variance = 0.0;
        long double best_prob = 0.0;
        for (double mean = 2.8; mean <= 4.5; mean += 0.1) {
          for (double variance = 0.1; variance <= 1.2; variance += 0.1) {
            AlphaEstimate ae{mean, variance};
            long double cum_prob = 1.0;
            for (auto dist_rss = quad->begin(); dist_rss != quad->end(); ++dist_rss) {
              long double thisprob = probSignalFromAlphas(dist_rss->second, dist_rss->first, ae, 1.0, 0.0);
              /*
              if (0.0 == thisprob) {
                std::cerr<<"Got zero from rss and distance "<<dist_rss->second<<", "<<dist_rss->first<<'\n';
              }
              */

              cum_prob *= probSignalFromAlphas(dist_rss->second, dist_rss->first, ae, 1.0, 0.0);
              //cum_prob *= probDensityFromAlpha(dist_rss->second, dist_rss->first, ae);
              //std::cerr<<"Old way would give "<<probSignalFromAlphas(dist_rss->second, dist_rss->first, ae, 1.0, 0.0)<<
              //  " and new way gives "<<probDensityFromAlpha(dist_rss->second, dist_rss->first, ae)<<'\n';
            }
            if (cum_prob > best_prob) {
              best_prob = cum_prob;
              best_variance = variance;
              best_mean = mean;
            }
          }
        }
        if (best_mean == 0.0) {
          std::cerr<<"Failed to get an alpha value (all 0 probabilities) after using "<<quad->size()<<" entries.\n";
        }
        alphas[rxer].push_back(AlphaEstimate{best_mean, sqrt(best_variance)});
      }
    }
  }
  std::cerr<<"Returning "<<alphas.size()<<" alpha estimates.\n";
  return alphas;
}

//Estimate the alpha values for signal propagation in radial slices
vector<vector<AlphaEstimate>> estimateRadialAlphas(TrainingData& td, LandmarkData& rxers,
    vector<vector<double>> antenna_gain = vector<vector<double>>()) {
  //First get the an overall estimation of alpha values to use as default values.
  vector<AlphaEstimate> default_alphas = estimateAlphas(td, rxers);
  //Use quad alphas for variance estimates in the slices.
  vector<vector<AlphaEstimate>> quad_alphas = estimateQuadAlphas(td, rxers, antenna_gain);
  vector<vector<AlphaEstimate>> alphas(rxers.size());
  //The default initial loss
  const double initial_loss = -5.0;
  for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
    //Default to 64 slices
    double num_slices = 64.0;
    double gain_slice_step = 0.0;
    bool use_gain = false;
    if (antenna_gain.size() == rxers.size() and antenna_gain[rxer].size() != 0) {
      gain_slice_step = 2 * M_PI / antenna_gain[rxer].size();
      use_gain = true;
    }
    double slice_step = 2 * M_PI / num_slices;
    std::cerr<<"Doing "<<num_slices<<" slices\n";
    //First order all of the fiduciary transmitters from 0 radians to 2PI radians
    for (double cur_slice = 0; cur_slice < num_slices; ++cur_slice) {
      double angle = slice_step * cur_slice;
      //Go over each transmitter and weigh that alpha value by its rotation from this slice
      //Remember how much scaling was done so that we can renormalize at the end
      double total_weight = 0.0;
      double alpha_sum = 0.0;
      //std::cerr<<"\nDoing angle "<<angle<<" for receiver "<<rxer<<'\n';
      for (size_t txer = 0; txer < td.size(); ++txer) {
        //Distance along x and y axes to get to the transmitter from the receiver
        //(positive y is down, negative y is up, positive x is right, negative x is left
        double xdiff = td[txer].x - rxers[rxer].first;
        double ydiff = td[txer].y - rxers[rxer].second;
        double rss = td[txer].rssis[rxer];
        double dist = sqrt(pow(xdiff, 2) + pow(ydiff, 2));
        double ant_loss = initial_loss;
        //Interpolate over the gain slices provided to find the gain in this direction
        if (use_gain) {
          ant_loss = interpolateSlices(slice_step*cur_slice, antenna_gain[rxer]);
        }
        double alpha = (rss - ant_loss) / (-10 * log10(dist));
        if (not (isnan(alpha))) {
          //Find the angle relative to the center of the current slice
          //and then scale the signal strength by the rotation from this angle
          double tx_angle = atan2( ydiff, xdiff);
          if (tx_angle < 0.0) { tx_angle = 2*M_PI + tx_angle;}
          double rotation = (tx_angle - angle);
          //Compensate for rotating more than half a circle
          if (rotation < -M_PI) {
            rotation = 2*M_PI + rotation;
          }
          else if (rotation > M_PI) {
            rotation = 2*M_PI - rotation;
          }
          //the number of slices from the current slice to get to this sample
          int slice_dist = round(abs(rotation)/slice_step);
          //Decrease weight by 60% for each slice distant
          double weight = pow(0.40, slice_dist);
          //double weight = pow(1.0 - rotation / M_PI, 3.0);
          alpha_sum += weight*alpha;
          total_weight += weight;
          //std::cerr<<"tx angle, rotation, slice dist, and weight are "<<tx_angle<<", "<<rotation<<", "<<slice_dist<<", "<<weight<<'\n';
          //std::cerr<<"txer "<<txer<<", dist and alpha are "<<dist<<", "<<alpha<<" for angle "<<tx_angle<<": weight is "<<weight<<'\n';
        }
        else {
          std::cerr<<"was nan: dist and alpha were "<<dist<<", "<<alpha<<" for angle "<<atan2(ydiff, xdiff)<<" and rss "<<rss<<'\n';
        }
      }
      if (total_weight > 0.0) {
        //alphas[rxer].push_back(AlphaEstimate{alpha_sum / total_weight, default_alphas[rxer].variance});
        //Use the variance of this quadrant to estimate the alpha value
        //AlphaEstimate quad_est = interpolateSlices(angle > M_PI ? 2*M_PI - angle : angle, quad_alphas[rxer]);
        AlphaEstimate quad_est = quad_alphas[rxer][cur_slice*4 / num_slices];
        alphas[rxer].push_back(AlphaEstimate{alpha_sum / total_weight, quad_est.variance});
      }
      else {
        alphas[rxer].push_back(default_alphas[rxer]);
      }
      //std::cerr<<"Alpha avg/variance for receiver "<<rxer<<" at slice "<<(int)(angle/(8*M_PI))<<" is "<<alphas[rxer].back().mean<<"/"<<alphas[rxer].back().variance<<'\n';
    }
  }
  return alphas;
}

double getMinConfidenceCoefficient(double distance, double signal, AlphaEstimate& ae, double gain) {
  //First find the zero confidence interval distance
  double zero_conf_dist = expectedDistance(signal, ae.mean, gain);
  //The equation is the same whether we need to go closer or farther.
  //log10(distance) = (signal - gain) / (-10 * (alpha + confidence*sqrt(variance)))
  //(alpha + confidence*sqrt(variance)) = (gain - signal) / (10 * log10(dist))
  //confidence*sqrt(variance) = (gain - signal) / (10 * log10(dist)) - alpha
  //confidence = ((gain - signal) / (10 * log10(dist)) - alpha) / sqrt(variance)
  //Set the distance to a sane value for the equation below
  if (distance < 2.0) {
    distance = 2.0;
  }
  double confidence = ((gain - signal) / (10 * log10(distance)) - ae.mean) / sqrt(ae.variance);
  //std::cerr<<"Adjusting zero conf distance of "<<zero_conf_dist<<" to "<<distance<<" with confidence "<<confidence<<'\n';
  //std::cerr<<"Signal and alpha mean/variance and gain are "<<signal<<", "<<ae.mean<<"/"<<ae.variance<<", and "<<gain<<'\n';
  return fabs(confidence);
}

//The upper and lower bound for the origin of the given signal with the given
//alpha estimate and confidence coefficient
pair<double, double> distanceBounds(double signal, AlphaEstimate& ae, double confidence, double gain = -3) {
  //Larger alpha values decrease the distance that signal travel, and vice-versa
  double lower = expectedDistance(signal, ae.mean + confidence*sqrt(ae.variance), gain);
  double upper = expectedDistance(signal, ae.mean - confidence*sqrt(ae.variance), gain);
  return make_pair(lower, upper);
}

//The expected change in signal strength at a given distance and the given attenuation coefficient, alpha
double expectedRSS(double distance, double alpha, double initial = -10) {
  return initial + -10 * log10(distance) * alpha;
}

struct Locus {
  double x;
  double y;
  double radius;
};

struct PointProbability {
  double x;
  double y;
  long double prob;
};

pair<double, double> genRadAngle(double radius) {
  //TODO switch to C++ random number generator
  double angle = ((double)random() / RAND_MAX) * 2 * M_PI;
  double rad = ((double)random() / RAND_MAX) * radius;

  return make_pair(rad, angle);
}

//Generate a random x,y coordinate offset from the center of a circle with the given radius
pair<double, double> genXY(double radius) {
  double x, y;
  do {
    x = radius - 2*radius*((double)random() / RAND_MAX);
    y = radius - 2*radius*((double)random() / RAND_MAX);
  } while (x*x + y*y > radius*radius);

  return make_pair(x, y);
}

//Return the receiver with the lowest correlation and the average of its correlations
pair<size_t, double> findLowCorrRxer(Locus& l, Sample& unknown, LandmarkData& rxers, vector<AlphaEstimate>& alphas,
                                     vector<bool>& good_rxers, double var_coefficient) {

  //Probabilities from the receivers
  vector<vector<double>> probs(rxers.size());
  vector<double> iter_means;

  //Check the probabilities at 100 points.
  for (size_t i = 0; i < 100; ++i) {
    //Generate a random location around the locus
    pair<double, double> xy = genXY(l.radius);
    double x = l.x + xy.first;
    double y = l.y + xy.second;

    int total = 0;
    double sum = 0;
    for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
      if (good_rxers[rxer]) {
        double dist = sqrt(pow(rxers[rxer].first - x, 2.0) + pow(rxers[rxer].second - y, 2.0));
        double prob = probSignalFromAlphas(unknown.rssis[rxer], dist, alphas[rxer], var_coefficient);
        probs[rxer].push_back(prob);
        sum += prob;
        ++total;
      }
    }
    std::cerr<<"Pushing average "<<sum/total<<'\n';
    iter_means.push_back(sum / total);
  }

  //Now find the covariances with the mean
  //First find the means and variances
  double iter_mean = getAverage(iter_means);
  double iter_var = getVariance(iter_means, iter_mean);
  vector<double> rx_means(rxers.size());
  vector<double> rx_vars(rxers.size());
  for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
    if (good_rxers[rxer]) {
      rx_means[rxer] = getAverage(probs[rxer]);
      rx_vars[rxer] = getVariance(probs[rxer], rx_means[rxer]);
    }
  }
  size_t worst = 0;
  double low_covar = INT_MAX;
  std::cerr<<"Iter mean and variance are "<<iter_mean<<" and "<<iter_var<<'\n';
  for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
    std::cerr<<"Checking covariance of rxer "<<rxer<<"\n";
    std::cerr<<"Rxer mean and variance are "<<rx_means[rxer]<<" and "<<rx_vars[rxer]<<'\n';
    if (good_rxers[rxer]) {
      //Check the covariance
      double cum_covar = 0.0;
      for (size_t i = 0; i < 100; ++i) {
        std::cerr<<"Cumulative covariance increases by "<<((probs[rxer][i] - rx_means[rxer]) * (iter_means[i] - iter_mean)) / ((100-1)*rx_vars[rxer]*iter_var)<<'\n';

        cum_covar += (probs[rxer][i] - rx_means[rxer]) * (iter_means[i] - iter_mean);
      }
      cum_covar /= (100 - 1)*rx_vars[rxer]*iter_var;
      std::cerr<<"Cumulative covariance is "<<cum_covar<<'\n';
      if (cum_covar < low_covar) {
        worst = rxer;
        low_covar = cum_covar;
      }
    }
  }
  return make_pair(worst, low_covar);
}

//Return the receiver with the lowest probabilities and the average of its probabilities
pair<size_t, double> findLowProbRxer(Locus& l, Sample& unknown, LandmarkData& rxers, vector<AlphaEstimate>& alphas,
                                  vector<bool>& good_rxers, double var_coefficient) {

  //Probabilities from the receivers
  vector<vector<double>> probs(rxers.size());

  //Check the probabilities at 100 points.
  for (size_t i = 0; i < 100; ++i) {
    //Generate a random location around the locus
    pair<double, double> xy = genXY(l.radius);
    double x = l.x + xy.first;
    double y = l.y + xy.second;

    for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
      if (good_rxers[rxer]) {
        if (not (isnan(unknown.rssis[rxer]) or isnan(alphas[rxer].mean))) {
          double dist = sqrt(pow(rxers[rxer].first - x, 2.0) + pow(rxers[rxer].second - y, 2.0));
          double prob = probSignalFromAlphas(unknown.rssis[rxer], dist, alphas[rxer], var_coefficient);
          probs[rxer].push_back(prob);
        }
      }
    }
  }

  size_t worst = 0;
  double low_prob = 2.0;
  for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
    if (good_rxers[rxer]) {
      double avg = getAverage(probs[rxer]);
      if (avg < low_prob) {
        worst = rxer;
        low_prob = avg;
      }
    }
  }
  return make_pair(worst, low_prob);
}

double genRandPointProb(double x, double y, Sample& unknown, LandmarkData& rxers, vector<AlphaEstimate>& alphas,
                        vector<bool>& good_rxers, double var_coefficient) {
  double cum_prob = 1.0;
  for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
    if (good_rxers[rxer]) {
      if (not (isnan(unknown.rssis[rxer]) or isnan(alphas[rxer].mean))) {
        double dist = sqrt(pow(rxers[rxer].first - x, 2.0) + pow(rxers[rxer].second - y, 2.0));
        std::cerr<<"\trxer "<<rxer;
        double prob = probSignalFromAlphas(unknown.rssis[rxer], dist, alphas[rxer], var_coefficient);
        cum_prob *= prob;
      }
    }
  }
  return cum_prob;
}

struct Scorer {
  private:
    //Data needed to check a set of alpha values
    Sample unknown;
    LandmarkData rxers;
    vector<AlphaEstimate> alpha_ests;
    vector<vector<AlphaEstimate>> radial_alpha_ests;
    double maxx;
    double maxy;
    double resolution;
  public:
    Scorer(Sample unknown, LandmarkData rxers, vector<AlphaEstimate> ae, vector<vector<AlphaEstimate>> rad_ae,
           double maxx, double maxy, double resolution) : maxx(maxx), maxy(maxy), resolution(resolution){
      //Only use the receivers that have real values.
      this->unknown = unknown;
      this->unknown.rssis.clear();
      for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
        if (not (isnan(unknown.rssis[rxer]) or isnan(ae[rxer].mean))) {
          this->rxers.push_back(rxers[rxer]);
          this->alpha_ests.push_back(ae[rxer]);
          this->unknown.rssis.push_back(unknown.rssis[rxer]);
          this->radial_alpha_ests.push_back(rad_ae[rxer]);
        }
      }
    }

    vector<float> makeInitialAlphaBits() {
      vector<float> alpha_bits;
      for (auto I = alpha_ests.begin(); I != alpha_ests.end(); ++I) {
        //Each receiver's alpha value requires five bits
        int approx = (I->mean - 2.0) / 0.1;
        std::bitset<5> bits(approx);
        for (int p = 4; p >= 0; --p) {
          //We are more certain about the higher-order bits so have the bit position affect the probability
          //alpha_bits.push_back( bits.test(p) ? (1.0 - pow(0.5, p+1)) : pow(0.5, p+1));
          alpha_bits.push_back( bits.test(p) ? (0.75) : 0.25);
          //alpha_bits.push_back( bits.test(p) ? (0.5 + 0.5*pow(0.5, p+1)) : 0.5 - pow(0.5, p+1));
        }
      }
      //Also include switches for each of the receivers.
      for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
        alpha_bits.push_back(0.8);
        //alpha_bits.push_back(1.0);
      }
      return alpha_bits;
    }

    vector<float> makeInitialRadialAlphaBits() {
      vector<float> alpha_bits;
      for (auto J = radial_alpha_ests.begin(); J != radial_alpha_ests.end(); ++J) {
        for (auto I = J->begin(); I != J->end(); ++I) {
          //Each receiver's alpha value requires five bits
          int approx = (I->mean - 2.0) / 0.1;
          std::bitset<5> bits(approx);
          for (int p = 4; p >= 0; --p) {
            //We are more certain about the higher-order bits so have the bit position affect the probability
            //alpha_bits.push_back( bits.test(p) ? (1.0 - pow(0.5, p+1)) : pow(0.5, p+1));
            alpha_bits.push_back( bits.test(p) ? (0.75) : 0.25);
            //alpha_bits.push_back( bits.test(p) ? (0.5 + 0.5*pow(0.5, p+1)) : 0.5 - pow(0.5, p+1));
          }
        }
      }
      //Also include switches for each of the receivers.
      for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
        alpha_bits.push_back(0.8);
        //alpha_bits.push_back(1.0);
      }
      return alpha_bits;
    }

    //Check which receivers are turned on or off
    vector<bool> makeRxOn(vector<bool>& alpha_bits) {
      vector<bool> rxon;
      for (auto I = alpha_bits.begin() + alpha_bits.size()-rxers.size(); I != alpha_bits.end(); ++I) {
        rxon.push_back(*I);
      }
      return rxon;
    }

    //Turn the bits into actual alpha values
    vector<AlphaEstimate> makeAlphaEstimate(vector<bool>& alpha_bits) {
      vector<AlphaEstimate> alphas;
      for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
        //Each alpha goes from 1 to 4.2
        //Five bits are used
        size_t base = rxer * 5;
        double alpha = 0.0;
        std::bitset<5> bits;
        for (size_t offset = 0; offset < 5; ++offset) {
          bits.set(4 - offset, alpha_bits[base+offset]);
        }
        //2 is the minimum alpha
        alpha = 2.0 + (bits.to_ulong())/10.0;
        /*
        std::cerr<<"Stored alpha is "<<bits.to_ulong()<<'\n';
        std::cerr<<"Rxer "<<rxer<<" is using alpha "<<alpha<<'\n';
        */
        alphas.push_back(AlphaEstimate{alpha, alpha_ests[rxer].variance});
      }
      return alphas;
    }

    //Turn the bits into actual alpha values
    vector<vector<AlphaEstimate>> makeRadialAlphaEstimate(vector<bool>& alpha_bits) {
      vector<vector<AlphaEstimate>> alphas(rxers.size());
      for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
        //Each alpha goes from 1 to 4.2
        //There are 16 slices for each receiver
        //Five bits are used for each alpha value
        const size_t num_slices = 16;
        size_t base = rxer * num_slices * 5;
        for (size_t slice = 0; slice < num_slices; ++slice) {
          size_t slice_base = base + slice * 5;
          double alpha = 0.0;
          std::bitset<5> bits;
          for (size_t offset = 0; offset < 5; ++offset) {
            bits.set(4 - offset, alpha_bits[slice_base+offset]);
          }
          //2 is the minimum alpha
          alpha = 2.0 + (bits.to_ulong())/10.0;
          alphas[rxer].push_back(AlphaEstimate{alpha, alpha_ests[rxer].variance});
        }
      }
      return alphas;
    }

    //Find a coefficient for the variance that gives non-zero values for the given alphas
    double findVarCoefficient(vector<AlphaEstimate>& alphas) {
      const int burnin = 500;
      size_t num_zeros = burnin;
      double var_coefficient = 0.0;
      while (num_zeros > 0.9*burnin) {
        std::cerr<<"Checking for zeros...\n";
        num_zeros = burnin;
        var_coefficient += 1.0;
        for (size_t i = 0; i < burnin; ++i) {
          //Generate a random location around the locus
          //TODO switch to C++ random number generator
          double x = ((double)random()/RAND_MAX) * 1.2 * maxx - (0.1*maxx);
          double y = ((double)random()/RAND_MAX) * 1.2 * maxy - (0.1*maxy);
          double cum_prob = 1.0;
          for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
            double dist = sqrt(pow(rxers[rxer].first - x, 2.0) + pow(rxers[rxer].second - y, 2.0));
            double prob = probSignalFromAlphas(unknown.rssis[rxer], dist, alphas[rxer], var_coefficient);
            cum_prob *= prob;
          }
          if (cum_prob > 0.0) {
            --num_zeros;
          }
        }
        std::cerr<<"\tFound "<<num_zeros<<" zeros.\n";
      }
      return var_coefficient;
    }

    //Scoring function that uses root mean squared distance from predicted distances
    float scoreWithRadialRMSD(vector<bool>& alpha_bits) {
      Sample unknown = this->unknown;
      //Adjust for initial loss
      std::for_each(unknown.rssis.begin(), unknown.rssis.end(), [](double& f){ f += 5.0;});

      //Turn the alpha bits into actual alpha values
      vector<vector<AlphaEstimate>> alphas = makeRadialAlphaEstimate(alpha_bits);
      vector<bool> rx_on = makeRxOn(alpha_bits);
      vector<uint32_t> chosen_slices(alphas.size());

      pair<double, double> best_point;
      double min_sum_squares = std::numeric_limits<double>::max();
      double min_alph_change = std::numeric_limits<double>::max();
      for (size_t outer_rx = 0; outer_rx < rxers.size(); ++outer_rx) {
        for (size_t out_slice = 0; out_slice < 16; ++out_slice) {
          double angle = out_slice * M_PI / 8.0;
          double radius = pow(10, -1*unknown.rssis[outer_rx]/(10*alphas[outer_rx][angle/(M_PI/8.0)].mean));
          double x = rxers[outer_rx].first + cos(angle)*radius;
          double y = rxers[outer_rx].second + sin(angle)*radius;
          double sum_squares = 0.0;
          double ss_alph_change = 0.0;
          vector<uint32_t> slices(alphas.size());
          slices[outer_rx] = out_slice;
          for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
            if (rx_on[rxer] and rxer != outer_rx) {
              //Distance along x and y axes to get to the inner receiver from this receiver
              //(positive y is down, negative y is up, positive x is right, negative x is left
              double xdiff = x - rxers[rxer].first;
              double ydiff = y - rxers[rxer].second;
              //Add 180 degrees (pi) so that there is no negative angle
              double rx_angle = atan2( ydiff, xdiff) + M_PI;
              //This is out of phase by M_PI/16 so add that back in
              uint32_t slice = (rx_angle + M_PI/16.0) / (M_PI/8.0);
              double dist = sqrt(pow(xdiff, 2.0) + pow(ydiff, 2.0));
              double r1 = pow(10, -1*unknown.rssis[rxer]/(10*alphas[rxer][slice].mean));
              sum_squares += pow(dist - r1, 2.0);
              //How much would the current receiver's alpha need to change to have its signal to distance hit this point?
              double diff_alph = -1.0 * unknown.rssis[rxer] / (10 * log10(dist));
              double alph_change = diff_alph - alphas[rxer][slice].mean;
              ss_alph_change += pow(alph_change, 2.0);
              slices[rxer] = slice;
            }
          }
          //TODO Try to remember the high hit points and check around them more or something.
          //This could save processing time and improve results
          if (sum_squares < min_sum_squares) {
            min_sum_squares = sum_squares;
            best_point = make_pair(x, y);
            min_alph_change = ss_alph_change;
            chosen_slices = slices;
          }
        }
      }
      size_t total_rxers = std::count_if(rx_on.begin(), rx_on.end(), [](bool b){return b;});
      if (total_rxers == 0) {
        return std::numeric_limits<double>::max();
      }
      //Room me square distance
      double rmsd = sqrt(min_sum_squares/(total_rxers-1));
      //Root mean alpha change
      double rmac = sqrt(min_alph_change/(total_rxers-1));
      if (isnan(rmsd)) { return std::numeric_limits<float>::min();}
      //std::cerr<<"Returning RMSD "<<rmsd<<"\n";
      //Give a penalty for not including a receiver and for deviating from the estimated alphas
      double deviation_penalty = 1.0;
      size_t num_deviated = 0;
      double sum_deviations = 0.0;
      double total_movement = 0.0;
      double abs_sum_deviations = 0.0;
      for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
        if (rx_on[rxer]) {
          double r1 = pow(10, -1*unknown.rssis[rxer]/(10*radial_alpha_ests[rxer][chosen_slices[rxer]].mean));
          double r2 = pow(10, -1*unknown.rssis[rxer]/(10*alphas[rxer][chosen_slices[rxer]].mean));
          total_movement += pow(fabs(r1 - r2), 2.0);
          int deviations = (radial_alpha_ests[rxer][chosen_slices[rxer]].mean - alphas[rxer][chosen_slices[rxer]].mean) / sqrt(radial_alpha_ests[rxer][chosen_slices[rxer]].variance);
          if (isnan(deviations)) {
            deviations = 0.0;
          }
          sum_deviations += deviations;
          abs_sum_deviations += fabs(deviations);
          if (abs(deviations) > deviation_penalty) {
            deviation_penalty = abs(deviations);
          }
          if (deviations != 0.0) {
            ++num_deviated;
          }
        }
      }
      //double drop_penalty = (1 + rxers.size() - total_rxers);
      //double dropped = rxers.size() - total_rxers;
      double cost_dropped = (rxers.size()-total_rxers)*abs_sum_deviations/total_rxers;
      //Expect about a quarter of the receivers to get a deviation less signal than estimated
      double expected_dev = -0.25*total_rxers;
      double actual_dev = sum_deviations / total_rxers;
      //TODO FIXME This is just for evaluation of different scoring methods.
      {
        pair<double, double> actual{142.0, 24.0};
        double err = sqrt(pow(best_point.first - actual.first, 2.0)+pow(best_point.second - actual.second, 2.0));
        //So far:
        //  rmsd is very correlated
        //  negative deviations are good, high positive ones are bad, unless the total deviations is too high
        //  total movement is a little correlated
        std::cerr<<"SCORE "<<err<<'\t'<<rmsd<<'\t'<<rxers.size()-total_rxers<<'\t'<<num_deviated<<'\t'<<
          sum_deviations<<'\t'<<abs_sum_deviations<<'\t'<<cost_dropped<<'\t'<<
          total_movement<<'\t'<<0.0<<'\t'<<pow(expected_dev - actual_dev, 2.0)<<'\t'<<rmac<<'\n';
      }
      //return -1*rmsd*deviation_penalty*drop_penalty - drop_penalty - num_deviated;
      //return -1*rmsd - drop_penalty - deviation_penalty - close_penalty;
      //THIS WORKS PRETTY WELL
      //return -1*rmsd - .5*(sum_deviations) - num_deviated - drop_penalty;
      //return -1*rmsd - .5*(sum_deviations) - num_deviated - (dropped < rxers.size()/4.0 ? 0 : dropped * 10);
      //return -1*rmsd - 100*dropped*abs_sum_deviations;
      //Total movement looks promising, but too many points don't converge
      //return -1*rmsd - 10*total_movement;
      //return -1*rmsd*drop_penalty*deviation_penalty*close_penalty;
      return -1 * rmac;
    }

    //Scoring function that uses root mean squared distance from predicted distances
    float scoreWithRMSD(vector<bool>& alpha_bits) {
      Sample unknown = this->unknown;
      //Adjust for initial loss
      std::for_each(unknown.rssis.begin(), unknown.rssis.end(), [](double& f){ f += 5.0;});
      for (auto I = alpha_bits.begin(); I != alpha_bits.end(); ++I) {
        std::cerr.width(5);
      }
      //Turn the alpha bits into actual alpha values
      vector<AlphaEstimate> alphas = makeAlphaEstimate(alpha_bits);
      vector<bool> rx_on = makeRxOn(alpha_bits);

      size_t closest = 0;
      double close_rss = unknown.rssis[0];
      //Find out the closest receiver so that it can be counted more than the others.
      for (size_t rxer = 1; rxer < rxers.size(); ++rxer) {
        if (rx_on[rxer] and unknown.rssis[rxer] > close_rss) {
          closest = rxer;
          close_rss = unknown.rssis[rxer];
        }
      }

      pair<double, double> best_point;
      double min_sum_squares = std::numeric_limits<double>::max();
      double min_alph_change = std::numeric_limits<double>::max();
      for (size_t outer_rx = 0; outer_rx < rxers.size(); ++outer_rx) {
        double radius = pow(10, -1*unknown.rssis[outer_rx]/(10*alphas[outer_rx].mean));
        for (double angle = 0; angle < 2 * M_PI; angle += M_PI / 8.0) {
          double x = rxers[outer_rx].first + cos(angle)*radius;
          double y = rxers[outer_rx].second + sin(angle)*radius;
          double sum_squares = 0.0;
          double ss_alph_change = 0.0;
          for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
            if (rx_on[rxer] and rxer != outer_rx) {
              double dist = sqrt(pow(rxers[rxer].first - x, 2.0) + pow(rxers[rxer].second - y, 2.0));
              double r1 = pow(10, -1*unknown.rssis[rxer]/(10*alphas[rxer].mean));
              sum_squares += pow(dist - r1, 2.0);
              //How much would the current receiver's alpha need to change to have its signal to distance hit this point?
              double diff_alph = -1.0 * unknown.rssis[rxer] / (10 * log10(dist));
              double alph_change = diff_alph - alphas[rxer].mean;
              ss_alph_change += pow(alph_change, 2.0);
            }
          }
          //TODO Try to remember the high hit points and check around them more or something.
          //This could save processing time and improve results
          if (sum_squares < min_sum_squares) {
            min_sum_squares = sum_squares;
            best_point = make_pair(x, y);
            min_alph_change = ss_alph_change;
          }
        }
      }
      size_t total_rxers = std::count_if(rx_on.begin(), rx_on.end(), [](bool b){return b;});
      if (total_rxers == 0) {
        return std::numeric_limits<double>::max();
      }
      double rmsd = sqrt(min_sum_squares/(total_rxers-1));
      double rmac = sqrt(min_alph_change/(total_rxers-1));
      if (isnan(rmsd)) { return std::numeric_limits<float>::min();}
      //std::cerr<<"Returning RMSD "<<rmsd<<"\n";
      //Give a penalty for not including a receiver and for deviating from the estimated alphas
      double deviation_penalty = 1.0;
      size_t num_deviated = 0;
      double sum_deviations = 0.0;
      double total_movement = 0.0;
      double abs_sum_deviations = 0.0;
      for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
        if (rx_on[rxer]) {
          double r1 = pow(10, -1*unknown.rssis[rxer]/(10*alpha_ests[rxer].mean));
          double r2 = pow(10, -1*unknown.rssis[rxer]/(10*alphas[rxer].mean));
          total_movement += pow(fabs(r1 - r2), 2.0);
          int deviations = (alpha_ests[rxer].mean - alphas[rxer].mean) / sqrt(alpha_ests[rxer].variance);
          if (isnan(deviations)) {
            deviations = 0.0;
          }
          //int deviations = (alpha_ests[rxer].mean - alphas[rxer].mean) / sqrt(0.1);
          sum_deviations += deviations;
          abs_sum_deviations += fabs(deviations);
          if (abs(deviations) > deviation_penalty) {
            deviation_penalty = abs(deviations);
          }
          if (deviations != 0.0) {
            ++num_deviated;
          }
        }
      }
      //double close_penalty = fabs((alpha_ests[closest].mean - alphas[closest].mean) / sqrt(0.1));
      double drop_penalty = (1 + rxers.size() - total_rxers);
      //double dropped = rxers.size() - total_rxers;
      double cost_dropped = (rxers.size()-total_rxers)*abs_sum_deviations/total_rxers;
      double close_dev = fabs(alpha_ests[closest].mean-alphas[closest].mean)/alpha_ests[closest].variance;
      //Expect about a quarter of the receivers to get a deviation less signal than estimated
      double expected_dev = -0.25*total_rxers;
      double actual_dev = sum_deviations / total_rxers;
      //TODO FIXME This is just for evaluation of different scoring methods.
      {
        pair<double, double> actual{142.0, 24.0};
        double err = sqrt(pow(best_point.first - actual.first, 2.0)+pow(best_point.second - actual.second, 2.0));
        //So far:
        //  rmsd is very correlated
        //  negative deviations are good, high positive ones are bad, unless the total deviations is too high
        //  total movement is a little correlated
        std::cerr<<"SCORE "<<err<<'\t'<<rmsd<<'\t'<<rxers.size()-total_rxers<<'\t'<<num_deviated<<'\t'<<
          sum_deviations<<'\t'<<abs_sum_deviations<<'\t'<<cost_dropped<<'\t'<<
          total_movement<<'\t'<<close_dev<<'\t'<<pow(expected_dev - actual_dev, 2.0)<<'\t'<<rmac<<'\n';
      }
      //return -1*rmsd*deviation_penalty*drop_penalty - drop_penalty - num_deviated;
      //return -1*rmsd - drop_penalty - deviation_penalty - close_penalty;
      //THIS WORKS PRETTY WELL
      return -1*rmsd - .5*(sum_deviations) - num_deviated - drop_penalty;
      //return -1*rmsd - .5*(sum_deviations) - num_deviated - (dropped < rxers.size()/4.0 ? 0 : dropped * 10);
      //return -1*rmsd - 100*dropped*abs_sum_deviations;
      //Total movement looks promising, but too many points don't converge
      //return -1*rmsd - 10*total_movement;
      //return -1*rmsd*drop_penalty*deviation_penalty*close_penalty;
    }

    //Get the best point with RMSD
    pair<double, double> getPointWithRadialRMSD(vector<float>& alpha_probs) {
      Sample unknown = this->unknown;
      //Adjust for initial loss
      std::for_each(unknown.rssis.begin(), unknown.rssis.end(), [](double& f){ f += 5.0;});
      //Turn the alpha bits into actual alpha values
      vector<bool> alpha_bits(alpha_probs.size());
      std::transform(alpha_probs.begin(), alpha_probs.end(), alpha_bits.begin(), [](float a) { return a > 0.5;});
      vector<vector<AlphaEstimate>> alphas = makeRadialAlphaEstimate(alpha_bits);
      vector<bool> rx_on = makeRxOn(alpha_bits);
      std::cerr<<"RSS values were ";
      for (auto I  = unknown.rssis.begin(); I != unknown.rssis.end(); ++I) {
        std::cerr<<'\t'<<*I;
      }
      std::cerr<<'\n';
      std::cerr<<"Final alphas are ";
      for (auto I  = alphas.begin(); I != alphas.end(); ++I) {
        for (auto J = I->begin(); J != I->end(); ++J) {
          std::cerr<<'\t'<<J->mean;
        }
        std::cerr<<"\n\t";
      }
      std::cerr<<'\n';


      vector<uint32_t> chosen_slices(alphas.size());

      pair<double, double> best_point;
      double min_sum_squares = std::numeric_limits<double>::max();
      double min_alph_change = std::numeric_limits<double>::max();
      for (size_t outer_rx = 0; outer_rx < rxers.size(); ++outer_rx) {
        for (size_t out_slice = 0; out_slice < 16; ++out_slice) {
          double angle = out_slice * M_PI / 8.0;
          double radius = pow(10, -1*unknown.rssis[outer_rx]/(10*alphas[outer_rx][angle/(M_PI/8.0)].mean));
          double x = rxers[outer_rx].first + cos(angle)*radius;
          double y = rxers[outer_rx].second + sin(angle)*radius;
          double sum_squares = 0.0;
          double ss_alph_change = 0.0;
          vector<uint32_t> slices(alphas.size());
          slices[outer_rx] = out_slice;
          for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
            if (rx_on[rxer] and rxer != outer_rx) {
              //Distance along x and y axes to get to the inner receiver from this receiver
              //(positive y is down, negative y is up, positive x is right, negative x is left
              double xdiff = x - rxers[rxer].first;
              double ydiff = y - rxers[rxer].second;
              //Add 180 degrees (pi) so that there is no negative angle
              double rx_angle = atan2( ydiff, xdiff) + M_PI;
              //This is out of phase by M_PI/16 so add that back in
              uint32_t slice = (rx_angle + M_PI/16.0) / (M_PI/8.0);
              double dist = sqrt(pow(xdiff, 2.0) + pow(ydiff, 2.0));
              double r1 = pow(10, -1*unknown.rssis[rxer]/(10*alphas[rxer][slice].mean));
              sum_squares += pow(dist - r1, 2.0);
              //How much would the current receiver's alpha need to change to have its signal to distance hit this point?
              double diff_alph = -1.0 * unknown.rssis[rxer] / (10 * log10(dist));
              double alph_change = diff_alph - alphas[rxer][slice].mean;
              ss_alph_change += pow(alph_change, 2.0);
              slices[rxer] = slice;
            }
          }
          //TODO Try to remember the high hit points and check around them more or something.
          //This could save processing time and improve results
          if (sum_squares < min_sum_squares) {
            min_sum_squares = sum_squares;
            best_point = make_pair(x, y);
            min_alph_change = ss_alph_change;
            chosen_slices = slices;
          }
        }
      }

      return best_point;
    }

    //Get the best point with RMSD
    pair<double, double> getPointWithRMSD(vector<float>& alpha_probs) {
      Sample unknown = this->unknown;
      //Adjust for initial loss
      std::for_each(unknown.rssis.begin(), unknown.rssis.end(), [](double& f){ f += 5.0;});
      //Turn the alpha bits into actual alpha values
      vector<bool> alpha_bits(alpha_probs.size());
      std::transform(alpha_probs.begin(), alpha_probs.end(), alpha_bits.begin(), [](float a) { return a > 0.5;});
      vector<AlphaEstimate> alphas = makeAlphaEstimate(alpha_bits);
      vector<bool> rx_on = makeRxOn(alpha_bits);
      std::cerr<<"RSS values were ";
      for (auto I  = unknown.rssis.begin(); I != unknown.rssis.end(); ++I) {
        std::cerr<<'\t'<<*I;
      }
      std::cerr<<'\n';
      std::cerr<<"Final alphas are ";
      for (auto I  = alphas.begin(); I != alphas.end(); ++I) {
        std::cerr<<'\t'<<I->mean;
      }
      std::cerr<<'\n';
      std::cerr<<"Final delta alphas are ";
      double sum_deviations = 0.0;
      double total_movement = 0.0;
      for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
        if (rx_on[rxer]) {
          double r1 = pow(10, -1*unknown.rssis[rxer]/(10*alpha_ests[rxer].mean));
          double r2 = pow(10, -1*unknown.rssis[rxer]/(10*alphas[rxer].mean));
          total_movement += pow(fabs(r1 - r2), 2.0);
          //int deviations = (alpha_ests[rxer].mean - alphas[rxer].mean) / alpha_ests[rxer].variance;
          int deviations = (alpha_ests[rxer].mean - alphas[rxer].mean) / 0.1;
          std::cerr<<'\t'<<deviations;
          sum_deviations += deviations;
        }
        else {
          std::cerr<<"\tskip";
        }
      }
      std::cerr<<'\n';
      std::cerr<<"\tDeviation sum is "<<sum_deviations<<'\n';
      std::cerr<<"\tTotal movement is "<<total_movement<<'\n';

      size_t closest = 0;
      double close_rss = unknown.rssis[0];
      //Find out the closest receiver so that it can be counted more than the others.
      for (size_t rxer = 1; rxer < rxers.size(); ++rxer) {
        if (rx_on[rxer] and unknown.rssis[rxer] > close_rss) {
          closest = rxer;
          close_rss = unknown.rssis[rxer];
        }
      }

      std::cerr<<"\tClose penalty is "<<fabs((alpha_ests[closest].mean - alphas[closest].mean) / sqrt(0.1))<<'\n';
      std::cerr<<"Using "<<std::count_if(rx_on.begin(), rx_on.end(), [](bool b){return b;})<<" out of "<<rx_on.size()<<" receivers.\n";

      for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
        if (rx_on[rxer]) {
          double r1 = pow(10, -1*unknown.rssis[rxer]/(10*alphas[rxer].mean));
          double r2 = pow(10, -1*unknown.rssis[rxer]/(10*alpha_ests[rxer].mean));
          std::cerr<<"Rx ring: rxer "<<rxer<<" r1 and r2 are "<<r1<<" "<<r2<<" from location "<<
            rxers[rxer].first<<", "<<rxers[rxer].second<<'\n';
        }
      }

      double min_sum_squares = std::numeric_limits<double>::max();
      pair<double, double> best_point;
      for (size_t outer_rx = 0; outer_rx < rxers.size(); ++outer_rx) {
        double radius = pow(10, -1*unknown.rssis[outer_rx]/(10*alphas[outer_rx].mean));
        for (double angle = 0; angle < 2 * M_PI; angle += M_PI / 8.0) {
          double x = rxers[outer_rx].first + cos(angle)*radius;
          double y = rxers[outer_rx].second + sin(angle)*radius;
          double sum_squares = 0.0;
          for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
            if (rx_on[rxer] and rxer != outer_rx) {
              double dist = sqrt(pow(rxers[rxer].first - x, 2.0) + pow(rxers[rxer].second - y, 2.0));
              double r1 = pow(10, -1*unknown.rssis[rxer]/(10*alphas[rxer].mean));
              sum_squares += pow(dist - r1, 2.0);
            }
          }
          //TODO Try to remember the high hit points and check around them more or something.
          //This could save processing time and improve results
          if (sum_squares < min_sum_squares) {
            min_sum_squares = sum_squares;
            best_point = make_pair(x, y);
          }
        }
      }

      return best_point;
    }

    //Scoring function that uses trilateral location estimation
    float scoreWithTrilateration(vector<bool>& alpha_bits) {
      for (auto I = alpha_bits.begin(); I != alpha_bits.end(); ++I) {
        std::cerr.width(5);
      }
      //Turn the alpha bits into actual alpha values
      vector<AlphaEstimate> alphas = makeAlphaEstimate(alpha_bits);
      vector<bool> rx_on = makeRxOn(alpha_bits);

      //Readjust variances to get non-zero results, but start at the previous discovered variances.
      double var_coefficient = 0.0;

      size_t max_hits = 0;
      while (max_hits == 0) {
        //Var coefficient starts off at one and keeps rising until we hit a good value
        ++var_coefficient;
        for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
          if (rx_on[rxer]) {
            //Stop and give up if alpha falls below 1 for any receiver
            if (alphas[rxer].mean*(1.0 - alphas[rxer].variance*var_coefficient) <= 1.0) {
              return 0.0;
            }
          }
        }
        for (double x = 0; x <= maxx; x += resolution/2.0) {
          for (double y = 0; y <= maxy; y += resolution/2.0) {
            //std::cerr<<"Using ("<<x<<", "<<y<<")\n";
            size_t hits = 0;
            for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
              if (rx_on[rxer]) {
                double dist = sqrt(pow(rxers[rxer].first - x, 2.0) + pow(rxers[rxer].second - y, 2.0));
                //Find the two radiuses that the transmitter can be between
                double r1 = pow(10, -1*unknown.rssis[rxer]/(10*alphas[rxer].mean*(1+alphas[rxer].variance*var_coefficient)));
                double r2 = pow(10, -1*unknown.rssis[rxer]/(10*alphas[rxer].mean*(1-alphas[rxer].variance*var_coefficient)));
                if (r1 <= dist and dist <= r2) {
                  ++hits;
                }
              }
            }
            //TODO Try to remember the high hit points and check around them more or something.
            //This could save processing time and improve results
            //std::cerr<<"\thits are "<<hits<<'\n';
            if (hits > max_hits) {
              //std::cerr<<hits<<" is the new maximum.\n";
              max_hits = hits;
            }
          }
        }
      }
      std::cerr<<"Returning score "<<max_hits - (var_coefficient-1.0)<<"\n";
      std::cerr<<"Generated from "<<max_hits<<" max hits and "<<var_coefficient<<" var coefficient with "<<rxers.size()<<" receivers.\n";
      return max_hits - 2*var_coefficient;
    }

    //Return the most likely point using the given probability vector
    //Get this point over <rounds> rounds with <iterations> iterations in each round.
    pair<double, double> getPointWithTrilateration(vector<float>& alpha_probs, int rounds, int iterations) {
      //Turn the alpha bits into actual alpha values
      vector<bool> alpha_bits(alpha_probs.size());
      std::transform(alpha_probs.begin(), alpha_probs.end(), alpha_bits.begin(), [](float a) { return a > 0.5;});
      vector<AlphaEstimate> alphas = makeAlphaEstimate(alpha_bits);
      vector<bool> rx_on = makeRxOn(alpha_bits);
      std::cerr<<"Final alphas are ";
      for (auto I  = alphas.begin(); I != alphas.end(); ++I) {
        std::cerr<<'\t'<<I->mean;
      }
      std::cerr<<'\n';
      std::cerr<<"Using "<<std::count_if(rx_on.begin(), rx_on.end(), [](bool b){return b;})<<" out of "<<rx_on.size()<<" receivers.\n";

      //Readjust variances to get non-zero results, but start at the previous discovered variances.
      double var_coefficient = 0.0;

      size_t max_hits = 0;
      vector<double> best_xs;
      vector<double> best_ys;
      while (max_hits == 0) {
        //Var coefficient starts off at one and keeps rising until we hit a good value
        ++var_coefficient;
        for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
          if (rx_on[rxer]) {
            //Stop and give up if alpha falls below 1 for any receiver
            if (alphas[rxer].mean*(1.0 - alphas[rxer].variance*var_coefficient) <= 1.0) {
              return make_pair(0.0, 0.0);
            }
            //Find the two radiuses that the transmitter can be between
            double r1 = pow(10, -1*unknown.rssis[rxer]/(10*alphas[rxer].mean*(1+alphas[rxer].variance*var_coefficient)));
            double r2 = pow(10, -1*unknown.rssis[rxer]/(10*alphas[rxer].mean*(1-alphas[rxer].variance*var_coefficient)));
            std::cerr<<"Rx ring: rxer "<<rxer<<" r1 and r2 are "<<r1<<" "<<r2<<" from location "<<
              rxers[rxer].first<<", "<<rxers[rxer].second<<'\n';
          }
        }
        for (double x = 0; x <= maxx; x += resolution/8.0) {
          for (double y = 0; y <= maxy; y += resolution/8.0) {
            //std::cerr<<"Using ("<<x<<", "<<y<<")\n";
            size_t hits = 0;
            for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
              if (rx_on[rxer]) {
                double dist = sqrt(pow(rxers[rxer].first - x, 2.0) + pow(rxers[rxer].second - y, 2.0));
                //Find the two radiuses that the transmitter can be between
                double r1 = pow(10, -1*unknown.rssis[rxer]/(10*alphas[rxer].mean*(1+alphas[rxer].variance*var_coefficient)));
                double r2 = pow(10, -1*unknown.rssis[rxer]/(10*alphas[rxer].mean*(1-alphas[rxer].variance*var_coefficient)));
                if (r1 <= dist and dist <= r2) {
                  ++hits;
                }
              }
            }
            //TODO Try to remember the high hit points and check around them more or something.
            //This could save processing time and improve results
            //std::cerr<<"\thits are "<<hits<<'\n';
            if (hits > max_hits) {
              //std::cerr<<hits<<" is the new maximum.\n";
              max_hits = hits;
              best_xs.clear();
              best_ys.clear();
            }
            if (hits == max_hits) {
              best_xs.push_back(x);
              best_ys.push_back(y);
            }
          }
        }
      }
      std::cout<<"Ending with "<<max_hits<<" hits out of "<<rxers.size()<<" receivers with "<<
        std::count_if(rx_on.begin(), rx_on.end(), [](bool b){return b;})<<" receivers on.\n";
      std::cout<<"Best points are ";
      for (int i = 0; i < best_ys.size(); ++i) {
        std::cout<<"\t("<<best_xs[i]<<", "<<best_ys[i]<<")";
      }
      std::cout<<'\n';
      //Find the center of the best hits
      double xcenter = getAverage(best_xs);
      double ycenter = getAverage(best_ys);
      return make_pair(xcenter, ycenter);
    }

    //Scoring function for the cross entropy method.
    float scoreAlphas(vector<bool>& alpha_bits) {
      std::cerr<<"Alpha bits is ";
      for (auto I = alpha_bits.begin(); I != alpha_bits.end(); ++I) {
        std::cerr.width(5);
        std::cerr<<*I<<'\t';
      }
      std::cerr<<'\n';
      //Score starts at 1 and is multiplied by later probabilities.
      float score = 1.0;
      //Turn the alpha bits into actual alpha values
      vector<AlphaEstimate> alphas = makeAlphaEstimate(alpha_bits);
      vector<bool> rx_on = makeRxOn(alpha_bits);

      //Decrease the score depending upon the number of deviations this
      //alpha is from our estimated values for each receiver.
      for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
        if (rx_on[rxer]) {
          int deviations = (alpha_ests[rxer].mean - alphas[rxer].mean) / alpha_ests[rxer].variance;
          score *= 1.0 / (1.0 + pow(deviations, 2.0));
        }
      }

      //Readjust variances to get non-zero results
      //double var_coefficient = findVarCoefficient(alphas);
      double var_coefficient = 1.0;

      int num_points = (maxx*maxy)/(resolution * resolution);
      double num_zeros = num_points;
      double max_prob = 0.0;
      while (num_zeros > 0.9 * num_points) {
        num_zeros = 0;
        max_prob = 0.0;
        //TODO Try randomizing the points again
        for (double x = 0; x <= maxx; x += resolution) {
          for (double y = 0; y <= maxy; y += resolution) {
            std::cerr<<"Using ("<<x<<", "<<y<<")\n";
            double cum_prob = 1.0;
            for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
              if (rx_on[rxer]) {
                double dist = sqrt(pow(rxers[rxer].first - x, 2.0) + pow(rxers[rxer].second - y, 2.0));
                //std::cerr<<"\trxer "<<rxer;
                double prob = probSignalFromAlphas(unknown.rssis[rxer], dist, alphas[rxer], var_coefficient);
                cum_prob *= prob;
              }
            }
            //TODO Try to remember the high probability points and check around them more or something.
            //This could save processing time and improve results
            std::cerr<<'\n';
            std::cerr<<"\tcum_prob is "<<cum_prob<<'\n';
            if (cum_prob > max_prob) {
              std::cerr<<cum_prob<<" is the new maximum.\n";
              max_prob = cum_prob;
            }
            if (cum_prob == 0.0) {
              ++num_zeros;
            }
          }
        }
        //Var coefficient starts off at one and keeps rising until we hit a good value
        ++var_coefficient;
      }
      //Take out a penalty for each unused receiver
      //A penalty of -5 corresponds to a receiver that gave a point probability of .00001
      double penalty = 5.0 * std::count_if(rx_on.begin(), rx_on.end(), std::logical_not<bool>());
      std::cerr<<"Returning score "<<score * max_prob<<" which in log format is "<<log(score*max_prob) - penalty<<'\n';
      if (max_prob == 0.0) {
        return std::numeric_limits<float>::min();
      }
      return log(score * max_prob) - penalty;
    }

    //Return the most likely point using the given probability vector
    //Get this point over <rounds> rounds with <iterations> iterations in each round.
    pair<double, double> getPoint(vector<float>& alpha_probs, int rounds, int iterations) {
      double xmin = -0.2*maxx;
      double width = 1.4 * maxx;
      double ymin = -0.2*maxy;
      double height = 1.4 * maxy;
      //Turn the alpha bits into actual alpha values
      vector<bool> alpha_bits(alpha_probs.size());
      std::transform(alpha_probs.begin(), alpha_probs.end(), alpha_bits.begin(), [](float a) { return a > 0.5;});
      vector<AlphaEstimate> alphas = makeAlphaEstimate(alpha_bits);
      vector<bool> rx_on = makeRxOn(alpha_bits);
      std::cerr<<"Final alphas are ";
      for (auto I  = alphas.begin(); I != alphas.end(); ++I) {
        std::cerr<<'\t'<<I->mean;
      }
      std::cerr<<'\n';
      std::cerr<<"Using "<<std::count_if(rx_on.begin(), rx_on.end(), [](bool b){return b;})<<" out of "<<rx_on.size()<<" receivers.\n";

      //Readjust variances to get non-zero results
      double var_coefficient = findVarCoefficient(alphas);

      double max_prob = 0.0;
      pair<double, double> best;
      for (size_t round = 0; round < rounds; ++round) {
        //Recenter and shrink the search region after every round.
        //Remember the probabilities at different points so that we can recenter in high probability areas.
        vector<PointProbability> point_probs;
        for (size_t i = 0; i < iterations; ++i) {
          //Generate a random location around the locus
          //TODO switch to C++ random number generator
          double x = xmin + ((double)random()/RAND_MAX) * width;
          double y = ymin + ((double)random()/RAND_MAX) * height;
          std::cerr<<"Using ("<<x<<", "<<y<<")\n";
          double cum_prob = 1.0;
          for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
            if (rx_on[rxer]) {
              double dist = sqrt(pow(rxers[rxer].first - x, 2.0) + pow(rxers[rxer].second - y, 2.0));
              std::cerr<<"\trxer "<<rxer;
              double prob = probSignalFromAlphas(unknown.rssis[rxer], dist, alphas[rxer], var_coefficient);
              cum_prob *= prob;
            }
          }
          std::cerr<<'\n';
          std::cerr<<"\tcum_prob is "<<cum_prob<<'\n';
          if (cum_prob > max_prob) {
            std::cerr<<cum_prob<<" is the new maximum.\n";
            max_prob = cum_prob;
            best = make_pair(x, y);
          }
          point_probs.push_back(PointProbability{x, y, cum_prob});
        }
        //Sort the point probabilities and then find the center of the upper quantile's points.
        double quantile = 0.90;
        std::sort(point_probs.begin(), point_probs.end(),
            [](const PointProbability& a, const PointProbability& b) { return a.prob < b.prob;});
        size_t start = quantile * point_probs.size();
        //Only use positions where the probability was not zero
        while (start < point_probs.size() and point_probs[start].prob <= 0) {
          ++start;
        }
        //size_t total = point_probs.size() - start;
        //Calculate the biased center of the upper percentile.
        double total_prob = std::accumulate(point_probs.begin() + start, point_probs.end(), 0.0,
            [&](double a, PointProbability& b) { return a + b.prob;});
        double cent_x = std::accumulate(point_probs.begin() + start, point_probs.end(), 0.0,
            [&](double a, PointProbability& b) { return a + b.x*(b.prob/total_prob);});
        double cent_y = std::accumulate(point_probs.begin() + start, point_probs.end(), 0.0,
            [&](double a, PointProbability& b) { return a + b.y*(b.prob/total_prob);});
        //Half the width and height of the search area and recenter
        width /= 2.0;
        height /= 2.0;
        xmin = cent_x - width/2.0;
        ymin = cent_y - height/2.0;
      }
      return best;
    }
};

/**
 * getNextLocus - Get the center and radius for the next locus during localization.
 * @l The current locus
 * @unknown The transmitter with unknown location but known RSS
 * @rxers The locations of the receivers
 * @alphas Estimates of alphas for signal propagation, as seen by each receiver
 * @quantile Which quantile to use for the next locus. The upper (1.0 - quantile) points will be used.
 * @iterations The number of points to generate before picking the upper quantile and making the next locus.
 */
Locus getNextLocus(Locus& l, Sample& unknown, LandmarkData& rxers, vector<AlphaEstimate>& alphas, double quantile, size_t iterations) {
  vector<PointProbability> point_probs;
  //Compute point probability taken across all of the receivers?
  std::cerr<<"Working with radius "<<l.radius<<'\n';
  vector<bool> good_rxers(rxers.size(), false);
  //Only accept receivers if they are close to the locus
  {
    size_t accepted = 0;
    double mod = 1;
    while (accepted < 2 and accepted != rxers.size()) {
      accepted = 0;
      for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
        double rx_dist = sqrt(pow(rxers[rxer].first - l.x, 2) + pow(rxers[rxer].second - l.y, 2));
        if (not (isnan(unknown.rssis[rxer]) or isnan(alphas[rxer].mean)) and rx_dist <= l.radius*mod) {
          good_rxers[rxer] = true;
          ++accepted;
        }
      }
      mod *= 2;
    }
  }
  //If the radius is small and we are only using a couple of receivers the entire
  //locus area might have a low probability. In that case, readjust our parameters by shrinking the variance.
  double var_coefficient = 0.0;
  //Find a reasonable var coefficient so that at least 10 percent of the values are non-zero
  const int burnin = 1000;
  vector<double> total_probs(rxers.size());
  size_t num_zeros = burnin;
  while (num_zeros > 0.9*burnin) {
    //std::cerr<<"Checking for zeros...\n";
    std::cerr<<"Checking for nans...\n";
    num_zeros = burnin;
    var_coefficient += 1.0;
    for (size_t i = 0; i < burnin; ++i) {
      //Generate a random location around the locus
      //TODO switch to C++ random number generator
      double angle = ((double)random() / RAND_MAX) * 2 * M_PI;
      double rad = ((double)random() / RAND_MAX) * l.radius;
      double x = l.x + cos(angle)*rad;
      double y = l.y + sin(angle)*rad;
      //std::cerr<<"Using radius and angle "<<rad<<" "<<angle<<"("<<x<<", "<<y<<")\n";
      //double cum_prob = genRandPointProb(x, y, unknown, rxers, alphas, good_rxers, var_coefficient);
      double cum_prob = 1.0;
      for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
        if (good_rxers[rxer]) {
          double dist = sqrt(pow(rxers[rxer].first - x, 2.0) + pow(rxers[rxer].second - y, 2.0));
          double prob = probSignalFromAlphas(unknown.rssis[rxer], dist, alphas[rxer], var_coefficient);
          total_probs[rxer] += prob / burnin;
          //cum_prob *= prob;
          cum_prob += log10(prob);
        }
      }
      //std::cerr<<"\tcum_prob is "<<cum_prob<<'\n';
      //if (cum_prob > 0.0)
      if (not isnan(cum_prob)) {
        --num_zeros;
      }
    }
    std::cerr<<"\tFound "<<num_zeros<<" zeros.\n";
  }

  for (size_t i = 0; i < iterations; ++i) {
    //Generate a random location around the locus
    //TODO switch to C++ random number generator
    double angle = ((double)random() / RAND_MAX) * 2 * M_PI;
    double rad = ((double)random() / RAND_MAX) * l.radius;
    double x = l.x + cos(angle)*rad;
    double y = l.y + sin(angle)*rad;
    //std::cerr<<"Using radius and angle "<<rad<<" "<<angle<<"("<<x<<", "<<y<<")\n";
    //double cum_prob = genRandPointProb(x, y, unknown, rxers, alphas, good_rxers, var_coefficient);
    double cum_prob = 1.0;
    for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
      if (good_rxers[rxer]) {
        double dist = sqrt(pow(rxers[rxer].first - x, 2.0) + pow(rxers[rxer].second - y, 2.0));
        //std::cerr<<"\trxer "<<rxer;
        double prob = probSignalFromAlphas(unknown.rssis[rxer], dist, alphas[rxer], var_coefficient);
        //cum_prob *= prob / (iterations*total_probs[rxer]/burnin);
        //cum_prob *= prob;
        cum_prob += log10(prob);
      }
    }
    point_probs.push_back(PointProbability{x, y, cum_prob});
    //std::cerr<<"\tcum_prob is "<<cum_prob<<'\n';
  }
  //Sort the point probabilities and then find the center of the upper quantile's points.
  std::sort(point_probs.begin(), point_probs.end(),
      [](const PointProbability& a, const PointProbability& b) { return a.prob < b.prob;});
  size_t start = quantile * point_probs.size();
  //Only use positions where the probability was not zero
  while (start < point_probs.size() and point_probs[start].prob <= 0) {
    ++start;
  }
  /*
  size_t total = point_probs.size() - start;
  double cent_x = std::accumulate(point_probs.begin() + start, point_probs.end(), 0.0,
      [&](double a, PointProbability& b) { return a + b.x/total;});
  double cent_y = std::accumulate(point_probs.begin() + start, point_probs.end(), 0.0,
      [&](double a, PointProbability& b) { return a + b.y/total;});
      */
  //Calculate the biased center of the upper percentile.
  double total_prob = std::accumulate(point_probs.begin() + start, point_probs.end(), 0.0,
      [&](double a, PointProbability& b) { return a + b.prob;});
  double cent_x = std::accumulate(point_probs.begin() + start, point_probs.end(), 0.0,
      [&](double a, PointProbability& b) { return a + b.x*(b.prob/total_prob);});
  double cent_y = std::accumulate(point_probs.begin() + start, point_probs.end(), 0.0,
      [&](double a, PointProbability& b) { return a + b.y*(b.prob/total_prob);});
  //Make the radius the greatest distance from the center to a point
  std::cerr<<"\nFinding radius for center at "<<cent_x<<", "<<cent_y<<'\n';
  double radius = std::accumulate(point_probs.begin() + start, point_probs.end(), 0.0,
      [&](double a, PointProbability& b) { double dist = sqrt(pow(b.x - cent_x, 2)+pow(b.y - cent_y, 2));
                                           if (dist > a) { std::cerr<<'\t'<<b.x<<", "<<b.y<<" now farthest (r = "<<dist<<")\n";}
                                           return std::max(dist, a); });
  return Locus{cent_x, cent_y, radius};
}

/**
 * getNextLocus - Get the center and radius for the next locus during localization.
 *                This overloaded function uses radial alpha estimations
 * @l The current locus
 * @unknown The transmitter with unknown location but known RSS
 * @rxers The locations of the receivers
 * @radial_alphas Estimates of alphas (in radial slices) for signal propagation, as seen by each receiver
 * @quantile Which quantile to use for the next locus. The upper (1.0 - quantile) points will be used.
 * @iterations The number of points to generate before picking the upper quantile and making the next locus.
 * @antenna_gain Evenly spaced estimations of the antenna gain about the receiver. First entry is for a rotation of 0 degrees.
 */
Locus getNextLocus(Locus& l, Sample& unknown, LandmarkData& rxers, vector<vector<AlphaEstimate>>& radial_alphas,
    double quantile, size_t iterations, vector<vector<double>> antenna_gain) {
  vector<PointProbability> point_probs;
  //Compute point probability taken across all of the receivers?
  std::cerr<<"Working with radius "<<l.radius<<'\n';
  vector<bool> good_rxers(rxers.size(), false);
  //For the radial locus search accept any receivers as long as they have a sample for this unknown
  {
    for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
      if (not (isnan(unknown.rssis[rxer]) or isnan(radial_alphas[rxer][0].mean))) {
        good_rxers[rxer] = true;
      }
    }
  }

  //If the radius is small and we are only using a couple of receivers the entire
  //locus area might have a low probability. In that case, re-adjust our parameters by increasing the variance.
  double var_coefficient = 0.0;
  //Find a reasonable var coefficient so that at least 10 percent of the values are non-zero
  const int burnin = 1000;
  size_t num_zeros = burnin;
  while (num_zeros > 0.9*burnin) {
    std::cerr<<"Checking for zeros...\n";
    num_zeros = burnin;
    var_coefficient += 1.0;
    for (size_t i = 0; i < burnin; ++i) {
      //Generate a random location around the locus
      pair<double, double> pxy = genXY(l.radius);
      double x = l.x + pxy.first;
      double y = l.y + pxy.second;
      long double cum_prob = 1.0;
      for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
        if (good_rxers[rxer]) {
          //Distance along x and y axes to get to the given location from the receiver
          double xdiff = x - rxers[rxer].first;
          double ydiff = y - rxers[rxer].second;
          //Get the angle, from -pi to pi
          double tx_angle = atan2( ydiff, xdiff);
          //Use the alpha value from this slice to estimate the probability of finding the transmitter here
          double dist = sqrt(pow(xdiff, 2) + pow(ydiff, 2));
          AlphaEstimate alpha = interpolateSlices(tx_angle, radial_alphas[rxer]);

          if (antenna_gain.size() > 0 /* and antenna_gain[rxer].size() > 0 */) {
            double thisprob = probSignalFromAlphas(unknown.rssis[rxer], dist, alpha,
                var_coefficient, interpolateSlices(tx_angle, antenna_gain[rxer]));
            cum_prob *= probSignalFromAlphas(unknown.rssis[rxer], dist, alpha,
                var_coefficient, interpolateSlices(tx_angle, antenna_gain[rxer]));
            /*
            cum_prob += log10(probSignalFromAlphas(unknown.rssis[rxer], dist, alpha,
                var_coefficient, interpolateSlices(tx_angle, antenna_gain[rxer])));
                */
          }
          else {
            cum_prob *= probSignalFromAlphas(unknown.rssis[rxer], dist, alpha, var_coefficient);
            //cum_prob += log10(probSignalFromAlphas(unknown.rssis[rxer], dist, alpha, var_coefficient));
          }
        }
      }
      if (cum_prob > 0.0) {
      //if (cum_prob > -1.0 * std::numeric_limits<double>::infinity())
        --num_zeros;
      }
    }
    std::cerr<<"\tFound "<<num_zeros<<" zeros.\n";
  }

  for (size_t i = 0; i < iterations; ++i) {
    //Generate a random location around the locus
    pair<double, double> pxy = genXY(l.radius);
    double x = l.x + pxy.first;
    double y = l.y + pxy.second;
    //For graphing
    //std::cerr<<"Using radius "<<l.radius<<"("<<x<<", "<<y<<")\n";
    long double cum_prob = 1.0;
    for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
      if (good_rxers[rxer]) {
        //Find the number of radians in each slice
        size_t num_slices = radial_alphas[rxer].size();
        double slice_size = 2*M_PI / num_slices;
        //Distance along x and y axes to get to the given location from the receiver
        double xdiff = x - rxers[rxer].first;
        double ydiff = y - rxers[rxer].second;
        //Get the angle, from -pi to pi
        double tx_angle = atan2( ydiff, xdiff);
        //Find which slice this is in
        int cur_slice = rint(tx_angle/slice_size);
        if (cur_slice < 0) {
          cur_slice = num_slices + cur_slice;
        }
        //Use the alpha value from this slice to estimate the probability of finding the transmitter here
        double dist = sqrt(pow(xdiff, 2) + pow(ydiff, 2));
        AlphaEstimate alpha = radial_alphas[rxer][cur_slice];
        //alpha = radial_alphas[rxer][0];

        if (antenna_gain.size() > 0) {
          cum_prob *= probSignalFromAlphas(unknown.rssis[rxer], dist, alpha,
              var_coefficient, interpolateSlices(tx_angle, antenna_gain[rxer]));
          /*
          cum_prob += log10(probSignalFromAlphas(unknown.rssis[rxer], dist, alpha,
              var_coefficient, interpolateSlices(tx_angle, antenna_gain[rxer])));
              */
        }
        else {
          cum_prob *= probSignalFromAlphas(unknown.rssis[rxer], dist, alpha, var_coefficient);
          //cum_prob += log10(probSignalFromAlphas(unknown.rssis[rxer], dist, alpha, var_coefficient));
        }
      }
    }
    point_probs.push_back(PointProbability{x, y, cum_prob});
    //For graphing
    //std::cerr<<"\tcum_prob is "<<cum_prob<<'\n';
  }
  //Sort the point probabilities and then find the center of the upper quantile's points.
  std::sort(point_probs.begin(), point_probs.end(),
      [](const PointProbability& a, const PointProbability& b) { return a.prob < b.prob;});
  size_t start = quantile * point_probs.size();
  //Only use positions where the probability was not zero
  while (start < point_probs.size() and point_probs[start].prob <= 0) {
    ++start;
  }
  //Calculate the biased center of the upper percentile.
  double total_prob = std::accumulate(point_probs.begin() + start, point_probs.end(), 0.0,
      [&](double a, PointProbability& b) { return a + b.prob;});
  double cent_x = std::accumulate(point_probs.begin() + start, point_probs.end(), 0.0,
      [&](double a, PointProbability& b) { return a + b.x*(b.prob/total_prob);});
  double cent_y = std::accumulate(point_probs.begin() + start, point_probs.end(), 0.0,
      [&](double a, PointProbability& b) { return a + b.y*(b.prob/total_prob);});
  //Make the radius the greatest distance from the center to a point
  //std::cerr<<"\nFinding radius for center at "<<cent_x<<", "<<cent_y<<'\n';
  double radius = std::accumulate(point_probs.begin() + start, point_probs.end(), 0.0,
      [&](double a, PointProbability& b) { double dist = sqrt(pow(b.x - cent_x, 2)+pow(b.y - cent_y, 2));
                                           /*if (dist > a) { std::cerr<<'\t'<<b.x<<", "<<b.y<<" now farthest (r = "<<dist<<")\n";}*/
                                           return std::max(dist, a); });
  return Locus{cent_x, cent_y, radius};
}

std::vector<CELocalize::Result> CELocalize::localize( TrainingData train_data,
    LandmarkData rx_locations,
    double region_width,
    double region_height,
    double resolution) {

  srandom(time(NULL));
  TrainingData tx_locations;
  TrainingData unknowns;
  for (auto I = train_data.begin(); I != train_data.end(); ++I) {
    if (isnan(I->x)) {
      unknowns.push_back(*I);
    }
    else {
      tx_locations.push_back(*I);
    }
  }

  //Estimate the alphas values for all of the receivers
  vector<AlphaEstimate> alphas = estimateAlphas(tx_locations, rx_locations);
  vector<vector<AlphaEstimate>> radial_alphas = estimateRadialAlphas(tx_locations, rx_locations);

  std::vector<Result> results;
  //Localize each unknown
  //Start from the center of the map.
  //Loop:
  //     Randomly generate an angle and a distance
  //     Check the probability of the observed RSS coming from that spot
  //     After some number of iterations (variance of the points stops changing) stop
  //     Sort the locations by their probabilities
  //     Restart the loop at the center of the upper percentile.
  //       The next radius is the maximum distance between any two points in the upper percentile.
  //       If the radius did not decrease by some amount then quit.
  //
  for (auto I = unknowns.begin(); I != unknowns.end(); ++I) {
    //Scorer scorer(*I, rx_locations, alphas, alphas, region_width, region_height, resolution);
    Scorer scorer(*I, rx_locations, alphas, radial_alphas, region_width, region_height, resolution);
    //vector<float> alpha_bits = scorer.makeInitialAlphaBits();
    vector<float> alpha_bits = scorer.makeInitialRadialAlphaBits();
    float min_improve = 2;
    //vector<CEIteration> cedata = runCE(100, 0.50, 1.0, min_improve, alpha_bits, [&](vector<bool> ab) { return scorer.scoreAlphas(ab);});
    //vector<CEIteration> cedata = runCE(100, 0.50, 1.0, min_improve, alpha_bits, [&](vector<bool> ab) { return scorer.scoreWithTrilateration(ab);});
    //vector<CEIteration> cedata = runCE(200, 0.20, 1.0, min_improve, alpha_bits, [&](vector<bool> ab) { return scorer.scoreWithRMSD(ab);});
    vector<CEIteration> cedata = runCE(2000, 0.90, 1.0, min_improve, alpha_bits, [&](vector<bool> ab) { return scorer.scoreWithRadialRMSD(ab);});
    //For testing
    //vector<CEIteration> cedata = runCE(10000, 0.00, 1.0, 300000, alpha_bits, [&](vector<bool> ab) { return scorer.scoreWithRMSD(ab);});
    //vector<CEIteration> cedata = runCE(10000, 0.00, 1.0, 300000, alpha_bits, [&](vector<bool> ab) { return scorer.scoreWithRadialRMSD(ab);});
    /*
    for (int i = 1; i < cedata.size(); ++i) {
      std::cout<<"Round "<<i<<" average quantile performance was "<<cedata[i].avg_quantile_performance<<'\n';
    }
    */
    std::cout<<"Finished in "<<cedata.size()<<" rounds with quantile performance "<<cedata.rbegin()->avg_quantile_performance<<'\n';
    std::cout<<"QuantVsErr "<<cedata.rbegin()->avg_quantile_performance<<" ";
    //Plot this with the command 
    //                     perl -ne 'if (/QuantVsErr (.*?) Location .*?Error (.*?)$/) { print "$1 $2\n"; }'

    //pair<double, double> p = scorer.getPoint(cedata.rbegin()->probability_vector, 5, 3000);
    //pair<double, double> p = scorer.getPointWithTrilateration(cedata.rbegin()->probability_vector, 5, 3000);
    //pair<double, double> p = scorer.getPointWithRMSD(cedata.rbegin()->probability_vector);
    pair<double, double> p = scorer.getPointWithRadialRMSD(cedata.rbegin()->probability_vector);
    results.push_back(Result{p.first, p.second});
  }
  return results;
}


std::vector<CELocalize::Result> CELocalize::bayesLocalize( TrainingData train_data,
    LandmarkData rx_locations,
    double region_width,
    double region_height,
    double resolution) {

  srandom(time(NULL));
  TrainingData tx_locations;
  TrainingData unknowns;
  for (auto I = train_data.begin(); I != train_data.end(); ++I) {
    if (isnan(I->x)) {
      unknowns.push_back(*I);
    }
    else {
      tx_locations.push_back(*I);
    }
  }

  //Estimate the alphas values for all of the receivers
  vector<AlphaEstimate> alphas = estimateAlphas(tx_locations, rx_locations);

  std::vector<Result> results;
  //Localize each unknown
  //Start from the center of the map.
  //Loop:
  //     Randomly generate an angle and a distance
  //     Check the probability of the observed RSS coming from that spot
  //     After some number of iterations (variance of the points stops changing) stop
  //     Sort the locations by their probabilities
  //     Restart the loop at the center of the upper percentile.
  //       The next radius is the maximum distance between any two points in the upper percentile.
  //       If the radius did not decrease by some amount then quit.
  //
  for (auto I = unknowns.begin(); I != unknowns.end(); ++I) {
    //Make a locus that covers at least twice the area of the region to dispel biases along the edges
    Locus next_locus{region_width/2.0, region_height/2.0, sqrt(2.0) * (region_width + region_height) / 2.0};
    Locus locus;
    //Keep generating new locuses until we improve by less than 5 percent
    do {
      std::cerr<<"New locus is at location "<<next_locus.x<<", "<<next_locus.y<<" with radius "<<next_locus.radius<<'\n';
      locus = next_locus;
      next_locus = getNextLocus(locus, *I, rx_locations, alphas, 0.990, 10000);
    } while (next_locus.radius < 0.95 * locus.radius and next_locus.radius > resolution);
    results.push_back(Result{next_locus.x, next_locus.y});
  }
  return results;
}

std::vector<CELocalize::Result> CELocalize::radialBayesLocalize( TrainingData train_data,
    LandmarkData rx_locations,
    double region_width,
    double region_height,
    double resolution,
    vector<vector<double>> antenna_gain) {

  srandom(time(NULL));
  //Split the training data into knowns and unknowns
  TrainingData tx_locations;
  TrainingData unknowns;
  for (auto I = train_data.begin(); I != train_data.end(); ++I) {
    if (isnan(I->x)) {
      unknowns.push_back(*I);
    }
    else {
      tx_locations.push_back(*I);
    }
  }
  std::cerr<<"First entry in gains is "<<antenna_gain[0].size()<<'\n';

  //Estimate the alphas values for all of the receivers
  vector<AlphaEstimate> alphas = estimateAlphas(tx_locations, rx_locations);
  //Use the given antenna gains during alpha estimation (if any antenna gains were given)
  //The estimateRadialAlphas function will ignore the given antenna gains if they cannot be used
  std::cerr<<"In the beginning the antenna gain had "<<antenna_gain.size()<<" entries.\n";
  vector<vector<AlphaEstimate>> radial_alphas = estimateRadialAlphas(tx_locations, rx_locations, antenna_gain);
  //vector<vector<AlphaEstimate>> radial_alphas = estimateQuadAlphas(tx_locations, rx_locations, antenna_gain);
  //TODO Testing non radial alphas with radial gains
  //FIXME If this is not good then remove it
  /*
  for (size_t rxer = 0; rxer < alphas.size(); ++rxer) {
    radial_alphas[rxer].clear();
    radial_alphas[rxer].push_back(alphas[rxer]);
  }
  */

  std::cerr<<"Plot these to see the antenna shapes\n";
  double slice_increment = 2*M_PI / radial_alphas[0].size();
  std::cerr<<"The slice increment is "<<slice_increment<<'\n';
  std::cerr<<"Each radial estimate has "<<radial_alphas[0].size()<<" slices.\n";
  for (size_t rxer = 0; rxer < radial_alphas.size(); ++rxer) {
    std::cerr<<"\nrxer\t"<<rxer<<'\n';
    for (int slice = 0; slice < radial_alphas[rxer].size(); ++slice) {
      //TODO Print out an x and y location for easy plotting
      double angle = slice * slice_increment;
      double rad = 1.0 * radial_alphas[rxer][slice].mean;
      double x = cos(angle)*rad;
      double y = sin(angle)*rad;
      std::cerr<<slice * slice_increment <<'\t'<<x<<'\t'<<y<<'\t'<<
        radial_alphas[rxer][slice].mean<<'\t'<<radial_alphas[rxer][slice].variance<<'\n';
    }
  }
  std::cerr<<'\n';

  std::vector<Result> results;
  //Localize each unknown
  //Start from the center of the map.
  //Loop:
  //     Randomly generate an angle and a distance
  //     Check the probability of the observed RSS coming from that spot
  //     After some number of iterations (variance of the points stops changing) stop
  //     Sort the locations by their probabilities
  //     Restart the loop at the center of the upper percentile.
  //       The next radius is the maximum distance between any two points in the upper percentile.
  //       If the radius did not decrease by some amount then quit.
  //
  for (auto I = unknowns.begin(); I != unknowns.end(); ++I) {
    LandmarkData cur_rx_locations = rx_locations;
    vector<vector<double>> cur_gains = antenna_gain;

    //Get rid of any receivers that don't have any data
    for (ptrdiff_t rxer = rx_locations.size() - 1; rxer >= 0; --rxer) {
      if ((isnan(I->rssis[rxer]) or isnan(radial_alphas[rxer][0].mean))) {
        //Remove this receiver and all of its samples
        I->rssis.erase(I->rssis.begin()+rxer);
        radial_alphas.erase(radial_alphas.begin()+rxer);
        cur_rx_locations.erase(cur_rx_locations.begin()+rxer);
        cur_gains.erase(cur_gains.begin()+rxer);
      }
    }

    //Make a locus that covers at least twice the area of the region to dispel biases along the edges
    Locus next_locus{region_width/2.0, region_height/2.0, sqrt(2.0) * (1.1 * region_width + region_height) / 2.0};
    Locus locus;
    //Keep generating new locuses until we improve by less than 5 percent
    do {
      std::cerr<<"New locus is at location "<<next_locus.x<<", "<<next_locus.y<<" with radius "<<next_locus.radius<<'\n';
      locus = next_locus;
      double quantile = 0.990;
      size_t iterations = 10000;
      next_locus = getNextLocus(locus, *I, cur_rx_locations, radial_alphas, quantile, iterations, cur_gains);
    } while (next_locus.radius < 0.95 * locus.radius and next_locus.radius > resolution);
    results.push_back(Result{next_locus.x, next_locus.y});
  }
  return results;
}

vector<double> calculateProbabilityVector(double x, double y, Sample& unknown,
    LandmarkData& rxers, vector<bool>& good_rxers,
    vector<vector<double>>& antenna_gain, vector<vector<AlphaEstimate>>& radial_alphas) {
  vector<double> prob_vector;
  //TODO This should probably increase if we are having trouble getting a value
  double var_coefficient = 1.0;
  for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
    if (good_rxers[rxer]) {
      //Find the number of radians in each slice
      size_t num_slices = radial_alphas[rxer].size();
      double slice_size = 2*M_PI / num_slices;
      //Distance along x and y axes to get to the given location from the receiver
      double xdiff = x - rxers[rxer].first;
      double ydiff = y - rxers[rxer].second;
      //Get the angle, from -pi to pi
      double tx_angle = atan2( ydiff, xdiff);
      //Find which slice this is in
      int cur_slice = rint(tx_angle/slice_size);
      if (cur_slice < 0) {
        cur_slice = num_slices + cur_slice;
      }
      //Use the alpha value from this slice to estimate the probability of finding the transmitter here
      double dist = sqrt(pow(xdiff, 2) + pow(ydiff, 2));
      AlphaEstimate alpha = radial_alphas[rxer][cur_slice];

      if (antenna_gain.size() > 0) {
        prob_vector.push_back(probSignalFromAlphas(unknown.rssis[rxer], dist, alpha,
            var_coefficient, interpolateSlices(tx_angle, antenna_gain[rxer])));
      }
      else {
        prob_vector.push_back(probSignalFromAlphas(unknown.rssis[rxer], dist, alpha, var_coefficient));
      }
    }
  }
  return prob_vector;
}

double calculateProbability(double x, double y, Sample& unknown,
    LandmarkData& rxers, vector<bool>& good_rxers,
    vector<vector<double>>& antenna_gain, vector<vector<AlphaEstimate>>& radial_alphas) {
  double cum_prob = 1.0;
  //TODO This should probably increase if we are having trouble getting a value
  double var_coefficient = 1.0;
  for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
    if (good_rxers[rxer]) {
      //Find the number of radians in each slice
      size_t num_slices = radial_alphas[rxer].size();
      double slice_size = 2*M_PI / num_slices;
      //Distance along x and y axes to get to the given location from the receiver
      double xdiff = x - rxers[rxer].first;
      double ydiff = y - rxers[rxer].second;
      //Get the angle, from -pi to pi
      double tx_angle = atan2( ydiff, xdiff);
      //Find which slice this is in
      int cur_slice = rint(tx_angle/slice_size);
      if (cur_slice < 0) {
        cur_slice = num_slices + cur_slice;
      }
      //Use the alpha value from this slice to estimate the probability of finding the transmitter here
      double dist = sqrt(pow(xdiff, 2) + pow(ydiff, 2));
      AlphaEstimate alpha = radial_alphas[rxer][cur_slice];

      if (antenna_gain.size() > 0) {
        cum_prob *= probSignalFromAlphas(unknown.rssis[rxer], dist, alpha,
            var_coefficient, interpolateSlices(tx_angle, antenna_gain[rxer]));
      }
      else {
        cum_prob *= probSignalFromAlphas(unknown.rssis[rxer], dist, alpha, var_coefficient);
      }
    }
  }
  return cum_prob;
}

/*
 * Compute the probability density
 */
double calculateProbabilityDensity(double x, double y, Sample& unknown,
    LandmarkData& rxers, vector<bool>& good_rxers,
    vector<vector<double>>& antenna_gain, vector<vector<AlphaEstimate>>& radial_alphas) {
  double cum_prob = 1.0;
  for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
    if (good_rxers[rxer]) {
      //Find the number of radians in each slice
      size_t num_slices = radial_alphas[rxer].size();
      double slice_size = 2*M_PI / num_slices;
      //Distance along x and y axes to get to the given location from the receiver
      double xdiff = x - rxers[rxer].first;
      double ydiff = y - rxers[rxer].second;
      //Get the angle, from -pi to pi
      double tx_angle = atan2( ydiff, xdiff);
      //Find which slice this is in
      int cur_slice = rint(tx_angle/slice_size);
      if (cur_slice < 0) {
        cur_slice = num_slices + cur_slice;
      }
      //Use the alpha value from this slice to estimate the probability of finding the transmitter here
      double dist = sqrt(pow(xdiff, 2) + pow(ydiff, 2));
      AlphaEstimate alpha = interpolateSlices(tx_angle, radial_alphas[rxer]);

      if (antenna_gain.size() > 0) {
        cum_prob *= probSignalFromAlphas(unknown.rssis[rxer], dist, alpha,
            alpha.variance, interpolateSlices(tx_angle, antenna_gain[rxer]));
      }
      else {
        cum_prob *= probSignalFromAlphas(unknown.rssis[rxer], dist, alpha, alpha.variance);
      }
    }
  }
  return cum_prob;
}

/* Ease of use fingerprint class that makes it easy to
 * do receiver voting between different locations.
 */
struct Fingerprint {
  vector<double> probabilities;
  double x;
  double y;
  double probability;
  Fingerprint() : probabilities() {
    x = -1;
    y = -1;
  }
  Fingerprint(double x, double y, Sample& unknown, LandmarkData& rxers,
              vector<bool>& good_rxers, vector<vector<double>>& antenna_gain,
              vector<vector<AlphaEstimate>>& radial_alphas) {
    this->x = x;
    this->y = y;
    //Compute the probability of the unknown being at x,y for each receiver.
    for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
      probabilities = calculateProbabilityVector(x, y, unknown, rxers, good_rxers, antenna_gain, radial_alphas);
    }
    probability = accumulate(probabilities.begin(), probabilities.end(), 1.0,
        [](double a, double b) { return a*b;});
  }

  bool operator>(Fingerprint& other) {
    //This fingprint is better than the other if the other is empty
    if (other.probabilities.empty()) { return true;}
    //Otherwise see if more receivers favor this one over the other
    size_t preferals = inner_product(probabilities.begin(), probabilities.end(),
        other.probabilities.begin(), 0, [](size_t a, bool b) {return a + (b ? 1 : 0);},
        [](double a, double b) { return a > b;});
    return preferals > probabilities.size()/2;
  }
};

/**
 * bayesWalkVote - Rather than probability use voting between receivers to
 *                 compare two possible locations
 * @unknown The transmitter with unknown location but known RSS
 * @rxers The locations of the receivers
 * @radial_alphas Estimates of alphas (in radial slices) for signal propagation, as seen by each receiver
 * @iterations The number of points to generate before picking the upper quantile and returning a locus
 * @antenna_gain Evenly spaced estimations of the antenna gain about the receiver. First entry is for a rotation of 0 degrees.
 * @region_width Region width
 * @region_height Region height
 */
Locus bayesWalkVote(Sample& unknown, LandmarkData& rxers, vector<vector<AlphaEstimate>>& radial_alphas,
    size_t iterations, vector<vector<double>> antenna_gain, double region_width, double region_height) {

  //These variables could be turned into arguments
  size_t burnin = 100;
  //Upper quantile of points to make a centroid out of
  double quantile = 0.99;
  //Convergence variables - smaller numbers converge faster
  double converge = 0.99;
  //The number of tries taken to try to exceed a slice probability
  size_t max_tries = 2000;

  //Prune out useless receivers.
  vector<bool> good_rxers(rxers.size(), false);
  //For the radial locus search accept any receivers as long as they have a sample for this unknown
  {
    for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
      if (not (isnan(unknown.rssis[rxer]) or isnan(radial_alphas[rxer][0].mean))) {
        good_rxers[rxer] = true;
      }
    }
  }

  vector<Fingerprint> point_prints;
  //Start at the center
  double cur_x = region_width / 2.0;
  double cur_y = region_height / 2.0;
  //Which axis to walk in
  bool walk_x = true;

  //Use the mersenne twister for the random number generator
  std::random_device rd;
  std::mt19937_64 eng(rd());
  //Default normal distribution with mean 0 and variance 1.
  std::normal_distribution<> norm;
  //Uniform distributions for the x and y axes
  std::uniform_real_distribution<> x_uniform(0, region_width);
  std::uniform_real_distribution<> y_uniform(0, region_height);
  Fingerprint best_print;
  for (size_t i = 0; i < iterations+burnin; ++i) {
    //Walk in the x-axis
    double x = cur_x;
    double y = cur_y;
    //Sample across this axis to obtain a basis for comparison
    Fingerprint sample_best;
    size_t slice_size = 5;
    for (size_t sample = 0; sample < slice_size; ++sample) {
      if (walk_x) x = x_uniform(eng);
      else y = y_uniform(eng);
      Fingerprint sample_print(x, y, unknown, rxers, good_rxers, antenna_gain, radial_alphas);
      if (sample_print > sample_best) {
        sample_best = sample_print;
      }
    }
    //Generate and reject x locations until one is found with a higher probability
    Fingerprint try_print = sample_best;
    size_t tries = 0;
    while (not (try_print > sample_best) and tries < max_tries) {
      if (walk_x) x = x_uniform(eng);
      else y = y_uniform(eng);
      try_print = Fingerprint(x, y, unknown, rxers, good_rxers, antenna_gain, radial_alphas);
      ++tries;
    }
    if (tries >= max_tries) {
      x = sample_best.x;
      y = sample_best.y;
      try_print = sample_best;
    }
    //Store the print
    if (i >= burnin) {
      ////For graphing
      //std::cerr<<"Walking to ("<<try_print.x<<", "<<try_print.y<<")\n";
      point_prints.push_back(try_print);
    }
    else {
      //For graphing
      //std::cerr<<"Walking to ("<<try_print.x<<", "<<try_print.y<<")\n";
    }

    //Move to the next location.
    //Also decrease our search distance in this axis to force convergence
    double left = converge;
    double right = 1.0 - converge;
    if (walk_x) {
      cur_x = x;
      if (try_print > best_print) x_uniform = std::uniform_real_distribution<>(left*x_uniform.min() + right*x, left*x_uniform.max() + right*x);
    }
    else {
      cur_y = y;
      if (try_print > best_print) y_uniform = std::uniform_real_distribution<>(left*y_uniform.min() + right*y, left*y_uniform.max() + right*y);
    }
    if (try_print > best_print) best_print = try_print;
    //Walk in the other axis for the next iteration
    walk_x = not walk_x;
  }
  //Triangle inequality doesn't hold for fingerprints so we must sort using probabilities
  /*
  std::sort(point_prints.begin(), point_prints.end(),
      [](const Fingerprint& a, const Fingerprint& b) { return a.probability < b.probability;});
      */
  size_t start = quantile * point_prints.size();
  //Calculate the biased center of the upper percentile.
  auto begin = point_prints.begin() + start;
  double total_prints = distance(begin, point_prints.end());
  double cent_x = std::accumulate(begin, point_prints.end(), 0.0,
      [&](double a, Fingerprint& b) { return a + b.x/total_prints;});
  double cent_y = std::accumulate(begin, point_prints.end(), 0.0,
      [&](double a, Fingerprint& b) { return a + b.y/total_prints;});
  //Make the radius the greatest distance from the center to a point
  std::cerr<<"\nFinding radius for center at "<<cent_x<<", "<<cent_y<<'\n';
  double radius = std::accumulate(begin, point_prints.end(), 0.0,
      [&](double a, Fingerprint& b) { double dist = sqrt(pow(b.x - cent_x, 2)+pow(b.y - cent_y, 2));
                                           if (dist > a) { std::cerr<<'\t'<<b.x<<", "<<b.y<<" now farthest (r = "<<dist<<")\n";}
                                           return std::max(dist, a); });
  return Locus{cent_x, cent_y, radius};
}

/**
 * bayesWalk - Walk from point to point, alternating motion in the x and y axes.
 * @unknown The transmitter with unknown location but known RSS
 * @rxers The locations of the receivers
 * @radial_alphas Estimates of alphas (in radial slices) for signal propagation, as seen by each receiver
 * @iterations The number of points to generate before picking the upper quantile and returning a locus
 * @antenna_gain Evenly spaced estimations of the antenna gain about the receiver. First entry is for a rotation of 0 degrees.
 * @region_width Region width
 * @region_height Region height
 */
Locus bayesWalk(Sample& unknown, LandmarkData& rxers, vector<vector<AlphaEstimate>>& radial_alphas,
    size_t iterations, vector<vector<double>> antenna_gain, double region_width, double region_height) {

  //These variables could be turned into arguments
  size_t burnin = 100;
  //Upper quantile of points to make a centroid out of
  double quantile = 0.99;
  //Convergence variables - smaller numbers converge faster
  double converge = 0.99;
  //The number of tries taken to try to exceed a slice probability
  size_t max_tries = 2000;

  //Prune out useless receivers.
  vector<bool> good_rxers(rxers.size(), false);
  //For the radial locus search accept any receivers as long as they have a sample for this unknown
  {
    for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
      if (not (isnan(unknown.rssis[rxer]) or isnan(radial_alphas[rxer][0].mean))) {
        good_rxers[rxer] = true;
      }
    }
  }

  vector<PointProbability> point_probs;
  //Start at the center
  double cur_x = region_width / 2.0;
  double cur_y = region_height / 2.0;
  //Which axis to walk in
  bool walk_x = true;

  //Use the mersenne twister for the random number generator
  std::random_device rd;
  std::mt19937_64 eng(rd());
  //Default normal distribution with mean 0 and variance 1.
  std::normal_distribution<> norm;
  //Uniform distributions for the x and y axes
  std::uniform_real_distribution<> x_uniform(0, region_width);
  std::uniform_real_distribution<> y_uniform(0, region_height);
  double best_prob = 0;
  for (size_t i = 0; i < iterations+burnin; ++i) {
    //Walk in the x-axis
    double x = cur_x;
    double y = cur_y;
    double max_x = x;
    double max_y = y;
    //Sample across this axis to obtain a basis for comparison
    double sample_max = 0.0;
    size_t slice_size = 5;
    for (size_t sample = 0; sample < slice_size; ++sample) {
      if (walk_x) x = x_uniform(eng);
      else y = y_uniform(eng);
      double sample_prob = calculateProbability(x, y, unknown, rxers, good_rxers, antenna_gain, radial_alphas);
      if (sample_prob > sample_max) {
        sample_max = sample_prob;
        max_x = x;
        max_y = y;
      }
    }
    //Generate and reject x locations until one is found with a higher probability
    double cum_prob = calculateProbability(x, y, unknown, rxers, good_rxers, antenna_gain, radial_alphas);
    size_t tries = 0;
    while (cum_prob < sample_max and tries < max_tries) {
      if (walk_x) x = x_uniform(eng);
      else y = y_uniform(eng);
      cum_prob = calculateProbability(x, y, unknown, rxers, good_rxers, antenna_gain, radial_alphas);
    }
    if (tries >= max_tries) {
      x = max_x;
      y = max_y;
      cum_prob = sample_max;
    }
    //Store the location and probability if we are past the burn in phase.
    if (i >= burnin) {
      ////For graphing
      //std::cerr<<"Walking to ("<<x<<", "<<y<<")\n";
      point_probs.push_back(PointProbability{x, y, cum_prob});
    }
    else {
      //For graphing
      //std::cerr<<"Walking to ("<<x<<", "<<y<<")\n";
    }

    //Move to the next location.
    //Also decrease our search distance in this axis to force convergence
    double left = converge;
    double right = 1.0 - converge;
    if (walk_x) {
      cur_x = x;
      if (cum_prob > best_prob) x_uniform = std::uniform_real_distribution<>(left*x_uniform.min() + right*x, left*x_uniform.max() + right*x);
    }
    else {
      cur_y = y;
      if (cum_prob > best_prob) y_uniform = std::uniform_real_distribution<>(left*y_uniform.min() + right*y, left*y_uniform.max() + right*y);
    }
    if (cum_prob > best_prob) best_prob = cum_prob;
    //Walk in the other axis for the next iteration
    walk_x = not walk_x;
  }
  //Sort the point probabilities and then find the center of the upper quantile's points.
  std::sort(point_probs.begin(), point_probs.end(),
      [](const PointProbability& a, const PointProbability& b) { return a.prob < b.prob;});
  size_t start = quantile * point_probs.size();
  //Only use positions where the probability was not zero
  while (start < point_probs.size() and point_probs[start].prob <= 0) {
    ++start;
  }
  //Calculate the biased center of the upper percentile.
  double total_prob = std::accumulate(point_probs.begin() + start, point_probs.end(), 0.0,
      [&](double a, PointProbability& b) { return a + b.prob;});
  double cent_x = std::accumulate(point_probs.begin() + start, point_probs.end(), 0.0,
      [&](double a, PointProbability& b) { return a + b.x*(b.prob/total_prob);});
  double cent_y = std::accumulate(point_probs.begin() + start, point_probs.end(), 0.0,
      [&](double a, PointProbability& b) { return a + b.y*(b.prob/total_prob);});
  //Make the radius the greatest distance from the center to a point
  std::cerr<<"\nFinding radius for center at "<<cent_x<<", "<<cent_y<<'\n';
  double radius = std::accumulate(point_probs.begin() + start, point_probs.end(), 0.0,
      [&](double a, PointProbability& b) { double dist = sqrt(pow(b.x - cent_x, 2)+pow(b.y - cent_y, 2));
                                           if (dist > a) { std::cerr<<'\t'<<b.x<<", "<<b.y<<" now farthest (r = "<<dist<<")\n";}
                                           return std::max(dist, a); });
  return Locus{cent_x, cent_y, radius};
}

std::vector<CELocalize::Result> CELocalize::bayesWalkLocalize( TrainingData train_data,
    LandmarkData rx_locations,
    double region_width,
    double region_height,
    double resolution,
    vector<vector<double>> antenna_gain) {

  srandom(time(NULL));
  //Split the training data into knowns and unknowns
  TrainingData tx_locations;
  TrainingData unknowns;
  for (auto I = train_data.begin(); I != train_data.end(); ++I) {
    if (isnan(I->x)) {
      unknowns.push_back(*I);
    }
    else {
      tx_locations.push_back(*I);
    }
  }
  std::cerr<<"First entry in gains is "<<antenna_gain[0].size()<<'\n';

  //Estimate the alphas values for all of the receivers
  vector<AlphaEstimate> alphas = estimateAlphas(tx_locations, rx_locations);
  //Use the given antenna gains during alpha estimation (if any antenna gains were given)
  //The estimateRadialAlphas function will ignore the given antenna gains if they cannot be used
  std::cerr<<"In the beginning the antenna gain had "<<antenna_gain.size()<<" entries.\n";
  vector<vector<AlphaEstimate>> radial_alphas = estimateRadialAlphas(tx_locations, rx_locations, antenna_gain);
  //vector<vector<AlphaEstimate>> radial_alphas = estimateQuadAlphas(tx_locations, rx_locations, antenna_gain);

  std::cerr<<"Plot these to see the antenna shapes\n";
  double slice_increment = 2*M_PI / radial_alphas[0].size();
  std::cerr<<"The slice increment is "<<slice_increment<<'\n';
  std::cerr<<"Each radial estimate has "<<radial_alphas[0].size()<<" slices.\n";
  for (size_t rxer = 0; rxer < radial_alphas.size(); ++rxer) {
    std::cerr<<"\nrxer\t"<<rxer<<'\n';
    for (int slice = 0; slice < radial_alphas[rxer].size(); ++slice) {
      //TODO Print out an x and y location for easy plotting
      double angle = slice * slice_increment;
      double rad = 1.0 * radial_alphas[rxer][slice].mean;
      double x = cos(angle)*rad;
      double y = sin(angle)*rad;
      std::cerr<<slice * slice_increment <<'\t'<<x<<'\t'<<y<<'\t'<<
        radial_alphas[rxer][slice].mean<<'\t'<<radial_alphas[rxer][slice].variance<<'\n';
    }
  }
  std::cerr<<'\n';

  std::vector<Result> results;
  //Localize each unknown
  //Start from the center of the map.
  //Loop:
  //     Randomly generate an angle and a distance
  //     Check the probability of the observed RSS coming from that spot
  //     After some number of iterations (variance of the points stops changing) stop
  //     Sort the locations by their probabilities
  //     Restart the loop at the center of the upper percentile.
  //       The next radius is the maximum distance between any two points in the upper percentile.
  //       If the radius did not decrease by some amount then quit.
  //
  for (auto I = unknowns.begin(); I != unknowns.end(); ++I) {
    LandmarkData cur_rx_locations = rx_locations;
    vector<vector<double>> cur_gains = antenna_gain;

    //Get rid of any receivers that don't have any data
    for (ptrdiff_t rxer = rx_locations.size() - 1; rxer >= 0; --rxer) {
      if ((isnan(I->rssis[rxer]) or isnan(radial_alphas[rxer][0].mean))) {
        //Remove this receiver and all of its samples
        I->rssis.erase(I->rssis.begin()+rxer);
        radial_alphas.erase(radial_alphas.begin()+rxer);
        cur_rx_locations.erase(cur_rx_locations.begin()+rxer);
        cur_gains.erase(cur_gains.begin()+rxer);
      }
    }

    size_t iterations = 5000;
    Locus next_locus = bayesWalk(*I, cur_rx_locations, radial_alphas, iterations, cur_gains, region_width, region_height);
    std::cerr<<"New locus is at location "<<next_locus.x<<", "<<next_locus.y<<" with radius "<<next_locus.radius<<'\n';
    results.push_back(Result{next_locus.x, next_locus.y});
  }
  return results;
}

std::vector<CELocalize::Result> CELocalize::bayesWalkVoteLocalize( TrainingData train_data,
    LandmarkData rx_locations,
    double region_width,
    double region_height,
    double resolution,
    vector<vector<double>> antenna_gain) {

  srandom(time(NULL));
  //Split the training data into knowns and unknowns
  TrainingData tx_locations;
  TrainingData unknowns;
  for (auto I = train_data.begin(); I != train_data.end(); ++I) {
    if (isnan(I->x)) {
      unknowns.push_back(*I);
    }
    else {
      tx_locations.push_back(*I);
    }
  }
  std::cerr<<"First entry in gains is "<<antenna_gain[0].size()<<'\n';

  //Estimate the alphas values for all of the receivers
  vector<AlphaEstimate> alphas = estimateAlphas(tx_locations, rx_locations);
  //Use the given antenna gains during alpha estimation (if any antenna gains were given)
  //The estimateRadialAlphas function will ignore the given antenna gains if they cannot be used
  std::cerr<<"In the beginning the antenna gain had "<<antenna_gain.size()<<" entries.\n";
  vector<vector<AlphaEstimate>> radial_alphas = estimateRadialAlphas(tx_locations, rx_locations, antenna_gain);
  //vector<vector<AlphaEstimate>> radial_alphas = estimateQuadAlphas(tx_locations, rx_locations, antenna_gain);

  std::cerr<<"Plot these to see the antenna shapes\n";
  double slice_increment = 2*M_PI / radial_alphas[0].size();
  std::cerr<<"The slice increment is "<<slice_increment<<'\n';
  std::cerr<<"Each radial estimate has "<<radial_alphas[0].size()<<" slices.\n";
  for (size_t rxer = 0; rxer < radial_alphas.size(); ++rxer) {
    std::cerr<<"\nrxer\t"<<rxer<<'\n';
    for (int slice = 0; slice < radial_alphas[rxer].size(); ++slice) {
      //TODO Print out an x and y location for easy plotting
      double angle = slice * slice_increment;
      double rad = 1.0 * radial_alphas[rxer][slice].mean;
      double x = cos(angle)*rad;
      double y = sin(angle)*rad;
      std::cerr<<slice * slice_increment <<'\t'<<x<<'\t'<<y<<'\t'<<
        radial_alphas[rxer][slice].mean<<'\t'<<radial_alphas[rxer][slice].variance<<'\n';
    }
  }
  std::cerr<<'\n';

  std::vector<Result> results;
  //Localize each unknown
  //Start from the center of the map.
  //Loop:
  //     Randomly generate an angle and a distance
  //     Check the probability of the observed RSS coming from that spot
  //     After some number of iterations (variance of the points stops changing) stop
  //     Sort the locations by their probabilities
  //     Restart the loop at the center of the upper percentile.
  //       The next radius is the maximum distance between any two points in the upper percentile.
  //       If the radius did not decrease by some amount then quit.
  //
  for (auto I = unknowns.begin(); I != unknowns.end(); ++I) {
    LandmarkData cur_rx_locations = rx_locations;
    vector<vector<double>> cur_gains = antenna_gain;

    //Get rid of any receivers that don't have any data
    for (ptrdiff_t rxer = rx_locations.size() - 1; rxer >= 0; --rxer) {
      if ((isnan(I->rssis[rxer]) or isnan(radial_alphas[rxer][0].mean))) {
        //Remove this receiver and all of its samples
        I->rssis.erase(I->rssis.begin()+rxer);
        radial_alphas.erase(radial_alphas.begin()+rxer);
        cur_rx_locations.erase(cur_rx_locations.begin()+rxer);
        cur_gains.erase(cur_gains.begin()+rxer);
      }
    }

    size_t iterations = 5000;
    Locus next_locus = bayesWalkVote(*I, cur_rx_locations, radial_alphas, iterations, cur_gains, region_width, region_height);
    std::cerr<<"New locus is at location "<<next_locus.x<<", "<<next_locus.y<<" with radius "<<next_locus.radius<<'\n';
    results.push_back(Result{next_locus.x, next_locus.y});
  }
  return results;
}

//Helpful struct when keeping tracking of a value at some location.
struct ValPoint {
  double val;
  double x;
  double y;
};

std::ostream& operator<<(std::ostream& os, const ValPoint& vp) {
  return os <<"("<<vp.x<<", "<<vp.y<<") -> "<<vp.val<<'\n';
}

//Sum of the information in each entry, where information is
//p(x) * log(p(x))
double getShannonEntropy(vector<ValPoint>& vals) {
  double normalize = std::accumulate(vals.begin(), vals.end(), 0.0,
      [&](double a, ValPoint& b) { return a + b.val;});
  return -1.0 * std::accumulate(vals.begin(), vals.end(), 0.0,
      [&](double a, const ValPoint& vp){ return a + (vp.val/normalize) * log(vp.val/normalize);});
}

double getJensonShannonDivergence(vector<vector<ValPoint>>& points) {
  //H(Sum of weighted distributions) - (sum of weighted H(P))
  //Make the joint distribution
  vector<ValPoint> joint;
  if (points.size() > 0) {
    for (size_t i = 0; i < points[0].size(); ++i) {
      double sum = 0.0;
      for (size_t j = 0; j < points.size(); ++j) {
        sum += points[j][i].val;
      }
      joint.push_back(ValPoint{sum, 0.0, 0.0});
    }
  }
  return getShannonEntropy(joint) - std::accumulate(points.begin(), points.end(), 0.0,
      [&](double a, vector<ValPoint>& vals) { return a + getShannonEntropy(vals);});
}

///This function assumes that each receiver in @rxers has data and alpha values.
//The return value lists the estimated location of the transmitter as well as a
//confidence metric.
ValPoint abp_localize(Sample& unknown,
    LandmarkData& rxers,
    vector<vector<AlphaEstimate>>& radial_alphas,
    vector<vector<double>> antenna_gain,
    double region_width,
    double region_height,
    double resolution) {
  size_t total_points = (1 + region_width / resolution) * (1 + region_height / resolution);
  //Record the strongest receiver - assume that it has a line of sight path.
  size_t strongest_rxer = 0;
  for (size_t rxer = 1; rxer < rxers.size(); ++rxer) {
    if (unknown.rssis[rxer] > unknown.rssis[strongest_rxer]) {
      strongest_rxer = rxer;
    }
  }
  //Find the confidence coefficients necessary to include each point
  //in a confidence region of each receiver
  vector<vector<ValPoint>> origin_conf;
  for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
    vector<ValPoint> rxer_conf_points;
    for (double x = 0.0; x <= region_width; x+= resolution) {
      for (double y = 0.0; y <= region_height; y += resolution) {
        //Distance along x and y axes to get to the given location from the receiver
        double xdiff = x - rxers[rxer].first;
        double ydiff = y - rxers[rxer].second;
        //Get the angle, from -pi to pi
        double tx_angle = atan2( ydiff, xdiff);
        //Estimate the gain and alpha value at this position
        AlphaEstimate alpha = interpolateSlices(tx_angle, radial_alphas[rxer]);
        double gain = interpolateSlices(tx_angle, antenna_gain[rxer]);
        //Find the distance from the receiver to this point
        double point_dist = sqrt(pow(xdiff, 2) + pow(ydiff, 2));
        double manhatten_dist = fabs(xdiff) + fabs(ydiff);
        //Find the smallest confidence interval where this point is included.
        double conf_coefficient = getMinConfidenceCoefficient(point_dist, unknown.rssis[rxer], alpha, gain);
        //double manhatten_coef = getMinConfidenceCoefficient(manhatten_dist, unknown.rssis[rxer], alpha, gain);
        //See if a line of sight path or an approximated reflected path is more likely.
        //If this is the strongest receiver then assume it has a line of sight path.
        //if (rxer == strongest_rxer or conf_coefficient < manhatten_coef) {
          rxer_conf_points.push_back(ValPoint{conf_coefficient, x, y});
        //}
        //else {
          //rxer_conf_points.push_back(ValPoint{manhatten_coef, x, y});
        //}
        //std::cerr<<"rxer "<<rxer<<" makes conf point at "<<x<<"(, "<<y<<") with confidence "<<conf_coefficient<<'\n';
        //Later on we will calculate the confidence level associated with this
        //confidence coefficient. However, we need the total number of points
        //inside of each confidence region first so we will defer this.
      }
    }
    //Store the points
    origin_conf.push_back(rxer_conf_points);
  }
  //Sorting the points here allows us to easily count how many points
  //are included in each confidence interval later on during the
  //normalization step
  for (auto points = origin_conf.begin(); points != origin_conf.end(); ++points) {
    sort(points->begin(), points->end(),
        [](const ValPoint& a, const ValPoint& b) { return a.val < b.val;});
  }
  //Now we can find the probability of each point being the origin of
  //the given signal.
  //The probability of a signal coming from a point will be the
  //minimum confidence level necessary to include the point normalized
  //by the number of points that confidence level contains.
  //Make a 2D vector to store these values as we accumulate them
  //(we assume each receiver is independant so we can just multiply
  // the probabilities) as we solve for probabilities out of order
  const double sqrt2 = sqrt(2.0);
  map<pair<double, double>, double> origin_probs;
  //The same as origin probs but takes into account the percentages of the confidence intervals
  map<pair<double, double>, double> conf_probs;
  for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
    //TODO Use an index rather than an iterator so that finding the number less
    //than this one is easy
    auto conf = origin_conf.begin()+rxer;
    vector<double> leqs;
    for (auto point = conf->begin(); point != conf->end(); ++point) {
      //std::cerr<<"Expanding point "<<point->x<<", "<<point->y<<" ("<<point->val<<")\n";
      //Calculate the confidence coefficient needed to include this point
      //From Chebyshev's inequality the proportion of points included within
      //some number of standard deviations, k, is at least (1 / (1 + k**2))
      //If the data is distributed normally then that proportion is
      //approximately erf(k / sqrt(2))
      //First find out how much we can expand the confidence interval
      vector<ValPoint>::iterator up = find_if(point, conf->end(),
          [&](const ValPoint& a)->bool { return a.val > point->val;});
      //We can increase our confidence area without including any of the
      //lower confidence points by going to up->val. If all other points
      //work at a lower confidence interval than this one then this point
      //lies in the 100% confidence region (ie, all points)
      double max_conf = 1.0;
      if (up != conf->end()) {
        max_conf = up->val;
      }
      //Number of points less than or equal to this one (including this point)
      size_t num_leq = distance(conf->begin(), up);
      double conf_level = erf(max_conf / sqrt2);
      //std::cerr<<"Point has confidence and num_leq "<<conf_level<<" and "<<num_leq<<'\n';
      //To normalize we need to divide by num_leq
      pair<double, double> coordinate = make_pair(point->x, point->y);
      if (origin_probs.find(coordinate) == origin_probs.end()) {
        origin_probs[coordinate] = 1.0;
      }
      //origin_probs[make_pair(point->x, point->y)] *= (conf_level / num_leq);
      origin_probs[make_pair(point->x, point->y)] *= (1.0 / num_leq);
      if (conf_probs.find(coordinate) == conf_probs.end()) {
        conf_probs[coordinate] = 1.0;
      }
      conf_probs[make_pair(point->x, point->y)] *= (conf_level / num_leq);
      leqs.push_back(1.0 / num_leq);
      //std::cerr<<"Prob at point "<<point->x<<", "<<point->y<<" is now "<<origin_probs[coordinate]<<'\n';
    }
  }
  //Now do we go for minimum RMSE by finding the centroid of the upper
  //few points, or should we just pick the highest probability point?
  vector<ValPoint> final_probs(origin_probs.size());
  transform(origin_probs.begin(), origin_probs.end(), final_probs.begin(),
      [](const pair<pair<double, double>, double>& a)->ValPoint {
      return ValPoint{a.second, a.first.first, a.first.second};});
  sort(final_probs.begin(), final_probs.end(),
    [](const ValPoint& a, const ValPoint& b) { return a.val > b.val;});

  //This value correlates very well with the error of the prediction.
  //Lower values tend to indicate the possibility of higher error.
  vector<ValPoint> final_confprobs(conf_probs.size());
  transform(conf_probs.begin(), conf_probs.end(), final_confprobs.begin(),
      [](const pair<pair<double, double>, double>& a)->ValPoint {
      return ValPoint{a.second, a.first.first, a.first.second};});
  sort(final_confprobs.begin(), final_confprobs.end(),
      [](const ValPoint& a, const ValPoint& b) { return a.val > b.val;});
  //std::cout<<"Best val is "<<pow(final_confprobs[0].val, 1.0 / rxers.size())<<'\n';

  //For graphing
  /*
  std::for_each(final_probs.begin(), final_probs.end(), [&](const ValPoint& a) {
      std::cerr<<"Using ("<<a.x<<", "<<a.y<<")\n";
      std::cerr<<"cum_prob is "<<a.val<<'\n';
      });
      */
  //Calculate the biased center of the upper percentile.
  //double percentile = 0.01;
  //double percentile = 1.0;
  double percentile = 0.5;
  auto end = final_probs.begin() + percentile*final_probs.size();
  double total_prob = std::accumulate(final_probs.begin(), end, 0.0,
      [&](double a, ValPoint& b) { return a + b.val;});
  double cent_x = std::accumulate(final_probs.begin(), end, 0.0,
      [&](double a, ValPoint& b) { return a + b.x*(b.val/total_prob);});
  double cent_y = std::accumulate(final_probs.begin(), end, 0.0,
      [&](double a, ValPoint& b) { return a + b.y*(b.val/total_prob);});
  return ValPoint{pow(final_confprobs[0].val, 1.0 / rxers.size()), cent_x, cent_y};
}

/*
 * Area-based probability that uses estimated radial alpha values and provided
 * radial antenna gains.
 * For each signal an exhaustive search is done to find the smallest confidence
 * interval that would allow the signal to come from each tile. The probability
 * is normalized by the number of tiles included in that confidence interval.
 * The cumulative probability across all receivers is used and the point
 * with the highest probability is identified as the source of the signal.
 * @resolution determines the spacing between tiles.
 * This algorithm actually just uses points, not tiles, but this is roughly
 * equivalent to the common area based probability algorithm.
 */
std::vector<CELocalize::Result> CELocalize::areaBasedProbability( TrainingData train_data,
    LandmarkData rx_locations,
    double region_width,
    double region_height,
    double resolution,
    vector<vector<double>> antenna_gain) {

  srandom(time(NULL));

  //Split the training data into knowns and unknowns
  TrainingData tx_locations;
  TrainingData unknowns;
  for (auto I = train_data.begin(); I != train_data.end(); ++I) {
    if (isnan(I->x)) {
      unknowns.push_back(*I);
    }
    else {
      tx_locations.push_back(*I);
    }
  }

  //Get rid of any receivers that don't have any training data
  for (ptrdiff_t rxer = rx_locations.size() - 1; rxer >= 0; --rxer) {
    size_t first_num = 0;
    while (first_num < tx_locations.size()) {
      if (not isnan(tx_locations[first_num].rssis[rxer])) {
        break;
      }
      ++first_num;
    }
    if (first_num == tx_locations.size()) {
      //Remove this receiver and all of its samples
      for (auto I = tx_locations.begin(); I != tx_locations.end(); ++I) {
        I->rssis.erase(I->rssis.begin()+rxer);
      }
      rx_locations.erase(rx_locations.begin()+rxer);
      antenna_gain.erase(antenna_gain.begin()+rxer);
    }
  }


  std::cerr<<"First entry in gains is "<<antenna_gain[0].size()<<'\n';

  //Estimate the alphas values for all of the receivers
  vector<AlphaEstimate> alphas = estimateAlphas(tx_locations, rx_locations);
  //Use the given antenna gains during alpha estimation (if any antenna gains were given)
  //The estimateRadialAlphas function will ignore the given antenna gains if they cannot be used
  std::cerr<<"In the beginning the antenna gain had "<<antenna_gain.size()<<" entries.\n";
  //vector<vector<AlphaEstimate>> radial_alphas = estimateRadialAlphas(tx_locations, rx_locations, antenna_gain);
  vector<vector<AlphaEstimate>> radial_alphas = estimateQuadAlphas(tx_locations, rx_locations, antenna_gain);

  /*
  //First localize all of the training points to estimate the correlation
  //between the scoring metric and the potential error.
  vector<pair<double, double>> error_scores;
  double err_mean = 0.0;
  double err_var = 0.0;
  double score_mean = 0.0;
  double score_var = 0.0;
  for (auto I = tx_locations.begin(); I != tx_locations.end(); ++I) {
    LandmarkData cur_rx_locations = rx_locations;
    vector<vector<double>> cur_gains = antenna_gain;
    vector<vector<AlphaEstimate>> cur_alphas = radial_alphas;

    //Get rid of any receivers that don't have any data
    for (ptrdiff_t rxer = rx_locations.size() - 1; rxer >= 0; --rxer) {
      if ((isnan(I->rssis[rxer]) or isnan(cur_alphas[rxer][0].mean))) {
        //Remove this receiver and all of its samples
        I->rssis.erase(I->rssis.begin()+rxer);
        cur_alphas.erase(cur_alphas.begin()+rxer);
        cur_rx_locations.erase(cur_rx_locations.begin()+rxer);
        cur_gains.erase(cur_gains.begin()+rxer);
      }
    }

    ValPoint point_score = abp_localize(*I, cur_rx_locations, cur_alphas, cur_gains, region_width, region_height, resolution);
    //The distance between the expected location and the actual location
    //is the error
    double error = sqrt(pow(point_score.x - I->x, 2.0) + pow(point_score.y - I->y, 2.0));
    error_scores.push_back(make_pair(error, point_score.val));
    if (error_scores.size() == 1) {
      err_mean = error;
      score_mean = point_score.val;
    }
    //Running average and variance
    else {
      double prev_err_mean = err_mean;
      double prev_score_mean = score_mean;
      err_mean += (error - prev_err_mean)/error_scores.size();
      err_var += (error - prev_err_mean)*(error - err_mean);
      score_mean += (point_score.val - prev_score_mean)/error_scores.size();
      score_var += (point_score.val - prev_score_mean)*(point_score.val - score_mean);
    }
  }
  //Compute correlation
  double corr_err_score = 0.0;
  if (error_scores.size() > 0) {
    double sq_dev = sqrt(err_var) * sqrt(score_var);
    for (size_t i = 0; i < error_scores.size(); ++i) {
      corr_err_score += (error_scores[i].first - err_mean)*(error_scores[i].second - score_mean)/sq_dev;
    }
    corr_err_score /= error_scores.size();
    //std::cout<<"Correlation score is "<<corr_err_score<<'\n';
  }
  */

  std::vector<Result> results;
  //Localize each unknown
  for (auto I = unknowns.begin(); I != unknowns.end(); ++I) {
    LandmarkData cur_rx_locations = rx_locations;
    vector<vector<double>> cur_gains = antenna_gain;
    vector<vector<AlphaEstimate>> cur_alphas = radial_alphas;

    //Get rid of any receivers that don't have any data
    for (ptrdiff_t rxer = rx_locations.size() - 1; rxer >= 0; --rxer) {
      if ((isnan(I->rssis[rxer]) or isnan(cur_alphas[rxer][0].mean))) {
        //Remove this receiver and all of its samples
        I->rssis.erase(I->rssis.begin()+rxer);
        cur_alphas.erase(cur_alphas.begin()+rxer);
        cur_rx_locations.erase(cur_rx_locations.begin()+rxer);
        cur_gains.erase(cur_gains.begin()+rxer);
      }
    }
    /*
    //Use a simple bayesian search to find a smaller area to search.
    //Make a locus that covers at least twice the area of the region to dispel biases along the edges
    Locus locus{region_width/2.0, region_height/2.0, sqrt(2.0) * (1.1 * region_width + region_height) / 2.0};
    double quantile = 0.90;
    double iterations = 1000;
    locus = getNextLocus(locus, *I, cur_rx_locations, radial_alphas, quantile, iterations, cur_gains);
    locus.x -= locus.radius/2.0;
    locus.y -= locus.radius/2.0;

    //Constrain this to be inside of the actual width and height
    double search_width = locus.radius;
    double search_height = locus.radius;
    if (locus.x < 0.0) {
      search_width += locus.x;
      locus.x = 0;
    }
    else if (locus.x + search_width > region_width) {
      search_width = region_width - locus.x;
    }
    if (locus.y < 0.0) {
      search_height += locus.y;
      locus.y = 0;
    }
    else if (locus.y + search_height > region_height) {
      search_height = region_height - locus.y;
    }

    //Renormalize everything with respect to this smaller region.
    for_each(cur_rx_locations.begin(), cur_rx_locations.end(), [&](pair<double, double>& rx_loc) {
        rx_loc.first -= locus.x;
        rx_loc.second -= locus.y; });
    //At least search over a few points...
    if (locus.radius < resolution) {
      locus.radius = 2*resolution;
    }
    std::cerr<<"Region shrunk from "<<region_width<<", "<<region_height<<" to "<<search_width<<", "<<search_height<<" offset by "<<locus.x<<", "<<locus.y<<'\n';
    ValPoint point_score = abp_localize(*I, cur_rx_locations, cur_alphas, cur_gains, search_width, search_height, resolution);
    std::cerr<<"New locus is at location "<<point_score.x+locus.x<<", "<<point_score.y+locus.y<<" with score "<<point_score.val<<'\n';
    results.push_back(Result{point_score.x + locus.x, point_score.y + locus.y});
    */


    ValPoint point_score = abp_localize(*I, cur_rx_locations, cur_alphas, cur_gains, region_width, region_height, resolution);
    results.push_back(Result{point_score.x, point_score.y});
    //Use all of the receivers as good receivers
    vector<bool> good_rxers(cur_rx_locations.size(), true);
    //double point_prob = calculateProbability(point_score.x, point_score.y, *I, cur_rx_locations, good_rxers, cur_gains, cur_alphas);
    double point_prob = calculateProbabilityDensity(point_score.x, point_score.y, *I, cur_rx_locations, good_rxers, cur_gains, cur_alphas);
    //std::cout<<"Score found was "<<point_prob * point_score.val<<'\n';
    //std::cout<<"Score found was "<<point_prob<<'\n';
    //std::cout<<"Score found was "<<point_score.val<<'\n';
    //std::cout<<"Error guess is "<<(score_mean - point_score.val) / corr_err_score<<'\n';
  }
  return results;
}

//Cluster an area of high probability via a depth first search
//Assume that the given point is the highest in its cluster so
//we just descend to find everything else in the cluster
vector<pair<double, double>> dfs_cluster(map<pair<double, double>, double>& origin_probs,
    double x, double y, double resolution, double threshold) {
  map<pair<double, double>, bool> used;
  vector<pair<double, double>> cluster;
  vector<ValPoint> left;
  ValPoint first{1.0, x, y};
  left.push_back(first);
  while (not left.empty()) {
    ValPoint new_point = left.back();
    pair<double, double> here = make_pair(new_point.x, new_point.y);
    left.pop_back();
    //If we haven't used this point and it descends from the previous point include it
    if (not used[here] and
        origin_probs[here] <= new_point.val and
        origin_probs[here] > threshold) {
      used[here] = true;
      cluster.push_back(here);
      //Push in the neighbors (this pushes in the same point, but it will be dropped...)
      for (double xchange = -resolution; xchange <= resolution; xchange += resolution) {
        for (double ychange = -resolution; ychange <= resolution; ychange += resolution) {
          ValPoint new_val{origin_probs[here], here.first+xchange, here.second+ychange};
          left.push_back(new_val);
        }
      }
    }
  }
  return cluster;
}

/*
 * Assume that the strongest RSS value is the most ``real'' and adjust the other
 * values upwards to compensate, but don't move any values down.
 */
void fixup_rssis(Sample& unknown,
    LandmarkData& rxers,
    vector<vector<AlphaEstimate>>& radial_alphas,
    vector<vector<double>> antenna_gain) {
  //Order the receivers by the unknown's signal strength.
  vector<pair<size_t, double>> rxers_by_power;
  for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
    rxers_by_power.push_back(make_pair(rxer, unknown.rssis[rxer]));
  }
  std::sort(rxers_by_power.begin(), rxers_by_power.end(),
      [&](const pair<size_t, double>& a, const pair<size_t, double>& b) { return a.second > b.second;});
  //TODO finish trying this
  
}

/**
 * This function assumes that each receiver in @rxers has data and alpha values.
 * The return value lists the estimated location of the transmitter as well as a
 * confidence metric.
 */
ValPoint cluster_localize(Sample& unknown,
    LandmarkData& rxers,
    vector<vector<AlphaEstimate>>& radial_alphas,
    vector<vector<double>> antenna_gain,
    double region_width,
    double region_height,
    double resolution) {
  size_t total_points = (1 + region_width / resolution) * (1 + region_height / resolution);
  vector<map<pair<double, double>, long double>> rxer_point_probs(rxers.size());
  //Stores which receiver is closest to a point
  map<pair<double, double>, pair<size_t, double>> closest_rx_dist;
  //Find the confidence coefficients necessary to include each point
  //in a confidence region of each receiver
  vector<vector<ValPoint>> origin_conf;
  for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
    vector<ValPoint> rxer_conf_points;
    for (double x = 0.0; x <= region_width; x+= resolution) {
      for (double y = 0.0; y <= region_height; y += resolution) {
        //Distance along x and y axes to get to the given location from the receiver
        double xdiff = x - rxers[rxer].first;
        double ydiff = y - rxers[rxer].second;
        //Get the angle, from -pi to pi
        double tx_angle = atan2( ydiff, xdiff);
        //Estimate the gain and alpha value at this position
        AlphaEstimate alpha = interpolateSlices(tx_angle, radial_alphas[rxer]);
        double gain = interpolateSlices(tx_angle, antenna_gain[rxer]);
        //Find the distance from the receiver to this point
        double point_dist = sqrt(pow(xdiff, 2) + pow(ydiff, 2));
        //Find the smallest confidence interval where this point is included.
        double conf_coefficient = getMinConfidenceCoefficient(point_dist, unknown.rssis[rxer], alpha, gain);
        rxer_conf_points.push_back(ValPoint{conf_coefficient, x, y});
        //std::cerr<<"rxer "<<rxer<<" makes conf point at "<<x<<"(, "<<y<<") with confidence "<<conf_coefficient<<'\n';
        //Later on we will calculate the confidence level associated with this
        //confidence coefficient. However, we need the total number of points
        //inside of each confidence region first so we will defer this.

        pair<double, double> here = make_pair(x, y);
        rxer_point_probs[rxer][here] = probSignalFromAlphas(unknown.rssis[rxer], point_dist, alpha, 1.0, gain);
        if (rxer == 0) {
          closest_rx_dist[here] = make_pair(rxer, point_dist);
        } else if (closest_rx_dist[here].second > point_dist) {
          closest_rx_dist[here] = make_pair(rxer, point_dist);
        }
      }
    }
    //Store the points
    origin_conf.push_back(rxer_conf_points);
  }
  //Sorting the points here allows us to easily count how many points
  //are included in each confidence interval later on during the
  //normalization step
  for (auto points = origin_conf.begin(); points != origin_conf.end(); ++points) {
    sort(points->begin(), points->end(),
        [](const ValPoint& a, const ValPoint& b) { return a.val < b.val;});
  }
  //Now we can find the probability of each point being the origin of
  //the given signal.
  //The probability of a signal coming from a point will be the
  //minimum confidence level necessary to include the point normalized
  //by the number of points that confidence level contains.
  //Make a 2D vector to store these values as we accumulate them
  //(we assume each receiver is independant so we can just multiply
  // the probabilities) as we solve for probabilities out of order
  const double sqrt2 = sqrt(2.0);
  map<pair<double, double>, double> origin_probs;
  //The same as origin probs but takes into account the percentages of the confidence intervals
  map<pair<double, double>, double> conf_probs;
  for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
    //TODO Use an index rather than an iterator so that finding the number less
    //than this one is easy
    auto conf = origin_conf.begin()+rxer;
    vector<double> leqs;
    for (auto point = conf->begin(); point != conf->end(); ++point) {
      //std::cerr<<"Expanding point "<<point->x<<", "<<point->y<<" ("<<point->val<<")\n";
      //Calculate the confidence coefficient needed to include this point
      //From Chebyshev's inequality the proportion of points included within
      //some number of standard deviations, k, is at least (1 / (1 + k**2))
      //If the data is distributed normally then that proportion is
      //approximately erf(k / sqrt(2))
      //First find out how much we can expand the confidence interval
      vector<ValPoint>::iterator up = find_if(point, conf->end(),
          [&](const ValPoint& a)->bool { return a.val > point->val;});
      //We can increase our confidence area without including any of the
      //lower confidence points by going to up->val. If all other points
      //work at a lower confidence interval than this one then this point
      //lies in the 100% confidence region (ie, all points)
      double max_conf = 1.0;
      if (up != conf->end()) {
        max_conf = up->val;
      }
      //Number of points less than or equal to this one (including this point)
      size_t num_leq = distance(conf->begin(), up);
      //To normalize we need to divide by num_leq
      pair<double, double> coordinate = make_pair(point->x, point->y);
      if (origin_probs.find(coordinate) == origin_probs.end()) {
        origin_probs[coordinate] = 1.0;
      }
      origin_probs[make_pair(point->x, point->y)] *= (1.0 / num_leq);
      leqs.push_back(1.0 / num_leq);
      //std::cerr<<"Prob at point "<<point->x<<", "<<point->y<<" is now "<<origin_probs[coordinate]<<'\n';
    }
  }
  //Now do we go for minimum RMSE by finding the centroid of the upper
  //few points, or should we just pick the highest probability point?
  vector<ValPoint> final_probs(origin_probs.size());
  transform(origin_probs.begin(), origin_probs.end(), final_probs.begin(),
      [](const pair<pair<double, double>, double>& a)->ValPoint {
      return ValPoint{a.second, a.first.first, a.first.second};});
  sort(final_probs.begin(), final_probs.end(),
    [](const ValPoint& a, const ValPoint& b) { return a.val > b.val;});
  
  //Now cluster - keep track of whether or not each point has been used and
  //do breadth first search to build clusters.
  //As long as probabilities are decreasing we count these points as being in the cluster.
  //If, at the end, points end up in multiple clusters they are not counted in any clusters.
  //They are simply valleys that no cluster can claim.
  map<pair<double, double>, size_t> cluster_use_count;
  vector<vector<pair<double, double>>> clusters;
  //Set the 50th percentile as the threshold to include in any clusters.
  double threshold = final_probs[final_probs.size()/2.0].val;
  for (auto I = final_probs.begin(); I != final_probs.end() and I->val > threshold; ++I) {
    //Start a new cluster if this point is unused.
    if (0 == cluster_use_count[make_pair(I->x, I->y)]) {
      vector<pair<double, double>> cluster = dfs_cluster(origin_probs, I->x, I->y, resolution, threshold);
      //Now record another use for each value in the cluster
      for_each(cluster.begin(), cluster.end(), [&](pair<double, double>& p) {cluster_use_count[p]++;});
      clusters.push_back(cluster);
      //For reference print out the cluster.
      std::cerr<<"Cluster number "<<clusters.size()<<'\n';
      for_each(cluster.begin(), cluster.end(), [&](pair<double, double>& p) {std::cerr<<p.first<<", "<<p.second<<'\n';});
    }
  }
  //Print out the strongest receiver
  vector<pair<size_t, double>> rxers_by_power;
  for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
    rxers_by_power.push_back(make_pair(rxer, unknown.rssis[rxer]));
  }
  std::sort(rxers_by_power.begin(), rxers_by_power.end(),
      [&](const pair<size_t, double>& a, const pair<size_t, double>& b) { return a.second > b.second;});
  size_t strongest = rxers_by_power[0].first;
  size_t next_strongest = rxers_by_power[1].first;

  std::cerr<<"Strongest signal is from rxer "<<rxers_by_power[0].first<<" with power "<<rxers_by_power[0].second<<'\n';
  ValPoint best_point{0.0, 0.0, 0.0};
  //Rate each cluster based upon the cluster peak.
  for (auto C = clusters.begin(); C != clusters.end(); ++C) {
    //Only look at the peak
    pair<double, double> peak = C->front();
    long double close_prob = 1.0;//rxer_point_probs[closest_rx_dist[peak].first][peak];
    long double strong_prob = rxer_point_probs[strongest][peak];
    long double next_strong_prob = 1.0;//rxer_point_probs[next_strongest][peak];
    if (best_point.val < close_prob * strong_prob * next_strong_prob) {
      best_point =  ValPoint{(double)(close_prob * strong_prob * next_strong_prob), peak.first, peak.second};
    }
    std::cerr<<"Checking peak at "<<peak.first<<", "<<peak.second<<'\n';
    std::cerr<<"Closest receiver is "<<closest_rx_dist[peak].first<<" with distance "<<closest_rx_dist[peak].second<<'\n';
    for (size_t rxer = 0; rxer < rxers.size(); ++rxer) {
      long double point_prob = rxer_point_probs[rxer][peak];
      std::cerr<<"Rxer "<<rxer<<" has prob "<<point_prob<<'\n';
    }
    //Score the peak via its probability.
    //Defer to the closest receiver.
    //TODO
  }
  //return best_point;


  //For graphing
  /*
  std::for_each(final_probs.begin(), final_probs.end(), [&](const ValPoint& a) {
      std::cerr<<"Using ("<<a.x<<", "<<a.y<<")\n";
      std::cerr<<"cum_prob is "<<a.val<<'\n';
      });
      */
  //Calculate the biased center of the upper percentile.
  //double percentile = 0.01;
  //double percentile = 1.0;
  double percentile = 0.5;
  auto end = final_probs.begin() + percentile*final_probs.size();
  double total_prob = std::accumulate(final_probs.begin(), end, 0.0,
      [&](double a, ValPoint& b) { return a + b.val;});
  double cent_x = std::accumulate(final_probs.begin(), end, 0.0,
      [&](double a, ValPoint& b) { return a + b.x*(b.val/total_prob);});
  double cent_y = std::accumulate(final_probs.begin(), end, 0.0,
      [&](double a, ValPoint& b) { return a + b.y*(b.val/total_prob);});
  //return ValPoint{(double)clusters[0].size() / total_points, cent_x, cent_y};
  double peak_dist = sqrt(pow(cent_x - best_point.x, 2.0) + pow(cent_y - best_point.y, 2.0));
  return ValPoint{peak_dist, cent_x, cent_y};
}

/*
 * Area-based probability that uses estimated radial alpha values and provided
 * radial antenna gains.
 * For each signal an exhaustive search is done to find the smallest confidence
 * interval that would allow the signal to come from each tile. The probability
 * is normalized by the number of tiles included in that confidence interval.
 * Each point is rated into group of higher confidence.
 * Then these groups are grouped into clusters, starting from the highest
 * confidence cluster. Localization is done using the clusters.
 */
std::vector<CELocalize::Result> CELocalize::clusterLocalization( TrainingData train_data,
    LandmarkData rx_locations,
    double region_width,
    double region_height,
    double resolution,
    vector<vector<double>> antenna_gain) {

  srandom(time(NULL));

  //Split the training data into knowns and unknowns
  TrainingData tx_locations;
  TrainingData unknowns;
  for (auto I = train_data.begin(); I != train_data.end(); ++I) {
    if (isnan(I->x)) {
      unknowns.push_back(*I);
    }
    else {
      tx_locations.push_back(*I);
    }
  }

  //Get rid of any receivers that don't have any training data
  for (ptrdiff_t rxer = rx_locations.size() - 1; rxer >= 0; --rxer) {
    size_t first_num = 0;
    while (first_num < tx_locations.size()) {
      if (not isnan(tx_locations[first_num].rssis[rxer])) {
        break;
      }
      ++first_num;
    }
    if (first_num == tx_locations.size()) {
      //Remove this receiver and all of its samples
      for (auto I = tx_locations.begin(); I != tx_locations.end(); ++I) {
        I->rssis.erase(I->rssis.begin()+rxer);
      }
      rx_locations.erase(rx_locations.begin()+rxer);
      antenna_gain.erase(antenna_gain.begin()+rxer);
    }
  }


  std::cerr<<"First entry in gains is "<<antenna_gain[0].size()<<'\n';

  //Estimate the alphas values for all of the receivers
  vector<AlphaEstimate> alphas = estimateAlphas(tx_locations, rx_locations);
  //Use the given antenna gains during alpha estimation (if any antenna gains were given)
  //The estimateRadialAlphas function will ignore the given antenna gains if they cannot be used
  std::cerr<<"In the beginning the antenna gain had "<<antenna_gain.size()<<" entries.\n";
  //vector<vector<AlphaEstimate>> radial_alphas = estimateRadialAlphas(tx_locations, rx_locations, antenna_gain);
  vector<vector<AlphaEstimate>> radial_alphas = estimateQuadAlphas(tx_locations, rx_locations, antenna_gain);
  std::vector<Result> results;
  //Localize each unknown
  for (auto I = unknowns.begin(); I != unknowns.end(); ++I) {
    LandmarkData cur_rx_locations = rx_locations;
    vector<vector<double>> cur_gains = antenna_gain;
    vector<vector<AlphaEstimate>> cur_alphas = radial_alphas;

    //Get rid of any receivers that don't have any data
    for (ptrdiff_t rxer = rx_locations.size() - 1; rxer >= 0; --rxer) {
      if ((isnan(I->rssis[rxer]) or isnan(cur_alphas[rxer][0].mean))) {
        //Remove this receiver and all of its samples
        I->rssis.erase(I->rssis.begin()+rxer);
        cur_alphas.erase(cur_alphas.begin()+rxer);
        cur_rx_locations.erase(cur_rx_locations.begin()+rxer);
        cur_gains.erase(cur_gains.begin()+rxer);
      }
    }
    /*
    //Use a simple bayesian search to find a smaller area to search.
    //Make a locus that covers at least twice the area of the region to dispel biases along the edges
    Locus locus{region_width/2.0, region_height/2.0, sqrt(2.0) * (1.1 * region_width + region_height) / 2.0};
    double quantile = 0.90;
    double iterations = 1000;
    locus = getNextLocus(locus, *I, cur_rx_locations, radial_alphas, quantile, iterations, cur_gains);
    locus.x -= locus.radius/2.0;
    locus.y -= locus.radius/2.0;

    //Constrain this to be inside of the actual width and height
    double search_width = locus.radius;
    double search_height = locus.radius;
    if (locus.x < 0.0) {
      search_width += locus.x;
      locus.x = 0;
    }
    else if (locus.x + search_width > region_width) {
      search_width = region_width - locus.x;
    }
    if (locus.y < 0.0) {
      search_height += locus.y;
      locus.y = 0;
    }
    else if (locus.y + search_height > region_height) {
      search_height = region_height - locus.y;
    }

    //Renormalize everything with respect to this smaller region.
    for_each(cur_rx_locations.begin(), cur_rx_locations.end(), [&](pair<double, double>& rx_loc) {
        rx_loc.first -= locus.x;
        rx_loc.second -= locus.y; });
    //At least search over a few points...
    if (locus.radius < resolution) {
      locus.radius = 2*resolution;
    }
    std::cerr<<"Region shrunk from "<<region_width<<", "<<region_height<<" to "<<search_width<<", "<<search_height<<" offset by "<<locus.x<<", "<<locus.y<<'\n';
    ValPoint point_score = cluster_localize(*I, cur_rx_locations, cur_alphas, cur_gains, search_width, search_height, resolution);
    std::cerr<<"New locus is at location "<<point_score.x+locus.x<<", "<<point_score.y+locus.y<<" with score "<<point_score.val<<'\n';
    results.push_back(Result{point_score.x + locus.x, point_score.y + locus.y});
    */


    ValPoint point_score = cluster_localize(*I, cur_rx_locations, cur_alphas, cur_gains, region_width, region_height, resolution);
    results.push_back(Result{point_score.x, point_score.y});
    //Use all of the receivers as good receivers
    vector<bool> good_rxers(cur_rx_locations.size(), true);
    //double point_prob = calculateProbability(point_score.x, point_score.y, *I, cur_rx_locations, good_rxers, cur_gains, cur_alphas);
    double point_prob = calculateProbabilityDensity(point_score.x, point_score.y, *I, cur_rx_locations, good_rxers, cur_gains, cur_alphas);
    //std::cout<<"Score found was "<<point_prob * point_score.val<<'\n';
    //std::cout<<"Score found was "<<point_prob<<'\n';
    std::cout<<"Score found was "<<point_score.val<<'\n';
    //std::cout<<"Error guess is "<<(score_mean - point_score.val) / corr_err_score<<'\n';
  }
  return results;
}

