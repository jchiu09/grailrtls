#include <algorithm>
#include <array>
#include <iostream>
#include <map>
#include <string>
#include <time.h>
#include <vector>
#include <utility>
#include <limits>
#include <functional>
#include <sstream>
#include <cmath>
#include <mutex>
#include <thread>

#include "server_solver_protocol.hpp"
#include "grailV3_solver_client.hpp"
#include "solver_distributor_protocol.hpp"
#include "sample_data.hpp"
#include "netbuffer.hpp"
#include "tile_localize.hpp"

using namespace solver_distributor;

using std::make_pair;

struct Debug {
  bool off;
};

template<typename T>
Debug& operator<<(Debug& dbg, T arg) {
  if (dbg.off) {
    std::cerr<<arg;
  }
  return dbg;
}

typedef std::pair<TransmitterID, ReceiverID> tx_rx_pair;
struct comparePair {
  bool operator()(const tx_rx_pair& left, const tx_rx_pair& right) {
    return left.first < right.first or
      (left.first == right.first and left.second < right.second);
  }
};

pair<double, double> getCentroid(vector<Tile>& tiles, double tile_w, double tile_h) {
  if (0 == tiles.size()) {
    return make_pair(numeric_limits<double>::quiet_NaN(), numeric_limits<double>::quiet_NaN());
  }
  double total_x = 0.0;
  double total_y = 0.0;
  for (auto T = tiles.begin(); T != tiles.end(); ++T) {
    total_x += T->column * tile_w + tile_w/2.0;
    total_y += T->row * tile_h + tile_h/2.0;
  }
  return make_pair(total_x / tiles.size(), total_y / tiles.size());
}

//Print out statistics for chair usage in hour intervals.
//This function is not thread safe (but could be made so with a mutex)
struct localizeCallback {
  //Maximum x and y values of the region
  double maxx;
  double maxy;

  //The aggregator where locatization results should be sent.
  Aggregator grail_agg;
  std::u16string region;

  std::map<ReceiverID, std::pair<double, double>> rx_locations;
  //The locations of immobile transmitters
  std::map<TransmitterID, std::pair<double, double>> tx_locations;
  size_t update_interval;
  time_t next_update_time;

  std::mutex data_mutex;
  //The transmitters that have been active in the current time interval.
  std::map<TransmitterID, bool> active_tx;
  //The rssi values for a tx/rx pair.
  std::map<tx_rx_pair, std::vector<float>, comparePair> rssis;

  //Active variables updated every time a new packet is received
  //The transmitters that have been active in the current time interval.
  std::map<TransmitterID, bool> internal_active_tx;
  //The rssi values for a tx/rx pair.
  std::map<tx_rx_pair, std::vector<float>, comparePair> internal_rssis;

  Debug debug;
  
  //Constructor
  localizeCallback() {
    debug.off = true;
  }

  //Localizes every update_interval seconds.
  void timedLocalize() {
    while (1) {
      //Localize every update_interval seconds after the previous localization
      time_t time_seconds = time(NULL);
      //Sleep until the next update time
      if (time_seconds < next_update_time) {
        sleep(next_update_time - time_seconds);
      }

      //Lock the RSS data and copy it
      {
        std::unique_lock<std::mutex> lck(data_mutex);
        active_tx = internal_active_tx;
        rssis     = internal_rssis;
        //Clear the old data away
        this->internal_active_tx.clear();
        this->internal_rssis.clear();
      }
      //Set the time for of the next localization
      next_update_time = time(NULL) + update_interval;
      //Localize (this also sends data to the aggregator).
      this->localize();
    }
  }


  void packetCallback(SampleData& sample) {
    //Lock the data mutex and add this sample's data to internal storage.
    std::unique_lock<std::mutex> lck(data_mutex);
    //Log the sample
    internal_rssis[make_pair(sample.tx_id, sample.rx_id)].push_back(sample.rss);
    //Mark the sample as active if any receiver has received at least half of its packets.
    //This assumes that the tag is sending at once per second.
    if (not internal_active_tx[sample.tx_id] and
        internal_rssis[make_pair(sample.tx_id, sample.rx_id)].size() >= update_interval / 2) {
      debug<<"Tx "<<sample.tx_id<<" now active\n";
      //Mark the transmitter as active.
      internal_active_tx[sample.tx_id] = true;
    }
  }

  struct M2Setup {
    TrainingData t_data;
    LandmarkData l_data;
    std::vector<TransmitterID> unknowns;
  };

  //Generate training data for the m2 solver.
  M2Setup genTrainingData() {
    //Assemble data in the format needed by the M2 localization solver

    //The locations of the landmarks in the region.
    LandmarkData l_data;
    int num_landmarks = 0;
    std::map<int, ReceiverID> rx_indices;
    for (auto rx = rx_locations.begin(); rx != rx_locations.end(); ++rx) {
      l_data.push_back(rx->second);
      rx_indices[num_landmarks++] = rx->first;
    }
    debug<<"Receivers are:\n\t";
    for (auto I = rx_indices.begin(); I != rx_indices.end(); ++I) {
      debug<<'\t'<<I->second;
    }
    debug<<'\n';

    //Transmitter data for transmitters with known and unknown locations.
    TrainingData t_data_unknown;
    TrainingData t_data_known;
    std::vector<TransmitterID> unknowns;

    //A NaN value marks an unknown location.
    const float qnan = std::numeric_limits<float>::quiet_NaN();

    //For each active transmitter determine if its location is known and then
    //add its data either into the unknown or known t_data lists.
    for (auto txer = active_tx.begin(); txer != active_tx.end(); ++txer) {
      //Add this transmitter to the training data if it was active
      //in the previous time period.
      if (txer->second) {
        //Record the recent rssi values for each of the landmarks, defaulting
        //to a value of -1 if there is no data.
        std::vector<double> tx_rssis(num_landmarks, -1.0);
        size_t total_samples = 0;
        for (int index = 0; index < num_landmarks; ++index) {
          ReceiverID rxer = rx_indices[index];
          tx_rx_pair txrx = make_pair(txer->first, rxer);
          std::cerr<<"Raw signals from txer "<<txer->first<<" to rxer "<<rxer<<'\n';
          if (rssis.end() != rssis.find(make_pair(txer->first, rxer))) {
            //Select the median rssi to represent the link from tx to rx
            std::sort(rssis[txrx].begin(), rssis[txrx].end());
            int middle = rssis[txrx].size()/2;
            //If this is even do an average of the two middle values, otherwise
            //use the middle value.
            if (rssis[txrx].size() %2 == 0) {
              tx_rssis[index] = (rssis[txrx][middle] + rssis[txrx][middle+1]) / 2.0;
            }
            else {
              tx_rssis[index] = rssis[txrx][middle];
            }
            for (auto rssval = rssis[txrx].begin(); rssval != rssis[txrx].end(); ++rssval) {
              std::cerr<<" "<<*rssval;
            }
            std::cerr<<'\n';
            ++total_samples;
          }
        }

        //Make sure there are enough samples before trying to localize
        if (3 <= total_samples) {
          //If the txer isn't in the location list it has an unknown location
          //Otherwise the location is known
          auto tx_info = tx_locations.find(txer->first);
          if (tx_locations.end() == tx_info) {
            //Remember which transmitters had unknown locations.
            unknowns.push_back(txer->first);
            //Push the RSSI and location data back
            t_data_unknown.push_back( Sample{qnan, qnan, tx_rssis} );
          } else {
            //Push the RSSI and location data back
            t_data_known.push_back( Sample{tx_info->second.first, tx_info->second.second, tx_rssis} );
          }
        }
      }

    }

    //Now make a single training data vector with the unknowns at the end
    TrainingData t_data(t_data_unknown.size() + t_data_known.size());
    std::copy(t_data_known.begin(), t_data_known.end(), t_data.begin());
    std::copy(t_data_unknown.begin(), t_data_unknown.end(),
        t_data.begin() + t_data_known.size());
    debug<<"Generating training data with "<<t_data_known.size()<<
      " fiduciary transmitters and solving for "<<t_data_unknown.size()<<
      " unlocalized transmitters.\n";

    return M2Setup{t_data, l_data, unknowns};
  }


  //Call the m2 solver using transmitter data for transmitters in the active_tx table.
  void localize() {

    M2Setup m2s = this->genTrainingData();
    //The power at a distance of 0 from the receiver
    double initial_loss = -20.0;
    tile_localize::dataToParameter toAlpha = [=](Point rxer, Point sample, double rss) {
      double distance = euclDistance(rxer, sample);
      //If the value is -1 insert alpha of 3
      if (-1 == rss) {
        return 3.0;
      }
      //Determine alpha from the equation: Loss = 10*alpha*log10(distance) + initial_loss
      double alpha = (rss - initial_loss) / ( 10 * log10(distance));
      return abs(alpha);
    };
    //TODO FIXME HERE If any data points have a value of -1 they should not be used as vertices in
    //the initial map used for interpolation.
    //TODO Need a way to remove a vertex from the delaunay triangle mesh or I need to
    //rebuild the mesh for every receiver.

    tile_localize::parameterToData toRSS = [=](Point rxer, Point sample, double alpha)->double {
      double distance = euclDistance(rxer, sample);
      if (0.0 == distance) { return initial_loss;}
      //Determine the rss from the equation: Loss = 10*alpha*log10(distance) + initial_loss
      double rss = -1 * alpha * (10 * log10(distance)) + initial_loss;
      return rss;
    };

    double tile_w = 8;
    double tile_h = 8;
    double threshold = 10;

    //Don't localize if there are no targets of interest.
    if (0 == m2s.unknowns.size()) {
      debug<<"Not enough data to localize.\n";
      return;
    }

    debug<<"About to send solver this:\n";
    //Print out each row (transmitter)
    int tx_num = 0;
    for (auto I = m2s.t_data.begin(); I != m2s.t_data.end(); ++I) {
      debug<<I->x<<'\t'<<I->y<<'\t';
      //Print out the column value (receiver observed RSS)
      for (auto rss = I->rssis.begin(); rss != I->rssis.end(); ++rss) {
        debug<<*rss<<'\t';
      }
      debug<<'\n';
    }
    debug<<'\n';

    debug<<"Unknowns are:";
    for (auto U = m2s.unknowns.begin(); U != m2s.unknowns.end(); ++U) {
      debug<<" "<<*U;
    }
    debug<<'\n';


    vector<tile_localize::Result> results =
      tile_localize::SPMSolve(m2s.t_data, m2s.l_data, maxx, maxy, tile_w, tile_h, threshold, toAlpha, toRSS);

    //Create a new solver data message with results from this region at the current time.
    SolverDataMsg locations{std::u16string(this->region), msecTime()};

    debug<<"Localizing in region "<<std::string(locations.region.begin(), locations.region.end())<<'\n';

    debug<<"ID\tx\ty\t#Boxes\n";
    for (size_t tx_i = 0; tx_i < results.size(); ++tx_i) {
      tile_localize::Result& r = results[tx_i];
      pair<double, double> center = getCentroid(r, tile_w, tile_h);
      debug<<m2s.unknowns[tx_i]<<'\t'<<center.first<<'\t'<<center.second<<'\t'<<r.size()<<'\n';

      std::u16string target;
      {
        std::ostringstream os;
        os << m2s.unknowns[tx_i];
        std::string tag_name = os.str();
        target = std::u16string(tag_name.begin(), tag_name.end());
      }
      //Solution is of the form: x, y, tile_width, tile_height, # of boxes, [column, row]+
      SolutionData d{1, target};
      pushBackVal(center.first, d.data);
      pushBackVal(center.second, d.data);
      pushBackVal(tile_w, d.data);
      pushBackVal(tile_h, d.data);
      pushBackVal((uint32_t)r.size(), d.data);
      for (auto T = r.begin(); T != r.end(); ++T) {
        pushBackVal((uint32_t)T->column, d.data);
        pushBackVal((uint32_t)T->row, d.data);
      }

      locations.data.push_back(d);
    }
    if (locations.data.size() > 0) {
      //Send the location data to the grail data store/distributor.
      grail_agg.send(locations);
    }
  }
};


int main(int arg_count, char** arg_vector) {
  if (7 > arg_count) {
    std::cerr<<"This program needs 6 or more arguments:\n";
    std::cerr<<"\tclient [<server ip> <server port>]+ <aggregator ip> <aggregator port> <world server ip> <world server port>\n";
    std::cerr<<"Any number of server ip/port pairs may be provided to connect to multiple servers.\n";
    return 0;
  }

  //Grab the ip and ports for the servers and aggregator
  std::vector<NetTarget> servers;
  for (int s_num = 1; s_num < arg_count - 2; s_num += 2) {
    std::string server_ip(arg_vector[s_num]);
    uint16_t server_port = atoi(arg_vector[s_num + 1]);
    servers.push_back(NetTarget{server_ip, server_port});
  }
  std::string aggr_ip(arg_vector[arg_count-4]);
  int aggr_port = atoi(arg_vector[arg_count-3]);

  //Initialize constructs
  localizeCallback cb_struct;
  //TODO set these from a world server
  cb_struct.maxx = 218.0;
  cb_struct.maxy = 169.0;
  cb_struct.update_interval = 14;
  //The location data for the cb struct comes from the world server.

  //Get the IDs and locations of stationary transmitters and receivers from
  //the world server.
  std::string ws_ip(arg_vector[arg_count-2]);
  int ws_port = atoi(arg_vector[arg_count-1]);
  {
    WorldServer ws;
    if (not ws.connect(ws_ip, ws_port)) {
      std::cerr<<"Error connecting to the world server! Aborting!\n";
      return 0;
    }
    //First find fiduciary transmitters
    std::map<world_server::URI, std::map<std::u16string, world_server::FieldData>> wds = ws.getDataObjects(u"pipsqueak.transmitter.*");
    //Check each object to see if it has a location
    for (auto WD = wds.begin(); WD != wds.end(); ++WD) {
      std::map<std::u16string, world_server::FieldData> pip = WD->second;
      //Make sure the location and id fields exist and then read in their data.
      if (pip.end() != pip.find(u"location.x") and
          pip.end() != pip.find(u"location.y") and
          pip.end() != pip.find(u"id")) {
        uint64_t pip_id = readPrimitive<uint64_t>(pip[u"id"].data, 0);
        double pip_x = readPrimitive<double>(pip[u"location.x"].data, 0);
        double pip_y = readPrimitive<double>(pip[u"location.y"].data, 0);
        std::cerr<<"Transmitter id "<<pip_id<<" is at "<<pip_x<<", "<<pip_y<<'\n';
        cb_struct.tx_locations[pip_id] = make_pair(pip_x, pip_y);
      }
    }
    //Now repeat for the receivers
    wds = ws.getDataObjects(u"pipsqueak.receiver.*");
    for (auto WD = wds.begin(); WD != wds.end(); ++WD) {
      std::map<std::u16string, world_server::FieldData> pip = WD->second;
      //Make sure the location and id fields exist and then read in their data.
      if (pip.end() != pip.find(u"location.x") and
          pip.end() != pip.find(u"location.y") and
          pip.end() != pip.find(u"id")) {
        uint64_t pip_id = readPrimitive<uint64_t>(pip[u"id"].data, 0);
        double pip_x = readPrimitive<double>(pip[u"location.x"].data, 0);
        double pip_y = readPrimitive<double>(pip[u"location.y"].data, 0);
        std::cerr<<"Receiver id "<<pip_id<<" is at "<<pip_x<<", "<<pip_y<<'\n';
        cb_struct.rx_locations[pip_id] = make_pair(pip_x, pip_y);
      }
    }
    if (cb_struct.rx_locations.empty() or cb_struct.tx_locations.empty()) {
      std::cerr<<"Insufficient information on transmitter and receiver locations in the world server! Aborting!\n";
      return 0;
    }
  }

  Rule winlab_rule;
  winlab_rule.physical_layer  = 1;

  Transmitter all_sensors;

  winlab_rule.txers.push_back(all_sensors);


  winlab_rule.update_interval = 2000;
  cb_struct.region = u"Winlab";
  Subscription winlab_sub{u"Winlab", std::vector<Rule>{winlab_rule}};
  std::vector<Subscription> subs{winlab_sub};

  //There is only one solution type offered by the localization server, and it
  //consists of 4 doubles specifying the x and y coordinates and their standard
  //deviations.
  std::vector<SolutionType> types;
  SolutionType s_t{1, std::u16string(u"tile location")};
  types.push_back(s_t);
  //Set the first localization time.
  cb_struct.next_update_time = time(NULL) + cb_struct.update_interval;
  if (cb_struct.grail_agg.connect(aggr_ip, aggr_port, types)) {

    //Set up a timed localization thread. This allows the system to receive packets
    //even while it is localizing.
    std::thread timed_localize(std::mem_fun(&localizeCallback::timedLocalize), &cb_struct);

    //Connect to the grail server with our subscription lists.
    grailServerConnect(servers, subs,
        std::bind(std::mem_fun(&localizeCallback::packetCallback), &cb_struct, std::placeholders::_1));
  } else {
    std::cerr<<"Error connecting to the GRAIL aggregator.\n";
  }
}

