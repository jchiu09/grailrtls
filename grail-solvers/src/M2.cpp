/*
*
* Copyright (c) 2006 Rutgers University and Konstantinos Kleisouris
* All rights reserved.
*
* Permission to use, copy, modify, and distribute this software and its
* documentation for any purpose, without fee, and without written agreement is
* hereby granted, provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the distribution.
*
* 3. Neither the name of the University nor the names of its
* contributors may be used to endorse or promote products derived from
* this software without specific prior written permission.
*
* IN NO EVENT SHALL RUTGERS UNIVERSITY BE LIABLE TO ANY PARTY FOR DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT
* OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF RUTGERS
* UNIVERSITY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* RUTGERS UNIVERSITY SPECIFICALLY DISCLAIMS ANY WARRANTIES,
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
* AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS
* ON AN "AS IS" BASIS, AND RUTGERS UNIVERSITY HAS NO OBLIGATION TO
* PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*
*
* Author: Konstantinos Kleisouris
* Version: $Id: M2.cpp,v 1.1.2.7 2010/11/12 17:18:57 bfirner Exp $
* Creation Date: 06/27/2006
* Filename: M2.c
* History:
* $Log: M2.cpp,v $
* Revision 1.1.2.7  2010/11/12 17:18:57  bfirner
* Adding test localization solver that uses probability densities of
* boxes rather than ellipses based upon average x and y values.
*
* Revision 1.1.2.5  2010/10/10 18:23:25  bfirner
* Update that get everything working.
*
* Note that CVS is marking many files as changed that were not.
* Fixed bugs in the netbuffer class.
* Fixed flipped y coordinate in the script.
* Fixed timing bug in the localization script.
* Fixed the 128 bit number.
* Added support for position messages.
*
* Revision 1.2  2009/09/23 20:05:28  aopoku
* @kkonst checking in solver that removes negative one columns from training data.
*
* Revision 1.1  2007/09/17 05:01:08  stryke3
* Initial commit.
*
* Revision 1.3  2007/04/30 21:51:07  kkonst
* Added solver code
*
* Revision 1.2  2007/03/12 22:41:13  kkonst
* Modified the solvers so that a signal strength value of -1 is used whenever
* we do not have signal measurement from a specific Access Point.
*
* Revision 1.1  2006/07/19 20:13:20  kkonst
* Added source code of fast solver under demo3
*
* Revision 1.1.1.1  2006/06/27 15:15:52  kkonst
* Imported fast solver code
*
*/

#include "M2.hpp"
#include "rand.h"
#include "slice.h"

#include <numeric>
#include <algorithm>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include <vector>
#include <iostream>

#include <localize_common.hpp>

using namespace std;

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#define INSIDE 1
#define OUTSIDE 2

int rejections = 0, evaluate_energy = 0, proposals = 0;
const double b0_mean = 0.0;
const double b1_mean = 0.0;
const double b0_precision = 0.000001;        // 10^-6
const double b1_precision = 0.000001;        // 10^-6

void conjugateNormalB1i_PosteriorMeanSum_2
( mc_dynamic_state& ds, /* Structure holding pointers to dynamical state */
  const data_specifications& data_spec,
  int target_offset,
  double x_fixed,
  double y_fixed,
  double b,
  double precision, 
  double& nom_sum,
  double& denom_sum
) 
{
  int i;
  double D = 0.0, D2 = 0.0;

  int N_cases = data_spec.N_cases;
  double diff1 = 0.0, diff2 = 0.0;
  double x_value, y_value;

  for (i = 0; i < data_spec.observations_nopredict; i++) {
    if (data_spec.train_signal[target_offset][i] == -1) {
      continue;
    }

    D = data_spec.D[target_offset][i];
    D2 = data_spec.Dsqrd[target_offset][i];

    nom_sum += D * (data_spec.train_signal[target_offset][i] - b);
    denom_sum += D2;
  }

  for (i = data_spec.observations_nopredict; i < N_cases; i++) {
    if (data_spec.train_signal[target_offset][i] == -1) {
      continue;
    }

    x_value = ds.q[ds.first_x_index+i-data_spec.observations_nopredict];
    y_value = ds.q[ds.first_x_index+i+data_spec.predictNo-data_spec.observations_nopredict];
    
    diff1 = x_value-x_fixed;
    diff2 = y_value-y_fixed;
    
    D = log(1+sqrt(diff1*diff1 + diff2*diff2));
    
    nom_sum += D * (data_spec.train_signal[target_offset][i] - b);
    denom_sum += D * D;
  }

  nom_sum *= precision;
  denom_sum *= precision;
}


/*
  This is conjugate Normal sampler for b0i and b1i, where i = 1 to Number of APs
 */
double conjugateNormalB01i
( mc_dynamic_state& ds, /* Structure holding pointers to dynamical state */
  data_specifications& data_spec,  
  int k
  )    
{   
  int N_cases = data_spec.N_cases;
  double A = 0.0;
  double B = 0.0;
  
  if ((k >= ds.first_b0i_index) && (k < ds.first_b1i_index)) {              // This is for b0i
    double prior_precision = ds.q[2];
    double prior_mean = ds.q[0];
    int aps_index = k - ds.first_b0i_index;
    int b1_index = k + data_spec.MAX_APS;
    int tau_index = k + 2*data_spec.MAX_APS;
    int cntminus1 = 0;

    A = prior_mean*prior_precision;

    A += conjugateNormalB0i_PosteriorMeanSum_1(ds, data_spec, aps_index, data_spec.APS_X[aps_index], data_spec.APS_Y[aps_index], 
    					       ds.q[b1_index], ds.q[tau_index], cntminus1);
    
    B = prior_precision + (N_cases-cntminus1) * ds.q[tau_index];
  }
  else if ((k >= ds.first_b1i_index) && (k < ds.first_taui_index)) {        // This is for b1i
    double prior_precision = ds.q[3];
    double prior_mean = ds.q[1];
    int aps_index = k - ds.first_b1i_index;
    int b0_index = k - data_spec.MAX_APS;
    int tau_index = k + data_spec.MAX_APS;
    double A_sum = 0.0;
    double B_sum = 0.0;

    B = prior_precision;
    A = prior_mean*prior_precision;
    
    conjugateNormalB1i_PosteriorMeanSum_2(ds, data_spec, aps_index, data_spec.APS_X[aps_index], data_spec.APS_Y[aps_index], 
					  ds.q[b0_index], ds.q[tau_index], A_sum, B_sum);

    A += A_sum;
    B += B_sum;
  }  

  double post_mean = A / B;
  double post_variance = 1 / B;

  return random_gaussian(post_mean, post_variance);
}


double conjugateGammaTaui_PosteriorMeanSum
( mc_dynamic_state& ds, /* Structure holding pointers to dynamical state */
  data_specifications& data_spec,
  int target_offset,
  double x_fixed,
  double y_fixed,
  double b0,
  double b1
) 
{
  int i;
  double D=0.0, m=0.0, sum = 0.0;
  
  int N_cases = data_spec.N_cases;
  double diff1 = 0.0, diff2 = 0.0, diff3 = 0.0;
  double x_value, y_value;

  for (i = 0; i < data_spec.observations_nopredict ; i++) {
    if (data_spec.train_signal[target_offset][i] == -1) {
      continue;
    }

    D = data_spec.D[target_offset][i];
    m = b0 + (b1 * D);
    
    diff3 = data_spec.train_signal[target_offset][i] - m;
    
    sum += diff3 * diff3;    // Likelihood: Si
  }
  
  for (i = data_spec.observations_nopredict; i < N_cases; i++) {
    if (data_spec.train_signal[target_offset][i] == -1) {
      continue;
    }

    x_value = ds.q[ds.first_x_index+i-data_spec.observations_nopredict];
    y_value = ds.q[ds.first_x_index+i+data_spec.predictNo-data_spec.observations_nopredict];
    
    diff1 = x_value-x_fixed;
    diff2 = y_value-y_fixed;
    
    D = log(1+sqrt(diff1*diff1 + diff2*diff2));
    m = b0 + (b1 * D);
    
    diff3 = data_spec.train_signal[target_offset][i] - m;
    
    sum += diff3 * diff3;    // Likelihood: Si
  }

  return sum;
}


/*
  This is conjugate Gamma sampler for tau i 
 */
double conjugateGammaTaui
( mc_dynamic_state& ds, /* Structure holding pointers to dynamical state */
  data_specifications& data_spec,  
  int k
  )   		       
{
  int N_cases = data_spec.N_cases;
  double prior_shape = 0.001;
  double prior_mean = 0.001;
  int aps_index = k - ds.first_taui_index;
  int b0_index = k - 2*data_spec.MAX_APS;
  int b1_index = k - data_spec.MAX_APS;

  int cntminus1 = 0;
  int i;
  for (i = 0; i < N_cases; i++) {
    if (data_spec.train_signal[aps_index][i] == -1) {
      cntminus1++;
    }
  }
  
  double post_shape = prior_shape + (double)(N_cases-cntminus1)/2;
  double post_mean = 0.0;
  double sum = 0.0;

  
  sum = conjugateGammaTaui_PosteriorMeanSum(ds, data_spec, aps_index, data_spec.APS_X[aps_index], data_spec.APS_Y[aps_index], 
					    ds.q[b0_index], ds.q[b1_index]);
  
  post_mean = sum/2 + prior_mean;

  return rgamma(post_shape, 1/post_mean);
}



/*
  This is conjugate Normal sampler for b0 and b1
 */
double conjugateNormalB01
( mc_dynamic_state& ds, /* Structure holding pointers to dynamical state */
  data_specifications& data_spec,  
  int k
  )    
{   
  double A, B, sum = 0.0;
  B = 0.0;
  int i;
  
  if (k == 0) {
    A = b0_mean * b0_precision;
    B = b0_precision + data_spec.MAX_APS * ds.q[2];

    for (i = 0; i < data_spec.MAX_APS; i++) {
      sum += ds.q[ds.first_b0i_index+i];
    }
  }
  else if (k == 1) {
    A = b1_mean * b1_precision;
    B = b1_precision + data_spec.MAX_APS * ds.q[3];

    for (i = 0; i < data_spec.MAX_APS; i++) {
      sum += ds.q[ds.first_b1i_index+i];
    }
  } else {
    fprintf(stderr, "Value of k given to conjugateNormalB01 was incorred (got k=%d)\n", k);
  }
  
  A += sum * ds.q[k+2];

  double post_mean = A / B;
  double post_variance = 1 / B;

  return random_gaussian(post_mean, post_variance);
}


/*
  This is conjugate Gamma sampler for taub0 and taub1
 */
double conjugateGammaTauB01
( mc_dynamic_state& ds, /* Structure holding pointers to dynamical state */
  data_specifications& data_spec,  
  int k
  )   		       
{
  double prior_shape = 0.001;
  double prior_mean = 0.001;

  double post_shape = prior_shape + (double)data_spec.MAX_APS/2;
  double post_mean = 0.0;
  double sum = 0.0, diff = 0.0;
  int i;

  if (k == 2) {
    for (i = 0; i < data_spec.MAX_APS; i++) {
      diff = ds.q[ds.first_b0i_index+i] - ds.q[0];
      sum +=  diff * diff;
    }
  }
  else if (k == 3) {
    for (i = 0; i < data_spec.MAX_APS; i++) {
      diff = ds.q[ds.first_b1i_index+i] - ds.q[1];
      sum += diff * diff;
    }
  }
    
  post_mean = sum/2 + prior_mean;

  return rgamma(post_shape, 1/post_mean);
}


/**********************************************************************************/


void updateNodes_slice_1
( mc_dynamic_state& ds, /* Structure holding pointers to dynamical state */
  data_specifications& data_spec,
  double width, 
  int max_steps,
  int use_domain,
  int& rejections
  ) {
  int k;  

  for (k = ds.first_x_index; k < ds.first_y_index; k++) {
    int s_factor = 0;	/* Factor for faster shrinkage */
    double s_threshold = 0;/* Threshold for faster shrinkage */  
    mc_slice_1(ds, data_spec, max_steps, s_factor, s_threshold, k, width, use_domain, 1, 0.0, ds.DOMAIN_X, rejections); 
  }

  for (k = ds.first_y_index; k < ds.last_y_index; k++) {
    int s_factor = 0;	/* Factor for faster shrinkage */
    double s_threshold = 0;/* Threshold for faster shrinkage */  
    mc_slice_1(ds, data_spec, max_steps, s_factor, s_threshold, k, width, use_domain, 1, 0.0, ds.DOMAIN_Y, rejections); 
  }

  for (k = 0; k < ds.last_bprior_index; k++) {
    double new_value = conjugateNormalB01(ds, data_spec, k);

    ds.q[k] = new_value;
  }

  for (k = ds.last_bprior_index; k < ds.last_taubprior_index; k++) {
    double new_value = conjugateGammaTauB01(ds, data_spec, k);

    ds.q[k] = new_value;
  }

  for (k = ds.first_b0i_index; k < ds.first_taui_index; k++) {
    double new_value = conjugateNormalB01i(ds, data_spec, k);
    
    ds.q[k] = new_value;
  }

  for (k = ds.first_taui_index; k < ds.first_x_index; k++) {
    double new_value = conjugateGammaTaui(ds, data_spec, k);
    
    ds.q[k] = new_value;   
  }
}

void updateNodes_slice_1all
( mc_dynamic_state& ds, /* Structure holding pointers to dynamical state */
  data_specifications& data_spec,
  double width, 
  int max_steps,
  int use_domain,
  int& rejections
  ) {
  int k;  

  //Find a new X location
  for (k = ds.first_x_index; k < ds.first_y_index; k++) {
    int s_factor = 0;	/* Factor for faster shrinkage */
    double s_threshold = 0;/* Threshold for faster shrinkage */  
    mc_slice_1(ds, data_spec, max_steps, s_factor, s_threshold, k, width, use_domain, 1, 0.0, ds.DOMAIN_X, rejections);
  }

  //Find a new Y location
  for (k = ds.first_y_index; k < ds.last_y_index; k++) {
    int s_factor = 0;	/* Factor for faster shrinkage */
    double s_threshold = 0;/* Threshold for faster shrinkage */  
    mc_slice_1(ds, data_spec, max_steps, s_factor, s_threshold, k, width, use_domain, 1, 0.0, ds.DOMAIN_Y, rejections);
  }

  for (k = 0; k < ds.last_bprior_index; k++) {
    double new_value = conjugateNormalB01(ds, data_spec, k);

    ds.q[k] = new_value;
  }

  for (k = ds.last_bprior_index; k < ds.last_taubprior_index; k++) {
    double new_value = conjugateGammaTauB01(ds, data_spec, k);

    ds.q[k] = new_value;
  }
  
  for (k = ds.first_b0i_index; k < ds.first_b1i_index; k++) {
    int s_factor = 0;	/* Factor for faster shrinkage */
    double s_threshold = 0;/* Threshold for faster shrinkage */  
    
    mc_slice_1(ds, data_spec, 10, s_factor, s_threshold, k, 1.0, 0, 1, -1000000.0, 0.0, rejections);
  }

  for (k = ds.first_b1i_index; k < ds.first_taui_index; k++) {
    double new_value = conjugateNormalB01i(ds, data_spec, k);
    
    ds.q[k] = new_value;
  }

  for (k = ds.first_taui_index; k < ds.first_x_index; k++) {
    double new_value = conjugateGammaTaui(ds, data_spec, k);
    
    ds.q[k] = new_value;   
  }
}



/**********************************************************************************/

/* EVALUATE POTENTIAL 2D ENERGY */
void mc_app_energy_2D
( mc_dynamic_state& ds,	/* Current dyanamical state */
  data_specifications& data_spec,  
  double& energy,	/* Place to store energy */
  int k,
  int grad
) {  
  double e = 0.0;
  double D, D1, m, b0, b1, S, sigma, temp;
  int target_index;
  double diff1 = 0.0, diff2 = 0.0;
  int x_index, y_index, i;
  
  double fx_deriv = 0.0, fy_deriv = 0.0;
  
  x_index = k;
  y_index = k+data_spec.predictNo;
  target_index = data_spec.observations_nopredict + x_index - ds.first_x_index;
  
  for (i = 0; i < data_spec.MAX_APS; i++) {
    if (data_spec.train_signal[i][target_index] == -1) {
      continue;
    }

    diff1 = ds.q[x_index]-data_spec.APS_X[i];
    diff2 = ds.q[y_index]-data_spec.APS_Y[i];
    D = sqrt(diff1*diff1 + diff2*diff2);
    D1 = log(1 + D);
    b0 = ds.q[i + ds.first_b0i_index];
    b1 = ds.q[i + ds.first_b1i_index];
    sigma = 1/ds.q[i+ds.first_taui_index];
    S = data_spec.train_signal[i][target_index];

    m = b0 + b1 * D1;
    e += normal_dist(S, m, sigma); // Likelihood: This is for Si

    if (grad) { 
      temp = - ((S - m) * b1) / (sigma * (1 + D) * D);
      fx_deriv += diff1 * temp;        // NOT SURE WHETHER diff1 should be absolute
      fy_deriv += diff2 * temp;        // NOT SURE WHETHER diff2 should be absolute
    }
  }
  
  if (grad) {
    ds.grad[k] = fx_deriv;
    ds.grad[k+data_spec.predictNo] = fy_deriv;
  }

  energy = e;

  evaluate_energy++;
}


/* EVALUATE POTENTIAL ENERGY */
void mc_app_energy_mult
( mc_dynamic_state& ds,	/* Current dyanamical state */
  data_specifications& data_spec,  
  double& energy,	/* Place to store energy, null if not required */
  int grad
) {  
  double e = 0.0;
  double D, D1, m, b0, b1, S, sigma, temp;
  int target_index;
  double diff1 = 0.0, diff2 = 0.0;
  int x_index, y_index;
  int i, k;
  double fx_deriv = 0.0, fy_deriv = 0.0;

  //double x_prior_energy = unif_dist(0, DOMAIN_X);      // Prior: X[k]
  //double y_prior_energy = unif_dist(0, DOMAIN_Y);      // Prior: Y[k]
  
  //for (k = first_x_index; k < first_y_index; k++) {
  // e += x_prior_energy;
  //}

  //for (k = first_y_index; k < last_y_index; k++) {
  // e += y_prior_energy;
  //}

  for (k = ds.first_x_index; k < ds.first_y_index; k++) {          // A pair of (x, y) has the same likelihood
    x_index = k;
    y_index = k+data_spec.predictNo;
    target_index = data_spec.observations_nopredict + x_index - ds.first_x_index;

    for (i = 0; i < data_spec.MAX_APS; i++) {
      if (data_spec.train_signal[i][target_index] == -1) {
	continue;
      }
      
      diff1 = ds.q[x_index]-data_spec.APS_X[i];
      diff2 = ds.q[y_index]-data_spec.APS_Y[i];
      D = sqrt(diff1*diff1 + diff2*diff2);
      D1 = log(1 + D);
      b0 = ds.q[i + ds.first_b0i_index];
      b1 = ds.q[i + ds.first_b1i_index];
      sigma = 1/ds.q[i+ds.first_taui_index];
      S = data_spec.train_signal[i][target_index];
      
      m = b0 + b1 * D1;
      e += normal_dist(S, m, sigma); // Likelihood: This is for Si
      
      if (grad) { 
	temp = -(S - b0 - b1 * D1) / (sigma * (1 + D) * D);
	fx_deriv += b1 * diff1 * temp;        // NOT SURE WHETHER diff1 should be absolute
	fy_deriv += b1 * diff2 * temp;        // NOT SURE WHETHER diff2 should be absolute
      }   
    }

    if (grad) {
      ds.grad[k] = fx_deriv;
      ds.grad[k+data_spec.predictNo] = fy_deriv;
    }
  }
  
  energy = e;

  evaluate_energy++;
}


/* PERFORM MULTIVARIATE SLICE SAMPLING WITH HYPERRECTANGLES. */
void mc_slice_mult
( mc_dynamic_state& ds,	/* State to update */
  data_specifications& data_spec,  
  std::vector<double>& save,	/* Place to save current state */
  std::vector<double>& lowb,	/* Storage for low bounds of hyperrectangle */
  std::vector<double>& highb,	/* Storage for high bounds of hyperrectangle */
  int g_shrink,		/* Shrink based on gradient? */
  double width,
  int use_domain
  )
{
  double slice_point, maxp, pr;
  int k;
  //FIXME maxk might not be initialized before being used as an index if the x-range is 0.
  int maxk = 0;

  if (use_domain) {
    for (k = ds.first_x_index; k<ds.first_y_index; k++) { 
      lowb[k]  = 0.0; 
      highb[k] = ds.DOMAIN_X;
    }

    for (k = ds.first_y_index; k<ds.last_y_index; k++) { 
      lowb[k]  = 0.0; 
      highb[k] = ds.DOMAIN_Y;
    }
  }
  else {
    for (k = ds.first_x_index; k<ds.first_y_index; k++) { 
      lowb[k]  = ds.q[k] - rand_uniopen() * width;  
      highb[k] = lowb[k] + width;

      if (lowb[k] < 0) lowb[k] = 0.0;
      if (highb[k] > ds.DOMAIN_X) highb[k] = ds.DOMAIN_X;
    }

    for (k = ds.first_y_index; k<ds.last_y_index; k++) { 
      lowb[k]  = ds.q[k] - rand_uniopen() * width;  
      highb[k] = lowb[k] + width;

      if (lowb[k] < 0) lowb[k] = 0.0;
      if (highb[k] > ds.DOMAIN_Y) highb[k] = ds.DOMAIN_Y;      
    }
  }

  mc_app_energy_mult (ds, data_spec, ds.pot_energy, 0);

  slice_point = ds.pot_energy + rand_exp();
  //mc_value_copy (save, ds.q, ds.dim);
  //Copy the values from save into the position vector in ds
  std::copy(save.begin(), save.begin()+ds.dim, ds.q.begin());

  for (;;)
  { 
    for (k = ds.first_x_index; k<ds.last_y_index; k++) { 
      ds.q[k]  = lowb[k] + rand_uniopen() * (highb[k]-lowb[k]);
    }

    mc_app_energy_mult (ds, data_spec, ds.pot_energy, g_shrink);

    if (ds.pot_energy<=slice_point) { 
      return;
    }

    rejections++;

    if (g_shrink!=0) {  // Use gradient
      maxp = -1;

      for (k = ds.first_x_index; k<ds.last_y_index; k++) {     
        pr = ds.grad[k] * (highb[k]-lowb[k]);
        if (pr<0) pr = -pr;

        if (pr>maxp) { 
          maxp = pr;
          maxk = k;
        }	
      }

      if (ds.q[maxk]>save[maxk]) {      
        highb[maxk] = ds.q[maxk];
      }
      else { 
        lowb[maxk] = ds.q[maxk];
      }      
    }
    else { // Not Use gradient
      for (k = ds.first_x_index; k<ds.last_y_index; k++) {     
        if (ds.q[k]>save[k]) { 
          highb[k] = ds.q[k];
        }
        else { 
          lowb[k] = ds.q[k];
        }      
      }
    }
  }
}


void updateNodes_slice_2D
( mc_dynamic_state& ds, /* Structure holding pointers to dynamical state */
  data_specifications& data_spec,
  std::vector<double>& save,	/* Place to save current state */
  std::vector<double>& lowb,	/* Storage for low bounds of hyperrectangle */
  std::vector<double>& highb,/* Storage for high bounds of hyperrectangle */
  int g_shrink,
  double width, 
  int use_domain,
  int& rejections
  ) {
  int k;  

  for (k = ds.first_x_index; k < ds.first_y_index; k++) {
    mc_slice_mult2D(ds, data_spec, save, lowb, highb, k, g_shrink, width, use_domain,
		    0.0, ds.DOMAIN_X, 0.0, ds.DOMAIN_Y, rejections);
  }
  
  for (k = 0; k < ds.last_bprior_index; k++) {
    double new_value = conjugateNormalB01(ds, data_spec, k);

    ds.q[k] = new_value;
  }

  for (k = ds.last_bprior_index; k < ds.last_taubprior_index; k++) {
    double new_value = conjugateGammaTauB01(ds, data_spec, k);

    ds.q[k] = new_value;
  }

  for (k = ds.first_b0i_index; k < ds.first_taui_index; k++) {
    double new_value = conjugateNormalB01i(ds, data_spec, k);
    
    ds.q[k] = new_value;
  }

  for (k = ds.first_taui_index; k < ds.first_x_index; k++) {
    double new_value = conjugateGammaTaui(ds, data_spec, k);
    
    ds.q[k] = new_value;   
  }
}


void updateNodes_slice_2Dall
( mc_dynamic_state& ds, /* Structure holding pointers to dynamical state */
  data_specifications& data_spec,
  std::vector<double>& save,	/* Place to save current state */
  std::vector<double>& lowb,	/* Storage for low bounds of hyperrectangle */
  std::vector<double>& highb,/* Storage for high bounds of hyperrectangle */
  int g_shrink,
  double width, 
  int use_domain
  ) {
  int k;  

  //mc_slice_mult(ds, data_spec, save, lowb, highb, g_shrink, width, use_domain);
  
  for (k = ds.first_x_index; k < ds.first_y_index; k++) {
    mc_slice_mult2D(ds, data_spec, save, lowb, highb, k, g_shrink, width, use_domain,
		    0.0, ds.DOMAIN_X, 0.0, ds.DOMAIN_Y, rejections);
  }
  
  for (k = 0; k < ds.last_bprior_index; k++) {
    double new_value = conjugateNormalB01(ds, data_spec, k);

    ds.q[k] = new_value;
  }

  for (k = ds.last_bprior_index; k < ds.last_taubprior_index; k++) {
    double new_value = conjugateGammaTauB01(ds, data_spec, k);

    ds.q[k] = new_value;
  }

   for (k = ds.first_b0i_index; k < ds.first_b1i_index; k++) {
    int s_factor = 0;	/* Factor for faster shrinkage */
    double s_threshold = 0;/* Threshold for faster shrinkage */  
    
    mc_slice_1(ds, data_spec, 10, s_factor, s_threshold, k, 1.0, 0, 1, -1000000.0, 0.0, rejections);
  }

  for (k = ds.first_b1i_index; k < ds.first_taui_index; k++) {
    double new_value = conjugateNormalB01i(ds, data_spec, k);
    
    ds.q[k] = new_value;
  }

  for (k = ds.first_taui_index; k < ds.first_x_index; k++) {
    double new_value = conjugateGammaTaui(ds, data_spec, k);
    
    ds.q[k] = new_value;   
  }
}



/**********************************************************************************/

/* PERFORM METROPOLIS UPDATE ON TWO COMPONENT AT A TIME. */
void mc_met_2D
( mc_dynamic_state& ds,	/* State to update */
  data_specifications& data_spec,  
  double sf,	
  double temperature,
  int b_accept,		/* Use Barker/Boltzmann acceptance probability? */
  int k
)
{
  double old_energy, qsave_x, qsave_y, U, a, delta;

  mc_app_energy_2D(ds, data_spec, ds.pot_energy, k, 0);
  
  old_energy = ds.pot_energy; 
  qsave_x = ds.q[k];  
  qsave_y = ds.q[k+data_spec.predictNo];  
  
  ds.q[k] = rand_uniopen() * ds.DOMAIN_X;
  ds.q[k+data_spec.predictNo] = rand_uniopen() * ds.DOMAIN_Y;  

  mc_app_energy_2D(ds, data_spec, ds.pot_energy, k, 0);
  
  proposals += 1;
  delta = ds.pot_energy - old_energy;
  
  U = rand_uniform(); /* Do every time to keep in sync for coupling purposes*/
  
  a = exp(-delta/temperature);
  if (b_accept) { 
    a = 1/(1+1/a);
  }

  if (U >= a) {   // rejection
    rejections += 1;   
    ds.pot_energy = old_energy;    
    ds.q[k] = qsave_x;
    ds.q[k+data_spec.predictNo] = qsave_y;
  }
}


void updateNodes_met_1
( mc_dynamic_state& ds, /* Structure holding pointers to dynamical state */
  data_specifications& data_spec,
  double sf, 
  double temperature,
  int b_accept,
  int use_domain
  ) {
  int k;  

  for (k = ds.first_x_index; k < ds.first_y_index; k++) {
    mc_met_1(ds, data_spec, sf,	temperature, b_accept, k, use_domain, 0.0, ds.DOMAIN_X, proposals, rejections);
  }
  
  for (k = ds.first_y_index; k < ds.last_y_index; k++) {
    mc_met_1(ds, data_spec, sf,	temperature, b_accept, k, use_domain, 0.0, ds.DOMAIN_Y, proposals, rejections);
  }

  for (k = 0; k < ds.last_bprior_index; k++) {
    double new_value = conjugateNormalB01(ds, data_spec, k);

    ds.q[k] = new_value;
  }

  for (k = ds.last_bprior_index; k < ds.last_taubprior_index; k++) {
    double new_value = conjugateGammaTauB01(ds, data_spec, k);

    ds.q[k] = new_value;
  }

  for (k = ds.first_b0i_index; k < ds.first_taui_index; k++) {
    double new_value = conjugateNormalB01i(ds, data_spec, k);
    
    ds.q[k] = new_value;
  }

  for (k = ds.first_taui_index; k < ds.first_x_index; k++) {
    double new_value = conjugateGammaTaui(ds, data_spec, k);
    
    ds.q[k] = new_value;   
  }
}


void updateNodes_met_1all
( mc_dynamic_state& ds, /* Structure holding pointers to dynamical state */
  data_specifications& data_spec,
  double sf, 
  double temperature,
  int b_accept,
  int use_domain
  ) {
  int k;  

  for (k = ds.first_x_index; k < ds.first_y_index; k++) {
    mc_met_1(ds, data_spec, sf,	temperature, b_accept, k, use_domain, 0.0, ds.DOMAIN_X, proposals, rejections);
  }
  
  for (k = ds.first_y_index; k < ds.last_y_index; k++) {
    mc_met_1(ds, data_spec, sf,	temperature, b_accept, k, use_domain, 0.0, ds.DOMAIN_Y, proposals, rejections);
  }

  for (k = 0; k < ds.last_bprior_index; k++) {
    double new_value = conjugateNormalB01(ds, data_spec, k);

    ds.q[k] = new_value;
  }

  for (k = ds.last_bprior_index; k < ds.last_taubprior_index; k++) {
    double new_value = conjugateGammaTauB01(ds, data_spec, k);

    ds.q[k] = new_value;
  }

  for (k = ds.first_b0i_index; k < ds.first_b1i_index; k++) {
    int s_factor = 0;	/* Factor for faster shrinkage */
    double s_threshold = 0;/* Threshold for faster shrinkage */  
    
    mc_slice_1(ds, data_spec, 10, s_factor, s_threshold, k, 1.0, 0, 1, -1000000.0, 0.0, rejections);
  }

  for (k = ds.first_b1i_index; k < ds.first_taui_index; k++) {
    double new_value = conjugateNormalB01i(ds, data_spec, k);
    
    ds.q[k] = new_value;
  }

  for (k = ds.first_taui_index; k < ds.first_x_index; k++) {
    double new_value = conjugateGammaTaui(ds, data_spec, k);
    
    ds.q[k] = new_value;   
  }
}
/**********************************************************************************/

/* PERFORM MULTIVARIATE SLICE SAMPLING WITH INSIDE REFLECTION. */
void mc_slice_inside_2D
( mc_dynamic_state& ds,	/* State to update */
  data_specifications& data_spec,  
  int steps,		/* Number of steps to take */
  double sf,            /* scale factor */
  int k                 /* index for x in ds.q */
)
{
  double slice_point;  
  double gmag, proj;
  double old_pot;
  int rejects, rejected;

  mc_app_energy_2D(ds, data_spec, ds.pot_energy, k, 1);

  slice_point = ds.pot_energy + rand_exp();
  rejects = 0;
  
  for (int i = 0; i<steps; i++)
  {
    double x_old = ds.q[k];
    double y_old = ds.q[k+data_spec.predictNo];
    double xp_old = ds.grad[k];
    double yp_old = ds.grad[k+data_spec.predictNo];

    old_pot = ds.pot_energy;

    ds.q[k] += sf * ds.p[k];
    ds.q[k+data_spec.predictNo] += sf * ds.p[k+data_spec.predictNo];
    
    if ( (ds.q[k] < 0.0) || (ds.q[k] > ds.DOMAIN_X) ||
	 (ds.q[k+data_spec.predictNo] < 0.0) || (ds.q[k+data_spec.predictNo] > ds.DOMAIN_Y) ) {
      ds.q[k] = x_old;                    // Restore old ds.q[k]
      ds.q[k+data_spec.predictNo] = y_old;
      
      ds.p[k] = - ds.p[k];               // Retrace path the opposity way
      ds.p[k+data_spec.predictNo] = - ds.p[k+data_spec.predictNo];
      
      continue;
    }

    mc_app_energy_2D(ds, data_spec, ds.pot_energy, k, 1);

    if (ds.pot_energy>slice_point) 
    { 
      ds.q[k] = x_old;                    // Restore old ds.q[k]
      ds.q[k+data_spec.predictNo] = y_old;
      ds.grad[k] = xp_old;                // Restore old ds.grad[k]
      ds.grad[k+data_spec.predictNo] = yp_old;
      ds.pot_energy = old_pot;

      gmag = 0.0;
      gmag += ds.grad[k]*ds.grad[k];
      gmag += ds.grad[k+data_spec.predictNo]*ds.grad[k+data_spec.predictNo];
      
      proj = 0.0;
      proj += ds.p[k]*ds.grad[k];
      proj += ds.p[k+data_spec.predictNo]*ds.grad[k+data_spec.predictNo];

      rejected = 1;

      if (gmag>1e-30) // change direction
      { 
	ds.q[k] += sf * (2*ds.grad[k]*proj/gmag - ds.p[k]);
	ds.q[k+data_spec.predictNo] += sf * (2*ds.grad[k+data_spec.predictNo]*proj/gmag - ds.p[k+data_spec.predictNo]);

	mc_app_energy_2D(ds, data_spec, ds.pot_energy, k, 0);

        if (ds.pot_energy>slice_point)
        { 
	  ds.p[k] = 2*ds.grad[k]*proj/gmag - ds.p[k];
	  ds.p[k+data_spec.predictNo] = 2*ds.grad[k+data_spec.predictNo]*proj/gmag - ds.p[k+data_spec.predictNo];
	
          rejected = 0;
        }

	ds.q[k] = x_old;                 // Restore old ds.q[k]
	ds.q[k+data_spec.predictNo] = y_old;
        ds.pot_energy = old_pot;
      }

      ds.p[k] = - ds.p[k];
      ds.p[k+data_spec.predictNo] = - ds.p[k+data_spec.predictNo];
     
      rejects += rejected;
    }
  }
     
  ds.p[k] = - ds.p[k];
  ds.p[k+data_spec.predictNo] = - ds.p[k+data_spec.predictNo];
  
  rejections += rejects;
}


/* PERFORM MULTIVARIATE SLICE SAMPLING WITH OUTSIDE REFLECTION. */
void mc_slice_outside_2D
( mc_dynamic_state& ds,	/* State to update */
  data_specifications& data_spec,  
  int steps,		/* Number of steps to take */
  int in_steps,		/* Max number of steps inside slice */
  double sf,            /* scale factor */
  int k                 /* index for x in ds.q */
)
{
  double slice_point;  
  double gmag, proj;
  double old_pot;

  int kt=0, ki=0;

  mc_app_energy_2D(ds, data_spec, ds.pot_energy, k, 1);
  //mc_gradient_2D(ds, data_spec, k);

  double x_old = ds.q[k];
  double y_old = ds.q[k+data_spec.predictNo];
  double xp_old = ds.grad[k];
  double yp_old = ds.grad[k+data_spec.predictNo];

  old_pot = ds.pot_energy;
  slice_point = ds.pot_energy + rand_exp();

  for (;;)
  {
    double x_prev = ds.q[k];
    double y_prev = ds.q[k+data_spec.predictNo];

    ds.q[k] += sf * ds.p[k];
    ds.q[k+data_spec.predictNo] += sf * ds.p[k+data_spec.predictNo];

    if ( (ds.q[k] < 0.0) || (ds.q[k] > ds.DOMAIN_X) ||
	 (ds.q[k+data_spec.predictNo] < 0.0) || (ds.q[k+data_spec.predictNo] > ds.DOMAIN_Y) ) {
      ds.q[k] = x_prev;                    // Restore old ds.q[k]
      ds.q[k+data_spec.predictNo] = y_prev;
      
      ds.p[k] = - ds.p[k];                // Retrace path the opposity way
      ds.p[k+data_spec.predictNo] = - ds.p[k+data_spec.predictNo];
      
      kt += 1;
      if (kt>=steps) break;

      continue;
    }

    kt += 1;

    mc_app_energy_2D(ds, data_spec, ds.pot_energy, k, 1);

    if (ds.pot_energy<=slice_point) ki += 1;
    if (kt>=steps || ki>=in_steps) break;

    //mc_gradient_2D(ds, data_spec, k);

    if (ds.pot_energy>slice_point) { 
      gmag = 0.0;
      gmag += ds.grad[k]*ds.grad[k];
      gmag += ds.grad[k+data_spec.predictNo]*ds.grad[k+data_spec.predictNo];
      
      proj = 0.0;
      proj += ds.p[k]*ds.grad[k];
      proj += ds.p[k+data_spec.predictNo]*ds.grad[k+data_spec.predictNo];
      
      if (gmag>1e-30) {       // change direction
	ds.p[k] = ds.p[k] - 2*ds.grad[k]*proj/gmag;
	ds.p[k+data_spec.predictNo] = ds.p[k+data_spec.predictNo] - 2*ds.grad[k+data_spec.predictNo]*proj/gmag;
      }
    }
  }

  if (ds.pot_energy<=slice_point) { 
    ds.p[k] = - ds.p[k];
    ds.p[k+data_spec.predictNo] = - ds.p[k+data_spec.predictNo];
  }
  else { // Reject the whole trajectory
    ds.q[k] = x_old;
    ds.q[k+data_spec.predictNo] = y_old;
    ds.grad[k] = xp_old;
    ds.grad[k+data_spec.predictNo] = yp_old;     
    ds.pot_energy = old_pot;

    rejections++;
  }
}


void updateNodes_ref_2D
( mc_dynamic_state& ds, /* Structure holding pointers to dynamical state */
  data_specifications& data_spec,
  double sf,   /* scale factor */
  int steps,   /* total number of steps */
  int in_steps, /* steps inside the slice */
  int in_out_side
  ) {
  int k;  
 
  mc_heatbath(ds, 1.0, 0.0, ds.first_x_index, ds.last_y_index);

  for (k = ds.first_x_index; k < ds.first_y_index; k++) {
    if (in_out_side == OUTSIDE)
      mc_slice_outside_2D(ds, data_spec, steps, in_steps, sf, k);
    else if (in_out_side == INSIDE)
      mc_slice_inside_2D(ds, data_spec, steps, sf, k);
  }

  for (k = 0; k < ds.last_bprior_index; k++) {
    double new_value = conjugateNormalB01(ds, data_spec, k);

    ds.q[k] = new_value;
  }

  for (k = ds.last_bprior_index; k < ds.last_taubprior_index; k++) {
    double new_value = conjugateGammaTauB01(ds, data_spec, k);

    ds.q[k] = new_value;
  }

  for (k = ds.first_b0i_index; k < ds.first_taui_index; k++) {
    double new_value = conjugateNormalB01i(ds, data_spec, k);
    
    ds.q[k] = new_value;
  }

  for (k = ds.first_taui_index; k < ds.first_x_index; k++) {
    double new_value = conjugateGammaTaui(ds, data_spec, k);
    
    ds.q[k] = new_value;   
  }
}


/* DISPLAY USAGE MESSAGE AND EXIT. */

static void usage(void)
{
  fprintf (stderr, "Usage: M2 [Burnin Iter] [Additional Iter] [Training Data file] [APs file] [X domain] [Y domain] [slice1|slice2d|met1|ref2d] {-a Stats file} {-s [Samples file]} {-l [Likelihood file]} {-t [Time file]} {-v [Informative file]} {-w [width]} {-m [maxsteps]} {-f [scaling factor]} {-p [total steps]} {-i|-o} {-b} {-d} {-g}\n");
  exit(1);
}


//M2 solver
std::vector<Result> m2solve( int argc, char** argv,
    const TrainingData& train_data,
    const LandmarkData& ap_data,
    double DOMAIN_X,
    double DOMAIN_Y,
    int burnin,
    int additional_iter,
    SampleMethod sampling_method)
{ 
  const float outside_drift = DOMAIN_X/3.0;
  //Add the allowable drift to each dimension and constrain at the end
  DOMAIN_X += 2*outside_drift;
  DOMAIN_Y += 2*outside_drift;

  int c;
  int g_shrink = 0;
  int max_steps = 0;	/* Maximum number of intervals, zero for unlimited; if negative, intervals are found by doubling */
  double width = 1.0;   /* Estimate of the size of the slice */
  double sf = 1.0;   /* Estimate of the size of the slice */
  int use_domain = 0, b_accept = 0;
  int steps = 35, in_steps = steps;   /* Total number of steps, steps inside slice */
  int in_out_side = -1;  /* Will be either inside or outside */

  for (int i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      c = argv[i][1];
      
      switch (c) {
        case 'w':
          width = atof(argv[++i]); 
          break;	
        case 'm':
          max_steps = atoi(argv[++i]); 
          break;	
        case 'd':
          use_domain = 1;
          break;
        case 'g':
          g_shrink = 1;
          break;	
        case 'f':
          sf = atof(argv[++i]); 
          break;		
        case 'b':
          b_accept = 1;
          break;
        case 'p':
          steps = atoi(argv[++i]); 
          in_steps = steps;
          break;	
        case 'i':
          in_out_side = INSIDE; 
          break;		
        case 'o':
          in_out_side = OUTSIDE; 
          break;				
      }
    }
  }

  if ( sampling_method < slice1 or sampling_method > met1all ) {
    fprintf(stderr, "Method is not any of [slice1|slice2d|met1|ref2d|slice1all|slice2dall|met1all]\n");
    exit(1);
  }

  int N_inputs = 2;
  int total_iterations = additional_iter + burnin;
  int total_network_variables = 0;

  for (auto T = train_data.begin(); T != train_data.end(); ++T) {
    if (ap_data.size() != T->rssis.size()) {
      std::cerr<<"RSSI values in training data do not match AP data.\n";
      std::cerr<<"Aborting.\n";
      return std::vector<Result>(0);
    }
  }
  double_matrix train_signal_notclean(ap_data.size(), std::vector<double>(train_data.size()));
  std::vector<double> APS_X_NOTCLEAN(ap_data.size());
  std::vector<double> APS_Y_NOTCLEAN(ap_data.size());
  std::vector<double> train_x(train_data.size());
  std::vector<double> train_y(train_data.size());

  //Add the allowable drift to each dimension and constrain at the end
  //For each dimension in the x and y locations of the input data add
  //in the outside drift value.
  //This is to compensate for some items being at the border of the domain.

  //The number of APs before pruning APs with no data.
  int MAX_APS_NOTCLEAN = ap_data.size();
  std::transform(ap_data.begin(), ap_data.end(), APS_X_NOTCLEAN.begin(),
      [=](const std::pair<double, double>& p) { return p.first + outside_drift;});
  std::transform(ap_data.begin(), ap_data.end(), APS_Y_NOTCLEAN.begin(),
      [=](const std::pair<double, double>& p) { return p.second + outside_drift;});

  std::transform(train_data.begin(), train_data.end(), train_x.begin(),
      [=](const Sample& s) { return s.x + outside_drift;});
  std::transform(train_data.begin(), train_data.end(), train_y.begin(),
      [=](const Sample& s) { return s.y + outside_drift;});

  //N cases is the number of transmitters in this dataset
  int N_cases = train_data.size();

  for (int i = 0; i < MAX_APS_NOTCLEAN; ++i) {
    for (int j = 0 ; j < N_cases; ++j) {
      train_signal_notclean[i][j] = train_data[j].rssis[i];
    }
  }

  //The number of samples with unknown locations (as indicated by NAN values
  //in their coordinates.
  int predictNo = std::accumulate(train_data.begin(), train_data.end(), 0,
      [](int sum, const Sample& arg) { return sum + (isnan(arg.x) ? 1 : 0); });
  //Replace all NaN with 0.0.
  std::for_each(train_x.begin(), train_x.end(), [](double& arg) {
      if (isnan(arg)) { arg = 0.0;}});
  std::for_each(train_y.begin(), train_y.end(), [](double& arg) {
      if (isnan(arg)) { arg = 0.0;}});
  
  int MAX_APS;
  std::vector<double> aps_x;
  std::vector<double> aps_y;
  double_matrix train_signal;
  {
    std::vector<double> ap_data(MAX_APS_NOTCLEAN);
    //Check the data from each AP in all of the samples. If any AP gave only
    //values of -1 then remove its rssis from the data.
    std::vector<int> empty_columns;
    for (int column = 0; column < MAX_APS_NOTCLEAN; ++column) {
      bool empty_ap = std::accumulate(train_signal_notclean.begin(), train_signal_notclean.end(), true,
          [=](bool all_bad, const std::vector<double>& arg) {
          return all_bad and (arg[column] == -1.0);});
      if (empty_ap) {
        empty_columns.push_back(column);
      }
    }


    //Remove the columns of data with indices in the empty_columns vector
    //from the training data and the ap data

    //Get the ap locations for aps with empty data sets (all -1 RSSI)
    MAX_APS = MAX_APS_NOTCLEAN - empty_columns.size();

    auto next_empty = empty_columns.begin();
    int skipped = 0;
    for (int ap = 0; ap < MAX_APS_NOTCLEAN; ++ap) {
      //Skip this entry if this AP contributes no data
      if (next_empty != empty_columns.end() and *next_empty == ap) {
        ++next_empty;
        ++skipped;
      } else {
        //Since ths row is nonempty record data from it - the x and y location of
        //the access point and the RSS values that it saw for each transmitter.
        aps_x.push_back(APS_X_NOTCLEAN[ap]);
        aps_y.push_back(APS_Y_NOTCLEAN[ap]);
        train_signal.push_back(std::vector<double>(N_cases));
        //For each transmitter store data from this ap
        for (int txer = 0; txer < N_cases; ++txer) {
          //If the signal came in with a -1 that means there was no data. However,
          //this receiver was working during the last period so this receiver was
          //too far away to receive this transmitter's signal.
          //TODO FIXME Should be a way of getting better results here.
          if (train_signal_notclean[ap][txer] == -1.0) {
            train_signal_notclean[ap][txer] = -95.0;
          }
          //Change the signal data to absolute values for the solver.
          train_signal.back()[txer] = fabs(train_signal_notclean[ap][txer]);
        }
      }
    } 
  }

  // The number of training points
  int observations_nopredict = N_cases - predictNo;

  data_specifications data_spec(N_inputs, MAX_APS, N_cases, train_x, train_y, train_signal,
        predictNo, observations_nopredict, aps_x, aps_y);

  //TODO FIXME there should be multiple arrays, not one array with a bunch of stuff in it.
  total_network_variables = 4+MAX_APS*3+2*predictNo;

  double_matrix samples_in_memory =
    double_matrix(total_network_variables, std::vector<double>(additional_iter));
 
  //Initialize the dynamic state struct
  //TODO FIXME declare an instance of the struct here and make this the constructor
  mc_dynamic_state ds(MAX_APS, predictNo, total_network_variables, DOMAIN_X, DOMAIN_Y);

  rejections = 0;
  evaluate_energy = 0;
  
  //TODO FIXME add the other sampling methods back into this piece of code
  if ( sampling_method == slice1all) {       // single variate slice sampling
    for (int i = 0; i < total_iterations; i++) {
      updateNodes_slice_1all(ds, data_spec, width, max_steps, use_domain, rejections);
      //The first <burnin> values can be ignored.
      if (i >= burnin) {
        for (int j = 0; j < total_network_variables; j++) {
          samples_in_memory[j][i-burnin] = ds.q[j];
        }
      }  
    }
  } else if ( sampling_method == slice1) {
    for (int i = 0; i < total_iterations; i++) {
      updateNodes_slice_1(ds, data_spec, width, max_steps, use_domain, rejections);
      //The first <burnin> values can be ignored.
      if (i >= burnin) {
        for (int j = 0; j < total_network_variables; j++) {
          samples_in_memory[j][i-burnin] = ds.q[j];
        }
      }  
    }
  } else if ( sampling_method == slice2d) {  // 2d variate slice sampling
    std::vector<double> save(total_network_variables);  // For X, Ys to localize
    std::vector<double> lowb(total_network_variables);  // For Low bandwidth of X, Ys
    std::vector<double> highb(total_network_variables); // For High bandwidth of X, Ys

    for (int i = 0; i < total_iterations; i++) {
      updateNodes_slice_2D(ds, data_spec, save, lowb, highb, g_shrink, width, use_domain, rejections);
      //The first <burnin> values can be ignored.
      if (i >= burnin) {
        for (int j = 0; j < total_network_variables; j++) {
          samples_in_memory[j][i-burnin] = ds.q[j];
        }
      }  
    }   
  }
  /*
   *TODO FIXME add support for this
  else if (!strcmp(sampling_method, "slice2dall")) {  // 2d variate slice sampling
    double *save = chk_alloc(total_network_variables, sizeof (double)); // For X, Ys to localize
    double *lowb = chk_alloc(total_network_variables, sizeof (double));   // For Low bandwidth of X, Ys
    double *highb = chk_alloc(total_network_variables, sizeof (double));   // For High bandwidth of X, Ys
    
    for (i = 0; i < total_iterations; i++) {
      iterationNo = i + 1;
      
      updateNodes_slice_2Dall(&ds, &data_spec, save, lowb, highb, g_shrink, width, use_domain);
      
      if ((stats_file || samples_file) && (i >= burnin)) {
	for (j = 0; j < total_network_variables; j++) {
	  samples_in_memory[j][i-burnin] = ds.q[j];
	}
      }  
    }   
  }
  else if (!strcmp(sampling_method, "met1")) {  // single variate metropolis
    for (i = 0; i < total_iterations; i++) {
      iterationNo = i + 1;
      
      updateNodes_met_1(&ds, &data_spec, sf, 1.0, b_accept, use_domain);
      
      if ((stats_file || samples_file) && (i >= burnin)) {
	for (j = 0; j < total_network_variables; j++) {
	  samples_in_memory[j][i-burnin] = ds.q[j];
	}
      }  
    }
  } 
  else if (!strcmp(sampling_method, "met1all")) {  // single variate metropolis
    for (i = 0; i < total_iterations; i++) {
      iterationNo = i + 1;
      
      updateNodes_met_1all(&ds, &data_spec, sf, 1.0, b_accept, use_domain);
      
      if ((stats_file || samples_file) && (i >= burnin)) {
	for (j = 0; j < total_network_variables; j++) {
	  samples_in_memory[j][i-burnin] = ds.q[j];
	}
      }  
    }
  } 
  else if (!strcmp(sampling_method, "ref2d")) { // 2d reflective slice sampling
    if (in_out_side == -1) {
      fprintf(stderr, "Must specify -i (inside) or -o (outside)\n");
      exit(1);
    }

    for (i = 0; i < total_iterations; i++) {
      iterationNo = i + 1;
      
      updateNodes_ref_2D(&ds, &data_spec, sf, steps, in_steps, in_out_side);
      
      if ((stats_file || samples_file) && (i >= burnin)) {
	for (j = 0; j < total_network_variables; j++) {
	  samples_in_memory[j][i-burnin] = ds.q[j];
	}
      }  
    }
  }
  */

  //TODO debugging
  fprintf(stdout, "Total variables, aps, transmitters, and predictions: %d, %d, %d, %d\n",
      total_network_variables, MAX_APS, observations_nopredict, N_cases);
  std::vector<Result> results;


  //Correct for the outside drift allowed at the beginning
  DOMAIN_X -= 2*outside_drift;
  DOMAIN_Y -= 2*outside_drift;

  {
    int unknown = 1;

    for (int i = total_network_variables - 2*(N_cases - observations_nopredict); i < total_network_variables-1; i += 2) {
      size_t total = 0;
      //Running average and sum of squares for the x and y coordinates
      double average_x = 0.0;
      double average_y = 0.0;
      double ss_x = 0.0;
      double ss_y = 0.0;
      for (int j = 0; j < additional_iter; j++) {
        ++total;
        double x = samples_in_memory[i][j];
        double y = samples_in_memory[i+1][j];
        double prev_x_avg = average_x;
        double prev_y_avg = average_y;

        //Correct each x and y value for the drift allowed previously
        x -= outside_drift;
        if (x < 0) {
          x = 0.0;
        }
        else if (x > DOMAIN_X) {
          x = DOMAIN_X;
        }
        y -= outside_drift;
        if (y < 0) {
          y = 0.0;
        }
        else if (x > DOMAIN_Y) {
          y = DOMAIN_Y;
        }

        average_x += (x - average_x)/total;
        average_y += (y - average_y)/total;
        if (total > 1) {
          ss_x += (x - prev_x_avg)*(x - average_x);
          ss_y += (y - prev_y_avg)*(y - average_y);
        }
      }
      double stddev_x = 0.0;
      double stddev_y = 0.0;
      if ( total > 1 ) {
        stddev_x = sqrt(ss_x/(total-1));
        stddev_y = sqrt(ss_y/(total-1));
      }
      results.push_back(Result{average_x, average_y, stddev_x, stddev_y});
      //fprintf(stdout, "Unknown-%d (x, y): (%f, %f), (stddev_x=%f,stddev_y=%f)\n", unknown, average_x, average_y, stddev_x, stddev_y);

      /*
       * TODO This code is only necessary if the entries at the 2.5 percentile, median,
       * and 97.5 percentile are required.
      //Indices for the median, 2.5 percentile and 97.5 percentile values.
      std::sort(samples_in_memory[i].begin(), samples_in_memory[i].end());
      std::sort(samples_in_memory[i+1].begin(), samples_in_memory[i+1].end());
      int index_median = (50.0/100.0) * additional_iter;
      int index_2_5 = (2.5/100.0) * additional_iter;
      int index_97_5 = (97.5/100.0) * additional_iter;
      fprintf(stdout, "Unknown-%d x and y median, 2.5 percentile, and 97.5 percentile: (%f, %f), (%f, %f), (%f, %f)\n", unknown, samples_in_memory[i][index_median], samples_in_memory[i+1][index_median], samples_in_memory[i][index_2_5], samples_in_memory[i+1][index_2_5], samples_in_memory[i][index_97_5], samples_in_memory[i+1][index_97_5]);
      */

      ++unknown;
    }
  }
 
  return results;
}

/*
 * Split the given box into smaller boxes with higher densities of
 * points. Continue splitting as long as subregions increase point
 * density by at least <min_increase>.
 * If no subregions have probabilities higher than that of the given
 * threshold <min_samples> then only the given region is returned.
 * TODO Is density per area better than min_samples here? Try it out
 */
std::vector<Rect> zoomBoxes(const std::vector<double>& xs, const std::vector<double>& ys,
    Rect region, double min_increase) {
  //Don't subdivide rectangles less than one unit in size.
  if (region.upper_right_x - region.lower_left_x <= 5.0 or
      region.upper_right_y - region.lower_left_y <= 5.0) {
    return std::vector<Rect>{region};
  }
  //Divide the region into four boxes - lower left, upper left, lower right, upper right
  double mid_x = (region.upper_right_x + region.lower_left_x) / 2.0;
  double mid_y = (region.upper_right_y + region.lower_left_y) / 2.0;
  double x_borders[2][2] = {{region.lower_left_x, mid_x}, {mid_x, region.upper_right_x}};
  double y_borders[2][2] = {{region.lower_left_y, mid_y}, {mid_y, region.upper_right_y}};
  //Count the number of points inside of each region.
  int region_counts[2][2] = {{0, 0}, {0, 0}};
  int src_count = 0;
  for (size_t point = 0; point < xs.size(); ++point) {
    double x = xs[point];
    double y = ys[point];
    //Count this point only if it is in bounds of the given region
    if (x <= region.upper_right_x and
        x >= region.lower_left_x and
        y <= region.upper_right_y and
        y >= region.lower_left_y) {
      int x_coord = x < mid_x ? 0 : 1;
      int y_coord = y < mid_y ? 0 : 1;
      region_counts[x_coord][y_coord]++;
      ++src_count;
    }
  }

  std::vector<Rect> results;
  //The threshold for accepting a new subregion - the new
  //regions are 1/4 the size of the source region and must
  //increase in density by <min_increase>.
  int threshold = (src_count * (1.0 + min_increase)) / 4.0;
  for (int x = 0; x < 2; ++x) {
    for (int y = 0; y < 2; ++y) {
      if (region_counts[x][y] >= threshold) {
        Rect new_region{x_borders[x][0], y_borders[y][0], x_borders[x][1], y_borders[y][1]};
        //Recursively call zoomBoxes to find the accepted subregions of this subregion.
        std::vector<Rect> sub_boxes = zoomBoxes(xs, ys, new_region, min_increase);
        results.insert(results.end(), sub_boxes.begin(), sub_boxes.end());
      }
    }
  }
  //If none of the subregions have at least min_samples in them just return this region.
  if ( 0 == results.size()) {
    results.push_back(region);
  }

  return results;
}

//M2 solver that gives results in bounding boxes rather than points
vector<vector<Tile>> m2solveBoxes( int argc, char** argv,
    const TrainingData& train_data,
    const LandmarkData& ap_data,
    double DOMAIN_X,
    double DOMAIN_Y,
    int burnin,
    int additional_iter,
    SampleMethod sampling_method,
    double tile_width,
    double tile_height,
    double percentile)
{ 
  int c;
  int g_shrink = 0;
  int max_steps = 0;	/* Maximum number of intervals, zero for unlimited; if negative, intervals are found by doubling */
  double width = 1.0;   /* Estimate of the size of the slice */
  double sf = 1.0;   /* Estimate of the size of the slice */
  int use_domain = 0, b_accept = 0;
  int steps = 35, in_steps = steps;   /* Total number of steps, steps inside slice */
  int in_out_side = -1;  /* Will be either inside or outside */

  for (int i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      c = argv[i][1];
      
      switch (c) {
        case 'w':
          width = atof(argv[++i]); 
          break;	
        case 'm':
          max_steps = atoi(argv[++i]); 
          break;	
        case 'd':
          use_domain = 1;
          break;
        case 'g':
          g_shrink = 1;
          break;	
        case 'f':
          sf = atof(argv[++i]); 
          break;		
        case 'b':
          b_accept = 1;
          break;
        case 'p':
          steps = atoi(argv[++i]); 
          in_steps = steps;
          break;	
        case 'i':
          in_out_side = INSIDE; 
          break;		
        case 'o':
          in_out_side = OUTSIDE; 
          break;				
      }
    }
  }

  if ( sampling_method < slice1 or sampling_method > met1all ) {
    fprintf(stderr, "Method is not any of [slice1|slice2d|met1|ref2d|slice1all|slice2dall|met1all]\n");
    exit(1);
  }

  int N_inputs = 2;
  int total_iterations = additional_iter + burnin;
  int total_network_variables = 0;

  for (auto T = train_data.begin(); T != train_data.end(); ++T) {
    if (ap_data.size() != T->rssis.size()) {
      std::cerr<<"RSSI values in training data do not match AP data.\n";
      std::cerr<<"Aborting.\n";
      return vector<vector<Tile>>(0);
    }
  }
  double_matrix train_signal_notclean(ap_data.size(), std::vector<double>(train_data.size()));
  std::vector<double> APS_X_NOTCLEAN(ap_data.size());
  std::vector<double> APS_Y_NOTCLEAN(ap_data.size());
  std::vector<double> train_x(train_data.size());
  std::vector<double> train_y(train_data.size());

  //The number of APs before pruning APs with no data.
  int MAX_APS_NOTCLEAN = ap_data.size();
  std::transform(ap_data.begin(), ap_data.end(), APS_X_NOTCLEAN.begin(),
      [](const std::pair<double, double>& p) { return p.first;});
  std::transform(ap_data.begin(), ap_data.end(), APS_Y_NOTCLEAN.begin(),
      [](const std::pair<double, double>& p) { return p.second;});

  std::transform(train_data.begin(), train_data.end(), train_x.begin(),
      [](const Sample& s) { return s.x;});
  std::transform(train_data.begin(), train_data.end(), train_y.begin(),
      [](const Sample& s) { return s.y;});

  //N cases is the number of transmitters in this dataset
  int N_cases = train_data.size();

  for (int i = 0; i < MAX_APS_NOTCLEAN; ++i) {
    for (int j = 0 ; j < N_cases; ++j) {
      train_signal_notclean[i][j] = train_data[j].rssis[i];
    }
  }

  //The number of samples with unknown locations (as indicated by NAN values
  //in their coordinates.
  int predictNo = std::accumulate(train_data.begin(), train_data.end(), 0,
      [](int sum, const Sample& arg) { return sum + (isnan(arg.x) ? 1 : 0); });
  //Replace all NaN with 0.0.
  std::for_each(train_x.begin(), train_x.end(), [](double& arg) {
      if (isnan(arg)) { arg = 0.0;}});
  std::for_each(train_y.begin(), train_y.end(), [](double& arg) {
      if (isnan(arg)) { arg = 0.0;}});
  
  int MAX_APS;
  std::vector<double> aps_x;
  std::vector<double> aps_y;
  double_matrix train_signal;
  {
    std::vector<double> ap_data(MAX_APS_NOTCLEAN);
    //Check the data from each AP in all of the samples. If any AP gave only
    //values of -1 then remove its rssis from the data.
    std::vector<int> empty_columns;
    for (int column = 0; column < MAX_APS_NOTCLEAN; ++column) {
      bool empty_ap = std::accumulate(train_signal_notclean.begin(), train_signal_notclean.end(), true,
          [=](bool all_bad, const std::vector<double>& arg) {
          return all_bad and (arg[column] == -1.0);});
      if (empty_ap) {
        empty_columns.push_back(column);
      }
    }


    //Remove the columns of data with indices in the empty_columns vector
    //from the training data and the ap data

    //Get the ap locations for aps with empty data sets (all -1 RSSI)
    MAX_APS = MAX_APS_NOTCLEAN - empty_columns.size();

    auto next_empty = empty_columns.begin();
    int skipped = 0;
    for (int ap = 0; ap < MAX_APS_NOTCLEAN; ++ap) {
      //Skip this entry if this AP contributes no data
      if (next_empty != empty_columns.end() and *next_empty == ap) {
        ++next_empty;
        ++skipped;
      } else {
        //Since ths row is nonempty record data from it - the x and y location of
        //the access point and the RSS values that it saw for each transmitter.
        aps_x.push_back(APS_X_NOTCLEAN[ap]);
        aps_y.push_back(APS_Y_NOTCLEAN[ap]);
        train_signal.push_back(std::vector<double>(N_cases));
        //For each transmitter store data from this ap
        for (int txer = 0; txer < N_cases; ++txer) {
          //Change the signal data to absolute values for the solver.
          train_signal.back()[txer] = fabs(train_signal_notclean[ap][txer]);
        }
      }
    } 
  }

  // The number of training points
  int observations_nopredict = N_cases - predictNo;

  data_specifications data_spec(N_inputs, MAX_APS, N_cases, train_x, train_y, train_signal,
        predictNo, observations_nopredict, aps_x, aps_y);

  //TODO FIXME there should be multiple arrays, not one array with a bunch of stuff in it.
  total_network_variables = 4+MAX_APS*3+2*predictNo;

  double_matrix samples_in_memory =
    double_matrix(total_network_variables, std::vector<double>(additional_iter));
 
  //Initialize the dynamic state struct
  //TODO FIXME declare an instance of the struct here and make this the constructor
  mc_dynamic_state ds(MAX_APS, predictNo, total_network_variables, DOMAIN_X, DOMAIN_Y);

  rejections = 0;
  evaluate_energy = 0;
  
  //TODO FIXME add the other sampling methods back into this piece of code
  if ( sampling_method == slice1all) {       // single variate slice sampling
    for (int i = 0; i < total_iterations; i++) {
      updateNodes_slice_1all(ds, data_spec, width, max_steps, use_domain, rejections);
      //The first <burnin> values can be ignored.
      if (i >= burnin) {
        for (int j = 0; j < total_network_variables; j++) {
          samples_in_memory[j][i-burnin] = ds.q[j];
        }
      }  
    }
  } else if ( sampling_method == slice1) {
    for (int i = 0; i < total_iterations; i++) {
      updateNodes_slice_1(ds, data_spec, width, max_steps, use_domain, rejections);
      //The first <burnin> values can be ignored.
      if (i >= burnin) {
        for (int j = 0; j < total_network_variables; j++) {
          samples_in_memory[j][i-burnin] = ds.q[j];
        }
      }  
    }
  } else if ( sampling_method == slice2d) {  // 2d variate slice sampling
    std::vector<double> save(total_network_variables);  // For X, Ys to localize
    std::vector<double> lowb(total_network_variables);  // For Low bandwidth of X, Ys
    std::vector<double> highb(total_network_variables); // For High bandwidth of X, Ys

    for (int i = 0; i < total_iterations; i++) {
      updateNodes_slice_2D(ds, data_spec, save, lowb, highb, g_shrink, width, use_domain, rejections);
      //The first <burnin> values can be ignored.
      if (i >= burnin) {
        for (int j = 0; j < total_network_variables; j++) {
          samples_in_memory[j][i-burnin] = ds.q[j];
        }
      }  
    }   
  }
  /*
   *TODO FIXME add support for this
  else if (!strcmp(sampling_method, "slice2dall")) {  // 2d variate slice sampling
    double *save = chk_alloc(total_network_variables, sizeof (double)); // For X, Ys to localize
    double *lowb = chk_alloc(total_network_variables, sizeof (double));   // For Low bandwidth of X, Ys
    double *highb = chk_alloc(total_network_variables, sizeof (double));   // For High bandwidth of X, Ys
    
    for (i = 0; i < total_iterations; i++) {
      iterationNo = i + 1;
      
      updateNodes_slice_2Dall(&ds, &data_spec, save, lowb, highb, g_shrink, width, use_domain);
      
      if ((stats_file || samples_file) && (i >= burnin)) {
	for (j = 0; j < total_network_variables; j++) {
	  samples_in_memory[j][i-burnin] = ds.q[j];
	}
      }  
    }   
  }
  else if (!strcmp(sampling_method, "met1")) {  // single variate metropolis
    for (i = 0; i < total_iterations; i++) {
      iterationNo = i + 1;
      
      updateNodes_met_1(&ds, &data_spec, sf, 1.0, b_accept, use_domain);
      
      if ((stats_file || samples_file) && (i >= burnin)) {
	for (j = 0; j < total_network_variables; j++) {
	  samples_in_memory[j][i-burnin] = ds.q[j];
	}
      }  
    }
  } 
  else if (!strcmp(sampling_method, "met1all")) {  // single variate metropolis
    for (i = 0; i < total_iterations; i++) {
      iterationNo = i + 1;
      
      updateNodes_met_1all(&ds, &data_spec, sf, 1.0, b_accept, use_domain);
      
      if ((stats_file || samples_file) && (i >= burnin)) {
	for (j = 0; j < total_network_variables; j++) {
	  samples_in_memory[j][i-burnin] = ds.q[j];
	}
      }  
    }
  } 
  else if (!strcmp(sampling_method, "ref2d")) { // 2d reflective slice sampling
    if (in_out_side == -1) {
      fprintf(stderr, "Must specify -i (inside) or -o (outside)\n");
      exit(1);
    }

    for (i = 0; i < total_iterations; i++) {
      iterationNo = i + 1;
      
      updateNodes_ref_2D(&ds, &data_spec, sf, steps, in_steps, in_out_side);
      
      if ((stats_file || samples_file) && (i >= burnin)) {
	for (j = 0; j < total_network_variables; j++) {
	  samples_in_memory[j][i-burnin] = ds.q[j];
	}
      }  
    }
  }
  */

  //TODO debugging
  //fprintf(stdout, "Total variables, aps, transmitters, and predictions: %d, %d, %d, %d\n",
      //total_network_variables, MAX_APS, observations_nopredict, N_cases);

  //Generate a tile list for each unknown, including the tiles in the specified percentile.
  vector<vector<Tile>> results;
  int columns = ceil(DOMAIN_X/tile_width);
  int rows    = ceil(DOMAIN_Y/tile_height);
  for (int i = total_network_variables - 2*(N_cases - observations_nopredict); i < total_network_variables-1; i += 2) {
    //Count the number of hits at each tile
    vector<vector<int>> hits(columns, vector<int>(rows, 0));
    std::vector<double>& xs = samples_in_memory[i];
    std::vector<double>& ys = samples_in_memory[i+1];
    for (int point = 0; point < xs.size(); ++point) {
      double x = xs[point];
      double y = ys[point];
      int col = (int)(x / tile_width);
      int row = (int)(y / tile_height);
      ++hits[col][row];
    }
    //Make all of the tiles.
    vector<Tile> result;
    for (int col = 0; col < columns; ++col) {
      for (int row = 0; row < rows; ++row) {
        result.push_back(Tile{col, row});
      }
    }
    //Sort the tiles by their use counts.
    sort(result.begin(), result.end(), [&](const Tile& a, const Tile& b) {
        return hits[a.column][a.row] > hits[b.column][b.row];});
    auto I = result.begin();
    double cum_prob = 0.0;
    do {
      cum_prob += (float)hits[I->column][I->row]/xs.size();
      ++I;
    } while (cum_prob < percentile);
    //Erase the result not in the specified percentile.
    result.erase(I, result.end());
    results.push_back(result);
  }

  return results;
}

