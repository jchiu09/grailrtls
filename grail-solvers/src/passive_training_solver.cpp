#include "aggregator_solver_protocol.hpp"
#include "grailV3_solver_client.hpp"
#include "sample_data.hpp"
#include "netbuffer.hpp"

#include "lda.h"

#define BURST_LATENCY 1

using namespace aggregator_solver;
using namespace solver_distributor;
using namespace std;

struct Debug {
	bool on;
};

template<typename T>
Debug& operator<<(Debug& dbg, T arg) {
	if (dbg.on) {
		std::cout<<arg;
 	}
	return dbg;
}

Distributor grail_dist;

vector<uint128_t> tx_ids;
vector<uint128_t> rx_ids;
vector<uint128_t> sp_ids;
vector<vector<double> > neighbour;

int cell_num;
int link_num;
int sample_num;

int cell = 0; 

int help_packet(uint128_t tx_id){
	// 1: separate the data stream into training data and pass by, 2,3,4 TBD 
	int help_type = 0;
	for(int i = 0; i < sp_ids.size(); i++){
		if(tx_id == sp_ids[i]){
			help_type = i+1;
		}
	}
	return help_type;
}

// check the tx_id and rx_id of the sample data is valid 
bool data_packet(uint128_t tx_id, uint128_t rx_id) {
	bool tx_find = false;
	bool rx_find = false;

	for(int i = 0; i < tx_ids.size(); i++){
		if(tx_id == tx_ids[i]){
			tx_find = true;
		}
	}
	for(int i = 0; i < rx_ids.size(); i++){
		if(rx_id == rx_ids[i]){
			rx_find = true;
		}
	}
	return (tx_find * rx_find);
}

//Collect passive RSSI data
//This function is not thread safe (but could be made so with a mutex)
void packetCallback(SampleData& sample) {
	static map<int, vector<vector<double> > > rssi_record;
	static map<int, vector<vector<double> > > rssi_buffer;
	static map<uint128_t, vector<double> > rssi_report;
	static vector<double> rssi_mean;

	static vector<double> prior(cell_num, 1); 	// the prior probability for each cell
	//static int cell = -1; 			// indicate the current cell number
	static int count_help = 0;			// count number of the help packets
	static bool curr_state = false;			// identify if in calibration
	static bool skip = false;
	static time_t skip_time = 0;
	//static time_t last_time = 0;
	static time_t time_seconds = 0;

	static Debug debug{false};

	static lda mylda(cell_num, link_num, sample_num);

	if (skip == true){
		time_seconds = time(NULL);
		if(time_seconds - skip_time > BURST_LATENCY){
			skip = false;
		}
	} 
	else{
		// a burst packet is inserted into the stream
		if (help_packet(sample.tx_id) == 1){
			count_help++;

			// if four burst packets are inserted, began to collect 
			if (count_help == 4 && skip == false){
				//cerr<<"Enough help packets for cell "<<help_packet(sample.tx_id)<<endl;
				skip = true;
				time_seconds = time(NULL);
				skip_time = time_seconds; // record the time when the help packets are enough
				//struct tm *time_parts;
				//time_parts = localtime (&time_seconds);
				//stringstream out;
				//out<<asctime(time_parts);
				//cerr<<"Currently at cell: "<<cell + 1<<" at time "<<out.str()<<endl;

				// begin to collect the cell information
				if(curr_state == false){
					//mylda.update_one(cell, backVector(map2vector(rssi_report), WIN_SIZE));
					curr_state = true;
				}

				// finish updating the cell information
				else if(curr_state == true){
					mylda.update_one(cell, backVector(map2vector(rssi_report), sample_num));
					cell++;
					curr_state = false;
				}

				mylda.logfile();
				rssi_report.clear();
				count_help = 0;
			}
		}

		// Read the packets from all the links as a RSSI vector; drop the whole vector if any element misses
		else if (data_packet(sample.tx_id, sample.rx_id)) {
			int tx_sub = getIndex(tx_ids, sample.tx_id.lower);
			int rx_sub = getIndex(rx_ids, sample.rx_id.lower);
			uint128_t link_id = tx_sub * rx_ids.size() + rx_sub;
			rssi_report[link_id].push_back(sample.rss);
			debug<<"Receive packet from "<<sample.tx_id.lower<<" to "<<sample.rx_id.lower<<"\n";
		}
	}
}

int main(int argc, char** argv) {

	if (argc < 5) {
    		cerr<<"This program needs 4 or more arguments:\n";
    		cerr<<"\tclient [<aggregator ip> <aggregator port>]+ <distributor ip> <distributor port>\n";
    		cerr<<"Any number of aggregator ip/port pairs may be provided to connect to multiple aggregators.\n";
    		return 0;
  	}

	//Grab the ip and ports for the aggregators and distributor.
	vector<NetTarget> aggregators;
	for (int s_num = 1; s_num < argc - 2; s_num += 2) {
		string aggr_ip(argv[s_num]);
		uint16_t aggr_port = atoi(argv[s_num + 1]);
		aggregators.push_back(NetTarget{aggr_ip, aggr_port});
	}
	string aggr_ip(argv[argc-2]);
	int aggr_port = atoi(argv[argc-1]);

	Rule winlab_rule;
	winlab_rule.physical_layer  = 1;
	winlab_rule.update_interval = 800;

	Subscription winlab_sub{winlab_rule};
	vector<Subscription> subs{winlab_sub};

	//Set up the types for the GRAIL distributor
	//There is only one solution type - temperature.
	SolutionType s_t{1, std::u16string(u"Passive_location")};
	vector<SolutionType> types{s_t};

	int buffer;
	int file_line = 0;
	string data;
	ifstream infile;

	infile.open("dfp.txt");
	if (!infile){
		cout<<"File open error!\n";
		return 0;
	}

	while(getline(infile, data)) {
		istringstream s(data);
		if(file_line == 0) {
			while(s>>buffer){
				tx_ids.push_back(buffer);
			}
		}
		else if (file_line == 1) {
			while(s>>buffer){
				rx_ids.push_back(buffer);
			}
		}
		else if (file_line == 2) {
			while(s>>buffer){
				sp_ids.push_back(buffer);
			}
		}
		else if (file_line == 3) {
			s>>cell_num>>sample_num;
			for(int i = 0; i < cell_num; i++) {
				neighbour.push_back(vector<double>(cell_num, 0));
			}
		}
		else if (file_line > 3)	{
			int cell;
			s>>cell;
			neighbour[cell - 1][cell - 1] = 1;
			while(s>>buffer){
				neighbour[cell - 1][buffer - 1] = 1;	
			}
		}
		file_line++;
	}

	link_num = tx_ids.size() * rx_ids.size();
	
	//Connect to the distributor first, then the aggregators.
	if (grail_dist.connect(aggr_ip, aggr_port, types)) {
		//Connect to the grail aggregators with our subscription lists.
	    	grailAggregatorConnect(aggregators, subs, packetCallback);
	} else {
		cerr<<"Error connecting to the GRAIL distributor.\n";
	}
}
