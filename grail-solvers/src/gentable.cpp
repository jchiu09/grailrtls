//To compile:
// g++ --std=c++0x gentable.cpp -L./lib -lsqlite3 -ldl -lgrail3 
#include <string>
#include <sqlite3.h>
#include <algorithm>
#include <map>
#include <utility>
#include <vector>
#include <iostream>
#include <sstream>
#include "netbuffer.hpp"

using namespace std;

typedef std::u16string URI;
typedef std::vector<uint8_t> Buffer;

//Each object URI in the world server can have multiple fields
struct FieldData {
  //The description of one of the fields of a URI.
  std::u16string description;
  //The creation date and the date when this field's data expires.
  uint64_t creation_date;
  uint64_t expiration_date;
  std::u16string data_type;
  //Data in the field.
  Buffer data;
};

struct WorldData {
  //The URI of an object in the world server.
  URI object_uri;
  //Fields of this URI
  std::vector<FieldData> fields;
};
/*
 * This is what the SQL looks like
CREATE TABLE 'fields' ('uri' TEXT, 'description' TEXT, 'creation date' INTEGER, 'expiration date' INTEGER, 'data type' TEXT, 'data' BLOB);
INSERT INTO "fields" VALUES('winlab.coffee pot','motion',0,0,'uint64_t',X'000000000000000E');
CREATE TABLE 'URIs' ('uri' TEXT PRIMARY KEY);
INSERT INTO "URIs" VALUES('winlab.coffee pot');
*/

//Database handle
sqlite3 *db_handle = NULL;

void insertField(const std::string& uri, const FieldData& fd) {
  //TODO For now inserting the UTF-16 strings as UTF-8
  std::string description(fd.description.begin(), fd.description.end());
  std::string data_type(fd.data_type.begin(), fd.data_type.end());
  std::ostringstream insert_stream;

  insert_stream << "INSERT INTO 'fields' VALUES (\""<<uri<<"\", \""<<description<<
    "\", ?1, ?2, \""<<data_type<<"\", ?3);";
  sqlite3_stmt* statement_p;
  //Prepare the statement
  sqlite3_prepare_v2(db_handle, insert_stream.str().c_str(), -1, &statement_p, NULL);
  //Bind the creation and expiration dates and the raw binary data.
  sqlite3_bind_int64(statement_p, 1, fd.creation_date);
  sqlite3_bind_int64(statement_p, 2, fd.expiration_date);
  //The blob's memory is static during this transaction.
  //Otherwise it would be proper to use SQLITE_TRANSIENT to force sqlite to make a copy.
  sqlite3_bind_blob(statement_p, 3, fd.data.data(), fd.data.size(), SQLITE_STATIC);
  
  //Call sqlite with the statement
  if (SQLITE_DONE != sqlite3_step(statement_p)) {
    //TODO This should be better at handling an error.
    std::cerr<<"Error inserting field into database.\n";
  }

  //Delete the statement
  sqlite3_finalize(statement_p);
}

//Insert world data into the database.
void insertWorldData(const WorldData& wd) {
  //First insert the uri into the URI table.
  std::ostringstream insert_stream;
  //TODO For now inserting the UTF-16 strings as UTF-8
  std::string utf8_uri(wd.object_uri.begin(), wd.object_uri.end());
  //Insert or ignore an error if this if this URI already exists.
  insert_stream << "INSERT OR IGNORE INTO 'URIs' VALUES (\""<<utf8_uri<<"\");";
  char* err;
  sqlite3_exec(db_handle, insert_stream.str().c_str(), NULL, NULL, &err);
  if (NULL != err) {
    std::cerr<<"Inserting data into world server database: "<<err<<'\n';
    std::cerr<<"Command was "<<insert_stream.str().c_str()<<'\n';
    sqlite3_free(err);
    //TODO Is the database okay for future transactions?
    return;
  }
  //Insert each of the fields into the fields database.
  std::for_each(wd.fields.begin(), wd.fields.end(),
      [&](const FieldData& fd) { insertField(utf8_uri, fd);});
}

int main() {
  int sql_succ = sqlite3_open_v2("world_server.db", &db_handle,
      SQLITE_OPEN_FULLMUTEX | SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);
  if (SQLITE_OK != sql_succ) {
    std::cerr<<"Error opening sqlite3 database: "<<sqlite3_errmsg(db_handle)<<'\n';
    sqlite3_close(db_handle);
    return 0;
  }
  //Locations of transmitters and receivers
  std::map<uint64_t, std::pair<double, double>> rx_locations;
  std::map<uint64_t, std::pair<double, double>> tx_locations;

  rx_locations[640] = make_pair(68.4, 49.2);
  rx_locations[641] = make_pair(77.3, 52);
  //Secretary area
  rx_locations[651] = make_pair(60.5, 23.5);
  rx_locations[652] = make_pair(70.3, 23.5);
  /*
  //Control room
  rx_locations[644] = make_pair(123, 42.3);
  rx_locations[646] = make_pair(127, 42.3);
  These two were relocated
  */
  //Illya area
  rx_locations[647] = make_pair(130, 103);
  rx_locations[650] = make_pair(130, 100);
  //Cube farm
  rx_locations[642] = make_pair(122.5, 24);
  rx_locations[643] = make_pair(122.5, 30);
  rx_locations[644] = make_pair(122.5, 37);
  //Cube farm 2
  rx_locations[196] = make_pair(106.2, 23);
  rx_locations[197] = make_pair(106.7, 28);
  //Orbit room - near pip table
  rx_locations[646] = make_pair(137, 70);

  //Set the stationary transmitters
  tx_locations[0x23] = make_pair(142, 24);
  tx_locations[0x27] = make_pair(123, 3);
  tx_locations[0x1D] = make_pair(123, 37);
  tx_locations[0x28] = make_pair(123, 20);
  tx_locations[0x31] = make_pair(95, 55);
  tx_locations[0x2C] = make_pair(97, 43);
  tx_locations[0x29] = make_pair(100, 34);
  tx_locations[0x21] = make_pair(99, 19);
  tx_locations[0x42] = make_pair(74, 49);
  tx_locations[0x26] = make_pair(72, 34);
  tx_locations[0x36] = make_pair(77, 24);
  tx_locations[0x4E] = make_pair(76, 5);
  tx_locations[0x49] = make_pair(55, 6);
  tx_locations[0x40] = make_pair(43, 28);
  tx_locations[0x24] = make_pair(47, 43);
  tx_locations[0x32] = make_pair(115, 43);
  tx_locations[0x2A] = make_pair(77, 87);
  tx_locations[0x2B] = make_pair(104, 79);
  //tx_locations[0x30] = make_pair(143, 74);
  tx_locations[0x25] = make_pair(143, 64);
  tx_locations[0x52] = make_pair(65, 24);
  tx_locations[0x55] = make_pair(124, 75);
  tx_locations[0x56] = make_pair(104, 56);
  tx_locations[0x57] = make_pair(131, 75);

  /* Do insertions like this:
     INSERT INTO "fields" VALUES('pipsqueak.transmitter.id','location.x',0,0,'double', '000000000000000E');
     INSERT INTO "URIs" VALUES('pipsqueak.transmitter.id');
     IDs are strings in the URIs.
     */

  for_each(rx_locations.begin(), rx_locations.end(), [&](pair<uint64_t, std::pair<double, double>> rxer) {
      WorldData wd;
      std::ostringstream uri_in;
      uri_in << "pipsqueak.receiver."<<rxer.first;
      string plain_uri = uri_in.str();
      u16string uri(plain_uri.begin(), plain_uri.end());
      wd.object_uri = uri;
      FieldData fdx{u"location.x", 0, 0, u"double"};
      pushBackVal(rxer.second.first, fdx.data);
      FieldData fdy{u"location.y", 0, 0, u"double"};
      pushBackVal(rxer.second.second, fdy.data);
      FieldData fdregion{u"region_uri", 0, 0, u"string"};
      pushBackUTF16(fdregion.data, u"winlab");
      FieldData fdid{u"id", 0, 0, u"uint64_t"};
      pushBackVal(rxer.first, fdid.data);
      wd.fields.push_back(fdx);
      wd.fields.push_back(fdy);
      wd.fields.push_back(fdregion);
      wd.fields.push_back(fdid);
      insertWorldData(wd); });

  for_each(tx_locations.begin(), tx_locations.end(), [&](pair<uint64_t, std::pair<double, double>> txer) {
      WorldData wd;
      std::ostringstream uri_in;
      uri_in << "pipsqueak.transmitter."<<txer.first;
      string plain_uri = uri_in.str();
      u16string uri(plain_uri.begin(), plain_uri.end());
      wd.object_uri = uri;
      FieldData fdx{u"location.x", 0, 0, u"double"};
      pushBackVal(txer.second.first, fdx.data);
      FieldData fdy{u"location.y", 0, 0, u"double"};
      pushBackVal(txer.second.second, fdy.data);
      FieldData fdregion{u"region_uri", 0, 0, u"string"};
      pushBackUTF16(fdregion.data, u"winlab");
      FieldData fdid{u"id", 0, 0, u"uint64_t"};
      pushBackVal(txer.first, fdid.data);
      wd.fields.push_back(fdx);
      wd.fields.push_back(fdy);
      wd.fields.push_back(fdregion);
      wd.fields.push_back(fdid);
      insertWorldData(wd);  });


  return 0;
}
