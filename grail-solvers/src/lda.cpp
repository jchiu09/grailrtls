#include "lda.h"

#include <cv.h>
#include <cvaux.h>
#include <highgui.h>

#include <passive_localization_utilities.h>

#include <iostream>
#include <fstream>
#include <map>
#include <sstream>
#include <string>
#include <vector>

using std::cout;
using std::endl;
using std::ifstream;
using std::istringstream;
using std::ofstream;


lda::lda(int cell_num, int link_num, int sample_num){

	this->cell_num = cell_num;
	this->link_num = link_num;
	this->sample_num = sample_num;

	for(int i = 0; i < cell_num; i++){
		for(int j = 0; j < link_num; j++){
			rssi_data[i].push_back(vector<double>());
			for(int k = 0; k < sample_num; k++){
				rssi_data[i][j].push_back(0);
			}
		}
	}
	
	for(int i = 0; i < cell_num; i++){
		for(int j = 0; j < link_num; j++){
			lda_mean[i].push_back(0);
		}
	}

	for(int i = 0; i < cell_num; i++){
		for(int j = 0; j < link_num; j++){
			lda_variance[i].push_back(0);
		}
	}

	for(int i = 0; i < link_num; i++){
		lda_covariance.push_back(vector<double>());
		for(int j = 0; j < link_num; j++){
			lda_covariance[i].push_back(0);
		}
	}
}

lda::lda(string filename, int cell_num, int link_num, int sample_num){

	this->cell_num = cell_num;
	this->link_num = link_num;
	this->sample_num = sample_num;

	for(int i = 0; i < cell_num; i++){
		for(int j = 0; j < link_num; j++){
			rssi_data[i].push_back(vector<double>());
			for(int k = 0; k < sample_num; k++){
				rssi_data[i][j].push_back(0);
			}
		}
	}
	
	for(int i = 0; i < cell_num; i++){
		for(int j = 0; j < link_num; j++){
			lda_mean[i].push_back(0);
		}
	}

	for(int i = 0; i < cell_num; i++){
		for(int j = 0; j < link_num; j++){
			lda_variance[i].push_back(0);
		}
	}

	for(int i = 0; i < link_num; i++){
		lda_covariance.push_back(vector<double>());
		for(int j = 0; j < link_num; j++){
			lda_covariance[i].push_back(0);
		}
	}

	ifstream infile(filename.c_str());
	string filedata;

	int count = 0;
	while(!infile.eof()){
		//get the area coordinates info line by line
		while(getline(infile, filedata)){
			istringstream s(filedata);
			int cell;
			s>>cell;
			for(int i = 0; i < link_num; i++){
				s>>rssi_data[cell-1][i][count%sample_num];
			}
			count++;
		}
	}

	infile.close();

	vector<double> temp_sample;

	// for each cell, get mean vector
	for(int i = 0; i < cell_num; i++){
		for(int j = 0; j < link_num; j++){
			temp_sample.push_back(getMean(rssi_data[i][j]));
		}
		lda_mean[i] = temp_sample;
		temp_sample.clear();
	}

	// for each cell, get variance vector
	for(int i = 0; i < cell_num; i++){
		for(int j = 0; j < link_num; j++){
			temp_sample.push_back(getVariance(rssi_data[i][j], getMean(rssi_data[i][j])));
		}
		lda_variance[i] = temp_sample;
		temp_sample.clear();
	}

	for(int i = 0; i < cell_num; i++){
		for(int j = 0; j < sample_num; j++){
			temp_sample = getVector(rssi_data[i], j, 1);
			for(int k = 0; k < link_num; k++){
				temp_sample[k] = temp_sample[k] - lda_mean[i][k];
			}
			for(int k = 0; k < link_num; k++){
				for(int l = 0; l < link_num; l++)
					lda_covariance[k][l] = lda_covariance[k][l] + 
						temp_sample[k] * temp_sample[l] / (sample_num * cell_num - cell_num);
			}
			temp_sample.clear();
		}
	}
	return;
}

//calculate the mean and covariance for lda classifier
void lda::formulate(){

	vector<double> temp_sample;

	// for each cell, get mean vector
	for(int i = 0; i < cell_num; i++){
		for(int j = 0; j < link_num; j++){
			temp_sample.push_back(getMean(rssi_data[i][j]));
		}
		lda_mean[i] = temp_sample;
		temp_sample.clear();
	}

	// for each cell, get variance vector
	for(int i = 0; i < cell_num; i++){
		for(int j = 0; j < link_num; j++){
			temp_sample.push_back(getVariance(rssi_data[i][j], getMean(rssi_data[i][j])));
		}
		lda_variance[i] = temp_sample;
		temp_sample.clear();
	}

	for(int k = 0; k < link_num; k++){
		for(int l = 0; l < link_num; l++){
			lda_covariance[k][l] = 0;
		}
	}

	for(int i = 0; i < cell_num; i++){
		for(int j = 0; j < sample_num; j++){
			temp_sample = getVector(rssi_data[i], j, 1);
			for(int k = 0; k < link_num; k++){
				temp_sample[k] = temp_sample[k] - lda_mean[i][k];
			}
			for(int k = 0; k < link_num; k++){
				for(int l = 0; l < link_num; l++)
					lda_covariance[k][l] = lda_covariance[k][l] + 
						temp_sample[k] * temp_sample[l] / (sample_num * cell_num - cell_num);
			}
			temp_sample.clear();
		}
	}	
}

//update the training data for a single class
void lda::update_one(int cell, vector<vector<double> > data){
	this->rssi_data[cell-1] = data;
}

//update the training data for all classes
void lda::update_all(map<int, vector<vector<double> > > data){
	this->rssi_data = data;
}

vector<double> lda::get_delta(vector<double> data){
	vector<double> delta;

	float *temp_data = new float[link_num];
	float *temp_covariance = new float[link_num * link_num];

	copy(data.begin(), data.end(), temp_data);
	for(int i = 0; i < link_num; i++){
		copy(lda_covariance[i].begin(), lda_covariance[i].end(), temp_covariance + link_num * i);
	}

	CvMat sigma = cvMat(link_num, link_num, CV_32FC1, temp_covariance);
	CvMat inv_sigma = cvMat(link_num, link_num, CV_32FC1, temp_covariance);
	cvInvert(&sigma, &inv_sigma);

	float *inv_temp_covariance = inv_sigma.data.fl; // convert matrix to array
	vector<vector<double> > inv_lda_covariance;

	int c = 0;
	for(int i = 0; i < link_num; i++){
		inv_lda_covariance.push_back(vector<double>());
		for(int j = 0; j < link_num; j++){
			inv_lda_covariance[i].push_back((double)inv_temp_covariance[c++]);
		}
	}
	
	for(int i = 0; i < cell_num; i++){
		delta.push_back(innerProduct(leftMul(data, inv_lda_covariance), lda_mean[i]) - 
				0.5 * innerProduct(leftMul(lda_mean[i], inv_lda_covariance), lda_mean[i]));
	}
/*
	for(int i = 0; i < link_num; i++){
		cout<<data[i]<<"\t";
	}

	for(int i = 0; i < cell_num; i++){
		for(int j = 0; j < link_num; j++){
			cout<<lda_mean[i][j]<<"\t";		
		}
		cout<<endl;
	}

	for(int i = 0; i < link_num; i++){
		for(int j = 0; j < link_num; j++){
			cout<<lda_covariance[i][j]<<"\t";		
		}
		cout<<endl;
	}
*/
	return delta;
}

vector<double> lda::get_sigma(vector<double> data) {
	vector<double> sigma;
	for(int i = 0; i < cell_num; i++) {
		sigma.push_back(getDistance(lda_variance[i], data));
	}
	return sigma;
}

void lda::readfile(string filename){
	ifstream infile(filename.c_str());
	string filedata;

	int count = 0;
	while(!infile.eof()){
		//get the area coordinates info line by line
		while(getline(infile, filedata)){
			istringstream s(filedata);
			int cell;
			s>>cell;
			for(int i = 0; i < link_num; i++){
				s>>rssi_data[cell-1][i][count%sample_num];
			}
			count++;
		}
	}

	infile.close();

	lda::formulate();

	return;
}

void lda::logfile(){
  ofstream myfile;
  time_t rawtime = time(NULL);
  struct tm * timeinfo;
  time (&rawtime);
  //time_t seconds = time(NULL);
  timeinfo = localtime (&rawtime);
  string filename = string(asctime(timeinfo)) + ".passive.data.txt";
  myfile.open(filename.c_str());

  //write into file
  for(int i = 0; i < cell_num; i++){
    for(int j = 0; j < sample_num; j++){
      myfile<<i+1<<"\t";
      for(int k = 0; k < link_num; k++){
        myfile<<rssi_data[i][k][j]<<"\t";
      }
      myfile<<endl;
    }
  }
  return;
}

void lda::showdata(){
	for(int i = 0; i < cell_num; i++){
		for(int j = 0; j < sample_num; j++){
			cout<<i+1<<"\t";
			for(int k = 0; k < link_num; k++){
				cout<<rssi_data[i][k][j]<<"\t";
			}
			cout<<endl;
		}
	}
	return;
}

void lda::clear(){
	for(int i = 0; i < cell_num; i++){
		for(int j = 0; j < link_num; j++){
			for(int k = 0; k < sample_num; k++){
				rssi_data[i][j][k] = 0;
			}
		}
	}
	
	for(int i = 0; i < cell_num; i++){
		for(int j = 0; j < link_num; j++){
			lda_mean[i][j] = 0;
		}
	}

	for(int i = 0; i < link_num; i++){
		for(int j = 0; j < link_num; j++){
			lda_covariance[i][j] = 0;
		}
	}
}
