#include <iostream>
#include <map>
#include <string>
#include <time.h>
#include <vector>
#include <algorithm>
#include <cmath>
#include <deque>
#include <functional>
#include <mutex>
#include <thread>
#include <tuple>
#include <utility>


#include "grailV3_solver_client.hpp"
#include "sample_data.hpp"
#include "netbuffer.hpp"
#include <world_model_protocol.hpp>
#include <grail_types.hpp>


using world_model::Attribute;
using world_model::grail_time;
using world_model::URI;


using namespace solver_distributor;
using namespace std;

std::vector<Attribute>::iterator findAttribute(std::vector<Attribute>& v, const std::u16string& name) {
	auto is_attribute = [&](Attribute& a) { return a.name == name; };
	return std::find_if(v.begin(), v.end(), is_attribute);
}

u16string tou16String (string& name) {
return u16string(name.begin(), name.end());
}
URI toURI (string& name) {
return URI(name.begin(),name.end());
}


int main(int ac, char** av) {
	//The distributor where tea results are sent
	Distributor grail_dist;

	if (4 != ac and 6 != ac) {
		std::cerr<<"This program needs 3 or 5 arguments:\n";
		std::cerr<<"\t"<<av[0]<<" <world model ip> <solver port> <client port>\n";
		std::cerr<<"\tOr to issue a historic request:\n";
		std::cerr<<"\t"<<av[0]<<" <world model ip> <solver port> <client port> <start time> <stop time>\n";
		std::cerr<<"\tTimes are issued in milliseconds since the epoch, 1970.\n\n";
		return 0;
	}
	bool historic_query = 6 == ac;
	time_t start = 0;
	time_t stop  = 0;
	if (historic_query) {
		start = std::stoll(std::string(av[4]));
		stop = std::stoll(std::string(av[5]));
	}
	//World model IP and ports
	std::string wm_ip(av[1]);
	int solver_port = std::stoi(std::string((av[2])));
	int client_port = std::stoi(std::string((av[3])));

	//Get the IDs and locations of switch sensors from the world server.

	//std::map<uint64_t, std::u16string> chair_name;

	//Structure for room name & its coordinates
	struct room_cord {
		std::string room_name;
		double minx, miny, maxx, maxy;
	} r1;
	std::map <std::string, room_cord> room;

	struct chair_pos {
		std::string chairid;
		double locationx, locationy;
		bool empty;
	};
	std::map<std::string, chair_pos> chair;
	
	std::map<std::string, int> active_chairs_in_room ;
	std::map<std::string,bool> room_in_use ;
	std::map<std::u16string, std::map<string, int>> in_room_count;
	std::map<std::u16string, std::map<string, bool>> used_in_room;
	std::map<std::u16string, bool> chair_in_use;
	std::map<std::u16string, bool> backup_chair_in_use;

	//Set up the solver world model connection;
	std::string origin = "grail/room_in_use\nversion 1.0";
	//Provide room in use as a non transient type
	std::vector<std::pair<u16string, bool>> type_pairs{{u"room_use.int", false}, {u"room_use.bool", false}};
	
	SolverWorldModel swm(wm_ip, solver_port, type_pairs, u16string(origin.begin(), origin.end()));
	if (not swm.connected()) {
		std::cerr<<"Could not connect to the world model as a solver - aborting.\n";
		return 0;
	}

	ClientWorldModel cwm(wm_ip, client_port);
	if (not cwm.connected()) {
		std::cerr<<"Could not connect to the world model as a client - aborting.\n";
		return 0;
	}

  enum class Ticket : uint32_t {chairs, rooms};
	
	//Get attributes named chair and room 
	URI any_chair = u".*chair.*";
	std::vector<URI> chair_attributes{u"sensor.*", u"empty", u"location.xoffset", u"location.yoffset"};
	grail_time interval = 1000;
	cwm.setupSynchronousDataStream(any_chair, chair_attributes, interval, (uint32_t)Ticket::chairs);

	URI any_room = u".*room.*";
	std::vector<URI> room_attributes{u"name", u"location.minx", u"location.maxx", u"location.miny", u"location.maxy"};
	cwm.setupSynchronousDataStream(any_room, room_attributes, interval, (uint32_t)Ticket::rooms);

	//Define some functions to make finding attributes simple
	auto is_chair = [&](Attribute& a) { return a.name.find(u"sensor.switch") != std::u16string::npos;};
	auto is_room = [&](Attribute& a) { return a.name == u"name";};

	cerr<<"Starting loop...\n";
	while (1) {
    //Get world model updates
    ClientWorldModel::world_state ws;
    uint32_t ticket;
    std::tie(ws, ticket) = cwm.getSynchronousStreamUpdate();
		//Make a list of new solutions
		std::vector<SolverWorldModel::Solution> solns;
		//Check each URI 
		
		for (auto I = ws.begin(); I != ws.end(); ++I) {
			
			//Check if there is a roomname attribute
			auto room_attrib = std::find_if(I->second.begin(), I->second.end(), is_room);
			if (room_attrib != I->second.end()) {
				std::string roomid(I->first.begin(), I->first.end());	
				//cout<<"The room name is "<< roomid << "\n";	
				//std::map<std::u16string, vector<Attribute>> pip;

				
				std::cout<<"Checking room ID: "<<roomid<<'\n';
				std::map<std::u16string, vector<Attribute>> pip;
				//vector<Attribute> pip1 = pip[roomid];

				//for (auto pip1 = I->second.begin(); pip1 != I->second.end(); ++pip1)
				auto pip1 = findAttribute(I->second, std::u16string(u"location.minx"));
				if (I->second.end() != pip1 ) {
					room[roomid].minx = readPrimitive<double>(pip1->data); }

				auto pip2 = findAttribute(I->second, u"location.miny");
				if (I->second.end() != pip2 ) {
					room[roomid].miny = readPrimitive<double>(pip2->data); }

				auto pip3 = findAttribute(I->second, u"location.maxx");
				if (I->second.end() != pip3 ) {
					room[roomid].maxx = readPrimitive<double>(pip3->data); }

				auto pip4 = findAttribute(I->second, u"location.maxy");
				if (I->second.end() != pip4 ) {
					room[roomid].maxy = readPrimitive<double>(pip4->data); }

				
			}
			auto chair_attrib = std::find_if(I->second.begin(), I->second.end(), is_chair);
			if (chair_attrib != I->second.end()) {
				std::string chairid(I->first.begin(), I->first.end());
				//Convert the name from X.Y to just .Y.
				auto last_dot = chairid.find_last_of('.');
				if (last_dot == std::string::npos) {
					last_dot = 0;
				}
				else {
					//Go past the last dot.
					++last_dot;
				}
				chairid = chairid.substr(last_dot);
				std::cerr<<"Checking Chair: "<<chairid<<'\n';

				auto pip1 = findAttribute(I->second, u"location.xoffset");
				if (I->second.end() != pip1 ) {
					chair[chairid].locationx = readPrimitive<double>(pip1->data); }

				auto pip2 = findAttribute(I->second, u"location.yoffset");
				if (I->second.end() != pip2 ) {
					chair[chairid].locationy = readPrimitive<double>(pip2->data); }
	
				auto pip3 = findAttribute(I->second, u"empty");
				if (I->second.end() != pip2 ) {
					chair[chairid].empty = readPrimitive<bool>(pip3->data); }
				/*for (auto pip1 = I->second.begin(); pip1 != I->second.end(); ++pip1) 

				  if (pip1.end() != pip1.find(u"location.xoffset") and u"double" == pip1[u"location.xoffset"].data) 
				  chair[chairid].locationx = readPrimitive<double>(pip1[u"location.xoffset"].data, 0); 

				  if (pip1.end() != pip1.find(u"location.yoffset") and u"double" == pip1[u"location.yoffset"].data) 
				  room[chairid].locationy = readPrimitive<double>(pip1[u"location.yoffset"].data,0);
				 */      
			
			}
		}

		/*for ( auto i = room.begin(); i != room.end(); ++i) {
			std::cout<< "The room " << std::string (i->first.begin(),i->first.end()) ;
			std::cout<< " has coordinates " << i->second.minx <<" "<< i->second.miny << " " << i->second.maxx << " " << i->second.maxy <<"\n";
		}
		*/
		double tolerance = 15.0;

	//Aliases for location and chair empty solution types.


		
	
	
	/*auto solutionCallback = [&](SolverWorldModel::Solution& solutions)
		// std::u16string region(solutions.region.begin(), solutions.region.end());
		for (auto sol = solutions.begin(); sol != solutions.end(); ++sol) 
			std::string target(sol->target.begin(), sol->target.end());
			//Convert the name from X.Y to just .Y.
			auto last_dot = target.find_last_of('.');
			if (last_dot == std::string::npos) {
				last_dot = 0;
			}
			else {
				//Go past the last dot.
				++last_dot;
			}
			target = target.substr(last_dot);
			std::cerr<<"Checking name: "<<target<<'\n';
			//std::string name(d_name.begin(), d_name.end());
			uint64_t tar_id;
			try {
				tar_id = stoull(target);
			}
			catch (std::exception& err) {
				//Ignore the stoull error - just don't process the packet.
				continue;
			}
			//TODO FIXME These chairs aren't working right, but will be fixed. This workaround is only for making a nice graph right now.
			if (tar_id == 618) {
				continue;
			}

			if ((sol->type == u"location" or sol->type == u"empty") and chair.find(tar_id) != chair.end()) 
				std::u16string cur_chair = chair[tar_id];
				//Update these values and then determine if the room in use status has changed. 
*/
		for ( auto chairmap = chair.begin(); chairmap!=chair.end(); ++chairmap) {
			string chair_name = chairmap->first;
			u16string cur_chair = tou16String(chair_name);
				//bool& in_use = chair_in_use[cur_chair];
				
			for ( auto j = room.begin(); j!=room.end(); ++j) {
				std::string k = j->first; 
				if (in_room_count.end() == in_room_count.find(cur_chair)) {
					in_room_count[cur_chair][k] = 0;
				}
			}
			if (chair_in_use.end() == chair_in_use.find(cur_chair)) {
				chair_in_use[cur_chair] = false;
				backup_chair_in_use[cur_chair] = false;
			}
		}
			
		for ( auto chairmap = chair.begin(); chairmap!=chair.end(); ++chairmap) {	
			string chair_name = chairmap->first;
			u16string cur_chair = tou16String(chair_name);
			std::map<string,int>& rcount = in_room_count[cur_chair];
			//bool& in_use = chair_in_use[cur_chair];
			//bool& backup_in_use = backup_chair_in_use[cur_chair];
			//cout<<"looking at chair" << string(cur_chair.begin(), cur_chair.end())<<endl;
			//Remember what values were before to check for transitions.

			std::map<std::string, int> prev_active_chairs = active_chairs_in_room;
			std::map<std::string,bool> prev_room_in_use = room_in_use;
				
			for ( auto j = room.begin(); j!=room.end(); ++j) {
				string k = j->first;
				//cout<<"looking at room: " << string(k.begin(), k.end())<<endl;
						
				//Get the location
				double x = chairmap->second.locationx;
				double y = chairmap->second.locationy;

				if (x > j->second.minx - tolerance and
                                    x < j->second.maxx + tolerance and
                                    y > j->second.miny - tolerance and
                                    y < j->second.maxy + tolerance) {
					rcount[k] += 2;
				//cout<< "rcount of " <<string(k.begin(), k.end())<<" is "<< rcount[k]<<endl;
				}
				else {
					rcount[k] -= 1;
				}
				//Constrain in room count to [0,6]
				if (rcount[k] > 6) {
					rcount[k] = 6;
				}
				else if (rcount[k] < 0) {
					rcount[k] = 0;
				}
				//std::cout << std::string (target.begin(),target.end())<<" rcount is "<<rcount[k]<<'\n';
				//TODO If we trusted the switches we would just use this one line
				//in_use = not empty;
				//TODO Instead we do this
				//std::cout<< std::string (sol->target.begin(), sol->target.end()) << " "<<empty<< "\n"; 
				
				bool empty = chairmap->second.empty;
				if (not empty) {
					//if (backup_chair_in_use[cur_chair]){
						chair_in_use[cur_chair] = true;
					/*}
					else {
						backup_chair_in_use[cur_chair] = true;
					}*/
				}
				else {
					chair_in_use[cur_chair] = false;
					//backup_chair_in_use[cur_chair] = false;
				}
						//std::cerr<<tar_id<<" in use is "<<in_use<<'\n';
					

				//Now determine room use
				//Count any chairs with scores of 4 as being in the room.
				bool in_use_in_room = chair_in_use[cur_chair] and (rcount[k] >= 4);
				//Check for an in room chair use transition
				if (in_use_in_room != used_in_room[cur_chair][k]) {
					used_in_room[cur_chair][k] = in_use_in_room;
					if (in_use_in_room) {
						++active_chairs_in_room[j->first];
						std::cerr<<"Chair "<<chairmap->first<<" is now in room " <<string(k.begin(),k.end())<<" and in use.\n";
					}
					else {
						--active_chairs_in_room[j->first];
						std::cerr<<"Chair "<<chairmap->first<<" is now not in use in room "<<string(k.begin(),k.end())<<" \n";
					}
				}
				//If any chairs are in use in the room then the room is in use.
				room_in_use[k] = 0 < active_chairs_in_room[k];
				grail_time soln_time = world_model::getGRAILTime();
				URI roomName= toURI (k);	
				//Send new solutions to the distributor if states changed.
				if (room_in_use[k] != prev_room_in_use[k]) {
					if (room_in_use[k]) { 
						std::cerr<< std::string (k.begin(),k.end()) <<" is in use.\n";
					}
					else { 
						std::cerr<< std::string (j->first.begin(),j->first.end())<<" is not in use.\n";
					}
					
				unsigned char use_status = (room_in_use[k] ? 1 : 0);
				SolverWorldModel::Solution result{u"room_use.bool", soln_time, roomName, vector<uint8_t>()};
				pushBackVal<uint8_t>(use_status, result.data);
				//result.data.push_back(SolutionData{room_solns::used, j->first, {use_status}});
				//TODO Just printing out solutions for now
				//std::cerr<<"The room being used is "<<string(k.begin(),k.end())<<'\n';
				solns.push_back(result);
				}


				if (active_chairs_in_room[k] != prev_active_chairs[k]) {
					SolverWorldModel::Solution result{u"room_use.int", soln_time, roomName, vector<uint8_t>()};
						
					pushBackVal<uint32_t>(active_chairs_in_room[k], result.data);
					//TODO Just printing out solutions for now
					std::cerr<<"The number of people now in "<<string(k.begin(),k.end())<<" is "<<active_chairs_in_room[k]<<'\n';
					solns.push_back(result);
				}
				

			}
		}
		
		if (not solns.empty()) {
		//Do not create URIs for these entries, just send the data
			swm.sendData(solns, false);
		}
	}  
	


	//Connect to the distributor as a solver first, then as a client
	/*if (grail_dist.connect(distributor_ip, solver_port, types)) {

	  if (historic_query) {
	  grailDistributorConnect(distributor_ip, client_port, solutionInterest, solutionCallback, start, stop);
	  }
	//Stream live data
	else {
	grailDistributorConnect(distributor_ip, client_port, solutionInterest, solutionCallback);
	}
	} else {
	std::cerr<<"Error connecting to the GRAIL distributor.\n";
	}
	 */
	

return 0;

}


