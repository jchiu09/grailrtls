#include "passive_localization_utilities.h"

using namespace std;

vector<uint128_t> tx_ids;
vector<uint128_t> rx_ids;
vector<uint128_t> sp_ids;
vector<vector<double> > neighbour;

int cell_num;
int link_num;
int sample_num;

int main(int argc, char *argv[]){

	int rx_id, tx_id, seq; 
	long long time = 0;
	double rssi = 0;

	int buffer;
	int file_line = 0;
	string data;
	ifstream infile;

	infile.open("dfp.txt");
	if (!infile){
		cout<<"File open error!\n";
		return 0;
	}

	while(getline(infile, data)) {
		istringstream s(data);
		if(file_line == 0) {
			while(s>>buffer){
				tx_ids.push_back(buffer);
			}
		}
		else if (file_line == 1) {
			while(s>>buffer){
				rx_ids.push_back(buffer);
			}
		}
		else if (file_line == 2) {
			while(s>>buffer){
				sp_ids.push_back(buffer);
			}
		}
		else if (file_line == 3) {
			s>>cell_num>>sample_num;
			for(int i = 0; i < cell_num; i++) {
				neighbour.push_back(vector<double>(cell_num, 0));
			}
		}
		else if (file_line > 3)	{
			int cell;
			s>>cell;
			neighbour[cell - 1][cell - 1] = 1;
			while(s>>buffer){
				neighbour[cell - 1][buffer - 1] = 1;	
			}
		}
		file_line++;
	}

	infile.close();
	link_num = tx_ids.size() * rx_ids.size();

	//read the base information from file
	vector<double> static_mean (link_num, 0);
	vector<double> static_var (link_num, 0);
	vector<double> static_num (link_num, 0);

	int count = 0;
/*
	//read the environmental RSSI for correction
	infile.open("base.txt.info.txt");

	if (!infile){
		cout<<"File open error!\n";
		return 0;
	}


	//get the info line by line
	while(getline(infile, data)){

			istringstream s(data);

			double mean = 0;
			double var = 0;
			double num = 0;

			s>>mean>>var>>num;

			static_mean[count] = mean;
			static_var[count] = var;
			static_num[count] = num;
			count++;
		}
	}

	infile.close();
*/

	count = 0;    //counter for each test CELL

	//help information
	if((int)atoi(argv[1]) == 0 && argc == 2){
		cout<<endl;
		cout<<"******************************************************************************"<<endl;
		cout<<"Help information for the smarthome program"<<endl;
		cout<<"Command format: program name, function id, file name, parameter 1, parameter 2"<<endl;
		cout<<"Function id 0: display help file"<<endl;
		cout<<"Function id 1: Check if any required radio devices are missing"<<endl;
		cout<<"Function id 2: Calculate the RSS median and variance on all the links when the"<<endl;
		cout<<"room is empty and save it into a file"<<endl;
		cout<<"Function id 3: Calculate the RSS median and variance on all the links for all "<<endl;
		cout<<"the cells and save it into a file"<<endl;
		cout<<"Function id 4: Calculate the RSS change median and variance on all the links  "<<endl;
		cout<<"for all the cells and save it into a file"<<endl;
		cout<<"Function id 98: Record the RSS change on all the links and save it into a file"<<endl;
		cout<<"Function id 99: Record the RSS on all the links and save it into a file"<<endl;
		cout<<"******************************************************************************"<<endl;
		cout<<endl;
	}

	// check if any device is failure
	else if((int)atoi(argv[1]) == 1 && argc == 3){

		count = 0;
		ifstream infile;
		infile.open(argv[2]);

		if (!infile){
			cout<<"File open error!\n";
			return 0;
		}

		vector< vector<double> > raw;
		for(int i = 0; i < link_num; i++){
			raw.push_back(vector<double>());
		}

		//get the info line by line
		while(getline(infile, data)){			
			istringstream s(data);
			rx_id = 0;
			tx_id = 0;
			time = 0;
			rssi = 0;
			seq = 0;
			s>>rx_id>>time>>tx_id>>seq>>rssi;

			while(getline(infile, data)){
				istringstream s(data);
				s>>rx_id>>time>>tx_id>>seq>>rssi;

				for(int tx = 0; tx < tx_ids.size(); tx++){
					for(int rx = 0; rx <= rx_ids.size(); rx++){
						if(rx_id == rx_ids[rx] && tx_id == tx_ids[tx] && (double) rssi >= -100){
							//cerr<<time<<endl; 
							raw[getIndex(tx_ids, tx_id) * rx_ids.size() + 
								getIndex(rx_ids, rx_id)].push_back(rssi);
						}
					}
				}							
			}
		}

		for(int i = 0; i < link_num; i++){
			vector<uint128_t> TxRx = decodeLink(i+1, rx_ids.size());
			cout<<"Link "<<i<<" with tx "<<tx_ids[TxRx[0].lower - 1]
				<<" - rx "<<rx_ids[TxRx[1].lower - 1]<<" record samples "<<raw[i].size()<<endl;
		}

		infile.close();
	}

	// obtain the environmental RSSI vector and write into file
	// format: ./passive_offline_analysis 2 base.txt
	else if((int)atoi(argv[1]) == 2 && argc == 3){
	
		int link = 0;

		double base_mean[link_num];
		double base_var[link_num];
		double base_num[link_num];

		for(int tx = 0; tx < tx_ids.size(); tx++){
			for(int rx = 0; rx < rx_ids.size(); rx++){
				ifstream infile;
				infile.open(argv[2]);

				if (!infile){
					cout<<"File open error!\n";
					return 0;
				}

				while(!infile.eof()){

					//get the info line by line
					while(getline(infile, data)){			

						istringstream s(data);
						rx_id = 0;
						tx_id = 0;
						time = 0;
						rssi = 0;
						seq = 0;
						s>>rx_id>>time>>tx_id>>seq>>rssi;
				
						//check for the click sign
						if(tx_id == sp_ids[0]){

		 					//find the beginning row of the data segment
							while(getline(infile, data)){

								istringstream s(data);
								rx_id = 0;
								tx_id = 0;
								time = 0;
								rssi = 0;
								seq = 0;
								s>>rx_id>>time>>tx_id>>seq>>rssi;

								if(getIndex(tx_ids, tx_id) != -1) {
									break;
								}
							}

							vector<double> raw;

							while(getline(infile, data)){
		
								istringstream s(data);
								s>>rx_id>>time>>tx_id>>seq>>rssi;

								//find the ending row of the data segment
								if (tx_id == sp_ids[0]) {
									break;
								}
								else if(rx_id == rx_ids[rx] && tx_id == tx_ids[tx]
										&& (double) rssi >= -100){
									//cerr<<time<<endl; //find the error timestamp
									raw.push_back(rssi);
								}
							}

							sort(raw.begin(), raw.end());

							int size = raw.size();
										
							if(size % 2 == 1) {
								base_mean[link] = raw[size / 2];
								base_var[link] = getVariance(raw, raw[size / 2]);
							}

							else{
								base_mean[link] = (raw[size / 2 - 1] + raw[size / 2]) / 2; 
								base_var[link] = getVariance(raw, (raw[size / 2 - 1] 
											+ raw[size / 2]) / 2);
							}
							
							base_num[link] = size;
					
							while(getline(infile, data)){
		
								istringstream s(data);
								s>>rx_id>>time>>tx_id>>seq>>rssi;

								//find the beginning of the next data segment
								if(getIndex(tx_ids, tx_id) != -1) {
									break;
								}
							}						
						}
					}	
				}

				link++;

				infile.close();	
			} //rx		
		} //tx

		ofstream myfile;
		string filename = string(argv[2]) + ".info.txt";
		myfile.open(filename.c_str());

		//write into file
		for(int i = 0; i < link_num; i++){
			myfile<<base_mean[i]<<"\t"<<base_var[i]<<"\t"<<base_num[i]<<endl;	
		}

		myfile.close();
	}


	//insert data record based on label (position) into the file
	//record format: area id, RSSI_x1, ... RSSI_x64, clock time
	else if((int)atoi(argv[1]) == 99 && argc == 3){

		ofstream myfile;
		string filename = string(argv[2]) + "_record.txt";
		myfile.open(filename.c_str());

		count = 0;
		ifstream infile;
		infile.open(argv[2]);

		if (!infile){
			cout<<"File open error!\n";
			return 0;
		}

		while(!infile.eof()){
			//get the info line by line
			while(getline(infile, data)){			

				istringstream s(data);
				rx_id = 0;
				tx_id = 0;
				time = 0;
				rssi = 0;
				seq = 0;
				s>>rx_id>>time>>tx_id>>seq>>rssi;

				//check for the click sign
				if(tx_id == sp_ids[0]){
 					//find the beginning row of the data segment
					while(getline(infile, data)){
						istringstream s(data);
						rx_id = 0;
						tx_id = 0;
						time = 0;
						rssi = 0;
						seq = 0;
						s>>rx_id>>time>>tx_id>>seq>>rssi;

						if(getIndex(tx_ids, tx_id) != -1) {
							break;
						}
					}

					//vector for storing the record
					vector< vector<double> > raw;
					for(int i = 0; i < link_num; i++){
						raw.push_back(vector<double>());
					}

					while(getline(infile, data)){
		
						istringstream s(data);
						s>>rx_id>>time>>tx_id>>seq>>rssi;

						//find the ending row of the data segment
						if (tx_id == sp_ids[0]){
							break;
						}
						else {
							for(int tx = 0; tx < tx_ids.size(); tx++){
								for(int rx = 0; rx <= rx_ids.size(); rx++){
									if(rx_id == rx_ids[rx] && tx_id == tx_ids[tx] 
										&& (double) rssi >= -100){
										//cerr<<time<<endl; 
										raw[getIndex(tx_ids, tx_id) * 
											rx_ids.size() + getIndex(rx_ids,
											rx_id)].push_back(rssi);
									}
								}
							}							
						}
					}
		
					//select number of the record for each class
					int temp_n = raw[0].size();
					for(int i = 1; i < link_num; i++){
						if(raw[i].size() <= temp_n){
							temp_n = raw[i].size();
						}
					}

					//insert the record to file
					for(int i = 0; i < temp_n; i++){
						//myfile<<count + 1<<"\t"<<x_pt[count]<<"\t"<<y_pt[count]<<"\t";
						myfile<<count + 1<<"\t";
						for(int j = 0; j < link_num; j++){
							myfile<<raw[j][i] - static_mean[j]<<"\t";cout<<raw[j][i];
						}
						myfile<<endl;
					}

					count++;
			
					while(getline(infile, data)){
	
						istringstream s(data);
						s>>rx_id>>time>>tx_id>>seq>>rssi;

						//find the beginning of the next data segment
						if(getIndex(tx_ids, tx_id) != -1) {
							break;
						}
					}						
				}
			}	
		}

		infile.close();
		myfile.close();
	}

	else{
		cout<<"Error number of input arguments\n";
		return 0;
	}

	return 0;
}

//TODO: two x coordinator: error unit area, distance from the sample position within the area
