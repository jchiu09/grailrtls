//For execvp to start other solvers
#include <unistd.h>

//For kill
#include <sys/types.h>
#include <signal.h>

#include <algorithm>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <thread>
#include <vector>

#include <grailV3_solver_client.hpp>
#include <grail_types.hpp>
#include <netbuffer.hpp>

using std::string;
using std::u16string;
using world_model::URI;
using world_model::grail_time;

char* newCString(std::string& str) {
  char* c = new char[str.size()+1];
  std::copy(str.begin(), str.end(), c);
  c[str.size()] = '\0';
  return c;
}

void startSolver(string name, std::vector<string> args) {
  char** args_p = new char*[args.size()+2];
  args_p[0] = newCString(name);
  args_p[args.size() + 1] = (char*)NULL;
  std::transform(args.begin(), args.end(), args_p + 1, [&](string& str) {
      return newCString(str);});
  execvp(name.c_str(), args_p);
}

int main(int ac, char** av) {
  if (ac != 3) {
    std::cerr<<"Incorrect number of arguments. Proper invocation is:\n";
    std::cerr<<"\t"<<av[0]<<" <world model ip> <client port>\n";
    return 1;
  }

  std::string ip(av[1]);
  uint16_t port = std::stoi(string(av[2]));

  ClientWorldModel cwm(ip, port);

  struct SolverDetails{
    bool running;
    string executable_name;
    std::vector<string> parameter_names;
    std::vector<string> parameter_values;
    std::vector<string> provides;
    std::vector<string> requires;
  };

  std::map<u16string, SolverDetails> solver_details;

  //List of pids for running solvers. If a solver is running to
  //fulfill a dependency but was not requested then it can show
  //up here but be false in the solver_running map.
  std::map<u16string, int> solver_pids;
  //List of which solvers are set to run in the world model
  std::map<u16string, bool> solver_running;
  //Used to check for dependencies
  std::map<string, std::set<u16string>> provides;

  URI all_solvers = u"solvers\\..*";
  std::vector<URI> solver_attributes{u"running.bool", u"executable.string",
    u"parameters.names.vector<sized string>",
    u"parameters.values.vector<sized string>",
    u"provides.vector<sized string>",
    u"requires.vector<sized string>"};
  grail_time interval = 1000;
  cwm.setupSynchronousDataStream(all_solvers, solver_attributes, interval, 1);

  //A function to make it easy to unpack the arrays of u16string into strings
  auto to_string = [&](u16string str) { return std::string(str.begin(), str.end());};
  auto string_unpacker = [&](BuffReader& br){return to_string(br.readSizedUTF16());};

  std::cerr<<"Starting loop...\n";
  while (1) {
    //Get world model updates
    ClientWorldModel::world_state ws;
    uint32_t ticket;
    std::tie(ws, ticket) = cwm.getSynchronousStreamUpdate();
    //Update solver attributes and see if any need to be started or stopped
    for (auto I = ws.begin(); I != ws.end(); ++I) {
      SolverDetails sd;
      //Read out all of the attributes
      for (auto J = I->second.begin(); J != I->second.end(); ++J) {
        if (J->name == u"running.bool") {
          sd.running = readPrimitive<uint8_t>(J->data);
        }
        else if (J->name == u"executable.string") {
          sd.executable_name = to_string(readSizedUTF16(J->data, 0));
        }
        else if (J->name == u"parameters.names.vector<sized string>") {
          sd.parameter_names = grail_types::unpackGRAILVector<string>(string_unpacker, J->data);
        }
        else if (J->name == u"parameters.values.vector<sized string>") {
          sd.parameter_values = grail_types::unpackGRAILVector<string>(string_unpacker, J->data);
        }
        else if (J->name == u"provides.vector<sized string>") {
          sd.provides = grail_types::unpackGRAILVector<string>(string_unpacker, J->data);
        }
        else if (J->name == u"requires.vector<sized string>") {
          sd.requires = grail_types::unpackGRAILVector<string>(string_unpacker, J->data);
        }
      }
      //If this has never appeared in the solver map before then don't
      //try turning on the solver immediately because we may not have
      //read in all of the dependencies yet.
      if (solver_details.end() == solver_details.find(I->first)) {
        sd.running = false;
      }
      else {
        //Check if this solver needs to be stopped or started
        if (sd.running != solver_details[I->first].running) {
          //TODO Check dependencies here and start them.
          //Start the solver
          if (sd.running) {
            string solver_name = to_string(I->first);
            std::vector<string> solver_args{"grail1.winlab.rutgers.edu", "7008", "localhost", "7009", "1000", "6000"};
            int pid = fork();
            //If this is the child then execute the solver
            if (pid == 0) {
              startSolver(solver_name, sd.parameter_values);
            }
            else {
              solver_pids[I->first] = pid;
            }
          }
          //Stop the solver
          else {
            //Kill the solver if we ran it
            if (solver_pids.end() != solver_pids.find(I->first)) {
              kill(solver_pids[I->first], SIGTERM);
              solver_running[I->first] = sd.running;
            }
          }
        }
      }
      solver_details[I->first] = sd;
      solver_running[I->first] = sd.running;
      std::for_each(sd.provides.begin(), sd.provides.end(), [&](const string& str) {
          provides[str].insert(I->first);});
    }
  }
  return 0;
}

