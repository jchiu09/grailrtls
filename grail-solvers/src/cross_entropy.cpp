/*
 * cross_entropy.{cpp,hpp} - Solve optimization problems with cross entropy
 * Source version: January 6, 2011
 * Copyright (c) 2011 Bernhard Firner
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *   1. The origin of this software must not be misrepresented; you must not
 *     claim that you wrote the original software. If you use this software
 *     in a product, an acknowledgment in the product documentation would be
 *     appreciated but is not required.
 *
 *   2. Altered source versions must be plainly marked as such, and must not be
 *      misrepresented as being the original software.
 *
 *   3. This notice may not be removed or altered from any source
 *      distribution.
 */

#include "cross_entropy.hpp"
#include <iostream>

#include <stdlib.h>
#include <ctime>
#include <cmath>

#include <algorithm>
#include <numeric>
#include <vector>
#include <functional>
#include <limits>
#include <utility>

using std::vector;
using std::function;
using std::pair;

bool probToDiscrete(float probability) {
  //Always return false if there is no chance. This simplifies the
  //numeric comparison "rand <= probability" because we don't need
  //to consider when both rand and probability equal 0.
  if (0.0 == probability) {
    return false;
  }
  return ((float)random()/RAND_MAX) <= probability;
}

/**
 * Run the cross entropy method with N simulations per round,
 * using the upper (1-quantile)*N results of each round to
 * form the probability vector for the next round, and using
 * initial_p_vector as the probility vector for round 0.
 * The simulation tries to reach a score of goal.
 * When successive iterations fail to improve by more than min_improve
 * the algorithm will return.
 * The function scoring_sim accepts a decision vector the
 * specifies the behavior for the monte carlo simulation. The
 * score of the simulation with that given decision vector is
 * returned.
 */
vector<CEIteration> runCE(size_t N, float quantile, float goal, float min_improve,
    vector<float> initial_p_vector, function<float (vector<bool>)> scoring_sim) {
  //Seed random number generation
  srandom(time(NULL));

  vector<CEIteration> iterations;
  const size_t quantile_start = ceilf((1 - quantile) * N);
  const size_t quantile_num = N - quantile_start;
  //Set up the initial cross entropy iteration.
  float nan = std::numeric_limits<float>::quiet_NaN();
  CEIteration last_iter{0, nan, nan, nan, initial_p_vector};
  iterations.push_back(last_iter);
  float improvement = 0;

  do {

    //During each round make a test vector of boolean values, which each values
    //set according to the corresponding probability in the probability vector
    //of the last iteration.
    //Store the score from each test vector to determine the probability vector
    //of the next round.
    typedef pair<float, vector<bool>> result;
    vector<result> scores;
    vector<float>& prev_p = last_iter.probability_vector;
    //TODO make this multithreaded and run multiple simulations simultaneously
    for (size_t cur_round = 1; cur_round <= N; ++cur_round) {
      vector<bool> test_v(prev_p.size());
      std::transform(prev_p.begin(), prev_p.end(), test_v.begin(), probToDiscrete);
      float score = scoring_sim(test_v);
      scores.push_back(std::make_pair(score, test_v));
    }

    //Sort the scores by their score value.
    std::sort(scores.begin(), scores.end(),
        [](const result& a, const result& b) ->bool { return a.first < b.first;});

    //Compute the CEIteration value for this iteration using the upper quantile
    //of results.
    last_iter.round = last_iter.round+1;
    last_iter.min_quantile_performance = scores[quantile_start].first;
    last_iter.avg_quantile_performance =
      std::accumulate(scores.begin() + quantile_start, scores.end(), 0.0,
          [](float sum, result& arg) { return sum + arg.first;}) / quantile_num;
    last_iter.avg_performance =
      std::accumulate(scores.begin(), scores.end(), 0.0,
          [](float sum, result& arg) { return sum + arg.first;}) / N;

    //Sum the numbers of times each bit was set in the upper quantile and use
    //that result to set the probabilities for the next round.
    vector<float> new_probs(prev_p.size());
    for (auto I = scores.begin() + quantile_start; I != scores.end(); ++I) {
      vector<bool>& used = I->second;
      for (size_t option = 0; option < prev_p.size(); ++option) {
        if ( used[option] ) {
          new_probs[option] += 1.0;
        }
      }
    }
    //Normalize to find the average.
    std::for_each(new_probs.begin(), new_probs.end(),
        [=](float& arg){ arg /= quantile_num; });
    last_iter.probability_vector = new_probs;

    //Always accept the first iteration
    if (isnan(iterations.back().min_quantile_performance)) {
      improvement = min_improve+1;
    } else {
      improvement = last_iter.avg_quantile_performance -
        iterations.back().avg_quantile_performance;
    }

    iterations.push_back(last_iter);

  } while (improvement > min_improve);

  return iterations;
}

