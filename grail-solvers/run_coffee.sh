#! /usr/bin/bash
#Example script to help run the coffee solver

agg_ip="127.0.0.1"
agg_solv_port="7008"

distrib_ip="127.0.0.1"
dist_solv_port="7009"
dist_client_port="7010"

worldip="127.0.0.1"
worldport="7011"


echo "./bin/temperature_client $agg_ip $agg_solv_port $distrib_ip $dist_solv_port"
./bin/temperature_client $agg_ip $agg_solv_port $distrib_ip $dist_solv_port &
echo "./bin/coffee_solver $distrib_ip $dist_solv_port $dist_client_port $worldip $worldport"
./bin/coffee_solver $distrib_ip $dist_solv_port $dist_client_port $worldip $worldport &

