.PHONY: aggregator world_model install run

all: aggregator world_model

install:
	cp grail3_java/aggregator/target/aggregator-3.0.2-SNAPSHOT-jar-with-dependencies.jar /usr/local/bin
	cd grail-solvers;cp bin/world_model_server /usr/local/bin

#Build the java aggregator
aggregator:
	cd grail3_java/;mvn install -pl aggregator -am

world_model:
	cd grail-solvers;make world_model

